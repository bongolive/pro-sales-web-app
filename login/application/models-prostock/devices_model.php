<?php
class Devices_model extends MY_Model {
	
	
	public $table = 'pro_devices';
	public $table_id = 'device_id';

	function __construct() {
		parent::__construct();
	}
	
	
	public function form_validation() {

		$this -> form_validation -> set_rules('device_imei', 'Device Imei', 'trim|required');
				//$this -> form_validation -> set_rules('assigned_on', 'Assigned date', 'trim|required');
		 
	}
	
	public function read_devices($where)
    {
        $this->db->select('*,CONCAT(firstname," ",lastname) as employee_name', FALSE);

        $this->db->from($this->table);
        $this->db->where($where);
        $this->db->join('employees', 'employees.id = devices.assigned_to', 'left');
        $data = $this->db->get()->result_array();
        if ($data) {
            return $data;
        } else {
            return FALSE;
        }
    }
	
	public function get_devicelistbyid($deviceid = FALSE)
	{
		if ($deviceid === FALSE)
		{
			$query = $this->db->get('phones');
			return $query->result_array();
		}
	
		$query = $this->db->get_where('phones', array('phoneid' => $deviceid));
		return $query->row_array();
	}
	
	
	public function get_devicelistbyImei($imei)
	{
		$query = $this->db->get_where($this->table, array('device_imei' => $imei));
		return $query->row_array();
	}
	
	
	public function get_device($cond)
	{
		$query = $this->db->get_where('phones',$cond);
		return $query->row_array();
	}
	
	
	public function get_devicelistbyuserid($userid = FALSE)
	{
		if ($userid === FALSE)
		{
			$query = $this->db->query("select phones.*,phones_users.userid,users.name as client_name from phones,phones_users,users where phones.phoneid=phones_users.phoneid and phones_users.userid=users.userid");
			return $query->result_array();
		}
	
		$query = $this->db->query("select phones.*,phones_users.userid,users.name as client_name from phones,phones_users,users where phones.phoneid=phones_users.phoneid and phones_users.userid=users.userid and phones_users.userid=".$userid);
		return $query->result_array();
	}

    // i added this to get all devices based on account
    public function getDeviceByAccountNo($account){
        $this->db->where('account_id',$account);
        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            return $query->result_array();
        }
    }


	public function add_device($data)
	{
		return $this->db->insert('phones', $data);
	}
	
	public function update_divice($data,$cond)
	{
		$this->db->where($cond);
		return $this->db->update('phones', $data);
	}
	
	public function update($data,$phoneid)
	{
		$this->db->where('device_id',$phoneid);
		return $this->db->update('devices', $data);
	}



    public function update_diviceByImei($data,$imei)
	{
		$this->db->where('phone_imei',$imei);
		return $this->db->update('phones', $data);
	}
	
	public function add_device_users($data)
	{
		return $this->db->insert('phones_users', $data);
	}
	
	public function deleteDeviceUsersByPhoneId($phoneid)
	{
		return $this->db->delete('phones_users', array('phoneid' => $phoneid)); 
	}
	
	public function deleteDeviceByPhoneId($phoneid)
	{
		return $this->db->delete('phones', array('phoneid' => $phoneid)); 
	}
	
	
	public function get_deviceFromPhonesUsers($userid,$imei)
	{
	
		$query = $this->db->query("select * from phones_users where userid = $userid and imei = '$imei'");
		return $query->result_array();
	}
	
	public function get_deviceLocationByImei($imei,$sdate,$edate)
	{
		$query = $this->db->query("select * from pro_location where imei = '$imei' and length(imei) > 0 and date between '$sdate' and '$edate'");
		return $query->result_array();
	}
	
	public function delete($deviceid, $userid)
	{
		$data =array(
				'phoneid' => $deviceid,
				'userid' => $userid
		);
		return $this->db->delete('phones_users', $data);
	}
	
	public function get_lastLocation($userid)
	{
		$query = $this->db->query("SELECT pu.phoneid, pu.imei, p.device_name, p.mobile_no, p.holder_name , 
	ifNull((SELECT l.latitude FROM location l WHERE l.imei = pu.imei ORDER BY l.servertime DESC LIMIT  1 ),NULL) AS latitude ,
	ifNull(( SELECT l.longitude FROM location l WHERE l.imei = pu.imei ORDER BY l.servertime DESC LIMIT  1 ),NULL) AS longitude,
	ifNull(( SELECT l.date FROM location l WHERE l.imei = pu.imei ORDER BY l.date DESC LIMIT  1),NULL) AS localdate
	 FROM phones_users pu INNER JOIN phones p ON pu.imei = p.phone_imei WHERE pu.userid =$userid");
		return $query->result_array();
	}

    public function getLastKnownLocation($where){
         $query = $this->db->query("SELECT e.id,CONCAT(e.firstname, ' ',e.lastname) AS holder_name, e.mobile, de.device_id,de.device_imei,de.assigned_to,de.device_descr AS device_name,
  ifnull((SELECT l.latitude FROM pro_location l WHERE l.imei = de.device_imei ORDER BY l.servertime DESC LIMIT 1),NULL ) AS latitude,
  ifnull((SELECT l.longitude FROM pro_location l WHERE l.imei = de.device_imei ORDER BY l.servertime DESC LIMIT 1),NULL ) AS longitude,
  ifnull((SELECT l.date FROM pro_location l WHERE l.imei = de.device_imei ORDER BY l.servertime DESC LIMIT 1),NULL ) AS localdate
FROM pro_employees e INNER JOIN pro_devices de ON e.id = de.assigned_to AND e.account_id = '$where'");
       if($query){
           return $query->result_array();
       }
    }

    public function _api_get_device_account($imei){
        $this->db->where('device_imei', $imei);
        $result = $this->db->get('pro_devices');
        if($result->num_rows()  >0 ){
            $j = $result->row();
            return $j->account_id ;
        }
    }
    public function _api_store_multimedia($data,$general){
        $ret = array();
        foreach($data  as $value){
            $datatype = $value['type'];
            switch($datatype){
                case 'image':
                    $save = $this->saveImageFrom($value,$general);
                    if(is_array($save)){
                        $size = $save['size'];
                        $type = $save['type'];
                        $name = $save['name'];
                        $content = $save['path'];
                        if(!$this->_api_is_media_existing(array('device_imei' => $general['device_imei'],
                            'md_name'=>$name))){
                            $insert = array('md_size' => $size,'md_path' => $content,'account_id' => $general['account_id'],
                                'device_imei' => $general['device_imei'],'md_update' => date('Y-m-d h:i:s'),
                                'md_type' => $type, 'md_name' => $name);
                            $this->db->insert('pro_media',$insert);

                            if($this->db->affected_rows()){
                                $ret[] = array('id'=>$value['id']);
                            }
                        } else {
                            $ret[] = array('id'=>$value['id']);
                        }
                    }
                    break;
                case 'video':
                    $vid = $this->saveVideoFrom($value,$general);
                    if(is_array($vid)){
                        $sizev = $vid['size'];
                        $typev = $vid['type'];
                        $namev = $vid['name'];
                        $contentv = $vid['path'];
                        if(!$this->_api_is_media_existing(array('device_imei' => $general['device_imei'],
                            'md_name'=>$namev))) {
                            $insertv = array('md_size' => $sizev, 'md_path' => $contentv, 'account_id' =>
                                $general['account_id'], 'device_imei' => $general['device_imei'], 'md_update' => date
                            ('Y-m-d h:i:s'), 'md_type' => $typev, 'md_name' => $namev);
                            $this->db->insert('pro_media', $insertv);

                            if ($this->db->affected_rows()) {
                                $ret[] = array('id' => $value['id']);
                            }
                        } else {
                            $ret[] = array('id' => $value['id']);
                        }
                    }
                    break;
                case 'sound':
                    $sid = $this->saveAudioFrom($value,$general);
                    if(is_array($sid)){
                        $sizea = $sid['size'];
                        $typea = $sid['type'];
                        $namea = $sid['name'];
                        $contenta = $sid['path'];
                        if(!$this->_api_is_media_existing(array('device_imei' => $general['device_imei'],
                            'md_name'=>$namea))) {
                            $inserta = array('md_size' => $sizea, 'md_path' => $contenta, 'account_id' =>
                                $general['account_id'], 'device_imei' => $general['device_imei'], 'md_update' => date
                            ('Y-m-d h:i:s'), 'md_type' => $typea, 'md_name' => $namea);
                            $this->db->insert('pro_media', $inserta);

                            if ($this->db->affected_rows()) {
                                $ret[] = array('id' => $value['id']);
                            }
                        } else {
                            $ret[] = array('id' => $value['id']);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
            return $ret;

    }
    public function saveImageFrom($data,$general){

            $imgname = $data['name'];
            $target = 'images/stock/'.$general['device_imei'].'/image' ;
        if(!file_exists($target)){
            mkdir($target,0755, true);
        }
        $filename = $general['device_imei'].'_'.$imgname;

//        if(!file_exists($target.'/'.$filename)){
            $imsrc = base64_decode($data['content']);
            $path = $target.'/'.$filename;

            $fp = fopen($path, 'w');
            fwrite($fp, $imsrc);
            $size = filesize($path);

            $f = finfo_open();
            $type = finfo_buffer($f,$imsrc,FILEINFO_MIME);
            $type = explode(";", $type);

            if(fclose($fp)) {
                return array('path' => $path,'size' => $size,'name' => $filename,'type' =>  $type[0]);
            }
      /*  } else{
            return false;
        }*/

    }

    public function saveVideoFrom($data,$general){

            $vdoname = $data['name'];
            $target = 'images/stock/'.$general['device_imei'].'/video' ;
           if(!file_exists($target)){
               mkdir($target,0755, true);
           }
           $filename = $general['device_imei'].'_'.$vdoname;

//        if(!file_exists($target.'/'.$filename)){
            $imsrc = base64_decode($data['content']);
            $path = $target.'/'.$filename;

            $fp = fopen($path, 'w');
            fwrite($fp, $imsrc);
            $size = filesize($path);

            $f = finfo_open();
            $type = finfo_buffer($f,$imsrc,FILEINFO_MIME);
            $type = explode(";", $type);

            if(fclose($fp)) {
                return array('path' => $path,'size' => $size,'name' => $filename,'type' => $type[0]);
            }
       /* } else{
            return false;
        }*/

    }
    public function saveAudioFrom($data,$general){
            $vdoname = $data['name'];
            $target = 'images/stock/'.$general['device_imei'].'/audio' ;
        if(!file_exists($target)){
            mkdir($target,0755, true);
        }
           $filename = $general['device_imei'].'_'.$vdoname;

//        if(!file_exists($target.'/'.$filename)){
            $imsrc = base64_decode($data['content']);
            $path = $target.'/'.$filename;

            $fp = fopen($path, 'w');
            fwrite($fp, $imsrc);
            $size = filesize($path);

            $f = finfo_open();
            $type = finfo_buffer($f,$imsrc,FILEINFO_MIME);
            $type = explode(";", $type);

            if(fclose($fp)) {
                return array('path' => $path,'size' => $size,'name' => $filename,'type' => $type[0]);
            }
       /* } else{
            return false;
        }*/

    }

    public function _api_is_media_existing($where){
        $this->db->where($where);
        $query = $this->db->get('pro_media');
        if($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

}