<div class="box box-danger">
	<div class="box box-danger">
		<div class="box-header">
			<h3 class="box-title"><?php echo lang('filtering') ?></h3>
		</div>
		<div class="box-body">
			<?php
			if ($controller == 'reports/surveyor_report') {
				$action = $controller.'/'.$tb_data[0]['interviewer_no'];
			} elseif ($controller == 'reports/location_report') {
				$action = $controller.'/'.$tb_data[0]['location_code'];
			} elseif ($controller == 'reports/survey_report') {
				$action = $controller.'/'.$tb_data[0]['survey_no'];
			}
			?>
			<form method="post" action="<?php echo site_url($action); ?>">
				<?php				if($controller =='reports/surveyor_report'){ ?>
					<input name="surveyor" type="hidden" value="<?php echo $tb_data[0]['interviewer_no']; ?>" >
			<?php	}elseif($controller =='reports/location_report'){ ?>
					<input name="location" type="hidden" value="<?php echo $tb_data[0]['location_code']; ?>" >
			<?php	}elseif($controller =='reports/survey_report'){ ?>
					<input name="survey" type="hidden" value="<?php echo $tb_data[0]['survey_no']; ?>" >
			<?php	} ?>
			<div class="row">
				<?php	 if($controller !='reports/survey_report'){ ?>
				<div class="col-xs-3">
					<?php echo form_dropdown('survey', $surveys, '', 'class="form-control"'); ?>
					
				</div>
				<?php
				}
				if($controller !='reports/surveyor_report'){				?>
				
				<div class="col-xs-3">
					<?php echo form_dropdown('interviewer', $interviewer, '', 'class="form-control"'); ?>
					
				</div>
				
				<?php } if($controller !='reports/location_report'){				?>
				<div class="col-xs-3">
					<?php echo form_dropdown('location', $locations, '', 'class="form-control"'); ?>
					
				</div>
				<?php } ?>
				<div class="col-xs-2">
					<?php
					$status = array('' => 'PMTCT Status', '1' => 'PMTCT Status 1', '2' => 'PMTCT Status 2');
					echo form_dropdown('pmtcp_status', $status, '', 'class="form-control"');
				?>
				</div>
				
				<div class="col-xs-3"> 
					 
					<input class="form-control span2" style="width: 40%; display: inline" id='dp1' name="startdate" type="text" placeholder="Start Date">
					<input class="form-control span2" style="width: 40%; display:  inline" id='dp2' name="enddate" type="text" placeholder="End Date">
				</div>
				<div class="col-xs-3"> 
					
					<input class="btn btn-info"   type="submit" value="Submit" >
					<a href="<?php site_url($controller); ?>" type="button" class="btn btn-default"><?php echo Lang('view_all'); ?></a>
					
					
					
				</div>
			</div>
			</form>
		</div>
	</div>
