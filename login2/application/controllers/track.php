<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Track extends User_Controller {
	
	 public $session_userid = '';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('devices_model');
		$this->session_userid = $this->session->userdata('userid');
	}
	
	
	public function index()
	{
		$condUserPerm = array('userid'=>$this->session->userdata('userid'));
		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] ="";
		$header['device_tab'] ="";
		$header['tasks_tab'] ="";
		$header['reports_tab'] ="";
		$header['deliveries_tab'] ="";
		$header['customers_tab'] ="";
		$header['boundaries_tab'] ="";
		$header['support_tab'] ="allow";
		$header['pathfinder_tab'] ="";
		$header['stocks_tab'] ="";
		$header['registrations_tab'] ="";
		
		foreach($user_permissions as $user_permission){
			if($user_permission['permission_name']=='track tab'){
				$header['track_tab'] ="allow";
			}else if($user_permission['permission_name']=='device tab'){
				$header['device_tab'] ="allow";
			}if($user_permission['permission_name']=='tasks tab'){
				$header['tasks_tab'] ="allow";
			}if($user_permission['permission_name']=='reports tab'){
				$header['reports_tab'] ="allow";
			}if($user_permission['permission_name']=='deliveries tab'){
				$header['deliveries_tab'] ="allow";
			}if($user_permission['permission_name']=='customers tab'){
				$header['customers_tab'] ="allow";
			}if($user_permission['permission_name']=='boundaries tab'){
				$header['boundaries_tab'] ="allow";
			}if($user_permission['permission_name']=='pathfinder survey tab'){
				$header['pathfinder_tab'] ="allow";
			}if($user_permission['permission_name']=='stocks tab'){
				$header['stocks_tab'] ="allow";
			}if($user_permission['permission_name']=='registrations tab'){
				$header['registrations_tab'] ="allow";
			}						
		}
		
		if($header['track_tab'] !="allow"){
			redirect('profile/support');
		}
		
		$fields['devices'] = $this->devices_model->get_devicelistbyuserid($this->session_userid );
        $header['tr_active'] = 'class="active"';
        $fields['tb_name'] = 'track_tb_name';
        $this->load->view('template/header',$header);
        $this->load->view('template/content/track',$fields);
        $this->load->view('template/footer');
	}
	
	public function lastlocation()
	{
		$devices = $this->devices_model->get_lastLocation($this->session_userid);
		if(count($devices)>0){
			$xml = "<locations>";
		
			foreach($devices as $row){
				$xml .= "<location>";
			
				$xml .= "<phoneid>".$row["phoneid"]."</phoneid>";
				$xml .= "<imei>".$row["imei"]."</imei>";
				$xml .= "<devicename>".$row["device_name"]."</devicename>";
				$xml .= "<mobileno>".$row["mobile_no"]."</mobileno>";
				$xml .= "<holdername>".$row["holder_name"]."</holdername>";
				$xml .= "<latitude>".$row["latitude"]."</latitude>";
				$xml .= "<longitude>".$row["longitude"]."</longitude>";
				$xml .= "<localdate>".$row["localdate"]."</localdate>";
			
				$xml .= "</location>";
			}
		
			$xml .= "</locations>";
		
			echo $xml;
		}else{
			echo "ERROR. !";
		}
	}
	
	public function showtrack()
	{
		$tracks_count = 0;
		$data = "";
		$start = "NO";
		$user_dtl = "";
		$start = urldecode($this->uri->segment(4));
        $sdate = urldecode($this->uri->segment(5));
        $edate = urldecode($this->uri->segment(6));
        $thc = array('-', 'A');
        $replace = array('/',':');
        $sdate = str_replace($thc,$replace,$sdate);
        $edate = str_replace($thc,$replace,$edate);
        //echo $start.$sdate.$edate;
		if(($start != 'start') && $sdate && $edate){
	
			if($this->input->post('start') == "start"){
				$start = "YES";
			}else{
				$date = date_create($sdate);
				$sdate = date_format($date, 'Y-m-d H:i:s');
				$date = date_create($edate);
				$edate = date_format($date, 'Y-m-d H:i:s');
				$imei = $start;
				$res = $this->devices_model->get_deviceLocationByImei($imei,$sdate,$edate);
		
				if (count($res)==0)
				{
					//die('Error: ' . mysql_error());
                    //var_dump($res);
                    $start = "NO";
                    
				}else{
					$tracks_count = count($res);
	
					if($tracks_count > 0){
						$i = 0;
						foreach($res as $r) {
					
							$data .= "[ '".$r['date']."' , ".$r['latitude'].", ".$r['longitude']." ] ".(($tracks_count-1 == $i ) ? "" : ", " );
							$i += 1;
						}

						$r  = $this->devices_model->get_devicelistbyImei($imei);
				
						if(count($r) >0){
							$user_dtl .= "'".$r["phone_imei"]."', '".$r["device_name"]."', '".$r["mobile_no"]."', '".$r["holder_name"]."' ";
						}
				
					}else{
						$tracks_count = 0;
					}
			
				}
			}
		}
			
        $content['start'] = $start;
		$content['user_dtl'] = $user_dtl;
		$content['data'] = $data;
		$content['user_dtl'] = $user_dtl;
		//Load shoetrack view here
		$this->load->view('template/content/showtrack',$content);
	}
    
    public function showdevices(){
        
        $this->load->view('template/content/show_devices');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
