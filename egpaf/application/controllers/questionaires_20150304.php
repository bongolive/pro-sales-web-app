<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class questionaires extends  My_Controller {
	public $user_id;
	public $account_id;

	public function __construct() {
		parent::__construct();
		if ($this -> session -> userdata('is_login') == FALSE) {
			redirect('login');
		}

		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('question_text', 'question_type', 'section_title', 'answer_choices', 'question_rank', 'language', );

		$this -> load -> model('questionaires_model');
		$this -> load -> model('surveys_model');
		$this -> data['table_id'] = $this -> questionaires_model -> table_id;

		$this -> questionaires_model -> form_validation();
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');

		//print_r($this->session->all_userdata());
		$this -> user_id = $this -> session -> userdata('user_id');
		$this -> account_id = $this -> session -> userdata('account_id');

		$this -> data['controller'] = 'questionaires';
		$this -> data['edit'] = 'questionaires/update_question';
		$this -> data['view'] = 'questionaires/view';
		$this -> data['delete'] = 'questionaires/delete';

	}

	public function index() {
		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('question_text', 'question_type', 'section_title', 'question_rank', 'language', );
		$surveys = $this -> surveys_model -> read(array('account_no' => $this -> account_id));
		foreach ($surveys as $surv) {
			$survey[$surv['survey_id']] = $surv['survey_title'];
		}

		$where = array('questionaires.account_no' => 1, 'questionaires.survey_no' => 1, 'language' => 'en');
		//filtering data

		if ($this -> input -> post('filter')) {
			extract($_POST);

			if ($survey_title) {
				$where['survey_no'] = $survey_title;
			}
			if ($survey_title == FALSE) {
				$where = FALSE;
			}

		}

		$questionaires = $this -> questionaires_model -> read_questions($where);

		if ($questionaires) {

			//$q_count = $this->questionaires_model->mycounter();
			//this will display the links to pages
			//$this->data['pagenate'] = $this -> pagination($q_count, $this -> limit, $this -> page);

			$this -> data['tb_data'] = $questionaires;
		} else {
			$this -> data['tb_data'] = FALSE;
		}

		$this -> data['tb_name'] = 'questionaires_tb_name';
		$this -> data['surveys'] = $survey;
		$this -> data['edit'] = 'questionaires/update_question';
		$this -> data['view'] = 'questionaires/view';
		$this -> data['delete'] = 'questionaires/delete';
		$this -> data['create'] = FALSE;
		///  'questionaires/create/' . $survey . '/' . $section;
		$this -> data['back'] = 'surveys';
		$this->session->set_userdata('sortable',TRUE);

		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/questionaires_rerank');
		$this -> load -> view('template/footer');

	}

	//questions reordering

	public function reordering() {
		// get the list of items id separated by cama (,)
		extract($_POST);

		// convert the string list to an array

		$list = explode(',', $list_order);

		$i = 1;
		foreach ($list as $id) {
			try {
				$data = array('question_rank' => $i);
				//get old question rank
				$qn = $this -> questionaires_model -> simple_read(array('question_id' => $id));

				if ($qn) {

					if ($this -> questionaires_model -> simple_update_qn($id, $data)) {

						//update the translation
						$this -> questionaires_model -> update_translation($id, $data);

						//updating the next question option
						$this -> update_choice(array('question_rank' => $qn[0]['question_rank'], 'survey_no' => $qn[0]['survey_no'], 'new_rank' => $i));
					}
				}

			} catch (PDOException $e) {
				echo 'PDOException : ' . $e -> getMessage();
			}

			$i++;
		}

	}

	public function update_choice($data) {

		//get the question details on survey
		$where = array('next_question' => $data['question_rank'], 'survey_no' => $data['survey_no']);
		$choices = $this -> questionaires_model -> read_answer_options($where);
		if ($choices) {

			foreach ($choices as $choice) {
				//update the choices
				$updates = array('next_question' => $data['new_rank']);
				$this -> questionaires_model -> update_answer_choices($choice['choice_id'], $updates);
			}

		}

	}

	public function view() {

		$where = array('question_id' => $this -> uri -> segment(3));

		$questionaires = $this -> questionaires_model -> read_questions($where);

		if ($questionaires) {
			foreach ($questionaires as $questionaires) {
			}

			$this -> data['data'] = $questionaires;
			//get questions answer options

			$answers = $this -> questionaires_model -> read_answer_options(array('question_no' => $questionaires['question_id']));
			if ($answers) {
				$this -> data['answers'] = $answers;
			} else {
				$this -> data['answers'] = FALSE;
			}

			//get the translated text
			$transalation = $this -> questionaires_model -> read_questions(array('transalation_of' => $questionaires['question_id']));

			if ($transalation) {
				$this -> data['transalation'] = $transalation[0];
				$transchoices = $this -> questionaires_model -> read_answer_options(array('question_no' => $transalation[0]['question_id']));

				if ($transchoices) {

					$this -> data['transchoices'] = $transchoices;
				} else {
					$this -> data['transchoices'] = FALSE;
				}

			} else {

				$this -> data['transalation'] = $this -> data['transchoices'] = FALSE;
				$this -> data['transchoices'] = FALSE;
			}

		} else {
			$this -> data['data'] = FALSE;
		}
		$this -> data['tb_name'] = 'questionaires_tb_name';

		$this -> data['stc_active'] = 'class="active"';
		$this -> data['add_btn'] = 'add_new_stock';

		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/questionaires_view', $this -> data);
		$this -> load -> view('template/footer');

	}

	public function create() {

		if ($this -> form_validation -> run() == FALSE) {

			$this -> data['info'] = FALSE;
			$this -> data['survey'] = $this -> uri -> segment(3);
			$this -> data['section'] = $this -> uri -> segment(4);
			//get questions
			$surveys = $this -> questionaires_model -> surveys(array('account_no' => $this -> account_id));
			if ($surveys) {
				foreach ($surveys as $survey) {
					$survy[$survey['survey_id']] = $survey['survey_title'];
				}
			} else {
				$survy = FALSE;
			}
			$this -> data['answers'] = FALSE;
			$this -> data['surveys'] = $survy;
			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/questionaires_form', $this -> data);
			$this -> load -> view('template/footer');
			$this -> load -> view('template/auto_rows');

		} else {
			extract($_POST);
//read the last question rank in the survey question list
$where = array('survey_no'=>$survey_no);
$rank  = $this->questionaires_model->get_last_qn_rank($where);

$rank = $rank+1;
			$question = array('question_text' => $question, 'question_type' => $question_type, 'language' => 'en', 'survey_section' => $section_no, 'survey_no' => $survey_no, 'question_rank' => $rank, 'account_no' => $this -> account_id);

			//	print_r($question);
			$this -> db -> trans_start();
			if ($this -> questionaires_model -> save($question)) {
				$qn_id = $this -> db -> insert_id();

				$choices = count($response_text);
				for ($i = 0; $i < $choices; $i++) {
					if (isset($required[$i])) {
						$req = 1;
					} else {
						$req = 0;
					}

					$sql_choice = array('question_no' => $qn_id, 'survey_no' => $survey_no, 'max_value' => $max_value[$i], 'min_value' => $min_value[$i], 'data_type' => $data_type[$i], 'required' => $req, 'choice_no' => $choice_no[$i], 'response_text' => $response_text[$i], 'next_question' => $next_question[$i], 'match_with' => $match_with[$i], 'account_no' => $this -> account_id);
					//	print_r($sql_choice);
					$this -> questionaires_model -> save_answer_choices($sql_choice);
				}
			}
			$this -> db -> trans_complete();
			redirect('questionaires/view/' . $qn_id);
		}

	}

	public function update_question() {

		if ($this -> form_validation -> run() == FALSE) {

			$where = array('question_id' => $this -> uri -> segment(3));

			$questionaires = $this -> questionaires_model -> read($where);

			if ($questionaires) {
				foreach ($questionaires as $questionaires) {
				}
				$this -> data['info'] = $questionaires;

				//get questions answer options

				$answers = $this -> questionaires_model -> read_answer_options(array('question_no' => $questionaires['question_id']));
				if ($answers) {
					$this -> data['answers'] = $answers;
				} else {
					$this -> data['answers'] = FALSE;
				}

			} else {
				$this -> data['answers'] = $this -> data['info'] = FALSE;
			}

			//get questions
			$surveys = $this -> questionaires_model -> surveys(array('account_no' => $this -> account_id));
			if ($surveys) {
				foreach ($surveys as $survey) {
					$survy[$survey['survey_id']] = $survey['survey_title'];
				}
			} else {
				$survy = FALSE;
			}
			$this -> data['surveys'] = $survy;
			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/questionaires_form');
			$this -> load -> view('template/footer');
			$this -> load -> view('template/auto_rows');

		} else {
			extract($_POST);

			$question = array('question_text' => $question, /*'survey_section' => $section_no,*/
			'survey_no' => $survey_no, 'question_rank' => $rank, 'question_type' => $question_type);

			if ($this -> questionaires_model -> update($question_id, $question)) {

				$tqn = $this -> questionaires_model -> read(array('transalation_of' => $question_id));

				//affect changes to the translation question
				$tdata = array('question_type' => $question_type, 'question_rank' => $rank);
				//	$this -> questionaires_model -> update($tqn[0]['question_id'], $tdata);

				//get translation question option choices
				$trans_choices = $this -> questionaires_model -> read_answer_options(array('question_no' => $tqn[0]['question_id']));

				//get all choices
				$firstchoices = $this -> questionaires_model -> read_answer_options(array('question_no' => $question_id));
				print_r($response_id);
				//check for existance
				$i = 0;
				foreach ($firstchoices as $fchoices) {
					if ($response_id[$i]) {
						if (in_array($fchoices['choice_id'], $response_id)) {
							if (isset($required[$i])) {
								$req = 1;
							} else {
								$req = 0;
							}

							$ch_updates = array('question_no' => $question_id, 'survey_no' => $survey_no, 'max_value' => $max_value[$i], 'min_value' => $min_value[$i], 'data_type' => $data_type[$i], 'required' => $req, 'choice_no' => $choice_no[$i], 'response_text' => $response_text[$i], 'next_question' => $next_question[$i], 'match_with' => $match_with[$i], 'account_no' => $this -> account_id);
							$this -> questionaires_model -> update_answer_choices($fchoices['choice_id'], $ch_updates);
							//update the translation
							$transt = array('max_value' => $max_value[$i], 'survey_no' => $survey_no, 'min_value' => $min_value[$i], 'data_type' => $data_type[$i], 'required' => $req, 'choice_no' => $choice_no[$i], 'next_question' => $next_question[$i], 'match_with' => $match_with[$i]);
							$this -> questionaires_model -> update_answer_choices($trans_choices[$i]['choice_id'], $transt);
						} else {

							$this -> questionaires_model -> delete_choices($fchoices['choice_id']);
							//delete the transalation choice

							$this -> questionaires_model -> delete_choices($trans_choices[$i]['choice_id']);

						}
					} else {
						//	$sql_choice = array('question_no' => $question_id, 'survey_no' => $survey_no, 'max_value' => $max_value[$i], 'min_value' => $min_value[$i], 'data_type' => $data_type[$i], 'required' => $req, 'choice_no' => $choice_no[$i], 'response_text' => $response_text[$i], 'next_question' => $next_question[$i], 'match_with' => $match_with[$i], 'account_no' => $this -> account_id);
						//$this -> questionaires_model -> save_answer_choices($sql_choice);
					}
					$i++;
				}
			}

			redirect('questionaires/view/' . $question_id);
		}

	}

	function my_counter($where = False) {

		$this -> db -> select("count($this->table_id) as total");
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}

		$result = $this -> db -> get() -> result_array();
		if ($result) {
			foreach ($result as $key => $value) {

			}
			return $value;
		} else {
			return false;
		}
	}

	/*
	 * questionaires categories ends
	 *
	 */

	public function delete() {

		$id = $this -> uri -> segment(3);

		$qn = $this -> questionaires_model -> read(array('question_id' => $id));

		if ($this -> questionaires_model -> delete($id)) {
			//delete question responses
			$this -> questionaires_model -> delete_choices($id);

		}
		redirect('surveys/view_section/' . $qn[0]['survey_no'] . '/' . $qn[0]['survey_section']);

	}

	public function save_translation() {
		extract($_POST);

		$where = array('question_id' => $question_id);

		$question = $this -> questionaires_model -> read($where);
		foreach ($question as $key => $value) {

		}

		//get questions answer options

		$answers = $this -> questionaires_model -> read_answer_options(array('question_no' => $question_id));

		///save the question

		$question = array('question_text' => $question_text, 'question_type' => $value['question_type'], 'transalation_of' => $question_id, 'survey_section' => $value['survey_section'], 'survey_no' => $value['survey_no'], 'question_rank' => $value['question_rank'], 'language' => 'sw', 'account_no' => $this -> account_id);
		$this -> db -> trans_start();

		if ($this -> questionaires_model -> save_transation($question)) {

			$qn_id = $this -> db -> insert_id();
			if ($answers) {
				$choices = count($response_text);
				$i = 0;
				foreach ($answers as $key => $value) {

					$sql_choice = array('question_no' => $qn_id, 'survey_no' => $value['survey_no'], 'max_value' => $value['max_value'], 'min_value' => $value['min_value'], 'data_type' => $value['data_type'], 'required' => $value['required'], 'choice_no' => $value['choice_no'], 'response_text' => $response_text[$i], 'next_question' => $value['next_question'], 'match_with' => $value['match_with'], 'account_no' => $this -> account_id);

					$this -> questionaires_model -> save_answer_choices($sql_choice);
					$i++;
				}
			}
		}
		$this -> db -> trans_complete();
		redirect('questionaires/view/' . $question_id);

	}

	public function update_translation() {
		extract($_POST);

		///save the question

		$question = array('question_text' => $question_text);

		//read original question choices

		if ($this -> questionaires_model -> update($translation_id, $question)) {

			$answers = $this -> questionaires_model -> read_answer_options(array('question_no' => $question_id));

			if ($answers) {
				//remove the
				$this -> questionaires_model -> delete_choices($translation_id);
				$i = 0;
				foreach ($answers as $key => $value) {

					$sql_choice = array('question_no' => $translation_id, 'survey_no' => $value['survey_no'], 'max_value' => $value['max_value'], 'min_value' => $value['min_value'], 'data_type' => $value['data_type'], 'required' => $value['required'], 'choice_no' => $value['choice_no'], 'response_text' => $response_text[$i], 'next_question' => $value['next_question'], 'match_with' => $value['match_with'], 'account_no' => $this -> account_id);

					$this -> questionaires_model -> save_answer_choices($sql_choice);
					$i++;
				}
			}
		}

		redirect('questionaires/view/' . $question_id);

	}

}
