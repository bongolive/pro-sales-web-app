                    <div class="modal-header">

									<h4><?php _l('return_device_stock'); ?></h4>

								</div>

                                <form class="form-horizontal" action="<?php echo site_url("stocks/updateReturnedStock"); ?>" method="post">
									 <?php foreach ($stocks as $stock): ?>
                                    <fieldset>
                                        
										<div class="control-group">
                                        	<label class="control-label" for="device_imei"><?php _l('device_imei'); ?></label>
                                        	<div class="controls">
                                        	<select name="phone_imei" id="phone_imei" required >
												<option value="">Select Device</option>
												<?php
												foreach ($devices as $device): ?>
												<option value="<?php echo $device['phone_imei']; ?>" <?php if($device['phone_imei']==$stock['phone_imei']){ echo "selected"; } ?>><?php echo $device['device_name']; ?></option>
												<?php endforeach; ?>
                                                
											</select>
											<input name="userid" type="hidden" id="userid" value="<?php echo $stock['user_id'];?>" />
                                        	</div>
                                    	</div>
										<div class="control-group">
                                        	<label class="control-label" for="product_name"><?php _l('product_name'); ?></label>
                                        	<div class="controls">
                                        	<select name="product_name" id="product_name" required >
												<option value="">Select Product Name</option>
												<?php
												foreach ($products as $product): ?>
												<option value="<?php echo $product['product_name']; ?>" <?php if($product['product_name']==$stock['product_name']){ echo "selected"; } ?>><?php echo $product['product_name']; ?></option>
												<?php endforeach; ?>
                                                
											</select>
                                        	</div>
                                    	</div>
										
										<div class="control-group">
                                        	<label class="control-label" for="items_returned" ><?php _l('items_returned'); ?></label>
                                        	<div class="controls">
                                        	<input name="returned_items" id="returned_items" class="input-xlarge" type="number" value="<?php echo $stock['returned_items'];?>" required />
                                        	</div>
                                    	</div>
										
                                        <div class="control-group">
                                        	<label class="control-label" for="stock_date"><?php _l('stock_date'); ?></label>
                                        	<div class="controls">
                                        	<input name="stock_date" id="stock_date" class="input-xlarge" type="text" value="<?php echo $stock['stock_date'];?>" required />
                                        	</div>
                                    	</div>
                                    	<div class="control-group">
                                        	<label class="control-label" for="status"><?php _l('status'); ?></label>
                                        	<div class="controls">
                                        	<select name="status" id="status" required>
												<option value="pending" <?php if($stock['status']=="pending"){ echo "selected"; } ?>>Pending</option>
												<option value="rejected" <?php if($stock['status']=="rejected"){ echo "selected"; } ?>>Rejected</option>
												<option value="approved" <?php if($stock['status']=="approved"){ echo "selected"; } ?>>Approved</option>
                                        	</select>
                                        	</div>
                                    	</div>          
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
							<?php endforeach; ?>
                        </form>
