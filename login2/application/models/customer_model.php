<?php
class Customer_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	
	public function getCustomer($cond)
	{
		$query = $this->db->get_where('customers', $cond);
		return $query->result_array();
	}
	
	public function add($data)
	{
		return $this->db->insert('customers', $data);
	}
	
	public function update($data,$cond)
	{
		$this->db->where($cond);
		return $this->db->update('customers', $data);
	}
	
	public function delete($cond)
	{
		return $this->db->delete('customers', $cond);
	}

}