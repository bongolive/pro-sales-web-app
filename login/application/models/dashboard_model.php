<?php
class Dashboard_model extends MY_Model {

	public function reservation($where_in = FALSE) {
		$this -> db -> select('count(req_id) as total,req_date');
		$this -> db -> from('requests');
		$this -> db -> where('status', null);
		if ($where_in) {
			$this -> db -> where_in('prop_id', $where_in);
		}
		$this -> db -> group_by('req_date');
		return $this -> db -> get() -> result_array();

	}

	public function pending($where_in = FALSE) {
		$this -> db -> select('count(req_id) as total,req_date');
		$this -> db -> from('requests');
		$this -> db -> where('status', null);
		if ($where_in) {
			$this -> db -> where_in('prop_id', $where_in);
		}
		return $this -> db -> get() -> result_array();

	}

	public function confirmed($where_in = FALSE) {
		$this -> db -> select('count(req_id) as total,req_date');
		$this -> db -> from('requests');
		$this -> db -> where('status', 'confirmed');
		if ($where_in) {
			$this -> db -> where_in('prop_id', $where_in);
		}
		$this -> db -> group_by('req_date');
		return $this -> db -> get() -> result_array();

	}

	public function req_leased($where_in = FALSE) {
		$this -> db -> select('count(req_id) as total,req_date');
		$this -> db -> from('requests');
		$this -> db -> where('status', 'leased');
		if ($where_in) {
			$this -> db -> where_in('prop_id', $where_in);
		}
		$this -> db -> group_by('req_date');
		return $this -> db -> get() -> result_array();

	}

	public function leases_today($where_in = FALSE) {

		$this -> db -> select('count(issue_date) as total,issue_date');
		$this -> db -> from('leasing');
		$this -> db -> like('issue_date', date('Y-m-d'));
		if ($where_in) {
			$this -> db -> where_in('prop_id', $where_in);
		}
		return $this -> db -> get() -> result_array();
	}

	public function units($where_in = FALSE) {
		$this -> db -> select('*');
		$this -> db -> from('prop_units');
		$this -> db -> where('unit_status', 'available');
		$this -> db -> where('unit_uses', 'For Lease');
		if ($where_in) {
			$this -> db -> where_in('prop_id', $where_in);
		}
		$qry = $this -> db -> get();
		return $qry -> num_rows();
	}

	public function checkin($where_in = FALSE) {

		$add_days = 7;
		$date = date('Y-m-d', strtotime(date('Y-m-d')) + (24 * 3600 * $add_days));

		$this -> db -> select('req_checkin');
		$this -> db -> from('requests');
		$this -> db -> where('req_checkin >=', date('Y-m-d'));
		$this -> db -> where('req_checkin <=', $date);
		if ($where_in) {
			$this -> db -> where_in('prop_id', $where_in);
		}
		$qry = $this -> db -> get();
		return $qry -> num_rows();
	}

	public function mauzo($where) {

		$qry = "SELECT 'lease_id',EXTRACT(MONTH FROM pay_date) as mwezi, sum(paid_amt) AS jumla FROM (`bg_payments`) JOIN `bg_leasing` ON `bg_payments`.`lease_id`=`bg_leasing`.`lease_id` JOIN `bg_property` ON `bg_property`.`id`=`bg_leasing`.`prop_id`  WHERE bg_payments.created_by =" . $where . " group by mwezi";

		$query = $this -> db -> query($qry);

		if ($query) {
			return $query -> result_array();
		} else {
			return FALSE;
		}

	}

	public function matumizi($where) {

		$qry = "SELECT EXTRACT(MONTH FROM date_of) as mwezi, sum(cost) AS jumla from bg_expenses where bg_expenses.created_by =" . $where . " group by mwezi";
		$query = $this -> db -> query($qry);
		if ($query) {
			return $query -> result_array();
		} else {
			return FALSE;
		}
	}

	public function sales_count($where_by = FALSE) {
		$this -> db -> select('sum(paid_amt) as sales,pay_date');
		$this -> db -> from('payments');
		if ($where_by) {
			$this -> db -> where_in('created_by', $where_by);
		}
		$this -> db -> group_by('pay_date');

		return $this -> db -> get() -> result_array();
	}

	public function leased($where) {

		$qry = "SELECT unit_status, COUNT(*) AS Total ,  
 			((SELECT COUNT(*) FROM bg_prop_units WHERE unit_status='leased' and created_by = " . $where . ")/ COUNT(*) ) * 100 AS 'leased' FROM bg_prop_units where unit_uses='For Lease' AND bg_prop_units.created_by =" . $where;

		$query = $this -> db -> query($qry);
		if ($query) {
			foreach ($query->result_array() as $row) {
				return $row;
			}
		} else {
			return FALSE;
		}

	}

}
