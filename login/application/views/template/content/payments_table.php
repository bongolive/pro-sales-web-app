				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">
					
					<?php
					$t_balance =  $t_sales = $t_paid=0;
					if($tb_data){
					foreach ($tb_data as $key => $value) {
					 	$t_sales = $t_sales +$value['sales_total'];
						$t_paid = $t_paid +$value['paid_amount'];
						
					}
					}else{
					$t_balance= 0;
					}
					$t_balance = $t_sales - $t_paid;
					?>
					
					 
 
		<div class="span3 data-block widget-block" style="">
				<section style="">
			<span style="font-size:35.6px"> <?php echo money_format('%.2n', $t_sales); ?></span>
				<strong class="widget-label">Today Sales (<?php echo $settings['default_currency'] ?>)</strong>
				</section>
		</div>
 
  		<div class="span3 data-block widget-block" style="">
				<section style="">
				<span style="color:green;font-size:35.6px"> <?php echo money_format('%.2n', $t_paid); ?></span>
				<strong class="widget-label">Total Sales (<?php echo $settings['default_currency'] ?>)</strong>
				</section>
		</div>
		<div class="span3 data-block widget-block" style="">
				<section style="">
			<span style="color:orane;font-size:35.6px">	   <?php echo money_format('%.2n',$t_balance); ?></span>
				<strong class="widget-label">Balance (<?php echo $settings['default_currency'] ?>) </strong>
				</section>
		</div>
		<div class="span3 data-block widget-block " style="">
			<section style="">
			<span style="color:darkred;font-size:35.6px">  <?php echo money_format('%.2n',$t_sales); ?></span>
				<strong class="widget-label">Total (<?php echo $settings['default_currency'] ?>) </strong>
			</section>
		</div>
 
					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('payments_history'); ?></h2> 
							<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('payments/export') ?>" ;"="" style="float: right; padding: 8px;"><img src="<?php echo base_url('template/img/icons/excel.png'); ?>" /> <?php _l('export'); ?></a>
							
            </header> 
         
              <!-- include the filtering paget-->
    
    <?php
		include_once ('filtering.php');
 ?>
         
								<div class="modal-body">
									 
								
								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>

									<tr><th></th>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>
 				  <?php if ($tb_data){
									foreach ($tb_data as $data):  
									  
                                            $id = $data[$table_id];
                                     ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
										<td><input type="checkbox" name="item_no[]" value="<?php echo $data[$table_id]; ?>"/></td>
                                        <?php foreach ($row_fields as $field): ?>
                                        	
										  <td><?php
										if ($field == 'balance') {
											$value = $data['sales_total'] - $data['paid_amount'];
											echo money_format('%.2n', $value);

										} else {
											echo $data[$field];
										}
 ?></td>
                                        <?php endforeach; ?>
										
                                            <td> <?php   if(($data['sales_total'] - $data['paid_amount']) > 0){  ?>
                                                	<a href="<?php echo site_url($pay) . '/' . $id; ?>"  class="btn btn-small btn-primary" title="Payments" ><span class="awe-money"/> <?php _l('pay') ?></a>
                                                	<?php }else{ ?>
                                                		<label class="label label-success"><?php _l('paid') ?></label>
                                                	<?php } ?>
                                                	<a href="<?php echo site_url('orders/view') . '/' . $id; ?>"  class="btn btn-small btn-success" title="View Order" ><span class="awe-eye-open"/><?php _l('view_order') ?></a>
                                                	 
											</td>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
								</tbody>
	</table> 
           
		</article>							
   
                        </div>                        	
                        </div>	
                        </section>
                        		
					 