<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** This file contains the essential functions for each page.
 *  Its is autoloaded
 */
 
 
 function get_css($css){
    
    if(is_array($css)){
        foreach($css as $css_name){
           echo '<link rel="stylesheet" href="'.base_url().'template/css/'.$css_name.'" type="text/css" />'; 
        }
    }elseif(!is_array($css) && $css != null ){
        echo '<link rel="stylesheet" href="'.base_url().'template/css/'.$css.'" type="text/css" />';  
    }
 }
 
 function get_js($js){
    
    if(is_array($js) && $js != null ){
        foreach($js as $js_name){
           if($js_name != ''){
           echo '<script src="'.base_url().'template/js/'.$js_name.'" type="text/javascript" ></script>';
           } 
        }
    }elseif(!is_array($js) && $js != null ){
        echo '<script src="'.base_url().'template/js/'.$js.'" type="text/javascript" ></script>'; 
    }
 }
 
 function get_img($url){
    if($url != null){
        echo base_url().'template/img/'.$url;
    }
 }
 
 
 function formatMobileNumber($mobileNumber)
{
	$countryCode = '255';
	$mobLenght = strlen($mobileNumber);
	if($mobLenght==9){
		$mobNo = $countryCode.$mobileNumber;
	}else if($mobLenght==10){
		$mobNo = $countryCode.substr($mobileNumber, -9);
	}else if($mobLenght==13){
		$mobNo = substr($mobileNumber, 1);
	}else{
		$mobNo = $mobileNumber;
	} 
	return $mobNo;
}
 
 
 function cleanQuery($string)
    {
      if(get_magic_quotes_gpc())  // prevents duplicate backslashes
      {
        $string = stripslashes($string);
      }
      if (phpversion() >= '4.3.0')
      {
        $string = mysql_real_escape_string($string);
      }
      else
      {
        $string = mysql_escape_string($string);
      }
      return $string;
    }
	
	
	function sentTask($headers, $fields)
	{
		$url = 'https://android.googleapis.com/gcm/send';
		
		// Open connection
		$ch = curl_init();

		// Set the url, number of POST vars, POST data
		curl_setopt( $ch, CURLOPT_URL, $url );

		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );

		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0); 
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);

		// Execute post
		$result = curl_exec($ch);
	}
	
	
	function checkoldimei($imei,$boundryalocation_data){
		for($i = 0; $i < count($boundryalocation_data); $i++){
			if($boundryalocation_data[$i] == $imei){
				return true;
			}
		}
		return false;
	}
	
	
	function get_dist_km($lat1, $lng1, $lat2, $lng2 ){
	
	$earthRadius = 3958.75;
	$dLat = deg2rad($lat2-$lat1);
	$dLng = deg2rad($lng2-$lng1);
	$a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * sin($dLng/2) * sin($dLng/2);
	$c = 2 * atan2(sqrt($a), sqrt(1-$a));
	$dist = $earthRadius * $c;
	
	$meterConversion = 1609.34;
	
	return (floatval($dist * $meterConversion) / 1000 );

}

function gen_md5_password($len = 6)
{
    return substr(md5(rand().rand()), 0, $len);
}

function send_email($to, $msg_body){
	
	$line_brake_ch = "\r\n"; // "\r\n" for windows... "\n" for Unix... "\r" for MAC

	$sender_name = "Foot Print Site";
	$sender = "no-reply@pro.co.tz";
	$sender_organization = "Foot Prints";
	
	$message_subject = "Boundary Alert From Foot Print Site.";
		
	$headers = "From: ".$sender_name." <".$sender.">".$line_brake_ch;
	$headers .= "Reply-To: ".$sender_name." <".$sender.">".$line_brake_ch;
	$headers .= "Errors-To: ".$sender_name." <".$sender.">".$line_brake_ch;
	
	$headers .= "Organization: ".$sender_organization."".$line_brake_ch;
	$headers .= "MIME-Version: 1.0".$line_brake_ch;
	$headers .= "Content-type: text/html; charset=iso-8859-1".$line_brake_ch;
	$headers .= "X-Priority: 3".$line_brake_ch;
	$headers .= "X-Mailer: PHP". phpversion() .$line_brake_ch;
	
	if(mail($to, $message_subject, $msg_body, $headers, "-f".$sender)){
		//echo "<br />SENT...";
	}else{
		//echo "<br />FAILED...";
	}
}
/*
function redirect($url, $permanent=false, $statusCode=303) {
    if($_SERVER['HTTP_X_REQUESTED_WITH'] === "XMLHttpRequest"){
        echo "<script>window.top.location.href='$url'</script>";
    }else{
        if(!headers_sent()) {
            header('location: '.$url, $permanent, $statusCode);
        } else {
            echo "<script>location.href='$url'</script>";
        }
        exit(0);
    }
}*/