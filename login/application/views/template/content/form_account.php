				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							
							<h2><?php
							if (!$info) {
								_l('account_details');
							} else {
								echo _l('account_details') . ' : ' . strtoupper($info['contact_person']);
							}
							 ?></h2>
					   
            </header> 

        </article>

    </div>	
    <div class="row">
    	  
<?php
if ($info) {
	echo form_open('accounts/update', array('class' => 'form-horizontal'));
	echo '<input type="hidden" name="account_id" value="' . $info['account_id'] . '"/>
				<input type="hidden" name="user_id" value="' . $info['user_id'] . '"/>';
} else {
	echo form_open('accounts/save', array('class' => 'form-horizontal'));
}
		?>
<article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l('account_details'); ?></h4>
								</div>
								<div class="modal-body">
   
 <div class="control-group">
	<span class="control-label"><?php _l('company_name'); ?></span> <?php echo form_error('company_name'); ?>
<div class="controls"><input type="text" name="company_name" value="<?php
if ($info) { echo $info['company_name'];
} else { echo set_value('company_name');
}
 ?>" size="50" /></div>
</div>     
 <div class="control-group">
	<span class="control-label"><?php _l('contact_person'); ?></span> <?php echo form_error('contact_person'); ?>
<div class="controls"><input type="text" name="contact_person" value="<?php
if ($info) { echo $info['contact_person'];
} else { echo set_value('contact_person');
}
 ?>" size="50" /></div>
</div>
 <!--<div class="control-group">
<span class="control-label"><?php //_l('lastname')?></span><?php //echo form_error('lastname'); ?>
<div class="controls"><input type="text" name="lastname" value="<?php
if ($info) { //echo $info['lastname'];
} else { //echo set_value('lastname');
}
 ?>" size="50" /></div>
</div> -->

 <div class="control-group">
<span class="control-label"><?php _l('mobile'); ?></span><?php echo form_error('mobile'); ?>
<div class="controls"><input type="text" name="mobile" value="<?php
if ($info) { echo $info['mobile'];
} else { echo set_value('mobile');
}
 ?>" size="50"  /></div>
</div>
 <div class="control-group">
<span class="control-label"><?php _l('email'); ?></span><?php echo form_error('email'); ?>
<div class="controls"><input type="text" name="email" value="<?php
if ($info) { echo $info['email'];
} else { echo set_value('email');
}
 ?>" size="50"  /></div>
</div>

<div class="control-group">
<span class="control-label"><?php _l('user_accounts'); ?></span><?php echo form_error('user_accounts'); ?>
<div class="controls"><input type="text" name="user_accounts" min="1" placeholder="1" value="<?php
if ($info) { echo $info['user_accounts'];
} else { echo set_value('user_accounts');
}
 ?>" size="50"  /></div>
</div>


<div class="control-group">
<span class="control-label"><?php _l('acc_type'); ?></span><?php echo form_error('acc_type'); ?>
<div class="controls">

<?php
$acc_type = array('normal' => _ll('acc_type_normal'), 'premium' => 'Premium');

if ($info) {$acc_type_sel = $info['acc_type'];
} else {$acc_type_sel = set_value('acc_type');
}

echo form_dropdown('acc_type', $acc_type, $acc_type_sel);
?>

 </div>
</div>



</article>
<article class="span6 data-block">
	<div class="modal-header">
									<h4><?php _l('logging_details'); ?></h4>
								</div>

<div class="control-group">
<span class="control-label"><?php _l('role'); ?></span><?php echo form_error('role'); ?>
<div class="controls">
<?php
//$role = array('admin' => 'Admin');
$role = array('admin' => 'Admin', 'storekeeper'=>'Store Keeper', 'sales'=>'Sales', 'manager'=>'Manager','agent'=>'Agent');
if ($info) {$role1 = $info['role'];
} else {$role1 = "";
}
echo form_dropdown('role', $role, $role1);
?>
 </div>
</div> 
 
 <div class="control-group">
<span class="control-label"><?php _l('username'); ?></span><?php echo form_error('username'); ?>
<div class="controls"><input type="text" name="username" value="<?php
if ($info) { echo $info['username'];
} else { echo set_value('username');
}
 ?>" size="50"  /></div>
</div>
   
<div class="control-group">
<span class="control-label"><?php _l('password'); ?></span><?php echo form_error('password'); ?>
<div class="controls"><input type="password" name="password" value="" size="50"  /></div>
</div> 
 

 <div class="control-group"> 
<span class="control-label"> <?php _l('conf_pass'); ?></span> <?php echo form_error('conf_pass'); ?>
<div class="controls"><input type="password" name="conf_pass" value="" size="50" /> </div>
 </div>
 </fieldset>
<div class="form-actions"> 

<a href="<?php echo site_url('accounts'); ?>" class="btn btn-large">Cancel</a>
 <button type="submit" class="btn btn-info btn-large"><i class="icon-pencil"></i>Save</button></div>


</fieldset> 
 
</div>
</div>
</article></section> 
 