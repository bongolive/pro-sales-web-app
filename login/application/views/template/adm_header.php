<!DOCTYPE html>

<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->

<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->

<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

	<head>

		<meta charset="utf-8">

		<title>Dashboard | ProMobile</title>

		<meta name="description" content="">

		<meta name="author" content="Bongolive | www.bongolive.co.tz">

		<meta name="robots" content="index, follow">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

		<!-- jQuery FullCalendar Styles -->
        <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.8.23/themes/smoothness/jquery-ui.css" />
        <?php get_js('plugins/jquery.fullcalendar.css');?>

		<!-- jQuery prettyCheckable Styles -->

        <?php get_js('plugins/prettyCheckable.css');?>

		<!-- Styles -->

		<?php get_css('sangoma-blue.css');?>

		<!-- Fav and touch icons -->

		<link rel="shortcut icon" href="favicon.ico">

		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php get_img('img/icons/apple-touch-icon-114-precomposed.png'); ?>">

		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php get_img('img/icons/apple-touch-icon-72-precomposed.png'); ?>">

		<link rel="apple-touch-icon-precomposed" href="<?php get_img('img/icons/apple-touch-icon-57-precomposed.png'); ?>">
        <style>
        
        /* css for timepicker */
		.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
		.ui-timepicker-div dl { text-align: left; }
		.ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
		.ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
		.ui-timepicker-div td { font-size: 90%; }
		.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
		
		.ui-timepicker-rtl{ direction: rtl; }
		.ui-timepicker-rtl dl { text-align: right; }
		.ui-timepicker-rtl dl dd { margin: 0 65px 10px 10px; }
        
        </style>
		

		<!-- JS Libs -->

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.0/jquery-ui.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/libs/jquery.js"><\/script>')</script>


        <?php get_js('jquery-ui-timepicker-addon_2.js');?>
        <?php get_js('libs/modernizr.js');?>

        <?php get_js('libs/selectivizr.js');?>

		

		<script>

			$(document).ready(function(){

				$('.show_date_piker').datetimepicker();
                
				// Tooltips

				$('[title]').tooltip({

					placement: 'top',

					container: 'body'

				});	



				// Tabs

				$('.demoTabs a, .demoTabs2 a').click(function (e) {

					e.preventDefault();

					$(this).tab('show');

					$('.fullcalendar').fullCalendar('render'); // Refresh jQuery FullCalendar for hidden tabs

				})



			});

		</script>

		

	</head>

	<body>

		

		<!-- Full height wrapper -->

		<div id="wrapper">

			<div style="position: absolute;top: 7px;right: 90px;"><a href="<?php echo site_url($this->lang->switch_uri('sw')); ?>" style="text-decoration: none;" ><img src="<?php get_img('flags/sw_tz.png');?>" />&nbsp;Swahili</a> | <a href="<?php echo site_url($this->lang->switch_uri('en')); ?>" style="text-decoration: none;"><img src="<?php get_img('flags/en_us.png');?>" />&nbsp;English</a></div>

			<!-- Main page header -->

			<header id="header" class="container">

				<h1>

					<!-- Main page logo -->

					<a href="#" class="brand" style="width: 270px;" >Pro</a>

					

				</h1>

				

				<!-- User profile -->

				<div class="user-profile">

					<figure>



						<!-- User profile info -->

						<figcaption>

							<strong><a href="#" class=""><?php echo $this->session->userdata('username');?></a></strong>

							<ul>

								<li><a href="<?php echo site_url('adm_profile'); ?>" title="Profile Settings">Profile</a></li>

								<li><a href="<?php echo site_url('admin/logout'); ?>" title="Logout">Logout</a></li>

							</ul>

						</figcaption>

						<!-- /User profile info -->



					</figure>

				</div>

				<!-- /User profile -->



				<!-- Main navigation -->

				<nav class="main-navigation">



					<!-- Responsive navbar button -->

					<div class="navbar">

						<a class="btn btn-alt btn-large btn-primary btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-home"></span> Dashboard</a>

					</div>

					<!-- /Responsive navbar button -->



					<!-- Navigation -->

					<div class="nav-collapse collapse" role="navigation">

						<ul>
                        
                            <li <?php if (isset($tr_active)) echo $tr_active; ?> ><a href="<?php echo site_url('admin'); ?>"><span class="icon-map-marker"></span><?php _l('track'); ?></a></li>
							<li <?php if (isset($dv_active)) echo $dv_active; ?> ><a href="<?php echo site_url('admin/devices'); ?>"><span class="icon-tags"></span><?php _l('device'); ?></a></li>

                            <li <?php if (isset($ts_active)) echo $ts_active; ?> ><a href="<?php echo site_url('admin/tasks'); ?>"><span class="icon-tasks"></span><?php _l('tasks'); ?></a></li>

                            <li <?php if (isset($rp_active)) echo $rp_active; ?> ><a href="<?php echo site_url('admin/reports'); ?>"><span class="icon-file"></span><?php _l('reports'); ?></a></li>

                            <li <?php if (isset($bd_active)) echo $bd_active; ?> ><a href="<?php echo site_url('admin/boundaries'); ?>"><span class="icon-globe"></span><?php _l('boundaries'); ?></a></li>

							<li <?php if (isset($sp_active)) echo $sp_active; ?> ><a href="<?php echo site_url('admin/users'); ?>"><span class="icon-asl"></span><?php _l('users'); ?></a></li>

						</ul>

					</div>

					<!-- /Navigation -->



				</nav>

				<!-- /Main navigation -->



			</header>