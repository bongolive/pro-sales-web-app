<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class
Settings extends CI_Controller {

	public $settings_dsc = array('cust_fullname', 'prop_name', 'unit_name', 'charges', 'balance');
	public $user_id;
	public $account_id;
	public function __construct() {

		parent::__construct();

		$this -> fields['css'] = array('bootstrap.css', 'bootstrap-responsive.css', 'bootstrap-overrides.css', 'ui-lightness/jquery-ui-1.8.21.custom.css', 'DT_bootstrap.css', 'responsive-tables.css', 'slate.css', 'datepicker.css', 'slate-responsive.css');
		$this -> fields['js'] = array('jquery-1.7.2.min.js', 'jquery-ui-1.8.21.custom.min.js', 'jquery.ui.touch-punch.min.js', 'bootstrap.js', 'plugins/datatables/jquery.dataTables.js', 'plugins/datatables/DT_bootstrap.js', 'plugins/responsive-tables/responsive-tables.js', 'Slate.js', 'demos/demo.tables.js');
			if (!$this -> session -> userdata('logged_in') == TRUE) {
		 $this -> session -> set_userdata('login_error', 'Please, Loggin first');
		 redirect('login');
		 exit;
		 }
		$this -> load -> model('settings_model');
		$this -> load -> model('permissions_model');

		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');

		$this -> fields['row_fields'] = $this -> settings_dsc;
		$this -> fields['tb_headers'] = $this -> settings_dsc;
		$this -> fields['tb_name'] = 'all_settings';
		$this -> fields['controller'] = 'settings';
		$this -> fields['view'] = 'settings/view/';
		$this -> fields['edit'] = 'settings/update/';
		$this -> fields['delete'] = 'settings/delete/';

		$this -> fields['tb_name'] = 'all_settings';
		$this -> fields['controller'] = 'settings';
		$this -> user_id =$this -> session -> userdata('user_id');
		$this -> account_id = $this -> session -> userdata('account_id');
	}

	public function index() {

		//get currencies

		  //$currencies = json_decode(file_get_contents('http://www.localeplanet.com/api/auto/currencymap.json'),true);
		// foreach ($currencies as $currencies) {	}
		 //echo "<pre>";print_r($currencies);
		//fetch available settings
		$where = array('account_id' => $this -> account_id);

		$info = $this -> settings_model -> read($where);
		if ($info) {
			$this -> fields['info'] = $info;
		} else {
			$this -> fields['info'] = FALSE;
		}
		$this -> fields['profile'] = $this -> settings_model -> read_profile($where);

		//settings
		$settings = $this -> settings_model -> read_settings($where);
		if ($settings) {
			foreach ($settings as $settings) {
				$this -> fields['settings'] = $settings;
			}
		} else {$this -> fields['settings'] = FALSE;
		}
		//$this -> fields['currency'] = $currencies;
		//$this -> settings_model -> read_currency();
		$this -> load -> view('template/header', $this -> fields);
		$this -> load -> view('template/content/settings_form');
		$this -> load -> view('template/footer');

	}

	public function update() {

		extract($_POST);

		if (isset($price_masking)) { $masking = 1;
		} else { $masking = 0;
		}
		if (isset($price_discounts)) { $discount = 1;
		} else { $discount = 0;
		}
		if (isset($mult_currency)) { $mult_currency = 1;
		} else { $mult_currency = 0;
		}
		if (isset($deduct_withholding)) { $deduct_withholding = 1;
		} else { $deduct_withholding = 0;
		}
		if (isset($send_reminder)) { $send_reminder = 1;
		} else { $send_reminder = 0;
		}

		if (isset($enable_tax_exclussion)) { $enable_tax_exclussion = 1;
		} else { $enable_tax_exclussion = 0;
		}

		//		$settings = array('vat' => $vat, /*'with_hold' => $with_hold, 'stamp_duty' => $stamp_duty, 'price_masking' => $masking*/, 'enable_tax_exclussion' => $enable_tax_exclussion, 'price_discounts' => $discount, 'currency' => $currency, 'mult_currency' => $mult_currency, 'send_reminder' => $send_reminder, 'deduct_withholding' => $deduct_withholding);

		$settings = array();

		foreach ($this->input->post() as $key => $value) {
			if ($value == 'on') {
				$settings[$key] = 1;
			} else {
				$settings[$key] = $value;
			}

		}
 	
//print_r($this->session->all_userdata());
	//	print_r($settings);

		//updating
		 	$this -> settings_model -> update($this->account_id, $settings);
		 $this -> session -> set_userdata('saved', TRUE);

		 redirect('settings');

	}

	public function update_profile() {

		extract($_POST);

		$settings = array('company_name' => $company_name, 'mobile' => $mobile, 'email' => $email, 'area' => $area, 'vat_no' => $vat_no, 'tin_no' => $tin_no, 'city' => $city, 'street' => $street);

		//updating
		$this -> settings_model -> update_profile($this -> account_id, $settings);
		$this -> session -> set_userdata('saved', TRUE);

		redirect('settings');

	}

	/*
	 *  payments modes start
	 */
	public function payment_mode() {

		$this -> fields['table_id'] = 'mode_id';
		$this -> fields['edit'] = 'settings/payment_mode/update';
		$this -> fields['delete'] = 'settings/c_status';

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array( 'mode_name', 'mode_symbol', 'mode_payment_no', 'status');

		$where = array('account_id' => $this -> account_id);

		$this -> fields['tb_data'] = $this -> settings_model -> get_payment_mode($where);

		$this -> fields['tb_name'] = 'payment_mode';

		$header['stc_active'] = 'class="active"';

		if ($this -> uri -> segment(4) == 'update') {
			//updating mode
			if ($this -> uri -> segment(5)) {
				$where = array('mode_id' => $this -> uri -> segment(5));
				$mode = $this -> settings_model -> get_payment_mode($where);
				if ($mode) {
					foreach ($mode as $mode) {
						$this -> fields['info'] = $mode;
					}

				}
			}

		} elseif ($this -> uri -> segment(4) == 'delete') {
			$id = $this -> uri -> segment(5);
			$data = array('mode_status' => '0');
			$this -> settings_model -> update_mode($id, $data);
			$this -> fields['info'] = FALSE;
		} else {

			$this -> fields['add_btn'] = 'add_new_stock';
			$this -> fields['info'] = FALSE;
		}

		$this -> load -> view('template/header');
		$this -> load -> view('template/content/payment_mode', $this -> fields);
		$this -> load -> view('template/footer');

	}

	public function add_mode() {

		if ($this -> input -> post()) {
			extract($_POST);

            $itemid = $mode_symbol.'.'.$this->account_id;
            $checkid = $this->settings_model -> check_mode($itemid);
            if(!$checkid){
                $data = array('mode_name' => ucfirst($mode_name),
                    'item_id' => $itemid,
                    'mode_symbol' => $mode_symbol, 'account_id' => $this -> account_id,  'mode_payment_no'=>$mode_payment_no, 'created_on' => date('Y-m-d h:i:s'));
                $this -> settings_model -> add_mode($data);
            }

			
		}else{
			
		}
redirect('settings/payment_mode');
	}

	public function update_mode() {

		if ($this -> input -> post('mode_id')) {
			extract($_POST);
            $itemid = $mode_symbol.'.'.$this->account_id;
            $checkid = $this->settings_model -> check_mode($itemid);
            if(!$checkid) {
                $data = array('mode_name' => ucfirst($mode_name), 'mode_symbol' => $mode_symbol,
                    'mode_payment_no' => $mode_payment_no, 'item_id' => $itemid);
                $this->settings_model->update_mode($mode_id, $data);
            }
		}
		redirect('settings/payment_mode');

	}
	
	public function c_status() {
		$id = $this -> uri -> segment(5);
		$act = $this -> uri -> segment(4);
		if ($act == 'add') {
			$data = array('status' => 1);
		} elseif ($act == 'remove') {
			$data = array('status' => 0);
		}
		 $this -> settings_model -> update_mode($id, $data);
			redirect('settings/payment_mode');
		 
	}
	

	/*
	 *  payments modes ends
	 *
	 */

}
