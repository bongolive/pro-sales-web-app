
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_warehouse'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form'); 
        echo form_open_multipart("system_settings/edit_warehousemap/".$id, $attrib); ?>  
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group">
                <label class="control-label" for="code"><?php echo $this->lang->line("user"); ?> *</label>
               <!-- <div class="controls"> <?php echo form_input('code', '', 'class="form-control" id="code" required="required"'); ?> </div>-->
                  <div class="controls"> <?php
                    //var_dump($warehousemap->user_id);

                    $us[""] = $this->lang->line("select") . " " . $this->lang->line("user");
                   
                    foreach ($users as $user) {
                       // var_dump($user);

                        $us[$user->uid] = $user->first_name;

                    }
                    //$user_sel = $user[$];
                    // var_dump($us);
                    echo form_dropdown('users', $us, (isset($_POST['users']) ? $_POST['users'] : $warehousemap->user_id), 'class="form-control select" id="users" required="required"');
                    ?> 
                    </div>

            </div>
            <div class="form-group">
                <label class="control-label" for="name"><?php echo $this->lang->line("warehouse"); ?> *</label>
                <!--<div class="controls"> <?php echo form_input('name', '', 'class="form-control" id="name" required="required"'); ?> </div>-->
                   <div class="controls"> <?php
                    $ct[""] = $this->lang->line("select") . " " . $this->lang->line("warehouse");
                    foreach ($warehouses as $warehouse) {
                        $ct[$warehouse->id] = $warehouse->name;
                    }
                    echo form_dropdown('warehouses', $ct, (isset($_POST['warehouses']) ? $_POST['warehouses'] : $warehousemap->warehouse_id), 'class="form-control select" id="warehouses" required="required"');
                    ?> 
                    </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_warehousemap', lang('add_warehousemap'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>  
</div>
<script type="text/javascript" src="<?=$assets?>js/custom.js"></script>
<?=$modal_js?>
