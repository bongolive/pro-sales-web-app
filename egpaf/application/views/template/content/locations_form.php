<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php
								if (!$info) {
									echo lang('new_location');
								} else {
									echo lang('update_location') . ' : ' . strtoupper($info['location_name']);
								}
							 ?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                  	
    <div class="row">
    	 <div class="col-md-12">  
									<?php
 									
									if ($info) {
										$action = 'locations/update';
									} else {
										$action = 'locations/create';
									};
								?>
								
                                	  <form role="form" method="post" action="<?php echo site_url($action); ?>">
                                	  	<div class="box-body">
							 
                                	<?php if($info){ ?>
                                		<input name="location_id" id="location_id" class="form-control" type="hidden"  value="<?php  echo $info['location_id']; ?>" />
                                		<?php } ?> 
                                    <fieldset  class="span12 data-block row-fluid">
                                    	<div class="span6"> 
                                        <div class="form-group">
                                        	<label class="control-label" for="location_code" ><?php echo lang('location_code'); ?></label>
                                        	<?php echo form_error('location_code'); ?>
                                        	<input name="location_code" id="location_code" class="form-control" type="text"  value="<?php
												if ($info) { echo $info['location_code'];
												} else { $value = set_value('location_code');
												}
 ?>" required />
                                    	</div>
                                        
                                        <div class="form-group">
                                        	<label class="control-label" for="location_name" ><?php echo lang('location_name'); ?></label>
                                        	<?php echo form_error('location_name'); ?>
                                        	<input name="location_name" id="location_name" class="form-control" type="text"  value="<?php
												if ($info) { echo $info['location_name'];
												} else { $value = set_value();
												}
 ?>" required />
                                    	</div>
                                    	
                                    	 
								 
                                    	 <div class="form-group">
                                        	<label class="control-label" for="location_type" ><?php echo lang('location_type'); ?></label>
                                        	<?php echo form_error('location_type'); ?>
                                        	 <?php
												if ($info) { $type =  $info['location_type'];
												} else { $type = set_value('location_type');
												}
												$f_types = array('Dispensary'=>'Dispensary', 'Health Center'=>'Health Center','Hospital'=>'Hospital','Test Location'=>'Test Location');
												$style= 'class="form-control"';
												echo form_dropdown('location_type',$f_types,$type,$style);
												?>
                                    	</div>
                                    	 
                                    	 </div>
                                     <div  class="span6"> 
                                    	 	<div class="form-group">
                                        	<label class="control-label" for="contact_person" ><?php echo lang('contact_person'); ?></label>
                                        	<?php echo form_error('contact_person'); ?>
                                        	<input name="contact_person" id="description" class="form-control" type="text" value="<?php
												if ($info) { echo $info['contact_person'];
												} else { $value = set_value();
												}
 ?>"  required>
                                    	</div>
                                    	 
                                    	<div class="form-group">
                                        	<label class="control-label" for="location_mobile" ><?php echo lang('location_mobile'); ?></label>
                                        	<?php echo form_error('location_mobile'); ?>
                                        	<input name="location_mobile" id="description" class="form-control" type="number" value="<?php
												if ($info) { echo $info['location_mobile'];
												} else { $value = set_value();
												}
 ?>"  required>
                                    	</div>
                                    	
                                    	<div class="form-group">
                                        	<label class="control-label" for="location_email" ><?php echo lang('location_email'); ?></label>
                                        	<?php echo form_error('location_email'); ?>
                                        	<input name="location_email" id="description" class="form-control" type="location_email" value="<?php
												if ($info) { echo $info['location_email'];
												} else { $value = set_value();
												}
 ?>">
                                    	</div>
                                    	<div class="form-group">
                                        	<label class="control-label" for="location_area" ><?php echo lang('location_area'); ?></label>
                                        	<?php echo form_error('location_area'); ?>
                                        	<input name="location_area" id="description" class="form-control" type="text" value="<?php
												if ($info) { echo $info['location_area'];
												} else { $value = set_value();
												}
 ?>"  required >
                                    	</div>
                                    	
                                    	<div class="form-group">
                                        	<label class="control-label" for="location_street" ><?php echo lang('location_street'); ?></label>
                                        	<?php echo form_error('location_street'); ?>
                                        	<input name="location_street" id="description" class="form-control" type="text" value="<?php
												if ($info) { echo $info['location_street'];
												} else { $value = set_value();
												}
 ?>"  required>
                                    	</div>
                                    	
                                    	<div class="form-group">
                                        	<label class="control-label" for="" ><?php echo lang('location_city'); ?></label>
                                        	<?php echo form_error('location_city'); ?>
                                        	<input name="location_city" id="description" class="form-control" type="text" value="<?php
												if ($info) { echo $info['location_city'];
												} else { $value = set_value();
												}
 ?>"  required>
                                    	</div>
                                    	
                                     </div>
								</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('locations'); ?>" type="button" class="btn btn-default" data-dismiss="modal">Back</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>
                        </div>                        	
                        </div>	
                        </section>
                        		
					 
