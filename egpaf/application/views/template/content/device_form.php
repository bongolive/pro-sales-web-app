				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							
							<h2><?php 
							if(!$info){
							_l('new_devices');
							}else{
								echo _l('update_device').' : '. strtoupper($info['device_imei']);
							}
							 ?></h2>
					   
            </header> 

        </article>

    </div>	
    <div class="row">
    	 
    	 <article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l('add_new_device'); ?></h4>
								</div>
								<div class="modal-body">
									<?php if($info){
											$action = 'devices/update_device';
										}else{
										$action = 'devices/new_devices';		
											};?>
											
					 			 	 
                                <form class="form-horizontal" method="post" action="<?php echo site_url($action); ?>">
                                	<?php if($info){ ?>
                                		<input name="device_id" id="device_id" class="input-large" type="hidden"  value="<?php  echo $info['device_id'];?>" />
                                		<?php } ?> 
                                    <fieldset>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="device_imei" ><?php _l('device_imei'); ?></label>
                                        	<div class="controls"><?php echo form_error('device_imei'); ?>
                                        	<input name="device_imei" id="device_imei" class="input-large" type="text"  value="<?php if($info){ echo $info['device_imei']; }else{ $value=set_value(); } ?>" required />
                                        	</div>
                                    	</div>
									 
                                    	
                                    	 <div class="control-group">
                                        	<label class="control-label" for="device_descr" ><?php _l('device_descr'); ?></label>
                                        	<div class="controls"><?php echo form_error('device_descr'); ?>
                                        	<textarea name="device_descr" id="device_descr"   type="text"><?php if($info){ echo $info['description']; }else{ $value=set_value(); } ?></textarea>
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="assigned_to"><?php echo lang('device_user'); ?></label>
                                        	<div class="controls">
                                        		
                                        		<?php
												if ($employees == FALSE) {

													$employees = array(''=>'No Users Available');
												}
												 
												if($info){$value =$info['assigned_to'];}else{ $value=set_value(); }
												echo form_dropdown('assigned_to',$employees,$value);
                                        		?>
                                        		<?php echo form_error('assigned_to'); ?> 
                                        	</div>
                                    	</div>   
                                    	 
                                    		<div class="control-group">
                                        	<label class="control-label" for="device_mobile" ><?php _l('device_mobile'); ?></label>
                                        	<div class="controls">
                                        		<?php echo form_error('device_mobile'); ?>
                                        	<input name="device_mobile" id="description" class="input-large" type="number" value="<?php if($info){ echo $info['device_mobile']; }else{ $value=set_value(); } ?>">
                                        	</div>
                                    	</div>
                                    		<!--
                                    			<div class="control-group">
                                        	<label class="control-label" for="assigned_on" ><?php _l('assigned_on'); ?></label>
                                        	<div class="controls"><?php echo form_error('assigned_on'); ?>
                                        	<input name="assigned_on" id="description"  class="input-large" type="date" value="<?php if($info){ echo $info['assigned_on']; }else{ $value=set_value(); }?>" required>
                                        	</div>
                                    	</div>-->
                                    	   
								</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('devices'); ?>" class="btn btn-alt" data-dismiss="modal">Back</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>
                        </div>                        	
                        </div>	
                        </section>
                        		
					 