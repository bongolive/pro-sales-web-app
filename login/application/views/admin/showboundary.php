
<script type="text/javascript">

        var center_lat = "<?php echo $lat; ?>";
        var center_lng = "<?php echo $long; ?>";
        var radius = <?php echo $rad; ?>;
        
        var map;
        
        function showBoundry() {
        	var centerPoint = new google.maps.LatLng(center_lat, center_lng);
        
        	var myOptions = {
        		zoom: 13,
        		center: centerPoint,
        		mapTypeId: google.maps.MapTypeId.ROADMAP
        	}
        
        	map = new google.maps.Map(document.getElementById("mapCanvasBoundary"), myOptions);
        	
        	var circleOptions = {
        	  center: new google.maps.LatLng(center_lat, center_lng),
        	  radius: radius,
        	  map: map,
        	  editable: false
        	};
        	var circle = new google.maps.Circle(circleOptions);
        	
        }
        
        
          $(document).ready(function(){
        	  
        		showBoundry();
        		
          });
</script>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: -3%;left: 99%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
    <div id="mapCanvasBoundary" style="width:100%; height:500px;"></div>
