<?php



/**

 * This file carries the main values for language in Swahili

 */



$lang['device_pg_title'] = 'Kifaa';

$lang['track_pg_title'] = 'Fwatilia'; 

$lang['task_pg_title'] = 'Tasks';

$lang['reports_pg_title'] = 'Ripoti';

$lang['customers_pg_title'] = 'Wateja';

$lang['profile_pg_title'] = 'Profile';

$lang['boundary_pg_title'] = 'Mipaka';

$lang['support_pg_title'] = 'Msaada';

$lang['login_pg_title'] = 'Login';

$lang['welcome_pg_title'] = 'Karibu';



// Table fields



$lang['device_tb_name'] = 'Vifaa';

$lang['boundaries_tb_name'] = 'Mipaka';

$lang['reports_tb_name'] = 'Ripoti';

$lang['customers_tb_name'] = 'Wateja';

$lang['tasks_tb_name'] = 'Tasks';

$lang['track_tb_name'] = 'Fwatilia';

$lang['user_tb_name'] = 'Watumiaji';

$lang['permission_tb_name'] = 'Ruhusa';



// Table Field names



$lang['device_name'] = 'Jina la Kifaa';

$lang['subject'] = 'Subject';

$lang['task'] = 'Task';

$lang['status'] = 'Hali';

$lang['date'] = 'Tarehe';

$lang['action'] = 'Kitendo';

$lang['device_imei'] = 'IMEI ya Kifaa';

$lang['track_intv'] = 'Mda wa kufwatilia';

$lang['mobile_nmber'] = 'Namba ya Simu';

$lang['holder_name'] = 'Jina la Mmiliki';

$lang['alert_name'] = 'Jina la Kushtua';

$lang['devices'] = 'Vifaa';

$lang['valid_from'] = 'Hai Kuanzia';

$lang['valid_to'] = 'Hai Mpaka';

$lang['boundary'] = 'Mpaka';

$lang['report'] = 'Ripoti';

$lang['comments'] = 'Maoni';

$lang['location'] = 'Sehemu';

$lang['customer_name'] = 'Jina la Mteja';

$lang['mobile_number'] = 'Namba ya Simu';

$lang['email'] = 'Barua pepe';

$lang['address'] = 'Anuani';

$lang['valid_till'] = 'Halali mpaka';

$lang['user_type'] = 'Aina ya mtumiaji';

$lang['permission_name'] = 'Jina La Ruhusa';

$lang['is_deleted'] = 'Imefutwa';


// Tabs

$lang['device'] = 'Kifaa';

$lang['support'] = 'Msaada';

$lang['track'] = 'Fuatilia';

$lang['reports'] = 'Ripoti';

$lang['customers'] = 'Wateja';

$lang['boundaries'] = 'Mipaka';

$lang['tasks'] = 'Majukumu';

$lang['users'] = 'Wateja';

$lang['permissions'] = 'Ruhusa';

$lang['update_client_permission'] = 'Rekebisha Ruhusa Kwa Mteja';

$lang['client_permissions'] = 'Ruhusa za Mteja';

// add Device

$lang['email_reports'] = 'Ripoti kwa Barua pepe';

$lang['enable_email_reports'] = 'Wezesha Ripoti kwa Barua pepe';

$lang['add_new_device'] = 'Kifaa Kipya';



// Add Task



$lang['add_task'] = 'Ongeza Jukumu';

$lang['new_customer'] = 'Ongeza Mteja';

$lang['add_new_permission'] = 'Ongeza Ruhusa';

$lang['add_new_user'] = 'Mtumiaji Mpya';

$lang['new'] = 'Mpya';

$lang['in_progress'] = 'Inaendelea';

$lang['done'] = 'Imekamilika';



// Add Boundary

$lang['add_boundary'] = 'Ongeza Mipaka';

$lang['edit_boundary'] = 'Badili Mipaka';

$lang['step_one'] = 'Hatua ya Kwanza (Taarifa)';



// Edit Device

$lang['edit_device'] = 'Badili taarifa za Kifaa';

$lang['update_customer'] = 'Rekebisha Taarifa za Mteja';

$lang['update_user'] = 'Rekebisha Taarifa za Mtumiaji';

$lang['update_permission'] = 'Rekebisha Ruhusa';



// Table reveal modal

$lang['confirm_deletion'] = 'Hakiki Ufutaji wa Taarifa';

$lang['are_you_sure'] = 'Je una uhakika';



//Tracking 



$lang['start_date'] = 'Kuanzia Tarehe';

$lang['end_date'] = 'Hadi Tarehe';

$lang['search'] = 'Tafuta';



//Downloading form



$lang['txt_task'] = 'Tasks';

$lang['all_dates'] = 'Tarehe Zote';

$lang['all_task'] = 'Tasks zote';

$lang['all_report'] = 'Ripoti zote';

$lang['all_subject'] = 'Vichwa vya habari vyote';

$lang['download_task'] = 'Pakua Tasks';

$lang['download_report'] = 'Pakua Ripoti';



$lang['stocks_tb_name'] = 'Hifadi ya Bidhaa';
$lang['product_name'] = 'Jina la Bidhaa';
$lang['items_received'] = 'Bidhaa Zilizopokelewa';
$lang['items_issued'] = 'Bidhaa Zilizouzwa';
$lang['items_returned'] = 'Bidhaa Zilizorudishwa';
$lang['stock_date'] = 'Tarehe Ya Hifadhi';
$lang['add_new_stock'] = 'Ongeza Hifadhi';
$lang['products_tb_name'] = 'Bidhaa';
$lang['description'] = 'Maelezo';
$lang['items_on_hand'] = 'Bidhaa zilizopo';
$lang['add_new_product'] = 'Ongeza Bidhaa Mpya';
$lang['sales_tb_name'] = 'Mauzo ya Bidhaa';
$lang['client_name'] = 'Jina la Mteja';
$lang['sales_datetime'] = 'Tarehe ya Mauzo';
$lang['quantity'] = 'Idadi';
$lang['price'] = 'Kiasi';
$lang['clients_tb_name'] = 'Wateja';
$lang['phone'] = 'Namba ya simu';
$lang['shop_name'] = 'Jina la Duka';
$lang['add_new_client'] = 'Ongeza Mteja Mpya';
$lang['returned_stocks_tb_name'] = 'Bidhaa Zilizorudishwa';



$lang['update_product'] = 'Rekebisha Taariha za Bidhaa';
$lang['update_device_stock'] = 'Rekebisha Stoku katika Simu';
$lang['return_device_stock'] = 'Rudisha Stoku toka katika Simu';
$lang['update_client'] = 'Rekebisha Taarifa za mteja';
