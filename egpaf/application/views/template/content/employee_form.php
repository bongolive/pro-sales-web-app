

<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                  	
    <div class="row">
    	 <div class="col-md-12">  
									<?php
									if ($info) {
										$action = 'employees/update';

									} else {
										$action = 'employees/create';
									}
		?>
								
                                	  <form role="form" method="post" action="<?php echo site_url($action); ?>">
                                	  	<div class="box-body">
							 <div class="row">
    	 <div class="col-md-6">
    	 	<div class="box box-info">
    	 	<div class="box-header">
<h3 class="box-title"><?php echo lang('employee_details') ?></h3>
</div> 
                                	<?php if($info){ ?>
                                		<input type="hidden" name="employee_id" value="<?php echo $info['emp_id']; ?>"/>
                                		<?php } ?> 
<fieldset  class="span12 data-block row-fluid">
  
 <div class="form-group">
	<span class="control-label"><?php echo lang('firstname'); ?></span> <?php echo form_error('firstname'); ?>
<input type="text" name="firstname" value="<?php
if ($info) { echo $info['firstname'];
} else {echo set_value('firstname');
}
 ?>" class="form-control" />
</div>
<div class="form-group">
<span class="control-label"><?php echo lang('middle_name')?></span><?php echo form_error('middle_name'); ?>
<input type="text" name="middle_name" value="<?php
if ($info) { echo $info['middle_name'];
} else {  echo set_value('middle_name');
}
  ?>" class="form-control" />
</div>
 <div class="form-group">
<span class="control-label"><?php echo lang('lastname')?></span><?php echo form_error('lastname'); ?>
<input type="text" name="lastname" value="<?php
if ($info) { echo $info['lastname'];
} else {  echo set_value('lastname');
}
  ?>" class="form-control" />
</div>
<div class="form-group">
<span class="control-label"><?php echo lang('staff_no')?></span><?php echo form_error('staff_no'); ?>
<input type="text" name="staff_no" value="<?php
if ($info) { echo $info['staff_no'];
} else {  echo set_value('staff_no');
}
  ?>" class="form-control" />
</div>
 <div class="form-group">
<span class="control-label"> <?php echo lang('gender'); ?></span><?php echo form_error('gender'); ?>

<?php
$options = array('male' => 'Male', 'female' => 'Female', );
if ($info) {$sex = $info['gender'];
} else {$sex = "";
}
$style = ' class="form-control"';
echo form_dropdown('gender', $options, $sex, $style);
?> 

</div>
 <div class="form-group">
<span class="control-label"><?php echo lang('dob'); ?></span><?php echo form_error('dob'); ?>
<input type="datetime" name="dob" value="<?php
if ($info) { echo $info['dob'];
} else { echo set_value('dob');
}
  ?>" class="form-control"  />
</div> 
 
 <div class="form-group">
<span class="control-label"><?php echo lang('location'); ?></span><?php echo form_error('location'); ?>
<?php
$style = ' class="form-control"';
if ($info) {$locate = $info['location'];
} else {$locate = set_value('location');
}
echo form_dropdown('location', $locations, $locate, $style);
?>

</div> 
 <div class="form-group">
<span class="control-label"><?php echo lang('date_of_emp'); ?></span><?php echo form_error('date_of_emp'); ?>
<input type="datetime" name="date_of_emp" value="<?php
if ($info) { echo $info['date_of_emp'];
} else { echo set_value('date_of_emp');
}
  ?>" class="form-control"  />
</div> 

 <div class="form-group">
<span class="control-label"><?php echo lang('mobile'); ?></span><?php echo form_error('mobile'); ?>
<input type="text" name="mobile" value="<?php
if ($info) { echo $info['mobile'];
} else { echo set_value('mobile');
}
?>"  class="form-control"size="50"  />
</div> 

 <div class="form-group">
<span class="control-label"> <?php echo lang('email'); ?></span> <?php echo form_error('email'); ?>
<input type="text" name="email" value="<?php
if ($info) { echo $info['email'];
} else { echo set_value('email');
}
 ?>" class="form-control" />
</div>
  </div></div>
  <div class="col-md-6"> 
  <div class="box box-success">  
		<div class="box-header">
<h3 class="box-title"><?php echo lang('user_details') ?></h3>
</div> 					 
 <div class="form-group">
 	<!-- if there is user details -->
 <?php	
 if ($info) {  ?>
 <input type="hidden" name="user_id" value="<?php echo $info['user_id']; ?>" />
<?php

}
 ?>
	<span class="control-label"><?php echo lang('username'); ?></span> <?php echo form_error('username'); ?>
<input type="text" name="username" value="<?php
if ($info) { echo $info['username'];
} else {echo set_value('username');
}
 ?>" class="form-control" />
</div>

 <div class="form-group">
<span class="control-label"><?php echo lang('user_device'); ?></span><?php echo form_error('device_imei'); ?>

<?php

if ($info) {$dev = $info['device'];
} else {$dev = set_value('device_imei');
}
$style = ' class="form-control"';
echo form_dropdown('device_imei', $devices, $dev, $style);
?>
 
</div>

 <div class="form-group">
<span class="control-label"><?php echo lang('role'); ?></span><?php echo form_error('role'); ?>

<?php
$roles = array('0' => 'User', '1' => 'Administrator', );
if ($info) {$role = $info['role'];
} else {$role = set_value('role');
}
$style = ' class="form-control"';
echo form_dropdown('role', $roles, $role, $style);
?>
 
</div>
 <div class="form-group">
<span class="control-label"><?php echo lang('allow_web_access'); ?></span><?php echo form_error('allow_web_access'); ?>

<?php
$webac = array('0' => 'No', '1' => 'Yes');
if ($info) { $web = $info['allow_web_access'];
} else {$web = set_value('role');
}
$style = ' class="form-control"';
echo form_dropdown('allow_web_access', $webac, $web, $style);
?>
 
</div>
 
 <div class="form-group">
<span class="control-label"><?php echo lang('password')?></span><?php echo form_error('password'); ?>
<input type="password" name="password" min="6" value="" class="form-control" /></div>
  
 <div class="form-group">
<span class="control-label"><?php echo lang('conf_password'); ?></span><?php echo form_error('conf_password'); ?>
<input type="password"  name="conf_password" min="6" value="" class="form-control"  /></div>

 <div class="form-group">
<span class="control-label"><?php echo lang('permissions'); ?></span><?php echo form_error('permissions'); ?>
 
	</div>
 <div class="form-group">
 	 
		<?php
		if($modules){
			if ($info) {
					$perms = explode(',', $info['permissions']);
			 
				}  
			
				foreach($modules as $key => $value){ 
					 
					?>
	<div class="checkbox" style="padding-left: 40px" >
<label>
 <input type="checkbox" name="permissions[]" value="<?php echo $value['module_id']; ?>" <?php
if($info){	if (in_array($value['module_id'], $perms)) { echo 'checked';
	}}
  ?>  )  /> <?php echo lang($value['module_name']); ?>  
 
</label>
</div>				
					
					
			<?php }

						}
		?>
	  
</div> 
  
<div class="form-actions"> 

<a href="<?php echo site_url('employees'); ?>" type="button" class="btn btn-default">Cancel</a>
 <button type="submit" class="btn btn-info btn-large"><i class="icon-pencil"></i>Save</button></div>

								</div>
                            </fieldset>
                        </form>
                        </article>
                        </div>                        	
                        </div>	
                        </section>
                        		
			
