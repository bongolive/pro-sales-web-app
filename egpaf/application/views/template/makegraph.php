
<script type="text/javascript">
//the graphs
	var area = new Morris.Area({
	element: 'revenue-chart',
	resize: true,parseTime: false,
	data: [<?php if($graph_data){ foreach ($graph_data as $key => $value) {	?>
			{x: '<?php echo $value['interview_date']; ?>',  y:<?php echo $value['total']; ?>
		},	<?php  } }else{ ?>	{x: '0',   y: 0},<?php } ?>	], xkey: 'x',	ykeys: [ 'y'],
		labels: ['Total'],
		lineColors: [  '#3c8dbc'],
		hideHover: 'auto',
		});
		
</script>