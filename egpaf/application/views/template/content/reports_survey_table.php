<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
 
                            <div class="box">
                                <div class="box-header">
                                                                       <h4 class="page-header box-title">
<?php echo lang('surveys_report') ?>
<small> View Report per Survey, select a Survey from the list </small>
</h4> 
                                 <!--    <a href="<?php echo site_url('reports/printX'); ?>" style="margin:10px; float: right"  type="button" class="btn btn-large btn-info" ><i class="fa fa-printer"></i> <?php echo lang('print'); ?> </a>-->
                                    <!--<a href="<?php echo site_url('reports'); ?>" style="margin:10px; float: right"  type="button" class="btn btn-large btn-primary" > <?php echo lang('back'); ?> </a>-->                                    
                                </div><!-- /.box-header --> 
                                <div class="box-body table-responsive">
                                    <table id="type1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr><th>#</th>
                                            	  <?php foreach($tb_headers as $header){?>

										<th><?php echo lang($header); ?></th>

                                        <?php } ?>
                                            	 <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
   <?php if ($tb_data){
   	 
									foreach ($tb_data as $data):   
                                            $id = $data['survey_id'];
                                    ?>
									<tr>
											<td><input id="optionsCheckbox" type="checkbox" value="option1"></td>
                                           <?php foreach ($row_fields as $field): ?>
										  <td>
										  	<?php
											if ($field == 'status') {
												if ($data[$field] == 1) {
													echo '<span class="label label-success">Active</span>';
												} else {

													echo '<span class="label label-warning">In Active</span>';
												}

											} elseif ($field == 'total_response') {
												if($summary){ 
												foreach ($summary as $responses) {
													if ($responses['survey_no'] == $data['survey_id']) {
														echo $responses['total'];
													}
												}}else{echo 0;}
											} else {
												echo ucwords($data[$field]);
											}
												 	?></td>
                                        <?php endforeach; ?>
										
                                            <td>
                                               <div class="btn-group">
                                               
                                               	 		<a href="<?php echo site_url('reports/survey_report') . '/' . $data['survey_id']; ?>"  class="btn btn-success" type="button"title="View" ><i class="fa fa-eye"></i> <?php echo lang('view') ?></a>
                                                          <a class="btn btn-large btn-info" type="button"   href="<?php echo site_url('reports/printing/' . $data['survey_id']); ?>"><i class="fa fa-file-o"></i> <?php echo lang('print') ?></a>
<a class="btn btn-large btn-danger" type="button"   href="<?php echo site_url('reports/raw_data_export/' . $data['survey_id']); ?>"><i class="fa fa-file-o"></i> <?php echo lang('export_raw') ?></a>

												 </div> 						 
											</td>
									
									</tr>
									<?php endforeach;
										}else {
 ?>
										<tr> <td  colspan="<?php echo count($tb_headers) ?>"> No data available <td> </tr> 
									<?php 	} ?>
                                        </tbody>
                               
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div> 
                </section><!-- /.content --> 