<?php
class MY_Model extends CI_Model {
	var $table = '';
	var $qry = '';
	var $table_id = '';

	function __construct() {
		// Call the Model constructor
		parent::__construct();
	}

	function save($data) {
		//do insert data into database
		//if ($this -> db -> insert($this -> table, $data)) {
		$insert_query = $this -> db -> insert_string($this -> table, $data);
		$insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);

		if ($this -> db -> query($insert_query)) {

			return True;
		} else {
			return FALSE;
		}
	}

	function update($id, $data) {
		//do update data into database
		$this -> db -> where($this -> table_id, $id);
		if ($this -> db -> update($this -> table, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	function delete($id) {
		//do delete data from database
		$this -> db -> where($this -> table_id, $id);
		$qry = $this -> db -> delete($this -> table);
		if ($qry) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function read($where = FALSE) {
		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		$result = $this -> db -> get() -> result_array();
		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	function mycounter($where = False) {

		$this -> db -> select("count($this->table_id) as total");
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}

		$result = $this -> db -> get() -> result_array();
		if ($result) {
			foreach ($result as $key => $value) {

			}
			return $value['total'];
		} else {
			return false;
		}
	}

}
 ?>
