<?php
include ("../connection.php");

//Use these two lines for a static date.
//$from_date = date_create("2013-01-18 07:00:00"); //year-month-day hours:minutes:seconds
//$to_date = date_create("2013-01-18 19:00:00"); //year-month-day hours:minutes:seconds

//Use these two lines for Today's date.
$from_date = date_create(date("Y-m-d")." 07:00:00"); //year-month-day hours:minutes:seconds
$to_date = date_create(date("Y-m-d")." 19:00:00"); //year-month-day hours:minutes:seconds

$sql_all_users = "SELECT userid, name, email FROM users";
$res_all_users = mysql_query($sql_all_users, $con);

if(mysql_num_rows($res_all_users) > 0){
	
	while($row_user = mysql_fetch_assoc($res_all_users)){
		
		$sql_user_devices = "SELECT p.device_name, p.phone_imei FROM phones p INNER JOIN phones_users pu ON p.phoneid = pu.phoneid WHERE pu.userid = ".$row_user["userid"]." AND p.email_report = true";
		$res_users_devices = mysql_query($sql_user_devices, $con);
		
		while( $row_user_devices = mysql_fetch_assoc($res_users_devices) ){
			
			$interval_start = clone $from_date;
			$interval_end = clone $from_date;
			$interval_end->modify("+30 minutes");
			
			$html = "<body>";
			
			$html .= '<br /><table border="1" cellpadding="0" cellspacing="0" align="center">';
			
			$html .= "<tr>";
			$html .= "<td width='100' align='center' colspan='3'>";
			$html .= '<strong>Device Name : </strong>'.$row_user_devices["device_name"];
			$html .= '<br /><strong>IMEI : </strong>'.$row_user_devices["phone_imei"];
			$html .= '<br /><strong>Date : </strong>'.date_format($from_date, "Y-m-d");
			$html .= "</td>";
			$html .= "</tr>";
			
			$html .= "<tr>";
			$html .= "<td width='100' align='center'><strong>From</strong></td>";
			$html .= "<td width='100' align='center'><strong>To</strong></td>";
			$html .= "<td  width='170' align='center'><strong>Location</strong></td>";
			$html .= "</tr>";
			
			while($interval_start < $to_date){
				
				$loc_content = "No Location Available";
				
				$sql_device_locations = "SELECT id, imei, date, latitude, longitude FROM location WHERE imei = '".$row_user_devices["phone_imei"]."' AND date BETWEEN '".date_format($interval_start, "Y-m-d H:i:s")."' AND '".date_format($interval_end, "Y-m-d H:i:s")."'";
				$res_device_locations = mysql_query($sql_device_locations, $con);
				
				if(mysql_num_rows($res_device_locations) > 0){
					$row_device_loc = mysql_fetch_assoc($res_device_locations);
					$loc_content = "<a href='showDeviceLocation.php?imei=".$row_user_devices["phone_imei"]."&device=".$row_user_devices["device_name"]."&from=".date_format($interval_start, "Y-m-d_H:i:s")."&to=".date_format($interval_end, "Y-m-d_H:i:s")."&lat=".$row_device_loc["latitude"]."&lng=".$row_device_loc["longitude"]."' target='_new'>";
					$loc_content .= "<img src='http://maps.googleapis.com/maps/api/staticmap?center=".$row_device_loc["latitude"].",".$row_device_loc["longitude"]."&zoom=14&size=150x150&maptype=roadmap&markers=color:red%7C".$row_device_loc["latitude"].",".$row_device_loc["longitude"]."&sensor=false' width='150' height='150' />";
					$loc_content .= "</a>";
				}
				
				$html .= "<tr>";
				$html .= "<td align='center' valign='middle'>".date_format($interval_start, "H:i:s")."</td>";
				$html .= "<td align='center' valign='middle'>".date_format($interval_end, "H:i:s")."</td>";
				$html .= "<td align='center' valign='middle' height='170'>".$loc_content."</td>";
				$html .= "</tr>";
				
				$interval_start = clone $interval_end;
				$interval_end->modify("+30 minutes");
				
			}
			
			$html .= '</table>';
			
			$html .= "</body>";
			
			
			
			$line_brake_ch = "\r\n"; // "\r\n" for windows... "\n" for Unix... "\r" for MAC

			$sender_name = "Foot Print Site";
			$sender = "no-reply@pro.co.tz";
			$sender_organization = "Foot Prints";
			
			$email_to = $row_user["email"];
			$message_subject = "Locations of Devices";
			$message_body = $html;
			
			$headers = "From: ".$sender_name." <".$sender.">".$line_brake_ch;
			$headers .= "Reply-To: ".$sender_name." <".$sender.">".$line_brake_ch;
			$headers .= "Errors-To: ".$sender_name." <".$sender.">".$line_brake_ch;
			
			$headers .= "Organization: ".$sender_organization."".$line_brake_ch;
			$headers .= "MIME-Version: 1.0".$line_brake_ch;
			$headers .= "Content-type: text/html; charset=iso-8859-1".$line_brake_ch;
			$headers .= "X-Priority: 3".$line_brake_ch;
			$headers .= "X-Mailer: PHP". phpversion() .$line_brake_ch;
			
			if(mail($email_to, $message_subject, $message_body, $headers, "-f".$sender)){
				//echo "<br />SENT...".$row_user_devices["device_name"];
			}else{
				//echo "<br />FAILED...".$row_user_devices["device_name"];
			}
			
		}
		
	}
	
}
?>

