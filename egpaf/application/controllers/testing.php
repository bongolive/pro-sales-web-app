<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Testing extends CI_Controller {

	public $perms_dsc = array('fullname', 'permissions');

	public function __construct() {

		parent::__construct();
	}

	public function index() {
		echo "this is index page";
	}

	public function tester() {
		echo "$this is testing page";
	}

}
