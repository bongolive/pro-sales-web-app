<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');

/**
 * Created by PhpStorm.
 * User: nasznjoka
 * Date: 4/28/15
 * Time: 2:09 PM
 */

//die("testing here if all models are alright");
class Web_service extends REST_Controller{
    private $account_id;
    private $device_imei;
    private $returned = array();

    function __construct()    {
        // Construct our parent class
        parent::__construct();

        $this -> load -> model('stock_model');
        $this -> load -> model('orders_model');
        $this -> load -> model('products_model');
        $this -> load -> model('transaction_model');
        $this -> load -> model('devices_model');
        $this -> load -> model('location_model');
        $this -> load -> model('employees_model');
        $this -> load -> model('settings_model');
        $this -> load -> model('customers_model');
        $this -> load -> library('user_agent');

        //die("testing here if all models are alright");
    }

    public function index_get()
    {
        $this->returned['failed'] = 1;
        $this->returned['message'] = "invalid request only get authorized";
        $this->response($this->returned);
    }

    public function index_post()    {

        $data = file_get_contents('php://input');
        $data = json_decode($data, true);

        $tags = $data['tag'];
        $imei = $data['imei'];

        // file_put_contents("imei.txt",$imei.'\n',FILE_APPEND);
        // if (!$this->agent->is_mobile()){

            if (isset($tags) && isset($imei)) {
                $account = $this->devices_model->_api_get_device_account( $imei );
                if ($account) {
                    switch ($tags) {
                        case 'upload_customers':
                            $this->account_id = $account;
                            $this->device_imei = $imei;
                            $this->_save_customers( $data["incommingcustomers"] );
                            break;
                        case 'assign_products':
                            $this->account_id = $account;
                            $this->device_imei = $imei;
                            $this->_assign_products();
                            break;
                        case 'assign_customers':
                            $this->account_id = $account;
                            $this->device_imei = $imei;
                            $this->_assign_customers();
                            break;
                        case 'upload_orders':
                            $this->account_id = $account;
                            $this->device_imei = $imei;
                            $this->_save_orders($data["orders"]);
                            break;
                        case 'upload_orders_items':
                            $this->account_id = $account;
                            $this->device_imei = $imei;
                            $this->_save_orders_items($data["orderitems"]);
                            break;
                        case 'assign_settings':
                            $this->account_id = $account;
                            $this->device_imei = $imei;
                            $this->_assign_settings();
                            break;
                        case 'device_tracking':
                            $this->account_id = $account;
                            $this->device_imei = $imei;
                            $this->_save_track($data['datapoints']);
                            break;
                        case 'multi_media_data':
                            $this->account_id = $account;
                            $this->device_imei = $imei;
                            $this->_process_media($data['multimedia_array']);
                            break;
                        case 'acktag':
                            $this->account_id = $account;
                            $this->device_imei = $imei;
                            if ($data['ack'] == 1) {
                                switch ( $data['key'] ) {
                                    case 'prod_system_id':
                                        $this->_update_stock($data);
                                        break;
                                    case 'system_customer_id':
                                        $this->_update_customer($data);
                                        break;
                                }
                            }
                            break; 
            			case 'upload_stock' :
            			     $this->_upload_stock();
            		      	break;			                    
                        default:
                            $this->returned['failed'] = 1;
                            $this->returned['message'] = "invalid request unkown tag";
                            $this->response($this->returned);
                            break;
                    }
                } else {			
			if($tags == "regtag"){
			     // Do Registration 
			
				
			     }
			else {		    
			    $this->returned['failed'] = 1;
			    $this->returned['message'] = "device not registered ";/*.$this->db->last_query();*/
			    $this->response($this->returned);
			}

                }
            } else {
                $this->returned['failed'] = 1;
                $this->returned['message'] = "invalid request tags or imei null";
                $this->response($this->returned);
            }
       /* } else {
            $this->returned["error"] = 1;
            $this->returned["message"] = "invalid request this is not android device";
            $this->response($this->returned);
        }*/
    }

	
    /**************************** assign customers to the device ******************************/
    private function _assign_customers(){
        $where = (array('account_id'=> $this->account_id,'device_imei' => $this->device_imei,'is_synced' => 0));
        $customers = $this-> customers_model -> _api_get_assigned_customers($where);

        if($customers){
            $this->returned['success'] = 1;
            $this->returned['customerList'] = $customers;
            $this->response($this->returned);
        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "no customers";
            $this->response($this->returned);
        }
    }

    /**************************** save customers from the device ******************************/
    private function _save_customers($data){

        $commons = (array('account_id'=> $this->account_id,'device_imei' => $this->device_imei));
        $customers = $this-> customers_model -> _api_receive_device_customer($commons,$data);

        if( $customers ){
            $returnval = array();
            foreach( $customers["systemid"] as $key => $value ):
                $returnval[] = array("system_customer_id" => $value['system_customer_id'],
                    "customer_id" => $value['customer_id']);
            endforeach;
            $this->returned['success'] = 1;
            $this->returned['customer'] = array('customerSentList' => $returnval);
            $this->response($this->returned);
        }else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "no customer received";
            $this->response($this->returned);
        }
    }

    /**************************** assign products to the device ******************************/
    private function _assign_products()
    {
        $where = array('device_imei' => $this->device_imei,'is_synced' => 0);

        $stock = $this->stock_model ->_api_get_all_stock( $where );
        if($stock){
            $this->returned['success'] = 1;
            $this->returned['productsList'] = $stock;
            $this->response($this->returned);
        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "no products";
            $this->response($this->returned);
        }
    }

    /**************************** Upload Orders *****************************/
    private function _save_orders($data)    {
        $where = array('device_imei'=> $this->device_imei, 'account_id' => $this->account_id);
        $save = $this->orders_model->_api_save_orders($data,$where);
        if ($save) {
            $arrayback = array();
            foreach ($data as $key => $value):
                $arrayback[] = array("order_id" => $value['order_id']);
            endforeach;
            $this->returned['success'] = 1;
            $this->returned['orderList'] = array('ordersList' => $arrayback);
            $this->response($this->returned);
        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "order not received";
            $this->response($this->returned);
        }
    }

    private function _save_orders_items($data)
    {
        $where = array('device_imei'=> $this->device_imei, 'account_id' => $this->account_id);
        $save = $this->orders_model->_api_save_order_details($data,$where);
        if($save){
            $returnval = array();
            foreach($data as $key => $value):
                $returnval[] = array("order_id" => $value['order_id']);
            endforeach;
            $this->returned['success'] = 1;
            $this->returned['orderList']= array('orderitemsList' => $returnval);
            $this->response($this->returned);
        }  else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "order item not received";
            $this->response($this->returned);
        }
    }

    private function _assign_settings(){
        $where = (array('account_id'=> $this->account_id));
        $getsettings = $this-> settings_model->_api_get_assigned_settings($where);
        $getpaymodes = $this-> settings_model->_api_get_assigned_paymodes($where);
        if($getsettings && $getpaymodes){
            $settins = array();
            $settins['settings']=$getsettings;
            $settins['pay_modes']=$getpaymodes;
            $this->returned['success'] = 1;
            $this->returned['settingList'] = array($settins);
            $this->response($this->returned);
        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "no settings and pay modes assigned";
            $this->response( $this->returned );
        }
    }

    private function _save_track($data){
        $where = array('device_imei'=> $this->device_imei, 'account_id' => $this->account_id);
        $save = $this-> location_model ->_api_save_tracking($data,$where);
        if($save){
            $returnval = array();
            foreach($data as $key => $value):
                $returnval[] = array("tracker_id" => $value['tracker_id']);
            endforeach;
            $this->returned['success'] = 1;
            $this->returned['datapointList']= array('datapointLists' => $returnval);
            $this->response($this->returned);
        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "no location data received";
            $this->response($this->returned);
        }
    }
    private function _process_media($data){

            //file_put_contents("debug2.txt",print_r($data,true),FILE_APPEND);
            $where = array('device_imei'=> $this->device_imei, 'account_id' => $this->account_id);
            $save = $this-> devices_model ->_api_store_multimedia($data,$where);

            if(is_array($save))
            {
                if (count($save, COUNT_NORMAL) > 0) {
                    $this->returned['success'] = 1;
                    $this->returned['datapointList'] = array('datapointLists' => $save);
                    $this->response($this->returned);
                } else {
                    $this->returned['error'] = 1;
                    $this->returned['message'] = "no content received";
                    $this->response($this->returned);
                }
            } else {
                $this->returned['error'] = 1;
                $this->returned['message'] = "no content received (not array)";
                $this->response($this->returned);
            }

    }

    private  function _update_stock($data){
        $this->returned['acknoledge'] = array('ref' => $data['ref']
        ,'sys_stock_id' => $data['sys_stock_id'],
            'key' => $data['key'],'ack' => $data['ack']);

        $where = array('stock_id' => $data['sys_stock_id'], 'account_id'=> $this->account_id);
        $data = $this-> stock_model -> _api_update_sales_team_sync($where);
        if($data){
            $this->returned['success'] = 1;
            $this->returned['time'] = date('Y-m-d h:i:s');
            $this->response($this->returned);
        }else{
            $this->returned['error'] = 1;
            $this->returned['message'] = "stock failed to update";
            $this->response($this->returned);
        }
    }
    private  function _update_customer($data){
        $this->returned['acknoledge'] = array('system_customer_id' => $data['system_customer_id'],
            'key' => $data['key'],'ack' => $data['ack']);

        $where = array('customer_id' => $data['system_customer_id'], 'device_imei' => $this->device_imei);
        $data = $this-> customers_model -> _api_update_sales_team_sync($where);
        
        if($data){
            $this->returned['success'] = 1;
            $this->returned['time'] = date('Y-m-d h:i:s');
            $this->response($this->returned);
        }else{
            $this->returned['error'] = 1;
            $this->returned['message'] = "stock failed to update";
            $this->response($this->returned);
        }
    }


 function encrypt_post(){
        $this->load->library('mcrypt');
        $data = file_get_contents('php://input');
        $data = json_decode($data, true);
        $mystring = "teststring";       
        $encrypted = $this->mcrypt->encrypt($mystring);
        $retArr = array('original'=>$mystring,
            'token'=>$encrypted,
            'decrypted'=>$this->mcrypt->decrypt( $encrypted ));

        $this->response( $retArr );
    }


 function authenticate_post(){
    
        $this->load->library('mcrypt');
        
        //$tag = $this->post('tag');
	    $data = file_get_contents('php://input');
        $data = json_decode($data, true);
	    
        $tag = $data['tag'];
	    $imei = $data['imei'];
        
        if($tag == "register_device"){

            //$acc_details =  $this->fetch_acccount_details( $imei );

            $acc_details = $this->devices_model->registration( $imei );            
            
            $acc_type = $acc_details->acc_type;

            $business_name = $acc_details->company_name;
            $contact_person = $acc_details->contact_person;
            $mobile_number = $acc_details->mobile;

            $status = ($acc_type) ? array('success'=>1) : array('failure'=>1);

            $encrypted = $this->mcrypt->encrypt( $imei."|".$acc_type );

            $ac_t = array( 'account'=>$acc_type , 'business_name'=>$business_name, 
                'contact_person'=>$contact_person, 'mobile'=>$mobile_number);
            
            //$debug_a = array('imei'=>$imei, 'decrypt'=>$this->mcrypt->decrypt($encrypted));
            $retArr = array_merge($status, $ac_t, array(
                            'authentication_key'=>$encrypted));

            $this->response( $retArr );
        }
        if($tag == "authenticate"){

		//$imei = $this->post('imei');
            $acc_details =  $this->devices_model->registration( $imei );            
            $acc_type = $acc_details['acc_type'];
            
            $encrypted = $this->mcrypt->encrypt( $imei."|".$acc_type );

            $encrypt_device = $data['authentication_key'];

            $status = ( $encrypted  == $encrypt_device ) ? array('success'=>1) : array('failure'=>1);
            
            $ac_t = array('account'=>$acc_type);
            //$debug_a = array('imei'=>$imei, 'decrypt'=>$this->mcrypt->decrypt($encrypted));
            $retArr = array_merge( $status , $ac_t  );

            $this->response( $retArr );
        }
    }

    function fetch_acccount_details($imei){

    }
    
    public function activateCustomer_post(){
        $data = file_get_contents('php://input');
        $data = json_decode($data, true);
        $device_imei = $data['imei'];

        $query = $this->db->query(" SELECT * FROM pro_accounts PA, pro_devices PD WHERE PA.account_id = PD.account_id AND PD.device_imei = '{$device_imei}' ");
        //echo $this->db->last_query();exit;
        if( $query->num_rows() > 0 ){            
            
            $row = $query->row_array(); //exit;

            if( $row['status'] == 0 ){

                    $account_id = $row['account_id'];
                    $this->db->where(array( 'account_id'=>$account_id ) );
                    $this->db->update('pro_accounts', array( 'status'=>1 ) );
                    $retArr = array_merge( array('success'=>1) , array('device_imei'=> $device_imei ) , array('message'=>'Account activated successfully.') );
                
                }
                else
                    $retArr = array_merge( array('failure'=>1) , array('device_imei'=> $device_imei ) , array('message'=>'This Account already Active.') );
        }
        else
            $retArr = array_merge( array('failure'=>1) , array('device_imei'=> $device_imei ),  array('message'=>'IMEI is not found in our system.') );

        $this->response( $retArr );
    }

}