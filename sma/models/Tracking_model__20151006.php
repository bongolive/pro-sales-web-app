<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tracking_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    

 public function getLastKnownLocation($where = null, $imei=null, $sdate=null, $edate=null){
        /* $query = $this->db->query("SELECT e.id,CONCAT(e.firstname, ' ',e.lastname) AS holder_name, e.mobile, de.device_id,de.device_imei,de.assigned_to,de.device_descr AS device_name,
          ifnull((SELECT l.latitude FROM pro_location l WHERE l.imei = de.device_imei ORDER BY l.servertime DESC LIMIT 1),NULL ) AS latitude,
          ifnull((SELECT l.longitude FROM pro_location l WHERE l.imei = de.device_imei ORDER BY l.servertime DESC LIMIT 1),NULL ) AS longitude,
          ifnull((SELECT l.date FROM pro_location l WHERE l.imei = de.device_imei ORDER BY l.servertime DESC LIMIT 1),NULL ) AS localdate
        FROM pro_employees e INNER JOIN pro_devices de ON e.id = de.assigned_to AND e.account_id = '$where'");*/
        $filter_string_imei = ($imei) ? " AND de.device_imei = ".$imei : ""; 

        $filter_string_date = ($sdate && $edate) ? " AND l.date BETWEEN '".urldecode($sdate) ."' AND '".urldecode($edate)."'" : ""; 

        $query = $this->db->query("SELECT l.id,CONCAT(e.first_name, ' ', e.last_name) AS holder_name, e.phone, de.device_id,
            de.device_imei,de.assigned_to,de.device_descr AS device_name,
          l.lat  AS latitude,
           l.lng  AS longitude,
         l.date  AS localdate,
         l.paid,
         l.grand_total,
         l.total_tax
        FROM sma_users e, sma_sales l, sma_devices de, sma_warehouse_map wm
        
        WHERE e.id = de.assigned_to     AND 
         l.device_imei = de.device_imei and
        l.warehouse_id = wm.warehouse_id AND
        l.account_id = e.account_id
        AND e.account_id = '{$where}' ".$filter_string_imei.$filter_string_date." GROUP BY de.device_imei ORDER BY l.date DESC LIMIT 10");
        //echo $this->db->last_query();exit;
       if( $query->num_rows() > 0 ){
           return $query->result_array();
       }
       else
        return array();
    }
    public function get_deviceLocationByImei($account_id,$imei,$sdate,$edate)    {
        //$filter_string = ($imei) ? " AND de.device_imei = ".$imei : ""; 
        $query = $this->db->query("SELECT l.id,CONCAT(e.first_name, ' ', e.last_name) AS holder_name, e.phone, de.device_id,
            de.device_imei,de.assigned_to,de.device_descr AS device_name,
          l.lat  AS latitude,
           l.lng  AS longitude,
         l.date  AS localdate
        FROM sma_users e, sma_sales l, sma_devices de, sma_warehouse_map wm
        
        WHERE e.id = de.assigned_to     AND 
         l.device_imei = de.device_imei AND
        l.warehouse_id = wm.warehouse_id AND
        l.account_id = e.account_id
        AND e.account_id = '{$account_id}' GROUP BY de.device_imei  ORDER BY l.date DESC LIMIT 10");
        //echo $this->db->last_query();exit;
       if( $query->num_rows() > 0 ){
           return $query->result_array();
       }
       else
        return array();
    }
    
    function getAllDevicesOfAccount($account_id){
        $this->db->where(array('account_id'=> $account_id));
        $q = $this->db->get('devices');

        //echo $this->db->last_query();exit;
        if($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return array();       
    }


    public function getTrackerLocation( $account_id, $imei=null, $sdate=null, $edate=null ){


            //$filter_string = ($imei) ? " AND de.device_imei = ".$imei : "";
            $query = $this->db->query(" SELECT l.id,CONCAT(u.first_name, ' ', u.last_name) AS holder_name, u.phone, de.device_id,
                                        de.device_imei,de.assigned_to,de.device_descr AS device_name,
                                        l.lat  AS lat,
                                        l.lng  AS lng,
                                     l.device_date  AS device_date
                                    FROM sma_users u, sma_device_tracking l, sma_devices de
                                    WHERE u.id = de.assigned_to AND l.device_imei = de.device_imei
                                    AND u.account_id = ".$account_id." GROUP BY l.lat,l.lng,sys_date ORDER BY l.sys_date DESC LIMIT 10");
            //echo $this->db->last_query();exit;
            if( $query->num_rows() > 0 ){
                return $query->result_array();
            }
            else
                return array();

    }

    public function getCustomerLocation($account_id = null, $imei=null, $sdate=null, $edate=null){

        $filter_string_imei = ($imei) ? " AND C.device_imei = ".$imei : "";


       /* $query = $this->db->query("SELECT coalesce(SUM(S.total), 0) AS grand_total, C.`name` as holder_name, C.phone , C.lattd  AS latitude, C.
                              lontd  AS longitude,
							C.last_update_time AS localdate,C.address
                            FROM pro_base.sma_companies C
                            LEFT OUTER JOIN sma_sales S
                            ON S.customer_id = C.id
                            WHERE C.group_name = 'customer' and C.account_id = {$account_id} ".$filter_string_imei."
                            GROUP BY S.customer_id;");
                            */
      $query = $this->db->query("SELECT coalesce(SUM(S.total), 0) AS grand_total, C.`name` as holder_name, C.phone , C.lattd  AS latitude, C.
                              lontd  AS longitude,
                          C.last_update_time AS localdate,C.address
                            FROM sma_companies C
                            LEFT OUTER JOIN sma_sales S
                            ON  S.customer_id = C.id WHERE C.group_name = 'customer' AND C.account_id = {$account_id} ".$filter_string_imei."
                            GROUP BY C.id ORDER BY S.date DESC LIMIT 10;");
       //echo $this->db->last_query();exit;

        //var_dump( $query->result_array()); exit;

        if( $query->num_rows() > 0 ){
            return $query->result_array();
        }
        else
            return array();
    }


}
