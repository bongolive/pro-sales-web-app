				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('adding_stock'); echo ": ". strtoupper($team['device_imei']); ?></h2> 
							
						 <a href="<?php echo site_url('stock/viewbatch/'.$team['batch_no']); ?>"  style="float: right; padding: 8px;" class="btn btn-alt" data-dismiss="modal"><?php _l('back') ?></a>
            			</header> 
       				 </article> 
    </div>	
    <div class="row">
     						
  
    	 <article class="span12 data-block">
								<div class="modal-header">
									<h4><?php _l('add_stock'); ?>
										<span class="icon-calendar" style="float: right; "><?php _l('stock_date'); echo ': '.date('Y-m-d');?></span></h4>
								
								</div>
								<div class="modal-body">
				 
							
							<form class="form-horizontal" method="post" action="<?php echo site_url('stock/updateteamstock'); ?>">
				 
                              	
                                	<input type="hidden" name="team_id" value="<?php echo $team['employee_id'];  ?>"/>
                                	<input type="hidden" name="device_imei" value="<?php echo $team['device_imei'];  ?>"/>
                                	<input type="hidden" name="batch_no" value="<?php echo $team['batch_no'];  ?>"/>
                           
                                    <fieldset>
      
<div class="control-group">
                                        	<label class="control-label" for="order_no" ><?php _l('stock_details'); ?></label>
                                        	<div class="controls">
                                        		<table class="datatable table table-striped table-bordered table-hover"><tr><th></th><th><?php  _l('product_name')?></th> <!--<th><?php  _l('stock_date')?></th>--> <th><?php  _l('stock_qty')?></th><th><?php  _l('entry_mode')?></th> <th><?php  _l('sale_price')?></th><th><?php _l('stock_value');?></th></tr></table>

<TABLE id="dataTable" class="datatable table table-striped table-bordered table-hover" >
   <?php 
   
   //get products
   					if($products ==FALSE){  $products = array(''=>'No Products'); } 
            	  
   
     	if($teamstock) {
 
	foreach ($teamstock as $info):
	 
		
		 	$prod = $info['product_id'];  
		?>
		
		<TR>
            <TD><INPUT type="hidden" name="stock_id[]" value="<?php echo $info['stock_id']; ?>"/></TD>
            <TD><?php echo form_dropdown('product_id[]', $products, $prod); ?>
            </TD>
            <TD><INPUT type="number" name="stock_qty[]"class="input-medium"  value="<?php echo $info['stock_qty']; ?>" placeholder="Quantity" required="" /></TD>
            	   <TD>
            	<?php 
            	$value =  $info['entry_mode']; 
            	$emodes = array('units'=>'Units','packages'=>'package');
				$attr = 'class="input-small"';
				echo form_dropdown('entry_mode',$emodes,$value,$attr);
				
            	?> </TD>
            <TD><INPUT type="number" name="price[]"class="input-medium" placeholder="sale Price" disabled=""  value="<?php echo $info['sale_price']; ?>"required="" /></TD>
             <TD><input  type="text"  name=""class="input-medium" value=" " placeholder="Stock value" disabled="" /></TD>	
            	
            
        </TR>
		
<?php 	endforeach;
}
		?>
     
        <TR>
             <TD><INPUT type="hidden" name="stock_id[]" value="added"/></TD>
            <TD><?php

			if ($products == FALSE) {  $products = array('' => 'No Products');
			}
			 $prod = set_value('product_id');
			 
			$id = 'id="s_t_product"';
			echo form_dropdown('product_id[]', $products, $prod,$id);
            		?>
            </TD>
         <!--  	<TD><input  type="date"  name="stock_date[]"class="input-medium" placeholder="Stock date" required="" /></TD>-->
            <TD><INPUT type="number" name="stock_qty[]"class="input-medium" id="s_t_qty" placeholder="Quantity" required="" /></TD>
               <TD>
            	<?php 
            	$value =  $info['entry_mode']; 
            	$emodes = array('units'=>'Units','packages'=>'package');
				$attr = 'class="input-small"';
				echo form_dropdown('entry_mode',$emodes,$value,$attr);
				
            	?> </TD>
            <TD><input  type="text"  name="sale"class="input-medium" value=" " placeholder="Sale price" disabled="" /></TD>
            <TD><input  type="text"  name="stock"class="input-medium" value=" " placeholder="Stockvalue" disabled="" /></TD>
            
        </TR>
      </TABLE> 
     <?php
    
     ?> 
       <span class="" style="float: right;padding-right: 5%">
 <input type="button" class="btn btn-medium btn-info" value="<?php _l('add_product') ?>" onclick="addRow('dataTable')" />
 <input type="button" class="btn btn-medium btn-danger" value="<?php _l('remove_product') ?>" onclick="deleteRow('dataTable')" />
 </span>   
 
    </div>
</div>
						</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('stock/viewbatch/'.$team['batch_no']); ?>" class="btn btn-alt" data-dismiss="modal"><?php _l('back') ?></a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit"><?php _l('save') ?></button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>
                        </div>                        	
                        </div>	
                        </section>
                        		
					 