<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l($tb_name); ?></h2>
							&nbsp; &nbsp;	<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url($print_excel);?>" style="float: right; padding:8px; margin-left: 20px;"> <img src="<?php echo base_url('template/img/icons/excel.png'); ?>" alt="export"/></a>
					  <?php if($tb_name == 'stocks_tb_name'){ ?>
  <a class="btn btn-alt btn-primary" data-toggle="modal" href="#demoModal" style="float: right; padding:8px;">Assign Stock to Device</a>
  					<?php }else if($tb_name == 'products_tb_name'){ ?>
  					
						<a class="btn btn-alt btn-primary" data-toggle="modal" href="#demoProducts" style="float: right; padding:8px;">Add New Product</a>
					
						
					<?php }else if($tb_name == 'clients_tb_name'){ ?>
						<a class="btn btn-alt btn-primary" data-toggle="modal" href="#clientsModel" style="float: right; padding:8px;">Add New Customer</a>
					<?php } ?>
  
  
            </header> 

        </article>

    </div>	
    <div class="row">
 
		<!-- Data block -->
	<article class="span2">
	
	<h4><?php _l('filter'); ?></h4>
	
    <form class="form-horizontal" method="post" action="">
    <?php if($tb_name == 'stocks_tb_name'  || $tb_name == 'sales_tb_name' || $tb_name == 'returned_stocks_tb_name' || $tb_name == 'products_tb_name' ){ ?>
                                        	
                                        	<select name="product_name" id="product_name"  class="span2">
												<option value="">Select Product</option>
												<?php
												foreach ($products as $product): ?>
												<option value="<?php echo $product['product_name']; ?>" <?php if($product['product_name']==$product_name){ echo "selected"; } ?>><?php echo $product['product_name']; ?></option>
												<?php endforeach; ?>
											</select><Br /><Br />
											
											<?php if($tb_name != 'products_tb_name'){ ?>
											
											<select name="phones" id="phones" class="span2">
												<option value="">Select Device</option>
												<?php
												foreach ($phones as $device): ?>
												<option value="<?php echo $device['phone_imei']; ?>" <?php if($device['phone_imei']==$device_imei){ echo "selected"; } ?>><?php echo $device['device_name']; ?></option>
												<?php endforeach; ?>
											</select><Br /><Br />


                                        	<input name="start_date" id="start_date" class="show_date_piker span2" type="text" placeholder="<?php _l('start_date'); ?>" value="<?php echo $start_date; ?>" /><Br /><Br />
										

                                        	<input name="end_date" id="end_date" class="show_date_piker span2" type="text" placeholder="<?php _l('end_date'); ?>" value="<?php echo $end_date; ?>"/><Br /><Br />
                                        	
                                        	<?php } ?>
                                        	
                                        	<?php } else if($tb_name == 'clients_tb_name'){ ?>
												
												<input name="shop_name" id="shop_name" class="span2" type="text" value="<?php echo $shop_name;?>" placeholder="<?php _l('shop_name'); ?>"  /><Br /><Br />
												
												<input name="client_name" id="client_name" class="span2" type="text" value="<?php echo $client_name;?>" placeholder="<?php _l('client_name'); ?>"  /><Br /><Br />
												
												<input name="phone" id="phone" class="span2" type="number" value="<?php echo $phone;?>" placeholder="<?php _l('phone'); ?>"  /><Br /><Br />
												
												
												<input name="email" id="email" class="span2" type="email" value="<?php echo $email;?>"placeholder="<?php _l('email'); ?>" /> <Br /><Br />
												
												
											<?php } ?>
                                        	
            				           <button class="btn btn-alt btn-large btn-primary" type="submit" >Search</button>
                        </form>
</article>		
		<!--Data block -->
		
		
		
        <article class="span10 data-block">
  <!--
				<header>

				    <h2><span class="icon-folder-open" style="padding-right:10px;"></span><?php _l($tb_name); ?></h2>
                     
				</header>
 -->
                <section>
 				

				
				<div class="modal fade hide" id="clientsModel">
								<div class="modal-header">
									<h4><?php _l('add_new_client'); ?></h4>
								</div>
								<div class="modal-body">
                                <form class="form-horizontal" method="post" action="<?php echo site_url('stocks/addclient');?>">
                                    <fieldset>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="shop_name"><?php _l('shop_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="shop_name" id="shop_name" class="input-xlarge" type="text" required />
                                        	</div>
                                    	</div> 
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="client_name" ><?php _l('client_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="client_name" id="client_name" class="input-xlarge" type="text" required />
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="phone"><?php _l('phone'); ?></label>
                                        	<div class="controls">
                                        	<input name="phone" id="phone" class="input-xlarge" type="number" required />
                                        	</div>
                                    	</div>
										
										<div class="control-group">
                                        	<label class="control-label" for="email"><?php _l('email'); ?></label>
                                        	<div class="controls">
                                        	<input name="email" id="email" class="input-xlarge" type="email" required />
                                        	</div>
                                    	</div> 
										
										<div class="control-group">
                                        	<label class="control-label" for="address" ><?php _l('address'); ?></label>
                                        	<div class="controls">
                                        	<textarea name="address" id="address" class="input-xlarge" rows="5" required></textarea>
                                        	</div>
                                    	</div>           
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </div>
				
				
				
				
				
				
				<?php if ($tb_name == 'stocks_tb_name'){?>
					
					<div class="modal fade hide" id="demoModal">
								<div class="modal-header">
									<h4><?php _l('add_new_stock'); ?></h4>
								</div>
								<div class="modal-body">
                                <form class="form-horizontal" method="post" action="<?php echo site_url('stocks/addStock');?>">
                                    <fieldset>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="phone_imei"><?php _l('device_name'); ?></label>
                                        	<div class="controls">
                                        	<select name="phone_imei" id="phone_imei" required >
												<option value="">Select Device</option>
												<?php
												foreach ($devices as $device): ?>
												<option value="<?php echo $device['phone_imei']; ?>"><?php echo $device['device_name']; ?></option>
												<?php endforeach; ?>
                                                
											</select>
                                        	</div>
                                    	</div>

                                       <div id="" style="height:200px; margin-top:20px; overflow-y: auto;">
                                       <?php foreach ($products as $product): ?>
                                       <div class="control-group">
                                        	<label class="control-label" for="<?php echo $product['product_name']; ?>"><?php echo $product['product_name']; ?></label>
                                        	<div class="controls">
                                        	<input type="hidden" name="product_name[]" value="<?php echo $product['product_name']; ?>" />
                                        	<input type="number" name="items[]" value="" class="input-xsmall" />
                                        	</div>
                                    	</div>
                                       <?php endforeach; ?>
                                      </div> 
                                       
       
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </div>
						
						
						
						
						
						
						
						
						
						<div class="modal fade hide" id="returnStockModal">
								<div class="modal-header">
									<h4><?php _l('return_stock'); ?></h4>
								</div>
								<div class="modal-body">
                                <form class="form-horizontal" method="post" action="<?php echo site_url('stocks/returnstock');?>">
                                    <fieldset>
                                        
										<div class="control-group">
                                        	<label class="control-label" for="device_imei"><?php _l('device_imei'); ?></label>
                                        	<div class="controls">
                                        	<select name="phone_imei" id="phone_imei" required >
												<?php
												foreach ($devices as $device): ?>
												<option value="<?php echo $device['phone_imei']; ?>"><?php echo $device['device_name']; ?></option>
												<?php endforeach; ?>
                                                
											</select>
                                        	</div>
                                    	</div>
										<div class="control-group">
                                        	<label class="control-label" for="product_name"><?php _l('product_name'); ?></label>
                                        	<div class="controls">
                                        	<select name="product_name" id="product_name" required >
												<?php
												foreach ($products as $product): ?>
												<option value="<?php echo $product['product_name']; ?>"><?php echo $product['product_name']; ?></option>
												<?php endforeach; ?>
                                                
											</select>
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="items_received" ><?php _l('items_received'); ?></label>
                                        	<div class="controls">
                                        	<input name="items_received" id="items_received" class="input-xlarge" type="number" required />
                                        	</div>
                                    	</div>
										
										<div class="control-group">
                                        	<label class="control-label" for="items_issued" ><?php _l('items_issued'); ?></label>
                                        	<div class="controls">
                                        	<input name="items_issued" id="items_issued" class="input-xlarge" type="number" required />
                                        	</div>
                                    	</div>
										
										<div class="control-group">
                                        	<label class="control-label" for="items_returned" ><?php _l('items_returned'); ?></label>
                                        	<div class="controls">
                                        	<input name="items_returned" id="items_returned" class="input-xlarge" type="number" required />
                                        	</div>
                                    	</div>
										
                                        <div class="control-group">
                                        	<label class="control-label" for="stock_date"><?php _l('stock_date'); ?></label>
                                        	<div class="controls">
                                        	<input name="stock_date" id="stock_date" class="input-xlarge" type="text" required />
                                        	</div>
                                    	</div>            
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </div>
						
						
						
						
						
						
						
						
					
					
					<?php }elseif($tb_name =="products_tb_name"){ ?>
				
						<div class="modal fade hide" id="demoProducts">
								<div class="modal-header">
									<h4><?php _l('add_new_product'); ?></h4>
								</div>
								<div class="modal-body">
                                <form class="form-horizontal" method="post" action="<?php echo site_url('stocks/addproduct');?>">
                                    <fieldset>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="product_name" ><?php _l('product_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="product_name" id="product_name" class="input-xlarge" type="text" required />
                                        	</div>
                                    	</div>
										<div class="control-group">
                                        	<label class="control-label" for="description" ><?php _l('description'); ?></label>
                                        	<div class="controls">
                                        	<textarea name="description" id="description" class="input-xlarge" rows="5" required></textarea>
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="items_on_hand"><?php _l('items_on_hand'); ?></label>
                                        	<div class="controls">
                                        	<input name="items_on_hand" id="items_on_hand" class="input-xlarge" type="number" required />
                                        	</div>
                                    	</div>            
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </div>				
					<?php }elseif($tb_name == 'clients_tb_name'){?>
						
						<div class="modal fade hide" id="clientsModel">
								<div class="modal-header">
									<h4><?php _l('add_new_client'); ?></h4>
								</div>
								<div class="modal-body">
                                <form class="form-horizontal" method="post" action="<?php echo site_url('stocks/addclient');?>">
                                    <fieldset>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="client_name" ><?php _l('client_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="client_name" id="client_name" class="input-xlarge" type="text" required />
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="phone"><?php _l('phone'); ?></label>
                                        	<div class="controls">
                                        	<input name="phone" id="phone" class="input-xlarge" type="number" required />
                                        	</div>
                                    	</div>
										
										<div class="control-group">
                                        	<label class="control-label" for="email"><?php _l('email'); ?></label>
                                        	<div class="controls">
                                        	<input name="email" id="email" class="input-xlarge" type="email" required />
                                        	</div>
                                    	</div> 
										
										<div class="control-group">
                                        	<label class="control-label" for="shop_name"><?php _l('shop_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="shop_name" id="shop_name" class="input-xlarge" type="text" required />
                                        	</div>
                                    	</div> 
										
										<div class="control-group">
                                        	<label class="control-label" for="address" ><?php _l('address'); ?></label>
                                        	<div class="controls">
                                        	<textarea name="address" id="address" class="input-xlarge" rows="5" required></textarea>
                                        	</div>
                                    	</div>           
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </div>
					<?php } ?>
				
				
				
				
				
   <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>

									<tr>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php }?>
									</tr>
								</thead>

								<tbody>

							 
									  <?php if (isset($tb_data)){ 
									foreach ($tb_data as $data):  
									 if (isset($data['id'])){
                                            $id = $data['id'];
                                    }elseif(isset($data['phoneid'])){
                                            $id = $data['phoneid'];
                                    }else{
                                        $id = '';
                                    }?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php echo $data[$field];?></td>
                                        <?php endforeach; ?>
										
                                            <td>
                                                <?php if ($tb_name == 'stocks_tb_name'){ ?>
                                                <a onclick="editContent('<?php echo site_url('stocks/editStock/'.$data['product_name'].'/'.$data['user_id'].'/'.$data['phone_imei'].'/'.$data['stock_date']);?>');"  data-toggle="modal" href="#revealModal" style="text-decoration: none;padding-left: 40px;"><span class="icon-pencil" id="<?php echo site_url('stocks/editStock/'.$data['product_name'].'/'.$data['user_id'].'/'.$data['phone_imei'].'/'.$data['stock_date']);?>" ></span></a>
                                                <?php }else if($tb_name == 'products_tb_name'){?>
													<a onclick="editContent('<?php echo site_url('stocks/editProductUpdated/'.$data['product_name'].'/'.$data['user_id']);?>');" data-toggle="modal" href="#revealModal" style="text-decoration: none;padding-left: 40px;"><span class="icon-plus" id="<?php echo site_url('stocks/editProductUpdated/'.$data['product_name'].'/'.$data['user_id']);?>" ></span></a>
													
													<a onclick="editContent('<?php echo site_url('stocks/editProduct/'.$data['product_name'].'/'.$data['user_id']);?>');" data-toggle="modal" href="#revealModal" style="text-decoration: none;padding-left: 40px;"><span class="icon-pencil" id="<?php echo site_url('stocks/editProduct/'.$data['product_name'].'/'.$data['user_id']);?>" ></span></a>
													
													
												<?php } else if($tb_name == 'clients_tb_name'){ ?>
													<a onclick="editContent('<?php echo site_url('stocks/editClient/'.$data['client_name'].'/'.$data['user_id'].'/'.$data['shop_name']);?>');" data-toggle="modal" href="#revealModal" style="text-decoration: none;padding-left: 40px;"><span class="icon-pencil" id="<?php echo site_url('stocks/editClient/'.$data['client_name'].'/'.$data['user_id'].'/'.$data['shop_name']);?>" ></span></a>
												<?php }else if($tb_name == 'returned_stocks_tb_name'){ ?>
													<?php if($data['status']=='pending'){ ?>
													<a onclick="editContent('<?php echo site_url('stocks/returnStock/'.$data['product_name'].'/'.$data['user_id'].'/'.$data['phone_imei'].'/'.$data['stock_date']);?>');"  data-toggle="modal" href="#revealModal" style="text-decoration: none;padding-left: 40px;"><span class="icon-pencil" id="<?php echo site_url('stocks/returnStock/'.$data['product_name'].'/'.$data['user_id'].'/'.$data['phone_imei'].'/'.$data['stock_date']);?>" ></span></a>
													<?php } ?>
													
												<?php } ?>
											</td>
									</tr>
									<?php endforeach; 
}else {
	echo '<tr> <td> No data available <td> </tr>';
	}									
									?>
								</tbody>
	</table> 
          </section>

        </article>

   

    </div>

</section>
