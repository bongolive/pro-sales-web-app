<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Super_dashboard extends User_Controller {

	public $user_id;
	public $account_id;

	public function __construct() {

		parent::__construct();
		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('business_name', 'contact_person', 'mobile', 'email', 'status', );
		$this -> fields['ft_data'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js');

		if (!$this -> session -> userdata('logged_in') == TRUE) {

			$this -> session -> set_userdata('login_error', 'Please, Loggin first');

			redirect('login');
		}

		$this -> load -> model('super_dashmodel');

	}

	public function index() {

		$accounts = $this -> super_dashmodel -> read();
		if ($accounts) {
			 
		}

		$this -> load -> view('template/header', $this -> fields);
		$this -> load -> view('template/content/super_dashboard');
		$this -> load -> view('template/footer');

	}

}
