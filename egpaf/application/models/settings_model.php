<?php
class Settings_model extends MY_Model {
	public $table = 'settings';
	public $table_id = 'setting_id';

	function __construct() {
		parent::__construct();
	}

	function update($account_id, $data) {
		//do update data into database
		$this -> db -> where('account_no', $account_id);
		$qry = $this -> db -> update($this -> table, $data);
		return $qry;
	}

	function update_profile($account_id, $data) {
		//do update data into database
		$this -> db -> where('account_id', $account_id);
		$qry = $this -> db -> update('accounts', $data);
		return $qry;
	}

	public function get_surveys() {
		$this -> db -> select('*');
		$this -> db -> from('surveys');
		$surveys = $this -> db -> get() -> result_array();
		return $surveys;
	}

	public function read_settings($where = FALSE) {
		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		//$this->db->join('currency','currency_id = currency');
		$settings = $this -> db -> get() -> result_array();
		return $settings;
	}

	public function read_profile($where = FALSE) {
		$this -> db -> select('*');
		$this -> db -> from('accounts');
		if ($where) {
			$this -> db -> where($where);
		}
		$settings = $this -> db -> get() -> result_array();
		return $settings;
	}
    /*
   * get all settings to this user sna account
   * */
    public function api_get_all_settings($data)
    {
        $this->db->where($data);

        $query = $this->db->get('pro_settings');

        if($query->num_rows() > 0) {
            return $query->row_array();  // its usage should be $settings = $query['settings']
        }
    }


}
?>