<?php

$lang['email'] = 'Email';
$lang['address'] = 'Location';
$lang['city'] = 'City';
$lang['street'] = 'Street';
$lang['category'] = 'Category';

$lang['employee_id'] = "Employee Id";
$lang['staff_no'] = "Staff No";
$lang['department'] = "Department";
$lang['location'] = "Location";
$lang['date_of_emp'] = "Date of Employee";
$lang['firstname'] = "Firstname";
$lang['lastname'] = "Lastname";
$lang['gender'] = "Gender";
$lang['middle_name'] = "Middle Name";
$lang['mobile'] = "Mobile";
$lang['email'] = "Email";
$lang['account_id'] = "Account Id";

$lang['question_id'] = 'Qn No';
$lang['question_text'] = $lang['question'] = 'Question';
$lang['question_type'] = 'Question Type';
$lang['answer_choices'] = 'Answers Choices';
$lang['category'] = 'Category';
$lang['survey_no'] = 'Survey';
$lang['question_rank'] = $lang['rank'] = 'Question Rank';
$lang['language'] = 'Language';
$lang['account_id'] = 'Account No';
$lang['survey_title'] = 'Survey Name';
$lang['new_question'] = "New Question";
$lang['questionaires_list'] = "Questionaires List";

//accounts

$lang['account_id'] = 'Ac.No';
$lang['account_title'] = 'Account_Title';
$lang['organisation'] = 'Organisation';
$lang['contact_person'] = 'Contact Person';
$lang['contact_person_title'] = 'Contact Person Title';
$lang['mobile'] = 'Mobile';
$lang['email'] = 'Email';
$lang['status'] = 'Status';

//location
$lang['locations'] = 'Locations';
$lang['location_id'] = 'Location No';
$lang['location_code'] = 'Location Code';
$lang['location_name'] = 'Location Name';
$lang['location_type'] = 'Location Type';
$lang['locations_list'] = 'Locations List';
$lang['location_status'] = 'Status';
$lang['location_area'] = 'Area';
$lang['location_street'] = 'Street';
$lang['location_city'] = 'City'; 
$lang['create_location'] = 'New Location';
$lang['contact_person'] = 'Contacts person';
$lang['location_level'] = 'Location Level';
$lang['location_country'] = 'Country';
$lang['new_location'] = 'New Location';
$lang['location_details'] = 'Location Details';
$lang['location_mobile'] = 'Mobile';
$lang['location_email'] = 'Email';

//questions
$lang['question_id'] = 'Qn No.';
$lang['question'] = 'Question';
$lang['answer_choices'] = 'Answers Choises';
$lang['rank'] = 'Rank';
$lang['question_details'] = 'Question Details';
$lang['questionaire'] = 'Questionaires';
$lang['questionaires_tb_name'] = 'questionaires List';
$lang['new_question'] = 'New Question';
$lang['update_question'] = 'Update';
$lang['questionaires'] = "Questionaires";
//responses

$lang['responder_id'] = 'Resp.No';
$lang['responder_mobile'] = 'Mobile';
$lang['response_date'] = 'Date';
$lang['last_question'] = 'Last Qn.';

//SETTINGS
$lang['active_survey'] = 'Active Survey';
$lang['sms_report'] = 'Send SMS report';
$lang['settings'] = 'Settings';
$lang['account_settings'] = 'Account Settings';

//surveys
$lang['survey_title'] = 'Survey Title';
$lang['descriptions'] = 'Descriptions';
$lang['key_word'] = 'Key Word';
$lang['no_questions'] = 'Questions';
$lang['cap_size'] = 'CapSize';
$lang['survey_id'] = 'No';
$lang['surveys_list'] = 'Surveys List';
$lang['survey_no'] = 'Survey No';
$lang['surveys'] = 'Surveys';

//users
$lang['user_id'] = 'No';
$lang['people'] = "People";
$lang['dob'] = "Date of Birth";
$lang['fullname'] = 'Employee Name';
$lang['lastname'] = 'Lastname';
$lang['location'] = 'Location';
$lang['username'] = 'Username';
$lang['password'] = 'Password';
$lang['conf_password'] = 'Confirm Password';
$lang['role'] = 'Role';
$lang['user_details'] = 'User Details';
$lang['add_user'] = 'New User';
$lang['permissions'] = 'Permissions';
$lang['role_title'] = 'Role Title';
$lang['conf_pass'] = 'Confirm Password';

//other
$lang['surveys_tb_name'] = $lang['surveys_list'] = 'Survey List';
$lang['new_survey'] = 'New Survey';
$lang['surveys'] = 'Surveys';
$lang['surveys_create'] = 'New Survey';
$lang['responce_status'] = 'Response Status';

$lang['back'] = 'Back';
$lang['survey reports'] = 'Survey Report';
$lang['update_location'] = 'Update Location';
$lang['incomplete_reminders'] = 'Incomplete Reminders Time';
$lang['surveys_report'] = 'Survey Report';
$lang['survey_key_word'] = 'Key Word';
$lang['survey_details'] = 'Survey Details';
$lang['new_surveys'] = 'New Survey';
$lang['sender_number'] = 'Sender Number';
$lang['average_score'] = 'Average Score';
$lang['reminders_per_survey'] = 'Reminders per Survey';
$lang['null_value_symbol'] = 'Empty Value Symbol';
$lang['location_reports'] = 'Send Location Reports SMS';
$lang['score'] = 'Score';
$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['update_survey'] = 'Updating';
$lang['employees'] = 'Employees';
$lang['new_employee'] = 'New Employee';
$lang['users'] = 'Users';

$lang['create_location'] = 'New Location';
$lang['location_list'] = 'Locations List';
$lang['reports'] = 'Reports';
$lang['new_employee'] = 'New Empleyee Details';
$lang['department'] = 'Department';
$lang['employee_details'] = 'Employee Details';

$lang['Back'] = 'Back';
$lang['report'] = 'Reports';
$lang['questions_list'] = 'Questions List';
$lang['employees_list'] = 'Employees List';
$lang['staff_no'] = 'Staff No';
 
$lang['active_survey'] = 'Active Survey';
$lang['skip_value'] = 'Skip Value';
$lang['default_language'] = 'Default Language';
$lang['sms_report'] = 'Send SMS Reports';
 
$lang['survey_reports'] = 'Survey Reports';
$lang['print'] = 'Export';
$lang['edit'] = 'Edit';
$lang['match_with'] = 'Match Question';
$lang['next_question'] = 'Next Question';
$lang['response_text'] = 'Respinse Text';
$lang['no'] = 'No.';
$lang['survey_title'] = 'Survey Title';
$lang['survey_report'] = 'Survey Report';
$lang['locations_report'] = 'Locations Report';
$lang['surveyor_report'] = 'Surveyor Report';

$lang['location_report'] = 'Location Report';

$lang['device_imei'] = 'Device Imei';
$lang['action'] = 'Action';
$lang['surveyor_name'] =  $lang['employee_name'] = 'Surveyor  Name';
$lang['view'] = 'View';
$lang['device_status'] = 'Device Status';
$lang[''] = '';

$lang['this_week_count'] = 'This Week Count';
$lang['this_month_count'] = 'This Month Count';
$lang['total_count'] = 'Total Count';
$lang['count_today'] = 'Count Today';
 

$lang['section_title'] = 'Section Title';
$lang['section_code'] = 'Section Code';
$lang['account_no'] = 'Account No';
$lang['new_section'] = 'New Section';
$lang['survey_descriptions'] = 'Descriptions';
$lang['new_section'] = 'New Section';
$lang['survey_sections'] = 'Survey Sections';
$lang['new_survey_sections'] = 'New Survey Section';
$lang['back_to_surveys'] = 'Back To Surveys';
$lang['survey_section'] = 'Survey Section';
$lang['translate_question'] = 'Translate Question';
$lang['language'] = 'Language';

$lang['question_text_translation'] = 'Translate';
$lang['translation'] = 'Translation';
$lang['surveyors_report'] = 'Surveyors Reports';
$lang['location_reports'] = 'Location Reports';
$lang['delete'] = 'Delete';
$lang['interview_date'] = 'Interview Date';
 
$lang['participant_no'] = 'Participant No';
$lang['interviewer_no'] = 'Interviewer NO';
$lang['start_time'] = 'Start Time';
$lang['end_time'] = 'End Time';
$lang['questionaire_responses'] = 'Questionaire Responses';
$lang['qn_numbering'] = 'Question Numbering';
$lang['filtering'] = 'Filter Data';
$lang['pmtcp_status'] = 'PMTCP Status';
$lang['location'] = 'Location';
$lang['dates'] = 'Date Range';
$lang['view_all'] = 'View All';
$lang['assigned_to'] = 'Assigned To';
 $lang['device_user'] = 'Device User';
$lang['device_mobile'] = 'Mobile'; 
$lang['device_descr'] = 'Descriptions';
$lang['devices_list'] = 'Device List';
$lang['devices'] = 'Devices';
$lang['new_devices'] = 'New Device';

$lang['reset'] = 'Reset';
$lang['back_to_devices'] = 'Back';
$lang['user_device'] = 'Assigned Device';
$lang['total_accounts'] = 'Total Accounts';
$lang['active_accounts'] = 'Active Accounts';
$lang['inactive_accounts'] = 'Inactive Accounts';
$lang['accounts'] = 'Accounts';
$lang['accounts_list'] = 'Accounts List';
$lang['new_account'] = 'New Account';
$lang['account_details'] = 'Account Details';
$lang['account_type'] = 'Account Type';
$lang['user_limit'] = 'User Limit';
$lang['contact_person'] = 'Contact Person';
$lang['total_response'] = 'Total Responses';
$lang['allow_web_access'] = 'Allow Web Access';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';