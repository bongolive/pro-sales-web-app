				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							
							<h2><?php
							if (!$info) {
								_l('new_products');
							} else {
								echo _l('update_product') . ' : ' . strtoupper($info['product_name']);
							}
							 ?></h2>
					   
            </header> 

        </article>

    </div>	
    <div class="row">
    	 
    	 <article class="span8 data-block">
    	 	 
								<div class="modal-header">
									<h4><?php _l('add_new_product'); ?></h4>
								</div>
								<div class="" style="max-height: auto;">
									<?php
								 
									if ($info) {
										$action = 'products/update_product';
									} else {
										$action = 'products/new_products';
									};
								?>
											 
                                <form class="form-horizontal" method="post" action="<?php echo site_url($action); ?>">
                                	<?php if($info){ ?>
                                		<input name="product_id" id="product_id" class="input-large" type="hidden"  value="<?php  echo $info['product_id']; ?>" />
                                		<?php } ?> 
                                    <fieldset>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="product_name" ><?php _l('product_name'); ?></label>
                                        	<div class="controls"><?php echo form_error('product_name'); ?>
                                        	<input name="product_name" id="product_name" class="input-large" type="text"  value="<?php
											if ($info) { echo $info['product_name'];
											} else { echo set_value();
											}
 ?>" required />
                                        	</div>
                                    	</div>
										<div class="control-group">
                                        	<label class="control-label" for="product_category" ><?php _l('product_category'); ?></label>
                                        	<div class="controls">
                                        		<?php
												$class = ' class="input-large"';
												if ($categories == FALSE) {

													$categories = array('' => 'No Category Specified');
												}

												if ($info) {$value = $info['package_type'];
												} else { $value = set_value();
												}
												echo form_dropdown('product_category', $categories, $value, $class);
                                        		?>
                                        		 <?php echo form_error('product_category'); ?>
                                        	</div>
                                    	</div>
                                    	
                                    	 
                                        <div class="control-group">
                                        	<label class="control-label" for="package_type"><?php _l('package_type'); ?></label>
                                        	<div class="controls">
                                        		
                                        		<?php
												if ($packages == FALSE) {

													$packages = array('' => 'No Packages Type Defined');
												}

												if ($info) {$value = $info['package_type'];
												} else { $value = set_value();
												}
												echo form_dropdown('package_type', $packages, $value);
                                        		?>
                                        		<?php echo form_error('package_type'); ?> 
                                        	</div>
                                    	</div>   
                                    	 
                                    		<div class="control-group">
                                        	<label class="control-label" for="package_units" ><?php _l('package_units'); ?></label>
                                        	<div class="controls">
                                        		<?php echo form_error('package_units'); ?>
                                        	<input name="package_units"  placeholder="0" id="description" class="input-large" type="number" value="<?php
											if ($info) { echo $info['package_units'];
											} else { echo set_value();
											}
 ?>"required>
                                        	</div>
                                        </div>
                                        	
                                        		<div class="control-group">
                                        	<label class="control-label" for="container_type" ><?php _l('container_type'); ?></label>
                                        	<div class="controls">
                                        		<?php echo form_error('container_type');
												
												if(!$containers){
													$containers = array(""=>"No Containers Specified"); 
												}
												if ($info) { $value =  $info['container_type'];
											} else { $value = set_value();
											}
												echo form_dropdown('container_type',$containers,$value);
												
												 ?>
                                        	 
                                        	</div>
                                        </div>
                                        
                                            	<div class="control-group">
                                        	<label class="control-label" for="re_order_value" ><?php _l('starting_quantity'); ?></label>
                                        	<div class="controls">
                                        		<?php echo form_error('starting_quantity'); ?>
                                        	<input name="starting_quantity" id="description"  placeholder="0" class="input-large" type="number" value="<?php
											if ($info) { echo $info['items_on_hand'];
											} else { echo set_value();
											}
 ?>"required>
                                        	</div>
                                        	</div>
                                        	
                                        
                                        
                                        	<div class="control-group">
                                        	<label class="control-label" for="re_order_value" ><?php _l('re_order_value'); ?></label>
                                        	<div class="controls">
                                        		<?php echo form_error('re_order_value'); ?>
                                        	<input name="re_order_value" id="description" placeholder="0" class="input-large" type="number" value="<?php
											if ($info) { echo $info['re_order_value'];
											} else { echo set_value();
											}
 ?>"required>
                                        	</div>
                                        	</div>
                                        	
                                        	<div class="control-group">
                                        	<label class="control-label" for="unit_size" ><?php _l('unit_size'); ?></label>
                                        	<div class="controls">
                                        		<?php echo form_error('unit_size'); ?>
                                        <div class="form-controls">
<div class="input-append">
<input id="appendedPrependedInput"  name="unit_size" placeholder="Size"  class="input-small" type="number" value="<?php
if ($info) { echo $info['unit_size'];
} else { echo set_value();
}
 ?>">

<?php
if ($info) {  $value = $info['unit_measure'];
} else { $value = set_value();
}
$units[''] = "Choose Units";
$units = array("" => "Measure", "Kg" => "Kg", "Mg" => "Mg", "G" => "G", "Ton" => "Ton", "Lbs" => "Lbs", "Oz" => "Oz", "Ml" => "Ml", "L" => "L", "Fl Oz" => "Fl Oz", "Pint" => "Pint", "Quart" => "Quart", "Gallon" => "Gallon", "Cm" => "Cm", "M" => "M", "In" => "In", "Ft" => "Ft", "Yds" => "Yds", "Cm3" => "Cm3", "M2" => "M2", "C2" => "C2", "Ft2" => "Ft2");
$attr = 'class="input-small"';
echo form_dropdown('unit_measure', $units, $value, $attr);
 ?>
                                        	</div>
                                        	</div>
                                        	</div>
                                        	</div>
                                        	
                                       
                                    		<div class="control-group">
                                        	<label class="control-label" for="sku_product_no" ><?php _l('sku_product_no'); ?></label>
                                        	<div class="controls"><?php echo form_error('sku_product_no'); ?>
                                        	<input name="sku_product_no" id="description" class="input-large"  placeholder="0" type="text" value="<?php
											if ($info) {  echo $info['sku_product_no'];
											} else { echo set_value();
											}
											?>" required>
                                        	</div>
                                    	</div>
                                    	<div class="control-group">
                                        	<label class="control-label" for="tax_rate" ><?php _l('tax_rate'); ?></label>
                                        	<div class="controls"><?php echo form_error('tax_rate'); ?>
                                        	<?php
											if ($info) { $tax = $info['tax_rate'];
											} else { $tax = set_value();
											}
											$vat = array("18" => "18%", '10' => "10%", "0" => "0%");
											echo form_dropdown('tax_rate', $vat, $tax);
                                        	?>
                                        	 
                                        	</div>
                                    	</div>
                                    	<div class="control-group">
                                        	<label class="control-label" for="unit_sale_price" ><?php _l('unit_sale_price'); ?></label>
                                        	<div class="controls"><?php echo form_error('unit_sale_price'); ?>
                                        	<input name="unit_sale_price"  placeholder="0" id="description" class="input-large" type="number" value="<?php
											if ($info) { echo $info['unit_sale_price'];
											} else { echo set_value();
											}
 ?>"  required> <p class="help-inline"><i>Before Tax</i></p>
                                        	</div>
                                    	</div>
                                    	  
                                    	  <div class="control-group">
                                        	<label class="control-label" for="descriptions" ><?php _l('descr/specs'); ?></label>
                                        	<div class="controls"><?php echo form_error('descriptions'); ?>
                                        	<textarea name="descriptions" id="descriptions"   type="text"><?php
											if ($info) { echo $info['description'];
											} else { echo set_value();
											}
 ?></textarea>
                                        	</div>
                                    	</div>
                                    	         
								</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('products'); ?>" class="btn btn-alt" data-dismiss="modal">Back</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>


                        	 <article class="span4 data-block">
								<div class="modal-header">
									<h4><?php _l('add_new_category'); ?></h4>
								</div>
								<?php
								if($this->session->userdata('csettings')==TRUE){ ?>
									<div class="alert alert-success fade in">
<button class="close" data-dismiss="alert">×</button>
<strong>Success!</strong>
Product category succefuly added
</div>
									<?php
									}
								?>
								
								<div class="modal-body">
								 
                                	<form class="form-inline no-margin" method="post" action="<?php echo site_url('products/add_category'); ?>">
  
                                 <input name="from_products" value="true" type="hidden" />
                                 
                                    <fieldset>
  
                                        <div class="control-group">
                                        	<label class="control-label" for="category_name" ><?php _l('category_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="category_name" id="category_name" value="<?php 	 echo set_value('category_name');  ?>" class="input-large" type="text" required />
                                        	</div>
                                    	</div>
										 
								</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('products/categories'); ?>" class="btn btn-alt" data-dismiss="modal">Clear</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>
                            	 <article class="span4 data-block">
								<div class="modal-header">
									<h4><?php _l('add_new_package'); ?></h4>
								</div>
									<?php
								if($this->session->userdata('psettings')==TRUE){ ?>
									<div class="alert alert-success fade in">
<button class="close" data-dismiss="alert">×</button>
<strong>Success!</strong>
Product Package succefuly added
</div>
									<?php
									}
								?>
								<div class="modal-body">
						 
                             	<form class="form-inline no-margin" method="post" action="<?php echo site_url('products/add_packages'); ?>">
                                	 
                                    <fieldset>
                                            <input name="from_products" value="true" type="hidden" />
                                        <div class="control-group">
                                        	<label class="control-label" for="package_name" ><?php _l('package_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="package_name" id="package_name" class="input-large" value="<?php echo set_value('package_name');
												 
											?>" type="text" required />
                                        	</div>
                                    	</div>
										    
								</div>
								<div class="modal-footer">
									<div class="form-actions">
										<a href="<?php echo site_url('products/packages'); ?>" class="btn btn-alt" data-dismiss="modal">Clear</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>
                                	 <article class="span4 data-block">
								<div class="modal-header">
									<h4><?php _l('add_new_container'); ?></h4>
								</div>
									<?php
								if($this->session->userdata('contsettings')==TRUE){ ?>
									<div class="alert alert-success fade in">
<button class="close" data-dismiss="alert">×</button>
<strong>Success!</strong>
Product Container succefuly added
</div>
									<?php
									}
								?>
								<div class="modal-body">
						 
                             	<form class="form-inline no-margin" method="post" action="<?php echo site_url('products/add_container'); ?>">
                                	 
                                    <fieldset>
                                            <input name="from_products" value="true" type="hidden" />
                                        <div class="control-group">
                                        	<label class="control-label" for="container_name" ><?php _l('container_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="container_name" id="container_name" class="input-large" value="<?php echo set_value('container_name');
												 
											?>" type="text" required />
                                        	</div>
                                    	</div>
										    
								</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('products/containers'); ?>" class="btn btn-alt" data-dismiss="modal">Clear</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>

                        </div>                        	
                        </div>	
                        </section>
                     <?php
                     	$psettings = array('psettings' => '', 'csettings' => '');
			 
$this->session->unset_userdata($psettings);
                     ?>   		
					 