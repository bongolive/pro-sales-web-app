                    <div class="modal-header">

									<h4><?php _l('update_client'); ?></h4>

								</div>

                                <form class="form-horizontal" action="<?php echo site_url("stocks/updateClient"); ?>" method="post">
									 <?php foreach ($clients as $client): ?>
										<fieldset>
                                        
                                       <div class="control-group">
                                        	<label class="control-label" for="shop_name"><?php _l('shop_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="shop_name" id="shop_name" class="input-xlarge" type="text" value="<?php echo $client['shop_name'];?>" required />
                                        	</div>
                                    	</div> 
                                       
                                        <div class="control-group">
                                        	<label class="control-label" for="client_name" ><?php _l('client_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="client_name" id="client_name" class="input-xlarge" type="text" value="<?php echo $client['client_name'];?>" required />
                                        	<input name="userid" type="hidden" id="userid" value="<?php echo $client['user_id'];?>" />
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="phone"><?php _l('phone'); ?></label>
                                        	<div class="controls">
                                        	<input name="phone" id="phone" class="input-xlarge" type="number" value="<?php echo $client['phone'];?>" required />
                                        	</div>
                                    	</div>
										
										<div class="control-group">
                                        	<label class="control-label" for="email"><?php _l('email'); ?></label>
                                        	<div class="controls">
                                        	<input name="email" id="email" class="input-xlarge" type="email" value="<?php echo $client['email'];?>" />
                                        	</div>
                                    	</div>
										
										<div class="control-group">
                                        	<label class="control-label" for="address" ><?php _l('address'); ?></label>
                                        	<div class="controls">
                                        	<textarea name="address" id="address" class="input-xlarge" rows="5"><?php echo $client['address'];?></textarea>
                                        	</div>
                                    	</div>           
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
									<?php endforeach; ?>
                        </form>
