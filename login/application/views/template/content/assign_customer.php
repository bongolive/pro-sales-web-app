				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000; background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('assign_customer');
							echo ": " . strtoupper($team['employee_name']);
 ?></h2> 
					 
						 
						 <a class="btn btn-alt btn-primary" data-toggle="modal"
href="<?php echo site_url('customers/sales_team'); ?>" ;"="" style="float: right; padding: 8px;">Back</a>
            			</header> 
       				 </article> 
    </div>	
    <div class="row">
     						
  
    	 <article class="span12 data-block">
					
								<div class="modal-header">
									<h4><?php _l('add_sales_team_customers'); ?></h4>
								</div>
								<div class="modal-body">
							
							<?php
							echo form_open('customers/add_customers');
							?>
							
							<input type="hidden" name="sales_team" value="<?php echo $team['id']; ?>"/>
							<input type="hidden" name="device_imei" value="<?php echo $team['device_imei']; ?>"/></td>
								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		 
								<tbody>
 				  <?php if ($tb_data){
									foreach ($tb_data as $data):  
								 
                                            $id = $data[$table_id];
                                     ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
										<td><input type="checkbox" name="customer[]" value="<?php echo $data[$table_id]; ?>"/></td>
                                 
	                                        	<td> 
	                                        		 <dl>
	                                        		 	 <dt><?php echo $data['contact_person']; ?></dt><dd><?php echo $data['business_name']; ?> </dd>
	                                        		 </dl> 
												</td>
												<td> 
	                                        		 <dl>
	                                        		 	 <dt><?php _l('contacts') ?></dt>
	                                        		 	 <dd><b><?php _l('mobile'); echo ":</b> ".$data['mobile'].", <b>"; _l('email'); echo "</b> :".$data['email']; ?> </dd>
	                                        		 	   <dd><?php _l('address');echo ": ". $data['street'] ." - ". $data['city']; ?> </dd>
	                                        		 </dl> 
												</td>
                                        	</tr>
                                        	
                                        	<?php endforeach;}else{ ?>
                                        	<tr><td>No Customers to assign</td></tr>	
                                        <?php	} ?>
                                        	</tbody>
                                        	</table>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url($controller); ?>" class="btn btn-alt" data-dismiss="modal"><?php _l('back') ?></a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit"><?php _l('assign'); ?></button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>
                        </div>                        	
                        </div>	
                        </section>
                        		
					 
