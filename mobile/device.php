<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
<link rel="stylesheet" href="bx_styles.css" type="text/css" />
<link rel="stylesheet" href="media-quries-resposive.css" type="text/css"  />
<title>footprint</title>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script src="js/jquery.bxSlider.js" type="text/javascript"></script>
<script src="js/jquery.bxSlider.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<link href="facebox.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="js/facebox.js" type="text/javascript"></script> 

<script type="text/javascript">
  $(document).ready(function(){
  
  $('a[rel*=facebox]').facebox({ 'width' : 300});
 
    $(".deleteitem").click(function(){
		var x=window.confirm("Are you sure want to delete this device?")
		if (x){			
			var parent = $(this).closest('TR');
			var id = parent.attr('id');
			var uid = parent.attr('uid');
			$.ajax({
				type: 'POST',
				data: 'id=' +id+"&uid="+uid,
				url: 'deletedevice.php?XDEBUG_SESSION_START=dk',
				success: function(msg){
					$('#'+id).remove();
				}
				});
		}
	});

	 $('.slider').bxSlider({
    auto: true,
    pager: true
  });
  });
</script>

</head>

<body>
<?php session_start();
include('common.php');
?>
		<div class="wrapper-center">
    <div class="mobile_wrapper">
	<div class="body_class">
    	<div class="center_header_page">
        	<div class="header-left">
            	<a href="index.php" ><img src="images/logo3.png" alt=""  /></a>
            </div><div class="header-right">
            	<span style="font-family:Arial; font-size:14px; color:#FFFFFF; margin-top:8px; float:left">Welcome <?php echo $_SESSION['username'];?> <br /><a href="logout.php" style="color:#FFFFFF">Logout</a></span>
            </div>
            
        </div>
	</div>
	    
   
    <div class="line">
    </div>
    <div class="center_header_page1">
      <div class="content" >
        
            <div class="content_first content_sec_heading">
                
        <div class="content_thirdlog"><!--content_third-->
         	<div class="content_thirdlog_lefttable">
<table width="100%" align="left" cellpadding="0" cellspacing="0" border="1" bordercolor="#ffffff">
<tr style="font-family: Calibri; color:#000000; font-size:14px; text-align:center; background-color:#CCCCCC ; height:50px;">
<td>Device Name</td>
<td>Tracking Interval</td>
<td>Edit</td>
<td>Delete</td>
</tr>
<?php
include ("connection.php");

$sql="SELECT * FROM phones where phoneid in (select phoneid from phones_users where userid = ".$_SESSION['userid'].")";
$result = mysql_query($sql);
while($row = mysql_fetch_array($result))
  {
  $int = "";
  if($row['checking_interval'] < 1){
  $int = "Stop";
  }
  else{
  $int = $row['checking_interval'];
  }
echo "<tr id='{$row['phoneid']}' uid='{$_SESSION['userid']}' style='font-family: Calibri; color:#000000; font-size:16px; text-align:center; background-image:url(images/bgupper.png); background-repeat:repeat ; height:70px;'>";
echo "<td>{$row['device_name']}</td>";
echo "<td>{$int}</td>";
echo "<td><a rel='facebox' href='editdevice.php?id={$row['phoneid']}'><img src='images/EDIT.png' /></a></td>";
echo "<td><a href='javascript:void(0);' class='deleteitem'><img src='images/deletebutton.png' /></a></td>";
echo "</tr>";
}
?>
<!--
<tr style="font-family: Calibri; color:#000000; font-size:13px; text-align:center; background-image:url(images/bg.png); background-repeat:repeat-x">
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td><img src="images/EDIT.png" /></td>
<td><img src="images/deletebutton.png" /></td>
</tr>
<tr style="font-family: Calibri; color:#000000; font-size:13px; text-align:center;  background-image:url(images/bg.png); background-repeat:repeat-x">
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td><img src="images/EDIT.png" /></td>
<td><img src="images/deletebutton.png" /></td>
</tr>
<tr style="font-family: Calibri; color:#000000; font-size:13px; text-align:center; background-image:url(images/bg.png); background-repeat:repeat-x;">
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td><img src="images/EDIT.png" /></td>
<td><img src="images/deletebutton.png" /></td>
</tr>
-->
</table>
					
          </div><a rel="facebox" href="devicepop.php">
          <div class="content_third_left_buttonagain"><span style="color:#FFFFFF">Add Device</span>
                        </div>
				</a>
         	
			 <div id="dialog"></div>          
            <div class="clear"></div>
			
         </div><!--content_third-->
            </div>
      </div>
    
</div>
    <div class="footer-wrapper">
    	<div class="center_footer_page">
        	<div class="footer_center">
            	<div class="footer_navi">
               	<a  href="device.php" >Device</a>
				<a href="track.php" >Track</a>
                <a href="support.php" >Support</a>
                <a href="logout.php">Logout</a>
               
                </div>
            </div>
         <div class="clear"></div>
        </div>
    </div>
    <div class="center_header_page">
    	<div class="footer_buttom">
    	<h3>FOOT PRINT</h3><p>Copyright &copy; 2012. All Rights Reserved.</p>
        </div>
         <div class="clear"></div>
    </div>
    </div>
    </div>
	<script type="text/javascript">
	
</script>
<script src="js/analytics.js" type="text/javascript"></script>
</body>
</html>
