<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Customers extends User_Controller {

	public $user_id;
	public $account_id;

	public function __construct() {
		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('business_name', 'contact_person', 'mobile', 'email', 'status', 'device_imei');
		parent::__construct();
		$this -> fields['ft_data'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js');

		if (!$this -> session -> userdata('logged_in') == TRUE) {
		 $this -> session -> set_userdata('login_error', 'Please, Loggin first');
		 redirect('login');
		 }

		$this -> load -> model('customers_model');
		$this -> load -> model('sales_orders_model');
		$this -> customers_model -> form_validation();
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');
		$this -> fields['view'] = 'customers/view/';
		$this -> fields['edit'] = 'customers/update/';
		$this -> fields['edit_agent'] = 'customers/update_agent/';
		$this -> fields['delete'] = 'customers/delete/';

		$this -> fields['table_id'] = $this -> customers_model -> table_id;

		$this -> fields['tb_name'] = 'all_customers';
		$this -> fields['controller'] = 'customers';
		$this -> user_id = $this->session->userdata('user_id');

		//var_dump($this->session->all_userdata() );exit;

		$this -> account_id = $this->account_id;

		$this->load->model(array('accounts_model', 'employees_model', 'devices_model'));
		//filtering the property items according to companyies

		/******************** check user permissions ***************************/

		$modules = explode(',', $this -> session -> userdata('permissions'));
		//$this -> fields['is_allowed'] = $this -> permissions_model -> read_modules($modules);

		/******************** check user permissions ***************************/
	}

	public function index() {

		$this -> fields['controller'] = 'customers';

		$where = array('account_id' => $this -> account_id);
		if ($this -> input -> post('filter')) {
			extract($_POST);
 
			if ($business_name) {
				$where['customer_id'] = $business_name;
			}

		}

		$customers = $this -> customers_model -> read($where);
		if ($customers) {
			$this -> fields['tb_data'] = $customers;
			foreach ($customers as $business) {
			$bs_ness[$business['customer_id']] = $business['business_name'];
		}
		 
		$this -> fields['business'] = $bs_ness;
		$this -> session -> unset_userdata('id');
} else { $this -> fields['business'] = FALSE; $this -> fields['tb_data'] =  FALSE;
		}
		//checking users permissions
		$this -> load -> view('template/header', $this -> fields);
		$this -> load -> view('template/content/customers_table');
		$this -> load -> view('template/footer');
	}

	public function customerlist($id = 0) {

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('employee_name', 'device_imei', 'mobile', 'email', 'emp_status');

		$this -> fields['controller'] = 'customers';
		$where = array('pro_employees.account_id' => $this -> account_id );
		if ($this -> input -> post('filter')) {
			extract($_POST); 
			if ($business_name) {
				$where['customer_id'] = $business_name;
			}

		}
		$this -> fields['table_id'] = 'id';
		$customers = $this -> customers_model -> read_sales_team($where);
		//echo $this->db->last_query();
		if ($customers) {
			$this -> fields['tb_data'] = $customers;
			//foreach ($customers as $business) {
			//$bs_ness[$business['customer_id']] = $business['business_name'];
			//}
		 
		//$this -> fields['business'] = $bs_ness;
			$this -> fields['business'] = '';
		$this -> session -> unset_userdata('id');
} else { $this -> fields['business'] = FALSE; $this -> fields['tb_data'] =  FALSE;
		}
		//checking users permissions
		$this -> load -> view('template/header', $this -> fields);
		$this -> load -> view('template/content/customers_table_agent');
		$this -> load -> view('template/footer');

	}
	//viewing
	public function view() {

		$this -> fields['cust_headers'] = array('contact_person', 'business_name',  'mobile', 'email','area', 'street', 'city','status');

		$id = $this -> uri -> segment(4);

		$where = array('customer_id' => $id);
		//geting the data
		$emp = $this -> customers_model -> read($where);
		if ($emp) {
			foreach ($emp as $emp) {
				$this -> fields['customer'] = $emp;
			}
		} else {
			$this -> fields['customer'] = FALSE;
		}

		//get customer orders

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('order_date', 'sales_total', 'total_tax', 'payment_status', 'paid_amount', 'receipt_no', );
		$orders = $this -> sales_orders_model -> read($where);

		if ($orders) {
			foreach ($orders as $orders) {
			}
			$this -> fields['tb_data'] = $orders;
		} else {
			$this -> fields['tb_data'] = FALSE;
		}
		$this -> fields['controller'] = 'customers';
		$this -> load -> view('template/header', $this -> fields);
		$this -> load -> view('template/content/customer_view', $this -> fields);
		$this -> load -> view('template/footer');

	}

	public function new_customer() {

		if ($this -> form_validation -> run() == FALSE) {
			$this -> fields['info'] = FALSE;

			//checking users permissions
			$this -> load -> view('template/header', $this -> fields);
			$this -> load -> view('template/content/customers_form', $this -> fields);
			$this -> load -> view('template/footer');
		} else {

			extract($_POST);
			$date = date('Y-m-d h:i:s');
			$data = array('contact_person' => $contact_person, 'cp_title' => $cp_title, 'business_name' => $business_name, 'service_type' => $service_type, 'service_category' => $service_category, 'device_imei' => '', 'mobile' => $mobile, 'email' => $email, 'street' => $street, 'area'=> $area, 'city' => $city, 'lat' => $lat, 'longtd' => $longtd, 'account_id' => $this -> account_id, 'created_on' => $date,  'created_by' => $this -> user_id, 'status' => 1, 'is_synced' => 0);

			//print_r($data);

			$this -> customers_model -> save($data);

			redirect('customers');

		}

	}

	public function new_customer_agent() {

		if ($this -> form_validation -> run() == FALSE) {
			$this -> fields['info'] = FALSE;

			//checking users permissions
			$this -> load -> view('template/header', $this -> fields);
			$this -> load -> view('template/content/customers_form_agent', $this -> fields);
			$this -> load -> view('template/footer');
		} else {
			
			extract($_POST);
			
			$date = date('Y-m-d h:i:s');
			
			$this->db->trans_start();
			
			$account_data = array('contact_person' => $contact_person, 'company_name' => $business_name, 'mobile' => $mobile, 'email' => $email,'street' => $street,  'area'=> $area, 'city' => $city, 'created_by' => $this -> account_id, 'vat_no'=>0, 'tin_no'=>0, 'user_accounts'=>0, 'user_id'=>$this->user_id,'expire_date'=>'0000-00-00');	
			
			$this -> accounts_model -> save( $account_data );
			//echo $this->db->last_query();exit;
			$accId = $this->db->insert_id();
	
			$name = explode(' ', $contact_person);
			$lastname = (array_key_exists(1, $name)) ? $name[1] : 'N/A';
			$employee_data = array('firstname'=>$name[0], 'lastname'=>$lastname, 'gender'=>'N/A', 'emp_role'=>'sales','mobile'=>$mobile, 'email'=>$email, 'created_by'=>$this -> user_id , 'created_on'=>$date, 'account_id' => $this -> account_id, 'emp_status' => 0,'commission'=>0);
	
			$this -> employees_model -> save( $employee_data );
			//echo $this->db->last_query();exit;
			$assignedTo = $this->db->insert_id();

			$device_data = array('device_imei'=>$imei, 'device_descr'=>'N/A', 'assigned_to'=>$assignedTo, 'created_on'=>$date, 'created_by' => $this -> user_id,  'account_id'=>$this -> account_id, 'device_status'=>0, 'device_mobile'=>$mobile);
			$this -> devices_model -> save( $device_data );
			//echo $this->db->last_query();exit;
			$this->db->trans_complete();
			
			redirect('customers/customerlist');

		}

	}
	public function update() {

		if ($this -> form_validation -> run() == FALSE) {
			if ($this -> session -> userdata('id')) {
				$id = $this -> session -> userdata('id');
			} else {
				$id = $this -> uri -> segment(4);
				$this -> session -> set_userdata('id', $id);
			}
			$where = array('customer_id' => $id);
			//geting the data
			$info = $this -> customers_model -> read($where);

			foreach ($info as $info) {
				$this -> fields['info'] = $info;
			}

			$this -> load -> view('template/header', $this -> fields);
			$this -> load -> view('template/content/customers_form', $this -> fields);
			$this -> load -> view('template/footer');

		} else {

			extract($_POST);
			$date = date('Y-m-d h:i:s');
			$data = array('contact_person' => $contact_person, 'cp_title' => $cp_title, 'business_name' => $business_name, 'service_type' => $service_type, 'service_category' => $service_category, 'device_imei' => '', 'mobile' => $mobile, 'email' => $email, 'street' => $street,  'area'=> $area,'city' => $city, 'lat' => $lat, 'longtd' => $longtd, 'account_id' => $this -> account_id, 'created_on' => $date);

			//print_r($data);

			$this -> customers_model -> update($customer_id, $data);

			redirect('customers');

		}
	}

	public function update_agent($id) {

		if ($this -> form_validation -> run() == FALSE) {
			if ($this -> session -> userdata('id')) {
				$id = $this -> session -> userdata('id');
			} else {
				$id = $this -> uri -> segment(4);
				$this -> session -> set_userdata('id', $id);
			}
			//$where = array('customer_id' => $id);
			$where = array('pro_employees.id' => $id);
			//geting the data
			$info = $this -> customers_model -> read_sales_team_agent($where);
			//echo $this->db->last_query();

			foreach ($info as $info) {
				$this -> fields['info'] = $info;
			}

			$this -> load -> view('template/header', $this -> fields);
			$this -> load -> view('template/content/customers_form_agent', $this -> fields);
			$this -> load -> view('template/footer');

		} else {

			extract($_POST);
			$date = date('Y-m-d h:i:s');
			//$data = array('contact_person' => $contact_person, 'cp_title' => $cp_title, 'business_name' => $business_name, 'service_type' => $service_type, 'service_category' => $service_category, 'device_imei' => $imei, 'mobile' => $mobile, 'email' => $email, 'street' => $street,  'area'=> $area,'city' => $city, 'lat' => $lat, 'longtd' => $longtd, 'account_id' => $this -> account_id, 'created_on' => $date);

			//print_r($data);
			
			$this->db->trans_start();

			$account_data = array('contact_person' => $contact_person, 'company_name' => $business_name, 'mobile' => $mobile, 'email' => $email,'street' => $street,  'area'=> $area, 'city' => $city);
	
			$this -> accounts_model-> update($accid, $account_data );
			//$accId = $this->db->insert_id();



	
			$name = explode(' ', $contact_person);
			$employee_data = array('firstname'=>$name[0], 'lastname'=>$name[1],'mobile'=>$mobile, 'email'=>$email );
	
			$this -> employees_model -> update($employee_id, $employee_data );
			
			//$assignedTo = $this->db->insert_id();

			$device_data = array('device_imei'=>$imei, 'device_descr'=>'N/A', 'assigned_to'=>$assignedTo, 'created_on'=>$date, 'created_by' => $this -> user_id,  'account_id'=>$accId, 'device_status'=>0, 'device_mobile'=>$mobile);
			$this -> devices_model -> update( $device_id, $device_data );

			$this->db->trans_complete();

			//$this -> customers_model -> update($customer_id, $data);

			redirect('customers/customerlist');

		}
	}

	public function checkmail($email, $customer_id) {

		$where = array('customers.customer_id' => $customer_id, 'email' => $email, 'account_id' => $this -> account_id);

		$customer = $this -> customers_model -> read($where);

		if ($customer) {
			return TRUE;
		} else {
			$where = array('customers.email' => $email);
			$customer = $this -> customers_model -> read($where);
			if ($customer) {
				$this -> form_validation -> set_message('checkmail', 'Sorry! the email is already used ');
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}

	public function checkmobile($mobile, $customer_id) {

		$where = array('mobile' => $mobile, 'customers.customer_id' => $customer_id, 'account_id' => $this -> account_id);

		$customer = $this -> customers_model -> read($where);

		if ($customer) {
			return TRUE;
		} else {

			$where = array('customers.mobile' => $mobile);
			$customer = $this -> customers_model -> read($where);
			if ($customer) {
				$this -> form_validation -> set_message('checkmobile', 'Sorry! the mobile number is already used');
				return FALSE;
			} else {
				return TRUE;
			}
		}

	}

	public function delete() {
		$id = $this -> uri -> segment(5);
		$act = $this -> uri -> segment(4);
		if ($act == 'add') {
			$data = array('status' => 1);
		} elseif ($act == 'remove') {
			$data = array('status' => 0);
		}
		if ($this -> customers_model -> update($id, $data)) {
			redirect('customers');
		}
	}

	/*
	 * assigning customers
	 */

	public function sales_team() {

		$this -> fields['controller'] = 'customers/sales_team';

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('id', 'employee_name', 'device_imei', 'mobile');

		$this -> fields['table_id'] = 'id';
		$this -> fields['view'] = 'customers/view_team';
		$where = array('employees.account_id' => $this -> account_id, 'emp_role' => 'sales');

		//filtering data
		if ($this -> input -> post('filter')) {
			extract($_POST);
 
			if ($employee) {
				$where['id'] = $employee;
			}

		}

		$team = $this -> customers_model -> read_sales_team($where);

		if ($team) {
			foreach ($team as $s_team) {
				$sale_team[$s_team['id']] = $s_team['firstname'] . ' ' . $s_team['lastname'];
			}
			$this -> fields['sales_team'] = $sale_team;
			$this -> fields['tb_data'] = $team;
		} else {
		$this -> fields['sales_team'] = 	$this -> fields['tb_data'] = FALSE;
		}
		$this -> fields['tb_name'] = 'team_tb_name';

		$header['stc_active'] = FALSE;
		$this -> fields['add_btn'] = FALSE;

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/sales_team_table', $this -> fields);
		$this -> load -> view('template/footer');
	}

	public function assign() {

		$this -> fields['controller'] = 'customers/assign';

		//read sales team details
		$where = array('employees.id' => $this -> uri -> segment(4));
		$team = $this -> customers_model -> read_sales_team($where);

		if ($team) {
			foreach ($team as $key => $value) {

				$this -> fields['team'] = $value;
			}

			$where = array('account_id' => $this -> account_id, 'device_imei' => 0);
			$customers = $this -> customers_model -> read($where);

			if ($customers) {
				$this -> fields['tb_data'] = $customers;
			} else { $this -> fields['tb_data'] = FALSE;
			}

			//$this -> fields['business'] = $bs_ness;
			$this -> session -> unset_userdata('id');

			//checking users permissions
			$this -> load -> view('template/header', $this -> fields);
			$this -> load -> view('template/content/assign_customer');
			$this -> load -> view('template/footer');

		} else {
			redirect('customers/sales_team');
		}
	}

	public function add_customers() {

		extract($_POST);
		$date = date('Y-m-d h:i:s');
		$to_team = $sales_team;
		$customers = $data = array('device_imei' => $device_imei, 'status' => 1, 'is_synced' => 0);

		//print_r($data);
		foreach ($customer as $key => $value) {
			$this -> customers_model -> update($value, $data);
		}

		redirect('customers/view_team/'.$to_team);
	}

	public function view_team() {

		$this -> fields['controller'] = 'customers/view_team';

		//read sales team details
		$where = array('employees.id' => $this -> uri -> segment(4));
		$team = $this -> customers_model -> read_sales_team($where);

		if ($team) {
			foreach ($team as $key => $value) {

				$this -> fields['team'] = $value;
			}
			//get
			if ($this -> input -> post('filter')) {
				extract($_POST);

				if ($business_name) {
					$where['customer_id'] = $business_name;
				}

			}

			$customers = $this -> customers_model -> read(array('device_imei' => $value['device_imei']));
			if ($customers) {
				$this -> fields['tb_data'] = $customers;
			} else { $this -> fields['tb_data'] = FALSE;
			}
			if ($customers) {
				foreach ($customers as $business) {
					$bs_ness[$business['customer_id']] = $business['business_name'];
				}
			} else {
				$bs_ness[] = "No customers";
			}
			$this -> fields['business'] = $bs_ness;
			$this -> session -> unset_userdata('id');

			$this -> fields['tb_name'] = "Customers List : " . $value['device_imei'];

			//checking users permissions
			$this -> load -> view('template/header', $this -> fields);
			$this -> load -> view('template/content/customers_table');
			$this -> load -> view('template/footer');
		} else {
			redirect('customers/sales_team');
		}

	}

	public function re_assign() {
		$id = $this -> uri -> segment(5);
		$team = $this -> uri -> segment(4);

		$data = array('device_imei' => 0);

		if ($this -> customers_model -> update($id, $data)) {
			redirect('customers/view_team/'.$team);
		}
	}

}
