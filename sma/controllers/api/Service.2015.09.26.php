<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';


/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Service extends REST_Controller
{

    private $device_imei;
    private $tag;
    private $account_id;
    private $returned = array();
    private $lastsync;
    private $user_id;

    function __construct()
    {

        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key

        $this->load->model('api_model');
        $this->load->library('user_agent');

    }

    /**
     *
     */
    function index_post()    {
        $this->proceed = false;

       // file_put_contents("json.txt", file_get_contents( 'php://input' ), FILE_APPEND);

        $this->device_imei = $this->post('imei');
        $this->tag = $this->post('tag');

        if ($this->tag && $this->device_imei) {
            //$exists = $this->devices_model->api_check_device_master($data);
            $user_data = $this->api_model->get_device_account($this->device_imei);
            if ($user_data) {
                //var_dump($user_data);exit;
                $this->account_id = $user_data->account_id;
                $this->user_id = $user_data->assigned_to;
                //var_dump($this);exit;
                if ($this->account_id && $user_data->active ) {
                    $exist_ = $this->api_model->api_check_device_sync(array('device_imei' => $this->device_imei));
                    if ($exist_) {
                        $this->proceed = true;
                    } else {
                        $reg = $this->api_model->api_register_device_sync(array('device_imei' => $this->device_imei));
                        if ($reg) {
                            $this->proceed = true;
                        }
                    }
                }
            }

            if ( $this->proceed ) {

                $lastsync = $this->api_model->api_get_device_entities_lastsync(array('device_imei' => $this->device_imei));
                //var_dump($lastsync);exit;
                if ($lastsync['products'] && $lastsync['customers'] && $lastsync['purchase_items'] && $lastsync['product_categories'] &&
                    $lastsync['sales'] && $lastsync['sales_items'] && $lastsync['warehouse'] && $lastsync['settings']
                ) {
                    $this->api_model->api_update_master_lastsync(array('device_imei' => $this->device_imei),
                        array('last_sync' => date('Y-m-d H:i:s')));
                }

                $this->lastsync = $lastsync;



                // if ( $this->account_id && $this->user_id ) {
                switch ( $this->tag ) {
                    case "upload_customers": // Done
                        $this->_upload_customers($this->post('incommingcustomers'));
                        break;
                    case "receive_products":  //Done
                        $this->_assign_products($lastsync);
                        break;
                    case "assign_customers":  //Done
                        //file_put_contents('monitor_sync_time.txt', json_encode(array($this->device_imei,date('Y-m-d h:i:s'))),FILE_APPEND);
                        $this->_assign_customers();
                        break;

                    case "upload_sales": // On old API it was upload_orders
                        $this->_upload_sales($this->post('sales_list'));
                        break;
                    case 'upload_sale_items':
                        $this->_upload_sale_items($this->post('sales_itemlist'));
                        break;
                    case 'upload_return_items':
                        $this->_upload_return_items( $this->post('sales_itemlist') );
                        break;
                    case "receive_sales": // On old API it was upload_orders
                        $this->_receive_sales();
                        break;
                    case 'receive_sale_items':
                        $this->_receive_sale_items();
                        break;
                    case "receive_product_cats": // Done
                        $this->_receive_product_category();
                        break;
                    case "upload_product_cats": // Done
                        $this->_upload_product_category($this->post('cat_list'));
                        break;
                    case "receive_purchase_items": // Done
                        $this->_receive_purchase_items();
                        break;
                    case "upload_purchase_items": // Done
                        $this->_upload_purchase_items($this->post('purch_items_list'));
                        break;

                    case 'multi_media_data':
                       // var_dump( $this->post('multimedia_array') );exit;
                        $this->_multi_media_data( $this->post('multimedia_array') );
                        break;

                    case 'acktag':
                        $code = $this->post('code');
                        if ($code)
                            $this->_reset_acktag($this->post('item'), $this->post('code'));
                        else
                            $this->_process_acktag($this->post('item'));
                        break;

                    case 'upload_products':
                        $this->_upload_products($this->post('productsList'));
                        break;

                    case 'receive_account_settings':
                        $this->_receive_account_settings();
                        break;

                    case 'receive_warehouse':
                        $this->_receive_warehouse();
                        break;

                    case 'device_tracking':
                        $this->_device_tracking( $this->post('datapoints') );
                        break;

                    case 'receive_delivery':
                        $this->_receive_delivery();
                        break;

                    case 'upload_delivery':
                        $this->_upload_delivery( $this->post('deliveries') );
                        break;

                    default:
                        $this->_invalid_request();
                        break;
                }
                //   }//If IMEI Match


            } // If Proceed
            else {
                $this->returned['error'] = 1;
                $this->returned['message'] = "Problem with Account & User Mapping. Check User, Device & Account Association or Account may be Disabled.";
            }
        } // If IMEI and Tag
        else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "Either Tag or IMEI is missing.";
        }

        $this->response($this->returned, 200); //200 being the HTTP response code
    }

    function  _receive_delivery() {

        //$commons = ( array( 'account_id' => $this->account_id, 'device_imei'=>$this->device_imei ));

        $where = array('account_id' => $this->account_id);

        $where['last_update_time > '] = $this->lastsync["delivery_synctime"];

        $deliveries = $this->api_model->getAllDeliveries( $where );

        if ( $deliveries ) {
            $this->returned['success'] = 1;
            $this->returned['deliveries'] = $deliveries;
            $this->response($this->returned, 200);

        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "No deliveries found.";
            $this->response( $this->returned, 200);
        }
    }

    function _device_tracking( $data ){
        //var_dump($datapoints);exit;
        $commons = ( array( 'account_id' => $this->account_id, 'device_imei'=>$this->device_imei ));

        $datapoints = $this->api_model->device_tracking($data, $commons);

        if (count($datapoints)) {
            $this->returned['success'] = 1;
            //if (count($datapoints))
            $this->returned['datapoints'] = $datapoints;
            $this->response($this->returned, 200);

        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "No data points processed.";
            $this->response($this->returned, 200);
        }
    }

    // This is for Delivery Upload
    function _upload_delivery( $data ){

        $commons = ( array( 'account_id' => $this->account_id, 'device_imei'=>$this->device_imei ));

        $deliveries = $this->api_model->upload_delivery($data, $commons);

        if (count( $deliveries )) {
            $this->returned['success'] = 1;
            $this->returned['deliveries'] = $deliveries;
            $this->response( $this->returned, 200 );

        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "No data points processed.";
            $this->response($this->returned, 200);
        }
    }

    /**************************** save customers from the device ******************************/
    function _invalid_request(){
        $this->returned['error'] = 1;
        $this->returned['message'] = "Invalid request. Unable to process the requested operation.";
        $this->response($this->returned, 200); //200 being the HTTP response code
    }

    function _upload_customers( $data )    {

        $commons = ( array( 'account_id' => $this->account_id, 'device_imei'=>$this->device_imei,'user_id'=>$this->user_id ) );

        $customers = $this->api_model->receive_device_customer( $commons, $data );

        if ( $customers ) {
            $this->returned['success'] = 1;
            if (count( $customers ))
                $this->returned['customerSentList'] = $customers;
            $this->response( $this->returned );

        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "no customer received";
            $this->response($this->returned, 200);
        }
    }

    function _upload_products($products)
    {
        //file_put_contents("products.txt", json_encode($products));
        //var_dump( $products );exit;
        $commons = (array('account_id' => $this->account_id, 'user_id' => $this->user_id));
        $upl_prod = $this->api_model->upload_products($commons, $products);
        //var_dump($upl_prod);exit;
        if (count($upl_prod)) {
            $this->returned['success'] = 1;
            if (count($upl_prod))
                $this->returned['productsList'] = $upl_prod;
            $this->response($this->returned);

        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "no products uploaded";
            $this->response($this->returned, 200);
        }
    }

    /**************************** assign products to the device ******************************/
    function _assign_products($lastsync)    {

        $where = array('account_id' => $this->account_id);

        //$where['created_by = '] = $this->user_id;

        //$where = (array('account_id'=> $this->account_id ,'last_update_time >' => $this->lastsync['customers_synctime'] ));
        $ack = $this->lastsync['products'];
        if ($ack == 1) {
            $where['last_update_time > '] = $this->lastsync["products_synctime"];
        }

        $stock = $this->api_model->get_all_products($where, $this->user_id);
        // echo $this->db->last_query();exit;
        if ($stock) {
            $this->returned['success'] = 1;
            $this->returned['productsList'] = $stock;
            $this->response($this->returned);
        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "no products";
            $this->response($this->returned);
        }
    }

    /*
     * ALTER TABLE `pro_base`.`sma_companies`
ADD COLUMN `tin` VARCHAR(50) NULL DEFAULT NULL AFTER `created_name`,
ADD COLUMN `vrn` VARCHAR(50) NULL DEFAULT NULL AFTER `tin`;

     */

    /**************************** assign customers to the device ******************************/
    private function _assign_customers()
    {
    		//echo $this->lastsync['customers_synctime'];
        $where = (array('account_id' => $this->account_id));
    	// $where = (array('account_id' => $this->account_id));

        $ack = $this->lastsync['customers'];
        
        if ($ack == 1) {
            $where['last_update_time > '] = $this->lastsync["customers_synctime"];
        }

        $customers = $this->api_model->get_all_customers($where);
         //echo $this->db->last_query();exit;

        if ($customers) {
            $this->returned['success'] = 1;
            $this->returned['customerList'] = $customers;
            $this->response($this->returned);
        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "no customers";
            $this->response($this->returned);
        }
    }


    /**************************** assign customers to the device ******************************/
    private function _receive_account_settings()
    {
       // $where = (array('account_id' => $this->account_id, 'last_update_time >' => $this->lastsync['customers_synctime']));
        $account_where = array('account_id'=>$this->account_id);
        $settings_ack = $this->lastsync['settings'];
        if ($settings_ack == 1) {
            $account_where['last_update_time > '] = $this->lastsync["settings_synctime"];
        }
        $account_settings = $this->api_model->getAccount_settings( $account_where );
        // echo $this->db->last_query();exit;

        if ($account_settings) {
            $this->returned['success'] = 1;
            $this->returned['settingsList'] = $account_settings;
            $this->response($this->returned);
        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "no settings for this account available.";
            $this->response($this->returned);
        }
    }

    /**************************** upload orders *****************************/
    private function _upload_sales($sales)    {

        $where = array('device_imei' => $this->device_imei, 'account_id' => $this->account_id, 'user_id' => $this->user_id);
        //$where['reference_no'] = $this->site->getReference('pay');
        $salesList = $this->api_model->save_sales($sales, $where);
        if ($salesList) {
            $this->returned['success'] = 1;
            $this->returned['sales_list'] = $salesList;
            $this->response($this->returned);
        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "No sales received.";
            $this->response($this->returned);
        }
    }


    /**************************** upload orders *****************************/
    private function _upload_product_category($cateories)    {

        $where = array( 'device_imei' => $this->device_imei, 'account_id' => $this->account_id );
        $save = $this->api_model->save_categories($cateories, $where);
        if ($save) {
            $this->returned['success'] = 1;
            $this->returned['cat_list'] = $save;
            $this->response($this->returned);
        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "order not received";
            $this->response($this->returned);
        }
    }

    /**** This is the Auntheticate method ***/
    function authenticate_post()
    {

        $this->load->library('mcrypt');

        //$tag = $this->post('tag');   //$data = file_get_contents('php://input');  //$data = json_decode($data, true);
        //file_put_contents("imei.txt", $this->post('imei')." - ".date("Y-m-d h:i:s")."\n");
        $this->tag = $this->post('tag');
        $this->device_imei = $this->post('imei');
        // echo $this->post('imei');
        if ($this->tag && $this->device_imei) {

            $lastsync = $this->api_model->api_get_device_entities_lastsync(array('device_imei' => $this->device_imei));
            $this->lastsync = $lastsync;
            //$exists = $this->devices_model->api_check_device_master($data);
           // $this->account_id = $this->api_model->get_device_account($this->device_imei);

            $user_data = $this->api_model->get_device_account($this->device_imei);

            $this->account_id = $user_data->account_id;

            $account_where = array('account_id'=>$this->account_id);
            $settings_ack = $this->lastsync['settings'];
            if ($settings_ack == 1) {
                $account_where['last_update_time > '] = $this->lastsync["settings_synctime"];
            }


            $account_settings = $this->api_model->getAccount_settings( $account_where );

            //echo $this->db->last_query();exit;
            if ($this->tag == "register_device") {

                //file_put_contents("imei1.txt", file_get_contents( 'php://input' ), FILE_APPEND);

                //$acc_details =  $this->fetch_acccount_details( $imei );

                $acc_details = $this->api_model->registration($this->device_imei);

                // Its exceptional Case
                //$this->db->update('device_sync_status', array('device_imei'=>$this->device_imei));
                $this->api_model->updateLastSyncStatuses(array('device_imei'=>$this->device_imei) , array('products'=>0,'customers'=>0,
                                                                                            'purchase_items'=>0,'product_categories'=>0,
                                                                                            'warehouse'=>0,'settings'=>0));


               // echo $this->db->last_query();exit;
                //var_dump( $acc_details );exit;
                if ( $acc_details ) {
                    $acc_type = $acc_details->acc_type;

                    $business_name = $acc_details->company;
                    $contact_person = $acc_details->c_person;
                    $mobile_number = $acc_details->mobile;

                    $status = ($acc_type) ? array('success' => 1) : array('failure' => 1);

                    $warehouses = $this->api_model->getWarehouseByDevice($this->device_imei);
                    //var_dump($this->account_id);exit;


                  //  echo $this->db->last_query();exit;

                    $encrypted = $this->mcrypt->encrypt($this->device_imei . "|" . $acc_type);

                    $ac_t = array('account' => $acc_type, 'business_name' => $business_name,
                        'contact_person' => $contact_person, 'mobile' => $mobile_number, 'wh_list' => $warehouses,'settingsList'=>$account_settings );

                    //$debug_a = array('imei'=>$imei, 'decrypt'=>$this->mcrypt->decrypt($encrypted));

                    $retArr = array_merge($status, $ac_t, array(
                        'authentication_key' => $encrypted));
                } else
                    $retArr = array_merge(array('failure' => 1), array('msg' => 'account information is not correct'));

                $this->response($retArr, 200);
            }
            if ($this->tag == "authenticate") {

                //$imei = $this->post('imei');
                $acc_details = $this->api_model->registration($this->device_imei);
                $acc_type = $acc_details->acc_type;

                $encrypted = $this->mcrypt->encrypt($this->post('imei') . "|" . $acc_type);

                $encrypt_device = $this->post('authentication_key');

                //$status = ($encrypted == $encrypt_device) ? array('success' => 1) : array('failure' => 1);
                if( ($encrypted == $encrypt_device) && $acc_details->active == 1){
                    $status = array('success' => 1);
                }
                else
                {
                    $status =  array('failure' => 1);
                }
                $ac_t = array('account' => $acc_type);
                //$debug_a = array('imei'=>$imei, 'decrypt'=>$this->mcrypt->decrypt($encrypted));
                $retArr = array_merge($status, $ac_t );

                $this->response($retArr);
            }
        }
    }


    function _product_category()   {


        $where = array('account_id' => $this->account_id);
        //$where = (array('account_id'=> $this->account_id ,'last_update_time >' => $this->lastsync['customers_synctime'] ));
        $ack = $this->lastsync['product_categories'];
        if ($ack == 1) {
            $where['last_update_time > '] = $this->lastsync["product_categories_synctime"];
        }


        $categories = $this->api_model->get_all_categories($where);

        // var_dump($categories);exit;

        if ($categories) {
            $this->returned['success'] = 1;
            $this->returned['cat_list'] = $categories;
            $this->response($this->returned, 200);
        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "no category";
            $this->response($this->returned, 200);
        }
    }

    function _upload_purchase_items($data)
    {
        //var_dump($data);exit;
        $where = (array('account_id' => $this->account_id, 'device_imei' => $this->device_imei, 'user_id' => $this->user_id));
        $purchaseitems = $this->api_model->save_purchase_items($data, $where);

        // var_dump( $categories ); exit;

        if ( $purchaseitems) {
            $this->returned['success'] = 1;
            $this->returned['purch_items_list'] = $purchaseitems;
            $this->response($this->returned, 200);
        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "No purchase items uploaded.";
            $this->response($this->returned, 200);
        }
    }

    function _upload_sale_items($data)
    {
        //var_dump($data);exit;
        $where = (array('account_id' => $this->account_id, 'device_imei' => $this->device_imei, 'user_id' => $this->user_id));
        $salesitems = $this->api_model->save_sale_items($data, $where);

        // var_dump($categories);exit;

        if ( $salesitems ) {
            $this->returned['success'] = 1;
            $this->returned['sales_itemlist'] = $salesitems;
            $this->response($this->returned, 200);
        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "No sales items uploaded.";
            $this->response($this->returned, 200);
        }
    }

    function _upload_return_items($data)
    {
        //var_dump($data);exit;
        $where = (array('account_id' => $this->account_id, 'device_imei' => $this->device_imei, 'user_id' => $this->user_id));
        $returnitems = $this->api_model->save_return_items($data, $where);

        // var_dump($categories);exit;

        if ( $returnitems ) {
            $this->returned['success'] = 1;
            $this->returned['sales_itemlist'] = $returnitems;
            $this->response($this->returned, 200);
        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "No return sales items uploaded.";
            $this->response($this->returned, 200);
        }
    }
    function _receive_purchase_items()    {

        //$whids = $this->api_model->getWarehouseByUser( $this->user_id );

        //$where['warehouse_id  IN ( ']  = implode(',', $whids);

        $where_in = $this->api_model->getWarehouseByUser($this->user_id);
        // var_dump($where_in);exit;
        $where = (array('purchase_items.account_id' => $this->account_id /*,'device_imei' => $this->device_imei */));

        $ack = $this->lastsync['purchase_items'];
        if ($ack == 1) {
            $where['purchase_items.last_update_time > '] = $this->lastsync["purchase_items_synctime"];
        }

        if($where_in)  // If user Id then Fetch, Otherwise Ignore the request
            $purchase_items = $this->api_model->get_all_purchase_items($where, $where_in);
        else
            $purchase_items = false;

        // echo $this->db->last_query();exit;

        if ($purchase_items) {
            $this->returned['success'] = 1;
            $this->returned['purch_items_list'] = $purchase_items;
            $this->response($this->returned);
        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "No purchase Items Found.";
            $this->response($this->returned);
        }
    }

    function _receive_sales()    {

        $salesData = false;
       // echo $this->db->last_query();exit;
        $where = (array('account_id' => $this->account_id, 'device_imei' => $this->device_imei));

        $ack = $this->lastsync['sales'];

        if ( trim($ack) == 1 ) {
            $salesData = $this->api_model->get_all_sales($where);
        }

        // After Delivery Added...
        if ( trim($ack) == 2 ) { // Note : In case of Delivery & Update ack to 2 and last_update_time

            $where['sale_status'] = "pending";
            $where['date > '] =  $this->lastsync['sales_synctime'];
            $salesData = $this->api_model->get_all_sales( $where );

        }
        if( $salesData ){
            $this->returned['success'] = 1;
            $this->returned['sales_list'] = $salesData;
            $this->response($this->returned, 200);

        }
        else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "No Sales Found.";
            $this->response($this->returned, 200);
        }
    }


    function _receive_sale_items()    {

        $saleItemsData = false;
        $where = (array('account_id' => $this->account_id, 'device_imei' => $this->device_imei));
        $ack = $this->lastsync['sales_items'];

        if (  trim($ack) == 1  ) {
                $saleItemsData = $this->api_model->get_all_sale_items( $where );
            }
            // After delivery added
        if (  trim( $ack ) == 2  ) {
                $where['created_on > '] =  $this->lastsync['sales_items_synctime'];
                $where['delivery_status'] =  'pending'; // For Delivery
                $saleItemsData = $this->api_model->get_all_sale_items($where);
            }

        if( $saleItemsData ){
            $this->returned['success'] = 1;
            $this->returned['sales_itemlist'] = $saleItemsData;
            $this->response($this->returned, 200);
        }

        else {
                $this->returned['error'] = 1;
                $this->returned['message'] = "No Sales Item Found.";
                $this->response($this->returned, 200);
            }
    }

   function _receive_warehouse()    {

        $where = false;
       $ack = $this->lastsync['warehouse'];
       if ($ack == 1) {
           $where = "AND sw.last_update_time > '". $this->lastsync["warehouse_synctime"]."'";
       }

       $warehouses = $this->api_model->getWarehouseByDevice($this->device_imei, $where);



       if ( $warehouses ) {
               $this->out['success'] = 1;
              // $this->response($this->out, 200);
               $this->out['message'] = "Success";
                $this->out['wh_list'] = $warehouses;

       }
       else
       {
           $this->out['failed'] = 1;
           $this->out['message'] = "Failed. No warehouse.";

       }
       $this->response($this->out, 200);

    }

    function _process_acktag( $item )
    {
        // var_dump( "item" );exit;
        if( $item == "receive_account_settings" )
            $item = "settings";
        
        if ($item &&  in_array(trim($item), array('sales', 'sales_items','products','customers','purchase_items', 'product_categories','sales_items','settings', 'warehouse','delivery')) ) {
            $where = array('device_imei' => $this->device_imei);

            if (in_array(trim($item), array('sales', 'sales_items' )))
                $status = 0;
            else
                $status = 1;

            $use = array($item => $status, $item . '_synctime' => date('Y-m-d H:i:s'));
            $p = $this->api_model->updateLastSyncStatuses($where, $use);
            if ($p) {
                $this->out['success'] = 1;
               // $this->out['tag'] = $this->db->last_query();
                $this->response($this->out, 200);
            }
            // break;

        } // If Item
        else
        {
            $this->out['failed'] = 1;
            $this->out['message'] = "Not a valid tag";
            $this->response($this->out, 200);
        }

    }

    function _reset_acktag($item, $code)    {
        // var_dump( "item" );exit;
       // $code = array(11=>);
        if ($item &&  in_array(trim($item), array('sales', 'sales_items','products','customers','purchase_items', 'product_categories','sales_items','settings', 'warehouse','delivery')) ) {
            $where = array('device_imei' => $this->device_imei);

           /* if (in_array(trim($item), array('sales', 'sales_items')))
                $status = 0;
            else*/
            $status = 0;

            $use = array($item => $status, $item . '_synctime' => date('0000-00-00 00:00:00'));
            $p = $this->api_model->updateLastSyncStatuses($where, $use);
            if ($p) {
               // $this->out['success'] = 1;
                $this->out['success'] = 1;
                $this->response($this->out, 200);
            }

                else
            {
                $this->out['failed'] = 1;
                $this->out['message'] = "Nothing to do.";
                $this->response($this->out, 200);
            }
            // break;

        } // If Item
        else
        {
            $this->out['failed'] = 1;
            $this->out['message'] = "Not a valid tag";
            $this->response($this->out, 200);
        }

    }

    /**************************** assign products to the device ******************************/
    private function  _receive_product_category()    {

        $where = (array('account_id' => $this->account_id));

        $ack = $this->lastsync['product_categories'];
        if ($ack == 1) {
            $where['last_update_time > '] = $this->lastsync["product_categories_synctime"];
        }


        $product_cat = $this->api_model->get_all_product_categories( $where );
        // echo $this->db->last_query();exit;

        if ( $product_cat ) {
            $this->returned['success'] = 1;
            $this->returned['cat_list'] = $product_cat;
            $this->response($this->returned);
        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "no categories";
            $this->response($this->returned);
        }
    }

    private function _multi_media_data( $data ){

        $where = array('device_imei'=> $this->device_imei, 'account_id' => $this->account_id);
        $save = $this-> api_model ->saveImageFrom( $data ,$where);

        if(is_array($save))
        {
            if (count($save, COUNT_NORMAL) > 0) {

                switch( $data['type'] ){
                    case 'delivery':
                        $this->api_model->updateDeliverySignature( $data, $save );
                        break;
                    default:
                        break;
                }

                $this->returned['success'] = 1;
                $this->returned['foreginkeyid'] = $data['foreginkeyid'];
                $this->returned['multimedia_array'] =  $save;
                $this->response( $this->returned );
            } else {
                $this->returned['error'] = 1;
                $this->returned['message'] = "no content received";
                $this->response($this->returned);
            }
        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "no content received (not array)";
            $this->response( $this->returned );
        }

    }
}
