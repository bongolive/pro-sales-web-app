<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');
/**
 * Created by PhpStorm.
 * User: nasznjoka
 * Date: 5/12/15
 * Time: 6:06 PM
 */

class Service extends REST_Controller{
    protected $proceed = false;
    private $out = array();

    public function __construct(){

        parent::__construct();

        $this -> load -> model('accounts_model');
        $this -> load -> model('users_model');
        $this -> load -> model('devices_model');
        $this -> load -> model('employees_model');
        $this -> load -> model('locations_model');
        $this -> load -> model('questionaires_model');
        $this -> load -> model('surveys_model');
        $this -> load -> model('survey_sections_model');
        $this -> load -> model('settings_model');

    }

    public function index_get(){
        $this->out['error'] = "1";
        $this->out['msg'] = " invalid request only get allowed";
        $this->response($this->out);
    }

    public function index_post(){
        $data = file_get_contents("php://input");
        $data = json_decode($data, true);

        $this->write_log($data);

        if(isset($data['tag']) && isset($data['imei'])){
            $exists = $this->devices_model->api_check_device_master($data);
            if($exists){
                $exist_ = $this->devices_model->api_check_device_sync($data);
                if($exist_){
                    $this->proceed = true;
                } else {
                    $reg = $this->devices_model->api_register_device_sync($data);
                    if($reg){
                        $this->proceed = true;
                    }
                }
                if($this->proceed){

                    $tag = $data['tag'];

                    $lastsync = $this->devices_model->api_get_device_entities_lastsync($data);

                    $device = $this->devices_model->api_get_account($data);

                    if($lastsync['users'] == 1 && $lastsync['questions'] == 1 &&
                        $lastsync['surveys'] == 1 && $lastsync['settings'] == 1 && $lastsync['responses'] ==1){
                        $w = array('device_imei' => $data['imei']);
                        $d = array('last_sync' => date('Y-m-d H:i:s'));
                        $this->devices_model->api_update_master_lastsync($w,$d);
                    }

                    switch ($tag) {
                        case 'user_data':
                            $userack = $lastsync['users'];//extract user ack in the pro_device_sync_status
                            $userlast = $lastsync['users_synctime']; //extract user last sync in the
                            // pro_device_syn_status
                            $userwhere = array('device_imei' => $device['device_id'],
                                'account_no' => $device['account_no'], 'last_update_time >' => $userlast);

                            if($userack == 0){
                                $this->get_all_users($userwhere); //all users
                            } else if ($userack == 1){
                                $this->get_all_users_updates($userwhere); // updates and new if any
                            }
                            break;
                        case 'questions_data':
                            $questionack = $lastsync['questions'];
                            $questionslast = $lastsync['questions_synctime'];

                            $questionwhere = array('account_no' => $device['account_no'],
                                'last_update_time' => $questionslast);

                            if($questionack == 0){
                                $this->get_all_questions($questionwhere); //all users
                            } else if ($questionack == 1){
                                $this->get_all_questions_updates($questionwhere); // updates and new if any
                            }
                            break;
                        case 'survey_data':
                            $surveyack = $lastsync['surveys'];
                            $surveylast = $lastsync['surveys_synctime'];
                            $wheresurvey = array('account_no' => $device['account_no'], 'last_update_time >' =>
                                $surveylast);

                            if($surveyack == 0){
                                $this->get_all_surveys($wheresurvey);
                            } else if ($surveyack == 1){
                                $this->get_all_surveys_updates($wheresurvey);
                            }
                            break;
                        case 'response_choice_data':
                            $responsesack = $lastsync['responses'];
                            $responselast = $lastsync['responses_synctime'];
                            $whereresponse = array('account_no' => $device['account_no'], 'last_update_time >' => $responselast);

                            if($responsesack == 0){
                                $this->get_all_responsechoice($whereresponse);
                            } else if ($responsesack == 1){
                                $this->get_all_responsechoice_updates($whereresponse);
                            }
                            break;
                        case 'settings_data':
                            $settingack = $lastsync['settings'];
                            $settinglast = $lastsync['settings_synctime'];

                            $wheresettings = array('account_no' => $device['account_no'], 'last_update_time >' =>
                                $settinglast);

                            if($settingack == 0){
                                $this->get_all_settings($wheresettings);
                            } else if ($settingack == 1){
                                $this->get_all_settings_updates($wheresettings);
                            }
                            break;
                        case 'participant_data':

                            $partarray = array('account_no' => $device['account_no'], 'device_imei' =>
                                $device['device_imei']);
                            if(isset($data['Participant'])){
                            $this->save_participants($partarray,$data['Participant']);
                           }
                            break;
                        case 'response_data':

                            $respoarray = array('account_no' => $device['account_no'], 'device_imei' =>
                                $device['device_imei']);
                            if(isset($data['response'])){
                                $this->save_responses($respoarray,$data['response']);
                            }
                            break;
                        case 'acktag':
                            if(isset($data['item'])){
                                $this->process_ack($data);
                            }
                            break;
                    }

                } else {
                    $this->out['error'] = "1";
                    $this->out['msg'] = "unkonw device";
                    $this->response($this->out);
                }

            } else {
                $this->out['error'] = "1";
                $this->out['msg'] = "unkonw device";
                $this->response($this->out);
            }
        } else {
            $this->out['error'] = "1";
            $this->out['msg'] = " invalid request";
            $this->response($this->out);
        }

            $this->sending_data($this->response($this->out));
            $this->write_log($this->out);
    }

    protected function get_all_users($data){
        $users = $this->employees_model->api_get_all_users($data);
        if($users){
            $this->out['success'] = "1";
            $this->out['user_data_array'] = $users;
            $this->response($this->out);
            $this->out = $this->response($this->out);
        } else {
            $this->out['error'] = "1";
            $this->out['msg'] = "no users ";
            $this->response($this->out);
        }
    }

    private function get_all_users_updates($array)
    {
        $users = $this->employees_model->api_get_all_users($array);
        if($users){
            $this->out['success'] = "1";
            $this->out['update_user_data_array'] = $users;
            $this->response($this->out);
            $this->out = $this->response($this->out);
        } else {
            $this->out['error'] = "1";
            $this->out['msg'] = "no users ";
            $this->response($this->out);
        }
    }

    private function get_all_questions($data)
    {
        $questions = $this->questionaires_model->api_get_all_questions($data);
        if($questions){
            $this->out['success'] = "1";
            $this->out['questions_data_array'] = $questions;
            $this->response($this->out);
            $this->out = $this->response($this->out);
        } else {
            $this->out['error'] = "1";
            $this->out['msg'] = "no questions ";
            $this->response($this->out);
        }
    }

    private function get_all_questions_updates($data)
    {
        $questions = $this->questionaires_model->api_get_all_questions($data);
        if($questions){
            $this->out['success'] = "1";
            $this->out['update_questions_data_array'] = $questions;
            $this->response($this->out);
            $this->out = $this->response($this->out);
        } else {
            $this->out['error'] = "1";
            $this->out['msg'] = "no questions ";
            $this->response($this->out);
        }
    }
    private function get_all_responsechoice($data)
    {
        $questions = $this->questionaires_model->api_get_all_response_choices($data);
        if($questions){
            $this->out['success'] = "1";
            $this->out['response_choices_array'] = $questions;
            $this->response($this->out);
            $this->out = $this->response($this->out);
        } else {
            $this->out['error'] = "1";
            $this->out['msg'] = "no responses ";
            $this->response($this->out);
        }
    }

    private function get_all_responsechoice_updates($data)
    {
        $questions = $this->questionaires_model->api_get_all_response_choices($data);
        if($questions){
            $this->out['success'] = "1";
            $this->out['update_response_choices_array'] = $questions;
            $this->response($this->out);
            $this->out = $this->response($this->out);
        } else {
            $this->out['error'] = "1";
            $this->out['msg'] = "no responses ";
            $this->response($this->out);
        }
    }
    private function get_all_surveys($data)
    {
        $questions = $this->surveys_model->api_get_all_surveys($data);
        if($questions){
            $this->out['success'] = "1";
            $this->out['survey_data_array'] = $questions;
            $this->response($this->out);
            $this->out = $this->response($this->out);
        } else {
            $this->out['error'] = "1";
            $this->out['msg'] = "no surveys ";
            $this->response($this->out);
        }
    }

    private function get_all_surveys_updates($data)
    {
        $questions = $this->surveys_model->api_get_all_surveys($data);
        if($questions){
            $this->out['success'] = "1";
            $this->out['update_survey_data_array'] = $questions;
            $this->response($this->out);
            $this->out = $this->response($this->out);
        } else {
            $this->out['error'] = "1";
            $this->out['msg'] = "no surveys ";
            $this->response($this->out);
        }
    }
    private function get_all_settings($data)
    {
        $questions = $this->settings_model->api_get_all_settings($data);
        if($questions){
            $this->out['success'] = "1";
            $this->out['settings_data_array'] = $questions;
            $this->response($this->out);
            $this->out = $this->response($this->out);
        } else {
            $this->out['error'] = "1";
            $this->out['msg'] = "no settings ";
            $this->response($this->out);
        }
    }

    private function get_all_settings_updates($data)
    {
        $questions = $this->settings_model->api_get_all_settings($data);
        if($questions){
            $this->out['success'] = "1";
            $this->out['update_settings_data_array'] = $questions;
            $this->response($this->out);
            $this->out = $this->response($this->out);
        } else {
            $this->out['error'] = "1";
            $this->out['msg'] = "no settings ";
            $this->response($this->out);
        }
    }

    private function save_participants($where,$data){
        $part = $this-> questionaires_model -> api_save_participant($where,$data);
        if($part){
            $toreturn = array();
            foreach ($data as $key => $value) {
                $toreturn[] = array('participant_id' => $value['participant_id'], 'syc_date' => date('Y-m-d h:i:s'));
            }
            $this->out['success'] = "1";
            $this->out['imei'] = $where['device_imei'];
            $this->out['participant_data_array'] = array('participants' => $toreturn);
            $this->response($this->out);
            $this->out = $this->response($this->out);
        } else {
            $this->out['error'] = "1";
            $this->out['msg'] = "participant exists" ;
            $this->response($this->out);
        }
    }

    private function save_responses($where,$data){
        $part = $this-> questionaires_model -> api_save_responses($where,$data);
        if($part){
            $toreturn = array();
            foreach ($data as $key => $value) {
                $toreturn[] = array('participant_id' => $value['participant_id'], 'syc_date' => date('Y-m-d h:i:s'),
                    'response_id' => $value['response_id']);
            }
            $this->out['success'] = "1";
            $this->out['imei'] = $where['device_imei'];
            $this->out['response_data_array'] = array('responses' => $toreturn);
            $this->response($this->out);
            $this->out = $this->response($this->out);
        } else {
            $this->out['error'] = "1";
            $this->out['msg'] = "response exists" ;
            $this->response($this->out);
        }
    }

    private function process_ack($data){
        $where = array('device_imei' => $data['imei']);
        switch($data['item']){
            case 'users':
                $use = array('users' => '1','users_synctime' => date('Y-m-d H:i:s'));
                $p = $this-> devices_model -> api_update_last_sync_statuses($where,$use);
                if($p){
                        $this->out['success'] = "1";
                        $this->response($this->out);
                    }
                break;
            case 'questions':
                $qns = array('questions' => '1','questions_synctime' => date('Y-m-d H:i:s'));
                $q = $this -> devices_model -> api_update_last_sync_statuses($where,$qns);
                if($q){
                    $this->out['success'] = "1";
                    $this->response($this->out);
                }
                break;
            case 'responses':
                $response = array('responses' => '1','responses_synctime' => date('Y-m-d H:i:s'));
                $r = $this -> devices_model -> api_update_last_sync_statuses($where,$response);
                if($r){
                    $this->out['success'] = "1";
                    $this->response($this->out);
                }
                break;
            case 'settings':
                $setting = array('settings' => '1','settings_synctime' => date('Y-m-d H:i:s'));
                $s = $this -> devices_model -> api_update_last_sync_statuses($where,$setting);
                if($s){
                    $this->out['success'] = "1";
                    $this->response($this->out);
                }
                break;
            case 'surveys':
                $survey = array('surveys' => '1','surveys_synctime' => date('Y-m-d H:i:s'));
                $sv = $this -> devices_model -> api_update_last_sync_statuses($where,$survey);
                if($sv){
                    $this->out['success'] = "1";
                    $this->response($this->out);
                }
                break;
        }
    }

function sending_data($data) {

    header('Content-type : application/json');
    echo json_encode($data);
}

function write_log($data) {
    file_put_contents('log.txt', print_r($data, true), FILE_APPEND);
}

}