<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends My_Controller {
 
	 
	public function index()
	{
		
		$this->load->helper('url');
		//$data["language_msg"] = $this->lang->line("");
		///print_r($data["language_msg"]);
		$this->load->view('header');
		$this->load->view('welcome_message');
		$this->load->view('footer');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */