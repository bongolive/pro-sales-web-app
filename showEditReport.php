<?php
session_start();
include('common.php');
include ("connection.php");

$row = "";

if($_REQUEST["rptid"] != NULL){

	$sql = "SELECT r.*,p.device_name FROM reports r inner join phones p on p.phoneid=r.phoneid left join phones_users pu on pu.phoneid=p.phoneid where id = ".$_REQUEST["rptid"];
	
	$res =mysql_query($sql,$con);
	
	if (!$res)
	{
		die('Error: ' . mysql_error());
		exit;
	}else{
		if($row = mysql_fetch_assoc($res)){
			
		}else{
			echo "<h3>No Record found against this report id.</h3>";
			exit;
		}
	}
	
}else{
	echo "<h3>No Report Selected to Edit</h3>";
	exit;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">

function saveRecord(){
	
	document.getElementById("divformwraper").style.display = "none";
	document.getElementById("div_loading").style.display = "block";
	
	$.ajax( {
		
		type: "POST",
		url: $("#frmEditReport").attr( 'action' ),
		data: $("#frmEditReport").serialize(),
		success: function( response ) {
			if(response == "1"){
				document.getElementById("div_loading").innerHTML = "<h3>Record Saved Successfully . . .</h3>";
			}else{
				document.getElementById("div_loading").innerHTML = "<h3>Rocord Not Saved. Try Again Later . . .</h3>";
			}
			setTimeout("window.location.reload(true);",2000);
			
			window.location.reload(true);
		}
	} );
	
}

</script>

</head>

<body>

    <div class="center_header_page">
      <div class="content" >
        
            <div class="content_first content_sec_heading">
                
        <div class="content_thirdlog"><!--content_third-->
         	<div class="content_thirdlogpopup_left">
         	  
                <div class="content_third_left_second">
                	
                    <div id="divformwraper" style="display:block;">
                        <h3>Edit Report<?php
                            //echo $row['device_name'];
                        ?></h3>
                        <div class="clear"></div>
                        <form id="frmEditReport" action="saveEditReport.php" method="post">
                        	<div class="content_third_left_contactlogpro">
                                <table width="450" cellpadding="0" cellspacing="4" border="0">
                                    <tr>
                                        <td><p>Device Name : </p></td>
                                        <td><input type="text" disabled="disabled" value="<?php echo $row['device_name']; ?>" readonly="readonly" style="width: 225px;" /></td>
                                    </tr>
                                    <tr>
                                        <td><p>Subject : </p></td>
                                        <td><input type="text" disabled="disabled" value="<?php echo $row['subject']; ?>" readonly="readonly" style="width: 225px;" /></td>
                                    </tr>
                                    <tr>
                                        <td><p>Report : </p></td>
                                        <td><input type="text" disabled="disabled" value="<?php echo $row['report']; ?>" readonly="readonly" style="width: 225px;" /></td>
                                    </tr>
                                    <tr>
                                        <td><p>Date : </p></td>
                                        <td><input type="text" disabled="disabled" value="<?php echo $row['datetime']; ?>" readonly="readonly" style="width: 225px;" /></td>
                                    </tr>
                                    <tr>
                                        <td><p>Comments : </p></td>
                                        <td><textarea name="txt_comments" style="width: 225px;"><?php echo $row['comments']; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input type="hidden" name="rpt_id" value="<?php echo $row['id']; ?>" />
                                            <input type="button" name="btn_Save" value="Save" onclick="saveRecord();" style=" display:block; width:90px; height:35px; text-align:center; vertical-align:middle; background-color:transparent; background-image:url(images/button2.png); background-position:top left; background-repeat:no-repeat; color:#FFF; font-size:12px; font-weight:bold; border:0; padding:0 0 0 0;" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </form>
                    </div>
                    
                    <div id="div_loading" style="display:none; background-color:#CCC; text-align:center; opacity:0.5;filter:alpha(opacity=50); ">
                    	<img src="images/loading.gif" width="32" height="32" /><br />
                        Saving Record . . . 
                    </div>
				
              </div>
         	</div>            
         	
			
         </div><!--content_third-->
            </div>
      </div>
    
</div>
<script type="text/javascript">
 
</script>
</body>
</html>
