                    <div class="modal-header">

									<h4><?php _l('update_device_stock'); ?></h4>

								</div>

                                <form class="form-horizontal" action="<?php echo site_url("stocks/updateStock"); ?>" method="post">
									 <?php foreach ($stocks as $stock): ?>
                                    <fieldset>
                                        
										<div class="control-group">
                                        	<label class="control-label" for="phone_imei"><?php _l('device_name'); ?></label>
                                        	<div class="controls">
                                        	<select name="phone_imei" id="phone_imei" required >
												<option value="">Select Device</option>
												<?php
												foreach ($devices as $device): ?>
												<option value="<?php echo $device['phone_imei']; ?>" <?php if($device['phone_imei']==$stock['phone_imei']){ echo "selected"; } ?>><?php echo $device['device_name']; ?></option>
												<?php endforeach; ?>
                                                
											</select>
											<input name="userid" type="hidden" id="userid" value="<?php echo $stock['user_id'];?>" />
                                        	</div>
                                    	</div>
										<div class="control-group">
                                        	<label class="control-label" for="product_name"><?php _l('product_name'); ?></label>
                                        	<div class="controls">
                                        	<select name="product_name" id="product_name" required >
												<option value="">Select Product Name</option>
												<?php
												foreach ($products as $product): ?>
												<option value="<?php echo $product['product_name']; ?>" <?php if($product['product_name']==$stock['product_name']){ echo "selected"; } ?>><?php echo $product['product_name']; ?></option>
												<?php endforeach; ?>
                                                
											</select>
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="items_received" ><?php _l('items_received'); ?></label>
                                        	<div class="controls">
                                        	<input name="items_received" id="items_received" class="input-xlarge" type="number" value="<?php echo $stock['items_received'];?>" required />
                                        	</div>
                                    	</div>
										
										<div class="control-group">
                                        	<label class="control-label" for="items_issued" ><?php _l('items_issued'); ?></label>
                                        	<div class="controls">
                                        	<input name="items_issued" id="items_issued" class="input-xlarge" type="number" value="<?php echo $stock['items_issued'];?>" required />
                                        	</div>
                                    	</div>
										
                                        <div class="control-group">
                                        	<label class="control-label" for="stock_date"><?php _l('stock_date'); ?></label>
                                        	<div class="controls">
                                        	<input name="stock_date" id="stock_date" class="show_date_piker input-xlarge" type="text" value="<?php echo $stock['stock_date'];?>" required />
                                        	</div>
                                    	</div>            
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
							<?php endforeach; ?>
                        </form>
