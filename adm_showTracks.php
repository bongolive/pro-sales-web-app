<?php
session_start();
if($_SESSION['LogIn'] != 1){
	header("location: login.php");
}
error_reporting(0);
include ("connection.php");

$tracks_count = 0;

$data = "";

$start = "NO";

$user_dtl = "";

if($_SERVER['REQUEST_METHOD'] == 'GET'){
	
	if($_GET["start"] == "start"){
		$start = "YES";
	}else{
		$date = date_create($_GET['startdate']);
		$sdate = date_format($date, 'Y-m-d H:i:s');
		$date = date_create($_GET['enddate']);
		$edate = date_format($date, 'Y-m-d H:i:s');
		
		$sql = "select * from location where imei = '$_GET[device_name]' and length(imei) > 0 and date between '$sdate' and '$edate'";
	
		$res = mysql_query($sql,$con);
		
		if (!$res)
		{
			die('Error: ' . mysql_error());
		}else{
			$tracks_count = mysql_num_rows($res);
	
			if($tracks_count > 0){
				$i = 0;
				while($r = mysql_fetch_assoc($res)) {
					
					$data .= "[ '".$r['date']."' , ".$r['latitude'].", ".$r['longitude']." ] ".(($tracks_count-1 == $i ) ? "" : ", " );
					$i += 1;
				}
				
				$sql = "SELECT * from phones WHERE phone_imei = '$_GET[device_name]'";
				$res = mysql_query($sql,$con);
				
				if($r = mysql_fetch_assoc($res)){
					$user_dtl .= "'".$r["phone_imei"]."', '".$r["device_name"]."', '".$r["mobile_no"]."', '".$r["holder_name"]."' ";
				}
				
			}else{
				$tracks_count = 0;
			}
			
		}
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>footprint</title>


<style>

HTML, BODY{
	margin:0;
	padding:0;
	width:100%;
	height:100%;
}
#mapCanvas{
	width:100%;
	height:100%;
}

</style>


<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false"></script>

<script type="text/javascript">

var first_call = "<?php echo $start; ?>";


var user_dtails = [ <?php echo $user_dtl; ?> ];
var tracks = [ <?php echo $data; ?> ];

var points = [];

var min_point_distance = 20; // meters

var map;
var openedInfoWindow = null;

var startPoints = [];
var endPoints = [];
var wayPoints = [];

var directionsService = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});


function initializeMap() {

	var centerPoint = new google.maps.LatLng(-6.822924, 39.269656);

	var myOptions = {
		zoom: 5,
		center: centerPoint,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}

	map = new google.maps.Map(document.getElementById("mapCanvas"), myOptions);

	//var i;
	if(tracks.length > 0){
		var p_index = -1;
		var i = 0;
		for(i = 0; i < tracks.length; i++){
			
			if(i == 0){
				var p = new Array(tracks[i][0], '', tracks[i][1], tracks[i][2]);
				points.push(p);
				p_index++;
				
				startPoints = new google.maps.LatLng(tracks[i][1], tracks[i][2]);
				
			}else{
				if(tracks[i][1] == tracks[i-1][1] && tracks[i][2] == tracks[i-1][2] ){
					
					points[p_index][1] = tracks[i][0];
					
				}else if(google.maps.geometry.spherical.computeDistanceBetween( new google.maps.LatLng(tracks[i-1][1], tracks[i-1][2]), new google.maps.LatLng(tracks[i][1], tracks[i][2] ) ) < min_point_distance ){
					
					points[p_index][1] = tracks[i][0];
					
				}else{
					var p = new Array(tracks[i][0], '', tracks[i][1], tracks[i][2]);
					points.push(p);
					p_index++;
					
					wayPoints.push({location:new google.maps.LatLng(tracks[i][1], tracks[i][2])});
				}
			}
			
			endPoints = new google.maps.LatLng(tracks[i][1], tracks[i][2]);
			
			if(points.length > 10){
				break;
			}
			
		}
	
		if(i > 0 && wayPoints.length > 0){
			wayPoints.length = wayPoints.length-1;
		}
		
		if(points.length > 10){
			alert("There are too many Track points against your query. Please select a smaller interval.");
		}else{
			
			directionsDisplay.setMap(map);
		
		
			var request = {
				origin: startPoints,
				destination: endPoints,
				waypoints: wayPoints,
				optimizeWaypoints: true,
				travelMode: google.maps.TravelMode.DRIVING
			};
		
			directionsService.route(request, function(response, status) {
	//	alert(status);
				if (status == google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
				}
			});
			
			for(var i = 0; i < points.length; i++){
				var latlngpoint = new google.maps.LatLng( points[i][2], points[i][3]);
				
				var content = "<strong>IMEI : </strong>"+user_dtails[0]+"<br>";
				content += "<strong>Device Name : </strong>"+user_dtails[1]+"<br>";
				content += "<strong>Mobile Number : </strong>"+user_dtails[2]+"<br>";
				content += "<strong>Holder Name : </strong>"+user_dtails[3]+"<br>";
				content += "<strong>Time : </strong>"+points[i][0]+"<br>";
				
				if(points[i][1] != ''){
					var duration = "";
					
					var date1 = new Date(points[i][0].replace("-", "/").replace("-", "/"));
					var date2 = new Date(points[i][1].replace("-", "/").replace("-", "/"));
					
					var difference = date2 - date1;
					
					difference = (difference / 1000);
					difference = Math.floor( (difference / 60) );
					
					if( difference < 60){
						duration = difference + " Minutes";
					}else{
						duration = Math.floor( (difference / 60 ) ) + " Hours and " + (difference % 60 ) + " Minutes";
						
					}
					
					content += "<strong>Duration : </strong>" + duration;
					
				}
				
				//content = "<strong>Time : </strong>" + points[i][0] + ((points[i][1] == '') ? "" : " - TO - " + points[i][1]);
				
				setMarker(map, latlngpoint, content);
			}
		}
	}else{
		if(first_call == "NO"){
			alert("No Tracks data found against your query.");
		}
	}

}


function setMarker(map, latlng, content){
	
	var infowindow = new google.maps.InfoWindow({
		content: content
	});
	
	var marker = new google.maps.Marker({
		position: latlng,
		map: map
	});
	
	google.maps.event.addListener(marker, 'mouseover', function() {
		if (openedInfoWindow != null) {
			openedInfoWindow.close();
		}
		infowindow.open(map,marker);
		
		openedInfoWindow = infowindow;
		google.maps.event.addListener(infowindow, 'closeclick', function() {
			openedInfoWindow = null;
		});
	});
	
	
}


</script>


</head>

<body onload="initializeMap();">

<div id="mapCanvas">
</div>

</body>
</html>

