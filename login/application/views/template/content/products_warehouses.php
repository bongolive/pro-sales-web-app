				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('product_warehouse'); ?></h2>
					   
            </header> 

        </article>

    </div>	
    <div class="row">
 
    	 <article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l('product_warehouse'); ?></h4><h4>
                                        <a href="<?php echo site_url('products'); ?>" class="btn btn-alt" data-dismiss="modal">Back</a>
                                    </h4>
								</div>
								<div class="modal-body">

								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>

									<tr>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>
 				  <?php if ($tb_data){ 
									foreach ($tb_data as $data):  
									  
                                            $id = $table_id;
                                     ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php echo $data[$field]; ?></td>
                                        <?php endforeach; ?>
										
                                            <td> 
                                                	<a href="<?php echo site_url($view) . '/' . $data[$id]; ?>" class="btn btn-small"/><span class="icon-eye-open"/> View</a>
                                                	<a href="<?php echo site_url($edit) . '/' . $data[$id]; ?>" class="btn btn-small"/><span class="icon-pencil"/> Edit</a>
                                                	<a href="<?php echo site_url($delete) . '/' . $data[$id]; ?>" class="btn btn-small"/><span class="icon-trash" /> Delete</a>
											</td>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
								</tbody>
	</table> 
           
		</article>							
 
    	 
    	 <article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l('add_new_warehouse'); ?></h4>
								</div>
             <?php
             if($this->session->userdata('warehz')==TRUE){ ?>
                 <div class="alert alert-success fade in">
                     <button class="close" data-dismiss="alert">×</button>
                     <strong>Success!</strong>
                     Product Warehouse succefuly added
                 </div>
             <?php
             }
             ?>
								<div class="modal-body">
									<?php if($info){ ?>
										<form class="form-horizontal" method="post" action="<?php echo site_url('products/update_warehouse'); ?>">
								
								<?php	}else{ ?>
                                <form class="form-horizontal" method="post" action="<?php echo site_url('products/add_warehouse'); ?>">
                                	<?php
									}

									if($info){
 ?>
                                			<input name="wh_id" value="<?php echo $info['wh_id']; ?>" type="hidden" />
                                <?php 	} ?>
                                	
                                    <fieldset>
                                        <input name="from_warehouse" value="true" type="hidden" />
                                        <div class="control-group">
                                        	<label class="control-label" for="wh_title" ><?php _l('wh_title'); ?></label>
                                        	<div class="controls">
                                        	<input name="wh_title" id="wh_title" value="<?php
												if ($info) { echo $info['wh_title'];
												} else {echo set_value('wh_title');
												}
 ?>" class="input-large" type="text" required />
                                        	</div>
                                    	</div>

                                        <div class="control-group">
                                            <label class="control-label" for="wh_supervisor" ><?php _l('wh_supervisor'); ?></label>
                                            <div class="controls">
                                                <input name="wh_supervisor" id="wh_supervisor" value="<?php
                                                if ($info) { echo $info['wh_supervisor'];
                                                } else {echo set_value('wh_supervisor');
                                                }
                                                ?>" class="input-large" type="text" required />
                                            </div>
                                        </div>

                                    		     
								</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('products/warehouses'); ?>" class="btn btn-alt" data-dismiss="modal">Clear</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>
                        </div>                        	
                        </div>	
                        </section>
                <?php
                $psettings = array('warehz' => '');

                $this->session->unset_userdata($psettings);
                ?>

                        		
					 