<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CourierReports extends User_Controller {
	
	public $session_userid = '';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('reports_model');
        $this->load->model('devices_model');
		$this->session_userid = $this->session->userdata('userid');
	}

	

	public function index()

	{
		$condUserPerm = array('userid'=>$this->session->userdata('userid'));
		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] ="";
		$header['device_tab'] ="";
		$header['tasks_tab'] ="";
		$header['reports_tab'] ="";
		$header['deliveries_tab'] ="";
		$header['customers_tab'] ="";
		$header['boundaries_tab'] ="";
		$header['support_tab'] ="allow";
		$header['pathfinder_tab'] ="";
		$header['stocks_tab'] ="";
		$header['registrations_tab'] ="";
		
		foreach($user_permissions as $user_permission){
			if($user_permission['permission_name']=='track tab'){
				$header['track_tab'] ="allow";
			}else if($user_permission['permission_name']=='device tab'){
				$header['device_tab'] ="allow";
			}if($user_permission['permission_name']=='tasks tab'){
				$header['tasks_tab'] ="allow";
			}if($user_permission['permission_name']=='reports tab'){
				$header['reports_tab'] ="allow";
			}if($user_permission['permission_name']=='deliveries tab'){
				$header['deliveries_tab'] ="allow";
			}if($user_permission['permission_name']=='customers tab'){
				$header['customers_tab'] ="allow";
			}if($user_permission['permission_name']=='boundaries tab'){
				$header['boundaries_tab'] ="allow";
			}if($user_permission['permission_name']=='pathfinder survey tab'){
				$header['pathfinder_tab'] ="allow";
			}if($user_permission['permission_name']=='stocks tab'){
				$header['stocks_tab'] ="allow";
			}if($user_permission['permission_name']=='registrations tab'){
				$header['registrations_tab'] ="allow";
			}						
		}
		
		if($header['deliveries_tab'] !="allow"){
			redirect('profile/support');
		}
		
		
		$fields['tb_data'] = $this->reports_model->get_reportByUserId($this->session_userid);
		
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js','custom/custom-datatable.js');
        $fields['tb_name'] = 'reports_tb_name';
        $fields['row_fields'] = array('device_name','subject','report','datetime','comments');
        $fields['tb_headers'] = array('device_name','subject','report','date','comments','action');
        $header['rp_active'] = 'class="active"';
        $fields['download_btn'] = 'download_report';

        $this->load->view('template/header',$header);
        $this->load->view('template/content/table',$fields);
        $this->load->view('template/footer',$ft_data);

	}
    
    public function edit($reportid){
        
        // Refer to the edit function in the device controller
		$fields['reports'] = $this->reports_model->get_reportById($reportid);
       $this->load->view('template/content/edit_report',$fields);
    }
	
	public function update()
	{
		$data = array(
			'comments' => $this->input->post('comments')
		);
		
		$reportid = $this->input->post('reportid');
		
		$this->reports_model->update_report($data,$reportid);
		redirect('courierReports');
	}
	
	
	
	public function delete($reportid)
	{
		$result = 	$this->reports_model->delete($reportid);
		redirect('courierReports');
	}
	
	public function showmap()
	{
		$data['name'] = urldecode($this->uri->segment(4));
		$data['lat'] = $this->uri->segment(5);
		$data['lon'] = $this->uri->segment(6);
		$this->load->view('template/content/showmap',$data);
	}
	
	
	public function export()
	{
		

		$error = "";

		if($this->input->post("btn_export") !=""){
		
			if(count($this->input->post("cb_devices")) <= 0){
				$error .= "No Device Selected.";
			}else if($this->input->post("startdate") == ""){
				$error .= "<br />Start Date is Empty.";
			}else if($this->input->post("enddate") == ""){
				$error .= "<br />End Date is Empty.";
			}else{
		
				$date = date_create($this->input->post('startdate'));
				$s_date = date_format($date, 'Y-m-d H:i:s');
				$date = date_create($this->input->post('enddate'));
				$e_date = date_format($date, 'Y-m-d H:i:s');
		
				$imei = $this->input->post("cb_devices");
		
				$imei_query = "";
				for($i = 0; $i < count($imei); $i++){
					$imei_query .= "phoneid	 = '".$imei[$i]."' ".(($i < count($imei)-1) ? " OR " : "");
				}
		
			
				$this->load->library('excel');
	
	
				// Create new PHPExcel object
				//$objPHPExcel = new PHPExcel();
	
				// Set document properties
				$this->excel->getProperties()->setCreator("Foot Print Admin")
									 ->setLastModifiedBy("Foot Print Admin")
									 ->setTitle("Exported Reports from Foot Print Site")
									 ->setSubject("Exported Reports from Foot Print Site")
									 ->setDescription("Exported Reports from Foot Print Site for the User '".$this->session->userdata('username')."'")
									 ->setKeywords("Exported Reports from Foot Print Site")
									 ->setCategory("Exported Reports from Foot Print Site");			
		
				// Add some data
				$this->excel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Device Name')
					->setCellValue('B1', 'Subject')
					->setCellValue('C1', 'Report')
					->setCellValue('D1', 'Date')
					->setCellValue('E1', 'Comments')
					->setCellValue('F1', 'Latitude')
					->setCellValue('G1', 'Longitude');
				$this->excel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
		
				$result = $this->reports_model->export($s_date,$e_date,$imei_query);
				
				$index = 2;
				foreach($result  as $row)
				{
					$this->excel->setActiveSheetIndex(0)
						->setCellValue('A'.$index, $row["device_name"])
						->setCellValue('B'.$index, $row["subject"])
						->setCellValue('C'.$index, $row["report"])
						->setCellValue('D'.$index, $row["datetime"])
						->setCellValue('E'.$index, $row["comments"])
						->setCellValue('F'.$index, $row["latitude"])
						->setCellValue('G'.$index, $row["longitude"]);
						
					$index++;
				}
		
		
		
				// Rename worksheet
				$this->excel->getActiveSheet()->setTitle('Reports');
		
		
				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$this->excel->setActiveSheetIndex(0);
		
		
				// Redirect output to a client's web browser (Excel2007)
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="report_for_user-'.$this->session->userdata('username').'.xlsx"');
				header('Cache-Control: max-age=0');
		
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
				$objWriter->save('php://output');
				exit;
			}
	
		}
		
		redirect('courierReports');
		
		
	}

}



/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */
