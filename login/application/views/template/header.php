<!DOCTYPE html>

<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->

<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->

<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

	<head>

		<meta charset="utf-8">

		<title>Dashboard | ProMobile</title>

		<meta name="description" content="">

		<meta name="author" content="dewaty" >

		<meta name="robots" content="index, follow">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

       <!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->

		<!-- jQuery FullCalendar Styles -->
        <!--<link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.8.23/themes/smoothness/jquery-ui.css" />-->
        <?php get_js('libs/jquery-ui.css');?>
        <?php get_css('plugins/jquery.fullcalendar.css'); ?>
        <!-- Date range picker Styles -->
		
  <?php get_css('plugins/daterangepicker.css'); ?>

		<!-- jQuery prettyCheckable Styles -->

        <?php get_css('plugins/prettyCheckable.css'); ?>
        <?php get_css('plugins/daterangepicker.css'); ?>

		<!-- Styles -->
	<?php get_css('plugins/jquery.visualize.css'); ?>
		<?php get_css('sangoma-blue.css'); ?>

		<!-- Fav and touch icons -->

		<link rel="shortcut icon" href="favicon.ico">

		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php get_img('img/icons/apple-touch-icon-114-precomposed.png'); ?>">

		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php get_img('img/icons/apple-touch-icon-72-precomposed.png'); ?>">

		<link rel="apple-touch-icon-precomposed" href="<?php get_img('img/icons/apple-touch-icon-57-precomposed.png'); ?>">
        <style>
			/* css for timepicker */
			.ui-timepicker-div .ui-widget-header {
				margin-bottom: 8px;
			}
			.ui-timepicker-div dl {
				text-align: left;
			}
			.ui-timepicker-div dl dt {
				height: 25px;
				margin-bottom: -25px;
			}
			.ui-timepicker-div dl dd {
				margin: 0 10px 10px 65px;
			}
			.ui-timepicker-div td {
				font-size: 90%;
			}
			.ui-tpicker-grid-label {
				background: none;
				border: none;
				margin: 0;
				padding: 0;
			}

			.ui-timepicker-rtl {
				direction: rtl;
			}
			.ui-timepicker-rtl dl {
				text-align: right;
			}
			.ui-timepicker-rtl dl dd {
				margin: 0 65px 10px 10px;
			}

        </style>
		

		<!-- JS Libs -->
        <script language='JavaScript' type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js'></script>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.0/jquery-ui.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/libs/jquery.js"><\/script>')</script>



        <?php get_js('jquery-ui-timepicker-addon_2.js'); ?>
        <?php get_js('libs/modernizr.js'); ?> 
        <?php get_js('plugins/dataTables/jquery.datatables.min.js'); ?>
        <?php get_js('plugins/dateRangePicker/date.js'); ?>
        <?php get_js('plugins/dateRangePicker/daterangepicker.js'); ?>
 <?php get_js('plugins/datepicker/bootstrap-datepicker.js'); ?>
        <?php get_js('libs/selectivizr.js'); ?>
 <style type="text/css" media="screen">
	ul.pagination {
		margin: 0px;
		padding: 0px;
		height: 100%;
		overflow: hidden;
		font-size: 12px;
		font-family: sans-serif;
		float: right;
		list-style-type: none;
	}

	ul.pagination li.details {
		padding: 7px 10px 7px 10px;
		font-size: 14px;
	}

	ul.pagination li.dot {
		padding: 3px 0;
	}

	ul.pagination li {
		float: left;
		margin: 0px;
		padding: 0px;
		margin-left: 5px;
	}

	ul.pagination li:first-child {
		margin-left: 0px;
	}

	ul.pagination li a {
		color: black;
		display: block;
		text-decoration: none;
		padding: 5px 8px;
		text-decoration: none;
		border: 1px solid #BDC3C7;
		-moz-border-radius: 4px;
		border-radius: 4px;
	}

	ul.pagination li a img {
		border: none;
	}
</style>
	</head>

	<body>

		<!-- Full height wrapper -->

		<div id="wrapper">

			<div style="position: absolute;top: 7px;right: 90px;"><a href="<?php echo site_url($this -> lang -> switch_uri('sw')); ?>" style="text-decoration: none;" ><img src="<?php get_img('flags/sw_tz.png'); ?>" />&nbsp;Swahili</a> | <a href="<?php echo site_url($this -> lang -> switch_uri('en')); ?>" style="text-decoration: none;"><img src="<?php get_img('flags/en_us.png'); ?>" />&nbsp;English</a></div>

			<!-- Main page header -->

			<header id="header" class="container">

				<h1>

					<!-- Main page logo -->

					<a href="#" class="brand" style="width: 270px;" >Pro</a>

					

				</h1>

				

				<!-- User profile -->

				<div class="user-profile">

					<figure>



						<!-- User profile info -->

						<figcaption>

							<strong><a href="#" class=""><?php echo $this -> session -> userdata('username'); ?></a></strong>

							<ul>

								<li><a href="<?php echo site_url('profile'); ?>" title="Profile Settings">Profile</a></li>

								<li><a href="<?php echo site_url('profile/logout'); ?>" title="Logout">Logout</a></li>

							</ul>

						</figcaption>

						<!-- /User profile info -->



					</figure>

				</div>

				<!-- /User profile -->



				<!-- Main navigation -->

				<nav class="main-navigation">



					<!-- Responsive navbar button -->

					<div class="navbar">

						<a class="btn btn-alt btn-large btn-primary btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-home"></span> Dashboard</a>

					</div>

					<!-- /Responsive navbar button -->



					<!-- Navigation -->

						<?php
						$header = ($this->session->userdata('role') == 'agent') ? 'agent_header' : 'common_header'; 
						$this->load->view('template/'.$header);
						?>

					<!-- /Navigation -->
				</nav>
				<!-- /Main navigation -->
			</header>
