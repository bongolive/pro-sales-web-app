  
                 <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
 
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('accounts_list'); ?></h3>     
                                    
                                    <a href="<?php echo site_url('accounts/create') ?>" style="float: right;margin:10px; " class="btn btn-info" type="button" ><?php echo lang('new_account'); ?></a>                               
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="type1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr> 
                                            	  <?php foreach($tb_headers as $header){?>

										<th><?php echo lang($header); ?></th>

                                        <?php } ?>
                                            	 <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
   

									  <?php if ($tb_data){ 
									foreach ($tb_data as $data):   
                                            $id = $data[$table_id];
                                    ?>
									<tr>
										 
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php
										if ($field == 'firstname') {
											echo ucwords($data['firstname'].' '.$data['middle_name'].' '.$data['lastname']);
										} else {
											echo ucwords($data[$field]);
										}
										  	?></td>
                                        <?php endforeach; ?>
										
                                            <td>
                                            	<div class="btn-group">
                                            		
												<a href="<?php echo site_url($view) . '/' . $id; ?>" type="button" title="View" class="btn btn-info"><i class="fa fa-eye"></i></a>
												<a href="<?php echo site_url($edit) . '/' . $id; ?>" type="button"title="Edit" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
												<a href="<?php echo site_url($delete) . '/' . $id; ?>" type="button" title="Remove" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
												</div>
                                               
											</td>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td colspan="'.count($tb_headers).'"> No '.$controller.' data available <td> </tr>';
										}
									?>
                                        </tbody>
                                        
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
           