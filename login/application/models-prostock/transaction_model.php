<?php
/**
 * Created by PhpStorm.
 * User: nasznjoka
 * Date: 3/20/15
 * Time: 11:34 AM
 */

class Transaction_model extends CI_Model{
    private $tablename = "pro_transactions";
    public function __construct(){
        parent::__construct();
    }

    public function getTransactionId($data){
        $this->db->where('order_id', $data);
        $result = $this->db->get($this->tablename);
        if($result->num_rows()  >0 ){
            $j = $result->row();
            return $j->transaction_token ;
        }
    }
    public function generate_token ($len = 15)
    {

        $date = date('Y.m.d');

        $chars = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        );
        shuffle($chars);
        $num_chars = count($chars) - 1;
        $token = '';


        for ($i = 0; $i < $len; $i++)
        {
            $token .= $chars[mt_rand(0, $num_chars)];
        }
        $n = $this->getLastTokenId();
        $token = $token.$date."-".$n;
        return $token;
    }

    public function getLastTokenId(){
        $query = $this->db->get($this->tablename);
        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
    }


}