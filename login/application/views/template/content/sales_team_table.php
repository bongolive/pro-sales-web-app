				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('sales_team_details'); ?></h2>
							<?php /*if($controller =='customers/sales_team' ){ ?>
								 <a class="btn btn-alt btn-primary" data-toggle="modal" href="http://localhost/prostock2/login/en/stock/addstock" ;"="" style="float: right; padding: 8px;"><?php _l('assign_team'); ?></a>

								 <?php }else{ ?>
								 <a class="btn btn-alt btn-primary" data-toggle="modal" href="http://localhost/prostock2/login/en/stock/addstock" ;"="" style="float: right; padding: 8px;"><?php _l('assign_team'); ?></a>
								 <?php }*/
							?>
            </header> 
        </article>

    </div>	
    
      <!-- include the filtering paget-->
    
    <?php
	include_once ('filtering.php');
 ?>
    
    <div class="row">
    	 <article class="span12 data-block">
								<div class="modal-header">
									<h4><?php _l('sales_team_details'); ?></h4>
								</div>
								<div class="modal-body">
									  
								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>

									<tr><th></th>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>
 				  <?php if ($tb_data){
									foreach ($tb_data as $data):  
									  
                                            $id = $data[$table_id];

                                     ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
										<td><input type="checkbox" name="item_no[]" value="<?php echo $data[$table_id]; ?>"/></td>
                                        <?php foreach ($row_fields as $field): ?>
                                        	
										  <td><?php
										if ($field == 'stock_value') {
//											$value = $data['purchase_price'] * $data['stock_qty'];
											echo money_format('%.2n', 0);

										} elseif ($field == 'emp_status') {
											if ($data[$field] == 1) {
												echo '<span class="label label-success">Active</span>';
											} else {

												echo '<span class="label label-warning">In Active</span>';
											}
										} else {
											echo ucwords($data[$field]);
										}
 ?></td>
                                        <?php endforeach; ?>
										
                                            <td> 
                                            		<?php  if($controller =='customers/sales_team' ){    ?> 
                                            			
                                            			<a href="<?php echo site_url('customers/assign') . '/' . $id; ?>" class="btn btn-small btn-danger" title="View"><span class="icon-person"/> <?php _l('assign_customer') ?></a> 
                                            			<a href="<?php echo site_url('customers/view_team') . '/' . $id; ?>" class="btn btn-small btn-info" title="View"><span class="awe-eye-open"/><?php _l('view') ?></a>
													<?php }else{ ?> 
														
														<a href="<?php echo site_url($view) . '/' . $id; ?>" class="btn btn-small btn-success" title="View Team Sales"><span class="awe-eye-open"/> <?php _l('view_sales') ?></a>
														<a href="<?php echo site_url('stock/salesteamstock') . '/' . $id; ?>" class="btn btn-small btn-info" title="Team Stock History"><span class="awe-truck"/> <?php _l('stock_assignment') ?></a>
														<a href="<?php echo site_url('stock/add_stock_to_team') . '/' . $id; ?>" class="btn btn-small btn-danger" title="Assign Products to Team"><span class="awe-truck"/><?php _l('assign_products') ?></a>
                                                        <a href="<?php echo site_url('stock/current_stock_info') . '/' . $id; ?>" class="btn btn-small btn-info" title="Current Stock Information"><span class="awe-truck"/><?php _l('current_stock') ?></a>
												<!--<a href="<?php echo site_url('stock/team_stock_summary') . '/' . $id; ?>" class="btn btn-small btn-danger" title="View"><span class="icon-person"/>Assign</a> -->
													<?php } ?>
                                            	
                                            	 
                                                <!--<a href="<?php echo site_url($delete).'/'.$id; ?>" class="btn btn-small" > Delete</a>--> 
											</td>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td colspan="'.count($tb_headers).'"> No Sales Team available <span style="color:#C0392B;font-weight:bold" >( Please create an employee with sales role and assign a device to him/her)</span> <td> </tr>';
										}
									?>
								</tbody>
	</table> 
           
		</article>							
 
                        </div>                        	
                        </div>	
                        </section>
                        		
					 
