                        <div class="modal-header">

									<h4><?php _l('edit_device'); ?></h4>

								</div>
                                    <fieldset>
                                     <form class="form-horizontal" method="post" action="<?php echo site_url("admin/updateDevice"); ?>">
                                        <div class="modal-body">
                                        
                                        <div class="control-group">

                                        	<label class="control-label" for="device_name"><?php _l('device_name'); ?></label>

                                        	<div class="controls">

                                        	<input name="device_name" id="device_name" class="input-xlarge" type="text" value="<?php echo $devices['device_name'];?>" placeholder="<?php echo $devices['device_name'];?>" required>

                                        	<input name="deviceid" type="hidden" id="deviceid" value="<?php echo $devices['phoneid'];?>" />
                                        	</div>

                                    	</div>

                                    	<div class="control-group">

                                        	<label class="control-label" for="device_imei"><?php _l('device_imei'); ?></label>

                                        	<div class="controls">

                                        	<input name="device_imei" id="device_imei" class="input-xlarge" value="<?php echo $devices['phone_imei'];?>" placeholder="<?php echo $devices['phone_imei'];?>" type="text" required >

                                        	</div>

                                    	</div>

                                        <div class="control-group">

                                        	<label class="control-label" for="track_intv" ><?php _l('track_intv'); ?></label>

                                        	<div class="controls">

                                        	<select name="track_intv" id="track_intv" required >

												<option <?php if($devices['checking_interval']==''){ echo 'selected';} ?>></option>

                                                <option <?php if($devices['checking_interval']=='1'){ echo 'selected';} ?>>1</option>
                                                <option <?php if($devices['checking_interval']=='2'){ echo 'selected';} ?>>2</option>

												<option <?php if($devices['checking_interval']=='5'){ echo 'selected';} ?>>5</option>

												<option <?php if($devices['checking_interval']=='10'){ echo 'selected';} ?>>10</option>

												<option <?php if($devices['checking_interval']=='15'){ echo 'selected';} ?>>15</option>

												<option <?php if($devices['checking_interval']=='20'){ echo 'selected';} ?>>20</option>

                                                <option <?php if($devices['checking_interval']=='30'){ echo 'selected';} ?>>30</option>

												<option <?php if($devices['checking_interval']=='60'){ echo 'selected';} ?>>60</option>

												<option <?php if($devices['checking_interval']=='STOP'){ echo 'selected';} ?>>STOP</option>

											</select>

                                        	</div>

                                    	</div>

                                        <div class="control-group">

                                        	<label class="control-label" for="mobile"><?php _l('mobile_nmber'); ?></label>

                                        	<div class="controls">

                                        	<input name="mobile" id="mobile" class="input-xlarge" type="number" value="<?php echo $devices['mobile_no'];?>" placeholder="<?php echo $devices['mobile_no'];?>" required />

                                        	</div>

                                    	</div>

                                        <div class="control-group">

                                        	<label class="control-label" for="holder_name"><?php _l('holder_name'); ?></label>

                                        	<div class="controls">

                                    	       <input name="holder_name" id="holder_name" class="input-xlarge" value="<?php echo $devices['holder_name'];?>" placeholder="<?php echo $devices['holder_name'];?>" type="text" required />

												

                                        	</div>

                                    	</div>

                                        <div class="control-group">

                                        	<label class="control-label" for="email_reports"><?php _l('enable_email_reports'); ?></label>

                                        	<div class="controls">

                                            

                                        	   <input type="checkbox" name="email_reports" id="email_reports" value="1" <?php  if($devices['email_report']==1){ echo "Checked"; } ?> >


                                            </div>

                                    	</div>

								</div>

								<div class="modal-footer">

									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
                                    <div class="form-actions">

            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;padding: 6px 10px;">Submit</button>

                                        </div>
								</div>
                            </form>
                            </fieldset>
