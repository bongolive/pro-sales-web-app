<?php
class Employees_model extends MY_Model {
	public $table = 'employees';
	public $table_id = 'employee_id';

	function __construct() {
		parent::__construct();
	}

	public function form_validation() {

		$this -> form_validation -> set_rules('firstname', 'Firstname', 'trim|required');
		$this -> form_validation -> set_rules('lastname', 'lastname', 'trim|required');
		//$this -> form_validation -> set_rules('date_of_emp', 'Date of Employment', 'required');
		if ($this -> input -> post('username')) {
			$this -> form_validation -> set_rules('role', 'Role', 'trim|required');
		}
		$this -> form_validation -> set_rules('mobile', 'mobile', 'required|numeric|callback_checkmobile[' . $this -> input -> post('employee_id') . ']');
		//$this -> form_validation -> set_rules('email', 'email', 'valid_email|callback_checkmail[' . $this -> input -> post('employee_id') . ']');
	}

	public function get_modules($where = FALSE) {

		$this -> table = 'modules';

		$modules = $this -> read($where);
		if ($modules) {
			return $modules;
		} else {
			return FALSE;
		}

	} 

	public function save_user($data) {
		$this -> table = 'users';
		if ($this -> save($data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update_users($id, $data) {
		$this -> table = 'users';
		$this -> table_id = 'user_id';
		if ($this -> update($id, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function read_employee($where) {
		$this -> table = 'employees';
		$this -> db -> select('*,employees.employee_id as emp_id, users.device_imei as device');
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$this -> db -> join('users', 'employees.employee_id = users.employee_id', 'left');
		$this -> db -> join('devices', 'devices.device_id = users.device_imei', 'left');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function read_modules() {
		$this -> table = 'modules';

		$module = $this -> read();
		return $module;
	}

    public function api_get_all_users($data)
    {
        $this->db->where($data);
        $query = $this->db->get('pro_users');
        if($query->num_rows() > 0){
            return $query->result_array();  // its usage should be $settings = $query['settings']
        } 
    }

}
