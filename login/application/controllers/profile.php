<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends User_Controller {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
        $this->lang->load('profile');
		$this->session_userid = $this->session->userdata('userid');
	}
    
	public function index()
	{
		$condUserPerm = array('userid'=>$this->session->userdata('userid'));
		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] ="";
		$header['device_tab'] ="";
		$header['tasks_tab'] ="";
		$header['reports_tab'] ="";
		$header['deliveries_tab'] ="";
		$header['customers_tab'] ="";
		$header['boundaries_tab'] ="";
		$header['support_tab'] ="allow";
		$header['pathfinder_tab'] ="";
		$header['stocks_tab'] ="";
		$header['registrations_tab'] ="";
		
		foreach($user_permissions as $user_permission){
			if($user_permission['permission_name']=='track tab'){
				$header['track_tab'] ="allow";
			}else if($user_permission['permission_name']=='device tab'){
				$header['device_tab'] ="allow";
			}if($user_permission['permission_name']=='tasks tab'){
				$header['tasks_tab'] ="allow";
			}if($user_permission['permission_name']=='reports tab'){
				$header['reports_tab'] ="allow";
			}if($user_permission['permission_name']=='deliveries tab'){
				$header['deliveries_tab'] ="allow";
			}if($user_permission['permission_name']=='customers tab'){
				$header['customers_tab'] ="allow";
			}if($user_permission['permission_name']=='boundaries tab'){
				$header['boundaries_tab'] ="allow";
			}if($user_permission['permission_name']=='pathfinder survey tab'){
				$header['pathfinder_tab'] ="allow";
			}if($user_permission['permission_name']=='stocks tab'){
				$header['stocks_tab'] ="allow";
			}if($user_permission['permission_name']=='registrations tab'){
				$header['registrations_tab'] ="allow";
			}						
		}
		
		$message= $this->input->post('comment_question');
		$data['users'] = $this->user_model->get_userlist($this->session_userid);
		$this->load->view('template/header',$header);
		$this->load->view('template/content/profile',$data);
		$this->load->view('template/footer');
	}
	
	public function add()
	{
		
		$this->load->library('form_validation');
	
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
	
		if ($this->form_validation->run() === FALSE)
		{
			$data['errorflag'] = 1;
			$data['msg'] = "User not Inserted";
		
			//$this->load->view('template/header');
			$this->load->view('template/content/profile',$data);
			$this->load->view('template/footer');
		}
		else
		{

			$data = array(
				'email' => $this->input->post('email'),
				'password' => md5($this->input->post('password')),
				'mobile_no' => $this->input->post('mobile_no'),
				'company_name' => $this->input->post('company'),
				'name' => $this->input->post('name'),
				'username' => $this->input->post('username'),
			);
			
			$this->user_model->add_user($data);
			
			$data['users'] = $this->user_model->get_userlist($this->session_userid);
			$this->load->view('template/header');
			$this->load->view('template/content/profile',$data);
			$this->load->view('template/footer');
		}	
	}
	
	public function edit()
	{
		$data['users'] = $this->user_model->get_userlist($this->session_userid);
		
		$this->load->view('template/header');
		$this->load->view('template/edit_user',$data);
		$this->load->view('template/footer');
	}
	
	public function update()
	{
		$this->load->library('form_validation');
	
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
	
		if ($this->form_validation->run() === FALSE)
		{
			$data['errorflag'] = 1;
			$data['msg'] = "User not Inserted";
            $data['users'] = $this->user_model->get_userlist($this->session_userid);
			$this->load->view('template/header');
			$this->load->view('template/content/profile',$data);
			$this->load->view('template/footer');
		}
		else
		{
		
			$data2 = array(
				'email' => $this->input->post('email'),
				'mobile_no' => $this->input->post('mobile'),
				'company_name' => $this->input->post('company_name'),
				'name' => $this->input->post('name'),
				'username' => $this->input->post('username'),
			);
            $data['errorflag'] = 0;
			$data['msg'] = "Profile updated";
			$this->user_model->update_user($data2,$this->session_userid);
			
			$data['users'] = $this->user_model->get_userlist($this->session_userid);
			$this->load->view('template/header');
			$this->load->view('template/content/profile',$data);
			$this->load->view('template/footer');
		}	
	}
	
	public function change_password()
	{
		$this->load->library('form_validation');
	
		$this->form_validation->set_rules('password', 'Password', 'required');
	
		if ($this->form_validation->run() === FALSE)
		{
			$data['errorflag'] = 1;
			$data['msg'] = "Password not changed";
		}
		else
		{
			
			$data2 = array(
				'password' => md5($this->input->post('password'))
			);
			$data['msg'] = "Password not changed";
			$result = $this->user_model->update_user($data2,$this->session_userid);
			if ($result){
			     $data['msg'] = "Password successfully changed";
                 $data['errorflag'] = 0;
			}else{
			     $data['msg'] = "Password not changed";
                 $data['errorflag'] = 1;
			}
			$data['users'] = $this->user_model->get_userlist($this->session_userid);
			
		}	
            $this->load->view('template/header');
			$this->load->view('template/content/profile',$data);
			$this->load->view('template/footer');
	}


	public function delete($userid)
	{
		$result = 	$this->user_model->delete($userid);
		if($result != FALSE)
		{
			echo "User Deleted successfuly";
		}
		else
		{
			echo "User Not Deleted";
		}
	}
    
    public function logout(){
        $this->user_model->logout();
    	redirect('login');
    }
    
    public function support(){
        
		$condUserPerm = array('userid'=>$this->session->userdata('userid'));
		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] ="";
		$header['device_tab'] ="";
		$header['tasks_tab'] ="";
		$header['reports_tab'] ="";
		$header['deliveries_tab'] ="";
		$header['customers_tab'] ="";
		$header['boundaries_tab'] ="";
		$header['support_tab'] ="allow";
		$header['pathfinder_tab'] ="";
		$header['stocks_tab'] ="";
		$header['registrations_tab'] ="";
		
		foreach($user_permissions as $user_permission){
			if($user_permission['permission_name']=='track tab'){
				$header['track_tab'] ="allow";
			}else if($user_permission['permission_name']=='device tab'){
				$header['device_tab'] ="allow";
			}if($user_permission['permission_name']=='tasks tab'){
				$header['tasks_tab'] ="allow";
			}if($user_permission['permission_name']=='reports tab'){
				$header['reports_tab'] ="allow";
			}if($user_permission['permission_name']=='deliveries tab'){
				$header['deliveries_tab'] ="allow";
			}if($user_permission['permission_name']=='customers tab'){
				$header['customers_tab'] ="allow";
			}if($user_permission['permission_name']=='boundaries tab'){
				$header['boundaries_tab'] ="allow";
			}if($user_permission['permission_name']=='pathfinder survey tab'){
				$header['pathfinder_tab'] ="allow";
			}if($user_permission['permission_name']=='stocks tab'){
				$header['stocks_tab'] ="allow";
			}if($user_permission['permission_name']=='registrations tab'){
				$header['registrations_tab'] ="allow";
			}						
		}
		
		if($header['support_tab'] !="allow"){
			redirect('profile/support');
		}
		
		$message= $this->input->post('comment_question');
		$data['msg'] = "";
		$data['errorflag'] = 0;
		
		if(!empty($message)){
			$name =  $this->input->post('name');
 			$email =  $this->input->post('email');
  			$mobile =  $this->input->post('mobile');
 			$message = $this->input->post('comment_question');
 
 			$subject = "Contact Request from ".$email;
 			$body = "Name : ".$name."<br />Email : ".$email."<br />Mobile Number : ".$mobile."<br />Message : ".$message;
			
			$line_brake_ch = "\r\n"; // "\r\n" for windows... "\n" for Unix... "\r" for MAC
 			$sender_name = "Support";
			$sender = "info@pro.co.tz";
			$sender_organization = "Foot Prints";
			
			$email_to = "info@pro.co.tz";
			
			$message_subject = "Support Request from ".$email;
			$message_body = $body;
			
			$headers = "From: ".$sender_name." <".$sender.">".$line_brake_ch;
			$headers .= "Reply-To: ".$sender_name." <".$sender.">".$line_brake_ch;
			$headers .= "Errors-To: ".$sender_name." <".$sender.">".$line_brake_ch;
			
			$headers .= "Organization: ".$sender_organization."".$line_brake_ch;
			$headers .= "MIME-Version: 1.0".$line_brake_ch;
			$headers .= "Content-type: text/html; charset=iso-8859-1".$line_brake_ch;
			$headers .= "X-Priority: 3".$line_brake_ch;
			$headers .= "X-Mailer: PHP". phpversion() .$line_brake_ch;
			
			if(mail($email_to, $message_subject, $message_body, $headers, "-f".$sender)){
						$data['msg'] = "Email Sent successfully";
			}else{
						$data['msg'] = "Email Not Sent";
						$data['errorflag'] = 1;
			}
		}
		
        $data['users'] = $this->user_model->get_userlist($this->session_userid);
        $this->load->view('template/header',$header);
        $this->load->view('template/content/support',$data);
        $this->load->view('template/footer');
    }

}

/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */
