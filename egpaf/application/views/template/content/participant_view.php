  
  	<!-- Main content -->
	<section class="content">
		
		  <div class="box">
                                <div class="box-header">
                                	
                                	<h4 class="page-header box-title">
<?php echo lang('survey_title') ?> : <?php echo $survey['survey_title']; ?>
<small> <h4> <?php echo lang('participant_no') ?> : <?php echo $participant['participant_no']; ?> </h4>  </small>
</h4>
                                    
                                      
                                  <!--  <a href="<?php echo site_url('reports/print_participant/' . $survey['survey_id'] . '/' . $participant['participant_no']); ?>" style="margin:10px; float: right"  type="button" class="btn btn-large btn-info" ><i class="fa fa-printer"></i> <?php echo lang('print'); ?> </a>-->
                                    <a href="<?php echo site_url('reports/survey_report/' . $survey['survey_id']); ?>" style="margin:10px; float: right"  type="button" class="btn btn-large btn-primary" > <?php echo lang('back'); ?> </a>                                
                                </div><!-- /.box-header -->
                               </div>
 
<div class="row">
<div class="col-md-12">
<div class="box box-solid">
<div class="box-header">
<h3 class="box-title"><?php echo lang('questionaire_responses'); ?></h3>
 </div> 
                            <!-- Custom Tabs -->
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                 
                                	<?php 
                                
                                	 for($t=0;$t< count($sections);$t++){
                                	 	//fix the first as static
                                	 	if($t==0){ ?>
                                	<li class="active"><a href="#tab_<?php echo $sections[$t]['section_id']; ?>" data-toggle="tab"><?php echo $sections[$t]['section_title']; ?></a></li> 		
                                	 <?php 	}else{ ?>
 <li><a href="#tab_<?php echo $sections[$t]['section_id']; ?>" data-toggle="tab"><?php echo $sections[$t]['section_title']; ?></a></li>
										 

                                        <?php } } ?>
                                     
                                </ul>
                                <div class="tab-content">
                                	<?php
                                	for($t=0;$t< count($sections);$t++){
                                	 	//fix the first as static
                                	 	?>
                                    <div class="tab-pane  <?php
									if ($t == 0) { echo 'active';
									}
										?>" id="tab_<?php echo $sections[$t]['section_id']; ?>">
                                       <!-- List of questions in the first survey section --> 
                                     
                                      	
										<div class="box-body table-responsive">
                                   			 <table id="type0" class="table table-bordered table-striped">
                                       
                                   			<thead>
                                    
                                   	 		<tr> 
                                   	 			<?php foreach($tb_headers as $header){?>

											<th><?php echo lang($header); ?></th>

                                        <?php } ?>
                                            	  
                                            </tr>
                                         
                                        </thead>
                                    
                                        <tbody>
                                   <?php 
                                 
                                      if($questions){  
									foreach ($questions as $data):   
                                       if($data['survey_section']==$sections[$t]['section_id']){
                                    ?>
									<tr>
											
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php

										if ($field == 'answer_choices') {
										 
											if ($responses) {
												foreach ($responses as $key => $value) {
													if ($value['response_qn'] == $data['question_rank']) {
												//if($data['question_type']=='Closed_ended' || $data['question_type']=='Open_ended'){
													echo rtrim($value['response'],'|');
													 
												/*}else{		
														$respo = explode('|', $value['response']);
														if (count($respo) > 1) {
															echo "<ol>";
															foreach ($respo as $respz) {
																echo "<li>" . $respz . "</li>";
															}
															echo "</ol>";

														} else {
															echo $respo[0];
														}
												}*/
													}
												}

											}
											/*
											 if ($choices) {
											 echo "<ol>";
											 foreach ($choices as $answers) {
											 if ($answers['question_no'] == $data['question_id']) {
											 echo '<li>' . $answers['response_text'] . ' moja</li>';
											 }
											 }
											 echo "</ol>";
											 *
											 *
											 }
											 *
											 */
										} else {
											echo ucwords($data[$field]);
										}
 										  	?></td>
                                        <?php endforeach; ?>
										</tr>
										<?php
										}

										endforeach;

										}
 ?>
                                     </tbody>
                                     </table>
                                   
                                       
                                    </div><!-- /.tab-pane -->
                          
                                    </div><!-- /.tab-content -->
                                           <?php     } ?>
                              
                            </div><!-- nav-tabs-custom -->
                        </div><!-- /.col -->
                              
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
          