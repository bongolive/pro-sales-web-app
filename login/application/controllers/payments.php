<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Payments extends User_Controller {

	public $user_id;
	public $account_id;
	public $settings;

	public function __construct() {
		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('order_id', 'order_date', 'business_name', 'contact_person', 'sales_total', 'paid_amount', 'balance');
		parent::__construct();
		$this -> fields['ft_data'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js');

		$this -> load -> model('payments_model');
		$this -> load -> model('permissions_model');
		$this -> load -> model('settings_model');
        $this -> load -> model('transaction_model') ;
		$this -> payments_model -> form_validation();
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');
		$this -> fields['view'] = 'payments/view/';
		$this -> fields['pay'] = 'payments/make_payments/';
		$this -> fields['delete'] = 'payments/delete/';
		$this -> fields['edit'] = 'payments/update/';

		$this -> fields['table_id'] = $this -> payments_model -> table_id;

		$this -> fields['tb_name'] = 'All Payments';
		$this -> fields['controller'] = 'payments';
		$this -> user_id = $this -> session -> userdata('user_id'); ;
		$this -> account_id = $this -> session -> userdata('account_id');

		//filtering the property items according to companyies

		/******************** check user permissions ***************************/

		$modules = explode(',', $this -> session -> userdata('permissions'));
		//$this -> fields['is_allowed'] = $this -> permissions_model -> read_modules($modules);

		/******************** check user permissions ***************************/

		//settings
		$settings = $this -> settings_model -> read(array('account_id' => $this -> account_id));
		foreach ($settings as $settings) {
			$this -> fields['settings'] = $this -> settings = $settings;
		}
	}

	public function index() {

		$this -> fields['controller'] = 'payments';

		$where = array('sales_orders.account_id' => $this -> account_id);

		if ($this -> input -> post('filter')) {
			extract($_POST);
            if ($order_date) {

                $dd = explode('-', $order_date);

                $where['order_date >='] = $dd[0];
                $where['order_date <='] = $dd[1];
            }
			/*if ($product_name) {
				$where['product_id'] = $product_name;
			}*/
			if ($business_name) {
				$where['business_name'] = $business_name;
			}
			$this -> session -> set_userdata($where);
		}

		$tb_data = $this -> payments_model -> read_orders($where);

		if ($tb_data) {

			$this -> fields['tb_data'] = $tb_data;

		} else {
			$this -> fields['tb_data'] = FALSE;
		}

		//checking users permissions
		$this -> load -> view('template/header', $this -> fields);
		$this -> load -> view('template/content/payments_table');
		$this -> load -> view('template/footer');

	}

	public function export() {
		$row_fields = array('order_id', 'order_date', 'business_name', 'contact_person', 'sales_total', 'paid_amount', 'balance');
		$where = array('sales_orders.account_id' => $this -> account_id);

		IF ($this -> session -> userdata('order_date')) {
			$where['order_date'] = $order_date;
		}
		IF ($this -> session -> userdata('product_name')) {
			$where['product_id'] = $product_name;
		}
		IF ($this -> session -> userdata('business_name')) {
			$where['business_name'] = $business_name;
		}

		$tb_data = $this -> payments_model -> read_orders($where);

		if ($tb_data) {
			$rows = $details = array();
			foreach ($tb_data as $data) :
				 
				foreach ($row_fields as $field) :
					if($field =='balance'){
						$details[$field] = $data['sales_total'] - $data['paid_amount'];
					}else{
					$details[$field] = $data[$field];
					}
				endforeach;
				$rows[] = $details;
			endforeach;
			/*
			echo "<pre>";
			print_r($rows); */
			$this -> excel_export($rows, "Paymets");
		} else {
			redirect('payments');
		}

	}

	public function make_payments() {

		if ($this -> form_validation -> run() == FALSE) {
			$this -> fields['order_hd'] = $this -> fields['tb_headers'] = array('business_name', 'order_date', 'payment_status', 'order_status', 'delivery_date','trans_uniq_id');

			$order = $this -> uri -> segment(4);

            $transactiondetails = $this->transaction_model->getTransactionId($order);
//            $this->fields['transactionvalue'] = $transactiondetails;

            $info2 = array('trans_uniq_id' => $transactiondetails);

			$this -> fields['controller'] = 'payments';

			$where = array('sales_orders.order_id' => $order);

			$info = $this -> payments_model -> read_orders($where);

			if ($info) {
				foreach ($info as $info) {
				}
				$this -> fields['info'] = $info;
                $this -> fields['info']['trans_uniq_id'] = $transactiondetails;

			} else { $this -> fields['info'] = FALSE;
			}
			$this -> fields['trans'] = FALSE;

			//settings
			$settings = $this -> settings_model -> read(array('account_id' => $this -> account_id));
			foreach ($settings as $settings) {
				$this -> fields['settings'] = $settings;
			}
			$paymode = $this -> payments_model -> pay_modes(array('account_id' => $this -> account_id));
			if ($paymode) { $this -> fields['pay_modes'] = $paymode;
			} else { $this -> fields['pay_modes'] = array('Cash' => 'Cash');
			}


			//get all transactions for this order
			$transactions = $this -> payments_model -> readTransactions(array('order_id' => $info['order_id']));
			if ($transactions) {
				$this -> fields['transactions'] = $transactions;
			} else {
				$this -> fields['transactions'] = FALSE;
			}

            $this->fields['error_message'] = $this->session->flashdata('error');

			//checking users permissions
			$this -> load -> view('template/header', $this -> fields);
			$this -> load -> view('template/content/payment_form');
			$this -> load -> view('template/footer');

		} else {
			echo "<pre>";
			extract($_POST);

			//get the outstanding balance
            $transid = $this->transaction_model ->generate_token(2);

			$info = $this -> payments_model -> read(array('order_id' => $order_id));
			print_r($info);
			$tota_paid = ($info[0]['paid_amount'] + $paid_amount);
			$balance = $info[0]['sales_total'] - $tota_paid;

			//update order
			$order = array('paid_amount' => $tota_paid);
			print_r($order);
            if($balance >= 0  && $paid_amount > 0) {
                if ($this->payments_model->update_order($info[0]['order_id'], $order)) {
                    //saving transaction

                    $trans = array('trans_date' => date('Y-m-d h:i:s'), 'order_id' => $order_id, 'trans_amount' => $paid_amount,
                        'old_payments' => $info[0]['paid_amount'], 'trans_pay_mode' => $payment_mode,
                        'created_by' => $this->user_id, 'account_id' => $this->account_id, 'transaction_token' => $transid);
                    print_r($trans);
                    $this->payments_model->saveTransaction($trans);

                }
                redirect('payments/make_payments/' . $order_id);
            }  else {
                $msg = "You have overpaid. The Transaction is rejected";
                $this->session->set_flashdata('error',$msg);
                redirect('payments/make_payments/' . $order_id);
            }
		}

	}

	public function update() {

		if ($this -> form_validation -> run() == FALSE) {
			$this -> fields['order_hd'] = $this -> fields['tb_headers'] = array('business_name', 'order_date', 'payment_status', 'order_status', 'delivery_date');

			$id = $this -> uri -> segment(4);

			$where = array('trans_id' => $id);
			$trans = $this -> payments_model -> readTransactions($where);

			if ($trans) {
				foreach ($trans as $trans) {
				}
				$this -> fields['trans'] = $trans;
				$this -> fields['controller'] = 'payments';

				$where = array('sales_orders.order_id' => $trans['order_id']);

				$info = $this -> payments_model -> read_orders($where);

				if ($info) {
					foreach ($info as $info) {
					}
					$this -> fields['info'] = $info;

				} else { $this -> fields['info'] = FALSE;
				}
				$this -> fields['pay_info'] = FALSE;

				//settings
				$settings = $this -> settings_model -> read(array('account_id' => $this -> account_id));
				foreach ($settings as $settings) {
					$this -> fields['settings'] = $settings;
				}
				$paymode = $this -> payments_model -> pay_modes(array('account_id' => $this -> account_id));
				if ($paymode) { $this -> fields['pay_modes'] = $paymode;
				} else { $this -> fields['pay_modes'] = array('Cash' => 'Cash');
				}

				//get all transactions for this order
				$transactions = $this -> payments_model -> readTransactions(array('order_id' => $info['order_id']));
				if ($transactions) {
					$this -> fields['transactions'] = $transactions;
				} else {
					$this -> fields['transactions'] = FALSE;
				}
				//checking users permissions
				$this -> load -> view('template/header', $this -> fields);
				$this -> load -> view('template/content/payment_form');
				$this -> load -> view('template/footer');

			} else {
				redirect('payments');
			}

		} else {

			extract($_POST);

			//get the outstanding balance

			$where = array('trans_id' => $trans_id);
			$info = $this -> payments_model -> readTransactions($where);
			print_r($info);
			$tota_paid = ($info[0]['old_payments'] + $paid_amount);
			//$balance = $info[0]['sales_total'] - $tota_paid;

			//update order
			$order = array('paid_amount' => $tota_paid);
			if ($this -> payments_model -> update_order($info[0]['order_id'], $order)) {
				//saving transaction

				$trans = array('last_updated' => date('Y-m-d h:i:s'), 'order_id' => $info[0]['order_id'], 'trans_amount' => $paid_amount, 'trans_pay_mode' => $payment_mode, 'created_by' => $this -> user_id, 'account_id' => $this -> account_id);

				print_r($trans);
				$this -> payments_model -> updateTransactions($info[0]['trans_id'], $trans);

			}

			redirect('payments/make_payments/' . $info[0]['order_id']);
		}
	}

	public function delete($id) {
		$data = array('trans_status' => 0);
		$this -> payments_model -> updateTransactions($id, $trans);
		redirect('payments');

	}

}
