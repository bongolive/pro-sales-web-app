<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealAddBoundary" class="modal fade hide" style="width: 800px;margin-left:-385px;">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
                <form id="form-delete-confirm" class="form-horizontal" action="<?php echo site_url("tasks/delete"); ?>" method="post">
                <fieldset>
                        <div class="modal-body">                                        
                            <p><?php _l('are_you_sure'); ?>?</p>
                        </div>
                        <div class="modal-footer" style="height: 15px;">
									<a href="#" class="btn btn-alt" data-dismiss="modal">No</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;padding: 6px 10px;margin-left: -46px;">Yes</button>
                                    </div>
								</div>
                </fieldset>
                </form>
            </div>
</div>
<section class="container" role="main">

    <div class="row">

        <article class="span12 data-block">

        

				<header>

				    <h2><span class="icon-folder-open" style="padding-right:10px;"></span><?php _l($tb_name); ?></h2>
                    <?php if(isset($download_btn)){?>
                    <a class="btn btn-alt btn-primary" data-toggle="modal" href="#downloadModal" style="float: right; padding:8px;"><?php _l($download_btn); ?></a>
                    <?php }if(isset($add_btn)){ ?>
                    <a class="btn btn-alt btn-primary" data-toggle="modal" href="#demoModal" style="padding:8px;float: right;"><?php _l($add_btn); ?></a>
                    <?php }?>
                    
                    
					
					
					<?php if ($tb_name == 'stocks_tb_name'){?>
					
					<div class="modal fade hide" id="demoModal">
								<div class="modal-header">
									<h4><?php _l('add_new_stock'); ?></h4>
								</div>
								<div class="modal-body">
                                <form class="form-horizontal" method="post" action="<?php echo site_url('stocks/add');?>">
                                    <fieldset>
                                        <div class="control-group">
                                        	<label class="control-label" for="product_name"><?php _l('product_name'); ?></label>
                                        	<div class="controls">
                                        	<select name="product_name" id="product_name" required >
												<?php
												foreach ($products as $product): ?>
												<option value="<?php echo $product['product_name']; ?>"><?php echo $product['product_name']; ?></option>
												<?php endforeach; ?>
                                                
											</select>
                                        	</div>
                                    	</div>
                                    	<div class="control-group">
                                        	<label class="control-label" for="device_imei"><?php _l('device_imei'); ?></label>
                                        	<div class="controls">
                                        	<select name="product_name" id="product_name" required >
												<?php
												foreach ($devices as $device): ?>
												<option value="<?php echo $device['phone_imei']; ?>"><?php echo $device['device_name']; ?></option>
												<?php endforeach; ?>
                                                
											</select>
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="items_received" ><?php _l('items_received'); ?></label>
                                        	<div class="controls">
                                        	<input name="items_received" id="items_received" class="input-xlarge" type="number" required />
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="stock_date"><?php _l('stock_date'); ?></label>
                                        	<div class="controls">
                                        	<input name="stock_date" id="stock_date" class="input-xlarge" type="text" required />
                                        	</div>
                                    	</div>            
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </div>
					
					
					<?php }else if ($tb_name == 'device_tb_name'){?>
                    <div class="modal fade hide" id="demoModal">
								<div class="modal-header">
									<h4><?php _l('add_new_device'); ?></h4>
								</div>
								<div class="modal-body">
                                <form class="form-horizontal" method="post" action="<?php echo site_url('device/add');?>">
                                    <fieldset>
                                        <div class="control-group">
                                        	<label class="control-label" for="device_name"><?php _l('device_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="device_name" id="device_name" class="input-xlarge" type="text" placeholder="" required>
                                        	</div>
                                    	</div>
                                    	<div class="control-group">
                                        	<label class="control-label" for="device_imei"><?php _l('device_imei'); ?></label>
                                        	<div class="controls">
                                        	<input name="device_imei" id="device_imei" class="input-xlarge" type="text" required >
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="track_intv" ><?php _l('track_intv'); ?></label>
                                        	<div class="controls">
                                        	<select name="track_intv" id="track_intv" required >
												<option></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
												<option value="5">5</option>
												<option value="10">10</option>
												<option value="15">15</option>
												<option value="20">20</option>
                                                <option value="30">30</option>
												<option value="60">60</option>
												<option value="stop">STOP</option>
											</select>
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="mobile"><?php _l('mobile_nmber'); ?></label>
                                        	<div class="controls">
                                        	<input name="mobile" id="mobile" class="input-xlarge" type="number" required />
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="holder_name"><?php _l('holder_name'); ?></label>
                                        	<div class="controls">
                                    	       <input name="holder_name" id="holder_name" class="input-xlarge" type="text" required />
												
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="email_reports"><?php _l('enable_email_reports'); ?></label>
                                        	<div class="controls">
                                            
                                        	   <input type="checkbox" name="email_reports" value="1" id="email_reports" name="email_reports">
                                        	   
                                           
                                            </div>
                                    	</div>
                                                    
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </div>
                        <?php }elseif($tb_name == 'tasks_tb_name'){?>
                        <div class="modal fade hide" id="demoModal">
                        <div class="modal-header">
									<h4><?php _l('add_task'); ?></h4>
                                    
								</div>
								<div class="modal-body">
                                <form class="form-horizontal" action="<?php echo site_url("tasks/add"); ?>" method="post">
                                    <fieldset>
                                        <div class="control-group">
                                        	<label class="control-label" for="device_name" ><?php _l('device_name'); ?></label>
                                        	<div class="controls">
                                        	<select name="device_name" id="device_name" required >
												<option></option>
                                                <?php foreach ($devices as $device): ?>
                                                <option value="<?php echo $device['phoneid'];?>"><?php echo $device['device_name'];?></option>
												<?php endforeach; ?>
											</select>
                                        	</div>
                                    	</div>
                                    	<div class="control-group">
                                        	<label class="control-label" for="subject"><?php _l('subject'); ?></label>
                                        	<div class="controls">
                                        	<input name="subject" id="subject" class="input-xlarge" type="text" required >
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="task" ><?php _l('task'); ?></label>
                                        	<div class="controls">
                                        	   <textarea id="task" name="task" class="input-xlarge" rows="5"></textarea>
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="status"><?php _l('status'); ?></label>
                                        	<div class="controls">
                                        	<select name="status" id="status" required >
												<option></option>
                                                <option value="New"><?php _l('new'); ?></option>
												<option value="In Progress"><?php _l('in_progress'); ?></option>
												<option value="Done"><?php _l('done'); ?></option>
											</select>
                                        	</div>
                                    	</div>
                              
                                        <div class="control-group">
                                        	<label class="control-label" for="comments"><?php _l('comments'); ?></label>
                                        	<div class="controls">
                                            <label class="checkbox custom-checkbox">
                                        	   <input type="text" name="comments" id="comments" >
                                            </label>
                                            </div>
                                    	</div>
                                                    
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </div>
                        <!-- for downloading report -->
                        <div class="modal fade hide" id="downloadModal">
                            <div class="modal-header">
                                <h4><?php _l('download_task'); ?></h4>
                            </div>
                            
                            <?php
                                $devices = $this->devices_model->get_devicelistbyuserid($this->session->userdata('userid'));
                                if ($devices){
                            ?>
                                <form class="form-horizontal" action="<?php echo site_url("tasks/export"); ?>" method="post" >
                                    <div class="modal-body">
                                    <div class="control-group">
                                            	<label class="control-label"><?php _l('device_name'); ?></label>
                                            	<div class="controls" style="padding-left: 26px;height: 100px;overflow: auto;width: 235px;">
                                                    <?php 
                                                    foreach($devices as $device){ ?>
                                                        <label class="checkbox custom-checkbox"> 
											                 <input id="<?php echo 'cb'.$device['phoneid']; ?>" type="checkbox" name="cb_devices[]" value="<?php echo $device['phoneid']; ?>" checked="checked" />
											                 <?php echo $device['device_name']; ?>
                                                        </label>
                                                   <?php } ?> 
                                            	</div>
                           	        </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="startdate"><?php _l('start_date'); ?></label>
                                            	<div class="controls">
                                            	   <input class="show_date_piker" type="text" name="startdate" id="startdate" placeholder="<?php _l('all_dates'); ?>" />
                                            	</div>
                                    </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="enddate"><?php _l('end_date'); ?></label>
                                            	<div class="controls">
                                            	   <input class="show_date_piker" type="text" name="enddate" id="enddate" placeholder="<?php _l('all_dates'); ?>" />
                                            	</div>
                                    </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="txttask"><?php _l('txt_task'); ?></label>
                                            	<div class="controls">
                                            	   <input name="txttask" id="txttask" class="input-xlarge" type="text"  value="" placeholder="<?php _l('all_task'); ?>" >
                                            	</div>
                                    </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="subject"><?php _l('subject'); ?></label>
                                            	<div class="controls">
                                            	   <input name="subject" id="subject" class="input-xlarge" type="text"  value="" placeholder="<?php _l('all_subject'); ?>" >
                                            	</div>
                                    </div>
                                
                            </div>
                            <?php }else{ ?>
                            <p><?php _l('no_reports_to_download');?></p>
                            <?php }?>
                            <div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
                                    <?php if($devices){?>
									<div class="form-actions">
            				                <button name="btn_export" value="1" class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;"  >Submit</button>
                                    </div>
                                    <?php } ?>
                            </div>
                            </form>
                        </div>
                        <?php }elseif($tb_name == 'boundaries_tb_name'){ ?>
                        <div class="modal fade hide" id="demoModal" style="width: 800px;margin-left:-385px;">
                                <div class="modal-header">
									<h4><?php _l('add_boundary'); ?></h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 97%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
								</div>

                            <div class="modal-body">
                            <iframe src="<?php echo site_url('boundaries/edit/add'); ?>" width="100%" height="350px" scrolling="no" frameborder="0"></iframe>
                            <input id="show_valid_to" type="text" class="input-medium" style="position: absolute;top: 220px;left: 135px;"/>
                            <input id="show_valid_from" type="text" class="input-medium" style="position: absolute;top: 173px;left: 133px;"/>
                            </div>
                        </div>
                        <div class="modal fade hide" id="revealEditBoundary" style="width: 800px;margin-left:-385px;">
                                <div class="modal-header">
									<h4><?php _l('edit_boundary'); ?></h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 97%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
								</div>

                            <div class="modal-body">
                            <iframe id="editBoundary" src="" width="100%" height="350px" scrolling="no" frameborder="0"></iframe>
                            <input id="show_edit_valid_to"  type="text" class="input-medium" style="position: absolute;top: 220px;left: 135px;"/>
                            <input id="show_edit_valid_from" type="text" class="input-medium" style="position: absolute;top: 173px;left: 133px;"/>
                            </div>
                        </div>
                        <script>

		                  	$(document).ready(function(){
		                  	       $('#show_valid_to').datetimepicker();
                                   $('#show_valid_from').datetimepicker();
                                   $('#show_edit_valid_to').datetimepicker();
                                   $('#show_edit_valid_from').datetimepicker();
                                 });
                 	   </script>
                       <?php }elseif($tb_name == 'customers_tb_name'){?>
      
                       
                       
                       <div class="modal fade hide" id="demoModal">
                        <div class="modal-header">
									<h4><?php _l('new_customer'); ?></h4>
								</div>
								<div class="modal-body">
                                <form class="form-horizontal" action="<?php echo site_url("customers/add"); ?>" method="post">
                                    <fieldset>
                                    	<div class="control-group">
                                        	<label class="control-label" for="customername"><?php _l('customer_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="customername" id="customername" class="input-xlarge" type="text" required >
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="mobilenumber" ><?php _l('mobile_number'); ?></label>
                                        	<div class="controls">
                                        	   <input name="mobilenumber" id="mobilenumber" class="input-xlarge" type="number" required >
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="email"><?php _l('email'); ?></label>
                                        	<div class="controls">
                                        	<input name="email" id="email" class="input-xlarge" type="email" required >
                                        	</div>
                                    	</div>
                              
                                        <div class="control-group">
                                        	<label class="control-label" for="address"><?php _l('address'); ?></label>
                                        	<div class="controls">
                                            <input name="address" id="address" class="input-xlarge" type="text" >
                                            </div>
                                    	</div>
                                                    
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </div>
                    
                        <?php }elseif($tb_name == 'reports_tb_name'){?>
                  
                            <!-- for downloading report -->
                        <div class="modal fade hide" id="downloadModal">
                            <div class="modal-header">
                                <h4><?php _l('download_report'); ?></h4>
                            </div>
                            
                            <?php
                                $devices = $this->devices_model->get_devicelistbyuserid($this->session->userdata('userid'));
                                if ($devices){
                            ?>
                                <form class="form-horizontal" action="<?php echo site_url("reports/export"); ?>" method="post" >
                                    <div class="modal-body">
                                    <div class="control-group">
                                            	<label class="control-label"><?php _l('device_name'); ?></label>
                                            	<div class="controls" style="padding-left: 26px;height: 100px;overflow: auto;width: 235px;">
                                                    <?php 
                                                    foreach($devices as $device){ ?>
                                                        <label class="checkbox custom-checkbox"> 
											                 <input id="<?php echo 'cb'.$device['phoneid']; ?>" type="checkbox" name="cb_devices[]" value="<?php echo $device['phoneid']; ?>" checked="checked" />
											                 <?php echo $device['device_name']; ?>
                                                        </label>
                                                   <?php } ?> 
                                            	</div>
                           	        </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="startdate"><?php _l('start_date'); ?></label>
                                            	<div class="controls">
                                            	   <input class="show_date_piker" type="text" name="startdate" id="startdate" placeholder="<?php _l('all_dates'); ?>" />
                                            	</div>
                                    </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="enddate"><?php _l('end_date'); ?></label>
                                            	<div class="controls">
                                            	   <input class="show_date_piker" type="text" name="enddate" id="enddate" placeholder="<?php _l('all_dates'); ?>" />
                                            	</div>
                                    </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="txttask"><?php _l('report'); ?></label>
                                            	<div class="controls">
                                            	   <input name="txttask" id="txttask" class="input-xlarge" type="text"  value="" placeholder="<?php _l('all_report'); ?>" >
                                            	</div>
                                    </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="subject"><?php _l('subject'); ?></label>
                                            	<div class="controls">
                                            	   <input name="subject" id="subject" class="input-xlarge" type="text"  value="" placeholder="<?php _l('all_subject'); ?>" >
                                            	</div>
                                    </div>
                                
                            </div>
                            <?php }else{ ?>
                            <p><?php _l('no_reports_to_download');?></p>
                            <?php }?>
                            <div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
                                    <?php if($devices){?>
									<div class="form-actions">
            				                <button name="btn_export" value="1" class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;"  >Submit</button>
                                    </div>
                                    <?php } ?>
                            </div>
                            </form>
                        </div>
                        
                        <?php }?>
				</header>

                

                <section>

                    <table class="datatable table table-striped table-bordered table-hover" id="example-2">

                    <thead>

									<tr>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php }?>
									</tr>
								</thead>

								<tbody>

							 
									  <?php if (isset($tb_data)){ 
									foreach ($tb_data as $data):  
									 if (isset($data['id'])){
                                            $id = $data['id'];
                                    }elseif(isset($data['phoneid'])){
                                            $id = $data['phoneid'];
                                    }else{
                                        $id = '';
                                    }?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php echo $data[$field];?></td>
                                        <?php endforeach; ?>
										
                                            <td>
                                                <?php if ($tb_name == 'device_tb_name'){?>
                                                <a onclick="editContent('<?php echo site_url('device/edit/'.$data['phoneid']);?>');" data-toggle="modal" href="#revealModal" style="text-decoration: none;padding: 10px;" >
												<span class="icon-pencil" id="<?php echo site_url('device/edit/'.$data['phoneid']);?>"></span></a>
												<a data-toggle="modal" href="#revealDelete" style="text-decoration: none;"><span class="icon-trash" id="<?php echo site_url('device/delete/'.$data['phoneid']);?>" ></span></a>
                                                <?php }elseif($tb_name == 'tasks_tb_name'){?>
                                                <a onclick="editContent('<?php echo site_url('tasks/edit/'.$data['id']);?>');" data-toggle="modal" href="#revealModal" style="text-decoration: none;padding: 10px;"><span class="icon-pencil" id="<?php echo site_url('tasks/edit/'.$data['id']);?>" ></span></a></button>
												<a data-toggle="modal" href="#revealDelete" style="text-decoration: none;"><span class="icon-trash" id="<?php echo site_url('tasks/delete/'.$data['id']);?>"></span></a>
                                                <?php }elseif($tb_name == 'boundaries_tb_name'){?>
                                                <a data-toggle="modal" href="#revealModal" style="text-decoration: none;padding-left: 40px;"><span class="icon-eye-open" id="<?php echo site_url('boundaries/showboundary/'.$data['centerlat'].'/'.$data['centerlng'].'/'.$data['radius']);?>" ></span></a>
                                                <a data-toggle="modal" href="#revealEditBoundary" style="text-decoration: none;padding: 10px;"><span class="icon-pencil-boundary" id="<?php echo site_url('boundaries/edit/edit/'.$data['boundryid']);?>" ></span></a>
												<a data-toggle="modal" href="#revealDelete" style="text-decoration: none;"><span class="icon-trash" id="<?php echo site_url('boundaries/delete/'.$data['boundryid']);?>" ></span></a>
                                                <?php }elseif($tb_name == 'reports_tb_name'){?>
                                                <a data-toggle="modal" href="#revealModal" style="text-decoration: none;padding-left: 40px;"><span class="icon-eye-open" id="<?php echo site_url('reports/showmap/'.$data['device_name'].'/'.$data['latitude'].'/'.$data['longitude']);?>" ></span></a>
                                                <a onclick="editContent('<?php echo site_url('reports/edit/'.$data['id']);?>');" data-toggle="modal" href="#revealModal" style="text-decoration: none;padding: 10px;"><span class="icon-pencil" id="<?php echo site_url('reports/edit/'.$data['id']);?>" ></span></a>
												<a data-toggle="modal" href="#revealDelete" style="text-decoration: none;"><span class="icon-trash" id="<?php echo site_url('reports/delete/'.$data['id']);?>" ></span></a>
                                                <?php }elseif($tb_name == 'customers_tb_name'){?>
                                                <a onclick="editContent('<?php echo site_url('customers/edit/'.$data['id']);?>');" data-toggle="modal" href="#revealModal" style="text-decoration: none;padding: 10px;"><span  class="icon-pencil" id="<?php echo site_url('customers/edit/'.$data['id']);?>" ></span></a>
												<a data-toggle="modal" href="#revealDelete" style="text-decoration: none;"><span class="icon-trash" id="<?php echo site_url('customers/delete/'.$data['id']);?>" ></span></a>
                                                <?php } ?>
											</td>
									</tr>
									<?php endforeach; 
}else {
	echo '<tr> <td> No data available <td> </tr>';
	}									
									?>
								</tbody>
							</table>

          </section>

        </article>

   

    </div>

</section>
