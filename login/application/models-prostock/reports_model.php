<?php
class Reports_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_reportById($reportid = FALSE)
	{
		$query = $this->db->query("select reports.*,phones.device_name from reports,phones where reports.phoneid = phones.phoneid and reports.id = $reportid");
			
		return $query->row_array();
	}
	
	public function getReportsForPhones($phoneid)
	{
		$query = $this->db->get_where('reports', array('phoneid' => $phoneid,'isdelete' => 0));
		return $query->result_array();
	}
	
	
	public function get_reportByUserId($userid = FALSE)
	{
		if ($userid === FALSE)
		{
			$query = $this->db->query("select reports.*,phones.device_name,users.name from reports,phones,users,phones_users where reports.phoneid = phones.phoneid and phones.phoneid = phones_users.phoneid and phones_users.userid=users.userid");
			
		return $query->result_array();
			
		}
	
		$query = $this->db->query("select reports.*,phones.device_name,users.name from reports,phones,users,phones_users where reports.phoneid = phones.phoneid and phones.phoneid = phones_users.phoneid and phones_users.userid=users.userid and users.userid = $userid");
		return $query->result_array();
	}
	
	
	public function add_report($data)
	{
		$this->db->insert('reports', $data);
		return $this->db->insert_id();
	}
	
	public function update_report($data,$reportid)
	{
		$this->db->where('id',$reportid);
		return $this->db->update('reports', $data);
	}
	
	public function delete($reportid)
	{
		return $this->db->delete('reports', array('id' => $reportid));
	}
	
	public function export($s_date,$e_date,$imei_query)
	{
		$query = $this->db->query("SELECT p.device_name, r.* FROM reports r INNER JOIN phones p ON r.phoneid = p.phoneid WHERE r.datetime between '".$s_date."' AND '".$e_date."' AND r.phoneid IN (SELECT phoneid FROM phones WHERE ".$imei_query." )");
		return $query->result_array();
	}

}
