<?php
Doo::loadModel('base/CountryOperatorBase');

class CountryOperator extends CountryOperatorBase{
	
 }
 class User{
	    public $id;
	    public $operator_name;
		public $mcc;
    	public $mnc;
	    public $_table = 'country_operator';
	    public $_primarykey = 'id';
	    public $_fields = array('operator_name', 'mcc', 'mnc');
	    // public $_fields = array('username','password');
	}
class Getrtid{

	   /* public $prefix;
	    public $rtid;*/

	    public $_table = 'sc_prefixes';
	    // public $_primarykey = 'id';
	    // public $_fields = array('id','prefix', 'rtid');
	    public $_fields = 'id';
}

class Cost{
	public $rtid;
	public $grade;
	public $mcc;
    public $mnc;
    public $price;
    public $cost;
    public $sender_registration;
    public $default_sender;
    public $dlr;
    public $comments;
   	public $opt_out_req;
	public $_table = 'sc_route_cost';
	public $_primarykey = 'id';
	public $_fields = array('rtid','mcc','mnc','cost','price','grade','sender_registration','default_sender','dlr','comments','opt_out_req');
}
class UpdateAllCostRecords{
	public $_table = 'sc_route_cost';
	public $_primarykey = 'id';
	public $_fields = array('rtid','mcc','mnc','cost','grade');
}

class deleteAllOperatorRecords{
	public $_table = 'country_operator';
	public $_primarykey = 'id';
	public $_fields = array('id');
}