                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                        	
                          <i class="ion ion-person"></i>
                        </div>
                        <div class="pull-left info">
                          <p> 	<?php echo $this -> session -> userdata['username'] . ' - ' . $this -> session -> userdata['role_title']; ?>
									 </p>
                            <a href="<?php echo site_url('login/logout'); ?>"><i class="fa fa-circle text-success"></i>Log Out</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="<?php echo site_url('dashboard') ?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        
         <li>
                            <a href="<?php echo site_url('accounts') ?>">
                               <i class="fa fa-folder"></i> 
                                  <span><?php  echo lang('accounts') ?></span> 
                            </a> 
                        </li>
                        
                          <li>
                            <a href="<?php echo site_url('settings') ?>">
                                <i class="fa fa-gears (alias)"></i>
                                  <span><?php  echo lang('settings') ?></span> 
                            </a> 
                        </li>
                    </ul>
                </section>