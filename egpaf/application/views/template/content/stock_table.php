				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('products_stock_history'); ?></h2> 
							<a class="btn btn-alt btn-primary" data-toggle="modal" href="http://localhost/prostock2/login/en/stock/addstock" ;"="" style="float: right; padding: 8px;"><?php _l('add_stock');?></a>
							
            </header> 
        </article>

    </div>
    
    <!-- include the filtering paget-->
    
    <?php include_once('filtering.php'); ?>
    
    	
    <div class="row">
    	 <article class="span12 data-block">
								<div class="modal-header">
									<h4><?php _l('all_products_stock_history'); ?></h4>
								</div>
								<div class="modal-body">
									 
								
								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>

									<tr><th></th>

                                        <?php foreach($tb_headers as $header){?>

											<th><?php echo lang($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>
 				  <?php if ($tb_data){
									foreach ($tb_data as $data):  
									  
                                            $id = $data[$table_id];
                                     ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
										<td><input type="checkbox" name="item_no[]" value="<?php echo $data[$table_id]; ?>"/></td>
                                        <?php foreach ($row_fields as $field): ?>
                                        	
										  <td><?php
										  if($field =='value'){
										  	$value = $data['purchase_price'] * $data['stock_qty'];
										  	echo money_format('%.2n', $value);
										  			
										  }else{
										   echo $data[$field];} ?></td>
                                        <?php endforeach; ?>
										
                                            <td> 
                                                	<a href="<?php echo site_url($edit).'/'.$id; ?>"  class="btn btn-small" title="Edit" ><span class="awe-pencil"/></a>
                                                	<a href="<?php echo site_url($delete).'/'.$id; ?>"  class="btn btn-small btn-danger" title="Delete" ><span class="awe-remove"/></a>
                                                	 
											</td>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
								</tbody>
	</table> 
           
		</article>							
   
                        </div>                        	
                        </div>	
                        </section>
                        		
					 