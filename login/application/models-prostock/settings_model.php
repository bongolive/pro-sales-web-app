<?php
class Settings_model extends MY_Model {
	public $table = 'settings';
	public $table_id = 'setting_id';

	function __construct() {
		parent::__construct();
	}

	function update($account_id, $data) {
		//do update data into database
		$this -> db -> where('account_id', $account_id);
		$qry = $this -> db -> update($this -> table, $data);
		return $qry;
	}

	function update_profile($account_id, $data) {
		//do update data into database
		$this -> db -> where('account_id', $account_id);
		$qry = $this -> db -> update('accounts', $data);
		return $qry;
	}

	public function read_currency() {
		$this -> db -> select('*');
		$this -> db -> from('currency');
		$settings = $this -> db -> get() -> result_array();
		return $settings;
	}

	public function read_settings($where = FALSE) {
		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		//$this->db->join('currency','currency_id = currency');
		$settings = $this -> db -> get() -> result_array();
		return $settings;
	}

	public function read_profile($where = FALSE) {
		$this -> db -> select('*');
		$this -> db -> from('accounts');
		if ($where) {
			$this -> db -> where($where);
		}
		$settings = $this -> db -> get() -> result_array();
		return $settings;
	}

	//get_payment_mode

	public function get_payment_mode($where = FALSE) {
		$this -> db -> select('*');
		$this -> db -> from('payment_mode');
		if ($where) {
			$this -> db -> where($where);
		}
		$settings = $this -> db -> get() -> result_array();
		if ($settings) {
			return $settings;
		} else {
			return FALSE;
		}
	}

	public function update_mode($mode, $data) {

		$this -> db -> where('mode_id', $mode);
		$this -> db -> update("payment_mode", $data);

		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function add_mode($data) {
		$this -> table = 'payment_mode';
		$result = $this -> save($data);
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

    public function check_mode($where){
        $this->table = 'payment_mode';

        $this->db->where('item_id',$where);
        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function _api_get_assigned_settings($where){
        $this->db->where($where);
        $query = $this->db->get('pro_settings');
        if($query){
            $sets = array();
            $row = $query->row_array();
            $sets[] = array('include_tax' => $row['include_tax'], 'vat' => 18,
                'enable_discounts' => $row['enable_discounts'],
                'print_receipt' => $row['print_receipts'],
                'alter_sales_price' => $row['use_fixed_prices'],
                'discount_limit' => $row['discount_limit'], 'default_currency' => 'TZS',
                'use_fixed_prices' => 0, 'tracktime' => $row['tracking_interval']);

            return $sets;
        } else {
            return false;
        }
    }


    public function _api_get_assigned_paymodes($where){
        $this->db->where($where);
        $query = $this->db->get('pro_payment_mode');
        if($query){
            $modes = array();
            $row = $query->row_array();
                $modes[] = array('system_mode_id' => $row['mode_id'],
                    'mode_title' => $row['mode_name'],
                    'mode_symbol' => $row['mode_symbol']);
            return $modes;
        } else {
            return false;
        }
    }

}
?>