<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
 
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('questions_list') ?></h3>   
                                    <div class="btn-group" style="float: right;margin:10px; ">
                                    	
                                    <a href="<?php echo site_url('surveys/view_section/' . $data['survey_no'] . '/' . $data['survey_section']); ?>"   class="btn btn-primary" type="button" ><?php echo lang('back'); ?></a>	
                                 	<?php if(in_array(3, $this->user_permissions)){  ?> 
                                    	<a href="<?php echo site_url($edit) . '/' . $data['question_id']; ?>"    class="btn btn-info" type="button" ><?php echo lang('edit'); ?></a>
                                    	<a href="<?php echo site_url('questionaires/create/'.$data['survey_no'].'/'.$data['survey_section']) ?>"  class="btn btn-success" type="button" ><?php echo lang('new_question'); ?></a>
                                    	 <?php } ?>
                                    </div>
                                       
                                                                  
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                
                                    <table id="example1" class="table table-bordered table-striped">
                                        <tbody>
                                             
                                            	  <?php foreach($tb_headers as $header){
if($header =='answer_choices'){ ?>
	<tr><th><?php echo lang($header); ?></th><td><?php   
	if($answers){?>
		<table class="table">
			<col>
			<col width="30%">
			<tr><th><?php echo lang('choice_no'); ?></th><th><?php echo lang('response_text'); ?>
			 <th><?php echo lang('data_type'); ?> </th>
			 <th><?php echo lang('min_value'); ?> </th>
			 <th><?php echo lang('max_value'); ?> </th>
			 
			</th><th><?php echo lang('next_question'); ?></th><th><?php echo lang('match_with'); ?></th></tr>
			<?php 
			$a = 'A'; $t = 0;
			 
			foreach ($answers as $key => $value) { ?>
				<tr><th><?php  echo $value['choice_no']?></th><td><?php  echo ucwords($value['response_text']); ?>
						<small style="display: block;font-style: italic;font-size-adjust: 12px"> (
				<?php
				if ($transchoices) {
					//for ($tt = 0; $tt < count($transchoices); $tt++) {
						 if(isset($transchoices[$t])){
						echo $transchoices[$t]['response_text'];
						}
					//}
				}else{echo 'No Transaltion';}
						?> )
					</small>
				</td>
				<td><?php  echo ucwords($value['data_type']); ?></td>
				<td><?php  echo ucwords($value['min_value']); ?></td>
				<td><?php  echo ucwords($value['max_value']); ?></td>
				<td><?php  echo ucwords($value['next_question']); ?></td>
				<td><?php  echo ucwords($value['match_with']); ?></td> </tr>
		<?php $a++;
					$t++;
					}
			?>
		</table>
		
		
<?php 	} ?></td></tr>

<?php
}elseif($header =='question_text'){
 ?>
		
 <tr><th><?php echo lang($header); ?></th><td style=" font-size-adjust: 14px"><?php  echo ucwords($data[$header]); ?>
 	<small style="display: block;font-style: italic;font-size-adjust: 12px">( <?php
	if ($transalation) {
		echo $transalation['question_text'];
	} else { echo "no transalation";
	}
 ?> )</small>
 </td></tr>

                                        <?php
										}else{
											?>
								 <tr><th><?php echo lang($header); ?></th><td><?php  echo ucwords($data[$header]); ?></td></tr>			
									<?php 	}
									}
												   ?>
                                            	 
                                            </tr>
                                        </tbody>
                                         
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content --> 
<!---	
	options to add translations to questions
	---> 
  
	
	<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
 
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('translate_question') ?> : ( <?php  echo "Kiswahili"; ?> )</h3> 
                                 <?php if($transalation){  ?>
                                 	 <?php if(in_array(3, $this->user_permissions)){   ?>
                                 	   <label class="alert alert-info" >Edit Translation</alert> 
                                 	   	 <?php } } ?>
                                    <div class="btn-group" style="float: right;margin:10px; ">
                                 <!--   <a href="<?php echo site_url('surveys/view/'.$data['survey_no']); ?>"   class="btn btn-primary" type="button" ><?php echo lang('back'); ?></a>	
                                    	<a href="<?php echo site_url($edit).'/'.$data['question_id']; ?>"    class="btn btn-info" type="button" ><?php echo lang('edit'); ?></a>
                                    	<a href="<?php echo site_url('questionaires/create') ?>"  class="btn btn-success" type="button" ><?php echo lang('new_question'); ?></a>-->
                                    </div>
                                       
                                                                  
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
         <?php if(in_array(3, $this->user_permissions)){   
			if ($transalation) { $from_action = 'questionaires/update_translation';
			} else {
				$from_action = 'questionaires/save_translation';
			} }
  ?> 
   <?php if(in_array(3, $this->user_permissions)){  ?> 
                                	<form action="<?php echo site_url($from_action); ?>" method="post" >
                                    <input type="hidden" name="question_id" value="<?php echo $data['question_id']; ?>" />
                                  <?php  
									if ($transalation) { ?>
				  <input type="hidden" name="translation_id" d value="<?php echo $transalation['question_id']; ?>" />
										<?php } } ?>  
                                     <table id="example1" class="table table-bordered table-striped">
                                    	<col width="15%">
                                    	<col>
                                        <tbody>
                                             
                                            	  <?php
                                            	  $tb_headers = array('question_text','question_text_translation',   'answer_choices' );
                                            	  
                                            	   foreach($tb_headers as $header){
                                            	   		
														if($header =='question_text_translation'){ ?>
								<tr><th><i><?php echo lang($header); ?></i></th><td>
									<input type="text" class="form-control" value="<?php
								if ($transalation) {
									echo $transalation['question_text'];
								} else { echo "no transalation";
								}
 ?> " name="question_text" placeholder="Question Text Translation"  <?php if(!in_array(3, $this->user_permissions)){ echo "disabled" ; } ?>    /> </td></tr>					
 <?php
}elseif($header =='answer_choices'){
 ?>
	<tr><th><?php echo lang($header); ?></th><td><?php   
	if($answers){?>
		<table class="table">
			<col width="15%">
			<col width="85%">
			 <tr><th><?php echo lang('choice_no'); ?></th></th><th><?php echo lang('response_text'); ?></th></tr>
			<?php 
			$a = 'A';$t=0;
			foreach ($answers as $key => $value) { ?>
				<tr><th><?php echo $value['choice_no']; ?> :</th><td><?php  echo ucwords($value['response_text']); ?></td></tr>
				<tr><th>  <i>(<?php echo lang('translation') ?>)</i></th><td>
					<input type="hidden" name="response_no[]" value=" <?php
					if ($transchoices) {
						//print_r($transchoices);
						//for ($tt = 0; $tt < count($transchoices); $tt++) {
							if(isset($transchoices[$t])){
						 	
						 echo $transchoices[$t]['choice_id'];
							}
						//}
					}
				?> "/>
					<input type="text" class="form-control" name="response_text[]" 
					value="<?php if ($transchoices) { if(isset($transchoices[$t])){ echo $transchoices[$t]['response_text'];}} ?>" placeholder="Translation"  <?php if(!in_array(3, $this->user_permissions)){ echo "disabled" ; } ?>  /> </td></tr>
		<?php $a++;
					$t++;
					}
			?>
		</table>
		
		
<?php 	} ?></td></tr>


<?php
}else{
 ?>
 <tr><th><?php echo lang($header); ?></th><td><?php  echo ucwords($data[$header]); ?></td></tr>

                                        <?php
										}
										}
								 	   ?>
                                            	 
                                            </tr>
                                        </tbody>
                                         
                                    </table>
                                    
								<div class="modal-footer">
									 <?php if(in_array(3, $this->user_permissions)){   ?> 
									<div class="form-actions">
										 <a href="<?php echo site_url('surveys/view_section/' . $data['survey_no'] . '/' . $data['survey_section']); ?>"   class="btn btn-default" type="button" ><?php echo lang('back'); ?></a>
										  <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
                                    
                                    <?php } ?>
								</div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content --> 
	
	
	               
