<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
	
	
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l($tb_name); ?></h2>
							&nbsp; &nbsp;	<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url(); ?>" style="float: right; padding:8px; margin-left: 20px;"> <img src="<?php echo base_url('template/img/icons/excel.png'); ?>" alt="export"/></a>
					  
						<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('products/new_products');?>";" style="float: right; padding:8px;"><?php _l('new_product');?></a>
					  
            </header> 

        </article>

    </div>	
    
    <!-- include the filtering page -->
	<?php  include_once('filtering.php')?>
 
   	 
    <div class="row">
  
        <article class="span12 data-block">
 
            <section>
         		
   <table class="table table-striped table-bordered table-hover table-media">
								<thead>

								<tr>
										<th class="span1"><input id="optionsCheckbox" type="checkbox" value="option1"></th>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php if($header =='unit_sale_price'){ _l($header); echo '<span style="color:#2980B9"> ('.$this->settings['default_currency'].' )</span>'; } else{  _l($header); } ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>

							 
									  <?php if ($tb_data){ 
									foreach ($tb_data as $data):   
                                            $id = $data[$table_id];
                                    ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
											<td><input id="optionsCheckbox" type="checkbox" value="option1"></td>
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php   
										  	if($field =='product_status'){
										  	if($data[$field]==1){
										  echo '<span class="label label-success">Active</span>';		
											}else{
										  	
										  echo '<span class="label label-warning">In Active</span>';
										  }
											
											 }elseif($field =='items_on_hand'){
										  				$packs = $data['items_on_hand']/$data['package_units'];
														echo $data[$field].'<span style="color:#2980B9"> ('. round($packs,1).' '. $data['package_name'] .')</span>';
										  }else{									  
										  echo ucwords($data[$field]);
											 }
										  	?></td>
                                        <?php endforeach; ?>
										
                                            <td>
                                               <div class="btn-group">
													<a href="<?php echo site_url($view).'/'.$id; ?>" class="btn btn-small"> <span class="icon-eye-open"/> </a>
													<a href="<?php echo site_url($edit).'/'.$id; ?>" class="btn btn-small"> <span class="icon-pencil"/> </a>
													<a href="<?php echo site_url($delete).'/'.$id; ?>" class="btn btn-small"> <span class="icon-trash"/> </a>
												 </div> 						 
											</td>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td colspan="'.count($tb_headers).'"> No Products data available <td> </tr>';
										}
									?>
								</tbody>
	</table> 
          </section>

        </article>

    
    </div>

</section>
