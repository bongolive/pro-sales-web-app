<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
<link rel="stylesheet" href="bx_styles.css" type="text/css" />
<title>footprint</title>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
<link rel="stylesheet" media="all" type="text/css" href="css/pagination.css" />

<script src="js/jquery.bxSlider.js" type="text/javascript"></script>
<script type="text/javascript" src="js/commons.js"></script>
<script type="text/javascript" src="js/jquery.pagination.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    
	 $('.slider').bxSlider({
    auto: true,
    pager: true
  });
  });
</script>
<style type="text/css">

.content_third_left_contactlogpro input
{
	width:165px;	
}
.content_third_left_contactlogpro select
{
	width:165px;
}	
-->
</style>
</head>

<body>
<?php
session_start();
include('common.php');
include ("connection.php");
?>
    <div class="center_header_page">
      <div class="content" >
        
            <div class="content_first content_sec_heading">
                
                

         
        <div class="content_thirdlog"><!--content_third-->
         	<div class="content_thirdlogpopup_left">
         	  
                <div class="content_third_left_second">
                	<h3>Add Task</h3>
                    <div class="clear"></div>
					<form id="task_form" action="addedittask.php?XDEBUG_SESSION_START=dk" method="post" >
                    <div class="content_third_left_contactlogpro">					
                    	<div class="content_third_left_contactlogpro_input">
							<p>Device Name<span style="font-size:10px">(User Defined)</span></p>
							<select name="device_name" id="device_name" style="color:#000000; font-size:16px">
							<option selected value="-1">Select Device</option>
							<?php
							$sql="SELECT * FROM phones where phoneid in (select phoneid from phones_users where userid=".$_SESSION['userid'].")";
							$result = mysql_query($sql);
							while($row = mysql_fetch_array($result))
							{
							  echo "<option value={$row['phoneid']}>{$row['device_name']}</option>";
							}
							?>
							</select>						
                        </div>
                        <div class="content_third_left_contactlogpro_input">
							<p>Subject:</p><input type="text" value="" name="txt_subject" id="txt_subject" />
                        </div>					
						<div class="content_third_left_contactlogpro_input">
							<p>Task:</p><textarea  name="txt_task" id="txt_task" rows="5" cols="22" ></textarea>
							<input type="hidden" value="<?php echo $_SESSION['userid']?>" name="txt_userid" />
							<input type="hidden" value="add" name="addedit" />
                        </div>
						<div class="content_third_left_contactlogpro_input">
							<p>Status</p>
							<select name="status" id="status" style="color:#000000; font-size:16px">
								<option selected value="New">New</option>
								<option value="In-Progress">In-Progress</option>
								<option value="Done">Done</option>
							</select>						
                        </div>
                        <div class="content_third_left_contactlogpro_input">
							<p>Comments:</p><input type="text" name="txt_comments" id="txt_comments" />
                        </div>		
						
                        <div class="content_third_left_buttondevice">
                        	<a href="javascript:void(0);" onclick="checkSave();">SUBMIT</a>
                        </div>
                    </div>
                    </form>
              </div>
         	</div>            
         	
			
         </div><!--content_third-->
            </div>
      </div>
    
</div>
<script type="text/javascript">
  function checkSave(){
		
		  var sub = $("#txt_subject");
		  var task = $("#txt_task");
		var device = $("#device_name");
		
		 if(sub.val().length <= 0){
			alert("Subject should not blank");
			return false;
		 } else if(task.val().length <= 0){
			alert("Task should not blank");
			return false;
		 }else if(device.val() < 0){
			alert("Please select Device");
			return false;
		 }
		 else{
			document.getElementById('task_form').submit();
		  }
		  
	  }
</script>
</body>
</html>
