<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Super_dashboard extends CI_Controller {
	
    public $data = array();
    
    
	public function __construct()
	{
		
		parent::__construct();
        $this->data['css'] = array('bootstrap.css',
                             'bootstrap-responsive.css',
                             'bootstrap-overrides.css',
                             'ui-lightness/jquery-ui-1.8.21.custom.css',
                             'fullcalendar.css', 
                             'slate.css',
                             'slate-responsive.css',
                             'pages/calendar.css','datepicker.css',
                             'pages/reports.css'
                             );
        $this->data['js'] = array('jquery-1.7.2.min.js',
        									'plugins/excanvas/excanvas.min.js',
											'plugins/flot/jquery.flot.js',
                             		'bootstrap.js', 
                             		'plugins/fullcalendar/fullcalendar.min.js',
                             		'Slate.js',  
											'plugins/flot/jquery.flot.axislabels.js',
											'plugins/flot/jquery.flot.orderBars.js', 
									 		'plugins/flot/jquery.flot.resize.js', 
                            		'jquery-ui-1.8.21.custom.min.js',
                             		'jquery.ui.touch-punch.min.js',
                             );
       if(!$this->session->userdata('is_login') == TRUE) {
	                             	$this->session->set_userdata('login_error','Please, Loggin first');
											redirect('login');
                             	} 
                 $this->load->model('super_dashmodel');
                $this->load->model('properties_model');             	
                $this->load->model('units_model');       
                              	 
					$this->where_by=array($this->session->userdata('logid'));
	 				$where_p=array('created_by'=>$this->session->userdata('logid'));
		        	$this->data['prop']=$this->properties_model->read($where_p);                 
                 	if($this->data['prop']) {
						$w_id=array();
						foreach($this->data['prop'] as $property){ $w_id[] = $property['id']; }	
						$this->where_in=$w_id; 					
					}else { $this->where_in = array(1);}
					$where=FALSE;
					$this->data['unit']=$this->units_model->unitslist($where, $this->where_in); 	                   
	} 
	
	public function index()
	{
 		  $data['confirmed']	=		$this->super_dashmodel->confirmed($this->where_in); 
   	$data['leased']=      	$this->super_dashmodel->leased($this->where_in);
       	$data['accounts']=  			$this->super_dashmodel->accounts();
		        	$data['res']=    			$this->super_dashmodel->reservation($this->where_in);
		    
		        	$data['mauzo']		=			$this->super_dashmodel->mauzo($this->where_by); //for the graph
		        	$data['matumizi']	=		$this->super_dashmodel->matumizi($this->where_by); //for the graph
		       
		        $data['req_leased']=		$this->super_dashmodel->req_leased($this->where_in);
		        $data['pending']	=			$this->super_dashmodel->pending($this->where_in);
		        $data['sales']		=				$this->super_dashmodel->sales_count($this->where_by); 
   			 	$data['checkin']=			$this->super_dashmodel->checkin($this->where_in);
		        
		        	$data['leases_today']=	$this->super_dashmodel->leases_today($this->where_in);
		        	
    
      	$this->load->view('template/header',$this->data);
        	$this->load->view('template/content/super_dashboard',$data);
         $this->load->view('template/footer'); 
	 
	} 
	} 
 