<?php
session_start();
if($_SESSION['LogIn'] != 1){
	header("location: login.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Foot Print</title>


<script type="text/javascript">

var center_lat = "<?php echo $_GET["lat"] ?>";
var center_lng = "<?php echo $_GET["lng"] ?>";
var radius = <?php echo $_GET["radius"] ?>;

var map;

function showBoundry() {

	var centerPoint = new google.maps.LatLng(center_lat, center_lng);

	var myOptions = {
		zoom: 13,
		center: centerPoint,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}

	map = new google.maps.Map(document.getElementById("mapCanvas"), myOptions);
	
	var circleOptions = {
	  center: new google.maps.LatLng(center_lat, center_lng),
	  radius: radius,
	  map: map,
	  editable: false
	};
	var circle = new google.maps.Circle(circleOptions);
	
}


$(document).ready(function(){
	  
		showBoundry();
		
  });
</script>




</head>
<body>

<div id="mapCanvas" style="width:850px; height:500px;"></div>

</body>
</html>
