<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Controller extends MY_Controller
{

    public $session_userid = '';
    
	function __construct ()
	{
		parent::__construct();
		//$this->load->helper('form');
		$this->load->library('form_validation');
		//$this->load->library('session');
		$this->load->model('user_model');
		
		// Login check
		$exception_uris = array(
			'login', 
			'profile/logout'
		);
		if (in_array(uri_string(), $exception_uris) == FALSE) {
			if ($this->user_model->loggedin() == FALSE) {
				redirect('login');
			}
		}
	
	}
}