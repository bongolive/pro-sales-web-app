				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							
							<h2><?php _l('sales_team_stock_history'); ?></h2> 
							<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('stock/updateteamstock/'.$tb_data[0]['batch_no']) ?>" ;"="" style="float: right; padding: 8px;"><?php _l('edit'); ?></a>
							 <a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('stock/salesteamstock/'.$tb_data[0]['employee_id']) ?>" ;"="" style="float: right; padding: 8px;"><?php _l('back'); ?></a>
            </header> 
        </article>

    </div>	

    	<!-- Grid row -->
							<div class="row-fluid">
								<div class="span6">
									<div class="well">
										<h4 class="no-margin"><?php _l('team_details') ?></h4>
										<table class="table table-striped table-hover">
											<thead>
											<tr> 
												<th><?php _l('employee_name') ?></th><th><?php _l('device_imei') ?></th><th><?php _l('batch_no') ?></th>
											</tr>
											</thead>
										 	
										 	<tbody>
										 	<tr>
										 		<td><?php echo $tb_data[0]['employee_id'] ?></td><td><?php echo $tb_data[0]['device_imei'] ?></td><td><?php echo $tb_data[0]['batch_no'] ?></td>
										 	</tr>
										 	</tbody>
										 </table>
										 </div>	 </div></div> 	 
    	  <?php include_once('filtering.php') ?>
                           <div class="row"> 
                        <article class="span12 data-block">
								<div class="modal-header">
									<h4><?php _l('all_products_stock_history'); ?></h4>
								</div>
								<div class="modal-body"> 
								
								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>

									<tr>
<th>No.</th>
                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>
 				  <?php if ($tb_data){
 				  	$i = 1; 
									foreach ($tb_data as $data):  
									  
                                            $id = $table_id;
                                     ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
											<td><?php echo $i; ?></td>
                                        <?php foreach ($row_fields as $field): ?>
                                        
										  <td><?php echo $data[$field]; ?></td>
                                        <?php endforeach; ?>
										
                                            <td> 
                                            	<!--<a href="<?php echo site_url('stock/viewbatch/'.$data['batch_no']); ?>" class="btn btn-small btn-info"><span class="awe-eye-open"/> <?php _l('view');?></a>
                                                	<a href="<?php echo site_url('stock/updateteamstock/'.$data['batch_no']); ?>" class="btn btn-small"><span class="awe-pencil"/> <?php _l('edit');?></a>
                                                	<a href="<?php echo site_url('stock/delete/'.$data['batch_no']); ?>" class="btn btn-small btn-danger" ><span class="awe-trash"/> <?php _l('delete');?></a> -->
											</td>
									</tr>
									<?php
									$i++;
									 endforeach;
									
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
								</tbody>
	</table> 
           
		</article>		
                        
                        </div>                        	
                        </div>	
                        </section> 