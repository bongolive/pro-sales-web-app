<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Track extends User_Controller {
    public $user_id;
    public $account_id;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('devices_model');

        if (!$this -> session -> userdata('logged_in') == TRUE) {
            $this -> session -> set_userdata('login_error', 'Please, Loggin first');
            redirect('login');
        }

        $this->user_id = $this->session->userdata('userid');
        $this -> account_id = $this -> account_id;
	}
	
	
	public function index()
	{
		$condUserPerm = array('user_id'=>$this->user_id);
//		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
//		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
		//Make all tab not allowed
        //devices=4,track=3,stock=11,customer=9,reports=6,
        $header['track_tab'] ="allow";
        $header['device_tab'] ="allow";
        $header['tasks_tab'] ="allow";;
        $header['reports_tab'] ="allow";
        $header['deliveries_tab'] ="allow";
        $header['customers_tab']="allow";
        $header['boundaries_tab'] ="allow";
        $header['support_tab'] ="allow";
        $header['pathfinder_tab'] ="allow";
        $header['stocks_tab'] ="allow";
        $header['registrations_tab'] ="";

       /* $perms = explode(",",$user_permissions);
        $perm = array_values($perms);
        if($perm == 3){
            $header['track_tab'] ="allow";
        }


        if($header['track_tab'] !="allow"){
            redirect('profile/support');
        }*/
		
//		$fields['devices'] = $this->devices_model->get_devicelistbyuserid($this->session_userid );
        $fields['devices'] = $this->devices_model->getDeviceByAccountNo($this->account_id);
        $header['tr_active'] = 'class="active"';
        $fields['tb_name'] = 'track_tb_name';
        $this->load->view('template/header',$header);
        $this->load->view('template/content/track',$fields);
        $this->load->view('template/footer');
	}
	
	public function lastlocation()
	{
		$devices = $this->devices_model->getLastKnownLocation($this->account_id);
		if(count($devices)>0){
			$xml = "<locations>";
		
			foreach($devices as $row){

				$xml .= "<location>";
			
				$xml .= "<deviceid>".$row["device_id"]."</deviceid>";
				$xml .= "<imei>".$row["device_imei"]."</imei>";
				$xml .= "<devicename>".$row["device_name"]."</devicename>";
				$xml .= "<mobileno>".$row["mobile"]."</mobileno>";
				$xml .= "<holdername>".$row["holder_name"]."</holdername>";
				$xml .= "<latitude>".$row["latitude"]."</latitude>";
				$xml .= "<longitude>".$row["longitude"]."</longitude>";
				$xml .= "<localdate>".$row["localdate"]."</localdate>";
			
				$xml .= "</location>";
			}
		
			$xml .= "</locations>";
		
			echo $xml;
		}else{
			echo "ERROR. !";
		}
	}
	
	public function showtrack()
	{
		$tracks_count = 0;
		$data = "";
		$start = "NO";
		$user_dtl = "";
		$start = urldecode($this->uri->segment(4));
        $sdate = urldecode($this->uri->segment(5));
        $edate = urldecode($this->uri->segment(6));
        $thc = array('-', 'A');
        $replace = array('/',':');
        $sdate = str_replace($thc,$replace,$sdate);
        $edate = str_replace($thc,$replace,$edate);
        //echo $start.$sdate.$edate;
		if(($start != 'start') && $sdate && $edate){
	
			if($this->input->post('start') == "start"){
				$start = "YES";
			}else{
				$date = date_create($sdate);
				$sdate = date_format($date, 'Y-m-d H:i:s');
				$date = date_create($edate);
				$edate = date_format($date, 'Y-m-d H:i:s');
				$imei = $start;
				$res = $this->devices_model->get_deviceLocationByImei($imei,$sdate,$edate);
		
				if (count($res)==0)
				{
					//die('Error: ' . mysql_error());
                    //var_dump($res);
                    $start = "NO";
                    
				}else{
					$tracks_count = count($res);
	
					if($tracks_count > 0){
						$i = 0;
						foreach($res as $r) {
					
							$data .= "[ '".$r['date']."' , ".$r['latitude'].", ".$r['longitude']." ] ".(($tracks_count-1 == $i ) ? "" : ", " );
							$i += 1;
						}

						$r  = $this->devices_model->get_devicelistbyImei($imei);
				
						if(count($r) >0){
							$user_dtl .= "'".$r["device_imei"]."', '".$r["device_descr"]."', '".$r["device_mobile"]."', '".$r["assigned_to"]."' ";
						}
				
					}else{
						$tracks_count = 0;
					}
			
				}
			}
		}
			
        $content['start'] = $start;
		$content['user_dtl'] = $user_dtl;
		$content['data'] = $data;
		$content['user_dtl'] = $user_dtl;
		//Load shoetrack view here
		$this->load->view('template/content/showtrack',$content);
	}
    
    public function showdevices(){
        
        $this->load->view('template/content/show_devices');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
