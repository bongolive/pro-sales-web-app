<?php
class Questionaires_model extends MY_Model {
	public $table = 'questionaires';
	public $table_id = 'question_id';

	function __construct() {

		parent::__construct();
		$this -> table = 'questionaires';
		$this -> table_id = 'question_id';

	}

	public function form_validation() {
		$this -> form_validation -> set_rules('question', 'question', 'trim|required');
	}

	function save_response_choice($data) {
		$this -> table = "response_choices";
		$this -> table_id = 'choice_id';

		if ($this -> db -> insert($this -> table, $data)) {

			return True;
		} else {
			return FALSE;
		}
		echo $this -> db -> last_query();
	}

	public function save_transation($data) {
		$this -> table = 'questionaires';
		if ($this -> save($data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function update_translation($id, $data) {
		$this -> table = 'questionaires';

		//do update data into database
		$this -> db -> where('transalation_of', $id);
		if ($this -> db -> update($this -> table, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function read_survey_section($where) {
		$this -> table = 'survey_sections';
		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$results = $this -> db -> get() -> result_array();
		if ($results) {
			return $results;
		} else {
			return FALSE;
		}

	}

	public function simple_read($where = false) {

		$this -> table = 'questionaires';

		$this -> db -> select('*');

		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}

		$results = $this -> db -> get() -> result_array();
		if ($results) {
			return $results;
		} else {
			return FALSE;
		}
	}

	function simple_update_qn($id, $data) {
		//do update data into database
		$this -> table = 'questionaires';
		$this -> db -> where('question_id', $id);
		if ($this -> db -> update($this -> table, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function read_questions($where = false) {
		$this -> table = 'questionaires';
		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}

		$this -> db -> join('surveys', 'survey_id = questionaires.survey_no', 'left');
		$this -> db -> join('survey_sections', 'survey_section = section_id', 'left');
		$this -> db -> order_by('question_rank', 'asc');

		$results = $this -> db -> get() -> result_array();
		if ($results) {
			return $results;
		} else {
			return FALSE;
		}
	}

	public function read_questions_with_choices($where = false) {
		$this -> table = 'questionaires';
		$this -> db -> select('*,count(choice_id) as choices, GROUP_CONCAT(data_type) as data_type', FALSE);
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		//$this -> db -> order_by('survey_section', 'asc');
		$this -> db -> order_by('question_rank', 'asc');
		$this -> db -> group_by('response_choices.question_no');
		$this -> db -> join('survey_sections', 'survey_section = section_id', 'left');
		$this -> db -> join('response_choices', 'response_choices.question_no = question_id', 'left');
		$results = $this -> db -> get() -> result_array();
		if ($results) {
			return $results;
		} else {
			return FALSE;
		}
	}

	public function read_open($where) {
		$this -> db -> select('rank');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		$results = $this -> db -> get() -> result_array();
		if ($results) {
			return $results;
		} else {
			return FALSE;
		}
	}

	public function surveys($where) {
		$this -> table = 'surveys';
		$results = $this -> read($where);
		if ($results) {
			return $results;
		} else {
			return FALSE;
		}
	}

	public function save_answer_choices($data) {
		$this -> table = "response_choices";
		$result = $this -> save($data);
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function read_answer_options($data) {
		$this -> table = "response_choices";
		$result = $this -> read($data);
		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public function auto_update_answer_choices($id, $data) {
		$this -> table = "response_choices";
		$this -> table_id = 'transalation_of';
		if ($this -> update($id, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update_answer_choices($id, $data) {
		$this -> table = "response_choices";
		$this -> table_id = 'choice_id';
		if ($this -> update($id, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function delete_choices($choice) {
		$this -> db -> where('choice_id', $choice);
		$qry = $this -> db -> delete('response_choices');
		if ($qry) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function delete_trans_choices($choice) {
		$this -> db -> where('transalation_of', $choice);
		$qry = $this -> db -> delete('response_choices');
		if ($qry) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function delete_trans_choices2($choice) {
		$this -> db -> where('question_no', $choice);
		$qry = $this -> db -> delete('response_choices');
		if ($qry) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function get_last_qn_rank($where) {
		$this -> db -> select('max(question_rank) as rank');
		$this -> db -> from('questionaires');
		$this -> db -> where($where);
		$result = $this -> db -> get() -> result_array();

		return $result[0]['rank'];

	}

    /*
     * get all questions for this account to the user both new and updating
     *
     * */
    public function api_get_all_questions($data)
    {
//        $this->db->where($data);
        $account = $data['account_no'];
        $lasttime = $data['last_update_time'];
        $query = $this->db->query("select *,survey_section as category from pro_questionaires where account_no = '$account' && last_update_time >'$lasttime' ");
        if($query->num_rows() > 0){
            return $query->result_array();  // its usage should be $settings = $query['settings']
        }
    }

    /*
     * get all response choice to this user sna account
     * */
    public function api_get_all_response_choices($data)
    {
        $this->db->where($data);

        $query = $this->db->get('pro_response_choices');

        if($query->num_rows() > 0) {
            return $query->result_array();  // its usage should be $settings = $query['settings']
        }
    }

    /*
     * get all response choice to this user sna account
     * */
    public function api_save_participant($where,$data)
    {
        $return = false;

        foreach($data as $key => $value) {

            $wherepart = array('participant_no' => $value['participant_no'],
                'device_imei' => $where['device_imei'],'survey_no' => $value['survey_no']);
            $checkparticipants = $this->api_check_participant($wherepart);

            $data = array(
                'survey_no' => $value['survey_no'],
                'interview_date' => $value['interview_date'],
                'location_code' => $value['location_no'],
                'participant_no' => $value['participant_no'],
                'anc_number' => $value['anc'],
                'pmtct_status' => $value['status'],
                'interviewer_no' => $value['interviewer_no'],
                'device_imei' => $where['device_imei'],
                'start_time' => $value['start_time'],
                'end_time' => $value['end_time']
            );

            switch($value['ack']){
                case 0:
                    if (!$checkparticipants) {

                        $this->db->insert('pro_participants', $data);
                        if ($this->db->affected_rows()) {
                            $return = true;
                        }
                    }
                    break;
                case 2:
                    if (!$checkparticipants) {

                        $this->db->insert('pro_participants', $data);
                        if ($this->db->affected_rows()) {
                            $return = true;
                        }
                    } else {
                        $insert = array('end_time' => $value['end_time']);
                        $this->db->update('pro_participants', $insert, $wherepart);
                        if ($this->db->affected_rows()) {
                            $return = true;
                        }
                    }
                    break;
            }
        }
        return $return;
    }
/*
 *  get all response choice to this user sna account
  *
 * */
    public function api_save_responses($where,$data)
    {
        $return = false;

        foreach($data as $key => $value) {
            $this->db->trans_start();
/*
            $resparray = array('response_qn'=> $value['question_no'],'participant_no' =>
                $value['participant_id'], 'device_imei' => $where['device_imei']);*/

            $resparray = array('response_qn'=> $value['question_no'],'participant_no' =>
                $value['participant_id'], 'device_imei' => $where['device_imei'],'survey_no' => $value['survey_no']);

            $checkresponse = $this->api_check_response($resparray);

            $myanswer = $this -> my_final_answers($value,$where['account_no']);

            $location = $this-> api_get_location(array('device_imei' => $where['device_imei']));

            $data = array(
                'survey_no' => $value['survey_no'],
                'response_qn' => $value['question_no'],
                'response' => $value['response'],
                'response_no' => $myanswer,
                'location_code' => $location,
                'response_date' => $value['response_date'],
                'participant_no' => $value['participant_id'],
                'device_imei' => $where['device_imei']
            );

            switch($value['ack']){
                case 0:
                    if (!$checkresponse) {

                        $this->db->insert('pro_responses', $data);
                        if ($this->db->affected_rows()) {
                            $return = true;
                        }
                    }
                    break;
                case 2:
                    if (!$checkresponse) {

                        $this->db->insert('pro_responses', $data);
                        if ($this->db->affected_rows()) {
                            $return = true;
                        }
                    } else {
                        $this->db->where($resparray);
                        $q = $this->db->get('pro_responses');
                        if($q->num_rows() == 1){
                            $insert = array('response_no' => $myanswer,'response' => $value['response']);
                            $this->db->update('pro_responses', $insert, $resparray);
                            if ($this->db->affected_rows()) {
                                $return = true;
                            }
                        }
                    }
                    break;
                case 3:
                    $this->db->where($resparray);
                    $this->db->delete('pro_responses');
                    if ($this->db->affected_rows()) {
                        $return = true;
                    }
                    break;
            }
            $this->db->trans_complete();
        }
        return $return;
    }



    public function api_check_participant($data){
        $this->db->where($data);
        $query = $this->db->get('pro_participants');
        if($query->num_rows() > 0){
            return true;
        }
    }

    public function api_check_response($data){
        $this->db->where($data);
        $query = $this->db->get('pro_responses');
        if($query->num_rows() > 0){
            return true;
        }
    }



    public function my_final_answers($data,$account) {

        $opts = $this -> quetion_options(array('question_no' => $data['question_no'], 'survey_no' =>
            $data['survey_no'], 'account_no' => $account));

        if ($opts) {
            $mychoices = explode('||', rtrim($data['choice_no'], '||'));
            $mychoice2 = explode('||', rtrim($data['response'], '||'));

            if (array_key_exists('partial', $opts)) {

                unset($opts['partial']);
                //prepare two fields for the partial open ended i.e opt and fixed
                $partial = array(0, 0);

                //check which data is returned
                if(count($opts) > 2){
                    foreach ($opts as $choice) {

                        if ($choice['data_type'] == 3) {
                            if ($mychoices[0] == $choice['choice_no']) {

                                //$answr .= $mychoice2[0] . '|';
                                $optans = $mychoice2[0];
                            } else {
                                //$answr .= '0000-00-00|';
                                $optans = '0000-00-00';
                            }
                            $partial[1] = $optans;
                        } elseif ($choice['data_type'] == 2) {
                            if ($mychoices[0] == $choice['choice_no']) {
                                //$optans .= $mychoice2[0] . '|';
                                $optans = $mychoice2[0];
                            } else {
                                //$answr .= '0|';
                                $optans = '0';
                            }
                            $partial[1] = $optans;
                        } else {
                            if ($mychoices[0] == $choice['choice_no']) {
                                //$answr .= $mychoices[0] . '|';
                                //$optans .= $mychoices[0];
                                $partial[0] = $mychoices[0];
                            } else {
                                //$answr .= '0|';
                                //$optans .= 0;
                            }

                        }
                    }
                }else{

                    $p = 0;
                    foreach ($opts as $choice) {

                        if ($choice['data_type'] == 3) {
                            if ($mychoices[0] == $choice['choice_no']) {

                                //$answr .= $mychoice2[0] . '|';
                                $optans = $mychoice2[0];
                            } else {
                                //$answr .= '0000-00-00|';
                                $optans = '0000-00-00';
                            }
                            $partial[$p] = $optans;
                        } elseif ($choice['data_type'] == 2) {
                            if ($mychoices[0] == $choice['choice_no']) {
                                //$optans .= $mychoice2[0] . '|';
                                $optans = $mychoice2[0];
                            } else {
                                //$answr .= '0|';
                                $optans = '0';
                            }
                            $partial[$p] = $optans;
                        } else {
                            if ($mychoices[0] == $choice['choice_no']) {
                                //$answr .= $mychoices[0] . '|';
                                //$optans .= $mychoices[0];
                                $partial[$p] = $mychoices[0];
                            } else {
                                //$answr .= '0|';
                                //$optans .= 0;
                            }

                        }
                        $p++;
                    }

                }

                //get the final results for the partial open qn
                $answr = implode('|', $partial);

            } elseif (array_key_exists('open_ended', $opts)) {
                $answr = null;
                unset($opts['open_ended']);
                $ch = 0;
                foreach ($opts as $choice) {

                    if ($choice['data_type'] == 3) {
                        if (array_key_exists($ch, $mychoice2)) {
                            if (strpos($mychoice2[$ch],'-') !== false) {

                                $answr .= $mychoice2[$ch] . '|';
                            } else {
                                $answr .= '0000-00-00|';
                            }
                        } else {
                            $answr .= '0000-00-00|';
                        }

                    } elseif ($choice['data_type'] == 2) {
                        if (array_key_exists($ch, $mychoice2)) {
                            $answr .= $mychoice2[$ch] . '|';
                        } else {
                            $answr .= ' |';
                        }
                    } else {
                        if (array_key_exists($ch, $mychoice2)) {
                            $answr .= $mychoice2[$ch] . '|';
                        } else {
                            $answr .= ' |';
                        }
                    }
                    $ch++;
                }

            } else {

                $answr = '';
                $d = 0;
                foreach ($opts as $choice) {
                    if ($choice['data_type'] == 3) {
                        $answr .= $mychoice2[$d] . '|';
                    } elseif ($choice['data_type'] == 2) {
                        end($mychoice2);
                        $va = key($mychoice2);
                        $answr .= $mychoice2[$va] . '|';

                    } else {
                        if (in_array($choice['choice_no'], $mychoices)) {
                            $answr .= $choice['choice_no'] . '|';

                        } else {

                            $answr .= ' |';

                        }
                    }
                    $d++;
                }
            }
            //$answr = rtrim($answr, '|');
        } else {

            if ($data['choice_no'] == 0) {
                $answr = $data['response'];
            } else {
                $answr = $data['choice_no'];
            }
            //$answr = rtrim($answr, '|');
        }
        $answr = rtrim($answr, '|');

        return $answr;
    }

    //reading question options
    public function quetion_options($data) {
        //get question type


        //echo "select question_id,question_type  from pro_questionaires where question_rank = '" . $data['question_no'] . "' and  survey_no = '" . $data['survey_no'] . "' and account_no = '" . $data['account_no'] . "'";
        $qn = "select question_id,question_type  from pro_questionaires where question_rank = '" . $data['question_no'] . "' and  survey_no = '" . $data['survey_no'] . "' and account_no = '" . $data['account_no'] . "'";

        $question = $this -> db -> query($qn);

        if ($question->num_rows()) {

            $qn = $question->row_array();

            if ($qn['question_type'] == 'Closed_ended_mult' || $qn['question_type'] == 'Partial_open_ended_mult' || $qn['question_type'] == 'Partial_open_ended' || $qn['question_type'] == 'Open_ended') {

                $options = "select * from pro_response_choices where question_no = '" . $qn['question_id'] . "' and account_no = '" . $data['account_no'] . "'";

                $options = $this -> db -> query($options);

                $opts = array();
                if ($options->num_rows()) {

                    foreach($options->result_array() as $row) {
                        $opts[] = $row;
                    }
                }

                if ($qn['question_type'] == 'Partial_open_ended') {
                    $opts['partial'] = TRUE;
                } elseif ($qn['question_type'] == 'Open_ended') {$opts['open_ended'] = TRUE;
                }

                return $opts;
            } else {
                return FALSE;
            }
        } else {

            return FALSE;
        }

    }

    /*
     * get the location code
     * */
    private function api_get_location($data)
    {
        $this->db->where($data);
        $query = $this->db->get('pro_participants');
        if($query->num_rows()){
            $r = $query->row();
            return $r->location_code ;
        }
    }
}
