<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Product_stock extends User_Controller {

	public function __construct() {
		parent::__construct();

		$this -> fields['row_fields'] = array('stock_id', 'product_id', 'stock_date', 'stock_qty', 'purchase_price', 'supplier', 'order_no', 'created_by', 'created_on', 'last_update');
		$this -> fields['tb_headers'] = array('product_name', 'items_received', 'items_issued', 'items_returned', 'stock_date', 'action');
		$this -> fields['ft_data'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js');

		$this -> load -> model('stocks_model');
		$this->fields['table_id'] = $this->stocks_model->table_id;
		$this -> stocks_model -> form_validation();
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');

		$this -> session_userid = $this -> session -> userdata('userid');
	}
	
	
	public function index() {

		$where = array('product_id' => 1);

		$products = $this -> stocks_model -> read($where);

		if ($products) {

			$this -> fields['tb_data'] = $products;
		} else {
			$this -> fields['tb_data'] = FALSE;
		}
		$this -> fields['tb_name'] = 'products_tb_name';

		$header['stc_active'] = 'class="active"';
		$this -> fields['add_btn'] = 'add_new_stock';

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/products_stock', $this -> fields);
		$this -> load -> view('template/footer');

	}
	
}