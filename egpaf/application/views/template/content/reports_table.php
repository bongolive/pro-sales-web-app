<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
 
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('survey_reports'); ?> : <?php echo $pagetitle; ?></h3>
                                   <a href="<?php echo site_url('reports/printX'); ?>" style="margin:10px; float: right"  type="button" class="btn btn-large btn-info" ><i class="fa fa-printer"></i> <?php echo lang('print');?> </a>
                                    <a href="<?php echo site_url('reports'); ?>" style="margin:10px; float: right"  type="button" class="btn btn-large btn-primary" > <?php echo lang('back');?> </a>                                    
                                </div><!-- /.box-header --> 
                                
                                <div class="box-body table-responsive">
                                    <table id="type1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr><th>#</th>
                                            	  <?php foreach($tb_headers as $header){?>

										<th><?php echo lang($header); ?></th>

                                        <?php } ?>
                                           <th></th> 	 
                                            </tr>
                                        </thead>
                                        <tbody>
   <?php if ($tb_data){ 
									foreach ($tb_data as $data):   
                                            $id = $data['survey_id'];
                                    ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
											<td><input id="optionsCheckbox" type="checkbox" value="option1"></td>
                                           <?php foreach ($row_fields as $field): ?>
										  <td><?php
											if ($field == 'responce_status') {
												if ($data['last_question'] == $data['no_questions']) {
													echo '<span class="label label-success">Completed</span>';
												} else {
													$qn = $data['no_questions'] - $data['last_question'];
													echo '<span class="label label-warning">Incomplete (' . $qn . ')</span>';
													$suc = round(($data['last_question'] / $data['no_questions']) * 100);
													echo '<span class="label label-success">(' . $suc . '%)</span>';
												}
											} else {
												echo ucwords($data[$field]);
											}
										  	?></td>
                                        <?php endforeach; ?>
										
                                            <td>
                                               <div class="btn-group">
                                               	<?php
                                               	
                                               	 if($controller =='reports/view'){ ?>
                                               	 		<a href="<?php echo site_url('reports/view_responce') . '/' . $data['responder_id']; ?>"  class="btn btn-default" type="button"title="View" ><span class="awe-eye-open"/> View</a>
                                               	 <?php }else{ ?>
                                               	<a href="<?php echo site_url('reports/view') . '/' . $data['survey_id']; ?>"  class="btn btn-default" type="button" title="View" ><span class="awe-eye-open"/> View</a>
												<?php } ?>	<!--<a href="<?php echo site_url($view).'/'.$id; ?>" class="btn btn-small"> <span class="icon-eye-open"/>View </a> -->
												<!--	<a href="<?php echo site_url($edit).'/'.$id; ?>" class="btn btn-small"> <span class="icon-pencil"/> </a>
													<a href="<?php echo site_url($delete).'/'.$id; ?>" class="btn btn-small"> <span class="icon-trash"/> </a>-->
												 </div> 						 
											</td>
									
									</tr>
									<?php endforeach;
										}else { ?>
										<tr> <td  colspan="<?php echo count($tb_headers) ?>"> No data available <td> </tr> 
									<?php 	}  ?>
                                        </tbody>
                               
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div> 
                </section><!-- /.content --> 