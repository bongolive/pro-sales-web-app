<!DOCTYPE html>

<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->

<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->

<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>

		<meta charset="utf-8">

		<title>Login | ProMobile</title>

		<meta name="description" content="">

		<meta name="author" content="Bongolive (T) LTD | www.bongolive.co.tz">

		<meta name="robots" content="index, follow">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">



		<!-- Styles -->

        <?php get_css('sangoma-blue.css');?>

		

		<!-- Fav and touch icons -->

		

	<link rel="shortcut icon" href="favicon.ico">

		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/icons/apple-touch-icon-114-precomposed.png">

		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/icons/apple-touch-icon-72-precomposed.png">

		<link rel="apple-touch-icon-precomposed" href="img/icons/apple-touch-icon-57-precomposed.png">

		

		<!-- JS Libs -->

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>

		<script>window.jQuery || document.write('<script src="js/libs/jquery.js"><\/script>')</script>



		<?php get_js('libs/modernizr.js');?>



        <?php get_js('libs/selectivizr.js');?>

				

	</head>

<body class="login" style="background-color: #ecf0f1;">

    <section class="container" role="main">

                <div class="login-logo">

				<a href="#" class="brand" style="margin-left: -155px;width: 291px;">ProMobile</a>

				<h4 style="color: black;margin-left: -65px;"><?php _l('welcome_user'); ?></h4>

			     </div>

    			<form id="login_form" method="post" action="<?php site_url('login');?>" >
                    <?php if (($msg != '') && ($errorflag == 1)){ ?> 
                        <div class="alert alert-error fade in">
	                       <button class="close" data-dismiss="alert">&times;</button >
                           <?php _l($msg);?>
                        </div>
                     <?php }elseif(($msg != '') && ($errorflag == 0)){ ?>
                        <div class="alert alert-success fade in">
	                       <button class="close" data-dismiss="alert">&times;</button >
                           <?php _l($msg);?>
                        </div>
                     <?php } ?>
                    
    				<div class="control-group">

    					<div class="form-controls">

    						<div class="input-prepend">

    							<span class="add-on"><span class="icon-user"></span></span><input id="icon" type="text" placeholder="<?php _l('enter_your_name'); ?>" name="username" required>

    						</div>

    					</div>

    				</div>

    				<div class="control-group">

    					<div class="form-controls">

    						<div class="input-prepend">

    							<span class="add-on"><span class="icon-key"></span></span><input id="password" type="password" placeholder="<?php _l('password'); ?>" name="password" required>

    						</div>

    					</div>

    				</div>

    				<button class="btn btn-alt btn-primary btn-large btn-block" type="submit"><?php _l('login');?></button>

    				<a class="login-link" id="forget_password" href="#"><?php _l('forget_password'); ?>?</a>

                    

    			</form>

               <div id="showLostPass" style="display: none;">

               <form id="forgotpass_form" method="post" action="<?php echo site_url('login/sendforgotpassword');?>">

               <button id="close_forget_pass"type="button" class="close" style="position: absolute;top: 0%;left: 93%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>

                    <div class="control-group">

    					<div class="form-controls">

    						<div class="input-prepend">

    							<span class="add-on"><span class="icon-key"></span></span><input id="mail" type="email" placeholder="<?php _l('enter_email'); ?>" name="mail" required>

    						</div>

    					</div>

    				</div>

    				<button class="btn btn-alt btn-primary btn-large btn-block" type="submit"><?php _l('submit_email');?></button>

                </form>

                </div>

    			

    </section>

	<script>

        $(document).ready(function(){

            $("#forget_password").click(function(){

                    $('#showLostPass').slideDown();

                    $("#login_form").hide();

                });

            $("#close_forget_pass").click(function(){

                    $('#login_form').slideDown();

                    $("#showLostPass").hide();

            });

        });

    </script>

	</body>

</html>

