<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_device'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'add-device-form');
        echo form_open_multipart("auth/add_device", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            

            <div class="row">
                <div class="col-md-12">
                  
                <div class="form-group">
                    <?= lang("device_imei", "device_imei"); ?>
                    <input type="imei" name="imei" class="form-control" required="required" id="device_imei" />
                </div> 
                <div class="form-group">
                    <?= lang("device_phone", "device_phone"); ?>
                    <input type="tel" name="device_phone" class="form-control" required="required" id="device_phone" />
                </div>  
                <div class="form-group">
                    <?= lang("device")." Description"; ?>
                    <?php echo form_input('descr', '', 'class="form-control" id="city" required="required"'); ?>
                </div> 

            </div>


    </div>
    <div class="modal-footer">
        <?php echo form_submit('add_customer', lang('add_device'), 'class="btn btn-primary"'); ?>
    </div>
</div>
<?php echo form_close(); ?>  
</div>

<script type="text/javascript">
    $(document).ready(function (e) {
        $('#add-device-form').bootstrapValidator({feedbackIcons: {valid: 'fa fa-check', invalid: 'fa fa-times', validating: 'fa fa-refresh'}, excluded: [':disabled']});
        $('select.select').select2({minimumResultsForSearch: 6});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });

        
       $('#add-device-form').bootstrapValidator().on('submit', function (e) {
          if (e.isDefaultPrevented()) {
           // alert('invalid form');
            e.preventDefault();
            // handle the invalid form...
          } else {
            $.ajax({
                url: $('#add-device-form').attr('action'),
                type: 'POST',
                data: $('#add-device-form').serialize(),
                success: function(result) {
                    // ... Process the result ...
                    //console.log( result );
                    if(result.status){                        
                        bootbox.alert("Success.");
                        $("#device").append("<option value='"+result.insert_id+"'>"+result.device_imei+" ("+result.device_descr+")</option>");
                        //$("#device").val( result.insert_id ).trigger("chosen:updated");
                        $("#device option[value='"+result.insert_id+"']").prop('selected', true);
                       // $('#device').trigger("chosen:updated");
                        $("#device").select2().trigger('change');
                    }
                    else
                        bootbox.alert("Failed, " + result.message);

                }
            });
            $('.modal-dialog, #modal-loading, .modal-backdrop').hide();
            //bootbox.hideAll();
            //dialog.modal('hide');
            return false;
            e.preventDefault();
            // everything looks good!
          }
        });

    });
</script>
