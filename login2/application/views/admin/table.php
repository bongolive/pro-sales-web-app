<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealAddBoundary" class="modal fade hide" style="width: 800px;margin-left:-385px;">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
                <form id="form-delete-confirm" class="form-horizontal" action="<?php echo site_url("admin/deleteTask"); ?>" method="post">
                <fieldset>
                        <div class="modal-body">                                        
                            <p><?php _l('are_you_sure'); ?>?</p>
                        </div>
                        <div class="modal-footer" style="height: 15px;">
									<a href="#" class="btn btn-alt" data-dismiss="modal">No</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;padding: 6px 10px;margin-left: -46px;">Yes</button>
                                    </div>
								</div>
                </fieldset>
                </form>
            </div>
</div>
<section class="container" role="main">

    <div class="row">

        <article class="span12 data-block">

        

				<header>

				    <h2><span class="icon-folder-open" style="padding-right:10px;"></span><?php _l($tb_name); ?></h2>
                    <?php if(isset($download_btn)){?>
                    <a class="btn btn-alt btn-primary" data-toggle="modal" href="#downloadModal" style="float: right; padding:8px;"><?php _l($download_btn); ?></a>
                    <?php }if(isset($add_btn)){ ?>
                    <a class="btn btn-alt btn-primary" data-toggle="modal" href="#demoModal" style="padding:8px;float: right;"><?php _l($add_btn); ?></a>
                    <?php }?>
                    
                    <?php if ($tb_name == 'device_tb_name'){?>
                    <div class="modal fade hide" id="demoModal">
								<div class="modal-header">
									<h4><?php _l('add_new_device'); ?></h4>
								</div>
								<div class="modal-body">
                                <form class="form-horizontal" method="post" action="<?php echo site_url('admin/addDevice');?>">
                                    <fieldset>
                                        <div class="control-group">
                                        	<label class="control-label" for="user">Users</label>
                                        	<div class="controls">
                                        	<select id="user" name="user" class="input-xlarge">
                        					<option value="">Please select User</option>
                        					<?php foreach ($users as $user): ?>
                          					 <option value="<?php echo $user['userid'];?>"><?php echo $user['name'];?></option>
											<?php endforeach; ?>
                    						</select>
                                        	</div>
                                    	</div>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="device_name"><?php _l('device_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="device_name" id="device_name" class="input-xlarge" type="text" placeholder="" required>
                                        	</div>
                                    	</div>
                                    	<div class="control-group">
                                        	<label class="control-label" for="device_imei"><?php _l('device_imei'); ?></label>
                                        	<div class="controls">
                                        	<input name="device_imei" id="device_imei" class="input-xlarge" type="text" required >
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="track_intv" ><?php _l('track_intv'); ?></label>
                                        	<div class="controls">
                                        	<select name="track_intv" id="track_intv" required >
												<option></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
												<option value="5">5</option>
												<option value="10">10</option>
												<option value="15">15</option>
												<option value="20">20</option>
                                                <option value="30">30</option>
												<option value="60">60</option>
												<option value="stop">STOP</option>
											</select>
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="mobile"><?php _l('mobile_nmber'); ?></label>
                                        	<div class="controls">
                                        	<input name="mobile" id="mobile" class="input-xlarge" type="number" required />
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="holder_name"><?php _l('holder_name'); ?></label>
                                        	<div class="controls">
                                    	       <input name="holder_name" id="holder_name" class="input-xlarge" type="text" required />
												
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="email_reports"><?php _l('enable_email_reports'); ?></label>
                                        	<div class="controls">
                                            
                                        	   <input type="checkbox" name="email_reports" value="1" id="email_reports" name="email_reports">
                                        	   
                                           
                                            </div>
                                    	</div>
                                                    
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </div>
                        <?php } elseif($tb_name == 'user_tb_name'){?>
                    <div class="modal fade hide" id="demoModal">
								<div class="modal-header">
									<h4><?php _l('add_new_user'); ?></h4>
								</div>
								<div class="modal-body">
                                <form class="form-horizontal" method="post" action="<?php echo site_url('admin/addUser');?>">
                                    <fieldset>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="company_name"><?php _l('company_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="company_name" id="company_name" class="input-xlarge" type="text" value="" placeholder="<?php _l('company_name'); ?>">
                                        	</div>
                                    	</div>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="name"><?php _l('name'); ?></label>
                                        	<div class="controls">
                                        	<input name="name" id="name" class="input-xlarge" type="text" value="" placeholder="<?php _l('name'); ?>" required>
                                        	</div>
                                    	</div>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="mobile"><?php _l('mobile'); ?></label>
                                        	<div class="controls">
                                        	<input name="mobile" id="mobile" class="input-xlarge" value="" maxlength="13" minlength="9" type="number" placeholder="<?php _l('mobile'); ?>" required />
                                        	</div>
                                    	</div>
                                    	<div class="control-group">
                                        	<label class="control-label" for="email"><?php _l('email'); ?></label>
                                        	<div class="controls">
                                        	<input type="email" name="email" id="email" value="" class="input-xlarge" placeholder="<?php _l('email'); ?>" required >
                                        	</div>
                                    	</div>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="valid_till"><?php _l('valid_till'); ?></label>
                                        	<div class="controls">
                                        	<input name="valid_till" id="startdate" value="" class="show_date_piker input-xlarge" placeholder="<?php _l('valid_till'); ?>" required >
                                        	</div>
                                    	</div>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="user_type" ><?php _l('user_type'); ?></label>
                                        	<div class="controls">
                                        	<select name="user_type" id="user_type" required >
												<option></option>
                                                <option value="Client">Client</option>
                                                <option value="Administrator">Administrator</option>
											</select>
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="username"><?php _l('username'); ?></label>
                                        	<div class="controls">
                                        	<input name="username" id="username" class="input-xlarge" value="" placeholder="<?php _l('username'); ?>" type="text" required >
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="password"><?php _l('new_password'); ?></label>
                                        	<div class="controls">
                                    	      <input name="password" id="password" value="" class="input-xlarge" type="password" required/>
                                        	</div>
                                    	</div>
                                           
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </div>
                        <?php }else if($tb_name == 'tasks_tb_name'){?>
                        <div class="modal fade hide" id="demoModal">
                        <div class="modal-header">
									<h4><?php _l('add_task'); ?></h4>
                                    
								</div>
								<div class="modal-body">
                                <form class="form-horizontal" action="<?php echo site_url("admin/addTask"); ?>" method="post">
                                    <fieldset>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="user">Users</label>
                                        	<div class="controls">
                                        	<select id="user" name="user" class="input-xlarge">
                        					<option value="">Please select User</option>
                        					<?php foreach ($users as $user): ?>
                          					 <option value="<?php echo $user['userid'];?>"><?php echo $user['name'];?></option>
											<?php endforeach; ?>
                    						</select>
                                        	</div>
                                    	</div>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="device_name" ><?php _l('device_name'); ?></label>
                                        	<div class="controls">
                                        	<select name="device_name" id="device_name" required >
												<option></option>
                                                <?php foreach ($devices as $device): ?>
                                                <option value="<?php echo $device['phoneid'];?>"><?php echo $device['device_name'];?></option>
												<?php endforeach; ?>
											</select>
                                        	</div>
                                    	</div>
                                    	<div class="control-group">
                                        	<label class="control-label" for="subject"><?php _l('subject'); ?></label>
                                        	<div class="controls">
                                        	<input name="subject" id="subject" class="input-xlarge" type="text" required >
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="task" ><?php _l('task'); ?></label>
                                        	<div class="controls">
                                        	   <textarea id="task" name="task" class="input-xlarge" rows="5"></textarea>
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="status"><?php _l('status'); ?></label>
                                        	<div class="controls">
                                        	<select name="status" id="status" required >
												<option></option>
                                                <option value="New"><?php _l('new'); ?></option>
												<option value="In Progress"><?php _l('in_progress'); ?></option>
												<option value="Done"><?php _l('done'); ?></option>
											</select>
                                        	</div>
                                    	</div>
                              
                                        <div class="control-group">
                                        	<label class="control-label" for="comments"><?php _l('comments'); ?></label>
                                        	<div class="controls">
                                            <label class="checkbox custom-checkbox">
                                        	   <input type="text" name="comments" id="comments" >
                                            </label>
                                            </div>
                                    	</div>
                                                    
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </div>
                        <!-- for downloading report -->
                        <div class="modal fade hide" id="downloadModal">
                            <div class="modal-header">
                                <h4><?php _l('download_task'); ?></h4>
                            </div>
                            
                            <?php
                                //$devices = $this->devices_model->get_devicelistbyuserid($this->session->userdata('userid'));
                                if ($devices){
                            ?>
                                <form class="form-horizontal" action="<?php echo site_url("admin/exportTask"); ?>" method="post" >
                                    <div class="modal-body">
                                    
                                    <div class="control-group">
                                        	<label class="control-label" for="user">Users</label>
                                        	<div class="controls">
                                        	<select id="users" name="users" class="input-xlarge">
                        					<option value="">Please select User</option>
                        					<?php foreach ($users as $user): ?>
                          					 <option value="<?php echo $user['userid'];?>"><?php echo $user['name'];?></option>
											<?php endforeach; ?>
                    						</select>
                                        	</div>
                                    	</div>
                                    
                                    
                                    <div class="control-group">
                                            	<label class="control-label"><?php _l('device_name'); ?></label>
                                            	<div class="controls" id="divice_checkboxes" style="padding-left: 26px;height: 100px;overflow: auto;width: 235px;">
                                                    <?php 
                                                    foreach($devices as $device){ ?>
                                                        <label class="checkbox custom-checkbox"> 
											                 <input id="<?php echo 'cb'.$device['phoneid']; ?>" type="checkbox" name="cb_devices[]" value="<?php echo $device['phoneid']; ?>" checked="checked" />
											                 <?php echo $device['device_name']; ?>
                                                        </label>
                                                   <?php } ?> 
                                            	</div>
                           	        </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="startdate"><?php _l('start_date'); ?></label>
                                            	<div class="controls">
                                            	   <input class="show_date_piker" type="text" name="startdate" id="startdate" placeholder="<?php _l('all_dates'); ?>" />
                                            	</div>
                                    </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="enddate"><?php _l('end_date'); ?></label>
                                            	<div class="controls">
                                            	   <input class="show_date_piker" type="text" name="enddate" id="enddate" placeholder="<?php _l('all_dates'); ?>" />
                                            	</div>
                                    </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="txttask"><?php _l('txt_task'); ?></label>
                                            	<div class="controls">
                                            	   <input name="txttask" id="txttask" class="input-xlarge" type="text"  value="" placeholder="<?php _l('all_task'); ?>" >
                                            	</div>
                                    </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="subject"><?php _l('subject'); ?></label>
                                            	<div class="controls">
                                            	   <input name="subject" id="subject" class="input-xlarge" type="text"  value="" placeholder="<?php _l('all_subject'); ?>" >
                                            	</div>
                                    </div>
                                
                            </div>
                            <?php }else{ ?>
                            <p><?php _l('no_reports_to_download');?></p>
                            <?php }?>
                            <div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
                                    <?php if($devices){?>
									<div class="form-actions">
            				                <button name="btn_export" value="1" class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;"  >Submit</button>
                                    </div>
                                    <?php } ?>
                            </div>
                            </form>
                        </div>
                        
						<?php }elseif($tb_name == 'permission_tb_name'){ ?>
						<div class="modal fade hide" id="demoModal">
								<div class="modal-header">
									<h4><?php _l('add_new_permission'); ?></h4>
								</div>
								<div class="modal-body">
                                	<form class="form-horizontal" method="post" action="<?php echo site_url('admin/addPermission');?>">
                                    <fieldset>
                                        <div class="control-group">
                                        	<label class="control-label" for="permission_name"><?php _l('permission_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="permission_name" id="permission_name" class="input-xlarge" type="text" placeholder="" required>
                                        	</div>
                                    	</div>
                                    	       
								
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </div>
						<?php }elseif($tb_name == 'boundaries_tb_name'){ ?>
                        <div class="modal fade hide" id="demoModal" style="width: 800px;margin-left:-385px;">
                                <div class="modal-header">
									<h4><?php _l('add_boundary'); ?></h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 97%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
								</div>

                            <div class="modal-body">
                            <iframe src="<?php echo site_url('admin/editBoundary/add'); ?>" width="100%" height="350px" scrolling="no" frameborder="0"></iframe>
                            <input id="show_valid_to" type="text" class="input-medium" style="position: absolute;top: 270px;left: 135px;"/>
                            <input id="show_valid_from" type="text" class="input-medium" style="position: absolute;top: 223px;left: 133px;"/>
                            </div>
                        </div>
                        <div class="modal fade hide" id="revealEditBoundary" style="width: 800px;margin-left:-385px;">
                                <div class="modal-header">
									<h4><?php _l('edit_boundary'); ?></h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 97%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
								</div>

                            <div class="modal-body">
                            <iframe id="editBoundary" src="" width="100%" height="350px" scrolling="no" frameborder="0"></iframe>
                            <input id="show_edit_valid_to"  type="text" class="input-medium" style="position: absolute;top: 220px;left: 135px;"/>
                            <input id="show_edit_valid_from" type="text" class="input-medium" style="position: absolute;top: 173px;left: 133px;"/>
                            </div>
                        </div>
                        <script>

		                  	$(document).ready(function(){
		                  	       $('#show_valid_to').datetimepicker();
                                   $('#show_valid_from').datetimepicker();
                                   $('#show_edit_valid_to').datetimepicker();
                                   $('#show_edit_valid_from').datetimepicker();
                                 });
                 	   </script>
                        <?php }elseif($tb_name == 'reports_tb_name'){?>
                            <!-- for downloading report -->
                        <div class="modal fade hide" id="downloadModal">
                            <div class="modal-header">
                                <h4><?php _l('download_report'); ?></h4>
                            </div>
                            
                            <?php
                                //$devices = $this->devices_model->get_devicelistbyuserid($this->session->userdata('userid'));
                                if ($devices){
                            ?>
                                <form class="form-horizontal" action="<?php echo site_url("admin/exportReport"); ?>" method="post" >
                                    <div class="modal-body">
                                    
                                    <div class="control-group">
                                       <label class="control-label" for="user">Users</label>
                                       <div class="controls">
                                        	<select id="users" name="users" class="input-xlarge">
                        					<option value="">Please select User</option>
                        					<?php foreach ($users as $user): ?>
                          					 <option value="<?php echo $user['userid'];?>"><?php echo $user['name'];?></option>
											<?php endforeach; ?>
                    						</select>
                                       </div>
                                    </div>
                                    
                                    <div class="control-group">
                                            	<label class="control-label"><?php _l('device_name'); ?></label>
                                            	<div class="controls" id="divice_checkboxes" style="padding-left: 26px;height: 100px;overflow: auto;width: 235px;">
                                                    <?php 
                                                    foreach($devices as $device){ ?>
                                                        <label class="checkbox custom-checkbox"> 
											                 <input id="<?php echo 'cb'.$device['phoneid']; ?>" type="checkbox" name="cb_devices[]" value="<?php echo $device['phoneid']; ?>" checked="checked" />
											                 <?php echo $device['device_name']; ?>
                                                        </label>
                                                   <?php } ?> 
                                            	</div>
                           	        </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="startdate"><?php _l('start_date'); ?></label>
                                            	<div class="controls">
                                            	   <input class="show_date_piker" type="text" name="startdate" id="startdate" placeholder="<?php _l('all_dates'); ?>" />
                                            	</div>
                                    </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="enddate"><?php _l('end_date'); ?></label>
                                            	<div class="controls">
                                            	   <input class="show_date_piker" type="text" name="enddate" id="enddate" placeholder="<?php _l('all_dates'); ?>" />
                                            	</div>
                                    </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="txttask"><?php _l('report'); ?></label>
                                            	<div class="controls">
                                            	   <input name="txttask" id="txttask" class="input-xlarge" type="text"  value="" placeholder="<?php _l('all_report'); ?>" >
                                            	</div>
                                    </div>
                                    <div class="control-group">
                                            	<label class="control-label" for="subject"><?php _l('subject'); ?></label>
                                            	<div class="controls">
                                            	   <input name="subject" id="subject" class="input-xlarge" type="text"  value="" placeholder="<?php _l('all_subject'); ?>" >
                                            	</div>
                                    </div>
                                
                            </div>
                            <?php }else{ ?>
                            <p><?php _l('no_reports_to_download');?></p>
                            <?php }?>
                            <div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
                                    <?php if($devices){?>
									<div class="form-actions">
            				                <button name="btn_export" value="1" class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;"  >Submit</button>
                                    </div>
                                    <?php } ?>
                            </div>
                            </form>
                        </div>
                        
                        <?php }?>
				</header>

                

                <section>

                    <table class="datatable table table-striped table-bordered table-hover" id="example-2">

                    <thead>

									<tr>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php }?>
									</tr>
								</thead>

								<tbody>

									<?php foreach ($tb_data as $data): ?>
                                        
                                    <?php if (isset($data['id'])){
                                            $id = $data['id'];
                                    }elseif(isset($data['phoneid'])){
                                            $id = $data['phoneid'];
                                    }else{
                                        $id = '';
                                    }?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php echo $data[$field];?></td>
                                        <?php endforeach; ?>
										
                                            <td>
                                                <?php if ($tb_name == 'device_tb_name'){?>
                                                <a onclick="editContent('<?php echo site_url('admin/editDevice/'.$data['phoneid']);?>');" data-toggle="modal" href="#revealModal" style="text-decoration: none;padding: 10px;"><span class="icon-pencil" id="<?php echo site_url('admin/editDevice/'.$data['phoneid']);?>" ></span></a>
												<a data-toggle="modal" href="#revealDelete" style="text-decoration: none;"><span class="icon-trash" id="<?php echo site_url('admin/deleteDevice/'.$data['phoneid'].'/'.$data['userid']);?>" ></span></a>
                                                <?php }elseif($tb_name == 'tasks_tb_name'){?>
                                                <a onclick="editContent('<?php echo site_url('admin/editTask/'.$data['id']);?>');" data-toggle="modal" href="#revealModal" style="text-decoration: none;padding: 10px;" ><span class="icon-pencil" id="<?php echo site_url('admin/editTask/'.$data['id']);?>" ></span></a>
                                                
												<a data-toggle="modal" href="#revealDelete" style="text-decoration: none;"><span class="icon-trash" id="<?php echo site_url('admin/deleteTask/'.$data['id']);?>"  ></span></a>
                                                <?php }elseif($tb_name == 'boundaries_tb_name'){?>
                                                <a data-toggle="modal" href="#revealModal" style="text-decoration: none;padding-left: 40px;"><span class="icon-eye-open" id="<?php echo site_url('admin/showboundary/'.$data['centerlat'].'/'.$data['centerlng'].'/'.$data['radius']);?>" onclick="loadEditData('<?php echo site_url('admin/showboundary/'.$data['centerlat'].'/'.$data['centerlng'].'/'.$data['radius']);?>');" ></span></a>
                                                <a data-toggle="modal" href="#revealEditBoundary" style="text-decoration: none;padding: 10px;"><span class="icon-pencil-boundary" id="<?php echo site_url('admin/editBoundary/edit/'.$data['boundryid']);?>" onclick="loadEditData('<?php echo site_url('admin/editBoundary/edit/'.$data['boundryid']);?>');"></span></a>
												<a data-toggle="modal" href="#revealDelete" style="text-decoration: none;"><span class="icon-trash" id="<?php echo site_url('admin/deleteBoundary/'.$data['boundryid']);?>" ></span></a>
                                                <?php }elseif($tb_name == 'reports_tb_name'){?>
                                                <a data-toggle="modal" href="#revealModal" style="text-decoration: none;padding-left: 40px;"><span class="icon-eye-open" id="<?php echo site_url('admin/showmap/'.$data['device_name'].'/'.$data['latitude'].'/'.$data['longitude']);?>" ></span></a>
                                                
                                                <a onclick="editContent('<?php echo site_url('admin/editReport/'.$data['id']);?>');" data-toggle="modal" href="#revealModal" style="text-decoration: none;padding: 10px;" ><span class="icon-pencil" id="<?php echo site_url('admin/editReport/'.$data['id']);?>"></span></a>
												<a data-toggle="modal" href="#revealDelete" style="text-decoration: none;"><span class="icon-trash" id="<?php echo site_url('admin/deleteReport/'.$data['id']);?>" ></span></a>
                                                <?php }elseif($tb_name == 'user_tb_name'){?>
                                                <a data-toggle="modal" href="#revealModal" style="text-decoration: none;padding: 10px;" onclick="editContent('<?php echo site_url('admin/userPermissions/'.$data['userid']);?>');"><span class="icon-filter" id="<?php echo site_url('admin/userPermissions/'.$data['userid']);?>"></span></a>
                                                <a onclick="editContent('<?php echo site_url('admin/editUser/'.$data['userid']);?>');" data-toggle="modal" href="#revealModal" style="text-decoration: none;padding: 10px;" ><span class="icon-pencil" id="<?php echo site_url('admin/editUser/'.$data['userid']);?>" ></span></a>
												<a data-toggle="modal" href="#revealDelete" style="text-decoration: none;"><span class="icon-trash" id="<?php echo site_url('admin/deleteUser/'.$data['userid']);?>" ></span></a>
                                                <?php }elseif($tb_name == 'permission_tb_name'){?>
                                                <a onclick="editContent('<?php echo site_url('admin/editPermission/'.$data['id']);?>');" data-toggle="modal" href="#revealModal" style="text-decoration: none;padding: 10px;" ><span class="icon-pencil" id="<?php echo site_url('admin/editPermission/'.$data['id']);?>" ></span></a>
												<a data-toggle="modal" href="#revealDelete" style="text-decoration: none;"><span class="icon-trash" id="<?php echo site_url('admin/deletePermission/'.$data['id']);?>" ></span></a>
                                                <?php }?>
											</td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>

          </section>

        </article>

   

    </div>

</section>
