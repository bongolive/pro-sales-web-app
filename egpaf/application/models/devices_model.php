<?php
class Devices_model extends MY_Model {

	public $table = 'devices';
	public $table_id = 'device_id';

	function __construct() {
		parent::__construct();
	}

	public function form_validation() {

		if ($this -> input -> post('device_id')) {
			$this -> form_validation -> set_rules('device_imei', 'device imei', 'trim|required|callback_checkimei[' . $this -> input -> post('device_id') . ']');
		} else {
			$this -> form_validation -> set_rules('device_imei', 'device imei', 'trim|required|callback_checkimei[false]');
		}

	}

	public function read_devices($where) {
		$this -> db -> select("device_id,devices.device_imei,device_mobile,   last_sync ,  device_status , GROUP_CONCAT(firstname ,' ' ,lastname  SEPARATOR '|') as assigned_users", FALSE);
		//$this -> db -> select("  device_id,devices.device_imei,device_mobile,   last_sync ,  device_status , GROUP_CONCAT(firstname ,' ' ,lastname  SEPARATOR '|') as assigned_users", FALSE);
		
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$this -> db -> join('users', 'users.device_imei = devices.device_id', 'left');
		$this -> db -> join('employees', 'employees.employee_id = users.employee_id', 'left');
		$this -> db -> group_by('devices.device_imei');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function upd_dev_status($imei) {
		$data = array('settings' => 0, 'users' => 0, 'questions' => 0, 'responses' => 0, 'surveys' => 0,
            'settings_synctime' => '0000-00-00 00:00:00', 'users_synctime' => '0000-00-00 00:00:00',
            'questions_synctime' => '0000-00-00 00:00:00', 'responses_synctime' => '0000-00-00 00:00:00' ,
            'surveys_synctime' => '0000-00-00 00:00:00');
		$this -> db -> where('device_imei', $imei);
		if ($this -> db -> update('device_sync_status', $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}
/*
 * check if the device exist in the pro_devices table
 * */
    public function api_check_device_master($data)
    {
        $data = array('device_imei' => $data['imei']);
        $this->db->where($data);
        $query = $this->db->get('pro_devices');
        if($query->num_rows() == 1){
            return true;
        }
    }

/*
 * check if the device exists in the table pro _device_sync_status/_api
 * */
    public function api_check_device_sync($data)
    {
        $data = array('device_imei' => $data['imei']);
        $this->db->where($data);
        $query = $this->db->get('pro_device_sync_status');
        if($query->num_rows() == 1){
            return true;
        }
    }

/*
 * register the device in the sync table
 * */
    public function api_register_device_sync($data)
    {
        $data = array('device_imei' => $data['imei']);
        $this->db->insert('pro_device_sync_status', $data);
        if($this->db->affected_rows()){
            return true;
        }
    }


/*
 * get the last sync of the devices' entities
 * */
    public function api_get_device_entities_lastsync($data)
    {
        $data = array('device_imei' => $data['imei']);
        $this->db->where($data);
        $query = $this->db->get('pro_device_sync_status');
        if($query->num_rows() == 1){
            return $query->row_array();  // its usage should be $settings = $query['settings']
        }
    }

/*
 * get the account id
 * */
    public function api_get_account($data)
    {
        $data = array('device_imei' => $data['imei']);
        $this->db->where($data);
        $query = $this->db->get('pro_devices');
        if($query->num_rows() == 1){
             return $query->row_array();
        }
    }

/*
 * get the statuses of the devices' entities
 * */
    public function api_update_last_sync_statuses($where,$data)
    {
        $this->db->update('pro_device_sync_status',$data,$where);
        if($this->db->affected_rows()){
            return true;
        }
    }
/*
 * get the statuses of the devices' entities
 * */
    public function api_update_master_lastsync($where,$data)
    {
        $this->db->update('pro_devices',$data,$where);
        if($this->db->affected_rows()){
            return true;
        }
    }

}
