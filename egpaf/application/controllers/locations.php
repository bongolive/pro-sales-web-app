<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Locations extends My_Controller {
	public $user_id;
	public $account_id;

	public function __construct() {
		parent::__construct();

		if ($this -> session -> userdata('is_login') == FALSE) {
			redirect('login');
		}

 if(!in_array(6, $this->user_permissions)){
 	  
 	redirect('login/logout');
 } 

		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('location_code', 'location_name', 'location_type', 'location_city', );

		$this -> load -> model('locations_model');
		$this -> load -> model('surveys_model');
		$this -> data['table_id'] = $this -> locations_model -> table_id;

		$this -> locations_model -> form_validation();
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');

		//print_r($this->session->all_userdata());
		$this -> user_id = $this -> session -> userdata('user_id');
		$this -> account_id = $this -> session -> userdata('account_id');

		$this -> data['controller'] = 'locations';
		$this -> data['edit'] = 'locations/update';
		$this -> data['view'] = 'locations/view';
		$this -> data['delete'] = 'locations/delete';
		$this -> data['pagetitle'] = "Locations";
	}

	public function index() {

		$where = array('locations.account_no' => 1);
		//filtering data

		//surveys
		$surveys = $this -> surveys_model -> read(array('account_no' => $this -> account_id));
		if ($surveys) {
			$surv = array('' => 'Select Survey');
			foreach ($surveys as $surveys) {
				$surv[$surveys['survey_id']] = $surveys['survey_title'];
			}
		} else {
			$surv[''] = 'No Survyes Avalable';
		}

		$location = $this -> locations_model -> read(array('account_no' => $this -> account_id));
		if ($location) {
			$locations = array('' => 'Select Facility');
			foreach ($location as $location) {
				$locations[$location['location_id']] = $location['location_name'];
			}
		} else {
			$locations[''] = 'No Facilities Avalable';
		}

		$this -> data['surveys'] = $surv;
		$this -> data['locations'] = $locations;

		if ($this -> input -> post('filter')) {
			extract($_POST);

			if ($location_name) {
				$where['location_id'] = $location_name;
			}
			if ($location_code) {
				$where['location_code'] = $location_code;
			}
			if ($location_type) {
				$where['location_type'] = $location_type;
			}
			if ($location_name == FALSE && $location_code == FALSE && $location_type == FALSE) {
				$where = FALSE;
			}

		}
		$locations = $this -> locations_model -> read($where);

		if ($locations) {
			$this -> data['tb_data'] = $locations;
		} else {
			$this -> data['tb_data'] = FALSE;
		}

		$this -> data['tb_name'] = 'locations_tb_name';

		$this -> data['stc_active'] = 'class="active"';
		$this -> data['add_btn'] = 'add_new_stock';

		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/locations_table', $this -> data);
		$this -> load -> view('template/table_helper');
		$this -> load -> view('template/footer');

	}

	public function view() {

		$this -> data['headers'] = array('location_code', 'location_name', 'location_type', 'location_area', 'location_street', 'location_city', 'location_country', 'location_status', 'contact_person', 'location_mobile', 'location_email');

		$where = array('location_id' => $this -> uri -> segment(3));

		$locations = $this -> locations_model -> read($where);

		if ($locations) {

			$this -> data['info'] = $locations;
		} else {
			$this -> data['info'] = FALSE;
		}

		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/location_view', $this -> data);
		$this -> load -> view('template/footer');

	}

	public function create() {

		if ($this -> form_validation -> run() == FALSE) {
			$this -> data['controller'] = "locations";
			$this -> data['info'] = FALSE;

			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/locations_form', $this -> data);
			$this -> load -> view('template/footer');

		} else {
			extract($_POST);
			$location = array('location_code' => $location_code, 'location_name' => $location_name, 'location_type' => $location_type, 'location_area' => $location_area, 'location_street' => $location_street, 'location_city' => $location_city, 'contact_person' => $contact_person, 'location_mobile' => $location_mobile, 'location_email' => $location_email, 'account_no' => $this -> account_id);

			$this -> locations_model -> save($location);
			redirect('locations');
		}

	}

	public function update() {

		if ($this -> form_validation -> run() == FALSE) {

			$where = array('location_id' => $this -> uri -> segment(3));

			$locations = $this -> locations_model -> read($where);

			if ($locations) {
				foreach ($locations as $locations) {
				}
				$this -> data['info'] = $locations;
			} else {
				$this -> data['info'] = FALSE;
			}

			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/locations_form');
			$this -> load -> view('template/footer');

		} else {
			extract($_POST);

			$location = array('location_code' => $location_code, 'location_name' => $location_name, 'location_type' => $location_type, 'location_area' => $location_area, 'location_street' => $location_street, 'location_city' => $location_city, 'contact_person' => $contact_person, 'location_mobile' => $location_mobile, 'location_email' => $location_email, 'account_no' => $this -> account_id);
			print_r($location);
			$this -> locations_model -> update($location_id, $location);

			redirect('locations');
		}

	}

	//check location code
	public function checkcode($code, $id) {
		 echo $id;
		$locations = $this -> locations_model -> read(array('location_code' => $code));
		if ($locations) {
			if ($locations[0]['location_id'] == $id) {
				return TRUE;
			} else {
				$this -> form_validation -> set_message('checkcode', 'Sorry! the location code is already exist');
				return FALSE;
			}
		} else {
			return TRUE;
		}
	}

	public function delete() {
		$id = $this -> uri -> segment(3);
		//$status = array('status' => 0);
		if ($this -> locations_model -> delete($id)) {
			redirect('locations');
		}
	}

}
