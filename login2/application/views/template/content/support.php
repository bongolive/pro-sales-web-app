<section class="container" role="main">
<div class="row">
    <article class="span12 data-block">
            <header>
                <h2><?php _l('support'); ?></h2>
            </header>
            <section>
            <div class="row-fluid">
                <div class="span6">
                <h4><?php _l('support_details'); ?></h4>
                    <form class="form-horizontal" action="<?php echo site_url("profile/support"); ?>" method="post">
                        <fieldset>
                            <div class="control-group">
                            	<label class="control-label" for="input"><?php _l('name'); ?></label>
                            	<div class="controls">
								<input name="userid" type="hidden" value="<?php echo $users['userid'];?>" />
                            	<input name="name" id="name" class="input-xlarge" type="text" value="<?php echo $users['name'];?>" placeholder="<?php echo $users['name'];?>" required>
                          	</div>
                       	</div>
                            <div class="control-group">
                            	<label class="control-label" for="email" ><?php _l('email'); ?></label>
                            	<div class="controls">
                            	<input type="email" name="email" id="email" value="<?php echo $users['email'];?>" class="input-xlarge" placeholder="<?php echo $users['email']?>" required >
                            	</div>
                        	</div>
                            <div class="control-group">
                            	<label class="control-label" for="mobile"><?php _l('mobile'); ?></label>
                            	<div class="controls">
                            	<input name="mobile" id="mobile" class="input-xlarge" value="<?php echo $users['mobile_no'];?>" type="number" placeholder="<?php echo $users['mobile_no'];?>" required />
                            	</div>
                        	</div>
                            <div class="control-group">
                            	<label class="control-label" for="company_name"><?php _l('comment_question'); ?></label>
                            	<div class="controls">
                                    <textarea name="comment_question" class="input-xlarge" rows="5"></textarea>
                            	</div>
                        	</div>
                           <div class="form-actions">
				                <button class="btn btn-alt btn-large btn-primary" type="submit">Send</button>
				            </div>                  
                        </fieldset>

                    </form>
                </div>
            </div>
            </section>
    </article>
</div>

</section>