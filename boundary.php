<?php 
session_start();
include('common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />

<title>footprint</title>

<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>

<!--<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script src="http://code.jquery.com/jquery-migrate-1.1.1.min.js"></script>

<script src="http://code.jquery.com/jquery-1.8.0.min.js" type="text/javascript"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>-->
<link href="facebox.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="js/facebox.js" type="text/javascript"></script>




<script type="text/javascript">
  $(document).ready(function(){
  
  $('a[rel*=facebox]').facebox();
 
    /*$(".deleteitem").click(function(){
		var x=window.confirm("Are you sure want to delete this device?")
		if (x){
			var parent = $(this).closest('TR');
			var id = parent.attr('id');
			var uid = parent.attr('uid');
			$.ajax({
				type: 'POST',
				data: 'id=' +id+"&uid="+uid+"&type=device",
				url: 'deleteitem.php',
				success: function(msg){
					$('#'+id).remove();
				}
			});
		}
	});*/

	
  });
 
 
   function deleteclicked(ctrl){
	var x=window.confirm("Are you sure want to delete this Boundary?")
		if (x){			
			var parent = $(ctrl).closest('TR');
			var id = parent.attr('id');
			var uid = parent.attr('uid');
			$.ajax({
				type: 'POST',
				data: 'id=' +id+"&uid="+uid+"&type=boundary",
				url: 'deleteitem.php',
				success: function(msg){					
					$('#'+id).remove();
					
				}
				});
		}
  }
 
 
 
 
 
 
 
</script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>



</head>

<body >
<?php 
//session_start();
//include('common.php');
?>
	<div class="body_class">
    	<div class="center_header_page">
        	<div class="header-left">
            	<a href="index.php" ><img src="images/logo3.png" alt=""  /></a>
            </div><div class="header-right">
            	<span style="font-family:Arial; font-size:14px; color:#FFFFFF; margin-top:8px; float:left">Welcome <?php echo $_SESSION['username'];?> <br /><a href="logout.php" style="color:#FFFFFF">Logout</a></span>
            </div>
            
        </div>
	</div>
	<div class="center_header_pageinner">
    	<div class="menu_wrapperinner">
        	
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="device.php" >Device</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="track.php" >Track</a>
                </div>
            </div>
			  <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="task.php" >Tasks</a>
                </div>
            </div>
			  <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="report.php" >Reports</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="active menu_contentinner">
                		<a href="boundary.php" >Boundaries</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="profile.php" >Profile</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                		<a href="support.php" >Support</a>
                </div>
            </div>
          
            <div class="clear"></div>
			</div>
        </div>
    
    </div>
    <div class="line">
    </div>
    <div class="center_header_page">
      <div class="content" >
      		
            
            
           
            
        
            <div class="content_first content_sec_heading">
                
        <div class="content_thirdlog"><!--content_third-->
         	<div class="content_thirdlog_lefttable">
<table width="820px" align="right" cellpadding="0" cellspacing="0" border="1" bordercolor="#ffffff">
<tr style="font-family: Calibri; color:#000000; font-size:16px; text-align:center; background-image:url(images/bgupper.png); background-repeat:repeat ; height:70px;">
    <td><strong>Alert Name</strong></td>
    <td><strong>Device(s)</strong></td>
    <td><strong>Valid From</strong></td>
    <td><strong>Valid To</strong></td>
    <td><strong>Boundary</strong></td>
    <td><strong>Edit</strong></td>
    <td><strong>Delete</strong></td>
</tr>
<?php
include ("connection.php");

$sql="SELECT b.*, (SELECT COUNT(ba.alocationid) FROM  boundry_alocation ba WHERE ba.boundryid =  b.boundryid) AS devices FROM boundries b WHERE b.userid = ".$_SESSION['userid']." ORDER BY b.alertname ASC";

$result = mysql_query($sql);
while($row = mysql_fetch_array($result))
{ 
echo "<tr id='{$row['boundryid']}' uid='{$_SESSION['userid']}' style='font-family: Calibri; color:#000000; font-size:16px; text-align:center; background-image:url(images/bgupper.png); background-repeat:repeat ; height:70px;'>";
echo "<td>{$row['alertname']}</td>";
echo "<td>{$row['devices']}</td>";
echo "<td>{$row['valid_from']}</td>";
echo "<td>{$row['valid_to']}</td>";
echo "<td align='center' width='90'><a rel='facebox' href='showboundry.php?lat=".$row["centerlat"]."&lng=".$row["centerlng"]."&radius=".$row["radius"]."'><img src='images/boundry.png' /></a></td>";
echo "<td align='center' width='90'><a rel='facebox' href='boundrypopmaker.php?do=edit&editid=".$row["boundryid"]."'><img src='images/EDIT.png' /></a></td>";echo "<td><a href='javascript:void(0);' class='deleteitem1' id='deletetask' onclick='deleteclicked(this);'><img src='images/deletebutton.png' /></a></td>";
echo "</tr>";
}
?>

</table>

          </div >
          		<div style="width:126px; height:40px; margin:20px 0 0 20px; padding:10px 0 0 0; clear:both;">
          		<!--<a rel="facebox" href="boundrypop.php"><img src="images/btn-add_boundry.png" width="126" height="40" /></a>-->
          		<a rel="facebox" href="boundrypopmaker.php?do=add"><img src="images/btn-add_boundry.png" width="126" height="40" /></a>
				</div>
         	</div>
			 <div id="dialog"></div>
            <div class="clear"></div>
			
         </div><!--content_third-->
            </div>
      </div>
    
</div>
    <div class="footer-wrapper">
    	<div class="center_footer_page">
        	<div class="footer_center">
            	<div class="footer_navi" style="display:none;">
            	<a href="" >Home</a>
                <a href="" >Company</a>
                <a href="" >Clients</a>
                <a href="" >Resources</a>
                <a href="" >Support</a>
                <a href="" >Blog</a>
                <a href="" >Contact</a>
                </div>
            </div>
         <div class="clear"></div>
        </div>
    </div>
    <div class="center_header_page">
    	<div class="footer_buttom">
    	<h3>FOOT PRINT</h3><p>Copyright &copy; 2012. All Rights Reserved.</p>
        </div>
         <div class="clear"></div>
    </div>

<!--<script src="js/analytics.js" type="text/javascript"></script>-->
</body>
</html>
