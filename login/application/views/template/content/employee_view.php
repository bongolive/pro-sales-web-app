				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							
							<h2><?php

							echo _l('employee_details') . ' : ' . strtoupper($tb_data['firstname'] . ' ' . $tb_data['lastname']);
							 ?></h2>
							 <ul class="data-header-actions">
<li>
<a href="<?php echo site_url('employees') ?>"><?php _l('back') ?></a>
</li>
</ul>
					   
            </header> 

        </article>
 
      <article class="span12 data-block">
		  
   <table class="table table-striped table-bordered table-hover table-media">
			  <thead>

								<tr>
									

                                        <?php foreach($tb_headers as $head){?>

										<th><?php _l($head); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>
			   
					<tr>
						<?php foreach($tb_headers as $header){?>
							 <td> <?php echo $tb_data[$header]; ?></td>  
 			 			<?php } ?> 
 		 			</tr>
 			 </table>
 			 </section>
        </article>
        </div>
 
    </div>

</section>
