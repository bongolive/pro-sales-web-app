                    <div class="modal-header">

									<h4><?php _l('Update_Report'); ?></h4>

								</div>

                                <form class="form-horizontal" action="<?php echo site_url("admin/updateReport"); ?>" method="post">

                                    <fieldset>
                                        <div class="modal-body">
                                        <div class="control-group">

                                        	<label class="control-label" for="device_name" ><?php _l('device_name'); ?></label>

                                        	<div class="controls">

                                        	<input name="device_name" id="device_name" class="disabled input-xlarge" type="text" value="<?php echo $reports['device_name'];?>" disabled>
                                        	<input name="reportid" type="hidden" id="reportid" value="<?php echo $reports['id'];?>" />
                                        	</div>

                                    	</div>

                                    	<div class="control-group">

                                        	<label class="control-label" for="subject"><?php _l('subject'); ?></label>

                                        	<div class="controls">

                                        	<input name="subject" id="subject" value="<?php echo $reports['subject'];?>" class="disabled input-xlarge" type="text" disabled >

                                        	</div>

                                    	</div>

                                        <div class="control-group">

                                        	<label class="control-label" for="task" ><?php _l('report'); ?></label>

                                        	<div class="controls">

                                        	   <input name="report" id="report" value="<?php echo $reports['report'];?>" class="disabled input-xlarge" type="text" disabled>

                                        	</div>

                                    	</div>

                                        <div class="control-group">

                                        	<label class="control-label" for="status"><?php _l('status'); ?></label>

                                        	<div class="controls">
												<input name="datetime" id="datetime" value="<?php echo $reports['datetime'];?>" class="disabled input-xlarge" type="text" disabled>

                                        	</div>

                                    	</div>

                                        <div class="control-group">

                                        	<label class="control-label" for="comments"><?php _l('comments'); ?></label>

                                        	<div class="controls">

                                            <label class="checkbox custom-checkbox">

                                        	   <textarea name="comments" id="comments" class="input-xlarge" rows="5"><?php echo $reports['comments'];?></textarea>

                                            </label>

                                            </div>

                                    	</div>

                                                    

								</div>

								<div class="modal-footer">

									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>

									<div class="form-actions">

            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;padding: 6px 10px;">Submit</button>

                                    </div>

								</div>

                            </fieldset>

                        </form>