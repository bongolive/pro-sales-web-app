<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct() {
		parent::__construct();

		$this -> load -> model('permissions_model');
		$this -> load -> model('login_model');
		$this -> login_model -> form_validation();

		//unseting the login session/
		$auth = array('login_as' => '', 'logid' => '');
		$this -> session -> unset_userdata($auth);
		;
	}

	public function index() {
		if ($this -> form_validation -> run() == FALSE) {

			$this -> load -> view('template/login', $this -> data);

		} else {
			$data = extract($_POST);

			if ($login = $this -> login_model -> login($username, $password)) {
				foreach ($login as $login) {
					//remembering the user role
print_r($login);
					$user = array('role' => $login['role'], 'role_title' => $login['role_title'], 'user_id' => $login['user_id'], 'account_id' => $login['account_no'], 'username' => $login['username'],'permissions'=>$login['permissions'], 'is_login' => TRUE);

					$this -> session -> set_userdata($user);
				}
if($login['allow_web_access'] == 1){
				if ($login['role_title'] == 'super') {
					redirect('super_dashboard');
				} elseif ($login['role_title'] == 'admin' || $login['role_title'] == 'user' ) {
					redirect('dashboard');
				 
				} else {  redirect('dashboard'); // redirect('login', 'refresh');
				}
}else{
	$this -> session -> set_userdata('login_error', 'Sorry! No Acces To Web Appliacion');
	redirect('login');
}
			} else {
				$this -> session -> set_userdata('login_error', 'Sorry! Wrong username or password');
				redirect('login');
			}

		}
	}

	public function logout() {
		$this -> session -> sess_destroy();

		redirect('login');
		refresh();

	}

}
