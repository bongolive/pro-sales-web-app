<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Dashboard extends User_Controller {

	public $data = array();

	public function __construct() {

		parent::__construct();
 
		$this -> fields['css'] = array();
		$this -> fields['js'] = array('plugins/visualize/jquery.visualize.min.js' );
	
		$this -> load -> model('dashboard_model');
		$this -> load -> model('orders_model');
		$this -> load -> model('payments_model');
		$this -> load -> model('products_model');

	}

	public function index() {
	  
		$where = array('sales_orders.account_id' => $this -> account_id);
		$tb_data = $this -> payments_model -> read_orders($where);
		
		$this -> fields['sales'] = $this -> orders_model -> read(array('account_id' => $this -> account_id));
		
		$stats = array('sales_order_details.account_id' => $this -> account_id);

		$this -> fields['highsale'] = $this -> orders_model -> highsale($stats);
		$this -> fields['lowsale'] = $this -> orders_model -> lowsale($stats);

		$prod = array('account_id' => $this -> account_id);
		$this -> fields['products'] = $this -> products_model -> read($prod);

		if ($tb_data) {

			$this -> fields['tb_data'] = $tb_data;

		} else {
			
			$this -> fields['tb_data'] = FALSE;

		}
		//var_dump($this->fields);exit;
		//echo $this->db->last_query();exit;
		$this -> load -> view('template/header', $this -> fields);
		$this -> load -> view('template/content/dashboard');
		$this -> load -> view('template/footer');

	}

}
