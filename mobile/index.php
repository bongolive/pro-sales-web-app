<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
<link rel="stylesheet" href="bx_styles.css" type="text/css" />
<link rel="stylesheet" href="media-quries-resposive.css" type="text/css"  />
<title>footprint</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

<link href="facebox.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="js/facebox.js" type="text/javascript"></script> 
<script type="text/javascript">
  $(document).ready(function(){
     $('a[rel*=facebox]').facebox();
	 
	
  });
</script>

</head>

<body>
	<div class="wrapper-center">
    <div class="mobile_wrapper">

	<div class="body_class">
    	<div class="center_header_page">
        	<div class="header-left">
            	<a href="index.php" ><img src="images/logo3.png" alt=""  /></a>
            </div>
            
        </div>
	</div>
    <div class="line">
    </div>
    <div class="center_header_page">
      <div class="content" >
        
            <div class="content_first content_sec_heading">
        <div class="content_thirdlog"><!--content_third-->
         	<div class="content_thirdlog_left">
         	  
                <div class="content_third_left_second">
                	<h3>Customer Login</h3>
                    <div class="clear"></div>
					<form id="login_form" action="javascript:void(0);" method="post" >
                    <div class="content_third_left_contactlog">
                    	<div class="content_third_left_contactlog_input">
                    	<p>USERNAME:</p><input type="text" value="" name="username" id="username"/>
                        </div>
                        <div class="content_third_left_contactlog_input">
                        <p>PASSWORD:</p><input type="password" value="" name="txt_password" id="txt_password" />
                        </div>
                        <div class="content_third_left_contactlog_txtarea">
                        <span><a href="index.php#contact">NEW CUSTOMER?</a></span>
                        </div><br />
						<div class="content_third_left_contactlog_txtarea forget-title">
                        <span><a rel="facebox" href="forgotpassword.php?XDEBUG_SESSION_START=dk">FORGOT PASSWORD?</a></span>
                        </div>
                        <div class="content_third_left_buttonlog">
						
                        	<a href="javascript:void(0);" onclick="postForm();">LOGIN</a>
                        </div>
                    </div>
                    </form>
              </div>
         	</div>          
            
          <div class="content_third_right">
		               
		  </div>
            <div class="clear"></div>
			
         </div><!--content_third-->
            </div>
      </div>
    
</div>
    <div class="footer-wrapper">
    	<div class="center_footer_page">
        	<div class="footer_center">
            	<div class="footer_navi">
            	<a href="index.php" >Home</a>
                <a href="aboutprotracker.php" >About Pro Tracker</a>
                <a href="support.php" >Contact us</a>
                <a href="pro.co.tz">Full Site</a>
               
                </div>
            </div>
         <div class="clear"></div>
        </div>
    </div>
    <div class="center_header_page">
    	<div class="footer_buttom">
    	<h3>FOOT PRINT</h3><p>Copyrihgt &copy; 2012. All Rights Reserved.</p>
        </div>
         <div class="clear"></div>
    </div>
	</div>
   </div>
<script type="text/javascript">


function postForm(){
    
        var str = $("#login_form").serialize();
        $.ajax({
		type : 'POST',
		url : 'checklogin.php?XDEBUG_SESSION_START=dk',
		data : str,
		dataType: 'json',
		success : function(data){	

                if(data.status == "1") {
                    document.location = "device.php";
                } else {
					alert("Please enter correct username and password");
					$("#username").val("");
					$("#txt_password").val("");
                }

			}
  
		}); 
        return(false);
    
}
</script>
<script src="js/analytics.js" type="text/javascript"></script>
</body>
</html>
