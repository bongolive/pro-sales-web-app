<div id="page-title" class="clearfix">
            <h2 style="float: left;font-weight: normal;"><?php _l('account_form'); ?></h2>
            
</div> <!-- /.page-title -->
<div class="row">
<div class="span12"> 
 <div id="horizontal" class="widget widget-form">
            <div class="widget-header">	      				
	 <h3> 	 <i class="icon-pencil"></i>
<?php _l('account_form'); ?>
 </h3>	
 </div>
 <div class="widget-content">
<?php

	if ($info) { echo form_open('accounts/update', array('class' => 'form-horizontal'));
		echo '<input type="hidden" name="account_id" value="' . $info['account_id'] . '"  />';
		echo '<input type="hidden" name="user_id" value="' . $info['user_id'] . '"  />';

	} else {
		echo form_open('accounts/save', array('class' => 'form-horizontal'));
	}
  ?>
 <fieldset> 
 
 <div class="control-group">
	<span class="control-label"><?php _l('company_name'); ?></span> <?php echo form_error('company_name'); ?>
<div class="controls"><input type="text" name="company_name" value="<?php
		if ($info) { echo $info['company_name'];
		} else { echo set_value('company_name');
		}
 ?>" size="50" /></div>
</div>     
 <div class="control-group">
	<span class="control-label"><?php _l('firstname'); ?></span> <?php echo form_error('firstname'); ?>
<div class="controls"><input type="text" name="firstname" value="<?php
		if ($info) { echo $info['firstname'];
		} else { echo set_value('firstname');
		}
 ?>" size="50" /></div>
</div>
 <div class="control-group">
<span class="control-label"><?php _l('lastname')?></span><?php echo form_error('lastname'); ?>
<div class="controls"><input type="text" name="lastname" value="<?php
	if ($info) { echo $info['lastname'];
	} else { echo set_value('lastname');
	}
 ?>" size="50" /></div>
</div>

 <div class="control-group">
<span class="control-label"><?php _l('mobile'); ?></span><?php echo form_error('mobile'); ?>
<div class="controls"><input type="text" name="mobile" value="<?php
	if ($info) { echo $info['mobile'];
	} else { echo set_value('mobile');
	}
 ?>" size="50"  /></div>
</div>
 <div class="control-group">
<span class="control-label"><?php _l('email'); ?></span><?php echo form_error('email'); ?>
<div class="controls"><input type="text" name="email" value="<?php
	if ($info) { echo $info['email'];
	} else { echo set_value('email');
	}
 ?>" size="50"  /></div>
</div>

<div class="control-group">
<span class="control-label"><?php _l('units_limit'); ?></span><?php echo form_error('no_property'); ?>
<div class="controls"><input type="text" name="no_property" value="<?php
	if ($info) { echo $info['no_property'];
	} else { echo set_value('no_property');
	}
 ?>" size="50"  /></div>
</div>

<fieldset> <legend>Logging Details</legend>
<div class="control-group">
<span class="control-label"><?php _l('role'); ?></span><?php echo form_error('role'); ?>
<div class="controls">
<?php
	$role = array('admin' => 'Admin');
	if ($info) {$role1 = $info['role'];
	} else {$role1 = "";
	}
	echo form_dropdown('role', $role, $role1);
?>
 </div>
</div> 
 
 <div class="control-group">
<span class="control-label"><?php _l('username'); ?></span><?php echo form_error('username'); ?>
<div class="controls"><input type="text" name="username" value="<?php
	if ($info) { echo $info['username'];
	} else { echo set_value('username');
	}
 ?>" size="50"  /></div>
</div>
   
<div class="control-group">
<span class="control-label"><?php _l('password'); ?></span><?php echo form_error('password'); ?>
<div class="controls"><input type="password" name="password" value="" size="50"  /></div>
</div> 
 

 <div class="control-group"> 
<span class="control-label"> <?php _l('conf_pass'); ?></span> <?php echo form_error('conf_pass'); ?>
<div class="controls"><input type="password" name="conf_pass" value="" size="50" /> </div>
 </div>
 </fieldset>
<div class="form-actions"> 

<a href="<?php echo site_url('accounts'); ?>" class="btn btn-large">Cancel</a>
 <button type="submit" class="btn btn-info btn-large"><i class="icon-pencil"></i>Save</button></div>


</fieldset>
</form>  

</div>
</div>
