
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3><?php echo $totals['today'] ?> </h3>
						<p>
							<?php echo lang('count_today') ?>
						</p>
					</div>
					<div class="icon">
							<i class="ion ion-stats-bars"></i>
					</div>
					<a href="#" class="small-box-footer"> </a>
				</div>
			</div><!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3><?php echo $totals['week'] ?> </h3>
						<p>
							<?php echo lang('this_week_count') ?>
						</p>
					</div>
					<div class="icon">
						<i class="ion ion-stats-bars"></i>
					</div>
					<a href="#" class="small-box-footer"> </a>
				</div>
			</div><!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3><?php echo $totals['month'] ?> </h3>
						<p>
							<?php echo lang('this_month_count') ?>
						</p>
					</div>
					<div class="icon">
						<i class="ion ion-stats-bars"></i>
					</div>
					<a href="#" class="small-box-footer"> </a>
				</div>
			</div><!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-red">
					<div class="inner">
						<h3><?php echo $totals['total'] ?> </h3>
						<p>
							<?php echo lang('total_count') ?>
						</p>
					</div>
					<div class="icon">
							<i class="ion ion-stats-bars"></i>
					</div>
					<a href="#" class="small-box-footer"> </a>
				</div>
			</div><!-- ./col -->
		</div><!-- /.row -->
 	

		<!-- top row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">

			</div><!-- /.col -->
		</div>
		<!-- /.row -->

		 			</section><!-- /.Left col -->
 

 
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"> <?php echo lang('surveyor_name') ?> : <?php echo $surveyor['employee_name'] .'( '. $surveyor['device_imei'] . ' )'  ?> </h3>   
                                 <a class="btn btn-large btn-info" type="button" style="margin:10px; float: right" href="<?php echo site_url('reports/exporting');?>"><i class="fa fa-printer"></i><?php echo lang('print') ?>
</a>
<a class="btn btn-large btn-primary" type="button" style="margin:10px; float: right" href="<?php echo site_url('reports/surveyor_report');?>"> <?php echo lang('back') ?> </a>   
                                                                    
                                                                   
                                </div><!-- /.box-header -->
        <?php include_once('filtering.php'); ?> 
                               <div class="box-body table-responsive">
                                    <table id="type1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr> 
                                            	  <?php foreach($tb_headers as $header){?>

										<th><?php echo lang($header); ?></th>

                                        <?php } ?>
                                            	 <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
   <?php if ($tb_data){ 
									foreach ($tb_data as $data):   
                                            $id = $data['participant_id'];
                                    ?>
									<tr>
										<!--	<td><input id="optionsCheckbox" type="checkbox" value="option1"></td>-->
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php   
 								  
										  echo ucwords($data[$field]);
 	 
										  	?></td>
                                        <?php endforeach; ?>
										  <td> 
                                                	<a href="<?php echo site_url('reports/participant').'/'.$data['survey_no'].'/'.$data['participant_id']; ?>" type="button"   class="btn btn-small btn-success" title="View" ><i class="fa fa-eye"></i> <?php echo lang('view') ?></a>
                                                	 
											</td>
									
									</tr>
									<?php endforeach;
										} 
									?>
                                        </tbody>
                   
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
          