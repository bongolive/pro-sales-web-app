                    <div class="modal-header">

									<h4><?php _l('Update_User'); ?></h4>

								</div>

                                <form class="form-horizontal" method="post" action="<?php echo site_url('admin/updateUser');?>">
                                    <fieldset>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="company_name"><?php _l('company_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="company_name" id="company_name" class="input-xlarge" type="text" value="<?php echo $users['company_name'];?>" placeholder="<?php _l('company_name'); ?>">
                                            <input name="userid" type="hidden" value="<?php echo $users['userid'];?>" />
                                            <input name="old_password" type="hidden" value="<?php echo $users['password'];?>" />
                                        	</div>
                                    	</div>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="name"><?php _l('name'); ?></label>
                                        	<div class="controls">
                                        	<input name="name" id="name" class="input-xlarge" type="text" value="<?php echo $users['name'];?>" placeholder="<?php _l('name'); ?>" required>
                                        	</div>
                                    	</div>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="mobile"><?php _l('mobile'); ?></label>
                                        	<div class="controls">
                                        	<input name="mobile" id="mobile" class="input-xlarge" value="<?php echo $users['mobile_no'];?>" maxlength="13" minlength="9" type="number" placeholder="<?php echo $users['mobile_no'];?>" required />
                                        	</div>
                                    	</div>
                                    	<div class="control-group">
                                        	<label class="control-label" for="email"><?php _l('email'); ?></label>
                                        	<div class="controls">
                                        	<input type="email" name="email" id="email" value="<?php echo $users['email'];?>" class="input-xlarge" placeholder="<?php echo $users['email'];?>" required >
                                        	</div>
                                    	</div>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="valid_till"><?php _l('valid_till'); ?></label>
                                        	<div class="controls">
                                        	<input type="datetime" name="valid_till" id="valid_till" value="<?php echo $users['valid_till'];?>" class="datepicker input-xlarge" placeholder="<?php _l('valid_till'); ?>" required/>
                                        	</div>
                                    	</div>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="user_type" ><?php _l('user_type'); ?></label>
                                        	<div class="controls">
                                        	<select name="user_type" id="user_type" required >
												<option></option>
                                                <option value="Client" <?php if($users['user_type']=="Client"){ echo "selected"; } ?>>Client</option>
                                                <option value="Administrator" <?php if($users['user_type']=="Administrator"){ echo "selected"; } ?>>Administrator</option>
											</select>
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="username"><?php _l('username'); ?></label>
                                        	<div class="controls">
                                        	<input name="username" id="username" class="input-xlarge" value="<?php echo $users['username'];?>" placeholder="<?php echo $users['username'];?>" type="text" required >
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="password"><?php _l('new_password'); ?></label>
                                        	<div class="controls">
                                    	      <input name="password" id="password" value="" class="input-xlarge" type="password" />
                                        	</div>
                                    	</div>
                                           
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>