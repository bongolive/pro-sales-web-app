hreee
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_user')." (".$company->name.")"; ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo form_open("customers/add_customer_map/".$company->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
               
            <div class="row">
                
                <div class="col-md-12 col-sm-6">
                            <div class="form-group">
                                <?php echo lang('user', 'users'); ?> 
                                <div class="controls">
                                    <?php
                    $us[""] = $this->lang->line("select") . " " . $this->lang->line("user");
                   
                    foreach ($users as $user) {
                       // var_dump($user);
                        $us[$user->uid] = $user->first_name;
                    }
                    // var_dump($us);
                    echo form_dropdown('users', $us, (isset($_POST['users']) ? $_POST['users'] : ''), 'class="form-control select" id="users" required="required"');
                    ?> 
                                </div>
                            </div>

                           
                    </div>
                
                    
            </div>
        <div class="modal-footer">
    <?php echo form_submit('add_user', lang('map_customer'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
<?php echo form_close(); ?>  
</div>
<?=$modal_js?>

