				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">
					 
					<!-- Smart wizard -->
					<article class="span12  data-block"> 
						<header class="info">
							<h2><?php echo $info['survey_title']; ?> :<?php echo $info['responder_mobile']; ?> </h2>
						 	 
							<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('surveys/print_results/'.$info['survey_no']) ?>" ;"="" style="float: right; padding: 8px;"><img src="<?php echo base_url('template/img/icons/excel.png'); ?>" alt="export"/><?php _l('export'); ?></a>
							<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('reports/view/'.$info['survey_no']) ?>" ;"="" style="float: right; padding: 8px;"><?php _l('back'); ?></a>
		   			</header> 
         
              <!-- include the filtering paget-->
     
								<div class="modal-body"> 
								
								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>
 						<tr><th>QNo</th> 
										<th><?php _l('Question'); ?></th><th><?php _l('Responce'); ?></th>
 
									</tr>
								</thead>

								<tbody>
 				  <?php if ($questions){
 				 
 				 			$no= 1;
									foreach ($questions as $question):  
									   
                                     ?>
									<tr class="even gradeC" id="">
										<td><?php echo $no;
										$no++;
 ?></td>
										<td> <?php echo $question['question'] ?> </td>   
										      <td>
										     
										    	<?php 
										    	//$ans = explode(',' ,$question['answer_choices']);
												 if($replies){
												 	
										   			 foreach ($replies as $reply) {
										   				
														   if($reply['responce_qn'] == $question['rank']){
														   	$mychoice = explode(',',$question['answer_choices']);
														   	$myreply = explode(',', $reply['responce']);
															
															for($q=0;$q< count($myreply);$q++){
																if($myreply[$q] != 99){
																	echo ucwords($mychoice[$q] . " ( " . $myreply[$q]." )");
																}
															} 
													   }
													} 
												 }else{
												 	echo "99";
												 }				  
										  		//echo ucwords($data[$field]);
											 
										    ?>
										    </td>
                                          
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
								</tbody>
	</table> 
           
		</article>							
   
                        </div>                        	
                        </div>	
                        </section>
                        		
					 