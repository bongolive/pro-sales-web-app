<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l($tb_name); ?></h2>
							&nbsp; &nbsp;
							<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('orders'); ?>";" style="float: right; padding:8px;"><?php _l('back_to_orders'); ?></a>
							<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('stock/view_team/'.$order_info['employee_id']); ?>";" style="float: right; padding:8px;"><?php _l('team_orders'); ?></a>
								<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('orders/export'); ?>" style="float: right; padding:8px; margin-left: 20px;"> Export</a>
					  
						<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('orders/new_order'); ?>";" style="float: right; padding:8px;"><?php _l('new_order'); ?></a>
                            <a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('orders'); ?>" ;" style="float: right; padding: 8px;">Back</a>

                        </header>

        </article>

    </div>	
    
    
     <!-- searching
    <div class="row">
		        	<section>
		           		 <article class="span6 turquoise data-block">
								<header> <h2>Search form</h2></header>
									<section> 
												<form class="form-search no-margin">
													<div class="control-group">
														<div class="controls">
															<input class="search-query" type="text"/>
															<button class="btn btn-alt" type="submit">Search</button>
														</div>
													</div>
												</form> 
									 </section>
		   	 
   	</div>-->
   	
   	
   	
    <div class="row">
  
        <article class="span12 data-block"> 
 
         <div class="row-fluid">
								<div class="span6">
								 <section>
										<h4 class="no-margin"><?php _l('customer_details') ?></h4>
										<br />
										<table class="table table-striped table-hover">
												

                                        <?php foreach($order_hd as $header){
                                     
                                        	?>

										<tr><th><?php _l($header); ?></th><td><?php

										if ($header == 'order_status') {
											if ($order_info[$header] == 1) {
												echo '<span class="label label-inverse">'; _l('new_order'); echo '</span>';
											} elseif ($order_info[$header] == 2) {
												echo '<span class="label label-info">'; _l('closed'); echo '</span>';
											} elseif ($order_info[$header] == 3) {
												echo '<span class="label label-danger">'; _l('cancelled'); echo '</span>';
											} 
										} elseif ($header == 'payment_balance') {

										} elseif ($header == 'payment_status') {
											if ($order_info[$header] == 1) {
												echo _l('receipt_printed') ;//pid
											} else {
												echo _l('receipt_notprinted');//pending
											}
										} elseif($header == 'trans_uniq_id'){
                                            if(!is_null($transactionvalue)){
                                                echo $transactionvalue;
                                            } else {
                                                echo _l('trans_uniq_id_null');
                                            }
                                        } else {
											echo ucwords($order_info[$header]);
										}
									?> </td></tr>
                                            
											<?php } ?>
										 	</tbody>
										 </table>
								   </section>
								</div>
								
								<div class="span6" >
								 <section>	
								 	<h4 class="no-margin"><?php _l('payments_summary') ?></h4>
												<br />
											<table class="table table-striped table-hover">	 
											
											<?php
											$ordersales = 0;
											if($tb_data){
											foreach ($tb_data as $calc) :

												$sale = $calc['order_qty'] * $calc['sale_price'];
												$tsale = $sale + ($sale * ($calc['sale_tax'] / 100));
												$ordersales = $ordersales + $tsale;
											endforeach;
											}
											$currency = $this -> settings['default_currency'];
											//$settings['default_currency'];
											 ?>
											
  			<tr><td><?php _l('total_sales'); ?> </td><td><strong><?php echo $currency; ?> </strong> <strong><?php echo money_format('%i', $ordersales); ?></strong></td></tr>
			 <tr><td><?php _l('paid_amount'); ?> </td><td><strong><?php echo $currency; ?> </strong><strong><?php echo money_format('%i', $order_info['paid_amount']); ?></strong></td></tr>
			 <tr><td><?php _l('outstanding'); ?> </td><td><strong><?php echo $currency; ?> </strong> <strong><?php echo money_format('%i', $ordersales - $order_info['paid_amount']); ?></strong></td></tr>
 </table>
	 
								 	 
								 					 <a href="<?php echo site_url('orders/print_receipt') ?>"style="display: block; margin: 5px 30%; width: 25%;" class="btn btn-large btn-success">Issue Invoice</a>
											 
												 <a href="<?php echo site_url('payments/make_payments') . '/' . $order_info['order_id']; ?>" style="display: block;margin: 5px 30%;width: 25%;"  class="btn btn-large btn-primary">Make Payments</a>
												 
												 
									</section> 
								</div>
								 
							</div> 
            <section>
 <fieldset>
<legend><?php _l('order_details'); ?> 
		<?php 	if ($order_info['order_status'] == 1 ) { ?>
	<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('orders/update_order/' . $order_info['order_id']); ?>" style="float: right; padding:8px; margin-left: 20px;"><?php _l('edit_order') ?></a>
       		<?php  } ?></legend>  
   <table class="table table-striped table-bordered table-hover table-media">
								<thead>

								<tr>
										<th class="span1">No</th>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>

							 
									  <?php if ($tb_data){
									 $i = 1;$sales = $taxes = 0;
									foreach ($tb_data as $data):   
                                            $id = $data[$table_id];
                                    ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
											<td><?php echo $i;
										$i++;
 ?></td>
                                        <?php foreach ($row_fields as $field): ?>
										  <td style="text-align: center"><?php
										if ($field == 'total_sales') {
											$sale = money_format('%2n', $data['order_qty'] * $data['sale_price']);
											$sales = $sales + $sale;
											echo $sale;
										} elseif ($field == 'total_tax') {
											$tax = money_format('%2n', ($data['order_qty'] * $data['sale_price']) * ($data['sale_tax'] / 100));
											$taxes = $taxes + $tax;
											echo "$tax";
										} else {
											echo ucwords($data[$field]);
										}
											    ?></td> 
										  <?php endforeach; ?>
									</tr>
									<?php  endforeach;
										echo $pagenate;
									?>
									<tr>
										<td colspan="5"><?php _('sub_total') ?></td><td style="text-align: center"><?php echo money_format('%2n', $sales) ?></td><td style="text-align: center"><?php echo money_format('%2n', $taxes) ?></td>
									</tr>
									<tr>
										<td colspan="5"><?php _('total_sale') ?></td><td colspan="2" style="text-align: center"><b><?php echo $this->settings['default_currency'].' '; echo money_format('%2n', $sales + $taxes) ?></b></td>
										
									</tr>
									<?php }else {
										echo '<tr> <td colspan="'.count($tb_headers).'"> No data available </td> </tr>';
										}
 ?>
								</tbody>
	</table> 
          </section>

        </article>

    
    </div>

</section>
