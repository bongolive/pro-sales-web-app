<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Accounts extends User_Controller {

	public $accounts_dsc = array('company_name', 'contact_person', 'user_accounts', 'mobile', 'email', 'username');

	public function __construct() {

		parent::__construct();

		$this -> fields['css'] = array('bootstrap.css', 'bootstrap-responsive.css', 'bootstrap-overrides.css', 'ui-lightness/jquery-ui-1.8.21.custom.css', 'DT_bootstrap.css', 'responsive-tables.css', 'slate.css', 'slate-responsive.css', 'datepicker.css', );
		$this -> fields['js'] = array('jquery-1.7.2.min.js', 'jquery-ui-1.8.21.custom.min.js', 'jquery.ui.touch-punch.min.js', 'bootstrap.js', 'plugins/datatables/jquery.dataTables.js', 'plugins/datatables/DT_bootstrap.js', 'plugins/responsive-tables/responsive-tables.js', 'Slate.js', 'demos/demo.tables.js');

		if (!$this -> session -> userdata('logged_in') == TRUE) {
			$this -> session -> set_userdata('login_error', 'Please, Loggin first');
			redirect('login');
		}

		$this -> load -> model('accounts_model');
		$this -> accounts_model -> form_validation();

		$this -> load -> model('settings_model');
		$this -> load -> model('user_model');

		$this -> load -> model('permissions_model');
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');

		$this -> fields['row_fields'] = $this -> accounts_dsc;
		$this -> fields['tb_headers'] = $this -> accounts_dsc;

		$this -> fields['tb_name'] = 'all_accounts';
		$this -> fields['edit'] = 'accounts/update/';
		$this -> fields['delete'] = 'accounts/delete/';
		$this -> fields['links'] = "accounts/save";
		$this -> fields['view'] = "accounts/view/";
		$this -> fields['ctype'] = "accounts/changetype/";
		$this -> fields['controller'] = 'accounts';
		$this -> fields['excel'] = 'accounts';
		$this -> fields['table_id'] = $this -> accounts_model -> table_id;
		$this -> fields['acc'] = $this -> accounts_model -> read();

		/******************** check user permissions ***************************/

		$modules = explode(',', $this -> session -> userdata('permissions'));
		$this -> fields['is_allowed'] = $this -> permissions_model -> read_modules($modules);

		/******************** check user permissions ***************************/
	}

	public function index() {
		$this -> session -> unset_userdata('id');
		//the search from
		$this -> fields['controller'] = 'accounts';
		$where = array('accounts.created_by'=>0);
		if (isset($_POST['search'])) {
			extract($_POST);
			$where['accounts.account_id'] = $acc_id;
		} else {
			
		}
 
		$this -> fields['tb_data'] = $this -> accounts_model -> read($where);
		//echo $this->db->last_query();
		$this -> load -> view('template/header', $this -> fields);
		$this -> load -> view('template/content/accounts_table');
		$this -> load -> view('template/footer');

	}

	//viewing
	public function view() {
		$this -> fields['user_headers'] = array('account_id', 'role', 'username', 'created_on', 'status');
		$id = $this -> uri -> segment(4);

		$where = array('accounts.account_id' => $id);
		//geting the data
		$account = $this -> accounts_model -> read($where);
		//echo $this->db->last_query();exit;
		if ($account) {
			//read users accounts under the main account
			$users = $this -> user_model -> read(array('account_id' => $id));
			if ($users) {
				$this -> fields['users'] = $users;
			} else {
				$this -> fields['users'] = FALSE;
			}
			$this -> fields['tb_data'] = $account;
			$data['rudi'] = 'accounts';

			$this -> load -> view('template/header', $this -> fields);
			$this -> load -> view('template/content/accounts_view', $data);
			$this -> load -> view('template/footer');
		} else {
			redirect('accounts');
		}

	}

	public function save() {
		if ($this -> form_validation -> run() == FALSE) {
			$data['info'] = FALSE;
			$this -> load -> view('template/header', $this -> fields);
			$this -> load -> view('template/content/form_account', $data);
			$this -> load -> view('template/footer');
		} else {
			extract($_POST);


            $user = $this-> user_model -> check_user(array('username' => $username));
            if(!$user) {
                $this->db->trans_start();
                $data = array('company_name' => $company_name, 'mobile' => $mobile, 'email' => $email, 'user_accounts' => $user_accounts, 'contact_person' => $contact_person, 'created_by' => $this->account_id);

                //saving the account
                $this->accounts_model->save($data);
                //acc id
                $acc_id = $this->db->insert_id();

                /******** save user data  *******/
                foreach ($modules as $modules) {
                    $perms[] = $modules['module_id'];
                }

                $user_data = array('account_id' => $acc_id, 'permissions' => '1,2,3,4,5,6,7,8,9', 'role' => $role, 'username' => $username, 'password' => sha1($password), 'created_by' => $this->account_id, 'created_on' => date('Y-m-d h:i:s'));
                //print_r($user_data);
                $this->user_model->add_user($user_data);

                $get_user_id = $this->db->insert_id();
                //get the inserted user id
                $this->db->trans_complete();

                /******** end save user data  *******/

                /******** save account settings and update the account main user  ********/

                //update the account admin user
                $acc_user = array('user_id' => $get_user_id);
                $this->accounts_model->update($acc_id, $acc_user);

                //setup the account
                $user_settings = array('account_id' => $acc_id);
                $this->settings_model->save($user_settings);

                /******** end save account settings *******/

                /*********** give permissions to admin account *****************/

                //get models numbers
                //$modules = $this -> permissions_model -> read_modules();


                //$permits = implode(',', $perms);
                //	$data = array('user_id' => $get_user_id, 'permissions' => $permits, 'created_by' => $this->account_id);
                //$this -> permissions_model -> save($data);

                /*********** give permissions to admin account end *************/

                redirect('accounts');
            } else {
                redirect('accounts');
            }

		}
	}

	public function update() {
		if ($this -> form_validation -> run() == FALSE) {
			if ($this -> session -> userdata('id')) {
				$id = $this -> session -> userdata('id');
			} else {
				$id = $this -> uri -> segment(4);
				$this -> session -> set_userdata('id', $id);
			}
			$where = array('accounts.account_id' => $id);
			//geting the data

			$info = $this -> accounts_model -> read($where);

			//echo $this->db->last_query();exit;

			//var_dump($info);exit;

			if ($info) {
				foreach ($info as $info) {
					$data['info'] = $info;
				}

				$this -> load -> view('template/header', $this -> fields);
				$this -> load -> view('template/content/form_account', $data);
				$this -> load -> view('template/footer');
			} else {
				redirect('accounts');
			}
		} else {

			extract($_POST);
				$data = array('company_name' => $company_name, 'mobile' => $mobile, 'email' => $email, 'user_accounts' => $user_accounts, 'contact_person' => $contact_person, 'acc_type'=>$acc_type );
			
			$this -> accounts_model -> update($account_id, $data);

			if ($password) {
				$user_data = array('username' => $username, 'password' => sha1($password));
			} else {
				$user_data = array('username' => $username);
			}

			$this -> user_model -> update($user_id, $user_data);
			redirect('accounts');

		}
	}

	public function checkmail($email, $account_id) {
		if ($account_id) {
		} else {$account_id = 'no';
		}
		$where = array('accounts.account_id' => $account_id, 'email' => $email);

		$accounts = $this -> accounts_model -> read($where);

		if ($accounts) {
			return TRUE;
		} else {
			$where = array('accounts.email' => $email);
			$accounts = $this -> accounts_model -> read($where);
			if ($accounts) {
				$this -> form_validation -> set_message('checkmail', 'Sorry! the email is already used by someone else');
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}

	public function checkmobile($mobile, $account_id) {
		if ($account_id) {
		} else {$account_id = 'no';
		}
		$where = array('mobile' => $mobile, 'accounts.account_id' => $account_id);

		$account = $this -> accounts_model -> read($where);

		if ($account) {
			return TRUE;
		} else {

			$where = array('accounts.mobile' => $mobile);
			$account = $this -> accounts_model -> read($where);
			if ($account) {
				$this -> form_validation -> set_message('checkmobile', 'Sorry! the mobile number is already used by someone else');
				return FALSE;
			} else {
				return TRUE;
			}
		}

	}

	public function delete() {
		$id = $this -> uri -> segment(4);
		if ($this -> accounts_model -> delete($id)) {
			redirect('accounts');
		}
	}
	
}
