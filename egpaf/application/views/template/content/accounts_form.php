

<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                   <!-- <h3 class="box-title">
                                    	<?php
										if ($info) {
											echo lang('account_details') . ' : ' . strtoupper($info['organisation'] . ' ' . $info['lastname']);
										} else {
											echo lang('new_account');
										}
							 ?>
                                 </h3>-->
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                  	
    <div class="row">
    	 <div class="col-md-12">  
									<?php
									if ($info) {
										$action = 'accounts/update';

									} else {
										$action = 'accounts/create';
									}
		?>
								
                                	  <form role="form" method="post" action="<?php echo site_url($action); ?>">
                                	  	<div class="box-body">
							 <div class="row">
    	 <div class="col-md-6">
    	 
    	 	<div class="box-header">
    	 	  
<h3 class="box-title"><?php echo lang('account_details') ?></h3>
</div> 
                                	<?php if($info){ ?>
                                		<input type="hidden" name="account_id" value="<?php echo $info['account_id']; ?>"/>
                                		<?php } ?> 
<fieldset  class="span12 data-block row-fluid">
  
 <div class="form-group">
	<span class="control-label"><?php echo lang('organisation'); ?></span> <?php echo form_error('organisation'); ?>
<input type="text" name="organisation" value="<?php
if ($info) { echo $info['organisation'];
} else {echo set_value('organisation');
}
 ?>" class="form-control" />
</div>
<div class="form-group">
<span class="control-label"><?php echo lang('contact_person')?></span><?php echo form_error('contact_person'); ?>
<input type="text" name="contact_person" value="<?php
if ($info) { echo $info['contact_person'];
} else {  echo set_value('contact_person');
}
  ?>" class="form-control" />
</div>
 <div class="form-group">
<span class="control-label"><?php echo lang('user_limit')?></span><?php echo form_error('user_limit'); ?>
<input type="text" name="user_limit" value="<?php
if ($info) { echo $info['user_limit'];
} else {  echo set_value('user_limit');
}
  ?>" class="form-control" />
</div>
<div class="form-group">
<span class="control-label"><?php echo lang('account_type')?></span><?php echo form_error('account_type'); ?>
<input type="text" name="account_type" value="<?php
if ($info) { echo $info['account_type'];
} else {  echo set_value('account_type');
}
  ?>" class="form-control" />
</div>
  
 <div class="form-group">
<span class="control-label"><?php echo lang('mobile'); ?></span><?php echo form_error('mobile'); ?>
<input type="text" name="mobile" value="<?php
if ($info) { echo $info['mobile'];
} else { echo set_value('mobile');
}
?>"  class="form-control"size="50"  />
</div> 

 <div class="form-group">
<span class="control-label"> <?php echo lang('email'); ?></span> <?php echo form_error('email'); ?>
<input type="text" name="email" value="<?php
if ($info) { echo $info['email'];
} else { echo set_value('email');
}
 ?>" class="form-control" />
</div>
  <div class="form-group">
 	<!-- if there is user details -->
 <?php	
 if ($info) {  ?>
 <input type="hidden" name="user_id" value="<?php echo $info['admin']; ?>" />
<?php

	}
 ?>
	<span class="control-label"><?php echo lang('username'); ?></span> <?php echo form_error('username'); ?>
<input type="text" name="username" value="<?php
if ($info) { echo $info['username'];
} else {echo set_value('username');
}
 ?>" class="form-control" />
</div>
 <div class="form-group">
<span class="control-label"><?php echo lang('role'); ?></span><?php echo form_error('role'); ?>

<?php
$roles = array( '1' => 'Administrator', );
if ($info) {$role = $info['role'];
} else {$role = set_value('role');
}
$style = ' class="form-control"';
echo form_dropdown('role', $roles, $role, $style);
?>
 
</div>
 <div class="form-group">
<span class="control-label"><?php echo lang('password')?></span><?php echo form_error('password'); ?>
<input type="password" name="password" value="" class="form-control" /></div>
  
 <div class="form-group">
<span class="control-label"><?php echo lang('conf_password'); ?></span><?php echo form_error('conf_password'); ?>
<input type="password"  name="conf_password" value="" class="form-control"  /></div>
 
  
<div class="form-actions"> 

<a href="<?php echo site_url('accounts'); ?>" type="button" class="btn btn-default">Cancel</a>
 <button type="submit" class="btn btn-info btn-large"><i class="icon-pencil"></i>Save</button></div>

								</div>
                            </fieldset>
                        </form>
                        </article>
                        </div>                        	
                        </div>	
                        </section>
                        		
			