<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Stocks extends User_Controller {

	public function __construct() {
		parent::__construct();
		$this -> load -> model('stocks_model');
		$this -> session_userid = $this -> session -> userdata('userid');
	}

	public function index() {

		$condUserPerm = array('userid' => $this -> session -> userdata('userid'));
		$user_permissions = $this -> user_model -> getUserPermissions($condUserPerm);
		$user_permissions = $this -> user_model -> getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] = "";
		$header['device_tab'] = "";
		$header['tasks_tab'] = "";
		$header['reports_tab'] = "";
		$header['deliveries_tab'] = "";
		$header['customers_tab'] = "";
		$header['boundaries_tab'] = "";
		$header['support_tab'] = "allow";
		$header['pathfinder_tab'] = "";
		$header['stocks_tab'] = "";
		$header['registrations_tab'] = "";

		foreach ($user_permissions as $user_permission) {
			if ($user_permission['permission_name'] == 'track tab') {
				$header['track_tab'] = "allow";
			} else if ($user_permission['permission_name'] == 'device tab') {
				$header['device_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'tasks tab') {
				$header['tasks_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'reports tab') {
				$header['reports_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'deliveries tab') {
				$header['deliveries_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'customers tab') {
				$header['customers_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'boundaries tab') {
				$header['boundaries_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'pathfinder survey tab') {
				$header['pathfinder_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'stocks tab') {
				$header['stocks_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'registrations tab') {
				$header['registrations_tab'] = "allow";
			}
		}

		if ($header['stocks_tab'] != "allow") {
			redirect('profile/support');
		}

		$fields['product_name'] = $this -> input -> post("product_name");
		$fields['device_imei'] = $this -> input -> post("phones");
		$fields['start_date'] = $this -> input -> post("start_date");
		$fields['end_date'] = $this -> input -> post("end_date");

		//preparing query
		$condQuery = " WHERE product_stocks.user_id ='" . $this -> session_userid . "'";

		$where_stock = array();

		if (!empty($fields['product_name'])) {
			$condQuery .= " AND product_stocks.product_name = '" . $fields['product_name'] . "'";
			$where_stock['product_name'] = $fields['product_name'];
		}

		if (!empty($fields['device_imei'])) {
			$condQuery .= " AND product_stocks.phone_imei = '" . $fields['device_imei'] . "'";
			$where_stock['phone_imei'] = $fields['device_imei'];
		}

		if (!empty($fields['end_date']) && !empty($fields['start_date'])) {
			$condQuery .= " AND product_stocks.stock_date >='" . $fields['start_date'] . "' AND product_stocks.stock_date <= '" . $fields['end_date'] . "'";
			$where_stock['stock_date  >='] = $fields['start_date'];
			$where_stock['stock_date <='] = $fields['end_date'];
		} else if (!empty($fields['start_date'])) {
			$condQuery .= " AND product_stocks.stock_date ='" . $fields['start_date'] . "'";
			$where_stock['stock_date'] = $fields['start_date'];
		}

		//remember the filter selection
		$this -> session -> set_userdata($where_stock);

		$cond = array('user_id' => $this -> session_userid);
		$fields['tb_data'] = $this -> stocks_model -> getProductStock($condQuery);
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js');
		$fields['tb_name'] = 'stocks_tb_name';
		// Put the fields from the database in this array
		$fields['row_fields'] = array('device_name', 'product_name', 'items_received', 'items_issued', 'items_returned', 'stock_date');
		$fields['tb_headers'] = array('device_name', 'product_name', 'items_received', 'items_issued', 'items_returned', 'stock_date', 'action');
		$header['stc_active'] = 'class="active"';
		$fields['add_btn'] = 'add_new_stock';

		$this -> load -> model('devices_model');
		$fields['devices'] = $this -> devices_model -> get_devicelistbyuserid($this -> session_userid);

		$productCond = array('user_id' => $this -> session_userid);
		$fields['products'] = $this -> stocks_model -> getContent('products', $productCond);

		$phones = array('userid' => $this -> session -> userdata('userid'));
		$fields['phones'] = $this -> stocks_model -> get_phones($phones);

		$fields['print_excel'] = "stocks/device_stock_export";

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/stocks_table', $fields);
		$this -> load -> view('template/footer', $ft_data);
	}

	public function device_stock_export() {

		$condQuery = " WHERE product_stocks.user_id ='" . $this -> session_userid . "'";

		if ($this -> session -> userdata('product_name') !== FALSE) {
			$condQuery .= " AND product_stocks.product_name = '" . $this -> session -> userdata['product_name'] . "'";

		}

		if ($this -> session -> userdata('device_imei') !== FALSE) {
			$condQuery .= " AND product_stocks.phone_imei = '" . $this -> session -> userdata['phone_imei'] . "'";

		}

		if ($this -> session -> userdata('start_date >=') !== FALSE) {
			$condQuery .= " AND product_stocks.stock_date >='" . $this -> session -> userdata['start_date >='] . "' AND product_stocks.stock_date <= '" . $this -> session -> userdata['stock_date <='] . "'";

		} else if ($this -> session -> userdata('start_date') !== FALSE) {
			$condQuery .= " AND product_stocks.stock_date ='" . $this -> session -> userdata['start_date'] . "'";

		}

		$this -> session -> unset_userdata(array('start_date' => '', 'end_date' => "", 'start_date >=' => '', 'stock_date <=' => '', 'phone_imei' => '', 'product_name' => ''));

		$fields['tb_data'] = $this -> stocks_model -> getProductStock($condQuery);

		$this -> print_excel($fields['tb_data']);

		redirect('index');

	}

	public function returedstock_excel() {
		$condQuery = " WHERE returned_stocks.user_id ='" . $this -> session_userid . "'";

		if ($this -> session -> userdata('product_name') !== FALSE) {
			$condQuery .= " AND returned_stocks.product_name = '" . $fields['product_name'] . "'";

		}

		if ($this -> session -> userdata('device_imei') !== FALSE) {
			$condQuery .= " AND returned_stocks.phone_imei = '" . $fields['device_imei'] . "'";

		}

		if ($this -> session -> userdata('end_date') !== FALSE) {
			$condQuery .= " AND returned_stocks.stock_date >='" . $this -> session -> userdata('start_date') . "' AND returned_stocks.stock_date <= '" . $this -> session -> userdata('end_date') . "'";

		} else if ($this -> session -> userdata('start_date') !== FALSE) {
			$condQuery .= " AND returned_stocks.stock_date ='" . $this -> session -> userdata('start_date') . "'";

		}

		$this -> session -> unset_userdata(array('start_date' => '', 'end_date' => '', 'start_date >=' => '', 'stock_date <=' => '', 'phone_imei' => '', 'product_name' => ''));

		$fields['tb_data'] = $this -> stocks_model -> getReturnedStock($condQuery);
		$this -> print_excel($fields['tb_data']);

		redirect('returnedStocks');
	}

	public function returnedStocks() {
		$condUserPerm = array('userid' => $this -> session -> userdata('userid'));
		$user_permissions = $this -> user_model -> getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] = "";
		$header['device_tab'] = "";
		$header['tasks_tab'] = "";
		$header['reports_tab'] = "";
		$header['deliveries_tab'] = "";
		$header['customers_tab'] = "";
		$header['boundaries_tab'] = "";
		$header['support_tab'] = "allow";
		$header['pathfinder_tab'] = "";
		$header['stocks_tab'] = "";
		$header['registrations_tab'] = "";

		foreach ($user_permissions as $user_permission) {
			if ($user_permission['permission_name'] == 'track tab') {
				$header['track_tab'] = "allow";
			} else if ($user_permission['permission_name'] == 'device tab') {
				$header['device_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'tasks tab') {
				$header['tasks_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'reports tab') {
				$header['reports_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'deliveries tab') {
				$header['deliveries_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'customers tab') {
				$header['customers_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'boundaries tab') {
				$header['boundaries_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'pathfinder survey tab') {
				$header['pathfinder_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'stocks tab') {
				$header['stocks_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'registrations tab') {
				$header['registrations_tab'] = "allow";
			}
		}

		if ($header['stocks_tab'] != "allow") {
			redirect('profile/support');
		}

		$fields['product_name'] = $this -> input -> post("product_name");
		$fields['device_imei'] = $this -> input -> post("phones");
		$fields['start_date'] = $this -> input -> post("start_date");
		$fields['end_date'] = $this -> input -> post("end_date");

		//preparing query
		$condQuery = " WHERE returned_stocks.user_id ='" . $this -> session_userid . "'";
		$where_excel = array();
		if (!empty($fields['product_name'])) {
			$condQuery .= " AND returned_stocks.product_name = '" . $fields['product_name'] . "'";
			$where_excel['product_name'] = $fields['product_name'];
		}

		if (!empty($fields['device_imei'])) {
			$condQuery .= " AND returned_stocks.phone_imei = '" . $fields['device_imei'] . "'";
			$where_excel['product_name'] = $fields['product_name'];
		}

		if (!empty($fields['end_date']) && !empty($fields['start_date'])) {
			$condQuery .= " AND returned_stocks.stock_date >='" . $fields['start_date'] . "' AND returned_stocks.stock_date <= '" . $fields['end_date'] . "'";
			$where_excel['start_date'] = $fields['start_date'];
			$where_excel['end_date'] = $fields['end_date'];
		} else if (!empty($fields['start_date'])) {
			$condQuery .= " AND returned_stocks.stock_date ='" . $fields['start_date'] . "'";
			$where_excel['stock_date'] = $fields['start_date'];
		}

		$this -> session -> set_userdata($where_excel);

		$cond = array('user_id' => $this -> session_userid);
		$fields['tb_data'] = $this -> stocks_model -> getReturnedStock($condQuery);
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js');
		$fields['tb_name'] = 'returned_stocks_tb_name';
		// Put the fields from the database in this array
		$fields['row_fields'] = array('device_name', 'product_name', 'returned_items', 'stock_date', 'status');
		$fields['tb_headers'] = array('device_name', 'product_name', 'returned_items', 'stock_date', 'status', 'action');
		$header['stre_active'] = 'class="active"';
		$fields['add_btn'] = 'return_stock';

		$this -> load -> model('devices_model');
		$fields['devices'] = $this -> devices_model -> get_devicelistbyuserid($this -> session_userid);

		$productCond = array('user_id' => $this -> session_userid);
		$fields['products'] = $this -> stocks_model -> getContent('products', $productCond);

		$phones = array('userid' => $this -> session -> userdata('userid'));
		$fields['phones'] = $this -> stocks_model -> get_phones($phones);
		$fields['print_excel'] = 'stocks/returedstock_excel';

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/stocks_table', $fields);
		$this -> load -> view('template/footer', $ft_data);
	}

	public function products() {
		$condUserPerm = array('userid' => $this -> session -> userdata('userid'));
		$user_permissions = $this -> user_model -> getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] = "";
		$header['device_tab'] = "";
		$header['tasks_tab'] = "";
		$header['reports_tab'] = "";
		$header['deliveries_tab'] = "";
		$header['customers_tab'] = "";
		$header['boundaries_tab'] = "";
		$header['support_tab'] = "allow";
		$header['pathfinder_tab'] = "";
		$header['stocks_tab'] = "";
		$header['registrations_tab'] = "";

		foreach ($user_permissions as $user_permission) {
			if ($user_permission['permission_name'] == 'track tab') {
				$header['track_tab'] = "allow";
			} else if ($user_permission['permission_name'] == 'device tab') {
				$header['device_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'tasks tab') {
				$header['tasks_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'reports tab') {
				$header['reports_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'deliveries tab') {
				$header['deliveries_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'customers tab') {
				$header['customers_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'boundaries tab') {
				$header['boundaries_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'pathfinder survey tab') {
				$header['pathfinder_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'stocks tab') {
				$header['stocks_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'registrations tab') {
				$header['registrations_tab'] = "allow";
			}
		}

		if ($header['stocks_tab'] != "allow") {
			redirect('profile/support');
		}
		//posting data from search form

		$fields['product_name'] = $this -> input -> post("product_name");
		$fields['device_imei'] = $this -> input -> post("phones");
		$fields['start_date'] = $this -> input -> post("start_date");
		$fields['end_date'] = $this -> input -> post("end_date");

		//preparing query
		$condQuery = " WHERE user_id ='" . $this -> session_userid . "'";

		//adding the condition to the query
		$where_stock = array('user_id' => $this -> session_userid);
		if (!empty($fields['product_name'])) {
			$condQuery .= " AND product_name = '" . $fields['product_name'] . "'";
			$where_stock['product_name'] = $fields['product_name'];
		}

		$this -> session -> set_userdata($where_stock);

		$cond = array('user_id' => $this -> session_userid);
		$fields['tb_data'] = $this -> stocks_model -> getProductList($condQuery);
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js');
		$fields['tb_name'] = 'products_tb_name';
		// Put the fields from the database in this array
		$fields['row_fields'] = array('product_name', 'description', 'items_on_hand');
		$fields['tb_headers'] = array('product_name', 'description', 'items_on_hand', 'action');
		$header['stpr_active'] = 'class="active"';
		$fields['add_btn'] = 'add_new_product';

		$productCond = array('user_id' => $this -> session_userid);
		$fields['products'] = $this -> stocks_model -> getContent('products', $productCond);
		$fields['print_excel'] = "stocks/products_export";
		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/stocks_table', $fields);
		$this -> load -> view('template/footer', $ft_data);
	}

	public function products_export() {
		//check conditions

		//preparing query
		$condQuery = " WHERE user_id ='" . $this -> session_userid . "'";

		/*
		 *  check the conditions
		 */

		if ($this -> session -> userdata('product_name') !== FALSE) {
			$condQuery .= " AND product_name = '" . $this -> session -> userdata('product_name') . "'";
		}

		/*
		 *  check the conditions ends
		 */

		$cond = array('user_id' => $this -> session_userid);
		$fields['tb_data'] = $this -> stocks_model -> getProductList($condQuery);

		$this -> print_excel($fields['tb_data']);

		$this -> session -> unset_userdata(array('start_date' => '', 'start_date >=' => '', 'stock_date <=' => '', 'phone_imei' => '', 'product_name' => ''));

		redirect('products');

	}

	public function addProduct() {
		extract($_POST);
		$data['msg'] = "";
		$data['errorflag'] = 0;
		$data['product_name'] = $this -> input -> post("product_name");
		$data['description'] = $this -> input -> post("description");
		$data['items_on_hand'] = $this -> input -> post("items_on_hand");
		if (!empty($_POST)) {
			$this -> form_validation -> set_rules('product_name', 'Product Name', 'required');
			$this -> form_validation -> set_rules('description', 'Product Description', 'required');
			if ($this -> form_validation -> run()) {
				$datum = array('product_name' => $this -> input -> post('product_name'), 'user_id' => $this -> session -> userdata('userid'), 'description' => $this -> input -> post('description'), 'items_on_hand' => $this -> input -> post('items_on_hand'));

				$cond = array('product_name' => $this -> input -> post('product_name'), 'user_id' => $this -> session -> userdata('userid'));
				$prd = $this -> stocks_model -> getContent("products", $cond);
				$counter = count($prd);
				if ($counter == 0) {
					$tsResult = $this -> stocks_model -> addContent("products", $datum);
				}
			}
		}
		redirect('stocks/products');
	}

	public function editProduct($productname, $userid) {
		$cond = array('product_name' => urldecode($productname), 'user_id' => $userid);
		$fields['products'] = $this -> stocks_model -> getContent('products', $cond);
		$this -> load -> view('template/content/edit_product', $fields);
	}

	public function editProductUpdated($productname, $userid) {
		$cond = array('product_name' => urldecode($productname), 'user_id' => $userid);
		$fields['products'] = $this -> stocks_model -> getContent('products', $cond);
		$this -> load -> view('template/content/add_product_stock', $fields);
	}

	public function updateProduct() {
		$data = array('description' => $this -> input -> post('description'), 'items_on_hand' => $this -> input -> post('items_on_hand'));
		$cond = array('product_name' => $this -> input -> post('product_name'), 'user_id' => $this -> input -> post('userid'));
		$this -> stocks_model -> update("products", $data, $cond);
		redirect('stocks/products');
	}

	public function addProductStock() {
		$cond = array('product_name' => $this -> input -> post('product_name'), 'user_id' => $this -> input -> post('userid'));
		$prd = $this -> stocks_model -> getContent("products", $cond);
		$counter = count($prd);
		if ($counter > 0) {

			$items = $prd[0]['items_on_hand'] + $this -> input -> post('items_to_add');
			$data = array('items_on_hand' => $items);
			$this -> stocks_model -> update("products", $data, $cond);
		}

		redirect('stocks/products');
	}

	public function editStock($productname, $userid, $phoneimei, $stockdate) {
		$cond = array('product_name' => urldecode($productname), 'user_id' => $userid, 'phone_imei' => urldecode($phoneimei), 'stock_date' => urldecode($stockdate));
		$fields['stocks'] = $this -> stocks_model -> getContent('product_stocks', $cond);
		$this -> load -> model('devices_model');
		$fields['devices'] = $this -> devices_model -> get_devicelistbyuserid($this -> session_userid);
		$productCond = array('user_id' => $this -> session_userid);
		$fields['products'] = $this -> stocks_model -> getContent('products', $productCond);
		$this -> load -> view('template/content/edit_stock', $fields);
	}

	public function returnStock($productname, $userid, $phoneimei, $stockdate) {
		$cond = array('product_name' => urldecode($productname), 'user_id' => $userid, 'phone_imei' => urldecode($phoneimei), 'stock_date' => urldecode($stockdate));
		$fields['stocks'] = $this -> stocks_model -> getContent('returned_stocks', $cond);
		$this -> load -> model('devices_model');
		$fields['devices'] = $this -> devices_model -> get_devicelistbyuserid($this -> session_userid);
		$productCond = array('user_id' => $this -> session_userid);
		$fields['products'] = $this -> stocks_model -> getContent('products', $productCond);
		$this -> load -> view('template/content/return_stock', $fields);
	}

	public function updateStock() {
		$data = array('items_received' => $this -> input -> post('items_received'), 'items_issued' => $this -> input -> post('items_issued'));

		$cond = array('product_name' => $this -> input -> post('product_name'), 'user_id' => $this -> input -> post('userid'), 'phone_imei' => $this -> input -> post('phone_imei'), 'stock_date' => $this -> input -> post('stock_date'));
		$this -> stocks_model -> update("product_stocks", $data, $cond);
		redirect('stocks');
	}

	public function updateReturnedStock() {
		$data = array('returned_items' => $this -> input -> post('returned_items'), 'status' => $this -> input -> post('status'));

		$cond = array('product_name' => $this -> input -> post('product_name'), 'user_id' => $this -> input -> post('userid'), 'phone_imei' => $this -> input -> post('phone_imei'), 'stock_date' => $this -> input -> post('stock_date'));

		$condProd = array("product_name" => $this -> input -> post('product_name'), "user_id" => $this -> session -> userdata('userid'));

		$status = $this -> input -> post('status');

		if ($status == "approved") {
			$product = $this -> stocks_model -> getContent('products', $condProd);
			$itemsonhand = $product[0]['items_on_hand'] + $this -> input -> post('returned_items');
			$dataUpdate = array('items_on_hand' => $itemsonhand);
			$response = $this -> stocks_model -> update('products', $dataUpdate, $condProd);
		}

		$this -> stocks_model -> update("returned_stocks", $data, $cond);
		redirect('stocks');
	}

	public function editClient($clientname, $userid, $shopname) {
		$cond = array('client_name' => urldecode($clientname), 'user_id' => $userid, 'shop_name' => urldecode($shopname));
		$fields['clients'] = $this -> stocks_model -> getContent('stock_clients', $cond);
		$this -> load -> view('template/content/edit_client', $fields);
	}

	public function updateClient() {
		$data = array('phone' => $this -> input -> post('phone'), 'email' => $this -> input -> post('email'), 'address' => $this -> input -> post('address'));

		$cond = array('client_name' => $this -> input -> post('client_name'), 'user_id' => $this -> input -> post('userid'), 'shop_name' => $this -> input -> post('shop_name'));

		$this -> stocks_model -> update("stock_clients", $data, $cond);
		redirect('stocks/clients');
	}

	public function addStock() {
		extract($_POST);
		$data['msg'] = "";
		$data['errorflag'] = 0;
		$data['product_name'] = $this -> input -> post("product_name");
		$data['phone_imei'] = $this -> input -> post("phone_imei");
		$data['items'] = $this -> input -> post("items");
		$data['msg'] = "";
		if (!empty($_POST)) {
			$this -> form_validation -> set_rules('product_name', 'Product Name', 'required');
			$this -> form_validation -> set_rules('phone_imei', 'Device Description', 'required');
			$this -> form_validation -> set_rules('items', 'Items Received', 'required');
			if ($this -> form_validation -> run()) {
				$date = date("Y-m-d h:i:s");
				$userid = $this -> session -> userdata('userid');

				$arrDevices = $deviceImei = $data['phone_imei'];
				$arrProducts = $data['product_name'];
				$arrItems = $data['items'];

				$countDevices = count($arrDevices);
				$countPrododucts = count($arrProducts);
				$countItems = count($arrItems);

				//now checking how many saved and how many fail
				$successProduct = 0;
				$failProducts = 0;

				//				foreach($arrDevices as $deviceImei){
				for ($i = 0; $i < $countPrododucts; $i++) {
					$productname = $arrProducts[$i];
					$quantity = $arrItems[$i];

					//checking if productname and quantity is not empty
					if (!empty($productname) && !empty($quantity)) {
						$condProd = array("product_name" => $productname, "user_id" => $userid);
						$productStock = $this -> stocks_model -> getContent('products', $condProd);
						if ($productStock[0]['items_on_hand'] >= $quantity) {
							//check if product exist with enough stock
							//deduct the assigned stock 
							$itemsonhand = $productStock[0]['items_on_hand'] - $quantity;
							$dataUpdate = array('items_on_hand' => $itemsonhand);
							$response = $this -> stocks_model -> update('products', $dataUpdate, $condProd);

							if ($response) {
								//check if stock exist in stock table
								$condz = array('product_name' => $productname, 'user_id' => $userid, 'phone_imei' => $deviceImei, 'stock_date' => $date);

								$jibu = $this -> stocks_model -> getContent("product_stocks", $condz);
								$jibuCount = count($jibu);

								if ($jibuCount > 0) {
									$data = array('items_received' => $quantity + $jibu[0]['items_received'],'stock_date'=>$date, 'is_synced' => 0);
									$this -> stocks_model -> update("product_stocks", $data, $condz);
								} else {
									$datum = array('product_name' => $productname, 'user_id' => $userid, 'phone_imei' => $deviceImei, 'items_received' => $quantity, 'stock_date' => $date, 'is_synced' => 0);
									$tsResult = $this -> stocks_model -> addContent("product_stocks", $datum);
									$successProduct = $successProduct + 1;
								}
							} else {
								$failProducts = $failProducts + 1;
							}

						} else {
							$failProducts = $failProducts + 1;
						}
					} else {
						$failProducts = $failProducts + 1;
					}

				}

				//}

				echo "Success ni :" . $successProduct . "   Na zilizofail ni: " . $failProducts;

			}
		}

		redirect('stocks');
	}

	public function addClient() {
		extract($_POST);
		$data['msg'] = "";
		$data['errorflag'] = 0;
		$data['client_name'] = $this -> input -> post("client_name");
		$data['phone'] = $this -> input -> post("phone");
		$data['email'] = $this -> input -> post("email");
		$data['shop_name'] = $this -> input -> post("shop_name");
		if (!empty($_POST)) {
			$this -> form_validation -> set_rules('client_name', 'Client Name', 'required');
			$this -> form_validation -> set_rules('phone', 'Phone', 'required');
			$this -> form_validation -> set_rules('email', 'Email', 'required');
			$this -> form_validation -> set_rules('shop_name', 'Shop Name', 'required');
			$this -> form_validation -> set_rules('address', 'address', 'required');
			if ($this -> form_validation -> run()) {
				$datum = array('client_name' => $this -> input -> post('client_name'), 'user_id' => $this -> session -> userdata('userid'), 'phone' => $this -> input -> post('phone'), 'email' => $this -> input -> post('email'), 'shop_name' => $this -> input -> post('shop_name'), 'address' => $this -> input -> post('address'), 'location' => "");

				$tsResult = $this -> stocks_model -> addContent("stock_clients", $datum);
			}
		}

		redirect('stocks/clients');
	}

	public function sales() {
		$condUserPerm = array('userid' => $this -> session -> userdata('userid'));
		$user_permissions = $this -> user_model -> getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] = "";
		$header['device_tab'] = "";
		$header['tasks_tab'] = "";
		$header['reports_tab'] = "";
		$header['deliveries_tab'] = "";
		$header['customers_tab'] = "";
		$header['boundaries_tab'] = "";
		$header['support_tab'] = "allow";
		$header['pathfinder_tab'] = "";
		$header['stocks_tab'] = "";
		$header['registrations_tab'] = "";

		foreach ($user_permissions as $user_permission) {
			if ($user_permission['permission_name'] == 'track tab') {
				$header['track_tab'] = "allow";
			} else if ($user_permission['permission_name'] == 'device tab') {
				$header['device_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'tasks tab') {
				$header['tasks_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'reports tab') {
				$header['reports_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'deliveries tab') {
				$header['deliveries_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'customers tab') {
				$header['customers_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'boundaries tab') {
				$header['boundaries_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'pathfinder survey tab') {
				$header['pathfinder_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'stocks tab') {
				$header['stocks_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'registrations tab') {
				$header['registrations_tab'] = "allow";
			}
		}

		if ($header['stocks_tab'] != "allow") {
			redirect('profile/support');
		}

		$fields['product_name'] = $this -> input -> post("product_name");
		$fields['device_imei'] = $this -> input -> post("phones");
		$fields['start_date'] = $this -> input -> post("start_date");
		$fields['end_date'] = $this -> input -> post("end_date");

		//preparing query
		$condQuery = " WHERE sales.user_id ='" . $this -> session_userid . "'";
		$where_excel = array();
		if (!empty($fields['product_name'])) {
			$condQuery .= " AND sales.product_name = '" . $fields['product_name'] . "'";
			$where_excel['product_name'] = $fields['product_name'];
		}

		if (!empty($fields['device_imei'])) {
			$condQuery .= " AND sales.phone_imei = '" . $fields['device_imei'] . "'";
			$where_excel['product_name'] = $fields['product_name'];
		}

		if (!empty($fields['end_date']) && !empty($fields['start_date'])) {
			$condQuery .= " AND sales.sales_datetime >='" . $fields['start_date'] . "' AND sales.sales_datetime <= '" . $fields['end_date'] . "'";
			$where_excel['end_date'] = $fields['end_date'];
			$where_excel['start_date'] = $fields['start_date'];
		} else if (!empty($fields['start_date'])) {
			$condQuery .= " AND sales.sales_datetime ='" . $fields['start_date'] . "'";
			$where_excel['start_date'] = $fields['start_date'];
		}

		$this -> session -> set_userdata($where_excel);
		$cond = array('user_id' => $this -> session_userid);
		$fields['tb_data'] = $this -> stocks_model -> getProductSales($condQuery);
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js');
		$fields['tb_name'] = 'sales_tb_name';
		// Put the fields from the database in this array
		$fields['row_fields'] = array('device_name', 'product_name', 'client_name', 'sales_datetime', 'quantity', 'price');
		$fields['tb_headers'] = array('device_name', 'product_name', 'client_name', 'sales_datetime', 'quantity', 'price', 'action');
		$header['stsa_active'] = 'class="active"';
		$fields['add_btn'] = 'add_new_sales';

		$this -> load -> model('devices_model');
		$fields['devices'] = $this -> devices_model -> get_devicelistbyuserid($this -> session_userid);

		$productCond = array('user_id' => $this -> session_userid);
		$fields['products'] = $this -> stocks_model -> getContent('products', $productCond);

		$phones = array('userid' => $this -> session -> userdata('userid'));
		$fields['phones'] = $this -> stocks_model -> get_phones($phones);
		$fields['print_excel'] = 'stocks/sales_excel';
		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/stocks_table', $fields);
		$this -> load -> view('template/footer', $ft_data);
	}

	public function sales_excel() {
		//preparing query
		$condQuery = " WHERE sales.user_id ='" . $this -> session_userid . "'";

		if ($this -> session -> userdata('product_name') !== FALSE) {
			$condQuery .= " AND sales.product_name = '" . $this -> session -> userdata('product_name') . "'";

		}

		if ($this -> session -> userdata('device_imei') !== FALSE) {
			$condQuery .= " AND sales.phone_imei = '" . $this -> session -> userdata('device_imei') . "'";

		}

		if ($this -> session -> userdata('end_date') !== FALSE) {
			$condQuery .= " AND sales.sales_datetime >='" . $this -> session -> userdata('start_date') . "' AND sales.sales_datetime <= '" . $this -> session -> userdata('end_date') . "'";

		} else if ($this -> session -> userdata('start_date') !== FALSE) {
			$condQuery .= " AND sales.sales_datetime ='" . $this -> session -> userdata('start_date') . "'";

		}

		$this -> session -> unset_userdata(array('start_date' => '', 'end_date' => '', 'phone_imei' => '', 'product_name' => ''));

		$fields['tb_data'] = $this -> stocks_model -> getProductSales($condQuery);
		$this -> print_excel($fields['tb_data']);

		redirect('sales');

	}

	public function clients() {
		$condUserPerm = array('userid' => $this -> session -> userdata('userid'));
		$user_permissions = $this -> user_model -> getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] = "";
		$header['device_tab'] = "";
		$header['tasks_tab'] = "";
		$header['reports_tab'] = "";
		$header['deliveries_tab'] = "";
		$header['customers_tab'] = "";
		$header['boundaries_tab'] = "";
		$header['support_tab'] = "allow";
		$header['pathfinder_tab'] = "";
		$header['stocks_tab'] = "";
		$header['registrations_tab'] = "";

		foreach ($user_permissions as $user_permission) {
			if ($user_permission['permission_name'] == 'track tab') {
				$header['track_tab'] = "allow";
			} else if ($user_permission['permission_name'] == 'device tab') {
				$header['device_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'tasks tab') {
				$header['tasks_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'reports tab') {
				$header['reports_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'deliveries tab') {
				$header['deliveries_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'customers tab') {
				$header['customers_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'boundaries tab') {
				$header['boundaries_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'pathfinder survey tab') {
				$header['pathfinder_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'stocks tab') {
				$header['stocks_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'registrations tab') {
				$header['registrations_tab'] = "allow";
			}
		}

		if ($header['stocks_tab'] != "allow") {
			redirect('profile/support');
		}

		$fields['shop_name'] = $this -> input -> post("shop_name");
		$fields['client_name'] = $this -> input -> post("client_name");
		$fields['phone'] = $this -> input -> post("phone");
		$fields['email'] = $this -> input -> post("email");

		$condQuery = " WHERE user_id ='" . $this -> session_userid . "'";
		$where_excel = array();
		if (!empty($fields['shop_name'])) {
			$condQuery .= " AND shop_name = '" . $fields['shop_name'] . "'";
			$where_exce['shop_name'] = $fields['shop_name'];
		}

		if (!empty($fields['client_name'])) {
			$condQuery .= " AND client_name = '" . $fields['client_name'] . "'";
			$where_exce['client_name'] = $fields['client_name'];
		}

		if (!empty($fields['phone'])) {
			$condQuery .= " AND phone = '" . $fields['phone'] . "'";
			$where_exce['client_name'] = $fields['client_name'];
		}

		if (!empty($fields['email'])) {
			$condQuery .= " AND email = '" . $fields['email'] . "'";
			$where_exce['email'] = $fields['email'];
		}
		if (!empty($fields['device_imei'])) {
			$condQuery .= " AND  phone_imei = '" . $fields['device_imei'] . "'";
			$where_stock['phone_imei'] = $fields['device_imei'];
		}

		$cond = array('user_id' => $this -> session_userid);
		$fields['tb_data'] = $this -> stocks_model -> getClientList($condQuery);
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js');
		$fields['tb_name'] = 'clients_tb_name';
		// Put the fields from the database in this array
		$fields['row_fields'] = array('client_name', 'phone', 'email', 'shop_name', 'address','device_imei');
		$fields['tb_headers'] = array('client_name', 'phone', 'email', 'shop_name', 'address','device_imei', 'action');
		$header['stcl_active'] = 'class="active"';
		$fields['add_btn'] = 'add_new_client';
		$fields['print_excel'] = 'stocks/clients_excel';
		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/stocks_table', $fields);
		$this -> load -> view('template/footer', $ft_data);
	}

	public function clients_excel() {
		$condQuery = " WHERE user_id ='" . $this -> session_userid . "'";

		if ($this -> session -> userdata('shop_name') !== FALSE) {
			$condQuery .= " AND shop_name = '" . $this -> session -> userdata('shop_name') . "'";
		}

		if ($this -> session -> userdata('client_name') !== FALSE) {
			$condQuery .= " AND client_name = '" . $this -> session -> userdata('client_name') . "'";
		}

		if ($this -> session -> userdata('phone') !== FALSE) {
			$condQuery .= " AND phone = '" . $this -> session -> userdata('phone') . "'";
		}

		if ($this -> session -> userdata('email') !== FALSE) {
			$condQuery .= " AND email = '" . $this -> session -> userdata('email') . "'";
		}
		if ($this -> session -> userdata('device_imei') !== FALSE) {
			$condQuery .= " AND product_stocks.phone_imei = '" . $this -> session -> userdata('device_imei') . "'";

		}

		$fields['tb_data'] = $this -> stocks_model -> getClientList($condQuery);

		$this -> session -> unset_userdata(array('phone' => '', 'device_imei' => '', 'shop_name' => '', 'start_date' => '', 'email' => '', 'client_name' => '', 'end_date' => "", 'start_date >=' => '', 'stock_date <=' => '', 'phone_imei' => '', 'product_name' => ''));
		$this -> print_excel($fields['tb_data']);
		redirect('clients');
	}

	/*
	 * printing excel
	 */

	public function print_excel($posted_data) {

		// Original PHP code by Chirp Internet: www.chirp.com.au
		// Please acknowledge use of this code by including this header.

		$this -> cleanData($str);

		// filename for download
		$filename = "prostock_" . date('Ymd') . ".xls";

		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-Type: application/vnd.ms-excel");

		$flag = false;
		if ($posted_data) {
			foreach ($posted_data as $row) {
				 unset($row['user_id'],$row['is_synced']);
				 if (!$flag) {
					// display field/column names as first row
					echo implode("\t", array_keys($row)) . "\r\n";
					$flag = true;
				}
				array_walk($row, array($this, 'cleanData'));
				echo implode("\t", array_values($row)) . "\r\n";
			  
			}
		}
		exit ;
	}

	public function cleanData(&$str) {
		$str = preg_replace("/\t/", "\\t", $str);
		$str = preg_replace("/\r?\n/", "\\n", $str);
		if (strstr($str, '"'))
			$str = '"' . str_replace('"', '""', $str) . '"';
	}

	/*
	 * printing to excel ends
	 */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
