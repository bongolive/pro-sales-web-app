<?php

class LangSwitch extends  CI_Controller {
  
	function english($language = "") {
		
		 $language = ($language != "") ? $language : "english";
		 $this -> session -> set_userdata('site_lang', $language);
		redirect(base_url());
	}
	function kiswahili($language = "") {
		
		 $language = ($language != "") ? $language : "kiswahili";
		 $this -> session -> set_userdata('site_lang', $language);
		redirect(base_url());
	}

}
