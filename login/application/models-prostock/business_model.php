<?php
class Business_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	
	public function getCustomer($cond)
	{
		$query = $this->db->get_where('businesses', $cond);
		return $query->result_array();
	}
	
	public function add($data)
	{
		return $this->db->insert('businesses', $data);
	}
	
	public function update($data,$cond)
	{
		$this->db->where($cond);
		return $this->db->update('businesses', $data);
	}
	
	public function delete($cond)
	{
		return $this->db->delete('businesses', $cond);
	}

}