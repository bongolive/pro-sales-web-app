<?php
/**
 * Created by PhpStorm.
 * User: nasznjoka
 * Date: 4/1/15
 * Time: 4:07 PM
 */


    class Contact extends MY_Controller{
        public $user_id;
        public $account_id;
        function __construct() {
            parent::__construct();
            $this->load->model('products_model');
            $this->load->helper('url');
            $this->load->helper('form');
            $this -> user_id = $this -> session -> userdata('userid');
            $this -> account_id = $this -> session -> userdata('account_id');
            $this -> fields['controller'] = 'contact';
            $this -> fields['get_all_users'] = 'orders/get_all_users';


        }

        public function index(){
            $header['stc_active'] = 'class="active"';


            $where = array('account_id' => $this -> account_id);

            //get products
            $products = $this -> products_model -> read($where);

            if ($products) {
                $product[] = 'Select Product';
                foreach ($products as $products) {
                    $product[$products['product_id']] = $products['product_name'];
                }
            } else {
                $product = FALSE;
            }


            $this -> fields['info'] = FALSE;
            $this -> fields['products'] = $product;

            $this -> load -> view('template/header', $header);
            $this -> load -> view('template/content/ajaxtest' ,$this->fields);
            $this -> load -> view('template/footer');
        }

        public function get_product_price()
        {
            $received = $this->input->post('product_id');
//        var_dump($received);
//        $received = json_decode($received);
//        file_put_contents('ajaxjson.txt','the received data is '.$received);
            $query = $this->products_model->get_unit_price("2");

            $output_string = '';
            if(!is_null($query)) {
                $output_string .= "{$query}";
            } else {
                $output_string = 'There are no price found';
            }
            echo json_encode($output_string);
//        echo json_encode($received);
        }

        public function get_all_users()
        {

            $query = $this->products_model->fetch_productsbyaccount($this->account_id);
            if ($query) {
                    $output_string = '';
                    $output_string .= "<table border='1'>\n";
//                    print_r($query);
                    foreach ($query as $row) {
                        $output_string .= '<tr>';
                        $output_string .= "<th>{$row['product_name']}</th>\n";
                        $output_string .= "<th>{$row['unit_sale_price']}</th>\n";
                        $output_string .= "<th>{$row['description']}</th>\n";
                        $output_string .= "<th>{$row['items_on_hand']}</th>\n";
                        $output_string .= "<th>{$row['assigned_stock']}</th>\n";
                        $output_string .= '</tr>';
                    }
                    $output_string .= '</table>';
                } else {
                    $output_string = 'There are no results';
                }
                echo json_encode($output_string);
            }

        function get_list_view(){


            $data = array();

            $data['title'] = 'Lorem ipsum';
            $data['list'] = $this->products_model->get_data();

            $this->load->view('sample_table', $data);

        }
        public function getUnitPrice(){
            if($this->input->post('product_id',TRUE))
                $this->fields['prodprice'] = $this->products_model->fetch_unit_price($this->input->post('product_id'));
            $this->load->view('template/content/ajaxtest',$this->fields);
        }

    }