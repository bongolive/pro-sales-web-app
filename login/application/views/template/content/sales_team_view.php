				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('sales_team'); echo ': '. strtoupper($team['employee_name']); ?></h2>
							
							
							<ul class="data-header-actions">
								<li >
									<a href="<?php echo site_url('stock/sales_team'); ?>" class="btn btn-alt"><?php _l('back'); ?></a>
								</li>
								<li>
									<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('stock/add_stock_to_team')."/". $team['id'];?>"><?php _l('add_stock_to_team');?></a>
							 	</li>
							</ul>
							
							
            </header> 
        </article>

    </div>
    
    
    <section class="container" role="main" >
<div class="row">
		<div class="span2 data-block widget-block" style="font-size:25px;">
				<section style="font-size:25px;">
					<strong class="widget-label"><?php echo $settings['default_currency']; ?></strong>
				<span style="font-size:25px;">  <?php echo money_format('%.2n',$t_sales); ?></span>
				<strong class="widget-label"><?php _l('daily_sales') ?></strong>
				</section>
		</div>
 
  		<div class="span3 data-block widget-block" style="font-size:25px;">
				<section style="font-size:25px;">
						<strong class="widget-label"><?php echo $settings['default_currency']; ?></strong>
				<span style="font-size:25px;"> 
					 <?php echo money_format('%.2n',$summary['total_sales']); ?></span>
				<strong class="widget-label"><?php _l('total_sales') ?></strong>
				</section>
		</div>
		<div class="span2 data-block widget-block" style="font-size:25px;">
				<section style="font-size:25px;">
						<strong class="widget-label"><?php echo $settings['default_currency']; ?></strong>
				<span style="font-size:25px;">   <?php echo money_format('%.2n',$summary['total_stock']-$summary['total_sales']); ?></span>
				<strong class="widget-label"><?php _l('stock_balance') ?></strong>
				</section>
		</div>
		<div class="span2 data-block widget-block " style="font-size:25px;">
			<section style="font-size:25px;">
					<strong class="widget-label"><?php echo $settings['default_currency']; ?></strong>
				 <span style="font-size:25px;">  <?php echo money_format('%.2n',$summary['total_stock']); ?></span>
				<strong class="widget-label"><?php _l('total_stock') ?></strong>
			</section>
			
		</div>
    <div class="span3 data-block widget-block " style="font-size:25px;">
			<section style="font-size:25px; color:green; ">
					<strong class="widget-label"><?php echo $settings['default_currency']; ?></strong>
				 <span style="font-size:25px;">  <?php
                      ?></span>
				<strong class="widget-label"><?php _l('team_commission') ?></strong>
			</section>
		</div>
</section>
    
    	
    <div class="row">
    	 <article class="span12 data-block">
								<div class="modal-header">
									<h4><?php _l('sales_team_history'); ?></h4>
								</div>
								<div class="modal-body">
									 
								
								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>

									<tr><th></th>

                                        <?php 
                                       
                                        foreach($tb_headers as $header){?>

										<th><?php
										if($header =='sale_price' || $header =='balance_value' ){
											_l($header); echo '<span style="color:#2980B9"> ('.$settings['default_currency'].')</span>';
										} else{
										 _l($header); } ?></th>

                                        <?php } ?>
									</tr>
								</thead>
								<tbody>
 				  <?php if ($tb_data){
 				   
									foreach ($tb_data as $data):  
									  
                                            $id = $table_id;
                                     ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
										<td><input type="checkbox"/></td>
										
                                        <?php foreach ($row_fields as $field): ?>
                                        	
										  <td>
										 	<?php if ($field == 'staff') {
											//check staff and way of submission
											echo ucwords($data['firstname'] . ' ' . $data['lastname']);
											if($data['device_imei']==0){
												echo '<span class="label label-success">Web</span>';
											}else{
												echo '<span class="label label-info">Mobile</span>';
											}
										} elseif ($field == 'order_status') {
											if ($data[$field] == 1) {
												echo '<span class="label label-inverse">'; _l('pending'); echo '</span>';
											//} elseif ($data[$field] == 2) {
												//echo '<span class="label label-info">New order</span>';
											} elseif ($data[$field] == 3) {
												echo '<span class="label label-important">Canceled</span>';
											} else {
												echo '<span class="label label-success">Processed</span>';
											}
										 }elseif ($field == 'paid_amount') {
                                                $amount = $data[$field];
                                                echo money_format('%.2n',$amount);
                                            }
                                            elseif ($field == 'sales_total') {
                                            $rate = $data[$field];
                                                echo money_format('%.2n',$rate);
                                            } elseif ($field == 'commission') {
                                                    $cost = $data[$field];
                                                echo money_format('%.2n', $rate * ($cost/100) );
                                            } else {
											echo ucwords($data[$field]);
										}?>  
										   </td>
                                     <?php endforeach; ?><td>
										  <div class="btn-group">
													<a href="<?php echo site_url('orders/view') . '/' . $data[$table_id]; ?>" class="btn btn-small"> <span class="icon-eye-open"/> </a> </div>
													</td>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td "'.count($tb_headers).'" > No Orders Available <td> </tr>';
										}
									?>
								</tbody>
	</table> 
           
		</article>							
  
                        </div>                        	
                        </div>	
                        </section>
                        		
					 