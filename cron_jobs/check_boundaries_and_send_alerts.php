<?php 

include ("../connection.php");

date_default_timezone_set("Africa/Dar_es_Salaam");
$curr_date = date("Y-m-d H:i:s");

$sql = "SELECT u.name, u.email, p.device_name, p.mobile_no, p.holder_name, ba.alocationid, ba.imei, b.alertname, b.centerlat, b.centerlng, b.radius, b.valid_from, b.valid_to, ba.laststatus, 
		
		ifNull((
		SELECT l.latitude
		FROM location l
		WHERE l.imei = ba.imei
		ORDER BY l.servertime DESC
		LIMIT  1
		),NULL) AS devicelat , 
		
		ifNull((
		SELECT l.longitude
		FROM location l
		WHERE l.imei = ba.imei
		ORDER BY l.servertime DESC
		LIMIT  1
		),NULL) AS devicelng

		FROM boundries b INNER JOIN boundry_alocation ba ON b.boundryid = ba.boundryid 
			INNER JOIN users u ON b.userid = u.userid 
			INNER JOIN phones p ON ba.imei = p.phone_imei 
		WHERE b.valid_from <= '".$curr_date."' AND b.valid_to >= '".$curr_date."' 
		ORDER BY b.alertname, u.name ASC";



$res = mysql_query($sql, $con);

while($row = mysql_fetch_assoc($res)){
	
	if($row["devicelat"] != NULL && $row["devicelng"] != NULL ){
		
		$dist_km = get_dist_km($row["devicelat"], $row["devicelng"], $row["centerlat"], $row["centerlng"]);
		//$dist_km = round($dist_km, 2);
		
		$message = "";
		
		$radius = floatval( $row["radius"]) / 1000;
		
		if( $dist_km <= $radius && $row["laststatus"] == "0"){
			
			$message .= "This is an email to notify you that your device named as '".$row["device_name"]."' entered in Boundary '".$row["alertname"]."'. ";
			
			send_email($row["email"], $message);
			set_new_status($row["alocationid"], "1");
			
		}else if($dist_km > $radius && $row["laststatus"] == "1"){
			
			$message .= "This is an email to notify you that your device named as '".$row["device_name"]."' leaved the Boundary '".$row["alertname"]."'. ";
			
			send_email($row["email"], $message);
			set_new_status($row["alocationid"], "0");
		}
		
	}
	
}


function get_dist_km($lat1, $lng1, $lat2, $lng2 ){
	
	$earthRadius = 3958.75;
	$dLat = deg2rad($lat2-$lat1);
	$dLng = deg2rad($lng2-$lng1);
	$a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * sin($dLng/2) * sin($dLng/2);
	$c = 2 * atan2(sqrt($a), sqrt(1-$a));
	$dist = $earthRadius * $c;
	
	$meterConversion = 1609.34;
	
	return (floatval($dist * $meterConversion) / 1000 );

}


function send_email($to, $msg_body){
	
	$line_brake_ch = "\r\n"; // "\r\n" for windows... "\n" for Unix... "\r" for MAC

	$sender_name = "Foot Print Site";
	$sender = "no-reply@pro.co.tz";
	$sender_organization = "Foot Prints";
	
	$message_subject = "Boundary Alert From Foot Print Site.";
		
	$headers = "From: ".$sender_name." <".$sender.">".$line_brake_ch;
	$headers .= "Reply-To: ".$sender_name." <".$sender.">".$line_brake_ch;
	$headers .= "Errors-To: ".$sender_name." <".$sender.">".$line_brake_ch;
	
	$headers .= "Organization: ".$sender_organization."".$line_brake_ch;
	$headers .= "MIME-Version: 1.0".$line_brake_ch;
	$headers .= "Content-type: text/html; charset=iso-8859-1".$line_brake_ch;
	$headers .= "X-Priority: 3".$line_brake_ch;
	$headers .= "X-Mailer: PHP". phpversion() .$line_brake_ch;
	
	if(mail($to, $message_subject, $msg_body, $headers, "-f".$sender)){
		//echo "<br />SENT...";
	}else{
		//echo "<br />FAILED...";
	}
}

function set_new_status($alocation_id, $new_status){
	global $con;
	$sql_update = "UPDATE boundry_alocation SET laststatus = '".$new_status."' WHERE alocationid = ".$alocation_id;
	mysql_query($sql_update, $con);
}

?>