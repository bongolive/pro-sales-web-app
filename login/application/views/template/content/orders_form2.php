				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('new_order');  ?></h2> 
						 
            			</header> 
       				 </article> 
    </div>	
    <div class="row">
     						
  
    	 <article class="span12 data-block">
								<div class="modal-header">
									<h4><?php _l('customer_details'); ?>
										<span class="icon-calendar" style="float: right; "><?php _l('order_date'); echo ': '.date('Y-m-d');?></span></h4>
								</div>
								<div class="modal-body">
						<?php  if($order){ ?>
							<form class="form-horizontal" method="post" action="<?php echo site_url('orders/update_order'); ?>">
								<input type="hidden" name="order_no" value="<?php echo $order['order_id']; ?>" />
							<?php }else{ ?>			
                                <form class="form-horizontal" method="post" action="<?php echo site_url('orders/new_order'); ?>">
                             <?php } ?>   	
                                                          
                                  	<table class="datatable table table-striped table-bordered table-hover">
                               <tr>
                               	<td>
                               		 <div class="control-group">
                                        	<label class="control-label" for="customer_name"><?php _l('customer_id'); ?></label>
                                        	<div class="controls">
                                        		
                                        		<?php
												if ($customers == FALSE) {

													$customers = array(''=>'No Customers Exist');
												}
												 
												if($order){$value =$order['customer_id'];}else{ $value=set_value(); }
												echo form_dropdown('customer_id',$customers,$value);
                                        		?>
                                        		<?php echo form_error('customer_name'); ?>  &nbsp;
                                        		<a href="" class="btn btn-success btn-medium"><?php _l('new_customer'); ?></a>
                                        	</div>
                                    	</div>  
                                    	
<div class="control-group">
										<div class="controls">
										
										</div>
									</div>

                                    	<div class="control-group">
                                        	<label class="control-label" for="order_date"><?php _l('order_date'); ?></label>
                                        	<div class="controls">
                                        		<?php echo form_error('order_date'); ?>
                                        			<div class="input-append no-margin">
												<input  name="order_date" class="datepicker input-small" value="<?php if($order){echo $order['order_date'];}else{echo set_value('order_date');}?>" placeholder="<?php echo date('Y-m-d')?>" type="text"><span class="add-on"><i class="icon-calendar"></i></span>
											</div>
                                        		 
                                        		</div>
                                        	</div>
                               	</td>
                               	<td>
                               		 	
                                        	<div class="control-group">
                                        	<label class="control-label" for="delivery_date"><?php _l('delivery_date'); ?></label>
                                        	<div class="controls">
                                        		<?php echo form_error('delivery_date'); ?>
                                        		
                                        		<div class="input-append no-margin">
												<input  name="delivery_date" class="datepicker input-small"  placeholder="<?php echo date('Y-m-d')?>" value="<?php if($order){ echo $order['delivery_date'];}else{ echo set_value('delivery_date');}?>" type="text"><span class="add-on"><i class="icon-calendar"></i></span>
											</div>
                                        		</div>
                                        	</div>
                                        	<div class="control-group">
                                        	<label class="control-label" for="assigned_to"><?php _l('assigned_to'); ?></label>
                                        	<div class="controls">
                                        		<?php
                                         
                                        				if($employees == FALSE){
                                        					$employees = 'No Sales Team';
                                        				} 
														if($order){
															$emp = $order['employee_id'];
														}else{
															$emp = FALSE;
														}
														echo form_dropdown('assigned_to',$employees,$emp,'required');
                                        		?>
                                        		 
                                        		</div>
                                        	</div>
                                        	  		
                               	</td>
                               	
                               </tr> </table>
  <fieldset>
<legend><?php _l('order_details');?></legend>
      
<div class="control-group">
                                        	<label class="control-label" for="order_no" ><?php _l('order_details'); ?></label>
                                        	<div class="controls"><?php echo form_error('product_id[]'); ?>
                                        		<table class="datatable table table-striped table-bordered table-hover"><tr><th></th><th><?php  _l('product_name')?></th> <!--<th><?php  _l('order_date')?></th>--> <th><?php  _l('order_qty')?></th> <th><?php  _l('sale_price')?></th><th><?php _l('order_value');?></th></tr></table>

<TABLE id="dataTable" class="datatable table table-striped table-bordered table-hover" >
   <?php 
   
   //get products
   					if($products ==FALSE){  $products = array(''=>'No Products'); } 
            		if($order==FALSE){$prod = set_value('product_id');}
   
     	if($order_details) {
 
	foreach ($order_details as $order):
		  
		 	$prod = $order['product_id'];  
		?>
		
		<TR>
            <TD><INPUT type="hidden" name="order_id" value="<?php echo $order['order_id']; ?>"/></TD>
            <TD><?php echo form_dropdown('product_id[]', $products, $prod); ?>
            </TD>
            
            <TD><INPUT type="number" name="order_qty[]"class="input-medium"  value="<?php echo $order['order_qty']; ?>" placeholder="Quantity" required="" /></TD>

            <TD><INPUT type="number" name="price[]"class="input-medium"  placeholder="sale Price" disabled=""  value="<?php echo $order['sale_price']; ?>"required="" /></TD>
             <TD><input  type="text"  name=""class="input-medium" value=" " placeholder="order value" disabled="" /></TD>	

            
        </TR>
		
<?php 	endforeach;
			}
		?>
    	 
     
        <TR>
            <TD><INPUT type="checkbox" name="chk"/></TD>
            <TD><?php

			if ($products == FALSE) {
				  $products = array('' => 'No Products');
			}
			 
			 $prod = set_value('product_id');
			 
			$id = 'id="s_t_product"';
			echo form_dropdown('product_id[]', $products, $prod,$id);
            		?>
            </TD>
         <!--  	<TD><input  type="date"  name="order_date[]"class="input-medium" placeholder="order date" required="" /></TD>-->
            <TD><INPUT type="number" name="order_qty[]"class="input-medium" id="s_t_qty" placeholder="Quantity" required="" /></TD>
            <TD><input  type="text"  name="sale"class="input-medium" value=" " placeholder="Sale price" disabled="" /></TD>
            <TD><input  type="text"  name="order"class="input-medium" value=" " placeholder="ordervalue" disabled="" /></TD> 
        </TR>
     
      </TABLE> 
  
       <span class="" style="float: right;padding-right: 5%">
 <input type="button" class="btn btn-medium btn-info" value="Add Row" onclick="addRow('dataTable')" />
 <input type="button" class="btn btn-medium btn-danger" value="Delete Row" onclick="deleteRow('dataTable')" />
 </span>   
 
    </div>
</div>
						</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('orders'); ?>" class="btn btn-alt" data-dismiss="modal">Back</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>
                        </div>                        	
                        </div>	
                        </section>
					 