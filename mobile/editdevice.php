<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
<link rel="stylesheet" href="bx_styles.css" type="text/css" />
<title>footprint</title>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function(){
    
  });
</script>
</head>

<body>
<?php
session_start();
// contact to database
include ("connection.php");
include('common.php');

$ID=0;
if(isset($_GET['id'])){
$ID=$_GET['id'];
$result = mysql_query("SELECT * FROM phones WHERE phoneid = '$ID'") or die ("Error in query");
$row=mysql_fetch_array($result);
}
?>
    <div class="center_header_page">
      <div class="content" >
        
            <div class="content_first content_sec_heading">
                
                

         
        <div class="content_thirdlog"><!--content_third-->
         	<div class="content_thirdlogpopup_left">
         	  
                <div class="content_third_left_second editdevice_sp_page">
                	<h3>Edit Device</h3>
                    <div class="clear"></div>
					<form id="edit_device_form" class="edit_device_form_sp" action="addeditdevice.php?XDEBUG_SESSION_START=dk" method="post" >
                    <div class="content_third_left_contactlogpro editdevice_s">
                    	<div class="content_third_left_contactlogpro_input" style="display:none;">
                    	<p>Device IMEI:</p><br /><input type="text" value="<?php echo $row['phone_imei']?>" name="txt_imei" id="txt_imei" />
                        </div>
                        
                        <div class="content_third_left_contactlogpro_input">
                        <p>Device Name:<span style="font-size:10px">(User Defined)</span><br /></p><input type="text" value="<?php echo $row['device_name']?>" name="txt_name" id="txt_name" />
                        </div>
                        <div class="clear"></div>
					  <div class="content_third_left_contactlogpro_select">
					  <p>Tracking Interval:</p><br /> 
                    	<select name="checking_interval" id="checking_interval" style="color:#000000; font-size:16px">
                          <option selected value="-1">Please Select</option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="5">5</option>
                          <option value="10">10</option>
                          <option value="15">15</option>
						  <option value="20">20</option>
						  <option value="30">30</option>
						  <option value="60">60</option>
						  <option value="0">Stop</option>
                        </select>
                    	
					  </div>
						<div class="content_third_left_contactlogpro_input">
                    	<p>Mobile Number:</p><br /><input type="text" value="<?php echo $row['mobile_no']?>" name="txt_mobile"  id="txt_mobile"/>
                        </div>
						<div class="content_third_left_contactlogpro_input">
                    	<p>Name of Holder:</p><br /><input type="text" value="<?php echo $row['holder_name']?>" name="txt_holder_name" id="txt_holder_name"/>
						<input type="hidden" value="<?php echo $_SESSION['userid']?>" name="txt_userid" />
						<input type="hidden" value="<?php echo $ID?>" name="txt_phoneid" />
						<input type="hidden" value="edit" name="addedit" />		
                        </div>
						                        
                        <div class="content_third_left_buttondevice">
                        	<a href="javascript:void(0);" id="editsubmit" onclick="checkSave();">SUBMIT</a>
                        </div>
                    </div>
                    </form>
              </div>
         	</div>            
         	
			
         </div><!--content_third-->
            </div>
      </div>
    
</div>
<script type="text/javascript">

$(document).ready(function(){
	var s = $("select[name='checking_interval']");
	var v = <?php echo $row['checking_interval']?>;
	s.val(v);
});

  function checkSave(){
		
		  var imei = $("#txt_imei");
		  var name = $("#txt_name");
		  var interval = $("#checking_interval");
			
		 if(imei.val().length <= 0){
			alert("IMEI number should not blank");
			return false;
		 } else if(name.val().length <= 0){
			alert("Device Name should not blank");
			return false;
		 }
		 else if(interval.val() < 0){
			alert("Interval should be selected");
			return false;
		 }
		 else{
			document.getElementById('edit_device_form').submit();
		  }
		  
	  }
</script>
</body>
</html>
