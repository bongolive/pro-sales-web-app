<?php

/**

 * This file carries the main values for language in English

 */

$lang['device_pg_title'] = 'Device';

$lang['track_pg_title'] = 'Track';

$lang['task_pg_title'] = 'Tasks';

$lang['reports_pg_title'] = 'Reports';

$lang['customers_pg_title'] = 'Customers';

$lang['profile_pg_title'] = 'Profile';

$lang['boundary_pg_title'] = 'Boundaries';

$lang['support_pg_title'] = 'Support';

$lang['login_pg_title'] = 'Login';

$lang['welcome_pg_title'] = 'Welcome';
 

// Table fields

$lang['device_tb_name'] = 'Devices';

$lang['boundaries_tb_name'] = 'Boundaries';

$lang['reports_tb_name'] = 'Reports';

$lang['customers_tb_name'] = 'Customers';

$lang['tasks_tb_name'] = 'Tasks';

$lang['track_tb_name'] = 'Track';

$lang['user_tb_name'] = 'Users';

$lang['permission_tb_name'] = 'Permissions';

// Table Field names

$lang['device_name'] = 'Device Name';

$lang['subject'] = 'Subject';

$lang['task'] = 'Task';

$lang['status'] = 'Status';

$lang['date'] = 'Date';

$lang['action'] = 'Actions';

$lang['device_imei'] = 'Device IMEI';

$lang['track_intv'] = 'Tracking Interval';

$lang['mobile_nmber'] = 'Mobile Number';

$lang['holder_name'] = 'Name of Holder';

$lang['alert_name'] = 'Alert Name';

$lang['devices'] = 'Device(s)';

$lang['valid_from'] = 'Valid From';

$lang['valid_to'] = 'Valid To';

$lang['boundary'] = 'Boundary';

$lang['report'] = 'Report';

$lang['comments'] = 'Comments';

$lang['location'] = 'Location';

$lang['customer_name'] = 'Customer Name';
$lang['customers_list'] = 'Customers List';
$lang['assign_customers'] = 'Assign Customers';

$lang['mobile_number'] = 'Mobile Number';

$lang['email'] = 'Email';

$lang['address'] = 'Location';
$lang['city'] = 'City';
$lang['street'] = 'Street';
$lang['category'] = 'Category';

$lang['valid_till'] = 'Valid Till';

$lang['user_type'] = 'User Type';

$lang['permission_name'] = 'Permission Name';

$lang['is_deleted'] = 'Is Deleted';

// Tabs

$lang['device'] = 'Device';

$lang['support'] = 'Support';

$lang['track'] = 'Track';

$lang['reports'] = 'Reports';

$lang['customers'] = 'Customers';

$lang['boundaries'] = 'Boundaries';

$lang['tasks'] = 'Tasks';

$lang['users'] = 'Users';

$lang['permissions'] = 'Permissions';

// add Device

$lang['email_reports'] = 'Email Reports';

$lang['enable_email_reports'] = 'Enable Email Reports';

$lang['add_new_device'] = 'Add New Device';

// Add Task

$lang['add_task'] = 'Add Task';

$lang['new_customer'] = 'Add Customer';

$lang['add_new_permission'] = 'New Permission';

$lang['add_new_user'] = 'Add User';

$lang['new'] = 'New';

$lang['in_progress'] = 'In Progress';

$lang['done'] = 'Done';

$lang['update_permission'] = 'Update Permission';

$lang['update_client_permission'] = 'Update Client Permissions';

$lang['client_permissions'] = 'Client Permissions';

// add Boundary

$lang['add_boundary'] = 'Add Boundary';

$lang['edit_boundary'] = 'Edit Boundary';

$lang['step_one'] = 'Step 1 (Defining Boundary)';

// Edit Device

$lang['edit_device'] = 'Edit this Device';

$lang['update_customer'] = 'Update Customer Record';

$lang['update_user'] = 'Update User';

// Table reveal modal

$lang['confirm_deletion'] = 'Confirm Deletion';

$lang['are_you_sure'] = 'Are you sure';

//Tracking

$lang['start_date'] = 'Start Date';

$lang['end_date'] = 'End Date';

$lang['search'] = 'Search';

//Downloading forms

$lang['txt_task'] = 'Tasks';

$lang['all_task'] = 'All Tasks';

$lang['all_dates'] = 'All Dates';

$lang['all_report'] = 'All Reports';

$lang['all_subject'] = 'All Subjects';

$lang['download_task'] = 'Download Tasks';

$lang['download_report'] = 'Download Report';

//customers form

$lang['stocks_tb_name'] = 'Device Stocks';
$lang['product_name'] = 'Product Name';
$lang['items_received'] = 'Received';
$lang['items_issued'] = 'Issued';
$lang['items_returned'] = 'Returned';
$lang['returned_items'] = 'Returned';
$lang['stock_date'] = 'Stock Date';
$lang['add_new_stock'] = 'Assign Stock To Devices';
$lang['products_tb_name'] = 'Products';
$lang['description'] = 'Description';
$lang['items_on_hand'] = 'Items on Hand';
$lang['add_new_product'] = 'Add New Product';
$lang['sales_tb_name'] = 'Product Sales';
$lang['client_name'] = 'Name';
$lang['sales_datetime'] = 'Sales Date';
$lang['quantity'] = 'Quantity Per Pack';
$lang['price'] = 'Price';
$lang['clients_tb_name'] = 'Customers';
$lang['phone'] = 'Phone';
$lang['shop_name'] = 'Shop Name';
$lang['add_new_client'] = 'Add New Customer';
$lang['returned_stocks_tb_name'] = 'Returned Stock';

$lang['update_product'] = 'Update Product';
$lang['update_device_stock'] = 'Updating Device Stock';
$lang['return_device_stock'] = 'Returned Device Stock';
$lang['update_client'] = 'Updating Customer Information';

$lang['products'] = 'Products';
$lang['returns'] = 'Return';
$lang['sales'] = 'Sales';
$lang['stock'] = 'Stock';

$lang['add_product_stock'] = 'Add Product Stock';
$lang['items_to_add'] = 'Items To Add';

$lang[''] = 'Account NO';
$lang['company_name'] = 'Company Name';
$lang['no_property'] = 'Limit';
$lang['firstname'] = 'Firstname';
$lang['lastname'] = 'Lastname';
$lang['tin_no'] = 'Tax Indentification Number ';
$lang['area'] = 'Area';
$lang['vat_no'] = 'VAT Reg No';
$lang['street'] = 'Street';
$lang['city'] = 'City';
$lang['created_by'] = 'Created By';
$lang['expire_date'] = 'Expire Date';

$lang['cp_title'] = 'Title';
$lang['service_type'] = 'Service Type';
$lang['service_category'] = 'service Category';
$lang['device_imei'] = 'Device Imei';
$lang['mobile'] = 'Mobile';
$lang['email'] = 'Email';

$lang['lat'] = 'Latitude';
$lang['longtd'] = 'Longtude';

$lang['created_on'] = 'Created On';
$lang['created_by'] = 'Created By';
$lang['last_update'] = 'Last Update';
$lang['status'] = 'Status';
$lang['is_synced'] = 'Sync Status';
$lang['mob_customer_id']='Customer_id';
$lang['customer_id'] = 'Customer_id';
$lang['contact_person'] = 'Contact Person';

$lang['device_id'] = 'Device No';
 
$lang['device_descr'] = 'Descriptions';
$lang['assigned_to'] = 'Assigned To';

$lang['assigned_on'] = 'Assigned';

$lang['device_status'] = 'Status';
$lang['device_mobile']="Device Mobile";

$lang['id'] = 'No';
 
$lang['gender'] = 'Gender';
$lang['emp_role'] = 'Role';
$lang['username'] = 'Username';
$lang['password'] = 'Password';
$lang['doe'] = 'Date of Birth';

$lang['emp_status']='Status';
 
$lang['module_name'] = 'Module Name';
$lang['created'] = 'Created Date';
$lang['updated'] = 'Last Modify';
$lang['module_status']='Status';
 

$lang['package_id'] = 'No';
$lang['package_name'] = 'Package Name ';
$lang['package_descr'] = $lang['descriptions']=  'Descriptions';
$lang['status'] = 'Status';
 
$lang['last_update']='Last Modify';

$lang['mode_id'] = 'No';
$lang['mode_title'] = 'Title';
$lang['mode_symbol'] = 'Display Name';
$lang['mode_payment_no'] = 'Account Number';
$lang['status'] = 'Status';


$lang['descr/specs'] = 'No';
$lang['user_id'] = 'User No';
$lang['permissions'] = 'Permission';
 
$lang['product_id'] = 'Product No';
$lang['product_name'] = 'Product Name';
 

$lang['items_on_hand'] = 'Available Stock';
$lang['category'] = 'Category';
$lang['product_status'] = 'Status';
$lang['package_type'] = 'Package Type';
$lang['package_units'] = 'Quantity Per Pack';
$lang['unit_sale_price'] = $lang['sale_price'] = 'Sale Price (Per Unit)';
$lang['sku_product_no'] = 'Product SKU';
$lang['tax_rate'] = 'VAT';
$lang['container_type'] = 'Container Type';
$lang['unit_measure'] = 'Unit Measure';

$lang['updated_on'] = 'Last Update';
$lang['updated_date'] = 'Last Update';
$lang['remained_qty'] = 'Current Remained Stock';
$lang['updated_by']='Updates By';

$lang['cat_id'] = 'No';
$lang['category_name'] = 'Category Title';
$lang['category_descr'] = 'Descriptions';
$lang['category_status'] = 'Status';


$lang['modify_date'] = 'Last Modify';


$lang['order_id'] = 'No';
$lang['phone_imei'] = 'Imei';
$lang['employee'] = $lang['employee_id'] = 'Employee';
$lang['order_date'] = 'Order Date';
$lang['customer_id'] = 'Customer';
$lang['sales_total'] = 'Total Sales';
$lang['total_tax'] = 'Total Tax';


$lang['payment_status'] = 'Receipt';
$lang['payment_mode'] = 'Payment Method';
$lang['payments_mode_title'] = 'Payment Mode';
$lang['receipt_no'] = 'Receipti No';
$lang['paid_amount'] = 'Amount Paid';
$lang['order_status'] = 'Order Status';
$lang['delivery_date'] = 'Delivery Date';


$lang['details_id'] = 'No';
$lang['order_id'] = 'Order NO';
$lang['edit_order'] = 'Update Order';
$lang['phone_imei'] = 'Imei';
$lang['product_id'] = 'Product No';
$lang['order_qty'] = 'Order QTY (Units)';
 
$lang['sale_tax'] = 'Sale Tax';


$lang['stock_id'] = 'Stock No';
$lang['employee_id'] = 'Empoloyee No';
$lang['device_imei'] = 'Imei';
$lang['stock_date'] = 'Stock Date';
$lang['stock_qty'] = 'Stock QTY';
$lang['stock_sales'] = 'Stock Sale';
$lang['stock_balance'] = 'Stock Balance';
$lang['is_synced'] = 'Sync Status';


$lang['created_date'] = 'Created On';
 

$lang['setting_id'] = 'No';

$lang['include_tax'] = 'Sale Price Inclusive of Tax ';
$lang['vat'] = 'VAT REG NO';
$lang['enable_discounts'] = 'Enable Sales Team to Give Discount';
$lang['allow_post_payment'] = 'Allow Post Payment for Sales';
$lang['print_receipts'] = 'Allow Receipt Printing from Device ';
$lang['use_fixed_prices'] = 'Allow Sales Team to Change Price';
$lang['discount_limit'] = 'Discount Limit';
$lang['default_currency']='Default Currency';
 

$lang['stock_date'] = 'Stock Date';
$lang['stock_qty'] = 'Stock QTY';
$lang['last_stock'] = 'Stock In Hand';
$lang['unit_purchase_price'] = $lang['purchase_price'] =  'Purchase Price (Per Unit)';
$lang['supplier'] = 'Supplier';
$lang['order_no'] = 'Purchase Order No';

 
$lang['supplier_id'] = 'Supplier No';
$lang['supplier_name'] = 'Supplier Name';
$lang['contact_person'] = 'Contact person';
$lang['sup_mobile'] = 'Mobile';
$lang['sup_email'] = 'Email';
$lang['sup_status']='Status';

$lang['trans_id'] = 'Transaction No';
$lang['trans_date'] = 'Transaction Date';
$lang['order_id'] = 'Order No';
$lang['trans_amount'] = 'Transaction Amount';
$lang['old_payments'] = 'Payments Made';
$lang['trans_pay_mode'] = 'Payment Settings';


$lang['last_updated'] = 'Last Modify';
$lang['trans_status']='Status';
 
$lang['role'] = 'Role';
$lang['permissions'] = 'Permission';
$lang['username'] = 'Username';
$lang['password'] = 'Password';


$lang['employee_id'] = 'Employee NO';


$lang['permission_name'] = 'Permission';
$lang['userid']='User No';
$lang['wh_id'] = 'No';
$lang['wh_title'] = 'Ware House Name';
$lang['wh_supervisor'] = 'Supervisor';
$lang['business_name']='Business Name';

$lang['new_order'] = 'New Order';
$lang['back_to_customers'] = 'Customers List';
$lang['customer_details'] = 'Customer Details';
$lang['order_details'] = 'Order Details';
$lang['product_category'] = 'Category';
$lang['new_product'] = 'New Product';
$lang['stock_info'] = 'Stock Status';

$lang['warehouse_name'] = 'Ware House';
$lang['latest_puchases_orders'] = 'Latest Purchases';
$lang['latest_sales_orders'] = 'Latest Orders';
$lang['puchase_date'] = 'Purchase Date';
$lang['grand_total'] = 'Grand Total';
$lang['total_sales'] = 'Total Sales';
$lang['sales_inv'] = 'On Sale QTY';
$lang['store_inv'] = 'Store QTY';
$lang['total_value'] = 'Total';
$lang['products_stock_history'] =$lang['all_products_stock_history'] = 'Procurement';
$lang['add_stock'] = 'Add Stock';

$lang['descr/specs']  = "Descriptions/Specifications";
$lang['stock_details'] = 'Details';
$lang['stock_history'] = 'Procurement';
$lang['product_categories'] = 'Product Categories';
$lang['product_packages'] = 'Product Packages';
$lang['all_products_stock_history'] = ' ';
$lang['orders'] = 'Orders';
$lang['puchases'] = 'Purchases List';
$lang['sales_teams'] = 'Sales Team';
$lang['assign_stock'] = 'Assign Team Stock';
$lang['order_tb_name'] = 'Orders List';
$lang['payments_history'] = 'Payments History';
$lang['all_sales_team_stock'] = 'Sales Team Stock History';
$lang['employee_name'] = 'Sales Person';
$lang['stock_value'] = 'Stock Value';
$lang['sales_team_history'] = 'Sales Team History';
$lang['balance_value'] = 'Balance';
$lang['add_stock_to_team'] =$lang['assign_team']= 'Add Teams Stock';
$lang['back'] = 'Back';
$lang['payments'] = 'Payments';
$lang['all_customers'] = 'Customers';
$lang['latest_customer_orders'] = 'Latest Transactions';
$lang['order_value'] = 'Total Charges';
$lang['all_employees'] = $lang['employees']= 'Employees';
$lang['new_employee'] = 'New Employee';
$lang['employee_details'] = 'Employee Details';
$lang['account'] = 'Accounts';
$lang['settings'] = 'Settings';
$lang['all_devices'] = 'Devices';
$lang['device_details'] = 'Device Details';
$lang['account_settings'] = 'Account Settings';
$lang['account_profile'] = 'Account Profile';
$lang['user_details'] = 'Log In Details';
$lang['conf_password'] = 'Confirm Password';
$lang['payments_summary'] = 'Payments Summary';
$lang['transactions_history'] = 'Transactions History';
$lang['payment_details'] = 'Payment Details';
$lang['sales_team'] = 'Sales Team';
$lang['sales_team_details'] = 'Sales Teams';
$lang['stock_tb_name'] = 'Stock List';
$lang['add_new_category'] = 'New Products Category';
$lang['all_products_categories'] = $lang['products_categories']  ='Products Categories';
$lang['all_products_packages'] =$lang['products_packages'] ='Products Packaging';
$lang['add_new_package'] = 'New Product Package';
$lang['re_order_value'] = 'Re-Order';
$lang['weight_size'] = 'Weight';
$lang['weight_units'] = 'Units';
$lang['new_product'] = 'New Product';
$lang['product'] = 'Product';
$lang['account_id'] = 'Account';
$lang['all_payment_mode'] = 'Payment Modes';
$lang['add_new_mode'] = 'New Mode';
$lang['mode_name'] = 'Title';
 
$lang['payments_mode'] = 'Payment Settings';
$lang['adding_stock'] = 'Stock Assignment';
$lang['assign_customer'] = 'Customers Assignment';
$lang['add_sales_team_customers'] = 'Assign Customers';
$lang['assign'] = 'Assign';
$lang['staff'] = 'Staff';
$lang['week_sales'] = 'Week Sales';
$lang['re-order'] = 'Re-Order';
$lang['outstanding'] = 'Outstanding';
$lang['pending_orders'] = 'Pending Orders';
$lang['products_status'] = 'Products Sales Status';
$lang['high_sale'] = 'First 20';
$lang['low_sale'] = 'Last 20';
$lang['map_location'] = 'Longitude/Latitude';
$lang['entry_mode'] = 'Entry Mode';
$lang['stock_settings'] = 'Stock Settings';
$lang['dashboard'] = 'Dashboard';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['unit_size'] = 'Unit Size';
$lang['add_new_container'] = 'Add New Container';
$lang['container_name'] = 'Container Name';
$lang['product_containers'] = 'Product Containers';
$lang['all_products_containers'] = 'Products Containers';
$lang['container_descr'] = 'Container Descr';
$lang['new_products'] = 'Product Details';
$lang['orders_history'] = 'Orders History';
$lang['view'] = 'View';
$lang['assign'] = 'Assign';
$lang['starting_quantity'] = 'Starting Quantity';
$lang['assign_products'] = 'Assign Products';
$lang['assign_customer'] = 'Assign Customer';
$lang['remove_product'] = 'Remove Product';
$lang['add_product'] = 'Add Product';
$lang['back'] = 'Back';
$lang['all_accounts'] = 'Accounts List';
$lang['accounts'] = 'Accounts';
$lang['new_account'] = 'New Account';
$lang['account_details'] = 'Account Details';
$lang['user_accounts'] = 'User Accounts';
$lang['conf_pass'] = 'Confirm Password';
$lang['logging_details'] = 'Logging Details';
$lang['sub_account_details'] = 'Sub Accounts';


$lang['no_products_available'] = 'No Products Available';
$lang['sales_summary'] = 'Sales Summary';
$lang['business_name'] = 'Business Name';
$lang['dates'] = 'Dates';
$lang['stock_assignment'] = 'Stock History';
$lang['view_sales'] = 'Sales';
$lang['sales_team_stock_history'] = 'Sales Team Stock History';
$lang['batch_no'] = 'Batch No';
$lang['team_details'] = 'Sales Team';
$lang['order_number'] = 'Order Number';
$lang['products_containers'] = 'Products Containers';
$lang['balance'] = 'Balance';
$lang['paid'] = 'Paid';
$lang['pay'] = 'Pay';
$lang['view_order'] = 'View Order';
$lang['processed'] = $lang['closed']=  'Closed';
$lang['canceled'] = 'Canceled';
$lang['pending'] = 'Pending';
 
$lang['total'] = 'Total';
$lang['team_commission'] = 'Team Commission';
$lang['commission_rate'] = 'Sales Team Commission Rate';
$lang['total_stock'] = 'Total Stock';
$lang['stock_balance'] = 'Stock Balance';
$lang['daily_sales'] = 'Today Sales';
$lang['new_order'] = 'New Order';
$lang['cancelled'] = 'Cancelled';
$lang['paid'] = 'Paid';
$lang['pending'] = 'Pending';
$lang['sub_total'] = 'Sub Total';
$lang['total_sale'] = 'Total Sale';
$lang['team_orders'] = 'Team Orders';
$lang['back_to_orders'] = 'All Orders';
$lang['products_batches'] = 'Assigned Product Batches';
$lang['view_all_products'] = 'All Team Products';
$lang['stock_sold'] = 'Stock Sales';
$lang['all_assigned_product'] = 'All Team Products';

//changes nasibu
$lang['receipt_notprinted'] = 'Not Issued';
$lang['receipt_printed'] = 'Issued';
$lang['trans_uniq_id'] = 'Transaction ID' ;
$lang['trans_uniq_id_null'] = 'Not Available';
$lang['allow_receipt_printing_with_outstanding'] = 'Allow Printing Receipts with Debt';
$lang['payment_method'] = 'Payment Method';
$lang['commission'] = 'Commission Rate';
$lang['commission_value'] = 'Commission';
$lang['tracking_menu'] = 'Tracking';
$lang['tracking_interval'] = 'Tracking Time Interval';
$lang['order_location'] = 'Order Location';
$lang['payment_error_msg'] = 'You paid more than the outstanding amount. The Payment is Rejected';
$lang['payment_success_msg'] = 'You have successfully paid you outstanding amount';
$lang['choose_warehouse'] = 'Choose Warehouse';
$lang['add_new_warehouse'] = 'Add New Warehouse';
$lang['product_warehouse'] = 'Product Warehouse';
$lang['warehouse_stock'] = 'All Stock In This Warehouse';
$lang['wh_title'] = 'Warehouse Name';
$lang['wh_supervisor'] = 'Warehouse Supervisor';
$lang['assigned_stock'] = 'Assigned Stock';
$lang['current_assigned_stock'] = 'Current Assigned Stock';
$lang['expiry_date'] = 'Expiry Date';
$lang['current_stock'] = 'Current Stock Information';
$lang['sales_person'] = 'Sales Person';
$lang['delivery_order_no'] = 'Delivery Order Number';
$lang['purchase_order_no'] = 'Purchase Order Number';
$lang['reset_device_sync_status'] = 'Reset Device Sync Status';
$lang['no_employee'] = 'No Employee Assigned';
$lang['acc_type'] = 'Account Type';
$lang['acc_type_normal'] = 'Normal';
$lang['acc_type_premium'] = 'Premium';
$lang['imei'] = 'IMEI';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
