<?php
class Stocks_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	
	public function getContent($table,$cond)
	{
		$query = $this->db->get_where($table,$cond);
		return $query->result_array();
	}
	
	public function addContent($table,$data)
	{
		return $this->db->insert($table, $data);
	}
	
	public function update($table,$data,$cond)
	{
		$this->db->where($cond);
		return $this->db->update($table, $data);
	}
	
	public function delete($table,$cond)
	{
		return $this->db->delete($table, $cond);
	}
	
	public function getUserDevices($userid){
	
	}
	
	public function get_phones($where)
	{ 
		$this->db->select('*');		 
		$this->db->from('phones_users'); 
	  	$this->db->join('phones','phones.phoneid=phones_users.phoneid');
		$this->db->where($where);
		$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
	}
	
	
	public function getProductStock($cond)
	{
		$query = $this->db->query("SELECT product_stocks.*,phones.device_name FROM `product_stocks` LEFT JOIN phones ON product_stocks.phone_imei = phones.phone_imei ".$cond);
		return $query->result_array();
	}
	
	
	public function getProductSales($cond)
	{
		$query = $this->db->query("SELECT sales.*,phones.device_name FROM `sales` LEFT JOIN phones ON sales.phone_imei = phones.phone_imei ".$cond);
		return $query->result_array();
	}
	
	public function getReturnedStock($cond)
	{
		$query = $this->db->query("SELECT returned_stocks.*,phones.device_name FROM `returned_stocks` LEFT JOIN phones ON returned_stocks.phone_imei = phones.phone_imei ".$cond);
		return $query->result_array();
	}
	
	public function getProductList($cond)
	{
		$query = $this->db->query("select * from products ".$cond);
		return $query->result_array();
	}
	
	public function getClientList($cond)
	{
		$query = $this->db->query("select * from stock_clients ".$cond);
		return $query->result_array();
	}
	
}
