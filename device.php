<?php 
session_start();
session_register();
include('common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
<link rel="stylesheet" href="bx_styles.css" type="text/css" />
<title>footprint</title>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script src="js/jquery.bxSlider.js" type="text/javascript"></script>
<script src="js/jquery.bxSlider.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<link href="facebox.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="js/facebox.js" type="text/javascript"></script> 

<script type="text/javascript">
  $(document).ready(function(){
  
  $('a[rel*=facebox]').facebox();
 
    $(".deleteitem").click(function(){
		var x=window.confirm("Are you sure want to delete this device?")
		if (x){			
			var parent = $(this).closest('TR');
			var id = parent.attr('id');
			var uid = parent.attr('uid');
			$.ajax({
				type: 'POST',
				data: 'id=' +id+"&uid="+uid+"&type=device",
				url: 'deleteitem.php',
				success: function(msg){
					$('#'+id).remove();
				}
				});
		}
	});

	 $('.slider').bxSlider({
    auto: true,
    pager: true
  });
  });
</script>


<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
var map;
var openedInfoWindow = null;
var allMarkers = [];

var userId = <?php echo $_SESSION['userid']; ?>;
var locations = [];

function showLastLocations(){
	locations.length = 0;
	var xmlDocument ;
	// show some processing icon
	document.getElementById("div_map_processing").style.display = 'block';
	
	jQuery.ajax({
	url: "getLastLocations.php?userId="+userId,//sending ajax request to our php file
	//cache: false,
	//processData: false,
	data: xmlDocument,//storing the data in xmlDocument cariable
	success: function(xmlDocument) {
		// retreving the data by applying loop on tag "segment"
		jQuery(xmlDocument).find("location").each(function()
		{
			var loc = [];
			loc.push( jQuery(this).find("phoneid").text());
			loc.push( jQuery(this).find("imei").text());
			loc.push( jQuery(this).find("devicename").text());
			loc.push( jQuery(this).find("mobileno").text());
			loc.push( jQuery(this).find("holdername").text());
			loc.push( jQuery(this).find("latitude").text());
			loc.push( jQuery(this).find("longitude").text());
			loc.push( jQuery(this).find("localdate").text());
			
			//if(jQuery(this).find("latitude").text() == ""){
			//	alert(jQuery(this).find("imei").text());
			//}
			
			locations.push(loc);
		});
		
		
		//alert("main:"+locations.length);
		//alert("detail:"+locations[0].length);
		removeAllMarkers();
		for(var i = 0; i < locations.length; i++){
			if(locations[i][5] != "" && locations[i][6] != "" && locations[i][7] != ""){
				var content = "<strong>IMEI : </strong>"+locations[i][1]+"<br>";
				content += "<strong>Device Name : </strong>"+locations[i][2]+"<br>";
				content += "<strong>Mobile Number : </strong>"+locations[i][3]+"<br>";
				content += "<strong>Holder Name : </strong>"+locations[i][4]+"<br>";
				content += "<strong>Time : </strong>"+locations[i][7]+"<br>";
				
				var latlngpoint = new google.maps.LatLng( locations[i][5], locations[i][6]);
				
				setMarker(map, latlngpoint, content);
			}
		}
		setBoundsToAllMarkers();
		
		document.getElementById("div_map_processing").style.display = 'none';
	}
  });
}

function initializeMap() {
//document.getElementById("ajaxLoader").style.display = 'block';
  
var centerPoint = new google.maps.LatLng(33.773654,-84.37324);

var myOptions = {
	zoom: 2,
	center: centerPoint,
	mapTypeId: google.maps.MapTypeId.ROADMAP
}

map = new google.maps.Map(document.getElementById("div_loc_map"), myOptions);

//
showLastLocations();
//
}

function setMarker(map, latlng, content){
	
	var infowindow = new google.maps.InfoWindow({
		content: content
	});
	
	var marker = new google.maps.Marker({
		position: latlng,
		map: map
	});
	
	allMarkers.push(marker);
	
	/*google.maps.event.addListener(marker, 'click', function() {
		if (openedInfoWindow != null) {
			openedInfoWindow.close();
		}
		infowindow.open(map,marker);
		
		openedInfoWindow = infowindow;
		google.maps.event.addListener(infowindow, 'closeclick', function() {
			openedInfoWindow = null;
		});
	});*/
	
	google.maps.event.addListener(marker, 'mouseover', function() {
		if (openedInfoWindow != null) {
			openedInfoWindow.close();
		}
		infowindow.open(map,marker);
		
		openedInfoWindow = infowindow;
		google.maps.event.addListener(infowindow, 'closeclick', function() {
			openedInfoWindow = null;
		});
	});
	
	
}

function removeAllMarkers(){
	/*if(allMarkers){
		for(i in allMarkers){
				allMarkers[i].setMap(null);
		}
		allMarkers.length = 0;
	}*/
	//alert("length : "+allMarkers.length);
	for(var i = 0; i < allMarkers.length; i ++){
		//alert(allMarkers[i]);
		allMarkers[i].setMap(null);
	}
	allMarkers.length = 0;
}

function setBoundsToAllMarkers(){
	var bounds = new google.maps.LatLngBounds();
	
	for (var index in allMarkers) {

		var latlng = allMarkers[index].getPosition();

		bounds.extend(latlng);
	}
	map.fitBounds(bounds);	
}




</script>

</head>

<body onload="initializeMap();">
<?php 
//session_start();
//include('common.php');
?>
	<div class="body_class">
    	<div class="center_header_page">
        	<div class="header-left">
            	<a href="index.php" ><img src="images/logo3.png" alt=""  /></a>
            </div><div class="header-right">
            	<span style="font-family:Arial; font-size:14px; color:#FFFFFF; margin-top:8px; float:left">Welcome <?php echo $_SESSION['username'];?> <br /><a href="logout.php" style="color:#FFFFFF">Logout</a></span>
            </div>
            
        </div>
	</div>
	<div class="center_header_pageinner">
    	<div class="menu_wrapperinner">
        	
            <div class="menu_firstinner">
            	
                <div class="active menu_contentinner">
                	<a href="" >Device</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="track.php" >Track</a>
                </div>
            </div>
			  <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="task.php" >Tasks</a>
                </div>
            </div>
			  <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="report.php" >Reports</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                		<a href="boundary.php" >Boundaries</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="profile.php" >Profile</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                		<a href="support.php" >Support</a>
                </div>
            </div>
          
            <div class="clear"></div>
			</div>
        </div>
    
    </div>
    <div class="line">
    </div>
    <div class="center_header_page">
      <div class="content" >
      		
            
            <div id="div_map_wraper" style="margin:0 auto; width:800px; height:400px; position:relative;" >
            
                <div id="div_loc_map" class="content_first" style="width:800px; height:400px; border:3px solid; border-radius:10px; -moz-border-radius:10px;">
                    
                </div>
                
                <div id="div_map_processing" style="width:800px; height:400px; position:absolute; top:0; display:none; background-color:#999; opacity:0.5;filter:alpha(opacity=50); border:3px solid; border-radius:10px; -moz-border-radius:10px;">
                	<img src="images/loading.gif" width="32" height="32" style="margin:200px 0 0 400px;" />
                </div>
                
            </div>
            <div style="clear:both; height:10px;"></div>
            <a style="margin:0 0 0 20px; display:block; width:90px; height:40px; line-height:30px; text-align:center; text-decoration:none; color:#FFF; font-weight:bold; background-image:url(images/button2.png); background-repeat:no-repeat; cursor:pointer;" onclick="showLastLocations();" >Refresh</a>
            
        
            <div class="content_first content_sec_heading">
                
        <div class="content_thirdlog"><!--content_third-->
         	<div class="content_thirdlog_lefttable">
<table width="820px" align="right" cellpadding="0" cellspacing="0" border="1" bordercolor="#ffffff">
<tr style="font-family: Calibri; color:#000000; font-size:16px; text-align:center; background-image:url(images/bgupper.png); background-repeat:repeat ; height:70px;">
<td><strong>Device IMEI</strong></td>
<td><strong>Device Name</strong></td>
<td><strong>Tracking Interval</strong></td>
<td><strong>Mobile Number</strong></td>
<td><strong>Name of Holder</strong></td>
<td><strong>Edit</strong></td>
<td><strong>Delete</strong></td>
</tr>
<?php
include ("connection.php");

$sql="SELECT * FROM phones where phoneid in (select phoneid from phones_users where userid = ".$_SESSION['userid'].")";
$result = mysql_query($sql);
while($row = mysql_fetch_array($result))
  {
  $int = "";
  if($row['checking_interval'] < 1){
  $int = "Stop";
  }
  else{
  $int = $row['checking_interval'];
  }
echo "<tr id='{$row['phoneid']}' uid='{$_SESSION['userid']}' style='font-family: Calibri; color:#000000; font-size:16px; text-align:center; background-image:url(images/bgupper.png); background-repeat:repeat ; height:70px;'>";
echo "<td>{$row['phone_imei']}</td>";
echo "<td>{$row['device_name']}</td>";
echo "<td>{$int}</td>";
echo "<td>{$row['mobile_no']}</td>";
echo "<td>{$row['holder_name']}</td>";
echo "<td><a rel='facebox' href='editdevice.php?id={$row['phoneid']}'><img src='images/EDIT.png' /></a></td>";
echo "<td><a href='javascript:void(0);' class='deleteitem'><img src='images/deletebutton.png' /></a></td>";
echo "</tr>";
}
?>
<!--
<tr style="font-family: Calibri; color:#000000; font-size:13px; text-align:center; background-image:url(images/bg.png); background-repeat:repeat-x">
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td><img src="images/EDIT.png" /></td>
<td><img src="images/deletebutton.png" /></td>
</tr>
<tr style="font-family: Calibri; color:#000000; font-size:13px; text-align:center;  background-image:url(images/bg.png); background-repeat:repeat-x">
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td><img src="images/EDIT.png" /></td>
<td><img src="images/deletebutton.png" /></td>
</tr>
<tr style="font-family: Calibri; color:#000000; font-size:13px; text-align:center; background-image:url(images/bg.png); background-repeat:repeat-x;">
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td>Demo</td>
<td><img src="images/EDIT.png" /></td>
<td><img src="images/deletebutton.png" /></td>
</tr>
-->
</table>
					
          </div><a rel="facebox" href="devicepop.php"><div class="content_third_left_buttonagain">&nbsp;
                        </div>
				</a>
         	</div> 
			 <div id="dialog"></div>          
            <div class="clear"></div>
			
         </div><!--content_third-->
            </div>
      </div>
    
</div>
    <div class="footer-wrapper">
    	<div class="center_footer_page">
        	<div class="footer_center">
            	<div class="footer_navi" style="display:none;">
            	<a href="" >Home</a>
                <a href="" >Company</a>
                <a href="" >Clients</a>
                <a href="" >Resources</a>
                <a href="" >Support</a>
                <a href="" >Blog</a>
                <a href="" >Contact</a>
                </div>
            </div>
         <div class="clear"></div>
        </div>
    </div>
    <div class="center_header_page">
    	<div class="footer_buttom">
    	<h3>FOOT PRINT</h3><p>Copyright &copy; 2012. All Rights Reserved.</p>
        </div>
         <div class="clear"></div>
    </div>
	<script type="text/javascript">
	
</script>
<script src="js/analytics.js" type="text/javascript"></script>
</body>
</html>
