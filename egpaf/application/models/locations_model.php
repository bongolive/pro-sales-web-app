<?php
class Locations_model extends MY_Model {
	public $table = 'locations';
	public $table_id = 'location_id';

	function __construct() {

		parent::__construct();

	}

	public function form_validation() {

		$this -> form_validation -> set_rules('location_name', 'location name', 'trim|required');
		if($this->input->post('location_id')){
			$this -> form_validation -> set_rules('location_code', 'location code', 'trim|required|callback_checkcode[' . $this -> input -> post('location_id') . ']');
		}else{
		$this -> form_validation -> set_rules('location_code', 'location code', 'trim|required|callback_checkcode[false]');
		}
		

	}

	public function read_stat($where) {
		$data = $this -> db -> select('count(location_id) as total') ->from($this->table)
		-> where($where) -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}

	}

}
