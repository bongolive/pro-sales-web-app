<?php 
session_start();
include('common.php');

$error = "";

if(isset($_POST["btn_export"])){
	
	if(count($_POST["cb_devices"]) <= 0){
		$error .= "No Device Selected.";
	}else if($_POST["startdate"] == ""){
		$error .= "<br />Start Date is Empty.";
	}else if($_POST["enddate"] == ""){
		$error .= "<br />End Date is Empty.";
	}else{
		
		$date = date_create($_POST['startdate']);
		$s_date = date_format($date, 'Y-m-d H:i:s');
		$date = date_create($_POST['enddate']);
		$e_date = date_format($date, 'Y-m-d H:i:s');
		
		$imei = $_POST["cb_devices"];
		
		$imei_query = "";
		for($i = 0; $i < count($imei); $i++){
			$imei_query .= "phone_imei = '".$imei[$i]."' ".(($i < count($imei)-1) ? " OR " : "");
		}
		//echo $imei_query;
		//exit;
		
		include ("connection.php");
			
		require_once 'PHPExcel_classes/PHPExcel.php';
	
	
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
	
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Foot Print Admin")
									 ->setLastModifiedBy("Foot Print Admin")
									 ->setTitle("Exported Reports from Foot Print Site")
									 ->setSubject("Exported Reports from Foot Print Site")
									 ->setDescription("Exported Reports from Foot Print Site for the User '".$_SESSION['username']."'")
									 ->setKeywords("Exported Reports from Foot Print Site")
									 ->setCategory("Exported Reports from Foot Print Site");			
		
				// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Device Name')
					->setCellValue('B1', 'Subject')
					->setCellValue('C1', 'Report')
					->setCellValue('D1', 'Date')
					->setCellValue('E1', 'Comments')
					->setCellValue('F1', 'Latitude')
					->setCellValue('G1', 'Longitude');
		$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
		
		
		$sql="SELECT p.device_name, r.* FROM reports r INNER JOIN phones p ON r.phoneid = p.phoneid WHERE r.datetime between '".$s_date."' AND '".$e_date."' AND r.phoneid IN (SELECT phoneid FROM phones WHERE ".$imei_query." )";
		$result = mysql_query($sql);
		$index = 2;
		while($row = mysql_fetch_array($result))
		{
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$index, $row["device_name"])
						->setCellValue('B'.$index, $row["subject"])
						->setCellValue('C'.$index, $row["report"])
						->setCellValue('D'.$index, $row["datetime"])
						->setCellValue('E'.$index, $row["comments"])
						->setCellValue('F'.$index, $row["latitude"])
						->setCellValue('G'.$index, $row["longitude"]);
						
			$index++;
		}
		
		
		
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Reports');
		
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		
		// Redirect output to a client's web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="report_for_user-'.$_SESSION['username'].'.xlsx"');
		header('Cache-Control: max-age=0');
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
	
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
<link rel="stylesheet" href="bx_styles.css" type="text/css" />
<title>footprint</title>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>



		<link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.8.23/themes/smoothness/jquery-ui.css" />
		<link rel="stylesheet" media="all" type="text/css" href="jquery-ui-timepicker-addon.css" />
		
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.8.24/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
		<script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
  
<script type="text/javascript">
	

  $(document).ready(function(){
    
  
 // $("#txt_startdate").datepicker();
 // $("#txt_startdate").val($.datepicker.formatDate('mm/dd/yy', new Date()));
  //$("#txt_enddate").datepicker();
 // $("#txt_enddate").val($.datepicker.formatDate('mm/dd/yy', new Date()));
  
  
		$('#startdate').datetimepicker();
		$('#enddate').datetimepicker();
  
  });
  //http://stackoverflow.com/questions/1187824/finding-date-by-subtracting-x-number-of-days-from-a-particular-date-in-javascrip
  
  
  function validateForm(){
	  
	  var devices = document.getElementsByName("cb_devices[]");
	  var flagDevices = false;
	  
	  for(var i = 0; i < devices.length; i++){
		  if(devices[i].checked){
			  flagDevices = true;
			  break;
		  }
	  }
	  if(flagDevices == false){
		  alert("No Device is selected.");
		  return false;
	  }else if(document.getElementById("startdate").value == ""){
		  alert("Start Date is Empty.");
		  return false;
	  }else if(document.getElementById("enddate").value == ""){
		  alert("End Date is Empty.");
		  return false;
	  }
	  return true;
  }


</script>
</head>

<body>
<?php //session_start();
//include('common.php');btn_export
?>
	<div class="body_class">
    	<div class="center_header_page">
        	<div class="header-left">
            	<a href="index.php" ><img src="images/logo3.png" alt=""  /></a>
            </div><div class="header-right">
            	<span style="font-family:Arial; font-size:14px; color:#FFFFFF; margin-top:8px; float:left">Welcome <?php echo $_SESSION['username'];?><br /><a href="logout.php" style="color:#FFFFFF">Logout</a></span>
            </div>
            
        </div>
	</div>
	<div class="center_header_pageinner">
    	<div class="menu_wrapperinner">
        	
            <div class="menu_firstinner">
            	
                <div class=" menu_contentinner">
                	<a  href="device.php" >Device</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="track.php" >Track</a>
                </div>
            </div>
			 <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="task.php" >Tasks</a>
                </div>
            </div>
			  <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="report.php" >Reports</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="profile.php" >Profile</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="support.php" >Support</a>
                </div>
            </div>
           
            <div class="clear"></div>
        </div>

    </div>
    <div class="line">
    </div>
    <div class="center_header_page">
      <div class="content" >
        
            <div class="content_first content_sec_heading">
                
        <div class="content_thirdlog"><!--content_third-->
         	<div class="content_thirdlog_left">
         	  
                <div class="content_third_left_second">
                	<h3>Export Reports</h3>
                    <div class="clear"></div>
					<!--<form id="track_form" action="javascript:void(0);" method="post" >-->
                    <form action="" method="post" onsubmit="return validateForm();">
                    <div class="content_third_left_contactlogpro">
                    	<div class="content_third_left_contactlogpro_input">
                    	<p>Select Device(s):</p>
                        <!--<select name="device_name" style="color:#000000; font-size:16px">-->
                        	<div style="width:225px; float:left; padding:0 0 10px 0;">
                        
                                
                                
                                <?php
                                include ("connection.php");
        
                                $sql="SELECT * FROM phones where phoneid in (select phoneid from phones_users where userid=".$_SESSION['userid'].")";
                                $result = mysql_query($sql);
                                while($row = mysql_fetch_array($result))
                                {
									echo '<div style="width:225px; float:left;">';
                                  	echo '<input id="cb'.$row['phone_imei'].'" type="checkbox" name="cb_devices[]" value="'.$row['phone_imei'].'" /><label for="cb'.$row['phone_imei'].'">'.$row['device_name'].'</label><br />';
									echo '</div>';
                                }
                                ?>
                                
                                
                            
                            </div>

                        </div>
				
                        <div class="content_third_left_contactlogpro_input">
                        <p>Start Date:</p><input type="text" value="" id="startdate" name="startdate" style="margin:0 0 10px 0;"/>
						<input type="hidden" value="" id="txt_startdate" name="txt_startdate"/>                     
						</div>
						<div class="content_third_left_contactlogpro_input">
                    	<p>End Date:</p><input type="text" value="" id="enddate" name="enddate"  style="margin:0 0 10px 0;"/>
						<input type="hidden" value="" id="txt_enddate" name="txt_enddate" />
                        </div>
                        
                        <div class="submit_button">
                        	<!--<a href="javascript:void(0);" id="submit_a">SUBMIT</a>-->
                            <input id="btnSubmitReport" type="submit" value="Export" name="btn_export" />
                        </div>
                        <div style="color:#F00; font-size:12px; text-align:left;"><?php echo $error; ?></div>
                    </div>
                    </form>
					
              </div>
         	</div>           
            
          
            <div class="clear"></div>
			
         </div><!--content_third-->
            </div>
      </div>
    
</div>
    <div class="footer-wrapper">
    	<div class="center_footer_page">
        	<div class="footer_center">
            	<div class="footer_navi" style="display:none;">
            	<a href="" >Home</a>
                <a href="" >Company</a>
                <a href="" >Clients</a>
                <a href="" >Resources</a>
                <a href="" >Support</a>
                <a href="" >Blog</a>
                <a href="" >Contact</a>
                </div>
            </div>
         <div class="clear"></div>
        </div>
    </div>
    <div class="center_header_page">
    	<div class="footer_buttom">
    	<h3>FOOT PRINT</h3><p>Copyrihgt &copy; 2012. All Rights Reserved.</p>
        </div>
         <div class="clear"></div>
    </div>
	
	<script src="js/analytics.js" type="text/javascript"></script>
</body>
</html>















