<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
<link rel="stylesheet" href="bx_styles.css" type="text/css" />
<title>footprint</title>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" media="all" type="text/css" href="jquery-ui-timepicker-addon.css" />
<link rel="stylesheet" media="all" type="text/css" href="css/pagination.css" />

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<link href="facebox.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="js/facebox.js" type="text/javascript"></script> 
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="js/jquery.pagination.js"></script>
<style type="text/css">

.content_third_left_contactlogpro input
{
	width:165px;	
}
.content_third_left_contactlogpro select
{
	width:165px;
}	
-->
</style>
<script type="text/javascript">
(function(cash) {
		$.fn.outerHTML = function() {
			return $(this).clone().wrap('<div></div>').parent().html();
		}
	})(jQuery);
	
 function deleteclicked(ctrl){
	var x=window.confirm("Are you sure want to delete this report?")
		if (x){			
			var parent = $(ctrl).closest('TR');
			var id = parent.attr('id');
			var uid = parent.attr('uid');
			$.ajax({
				type: 'POST',
				data: 'id=' +id+"&uid="+uid+"&type=report",
				url: 'deleteitem.php',
				success: function(msg){					
					$('#'+id).remove();
					initPagination();
				}
				});
		}
  }
  
  function getQuerystring(key, defaultValue) {
		if (defaultValue == null) defaultValue = "";
		key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
		var qs = regex.exec(window.location.href);
		if (qs == null)
			return defaultValue;
		else
			return qs[1];
	}
  
  $(document).ready(function(){
   /* $(".deleteitem").click(function(){
		var x=window.confirm("Are you sure want to delete this report?")
		if (x){			
			var parent = $(this).closest('TR');
			var id = parent.attr('id');
			var uid = parent.attr('uid');
			$.ajax({
				type: 'POST',
				data: 'id=' +id+"&uid="+uid+"&type=report",
				url: 'deleteitem.php',
				success: function(msg){
					$('#'+id).remove();
				}
				});
		}
	});*/
  });
  
  	 $(document).ready(function(){
		$('#startdate').datetimepicker();
		$('#enddate').datetimepicker();
		
		if(document.location.search.length) {
			
			var sd = decodeURIComponent(getQuerystring("startdate","")).replace("+"," ");
			var ed = decodeURIComponent(getQuerystring("enddate","")).replace("+"," ");
			$("#txtsubject").val(getQuerystring("txtsubject",""));
			$("#txtreport").val(getQuerystring("txtreport",""));
			$("#startdate").val(sd);
			$("#enddate").val(ed);
			
			var s = $("select[name='device_name']");
			var v = getQuerystring("device_name","");
			s.val(v);
	
		}
  });
</script>

</head>

<body>
<?php session_start();
include('common.php');
include ("connection.php");
?>
	<div class="body_class">
    	<div class="center_header_page">
        	<div class="header-left">
            	<a href="index.php" ><img src="images/logo3.png" alt=""  /></a>
            </div><div class="header-right">
            	<span style="font-family:Arial; font-size:14px; color:#FFFFFF; margin-top:8px; float:left">Welcome <?php echo $_SESSION['username'];?> <br /><a href="logout.php" style="color:#FFFFFF">Logout</a></span>
            </div>
            
        </div>
	</div>
	<div class="center_header_pageinner">
    	<div class="menu_wrapperinner">
        	
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="device.php" >Device</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="track.php" >Track</a>
                </div>
            </div>
			 <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="task.php" >Tasks</a>
                </div>
            </div>
			  <div class="menu_firstinner">
            	
                <div class="active menu_contentinner">
                	<a href="report.php" >Reports</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="profile.php" >Profile</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                		<a href="support.php" >Support</a>
                </div>
            </div>
           
            <div class="clear"></div>
			</div>
        </div>
    
    </div>
    <div class="line">
    </div>
    <div class="center_header_page">
      <div class="content" >
        
            <div class="content_first content_sec_heading">
                
        <div class="content_thirdlog"><!--content_third-->
         	<div class="content_thirdlog_left1">
         	  
                <div class="content_third_left_second">
                	<h3>Search</h3>
                    <div class="clear"></div>
					<form id="search_report_form" action="report.php" method="get" >
                    <div class="content_third_left_contactlogpro">
                    	
				
                        <div class="content_third_left_contactlogpro_input">
                        <input type="text" value="" id="startdate" name="startdate" placeholder="Start date"  class="search"/>						                     
						</div>
						
						<div class="content_third_left_contactlogpro_input">
                    	<input type="text" value="" id="enddate" name="enddate" placeholder="End date"  class="search"/>						
                        </div>
						
						<div class="content_third_left_contactlogpro_input">
                    	<select name="device_name" style="color:#000000; font-size:16px" class="search">
						<option selected value="-1">Select Device</option>
						<?php
						$sql="SELECT * FROM phones where phoneid in (select phoneid from phones_users where userid=".$_SESSION['userid'].")";
						$result = mysql_query($sql);
						while($row = mysql_fetch_array($result))
						{
                          echo "<option value={$row['phoneid']}>{$row['device_name']}</option>";
                        }
						?>
                        </select>
                        </div>                    
						<div class="content_third_left_contactlogpro_input">
							<input type="text" value="" name="txtsubject" id="txtsubject" placeholder="Subject" class="search"/>
                        </div>		
						<div class="content_third_left_contactlogpro_input">
							<input type="text" value="" name="txtreport" id="txtreport" placeholder="Report" class="search"/>
                        </div>	
						
                    </div>
					<input type="submit" value="" id="btn_search" name="btn_search" class="btnsearch"/>
                    </form>
					
              </div>
         	</div>            <div class="content_third_center1">
			
            </div>
         	<div class="content_third_right1">
			
<table width="660px" align="right" cellpadding="0" cellspacing="0" border="1" bordercolor="#ffffff" id="hiddenresult" style="display:none;">

<?php
if ($_SERVER['QUERY_STRING'] !== ''){
		
		//$_POST['txt_enddate'];
		$date = date_create($_GET['startdate']);
		$sdate = date_format($date, 'Y-m-d H:i:s');
		$date = date_create($_GET['enddate']);
		$edate = date_format($date, 'Y-m-d H:i:s');
		$sub = "r.subject like '%$_GET[txtsubject]%'";
		if(strlen($_GET['txtsubject']) <= 0){
			$sub = "1=1";
		}
		$rep = "r.report like '%$_GET[txtreport]%'";
		if(strlen($_GET['txtreport']) <= 0){
			$rep = "1=1";
		}
		$phoneid = "1=1";
		if($_GET['device_name'] >0){
			$phoneid = "r.phoneid = $_GET[device_name]";
		}
		$date = "r.datetime between '$sdate' and '$edate'";
		if(strlen($_GET['startdate']) <= 0 || strlen($_GET['enddate']) <= 0)
		{
			$date = "1=1";
		}
		$sql = "SELECT r.*,p.device_name FROM reports r inner join phones p on p.phoneid=r.phoneid left join phones_users pu on pu.phoneid=p.phoneid where  pu.userid=".$_SESSION['userid']." and $sub and $rep and $phoneid and $date";
		$result =mysql_query($sql,$con);
		
	}else{
		//$sql="SELECT * FROM reports where phoneid in (select phoneid from phones_users where userid = ".$_SESSION['userid'].")";
		$sql ="SELECT r.*,p.device_name FROM reports r inner join phones p on p.phoneid=r.phoneid left join phones_users pu on pu.phoneid=p.phoneid where  pu.userid=".$_SESSION['userid']." order by datetime DESC";

		$result = mysql_query($sql);
	}

if (mysql_num_rows($result) > 0){
while($row = mysql_fetch_array($result))
  {
 
echo "<tr id='{$row['id']}' uid='{$_SESSION['userid']}' style='font-family: Calibri; color:#000000; font-size:16px; text-align:center; background-image:url(images/bgupper.png); background-repeat:repeat ; height:70px;' class='result'>";
echo "<td>{$row['device_name']}</td>";
echo "<td>{$row['subject']}</td>";
echo "<td>{$row['report']}</td>";
echo "<td>{$row['datetime']}</td>";
echo "<td>{$row['comments']}</td>";
echo "<td><a rel='facebox' href='showmap.php?lat={$row['latitude']}&lon={$row['longitude']}&name={$row['device_name']}'>show</a></td>";

echo "<td><a rel='facebox' href='showEditReport.php?rptid={$row['id']}'><img src='images/EDIT.png' /></a></td>";

echo "<td><a href='javascript:void(0);' class='deleteitem' onclick='deleteclicked(this);'><img src='images/deletebutton.png' /></a></td>";
echo "</tr>";
}
}
?>

</table>

<div class="clear"></div>
<table width="660px" align="right" cellpadding="0" cellspacing="0" border="1" bordercolor="#ffffff" id="searchresult">

<tr>
	<td colspan="8" style="border:0px; font-family: Calibri; color:#000000; font-size:16px; text-align:right; height:40px;" align="right" valign="middle">
    	<a href="reportExcelExports.php"><strong style="float:right; height:20px; line-height:20px; margin:3px 3px 3px 3px;">Excel Export</strong><img src="images/excel_export.png" width="20" height="20"  style="float:right;" /></a>
    </td>
</tr>

<tr style="font-family: Calibri; color:#000000; font-size:16px; text-align:center; background-image:url(images/bgupper.png); background-repeat:repeat ; height:70px;">
	<td><strong>Device Name</strong></td>
	<td><strong>Subject</strong></td>
	<td><strong>Report</strong></td>
	<td><strong>Date</strong></td>
	<td><strong>Comments</strong></td>
	<td><strong>Location</strong></td>
	<td><strong>Edit</strong></td>
	<td><strong>Delete</strong></td>
</tr>
</table>
<div id="Pagination" class="pagination">
</div>
<div class="clear"></div>
          </div>
		   <div class="clear"></div>
					 </div>
         	
        	 <div id="dialog"></div>          
            <div class="clear"></div>
			
         </div><!--content_third-->
            </div>
      </div>
    
</div>
    <div class="footer-wrapper">
    	<div class="center_footer_page">
        	<div class="footer_center">
            	<div class="footer_navi" style="display:none;">
            	<a href="" >Home</a>
                <a href="" >Company</a>
                <a href="" >Clients</a>
                <a href="" >Resources</a>
                <a href="" >Support</a>
                <a href="" >Blog</a>
                <a href="" >Contact</a>
                </div>
            </div>
         <div class="clear"></div>
        </div>
    </div>
    <div class="center_header_page">
    	<div class="footer_buttom">
    	<h3>FOOT PRINT</h3><p>Copyright &copy; 2012. All Rights Reserved.</p>
        </div>
         <div class="clear"></div>
    </div>
	<script type="text/javascript">
	var page_size = 10;
        var num_entries = $('#hiddenresult tr.result').length;
        var num_entries2 = 0;
		var hrd;
        $(document).ready(function () {
			//alert(num_entries);
			/*
			hrd = $('#searchresult').html();
			$("#Pagination").pagination(num_entries, {
                items_per_page: page_size,
				num_edge_entries: 10,
                callback: pageselectCallback
            });
            if (num_entries <= page_size) {
                $("#Pagination").hide();
            } else {
                $("#Pagination").show();
            }*/
			initPagination();
        });
		
		function initPagination(){
			
		hrd = $('#searchresult').html();
		$('#searchresult').empty();

			$("#Pagination").pagination(num_entries, {
                items_per_page: page_size,
				num_edge_entries: 10,
                callback: pageselectCallback
            });
            if (num_entries <= page_size) {
                $("#Pagination").hide();
            } else {
                $("#Pagination").show();
            }
			
		}
	
	function pageselectCallback(page_index, jq) {
		var items_per_page = page_size;
		var max_elem = Math.min((page_index + 1) * items_per_page, num_entries);
		var newcontent = '';

		for (var i = page_index * items_per_page; i < max_elem; i++) {
			newcontent += $('#hiddenresult tr.result:eq(' + i + ')').clone().wrap('<div></div>').parent().html();
		}
		newcontent = hrd + newcontent;
		//            var new_content = jQuery('#hiddenresult div.result:eq(' + page_index + ')').clone();
		$('#searchresult').empty().append(newcontent);
		setTimeout("initfacebox();",100);
		$('#facebox .content').empty()
		
		return false;
	}
	function initfacebox(){
		$('a[rel*=facebox]').facebox();
	}
</script>
<script src="js/analytics.js" type="text/javascript"></script>
</body>
</html>
