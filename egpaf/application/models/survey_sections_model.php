<?php
class Survey_sections_model extends MY_Model {
	public $table = 'survey_sections';
	public $table_id = 'section_id';

	function __construct() {

		parent::__construct();

	}

	public function form_validation() {

		$this -> form_validation -> set_rules('section_title', 'Section Title', 'trim|required');

	}
	
}