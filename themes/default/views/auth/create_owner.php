<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('create_owner'); ?></h2>
    </div>
    <div class="box-content"> 
        <div class="row">            
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('create_owner'); ?></p>

                <?php $attrib = array('class' => 'form-horizontal', 'data-toggle' => 'validator', 'role' => 'form');
                echo form_open("auth/create_owner", $attrib);
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-5">
                            <div class="form-group">
                                <?php echo lang('first_name', 'first_name'); ?> 
                                <div class="controls">
                                    <?php echo form_input('first_name', (isset($_POST['first_name']) ? $_POST['first_name'] : ''), 'class="form-control" id="first_name" required="required" pattern=".{3,10}"'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('last_name', 'last_name'); ?>
                                <div class="controls">
                                    <?php echo form_input('last_name', (isset($_POST['last_name']) ? $_POST['last_name'] : ''), 'class="form-control" id="last_name" required="required"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= lang('gender', 'gender'); ?> 
                                <?php
                                $ge[''] = array('male' => lang('male'), 'female' => lang('female'));
                                echo form_dropdown('gender', $ge, (isset($_POST['gender']) ? $_POST['gender'] : ''), 'class="tip form-control" id="gender" data-placeholder="' . lang("select") . ' ' . lang("gender") . '" required="required"');
                                ?> 
                            </div>

                            <div class="form-group">
                                <?php echo lang('company', 'company'); ?>
                                <div class="controls">
                                    <?php echo form_input('company', (isset($_POST['company']) ? $_POST['company'] : ''), 'class="form-control" id="company" required="required"'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('phone', 'phone'); ?>
                                <div class="controls">
                                    <?php echo form_input('phone', (isset($_POST['phone']) ? $_POST['phone'] : ''), 'class="form-control" id="phone" required="required"'); ?>
                                </div>
                            </div>
                          <div class="form-group">
                               <?= lang('status', 'status'); ?>
                                <?php
                                $opt = array(1 => lang('active'), 0 => lang('inactive'));
                                echo form_dropdown('status', $opt, (isset($_POST['status']) ? $_POST['status'] : ''), 'id="status" data-placeholder="' . lang("select") . ' ' . lang("status") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?> 
                            </div>


                        </div>
                        <div class="col-md-5 col-md-offset-1">

                            <div class="form-group">
                                <?= lang("group", "group"); ?>
                                <?php
                              //  $gp[""] = "";
                                foreach ($groups as $group) {
                                    if($group['name'] != 'customer' && $group['name'] != 'supplier' && $group['name'] != 'superadmin' && $group['name'] != 'admin' && $group['name'] != 'sales' && $group['name'] != 'agent') {
                                        $gp[$group['id']] = $group['name'];
                                    }
                                }
                                echo form_dropdown('group', $gp, (isset($_POST['group']) ? $_POST['group'] : ''), 'id="group" data-placeholder="' . lang("select") . ' ' . lang("group") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?> 
                            </div>

                            <div class="form-group">
                                <strong><?php echo "App Type"; ?> *</strong>
                                <?php
                                //$gp[""] = "";
                                $app_type = array("proshop"=>"Proshop", "prosale"=>"Prosale");
                                echo form_dropdown('app_type', $app_type, (isset($_POST['app_type']) ? $_POST['app_type'] : ''), 'id="app_type" data-placeholder="' . lang("select") . 'App Type" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>


                            </div>

                          <div class="form-group">
                                <strong><?php echo "Account Type"; ?> *</strong>
                                <?php
                                //$gp[""] = "";
                                $acc_type = array("normal"=>"Normal", "premium"=>"Premium");
                                echo form_dropdown('acc_type', $acc_type, (isset($_POST['acc_type']) ? $_POST['acc_type'] : ''), 'id="acc_type" data-placeholder="' . lang("select") . 'Account Type" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                                <!-- Here -->


                            <div class="form-group">
                                <?php echo lang('email', 'email'); ?> 
                                <div class="controls">
                                    <input type="email" id="email" name="email" class="form-control" required="required" value="<?php echo (isset($_POST['email']) ? $_POST['email'] : ''); ?>"/>
                                    <?php /* echo form_input('email', '', 'class="form-control" id="email" required="required"'); */ ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('username', 'username'); ?> 
                                <div class="controls">
                                    <input type="text" id="username" name="username" class="form-control" required="required" pattern=".{4,20}" value="<?php echo (isset($_POST['username']) ? $_POST['username'] : ''); ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('password', 'password'); ?> 
                                <div class="controls">
                                    <?php echo form_password('password', '', 'class="form-control tip" id="password" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"'); ?>
                                    <span class="help-block"><?=lang('pasword_hint')?></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('confirm_password', 'confirm_password'); ?>
                                <div class="controls">
                                    <?php echo form_password('confirm_password',  '', 'class="form-control" id="confirm_password" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="'.lang('pw_not_came').'"'); ?>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-5 col-md-offset-1">
                                <div class="row">
                                <div class="col-md-8"><label class="checkbox" for="notify">
                                <input type="checkbox" name="notify" value="0" id="notify" /> <?= lang('notify_user_by_email') ?></label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                </div> 

                <p><?php echo form_submit('add_user', lang('add_user'), 'class="btn btn-primary"'); ?></p>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#group').change(function(event) {
            var group = $(this).val();
            if(group == 1 || group == 2) {
                $('.no').slideUp();
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'biller');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'warehouse');
            } else {
                $('.no').slideDown();
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'biller');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'warehouse');
            }
        });
    });
</script>