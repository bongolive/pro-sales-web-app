<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';





/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Service extends REST_Controller {
    
    private $device_imei;
    private $tag;
    private $account_id;    
    private $returned = array();
    private $lastsync;

    function __construct()    {
        
        // Construct the parent class
        parent::__construct();
        
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key

        $this->load->model('api_model');
        $this ->load->library('user_agent');

    }

    function index_post() {      
        // if( $this->api_model->check_device_master( $this->post('imei')) ) 
        $this->device_imei = $this->post('imei');
        $this->tag = $this->post('tag');

        if( $this->tag  &&  $this->device_imei ) {
            //$exists = $this->devices_model->api_check_device_master($data);
             $this->account_id = $this->api_model->get_device_account( $this->device_imei );
            if( $this->account_id ){
                $exist_ = $this->api_model->api_check_device_sync( array('device_imei' => $this->device_imei) );
                if( $exist_){
                    $this->proceed = true;
                } else {
                    $reg = $this->api_model->api_register_device_sync( array('device_imei' => $this->device_imei) );
                    if($reg){
                        $this->proceed = true;
                    }
               }
           }    
        
                if( $this->proceed ) {

                   $lastsync = $this->api_model->api_get_device_entities_lastsync( array('device_imei' => $this->device_imei) );
                   //var_dump($lastsync);exit;
                   if( $lastsync['products'] && $lastsync['customers'] && $lastsync['puchase_items'] && $lastsync['product_categories'] && 
                       $lastsync['sales'] && $lastsync['sales_items'] && $lastsync['warehouse'] && $lastsync['settings'] ) {                                              
                                
                                $this->api_model->api_update_master_lastsync( array('device_imei' => $this->device_imei), 
                                                                              array('last_sync' => date('Y-m-d H:i:s')));
                            }
                    $this->lastsync = $lastsync;

                    //file_put_contents("json.txt", file_get_contents('php://input'), FILE_APPEND);
                
                    if( $this->account_id ) {
                       switch ( $this->tag ){
                            case "upload_customers": // Done                           
                                 $this-> _upload_customers( $this->post('incommingcustomers') );
                                break;
                            case "assign_products":  //Done
                                $this-> _assign_products( $lastsync );
                                break;
                            case "assign_customers":  //Done
                                $this-> _assign_customers();
                                break;
                            case "upload_sales": // On old API it was upload_orders
                                $this-> _upload_sales( $this->post('sales') );
                                break;
                            case "receive_product_cats": // Done
                                $this-> _receive_product_category(  );
                                break;
                            case "upload_product_cats": // Done
                                $this-> _upload_product_category( );
                                break;
                            case "receive_purchase_items": // Pending
                                $this-> _receive_purchase_items();
                                break;                            
                            case "upload_purchase_items": // Done
                                $this-> _upload_purchase_items( $this->post('puritems') );
                                break;
                            case 'upload_sales_items':
                                $this-> _upload_orders_items();
                                break;
                            case 'assign_settings':
                                $this-> _assign_settings();
                                break;
                            case 'device_tracking':
                                $this-> _device_tracking();
                                break;
                            case 'multi_media_data':
                                $this-> _multi_media_data();
                                break;                            
                            case 'acktag':
                                $this-> _process_acktag($this->post('item'));
                                break;
                            case 'upload_stock':
                                $this-> _upload_stock();
                                break;           
                            default:
                                $this-> _invalid_request();
                                break;           
                            default:
                                $this-> _invalid_request();
                                break;
                       }
                    }   // If IMEI Match 
                } // If Proceed
    } // If IMEI and Tag
    else
    {
        $this->returned['error'] = 1;
        $this->returned['message'] = "Either Tag or IMEI is missing.";        
    }

    $this->response( $this->returned, 200 ); //200 being the HTTP response code
    }

    /**************************** save customers from the device ******************************/
    
    function _upload_customers($data){
       
        $commons = ( array('account_id'=> $this->account_id) );
        
        $customers = $this-> api_model -> receive_device_customer( $commons, $data );

        //var_dump($customers);exit;

        if( $customers ){

            $this->returned['success'] = 1;            
            if( count($customers) )
                 $this->returned['customerSentList'] = $customers;            
            $this->response($this->returned);

        }   else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "no customer received";
            $this->response($this->returned, 200 );
        }
    }

    /**************************** assign products to the device ******************************/
    function _assign_products ( $lastsync ) {
        
        $where = array('device_imei' => $this->device_imei);
        $ack = $lastsync[$this->tag];
        if( $ack == 1 ){
            $where['last_update_time']= " > ".$lastsync[$this->tag."_synctime"];
            }
        
        $stock = $this->api_model ->get_all_products( array('account_id'=>$this->account_id) );
        if($stock){
            $this->returned['success'] = 1;
            $this->returned['productsList'] = $stock;
            $this->response($this->returned);
        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "no products";
            $this->response( $this->returned );
        }
    }
    
    /**************************** assign customers to the device ******************************/
    private function _assign_customers(){
        $where = (array('account_id'=> $this->account_id ,'last_update_time >' => $this->lastsync['customers_synctime'] ));
        $customers = $this-> api_model -> get_all_customers($where);
       // echo $this->db->last_query();exit;

        if($customers){
            $this->returned['success'] = 1;
            $this->returned['customerList'] = $customers;
            $this->response($this->returned);
        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "no customers";
            $this->response($this->returned);
        }
    }
    
   /**************************** upload orders *****************************/
    private function _upload_sales ( $sales )
    {
        $where = array('device_imei'=> $this->device_imei, 'account_id' => $this->account_id);
        $save = $this->api_model->save_sales($sales, $where);
        if ($save) {
            $arrayback = array();
            foreach ($data as $key => $value):
                $arrayback[] = array("order_id" => $value['order_id']);
            endforeach;
            $this->returned['success'] = 1;
            $this->returned['orderList'] = array('ordersList' => $arrayback);
            $this->response($this->returned);
        } else {
            $this->returned['error'] = 1;
            $this->returned['message'] = "order not received";
            $this->response($this->returned);
        }
    }

    /**** This is the Auntheticate method ***/
 function authenticate_post(){

        $this->load->library('mcrypt');

        //$tag = $this->post('tag');   //$data = file_get_contents('php://input');  //$data = json_decode($data, true);
        //file_put_contents("imei.txt", $this->post('imei'));
        $this->tag = $this->post( 'tag' );
        $this->device_imei = $this->post( 'imei' );
       // echo $this->post('imei');
        if( $this->tag  &&  $this->device_imei ) {
            //$exists = $this->devices_model->api_check_device_master($data);
        $this->account_id = $this->api_model->get_device_account( $this->device_imei );

        if($this->tag == "register_device"){

            //$acc_details =  $this->fetch_acccount_details( $imei );

            $acc_details = $this->api_model->registration( $this->device_imei );            
            //echo $this->db->last_query();
            //var_dump( $acc_details );exit;
            if($acc_details){
            $acc_type = $acc_details->acc_type;

            $business_name = $acc_details->company;
            $contact_person = $acc_details->c_person;
            $mobile_number = $acc_details->mobile;

            $status = ($acc_type) ? array('success'=>1) : array('failure'=>1);

            $warehouses = $this->api_model->getWarehouseByDevice( $this->device_imei );

            $encrypted = $this->mcrypt->encrypt( $this->device_imei."|".$acc_type );

            $ac_t = array( 'account'=>$acc_type , 'business_name'=>$business_name, 
                'contact_person'=>$contact_person, 'mobile'=>$mobile_number,'wh_list'=>$warehouses );
            
            //$debug_a = array('imei'=>$imei, 'decrypt'=>$this->mcrypt->decrypt($encrypted));
           
                $retArr = array_merge($status, $ac_t, array(
                            'authentication_key'=>$encrypted));
            }
            else
                 $retArr = array_merge(array('failure'=>1), array('msg'=>'account information is not correct'));

            $this->response( $retArr, 200 );
        }
        if($this->tag == "authenticate"){

            //$imei = $this->post('imei');
            $acc_details =  $this->api_model->registration( $this->device_imei );            
            $acc_type = $acc_details->acc_type;
            
            $encrypted = $this->mcrypt->encrypt( $this->post('imei')."|".$acc_type );

            $encrypt_device = $this->post('authentication_key');

            $status = ( $encrypted  == $encrypt_device ) ? array('success'=>1) : array('failure'=>1);
            
            $ac_t = array('account'=>$acc_type);
            //$debug_a = array('imei'=>$imei, 'decrypt'=>$this->mcrypt->decrypt($encrypted));
            $retArr = array_merge( $status , $ac_t  );

            $this->response( $retArr );
        }
      }
    }
    

    function _product_category( ){
        
        $where = (array('account_id'=> $this->account_id /*,'device_imei' => $this->device_imei */));
        $categories = $this-> api_model -> get_all_categories($where);

       // var_dump($categories);exit;

        if($categories){
            $this->returned['success'] = 1;
            $this->returned['cat_list'] = $categories;
            $this->response($this->returned);
        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "no category";
            $this->response($this->returned);
        }
    }

    function _upload_purchase_items( $data ){
        //var_dump($this->db);exit;
        $where = (array('account_id'=> $this->account_id ,'device_imei' => $this->device_imei ));
        $purchaseitems = $this-> api_model -> save_purchase_items($data, $where);

       // var_dump($categories);exit;

        if( $purchaseitems ){
            $this->returned['success'] = 1;
            $this->returned['purch_items_list'] = $purchaseitems;
            $this->response($this->returned);
        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "No purchase items uploaded.";
            $this->response($this->returned);
        }
    }

    function _receive_purchase_items( ){
        
        $where = (array('account_id'=> $this->account_id /*,'device_imei' => $this->device_imei */));
        $purchase_items = $this-> api_model -> get_all_purchase_items($where);

       // var_dump($categories);exit;

        if($purchase_items){
            $this->returned['success'] = 1;
            $this->returned['purchase_list'] = $purchase_items;
            $this->response($this->returned);
        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "No purchase Items Found.";
            $this->response($this->returned);
        }
    }

    function _upload_orders_items(){
        return true;
    }
    
    function _assign_settings(){
        return true;
    }
    
    function _device_tracking(){
        return true;
    }
    
    function _multi_media_data(){
        return true;
    }
    
    function _acktag(){
        return true;
    }
    
    function _upload_stock(){
        return true;
    }
    
    function _invalid_request(){
        return true;
    }
    function _process_acktag( $item ){
       // var_dump( "item" );exit;
        if( $item ) {
        $where = array('device_imei' => $this->device_imei );
        switch( $item ){
            case 'customers':
                $use = array('customers' => 1,'customers_synctime' => date('Y-m-d H:i:s'));
                $p = $this-> api_model -> updateLastSyncStatuses($where, $use);
                if($p){
                        $this->out['success'] = 1;
                        $this->response($this->out, 200);
                    }
                break;
            } //Switch ($item)
        } // If Item
    }

    /**************************** assign products to the device ******************************/

    private function  _receive_product_category( ) {
        $where = (array('account_id'=> $this->account_id ,'last_update_time >' => $this->lastsync['product_categories_synctime'] ));
        $customers = $this-> api_model -> get_all_product_categories($where);
        // echo $this->db->last_query();exit;

        if($customers){
            $this->returned['success'] = 1;
            $this->returned['customerList'] = $customers;
            $this->response($this->returned);
        } else {

            $this->returned['error'] = 1;
            $this->returned['message'] = "no customers";
            $this->response($this->returned);
        }
    }
}   