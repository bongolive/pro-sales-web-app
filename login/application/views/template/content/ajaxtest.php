<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<script language='JavaScript' type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js'></script>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <title>Display Page</title>

</head>
<body>


<!--<form>
    <p><?php /*$js = 'id="selectpt"'; echo form_dropdown('employee_id', $employees, 1, $js) */?></p>

    <p><?php /*echo form_input($employee_bagde) */?></p>
</form>-->


<section class="container" role="main">
    <div class="row">

        <article class="span12  data-block">
            <header class="info">
                <h2><?php _l('products_stock_history'); ?></h2>

            </header>
            </article>

        <div class="row">


            <article class="span12 data-block">

                <div class="modal-header">
                    <h4><?php _l('add_stock'); ?></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" action="<?php echo site_url('stock/addstock'); ?>"
                          <fieldset>
                        <table class="datatable table table-striped table-bordered table-hover">
                            <tr><td>
                                    <button type='button' name='getdata' id='getdata'>Get Data.</button>
                                    <div id='result_table'>
                                    </div>

                                </td></tr>
                            <tr><td>
                                    <div class="control-group">
                                        <label class="control-label" for="product_name" ><?php _l('product_name'); ?></label>
                                        <div class="controls">
                                            <?php

                                            if ($products == FALSE) {  $products = array('' => 'No Products');
                                            }
                                            if ($info) { 	$prod = $info['product_id'];
                                            } else {$prod = set_value('product_id');
                                            }
                                            $js = 'id="product_id" onChange="get_price();"';
                                            echo form_dropdown('product_id[]', $products, $prod,$js);
                                            ?>

                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <textarea rows="10" cols="1" id="result_area" > <?php echo $prodprice;?></textarea>


</td><td><div class="control-group">
                                        <label class="control-label" for="propro" ><?php _l('product_name'); ?></label>
                                        <div class="controls">
                                    <select id="propro" onchange="showselection()">
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                        </div>
                                    </div>
                                </td>
                            </tr></table>
                              </fieldset>
                    </form>
                    <button ondblclick="load_data_ajax(1)">Load list (type 1)</button>
                    <button onmouseover="load_data_ajax(2)">Load list (type 2)</button>

                    <hr />

                    <div id="container">
                    </div>

                    </div>
                </article>
            </div>
        </div>
    </section>

<script type="text/javascript">
    var controller = 'contact';
    var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';

   /* function load_data_ajax(type){
        $.ajax({

            'url' : base_url + '/' + controller + '/get_list_view',
            'type' : 'POST', //the way you want to send data to your URL
            'data' : {'type' : type},
            'success' : function(data){ //probably this request will return anything, it'll be put in var "data"

                var container = $('#container'); //jquery selector (get element by id)
                if(data){
                    container.html(data);
                }
            }
        });
    }*/

    $("#product_id").change(function(){
        var res = $("#product_id option:selected").val();
//        $('#result_area').append(res);
//        alert(res);
        var controller = 'orders';
        var base_url = '<?php echo site_url(); ?>';
        $.ajax({
            type : 'POST',
            data : 'product_id='+ res,
            url : base_url+ '/' + controller+ '/getUnitPrice',
            success : function(data){
                $('#result_area').val(data);
            }
        });
    });

</script>

<!--<script type='text/javascript' language='javascript'>

    $('#getdata').click(function(){
        $.ajax({
            url: '<?php /*echo site_url('contact/get_all_users') ;*/?>',
            type:'POST',
            dataType: 'json',
            success: function(output_string){
                $('#result_area').val(output_string);
            }
        });
    });
</script>-->

<!--<script type="javascript">
    $("#product_id").change(function(){
        alert($("#product_id option:selected").val());
        $.ajax({
            type : 'POST',
            data : 'product_id='+ $(this).val(),
            url : base_url+ '/' + controller + '/',
            success : function(data){
                $('#location').val(data);
            }
        });
    });
</script>-->
</body>
</html>