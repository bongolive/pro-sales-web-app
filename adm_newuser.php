<?php
session_start();
if($_SESSION['LogIn'] != 1){
	header("location: login.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
<link rel="stylesheet" href="bx_styles.css" type="text/css" />
<title>footprint</title>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>

<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" media="all" type="text/css" href="jquery-ui-timepicker-addon.css" />
<link rel="stylesheet" media="all" type="text/css" href="css/pagination.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/commons.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.8.24/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="js/jquery.pagination.js"></script>
<link href="facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="js/facebox.js" type="text/javascript"></script>
 <script type="text/javascript">
  	$(document).ready(function(){
	$('#validto').datetimepicker();
  });
  
  function getQuerystring(key, defaultValue) {
		if (defaultValue == null) defaultValue = "";
		key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
		var qs = regex.exec(window.location.href);
		if (qs == null)
			return defaultValue;
		else
			return qs[1];
	}
</script>
<style type="text/css">

.content_third_left_contactlogpro input
{
	width:165px;	
}
.content_third_left_contactlogpro select
{
	width:165px;
}
.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	color: red;
	font-style: italic
}
div.error { display: none; }
-->
</style>
</head>

<body>
<?php
include('common.php');
include ("connection.php");
?>
    <div class="center_header_page">
      <div class="content" >
        
            <div class="content_first content_sec_heading">
                
                

         
        <div class="content_thirdlog"><!--content_third-->
         	<div class="content_thirdlogpopup_left">
         	  
                <div class="content_third_left_second">
                	<h3>Add User</h3>
                    <div class="clear"></div>
					<form id="user_form" name="user_form" action="adm_addedituser.php?XDEBUG_SESSION_START=dk" method="post" >
                    <div class="content_third_left_contactlogpro">					
                    	
                        <div class="content_third_left_contactlogpro_input">
                    	<p>Company Name:</p><input type="text" name="company_name" id="company_name" value="" class="optional" />
                        </div>
						
						
						<div class="content_third_left_contactlogpro_input">
							<p>Client Name</p>
							<input type="text" name="name" id="name" value="" class="required" />						
                        </div>
                        <div class="content_third_left_contactlogpro_input">
							<p>Mobile Number:</p><input type="text" value="" name="mobile" id="mobile" minlength="9" maxlength="12"class="required number"  />
                        </div>					
						<div class="content_third_left_contactlogpro_input">
							<p>E-Mail:</p><input type="text" name="email" id="email" value="" class="required email" />
							<input type="hidden" value="add" name="addedit" id="addedit" />
                        </div>
						<div class="content_third_left_contactlogpro_input">
							<p>Username:</p>
							<input type="text" name="username" id="username" value="" class="required" />					
                        </div>
                        <div class="content_third_left_contactlogpro_input">
							<p>Password:</p><input type="password" name="pass" id="pass" value="" class="required" />
                        </div>	
                        <div class="content_third_left_contactlogpro_input">
							<p>Confirm Password:</p><input type="password" name="conf_pass" id="conf_pass" value="" class="required" />
                        </div>
                        <div class="content_third_left_contactlogpro_input">
							<p>Date Valid To:</p>
							<input type="text" name="validto" id="validto" value="" class="required" />					
                        </div>
                        <div class="content_third_left_contactlogpro_input">
							<p>User Type:</p><select name="user_role" id="user_role" class="required" >
                            	<option value="">Select User role</option>
                                <option value="Client">Normal Client</option>
                                <option value="Administrator">Administrator</option>
                            </select>
                        </div>	
						
                        <div class="content_third_left_buttondevice">
                        	<a href="javascript:void(0);" onclick="checkSave();">SUBMIT</a>
                        </div>
                    </div>
                    </form>
              </div>
         	</div>            
         	
			
         </div><!--content_third-->
            </div>
      </div>
    
</div>
<script type="text/javascript">
  function checkSave(){
		
		  var pass = $("#pass").val();
		  var pass_conf = $("#conf_pass").val();
		
		if($("#name").val() == ''){
			alert("Name of Client is required");
		}else if($("#username").val() == ''){
			alert("Username of client is required");
		}else if($("#validto").val() == ''){
			alert("User access End Date is required");
		}else if(pass === pass_conf){
			document.getElementById('user_form').submit();
		}else{
			alert("Password Does not Match");
		 }
		  
	  }
	  
	 // $('#validto').datetimepicker();
	  
</script>
</body>
</html>
