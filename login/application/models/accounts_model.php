<?php
class Accounts_model extends MY_Model {
	public $table = 'accounts';
	public $table_id = 'account_id';

	function __construct() {
		parent::__construct();
	}

	public function form_validation() {

		$this -> form_validation -> set_rules('company_name', 'Company name', 'trim|required|xss_clean');
	//	$this -> form_validation -> set_rules('firstname', 'Firstname', 'trim|required|xss_clean');
		//$this -> form_validation -> set_rules('lastname', 'lastname', 'trim|required|xss_clean');
		$this -> form_validation -> set_rules('mobile', 'mobile', 'required|numeric||callback_checkmobile[' . $this -> input -> post('account_id') . ']');
		$this -> form_validation -> set_rules('email', 'email', 'valid_email|callback_checkmail[' . $this -> input -> post('account_id') . ']');
	 	
	 	$this -> form_validation -> set_rules('acc_type', 'Account Type', 'trim|required|xss_clean');
		 
		$this -> form_validation -> set_rules('username', 'Username', 'trim|required|xss_clean');
		
		if ($this -> input -> post('password')) {
			$this -> form_validation -> set_rules('password', 'password', 'trim|required|min_length[6]|matches[conf_pass]');
			$this -> form_validation -> set_rules('conf_pass', 'password', 'trim|required');
		}
	}

	public function read($where = FALSE) {
		$this -> db -> select('*,accounts.account_id AS account_id');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		$this -> db -> join('users', 'accounts.user_id = users.user_id');
		$data = $this -> db -> get() -> result_array();
		return $data;
	}
	
	function update($id, $data) {
		echo $this->table_id;
		//do update data into database
		$this -> db -> where($this -> table_id, $id);
		if ($this -> db -> update($this -> table, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

    public function check_account($where){
        $this->table = 'accounts';

        $this->db->where($where);
        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }
}
