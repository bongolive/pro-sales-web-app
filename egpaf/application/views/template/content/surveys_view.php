<!-- Main content -->
<section class="content">
	<div class="row">
	<!-- left column -->
	<div class="col-md-12">
	<!-- general form elements -->
	<div class="box box-primary">
	<div class="box-header">
	<div class="row">
	<div class="col-md-8">
	<h3 class="page-header box-title"> <?php echo lang('survey_title') ?> : <?php echo $survey['survey_title']; ?> <small> <?php echo $survey['descriptions']; ?> </small> </h3> </div>

	<div class="col-md-4">
	<!--<a class="btn btn-primary" type="button" href="<?php echo site_url('reports/survey_report/'.$survey['survey_id']) ?>" style="float: right;margin: 10px;"> <?php echo lang('report'); ?></a>-->
	<!--<a class="btn btn-primary" type="button" href="<?php echo site_url('surveys/create_section/'.$survey['survey_id']) ?>" style="float: right;margin: 10px;"> <?php echo lang('new_section'); ?></a>-->
	<a class="btn btn-info"  type="button" href="<?php echo site_url('surveys') ?>" ;"="" style="float: right; margin: 10px;"> <?php echo lang('back_to_surveys'); ?></a>
	</div></div>
	</div>

	</div> </div> </div>

	<div class="row">
	<?php if(in_array(3, $this->user_permissions)){  ?>
	<div class="col-md-8">
	<?php }else{ ?>
	<div class="col-md-12">

	<?php } ?>
	<div class="box box-primary">
	<div class="box-header">

	<h4 class="page-header box-title"> <?php echo lang('survey_sections') ?> <small>  Sections will help categorise survey questions </small> </h4> </div>

	<div class="box-body " >

	<div class="table-responsive">

	<table id="type1" class="table table-bordered table-striped" width="100%">

	<thead>
	<tr>
	<?php foreach($tb_headers as $header){?>

	<th><?php echo lang($header); ?></th>

	<?php } ?>
	<th></th>
	</tr>
	</thead>
	<tbody>

	<?php if ($tb_data){
	foreach ($tb_data as $data):
	$id = $data['section_id'];
	?>
	<tr class="even gradeC" id="<?php echo $id; ?>">

	<?php foreach ($row_fields as $field): ?>
	<td><?php

	echo ucwords($data[$field]);
	?></td>
	<?php endforeach; ?>

	<td>
	<div class="btn-group">
	<a href="<?php echo site_url('surveys/view_section') . '/' . $survey['survey_id'] . '/' . $id; ?>" type="button" class="btn btn-small btn-info"><i class="fa fa-eye"></i>   </a>
	<?php if(in_array(3, $this->user_permissions)){  ?>
	<a href="<?php echo site_url('surveys/update_section') . '/' . $survey['survey_id'] . '/' . $id; ?>" class="btn btn-small btn-primary"> <i class="fa fa-pencil"></i> </a>
	<a href="<?php echo site_url('surveys/delete_sections') . '/' . $survey['survey_id'] . '/' . $id; ?>" class="btn btn-small btn-danger"> <i class="fa fa-trash-o"></i> </a>
	<?php } ?>
	</div>
	</td>
	</tr>									<?php endforeach;
		}else {
	?>
	<tr> <td colspan="<?php echo count($tb_headers) + 2; ?>"> No data availabled <td> </tr>
	<?php } ?>
	</tbody>
	<tfoot>
	<tr>
	<?php foreach($tb_headers as $header){ ?>

	<th><?php echo lang($header); ?></th>

	<?php } ?>

	</tr>
	</tfoot>
	</table>
	</div>

	</div>  </div> </div>
	<?php if(in_array(3, $this->user_permissions)){  ?>
	<div class="row">
	<div class="col-md-4">
	<div class="box box-info">
	<div class="box-header">
	<h3 class="box-title"><?php echo lang('new_survey_sections') ?></h3>

	</div>
	<?php  if($info){ ?>
	<form role="form" method="post" action="<?php echo site_url('surveys/update_section/' . $survey['survey_id'] . '/' . $info['section_id']); ?>">

	<?php }else{ ?>
	<form role="form" method="post" action="<?php echo site_url('surveys/create_section/' . $survey['survey_id']); ?>">

	<?php  	} ?>
	<?php

	if ($this -> session -> userdata('empty_data')) {
		echo '<span class="alert alert-danger"> Section Title is Required </span>';
		$this -> session -> unset_userdata('empty_data');
	} elseif ($this -> session -> userdata('section_saved')) {
		echo '<span class="alert alert-success"> Section Title Added Succesfuly </span>';
		$this -> session -> unset_userdata('section_saved');
	}
	?>

	<div class="box-body">
	<?php if($info){ ?>
	<input name="section_id" id="section_id" class="form-control" type="hidden"  value="<?php  echo $info['section_id']; ?>" />
	<?php } ?>

	<div class="form-group">
	<label class="control-label" for="section_title" ><?php echo lang('section_title'); ?></label>

	<input name="section_title" id="section_title" class="form-control" placeholder="type section title" type="text"  value="<?php
	if ($info) { echo $info['section_title'];
	} else { $value = set_value();
	}
	?>" required />
	</div>
	<div class="form-group">
	<label class="control-label" for="qn_numbering" ><?php echo lang('qn_numbering'); ?></label>

	<input name="qn_numbering" id="qn_numbering" class="form-control" placeholder="Section Prefix e.g A,B,C" type="text"  value="<?php
	if ($info) { echo $info['qn_numbering'];
	} else { $value = set_value();
	}
	?>" required />
	</div>

	</div>
	<div class="modal-footer">
	<?php if(in_array(3, $this->user_permissions)){   ?>
	<div class="form-actions">
	<button type="reset" value="Reset" class="btn btn-alt btn-medium "  >
	Reset
	</button>
	<button class="btn btn-alt btn-medium btn-success" type="submit">
	Save
	</button>
	</div>  <?php } ?>
	</div></form>
	</div>
	</div>

	</div> <?php } ?>
</section>
