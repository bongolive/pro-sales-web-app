<?php
class Dashboard_model extends MY_Model {
	public $table = 'participants';
	public $table_id = 'participant_id';
	function __construct() {
		parent::__construct();
	}

	public function count_participants($where) {

		$this -> db -> select('count(*) as total');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}

		$data = $this -> db -> get() -> result_array();

		if (!$data) {
			$data = FALSE;
		}
		return $data;
	}
	
	public function  participants_trend($where) {

		$this -> db -> select('count(*) as total,interview_date');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		$this->db->group_by('interview_date');

		$data = $this -> db -> get() -> result_array();

		if (!$data) {
			$data = FALSE;
		}
		return $data;
	}

}
