<script type="javascript">

    function addRowss(tableID) {

        var table = document.getElementById(tableID);

        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);

        var colCount = table.rows[0].cells.length;

        for (var i = 0; i < colCount; i++) {

            var newcell = row.insertCell(i);

            newcell.innerHTML = table.rows[0].cells[i].innerHTML;
            //alert(newcell.childNodes);
            switch(newcell.childNodes[0].type) {
                case "text":
                    newcell.childNodes[0].value = "";
                    break;
                case "checkbox":
                    newcell.childNodes[0].checked = false;
                    break;
                case "select-one":
                    newcell.childNodes[0].selectedIndex = 0;
                    break;
            }
        }
    }
</script>
<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>

</div><!-- Main page container -->

<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('new_order');  ?></h2> 
						 
            			</header> 
       				 </article> 
    </div>

    <div class="row">
     						
  
    	 <article class="span12 data-block">
								<div class="modal-header">
									<h4><?php _l('customer_details'); ?>
									</h4>
								</div>
								<div class="modal-body">
						<?php  if($order){ ?>
							<form class="form-horizontal" method="post" action="<?php echo site_url('orders/update_order'); ?>">
								<input type="hidden" name="order_no" value="<?php echo $order['order_id']; ?>" />
							<?php }else{ ?>			
                                <form class="form-horizontal" method="post" action="<?php echo site_url('orders/new_order'); ?>">
                             <?php } ?>   	
                                                          
                                  	<table class="datatable table table-striped table-bordered table-hover">
                               <tr>
                               	<td>
                               		 <div class="control-group">
                                        	<label class="control-label" for="customer_name"><?php _l('customer_id'); ?></label>
                                        	<div class="controls">
                                        		
                                        		<?php
												if ($customers == FALSE) {

													$customers = array(''=>'No Customers Exist');
												}
												 
												if($order){$value =$order['customer_id'];}else{ $value=set_value(); }
                                                $js = 'id="s_t_customer" ';
												echo form_dropdown('customer_id',$customers,$value,$js);
                                        		?>
                                        		<?php echo form_error('customer_name'); ?>  &nbsp;
                                        		<a href="" class="btn btn-success btn-medium"><?php _l('new_customer'); ?></a>
                                        	</div>
                                    	</div>

                                    	
<div class="control-group">
										<div class="controls">
										
										</div>
									</div>

                                    	<div class="control-group">
                                        	<label class="control-label" for="order_date"><?php _l('order_date'); ?></label>
                                        	<div class="controls">
                                        		<?php echo form_error('order_date'); ?>
                                        			<div class="input-append no-margin">
												<input  name="order_date" class="datepicker input-small" value="<?php if($order){echo $order['order_date'];}else{echo set_value('order_date');}?>" placeholder="<?php echo date('Y-m-d')?>" type="text"><span class="add-on"><i class="icon-calendar"></i></span>
											</div>
                                        		 
                                        		</div>
                                        	</div>
                               	</td>
                               	<td>
                               		 	
                                        	<div class="control-group">
                                        	<label class="control-label" for="delivery_date"><?php _l('delivery_date'); ?></label>
                                        	<div class="controls">
                                        		<?php echo form_error('delivery_date'); ?>
                                        		
                                        		<div class="input-append no-margin">
												<input  name="delivery_date" class="datepicker input-small"  placeholder="<?php echo date('Y-m-d')?>" value="<?php if($order){ echo $order['delivery_date'];}else{ echo set_value('delivery_date');}?>" type="text"><span class="add-on"><i class="icon-calendar"></i></span>
											</div>
                                        		</div>
                                        	</div>
                                        	<div class="control-group">
                                        	<label class="control-label" for="assigned_to"><?php _l('assigned_to'); ?></label>
                                        	<div class="controls">
                                        		<?php
                                         
                                        				if($employees == FALSE){
                                        					$employees = 'No Sales Team';
                                        				} 
														if($order){
															$emp = $order['employee_id'];
														}else{
															$emp = FALSE;
														}
														echo form_dropdown('assigned_to',$employees,$emp,'required');
                                        		?>
                                        		 
                                        		</div>
                                        	</div>
                                        	  		
                               	</td>
                               	
                               </tr> </table>
  <fieldset>
<legend><?php _l('order_details');?></legend>
      
<div class="control-group">
                                        	<label class="control-label" for="order_no" ><?php _l('order_details'); ?></label>
                                        	<div class="controls"><?php echo form_error('product_id[]'); ?>
                                        		<table class="datatable table table-striped table-bordered table-hover"><tr><th></th><th><?php  _l('product_name')?></th> <!--<th><?php  _l('order_date')?></th>--> <th><?php  _l('order_qty')?></th> <th><?php  _l('sale_price')?></th><th><?php _l('order_value');?></th></tr></table>

<TABLE id="dataTable" class="datatable table table-striped table-bordered table-hover" >
   <?php 
   
   //get products
   					if($products ==FALSE){  $products = array(''=>'No Products'); } 
            		if($order==FALSE){$prod = set_value('product_id');}
   
     	if($order_details) {
 
	foreach ($order_details as $order):
		  
		 	$prod = $order['product_id'];  
		?>
		
		<TR>
            <TD><INPUT type="hidden" name="order_id" value="<?php echo $order['order_id']; ?>"/></TD>

            <TD><?php echo form_dropdown('product_id[]', $products, $prod); ?></TD>

            <TD><INPUT type="number" name="order_qty[]"class="input-medium"  value="<?php echo $order['order_qty']; ?>" placeholder="Quantity" required="" /></TD>

            <TD><INPUT type="number" name="price[]"class="input-medium" placeholder="sale Price" disabled=""  value=""required="" /></TD>
             <TD><input  type="text"  name="" class="input-medium" value=" " placeholder="order value" disabled="" /></TD>
            
        </TR>
		
<?php 	endforeach;
			}
		?>


        <TR id="s_t_rowvalue">
            <TD><INPUT type="checkbox" name="chk"/></TD>
            <TD><?php

			if ($products == FALSE) {
				  $products = array('' => 'No Products');
			}
			 
			 $prod = set_value('product_id');

			$id = 'id="s_t_product" ';
			echo form_dropdown('product_id[]', $products, $prod,$id);
            		?>
            </TD>
         <!--  	<TD><input  type="date"  name="order_date[]"class="input-medium" placeholder="order date" required="" /></TD>-->
            <TD><INPUT type="number" name="order_qty[]"class="input-medium" min="1" id="s_t_qty"'+_counter+'" placeholder="Quantity" required="" /></TD>

            <TD><input  type="number"   name="sale[]"  class="input-medium sale" id="s_t_sale"'+_counter'"  value=" " placeholder="Unit Price"/></TD>
            <TD><input  type="text"  name="order_value[]" class="input-medium ordervalue" id="s_t_ordervalue"'+_counter+'" onchange="showvalue()" value=" "  placeholder="Order Value" /></TD>
        </TR>

      </TABLE> 

       <span class="" style="float: right;padding-right: 5%">
 <input type="button" class="btn btn-medium btn-info" id="s_t_addrow" value="Add Row"   onclick="addRow('dataTable')" />
 <input type="button" class="btn btn-medium btn-danger" id="s_t_removerow" value="Delete Row"  onclick="deleteRow('dataTable')" />
 </span>   
 
    </div>
</div>
						</div>

								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('orders'); ?>" class="btn btn-alt" data-dismiss="modal">Back</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
                            </fieldset>

                        </form>

             <script type="text/javascript">
                 var rowsarray = [];
                 var salearray = [];
                 var qtyarray = [];
                 var ordervaluearray = [];
                 var _counter = 0;

                 $('#s_t_product').change(function (){
                     var rs = $("#s_t_product option:selected").val();

                     var controller = '/index.php/en/orders';
                     var base_url = '<?php echo site_url(); ?>';

                     $.ajax({
                         url: base_url + '/' + controller + '/get_product_price',
//                         url: '<?php //echo base_url(); ?>//index.php/en/orders/get_product_price',
                         type: 'POST',
                         dataType: 'json',
                         data: {product_id: rs},

                         success: function (output_string) {
                             $('#s_t_sale').val(output_string).prop('disabled',true);
//                             var price = $('.input-medium.sale').val();
                             var price = $('#s_t_sale').val();
//                             var qty = $('#s_t_qty').val();
                             var qty = $('#s_t_qty').val();
                             if(!qty.length > 0)
                             { qty = 0;
                                 $('#s_t_qty').val(qty);}
                             var tt = price*qty;
                             $('#s_t_ordervalue').val(tt).prop('disabled',true);
                         }
                     });
                 });


                 $('#s_t_qty').change(function (){
                     var price = $('.input-medium.sale').val();
                     var qty = $('#s_t_qty').val();
                     var tt = price*qty;
                     $('.input-medium.ordervalue').val(tt).prop('disabled',true);
                 });

                 $('#s_t_addrow').click(function (){
                     var _tt = $('.input-medium.ordervalue').val();
                     var _price = $('.input-medium.sale').val();
                     var _qty = $('#s_t_qty').val();
                     if(_tt < 1){
                         alert("please select a product, insert quantity to proceed");
                     } else {
                         qtyarray.push(_qty);
                         salearray.push(_price);
                         ordervaluearray.push(_tt);

                         rowsarray = $.merge([], salearray,qtyarray,ordervaluearray);
                         _counter + 1;
                     }
                 });

                 $('#s_t_removerow').mouseover(function (){
                     $.each(rowsarray, function( index, value ) {
                         alert( index + ": " + value );
                     });
                 });

                 $('#s_t_rowvalue').mouseover(function(){
                     var rowcount = $('#dataTable tr').length;
                     var rowindex = $(this).parent('#dataTable').index();
//                     alert(rowcount);
                     console.log(rowcount);/*
                     console.log(qtyarray);
                     console.log(salearray);
                     console.log(ordervaluearray);*/
                     console.log('row index is '+rowindex);
                 });

             </script>
                        </article>
                        </div>                        	
                        </div>
   </section>

					 