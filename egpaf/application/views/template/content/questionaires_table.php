<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
 
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('questions_list') ?></h3>    
                                    <a href="<?php echo site_url($back) ?>" style="float: right;margin:10px; " class="btn btn-info" type="button" ><?php echo lang('back'); ?></a>  
                                    <?php if(in_array(3, $this->user_permissions)){  ?> 
                                    <a href="<?php echo site_url($create) ?>" style="float: right;margin:10px; " class="btn btn-primary" type="button" ><?php echo lang('new_question'); ?></a>
                                    
                                     <a href="<?php echo site_url($reordering)  ?>" id="save_sort" style="float: right;margin:10px;color: #fff " class="btn btn-success" type="button" ><i class="fa fa-sort"></i> <?php echo lang('qn_reorder'); ?></a>                              
                                <?php } ?>
                                
                                </div><!-- /.box-header -->
                                
                                 <div class="box-body table-responsive">
                                    <table id="type1" class="table table-bordered table-striped  sorted_table questionz">
                                       
                                   <thead>
                                    
                                   	 <tr>
                                   	 	
                                            	  <?php foreach($tb_headers as $header){?>

											<th><?php echo lang($header); ?></th>

                                        <?php } ?>
                                            	 <th width="15%"><?php echo lang('action'); ?></th>
                                            </tr>
                                         
                                        </thead>
                                    
                                        <tbody id="sortable">
                                         <?php if ($tb_data){ 
									foreach ($tb_data as $data):   
                                            $id = $data[$table_id];
                                    ?>
									<tr id="qn_<?php echo $data['question_id'];?>">
											
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php   
									 								  
										  echo ucwords($data[$field]);
									 
										  	?></td>
                                        <?php endforeach; ?>
										
                                            <td>
                                               <div class="btn-group">
													<a href="<?php echo site_url($view).'/'.$data['question_id']; ?>" type="button" class="btn btn-small btn-info"><i class="fa fa-eye"></i> </a>
													   <?php if(in_array(3, $this->user_permissions)){  ?> 
													<a href="<?php echo site_url($edit).'/'.$data['question_id'];; ?>" type="button" class="btn btn-small btn-primary"> <i class="fa fa-pencil"></i> </a>
													<a href="<?php echo site_url($delete).'/'.$data['question_id'];; ?>" onclick="return confdeleting();" type="button" class="btn btn-small btn-danger"> <i class="fa fa-trash-o"></i> </a>
												    <?php  } ?> 
												 </div> 						 
											</td>
									</tr>									<?php endforeach;
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
                                    </tbody>  
                                        <tfoot>
                                            
                                               <?php foreach($tb_headers as $header){?>

											<th><?php echo lang($header); ?></th>

                                        <?php } ?>

                                            </tr> 
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content --> 