<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Businesses extends User_Controller {
	
	public $session_userid = '';
	public function __construct()
	{
		parent::__construct();
		$this->load->model('business_model');
		$this->session_userid = $this->session->userdata('userid');
	}

	public function index()
	{
	 
		$condUserPerm = array('userid' => $this -> session -> userdata('userid'));
		$user_permissions = $this -> user_model -> getUserPermissions($condUserPerm);
		
		//Make all tab not allowed
		$header['track_tab'] = "";
		$header['device_tab'] = "";
		$header['tasks_tab'] = "";
		$header['reports_tab'] = "";
		$header['deliveries_tab'] = "";
		$header['customers_tab'] = "";
		$header['boundaries_tab'] = "";
		$header['support_tab'] = "allow";
		$header['pathfinder_tab'] = "";
		$header['stocks_tab'] = "";
		$header['registrations_tab'] = "";

		foreach ($user_permissions as $user_permission) {
			if ($user_permission['permission_name'] == 'track tab') {
				$header['track_tab'] = "allow";
			} else if ($user_permission['permission_name'] == 'device tab') {
				$header['device_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'tasks tab') {
				$header['tasks_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'reports tab') {
				$header['reports_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'deliveries tab') {
				$header['deliveries_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'customers tab') {
				$header['customers_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'boundaries tab') {
				$header['boundaries_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'pathfinder survey tab') {
				$header['pathfinder_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'stocks tab') {
				$header['stocks_tab'] = "allow";
			}
			if ($user_permission['permission_name'] == 'registrations tab') {
				$header['registrations_tab'] = "allow";
			}
		}

		if ($header['stocks_tab'] != "allow") {
			redirect('profile/support');
		}

		$fields['shop_name'] = $this -> input -> post("shop_name");
		$fields['client_name'] = $this -> input -> post("client_name");
		$fields['phone'] = $this -> input -> post("phone");
		$fields['email'] = $this -> input -> post("email");

		$condQuery = " WHERE user_id ='" . $this -> session_userid . "'";
		$where_excel = array();
		if (!empty($fields['shop_name'])) {
			$condQuery .= " AND shop_name = '" . $fields['shop_name'] . "'";
			$where_exce['shop_name'] = $fields['shop_name'];
		}

		if (!empty($fields['client_name'])) {
			$condQuery .= " AND client_name = '" . $fields['client_name'] . "'";
			$where_exce['client_name'] = $fields['client_name'];
		}

		if (!empty($fields['phone'])) {
			$condQuery .= " AND phone = '" . $fields['phone'] . "'";
			$where_exce['client_name'] = $fields['client_name'];
		}

		if (!empty($fields['email'])) {
			$condQuery .= " AND email = '" . $fields['email'] . "'";
			$where_exce['email'] = $fields['email'];
		}
		if (!empty($fields['device_imei'])) {
			$condQuery .= " AND  phone_imei = '" . $fields['device_imei'] . "'";
			$where_stock['phone_imei'] = $fields['device_imei'];
		}

		$cond = array('user_id' => $this -> session_userid);
		$fields['tb_data'] = $this -> business_model -> getCustomer($cond);
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js');
		$fields['tb_name'] = 'clients_tb_name';
		// Put the fields from the database in this array
		$fields['row_fields'] = array('business_name', 'contact_name','category' ,'phone', 'email', 'address','street','city');
		$fields['tb_headers'] = array('business_name', 'contact_name', 'category','phone', 'email',  'address','street','city', 'action');
		$header['stcl_active'] = 'class="active"';
		$fields['add_btn'] = 'add_new_client';
		$fields['print_excel'] = 'stocks/clients_excel';
		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/table', $fields);
		$this -> load -> view('template/footer', $ft_data);
 
	}

}



/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */
