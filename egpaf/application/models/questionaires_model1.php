<?php
class Questionaires_model extends MY_Model {
	public $table = 'questionaires';
	public $table_id = 'question_id';

	function __construct() {

		parent::__construct();
		$this -> table = 'questionaires';
		$this -> table_id = 'question_id';

	}

	public function form_validation() {
		$this -> form_validation -> set_rules('question', 'question', 'trim|required');
	}

	public function save_transation($data) {
		$this -> table = 'questionaires';
		if ($this -> save($data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function update_translation($id, $data) {
		$this -> table = 'questionaires';

		//do update data into database
		$this -> db -> where('transalation_of', $id);
		if ($this -> db -> update($this -> table, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function simple_read($where = false) {

		$this -> table = 'questionaires';

		$this -> db -> select('*');

		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}

		$results = $this -> db -> get() -> result_array();
		if ($results) {
			return $results;
		} else {
			return FALSE;
		}
	}

	function simple_update_qn($id, $data) {
		//do update data into database
		$this -> table = 'questionaires';
		$this -> db -> where('question_id', $id);
		if ($this -> db -> update($this -> table, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function read_questions($where = false) {
		$this -> table = 'questionaires';
		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}

		$this -> db -> join('surveys', 'survey_id = questionaires.survey_no', 'left');
		$this -> db -> join('survey_sections', 'survey_section = section_id', 'left');
		$this -> db -> order_by('question_rank', 'asc');

		$results = $this -> db -> get() -> result_array();
		if ($results) {
			return $results;
		} else {
			return FALSE;
		}
	}

	public function read_questions_with_choices($where = false) {
		$this -> table = 'questionaires';
		$this -> db -> select('*, GROUP_CONCAT(data_type) as data_type', FALSE);
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		//$this -> db -> order_by('survey_section', 'asc');
		$this -> db -> order_by('question_rank', 'asc');
		$this -> db -> group_by('response_choices.question_no');
		$this -> db -> join('survey_sections', 'survey_section = section_id', 'left');
		$this -> db -> join('response_choices', 'response_choices.question_no = question_id', 'left');
		$results = $this -> db -> get() -> result_array();
		if ($results) {
			return $results;
		} else {
			return FALSE;
		}
	}

	public function read_open($where) {
		$this -> db -> select('rank');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		$results = $this -> db -> get() -> result_array();
		if ($results) {
			return $results;
		} else {
			return FALSE;
		}
	}

	public function surveys($where) {
		$this -> table = 'surveys';
		$results = $this -> read($where);
		if ($results) {
			return $results;
		} else {
			return FALSE;
		}
	}

	public function save_answer_choices($data) {
		$this -> table = "response_choices";
		$result = $this -> save($data);
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function read_answer_options($data) {
		$this -> table = "response_choices";
		$result = $this -> read($data);
		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public function auto_update_answer_choices($id, $data) {
		$this -> table = "response_choices";
		$this -> table_id = 'transalation_of';
		if ($this -> update($id, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update_answer_choices($id, $data) {
		$this -> table = "response_choices";
		$this -> table_id = 'choice_id';
		if ($this -> update($id, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function delete_choices($choice) {
		$this -> db -> where('choice_id', $choice);
		$qry = $this -> db -> delete('response_choices');
		if ($qry) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function delete_trans_choices($choice) {
		$this -> db -> where('transalation_of', $choice);
		$qry = $this -> db -> delete('response_choices');
		if ($qry) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function get_last_qn_rank($where) {
		$this -> db -> select('max(question_rank) as rank');
		$this -> db -> from('questionaires');
		$this -> db -> where($where);
		$result = $this -> db -> get() -> result_array();

		return $result[0]['rank'];

	}

	function save_response_choice($data) {
		$this -> table = "response_choices";
		$this -> table_id = 'choice_id';

		if ($this -> db -> insert($this -> table, $data)) {
			return True;
		} else {
			return FALSE;
		}
	}

}
