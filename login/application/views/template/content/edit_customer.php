                    <div class="modal-header">

									<h4><?php _l('update_customer'); ?></h4>

								</div>

                                <form class="form-horizontal" action="<?php echo site_url("customers/update"); ?>" method="post">
									 <?php foreach ($customers as $customer): ?>
                                    <fieldset>
                                        <div class="modal-body">
                                        <div class="control-group">

                                        	<label class="control-label" for="customername" ><?php _l('customer_name'); ?></label>

                                        	<div class="controls">

                                        	<input name="customername" id="customername" class="input-xlarge" type="text" value="<?php echo $customer['customername'];?>" required>
                                        	<input name="customerid" type="hidden" id="customerid" value="<?php echo $customer['id'];?>" />
                                        	</div>

                                    	</div>

                                    	<div class="control-group">

                                        	<label class="control-label" for="mobilenumber"><?php _l('mobile_number'); ?></label>

                                        	<div class="controls">

                                        	<input name="mobilenumber" id="mobilenumber" value="<?php echo $customer['mobilenumber'];?>" class="input-xlarge" type="number" required>

                                        	</div>

                                    	</div>

                                        <div class="control-group">

                                        	<label class="control-label" for="email" ><?php _l('email'); ?></label>

                                        	<div class="controls">

                                        	   <input name="email" id="email" value="<?php echo $customer['email'];?>" class="input-xlarge" type="email"> 

                                        	</div>

                                    	</div>

                                        <div class="control-group">

                                        	<label class="control-label" for="address"><?php _l('address'); ?></label>

                                        	<div class="controls">
												<input name="address" id="address" value="<?php echo $customer['address'];?>" class="input-xlarge" type="text" >
                                        	</div>
                                    	</div>
								</div>

								<div class="modal-footer">

									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>

									<div class="form-actions">

            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;padding: 6px 10px;">Submit</button>

                                    </div>

								</div>

                            </fieldset>
							<?php endforeach; ?>
                        </form>