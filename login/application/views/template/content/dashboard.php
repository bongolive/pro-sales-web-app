	<!-- Main page container -->
			<section class="container" role="main">

				<!-- Grid row -->
				<div class="row">
					
					<!-- Flexible block scaffolding example -->
					<div class="span12">

						<!-- Grid row -->
						<div class="row">
 
			<?php
			$t_balance = $t_sales = $t_paid = $pending = $re_order = 0;

			//get products
			if ($products) {
				foreach ($products as $key => $value) {
					if ($value['items_on_hand'] <= $value['re_order_value']) {
						$re_order = $re_order + 1;
					}

				}
			}
			//get pending orders
			if ($sales) {
				//read collects
				foreach ($sales as $p_orders) {
					if ($p_orders['order_status'] == 1) {
						$pending = $pending + 1;
						//add count to pending ordrs
					}
				}
			}
if($tb_data){
			foreach ($tb_data as $key => $value) {
				$t_sales = $t_sales + $value['sales_total'];
				$t_paid = $t_paid + $value['paid_amount'];

			}

			$t_balance = $t_sales - $t_paid;
}else{
	$t_balance = 0;
}
					?>
					
		<div class="span3 data-block widget-block" style="">
				<section style="">
				<span style="color:green;font-size:35.6px">  <?php echo   $re_order; ?></span>
				<strong class="widget-label">
					<a href="<?php echo site_url('products'); ?>"><?php _l('re-order');?></a>
					 <!--(<?php echo $settings['default_currency'] ?>)-->
				</strong>
				</section>
		</div>	
		
			<div class="span3 data-block widget-block " style="">
			<section style="">
				 <span style="color:darkred;font-size:35.6px">   <?php echo $pending; ?></span>
				<strong class="widget-label"><?php _l('pending_orders') ?> <!-- (<?php echo $settings['default_currency'] ?>) --></strong>
			</section>
		</div>		 
  
  		
		<div class="span3 data-block widget-block" style="">
				<section style="">
				<span style="color:orange;font-size:35.6px">  <?php echo money_format('%.2n', $t_balance); ?></span>
				<strong class="widget-label"><?php _l('outstanding') ?> (<?php echo $settings['default_currency'] ?>) </strong>
				</section>
		</div>
	<div class="span3 data-block widget-block" style="">
				<section style="">
				<span style=" font-size:35.6px">  <?php echo money_format('%.2n', $t_sales); ?></span>
				<strong class="widget-label"><?php _l('week_sales') ?> (<?php echo $settings['default_currency'] ?>)</strong>
				</section>
		</div>
  
						<!-- Data block -->
					<article class="span6  data-block">
					 <header>
							<h2><span class="icon-shopping-cart-sign"></span><?php _l('products_status') ?></h2>
							<ul class="data-header-actions">
								<li class="demoTabs"><a href="#high_sale" class="btn btn-alt"><?php _l('high_sale'); ?></a></li>
								<li class="demoTabs active"><a href="#low_sale" class="btn btn-alt"><?php _l('low_sale'); ?></a></li>
							</ul>
						</header>
						<section class="tab-content">
								 
							<div class="tab-pane active" id="high_sale">
							 
								<table class="table table-bordered table-striped">
									
									<?php
									if($highsale){
									 	foreach ($highsale as $highsale) { ?>
<tr> <td><?php echo $highsale['product_name'] ?></td><td><?php  echo $highsale['orders']; ?></td></tr>
								<?php 	}
										 }else{ ?>
<tr> <td colspan="2"><?php _l('no_products_available') ?></td></tr>									
								<?php  } ?>
									
									</table>
								 
								</div>
								
								<div class="tab-pane" id="low_sale">
							 
								<table class="table table-bordered table-striped">
									<?php
									if($lowsale){
		 			foreach ($lowsale as $lowsale) { ?>
<tr> <td><?php echo $lowsale['product_name'] ?></td><td><?php  echo $lowsale['orders']; ?></td></tr>
									<?php }	}else{ ?>
									<tr> <td colspan="2"><?php _l('no_products_available') ?></td></tr>	
									<?php } ?>
								</table>
								</div>
						</section> 
									
					 
					</article>
					<!-- /Data block -->
					
									<!-- Data block -->
					<article class="span6 data-block">
						<header>
							<h2><span class="icon-money"></span><?php _l('sales_summary') ?></h2>
						</header>
						<section>

							<table data-chart="pie" class="visualize-chart">
								
								<thead>
									 
								</thead>
								<tbody>
									<tr>
										<th scope="row"><?php _l('total_sales') ?></th><br />
									 
										<?php
										if ($sales) {
											//read collects
											foreach ($sales as $collects) {
												echo "<td> " . $collects['paid_amount'] . " </td>";
											}
										}else{
											echo "<td> 10 </td>";
										}
										?>
										 
									</tr>
									 
									<tr>
										<th scope="row">Outstanding</th>
										 
										<?php
										if ($sales) {
											//read collects
											foreach ($sales as $collects) {
												$outstand = $collects['sales_total'] - $collects['paid_amount'];
												echo "<td>  $outstand </td>";
											}
										}else{
												echo "<td>10</td>";
										}
										?>
									</tr>
								</tbody>
							</table>

						</section>
					</article>
					<!-- /Data block -->
					 
					<!-- /Data block -->
						</div>
						<!-- /Grid row -->

					</div>
					<!-- /Flexible block scaffolding example -->
 
					
				</div>
				<!-- /Grid row -->
 
 
 
			<!-- Main page container -->
			<section class="container" role="main">
  
				<!-- Grid row -->
				<div class="row">
				 
					
					
				</div>
				<!-- /Grid row -->
 

			</section>
			<!-- /Main page container -->
			
			<!-- Sticky footer push -->
			<div id="push"></div>
			
		</div>
		<!-- /Full height wrapper -->
		 