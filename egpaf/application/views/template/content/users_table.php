<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
 
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><?php $pagetitle ?></h3>                                    
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="type2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr> 
                                            	  <?php foreach($tb_headers as $header){?>

											<th><?php echo lang($header); ?></th>

                                        <?php } ?>
                                            	<th></th> 
                                            </tr>
                                        </thead>
                                        <tbody>
  				  <?php if ($tb_data){
									foreach ($tb_data as $data):  
									  
                                            $id = '';//$data[$table_id];
                                     ?>
									<tr>
								
                                        <?php foreach ($row_fields as $field): ?>
                                        	
										  <td><?php
										
										 if($field =='role'){
										     $roles = array('0' => 'User', '1' => 'Administrator', );
										   
										 if($data[$field] == 0){ echo $roles[0];  }elseif($data[$field] ==1){ echo $roles[1];  } 	 
										  	 
										   			
										  }else{
										   echo $data[$field];} ?></td>
                                        <?php endforeach; ?>
										
                                            <td> 
                                                	<a href="<?php echo site_url('employees/update').'/'.$data['employee_id']; ?>" type="button"  class="btn btn-small btn-warning" title="Edit" ><i class="fa fa-pencil"></i> <?php echo lang('edit') ?></a>
                                                	<!--<a href="<?php echo site_url($delete).'/'.$data['employee_id']; ?>" type="button"  class="btn btn-small btn-danger" title="Delete" ><i class="fa fa-trash-o"></i> <?php echo lang('delete') ?></a>-->
                                                	 
											</td>
									
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
                                        </tbody>
                                        <tfoot>
                                            <tr> 
                                               <?php foreach($tb_headers as $header){?>

											<th><?php echo lang($header); ?></th>

                                        <?php } ?>

                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div> 
                </section><!-- /.content --> 

