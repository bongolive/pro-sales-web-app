<div id="page-title" class="clearfix">
            <h2 style="float: left;font-weight: normal;"><?php _l('permissions_form');?></h2>
            
</div> <!-- /.page-title -->
<div class="row">
<div class="span12"> 
 <div id="horizontal" class="widget widget-form">
            <div class="widget-header">	      				
	 <h3> 	 <i class="icon-pencil"></i>
<?php _l('permissions_form');?>
 </h3>	
 </div>
 <div class="widget-content">
<?php 

if($info){ echo form_open('permissions/update',array('class' => 'form-horizontal'));
echo '<input type="hidden" name="perm_id" value="'.$info['perm_id'].'"/>';
}else{
 echo form_open('permissions/save',array('class' => 'form-horizontal'));
 echo '<input type="hidden" name="perm_id" value=""/>';
 }
  
  ?>
 <fieldset>     
 <div class="control-group">
	<span class="control-label"><?php _l('employee_name');?></span> <?php echo form_error('user_id'); ?>
<div class="controls">
<?php 


$user[]='Select user name';
if($users) {

foreach($users as $empl){
$user[$empl['user_id']] = $empl['fullname'];			
} 

if($info){ $emp=$info['user_id']; }else{ $emp= set_value('user_id');}
}else {
	$user[]='No available employees'; $emp=FALSE;
	}
echo form_dropdown('user_id',$user,$emp); 
  
?>
  
</div>
</div>
 

<div class="control-group">
<span class="control-label"><?php _l('Allow permissions');?></span><?php echo form_error('modules'); ?>
<div class="controls">
<?php 
 
  foreach($read_permissions as $perms){
		echo '<label class="radio">
			<input type="checkbox" name="permits[]"  id="optionsRadios2"  value="'.$perms['module_id'].'"';
		if($info) {
			$rules = explode(',',$info['permissions']);  
			$i=count($rules) -1 ; 
			 for($d=0;$d<=$i;$d++) {
			 	   if( $rules[$d] == $perms['module_id'] ){ echo 'checked'; }
				}
	} 
			echo '>'.$perms['module_title'].' </label>';  
  }
 
?>
 </div>
</div> 
 
<div class="form-actions"> 

<a href="<?php echo site_url('permissions');?>" class="btn btn-large">Cancel</a>
 <button type="submit" class="btn btn-info btn-large"><i class="icon-pencil"></i>Save</button></div>

</fieldset>
</form>  

</div>
</div>
