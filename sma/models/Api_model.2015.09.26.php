<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
        
        $this->MESSAGE_CODE = array(1 => array('code' => 1,'message' => 'Operation Successful.'),
                                    10 => array('code' => 10,'message' => 'Category Id does not exists.'),
                                    11 => array('code' => 11,'message' => 'Customer is missing.Reset and try.'),
                                    12 => array('code' => 12,'message' => 'Warehouse does not exists. App should register.'),
                                    13 => array('code' => 13,'message' => 'Product does not exists. Reset product and respective category and purchase items and try '),
                                    14 => array('code' => 14,'message' => 'Sales RefId is not matched. Resend Sale & try.'),
                                    15 => array('code' => 15,'message' => 'Reset Item Sync Status.'),
                                    16 => array('code' => 16,'message' => 'Reset Category.'),
                                    17 => array('code' => 17,'message' => 'Product not found of sale items.'),
                                    18 => array('code' => 18,'message' => 'Sale Item already exists.'),
                                    19 => array('code' => 19,'message' => 'Warehouse not found.'),
                                    20 => array('code' => 20,'message' => 'System error.'),
                                    21 => array('code' => 21,'message' => 'Sale already exists.'),
                                    22 => array('code' => 22,'message' => 'Empty Warehouse ID.'),

             //ave_sale_items
                                    );
    }

    // Fetch account details based on IMEI
    public function get_device_account( $imei )    {
        $this->db->select('devices.account_id,devices.assigned_to,users.active');
        $this->db->join('users','users.id=devices.assigned_to');
        $this->db->where('devices.device_imei', $imei);
        $result = $this->db->get( $this->db->dbprefix('devices') );
       // echo $this->db->last_query();exit;
        if ($result->num_rows() > 0) {
            $j = $result->row();
            return $j;
        } else
            return false;
    }

    // Receive customer from device and save
    public function receive_device_customer($commons, $data) {
        
        $flag = array();
        
        foreach ($data as $value) {
            $insertId = 0;
            $x        = $this->checkCustomerExists(array(
                'company' => trim($value['business_name']),'account_id'=>$commons['account_id']
            ));
            
            if (!$x) {
                switch ($value['is_synced']) {
                    case 0:
                        $insertId = $this->addCustomer($value, $commons);
                        $flag[]   = array(
                            'system_customer_id' => (int) $insertId,
                            '_id' => (int) $value['_id'],
                            'operation' => 'inserted'
                        );
                        break;
                    case 2:
                        if ($this->checkCustomerExistsWithId(trim( $value['system_customer_id']))) {
                            $commons['id']                 = trim( $value['system_customer_id'] );
                            $commons['system_customer_id'] = trim( $value['system_customer_id'] );
                            $updateId                      = $this->updateCustomer( $value, $commons );
                            $flag[]                        = array(
                                'system_customer_id' => (int) $updateId,
                                '_id' => (int) $value['_id'],
                                'operation' => 'updated'
                            );
                        } else {
                            $insertId = $this->addCustomer($value, $commons);
                            $flag[]   = array(
                                'system_customer_id' => (int) $insertId,
                                '_id' => (int) $value['_id'],
                                'operation' => 'inserted'
                            );
                        }
                        break;
                    case 3:
                        // delete(); //Not Implemented Yet
                        break;
                }
            } else {
                $commons['company']            = trim($value['business_name']);
                $commons['system_customer_id'] = $x;
                $updateId                      = $this->updateCustomer($value, $commons);
                $flag[]                        = array(
                    'system_customer_id' => (int) $updateId,
                    '_id' => (int) $value['_id'],
                    'operation' => 'updated'
                );
            }
            
            
        }
        
        return $flag;
    }

    // Adds customer received from Device
    function addCustomer($value, $commons)    {
        $created_name = $this->userNameById( $commons['user_id'], $commons['device_imei'] );
        $tin = isset( $value['tin'] ) ? $value['tin'] : '';
        $vrn = isset( $value['vrn'] ) ? $value['vrn'] : '';
        $vals = array(
            'name' => $value['customer_name'],
            'company' => trim( $value['business_name'] ),
            'phone' => $value['mobile'],
            'email' => $value['email'],
            'address' => $value['street'],
            'city' => $value['city'],
            'account_id' => $commons['account_id'],
            'group_id' => 3,
            'group_name' => 'customer',
            'customer_group_name' => 'General',
            'customer_group_id' => 1,
            'lattd' => $value['lat'],
            'lontd' => $value['lng'],
            'device_imei' => $commons['device_imei'],
            'created_name' => $created_name,
            'tin'=>$tin,
            'vrn'=>$vrn
        );
        $this->db->insert('companies', $vals);
        return $this->db->insert_id();
        
    }
    function updateCustomer($value, $commons)
    {
        
        $where = array(
            'id' => $commons['system_customer_id'],
            'account_id' => $commons['account_id']
        );
        $this->db->where($where);
        $tin = isset( $value['tin'] ) ? $value['tin'] : '';
        $vrn = isset( $value['vrn'] ) ? $value['vrn'] : '';
        $vals = array(
            'name' => $value['customer_name'],
            'company' => $value['business_name'],
            'phone' => $value['mobile'],
            'email' => $value['email'],
            'address' => $value['street'],
            'city' => $value['city'],
            'is_synced' => 1,
            'tin'=> $tin,
            'vrn'=>$vrn
        );
        
        $this->db->update( $this->db->dbprefix('companies'), $vals);
        $qry = $this->db->get_where( $this->db->dbprefix('companies'), $where);
        // echo $this->db->last_query();exit;
        if ($qry->num_rows() > 0)
            return $qry->row()->id;
        else
            return false;
    }
    
    /*
     * Check if customer already exists ???
     **/
    public function checkCustomerExists($where)
    {
        $query = $this->db->get_where( $this->db->dbprefix('companies'), $where);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0)
            return $query->row()->id;
        else
            return false;
    }
    
    /*
     * Check if customer already exists ???
     **/
    public function checkCustomerExistsWithId($id)
    {
        $query = $this->db->get_where( $this->db->dbprefix('companies'), array(
            'id' => $id
        ));
        if ($query->num_rows() > 0)
            return true;
        else
            return false;
    }
    
    public function checkSaleExists($refid, $imei)
    {
        $query = $this->db->get_where( $this->db->dbprefix('sales'), array(
            'reference_no' => $refid,
            'device_imei' => $imei
        ));
        if ($query->num_rows() > 0) {
            //return true;
            return $query->row()->id;
        } else
            return false;
    }

    public function checkReturnSaleExists( $sale_id )
    {
        $query = $this->db->get_where( $this->db->dbprefix('return_sales'), array(
            'sale_id' => $sale_id
        ));
        if ($query->num_rows() > 0) {
            //return true;
            return true;
        } else
            return false;
    }
    
    public function checkSaleItemExists($where)
    {
        $query = $this->db->get_where( $this->db->dbprefix('sale_items'), $where);
        if ($query->num_rows() > 0) {
            //return true;
            return $query->row()->id;
        } else
            return false;
    }
    public function checkPurchaseItemExists($where)
    {
        $query = $this->db->get_where( $this->db->dbprefix('purchase_items'), $where);
        if ($query->num_rows() > 0) {
            //return true;
            return $query->row()->id;
        } else
            return false;
    }
    
    public function upload_products($commons, $data)    {
        
        $flag = array();
        
        foreach ($data as $value) {
            $insertId = 0;
            $updateId = 0;
            
            if ($this->checkCategoryId($value['category'], $commons)) {
                
                if (!$this->checkProductExists(array('product_uniquecode' => $value['product_uniq_name'],'account_id' => $commons['account_id']))) {
                   // echo $this->db->last_query();exit;
                    switch ($value['is_synced']) {
                        case 0:
                            $insertId = $this->addProduct($value, $commons);
                            //echo $this->db->last_query();exit;                  
                            $flag[]   = array(
                                'prod_system_id' => (int) $insertId,
                                '_id' => (int) $value['_id'],
                                'operation' => 'inserted'
                            );
                            break;
                        case 2:
                            
                            if ($this->checkProductExists(array(
                                'product_uniquecode' => $value['product_uniq_name'],'account_id' => $commons['account_id']))) {
                                //echo $this->db->last_query();exit;
                                $commons['id'] = trim($value['system_product_id']);
                                $updateId      = $this->updateProduct($value, $commons);
                                $flag[]        = array(
                                    'prod_system_id' => (int) $updateId,
                                    '_id' => (int) $value['_id'],
                                    'operation' => 'updated'
                                );
                            } else {
                                $insertId = $this->addProduct($value, $commons);
                                //echo $this->db->last_query();exit;                  
                                $flag[]   = array(
                                    'prod_system_id' => (int) $insertId,
                                    '_id' => (int) $value['_id'],
                                    'operation' => 'inserted'
                                );
                                
                            }
                            
                            
                            break;
                        
                        case 3:
                            // delete(); //Not Implemented Yet
                            break;
                    }
                } else {
                    $commons['product_uniquecode'] = trim($value['product_uniq_name']);
                    $updateId                      = $this->updateProduct($value, $commons);
                    $flag[]                        = array(
                        'prod_system_id' => (int) $updateId,
                        '_id' => (int) $value['_id'],
                        'operation' => 'updated'
                    );
                }
                
                
            }
            
            else {
                $flag[] = array_merge(array(
                    'prod_system_id' => 0,
                    '_id' => $value['_id']
                ), $this->MESSAGE_CODE[10]);
            }
            
        } //foreach Ends
        return $flag;
    }
    
    function addProduct($value, $commons)
    {
        $tax_rate = $this->calculateTax($value['tax_rate'], $commons);
        $vals     = array(
            'code' => $value['product_code'],
            'product_uniquecode' => trim($value['product_uniq_name']),
            'barcode_symbology' => $value['barcode_symbol'],
            'name' => $value['product_name'],
            'type' => $value['product_type'],
            'cost' => $value['unit_sale_price'],
            'price' => $value['unit_sale_price'],
            'account_id' => $commons['account_id'],
            'tax_method' => $value['tax_method'],
            'category_id' => $value['category'],
            'details' => $value['description'],
            'tax_rate' => $tax_rate,
            'from_device' => 1,
            'unit' => 0,
            'created_by' => $commons['user_id'],
            'last_update_time' => date('Y-m-d H:i:s')
        );
        $this->db->insert( $this->db->dbprefix('products'), $vals);
        
        // ECHO $this->db->last_query();exit;
        return $this->db->insert_id();
        
    }
    
    function calculateTax($rate, $commons)
    {
        //$this->db->where(array('account_id'=> $commons['account_id'], 'rate'=> $rate) );
        $get_records_tax = $this->db->get_where( $this->db->dbprefix('tax_rates'), array(
            'account_id' => $commons['account_id'],
            'rate' => $rate
        ));
        //echo $this->db->last_query();exit;
        if ($get_records_tax->num_rows() > 0) {
            return $get_records_tax->row()->id;
        } else {
            $this->db->insert('tax_rates', array(
                'rate' => $rate,
                'account_id' => $commons['account_id'],
                'code' => 'VAT' . $rate,
                'name' => 'VAT @' . $rate . '%',
                'type' => 1
            ));
            return $this->db->insert_id();
        }
    }
    
    function checkCategoryId($catId, $commons)
    {
        //$this->db->where(array('account_id'=> $commons['account_id'], 'id'=> $catId) );
        $get_records = $this->db->get_where( $this->db->dbprefix('categories'), array(
            'account_id' => $commons['account_id'],
            'id' => $catId
        ));
        //echo $this->db->last_query();exit;
        if ($get_records->num_rows() > 0) {
            return true;
        } else
            return false;
        
        
    }
    
    //$this->checkCategory($value['category'])
    
    /*
     * Check if Products already exists ???
     **/
    public function checkProductExists($where)
    {
        $query = $this->db->get_where( $this->db->dbprefix('products'), $where);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0)
            return true;
        else
            return false;
    }
    function updateProduct($value, $commons)
    {
        
        $tax_rate = $this->calculateTax($value['tax_rate'], $commons);
        
        $where = array(
            'product_uniquecode' => $value['product_uniq_name'],
            'account_id' => $commons['account_id']
        );
        $this->db->where($where);
        
        $vals = array(
            'barcode_symbology' => $value['barcode_symbol'],
            'name' => $value['product_name'],
            'type' => $value['product_type'],
            'price' => $value['unit_sale_price'],
            'tax_method' => $value['tax_method'],
            'category_id' => $value['category'],
            'details' => $value['description'],
            'tax_rate' => $tax_rate,
            'from_device' => 1
        );
        
        $this->db->update($this->db->dbprefix('products'), $vals);
        $qry = $this->db->get_where( $this->db->dbprefix('products'), $where);
        if ($qry->num_rows() > 0)
            return $qry->row()->id;
        else
            return false;
    }
    /*
     * check if the device exists in the table pro _device_sync_status/_api
     * */
    public function api_check_device_sync($where)
    {
        
        $this->db->where($where);
        $query = $this->db->get( $this->db->dbprefix('device_sync_status') );
        if ($query->num_rows() == 1) {
            return true;
        }
    }
    
    /*
     * register the device in the sync table
     * */
    public function api_register_device_sync($data)
    {
        $data = array( 'device_imei' => $data['device_imei'] );
        $this->db->insert( $this->db->dbprefix('device_sync_status'), $data);
        if ($this->db->affected_rows()) {
            return true;
        }
    }
    
    /*
     * get the last sync of the devices' entities
     * */
    public function api_get_device_entities_lastsync($where)
    {
        //$data = array('device_imei' => $data['imei']);
        $this->db->where($where);
        $query = $this->db->get( $this->db->dbprefix('device_sync_status') );
        if ($query->num_rows() == 1) {
            return $query->row_array(); // its usage should be $settings = $query['settings']
        }
    }
    /*
     * get the statuses of the devices' entities
     * */
    public function api_update_master_lastsync($where, $data)
    {
        $this->db->update( $this->db->dbprefix('devices'), $data, $where);
        if ($this->db->affected_rows()) {
            return true;
        }
    }

    /*
     * Fetch all the products
     * */
    public function get_all_products($where, $user_id)
    {
        //var_dump($where);exit;
        $this->db->select('*');
        $this->db->where($where);

        $this->db->where("( created_by=0 OR created_by = {$user_id} )");

        $purchase = $this->db->get( $this->db->dbprefix("products") );
       // echo $this->db->last_query();exit;
        //echo $this->db->last_query();
        //echo $this->db->last_query();exit;
        if ($purchase->num_rows() > 0) {
            $rows        = $purchase->result_array();
            $productlist = array();
            
            foreach ($rows as $row) {
                $getTaxRate = $this->getTaxRate($row["tax_rate"]);
                $tmp['prod_system_id']    = $row["id"];
                $tmp['product_code']      = $row["code"];
                $tmp['product_name']      = $row["name"];
                $tmp['unit']              = $row["unit"];
                $tmp['cost']              = $row["cost"];
                $tmp['unit_sale_price']   = $row["price"];
                $tmp['re_order_value']    = $row["alert_quantity"];
                $tmp['category']          = $row["category_id"];
                $tmp['tax_method']        = $row["tax_method"];
                $tmp['barcode_symbol']    = $row["barcode_symbology"];
                $tmp['description']       = $row["details"];
                $tmp['product_type']      = $row["type"];
                $tmp['product_uniq_name'] = $row["product_uniquecode"];
                $tmp['tax_rate']          = $getTaxRate;
                
                $productlist[] = $tmp;
            }
            return $productlist;
        } else
            return array();
    }

    /*
     * Fetch all the Sales
     * */
    public function get_all_sales($where)
    {

        $this->db->select('*');
        $this->db->where($where);
        $salequery = $this->db->get( $this->db->dbprefix("sales") );
        //echo $this->db->last_query();exit;
        //echo $this->db->last_query();
        //echo $this->db->last_query();exit;
        if ($salequery->num_rows() > 0) {
            $rows        = $salequery->result_array();
            $saleslist = array();

          /*  id, date, reference_no, customer_id, customer, biller_id, biller, warehouse_id, note, staff_note, total, product_discount,
            order_discount_id, total_discount, order_discount, product_tax, order_tax_id, order_tax, total_tax, shipping, grand_total,
            sale_status, payment_status, payment_term, due_date, created_by, updated_by, updated_at, total_items, pos, paid, return_id,
            surcharge, account_id, device_imei, lat, lng, id, id
          */

            foreach ($rows as $row) {
                    $tmp["sy_sale_id"]= $row['id'];
                    $tmp["sale_refid"]= $row['reference_no'];
                    $tmp["total_items"]=$row['total_items'];
                    $tmp["total_discount"]= $row['total_discount'];
                    $tmp["ack"]= 0;
                    $tmp["tax_setting"]= $row['product_tax'];
                    $tmp["lng"]= $row['lng'];
                    $tmp["sales_date"]= $row['date'];
                    $tmp["total_sales"]= $row['total'];
                    $tmp["grand_total"] = $row['grand_total'];
                    $tmp["payment_status"]= $row['payment_status'];
                    $tmp["paid"]= $row['paid'];
                    $tmp["is_printed"]= $row['is_printed'];
                    $tmp["total_tax"]= $row['total_tax'];
                    $tmp["note"]= $row['note'];
                    $tmp["lat"]= $row['lat'];
                    $tmp["tax_setting"]= $row['tax_setting'];
                    $tmp["customer_id"]= $row['customer_id'];
                    $tmp["sale_status"]= $row['sale_status'];
                 $saleslist[] = $tmp;
            }
            return $saleslist;
        } else
            return array();
    }


    function getTaxRate($taxId = 0)
    {
        $get_tax_rate = $this->db->get_where( $this->db->dbprefix('tax_rates'), array(
            'id' => $taxId
        ));
        if ($get_tax_rate->num_rows() > 0)
            return $get_tax_rate->row()->rate;
        else
            return 18;
    }
    
    /*
     * Fetch all the categories
     * */
    public function get_all_product_categories($where)
    {
        
        $this->db->select("*");
        $this->db->where($where);
        $category = $this->db->get( $this->db->dbprefix("categories") );
        if ($category->num_rows() > 0) {
            $rows    = $category->result_array();
            $catlist = array();
            foreach ($rows as $row) {
                $tmpcat['cat_id']   = $row['id'];
                $tmpcat['cat_code'] = $row['code'];
                $tmpcat['cat_name'] = $row['name'];
                $catlist[]          = $tmpcat;
            }
            return $catlist;
        }
    }
    
    /*
     * Fetch all the purchase Items
     * */
    public function get_all_purchase_items($where, $where_in = false)    {
        $this->db->select("purchase_items.id,purchase_items.batch,
                                purchase_items.quantity,purchase_items.expiry,purchase_items.warehouse_id,
                                purchase_items.net_unit_cost,purchase_items.product_id,purchase_items.id,purchase_items.dev_timestamp");
        if ($where_in)
            $this->db->where_in('purchase_items.warehouse_id', $where_in);
        $this->db->where($where);
        $this->db->join('products', 'products.id = purchase_items.product_id');
        $this->db->group_by('purchase_items.id');
        $category = $this->db->get("purchase_items");
       // echo $this->db->last_query();exit;
        if ($category->num_rows() > 0) {
            $rows    = $category->result_array();
            $catlist = array();
            foreach ($rows as $row) {
                
                // $tmpcat['product'] = $row['product_name'];
                $tmpcat['batch']            = $row['batch'];

               // if (array_key_exists('last_update_time', $where)) {
                    $wh_qty  =  $this->getProductQuantity($row['product_id'], $row['warehouse_id']);
                    $tmpcat['product_qty']   =  $wh_qty['quantity'];
                /*}
                else {
                    $tmpcat['product_qty'] = $row['quantity'];
                }*/
                $tmpcat['expiry']           = $row['expiry'];
                $tmpcat['warehouse']        = $row['warehouse_id'];
                //$tmpcat['name'] = $row['product_name'];
                $tmpcat['unit_purch_price'] = $row['net_unit_cost'];
                //$tmpcat['product_qty'] = $row['quantity_balance'];             
                //$tmpcat['last_update'] = $row['last_update_time'];
                $tmpcat['prod_id']          = $row['product_id'];
                $tmpcat['sys_purchase_id']  = $row['id'];
                $tmpcat['timestamp']  = $row['dev_timestamp'];
                
                $catlist[] = $tmpcat;
                //}
            }
            // var_dump( $productlist );exit;
            return $catlist;
        }
    }
    
    /*
     * Fetch all the Customers
     * */
    public function get_all_customers($data)
    {
        $this->db->where($data);
        $this->db->where(array(
            'group_name' => "customer"
        ));
        $q = $this->db->get( $this->db->dbprefix('companies') );
        if ($q->num_rows() > 0) {
            $customers = array();
            //var_dump($q->result_array());
            foreach ($q->result_array() as $row) {
                $customers[] = array(
                    'system_customer_id' => (int) $row['id'],
                    'customer_name' => $row['name'],
                    'business_name' => $row['company'],
                    'mobile' => $row['phone'],
                    'email' => $row['email'],
                    'street' => $row['address'],
                    'city' => $row['city'],
                    'lat' => $row['lattd'],
                    'lng' => $row['lontd'],
                    'area' => $row['state'],
                    'tin' =>$row['tin'],
                    'vrn'=>$row['vrn']
                );
            }
            return $customers;
        }
    }
    
    /*
     * Fetch all the Customers
     * */
    public function getCustomerNameById($customer_id)
    {
        
        $this->db->where(array(
            'id' => $customer_id
        ));
        $q = $this->db->get( $this->db->dbprefix('companies') );
        if ($q->num_rows() > 0) {
            $row = $q->row();
            return $row->name;
        } else
            return false;
        
    }
    public function userNameById($id , $device){
        $this->db->select("CONCAT(`first_name`,' ',`last_name`,' ( ', ".$device." , ' ) ') as NAME");
        $this->db->where( array( 'id' => $id ) );
        $query = $this->db->get( $this->db->dbprefix("users") );
        if($query -> num_rows() )
            return $query->row()->NAME;
        else
            return "N/A";

    }

    public function save_sales($data, $where)    {
        
        $flag = array();
        foreach ($data as $value) {
            $biller       = $this->getDefaultBiller(array(
                'account_id' => $where['account_id']
            ));
            $warehouse    = $this->getDefaultWarehouseByUser($where['user_id']);
            $existsSaleId = $this->checkSaleExists($value['sale_refid'], $where['device_imei']);
            if ($existsSaleId == false) {
                if ($customer_name = $this->getCustomerNameById($value['customer_id'])) {
                    $created_name = $this->userNameById( $where['user_id'], $where['device_imei'] );
                    $salesdata = array(
                        /*'date' => date('Y-m-d H:i:s'),*/
                        'reference_no' => $value['sale_refid'],
                        'customer_id' => $value['customer_id'],
                        'customer' => $customer_name,
                        'biller_id' => $biller['id'],
                        'biller' => $biller['name'],
                        'warehouse_id' => $warehouse,
                        'note' => 'N/A',
                        'staff_note' => $value['note'],
                        'total' => $value['total_sales'],
                        'grand_total' => $value['grand_total'],
                        'total_tax' => $value['total_tax'],
                        'product_tax' => $value['total_tax'],
                        'created_by' => $where['user_id'],
                        'created_name' => $created_name,
                        'lat' => $value['lat'],
                        'lng' => $value['lng'],
                        'total_items' => $value['total_items'],
                        'paid' => $value['paid'],
                        'account_id' => $where['account_id'],
                        'device_imei' => $where['device_imei'],
                        'is_printed' => $value['is_printed'],
                        'tax_setting' => $value['tax_setting'],
                        'payment_status'=>$value['payment_status']

                    );
                    
                    $insert           = $this->db->insert('sales', $salesdata);
                    //echo $this->db->last_query();exit;
                    $sys_sales_ins_id = $this->db->insert_id();
                    
                    if ($insert && $sys_sales_ins_id) {
                        $ret[] = array(
                            '_id' => $value['_id'],
                            'sy_sale_id' => $sys_sales_ins_id,
                            'status' => 1,
                            'msg' => 'Success.'
                        );
                        //Add payment


                        $salePayment = array('sale_id'=> $sys_sales_ins_id, 'account_id'=>$where['account_id'],
                                             'user_id'=>$where['user_id'], 'amount'=>$value['grand_total'],
                            'reference_no'=>$this->getReference('IPAY'));

                        $this->addSalePayment( $salePayment );
                        //echo $this->db->last_query();exit;



                    }
                    else
                        $ret[] = array(
                            '_id' => $value['_id'],
                            'sy_sale_id' => 0,
                            'status' => 0,
                            'msg' => 'Operation failed.'
                        );
                } else //Customer Check
                    $ret[] = array(
                        '_id' => $value['_id'],
                        'sy_sale_id' => 0,
                        'status' => 0,
                        'msg' => 'Customer Id not found on System.'
                    );
                
            } //Check Sale Exists
            else
                $ret[] = array(
                    '_id' => $value['_id'],
                    'sy_sale_id' => $existsSaleId,
                    'status' => 0,
                    'msg' => 'Sale already Exists.'
                );
            
            
        } // End Foreach
        
        return $ret;
    }

    public function addSalePayment( $data ){

        $payment = array(
            'date' => date('Y-m-d H:i:s'),
            'reference_no' => $data['reference_no'],
            'amount' =>  $data['amount'] ,
            'paid_by' => 'other',
            'sale_id' => $data['sale_id'],
            /*'cheque_no' => $this->input->post('cheque_no'),
            'cc_no' => $this->input->post('pcc_no'),
            'cc_holder' => $this->input->post('pcc_holder'),
            'cc_month' => $this->input->post('pcc_month'),
            'cc_year' => $this->input->post('pcc_year'),*/
            'cc_type' => 'Visa',
            'created_by' => $data['user_id'],
            'account_id' => $data['account_id'],
            'note' => 'N/A',
            'type' => 'received'
        );

        $this->db->insert( $this->db->dbprefix('payments'),$payment);

    }
    public function registration($imei)
    {
        $query = $this->db->query(" SELECT ua.active,ua.acc_type,u.company,d.device_imei AS imei,CONCAT(u.first_name,' ', u.last_name) AS c_person,u.phone AS mobile
                                    FROM sma_users u, sma_devices d, sma_users ua
                                    WHERE u.id = d.assigned_to and d.device_imei = '{$imei}' AND ua.id = u.account_id ");
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return 0;
        }
        
    }
    
    function save_sale_items($data, $where){

            foreach ($data as $value) {
                /*
                 $sale_id = $this->checkSaleItemExists( array('sale_refid'=>$value['sale_refid'], 
                                    'device_imei'=>$where['device_imei'],'dev_timestamp'=>$value['timestamp']) );
                */
                $this->updateLastSyncStatuses( array('device_imei'=>$where['device_imei']), array('purchase_items'=>0) );
                //echo $this->db->last_query();exit;
                if($value['wh'] > 0) {
                    $sale_id = $this->checkSaleExists($value['sale_refid'], $where['device_imei']);
                    if ($sale_id) {
                        $product_details = $this->checkProduct($value['prod_system_id']);
                        //echo $product_details;exit;
                        if ($product_details) {
                            $insert_data =
                                array(/*'id'=>$value[''],*/
                                    'sale_id' => $sale_id, 'account_id' => $where['account_id'], 'product_id' => $value['prod_system_id'],
                                    'product_code' => $product_details->code, 'product_name' => $product_details->name, 'product_type' => $product_details->type,
                                    'net_unit_price' => $value['net_unit_price'], 'unit_price' => $value['unit_price'], 'quantity' => $value['prod_order_qty'],
                                    'warehouse_id' => $value['wh'], 'dev_timestamp' => $value['timestamp'], 'sale_refid' => $value['sale_refid'],
                                    'device_imei' => $where['device_imei'], 'subtotal' => $value['subtotal'], 'tax_amount' => $value['tax_amount'],
                                    'discounted_price' => $value['discounted_price'], 'tax' => $value['tax_rate'], 'item_tax' => $value['item_tax']);
                            //var_dump($this->checkSaleItemExists( array('sale_refid'=>$value['sale_refid'],'device_imei'=>$where['device_imei'],'dev_timestamp'=>$value['timestamp']) ));exit;
                            $sale_item_id_exists = $this->checkSaleItemExists(array('sale_refid' => $value['sale_refid'], 'device_imei' => $where['device_imei'],
                                'dev_timestamp' => $value['timestamp']));
                            if (!$sale_item_id_exists) {
                                $inserted = $this->db->insert($this->db->dbprefix('sale_items'), $insert_data);
                                // echo $this->db->last_query();exit;
                                $ins_id_saleitems = $this->db->insert_id();
                                if ($inserted && $ins_id_saleitems) {

                                    $current_qty = $product_details->quantity;
                                    $sale_qty = $value['prod_order_qty'];
                                    $qty_to_deduct = $current_qty - $sale_qty;
                                    $this->db->where(array('id' => $value['prod_system_id']));
                                    $this->db->update($this->db->dbprefix('products'), array('quantity' => $qty_to_deduct));



                                    $this->syncQuantitySale( $value['prod_system_id'],  $value['wh'], $sale_qty);



                                    $ret[] = array('order_id' => $value['order_id'], 'sys_sales_item_id' => $ins_id_saleitems, 'success' => 1, 'msg' => 'Success.');
                                } else {
                                    //    $ret[] = array();
                                    $ret[] = array_merge(array('order_id' => 0, 'sys_sales_item_id' => 0, 'failed' => 1), $this->MESSAGE_CODE[20]);
                                }
                            } else {
                                //  $ret[] = array();
                                $ret[] = array_merge(array('order_id' => $value['order_id'], 'sys_sales_item_id' => $sale_item_id_exists), $this->MESSAGE_CODE[18]);
                            }
                        } else {
                            //$ret[] = array( );
                            $ret[] = array_merge(array('order_id' => 0, 'sys_sales_item_id' => 0), $this->MESSAGE_CODE[17]);
                        } // End CheckProduct
                    } else {
                        //$ret[] = array( );
                        $ret[] = array_merge(array('order_id' => 0, 'sys_sales_item_id' => 0), $this->MESSAGE_CODE[21]);
                    }
                }
                else {
                    //$ret[] = array( );
                    $ret[] = array_merge(array('order_id' => 0, 'sys_sales_item_id' => 0), $this->MESSAGE_CODE[22]);
                }

                }//End Foreach    

                return $ret;

    }

 function copySaleToReturn( $sale_id, $user_id ){
     $this->db->where(array('reference_no' => $sale_id));
     $query = $this->db->get('sales');
     //echo $this->db->last_query();
     if( $query->num_rows() > 0 ) {
         $sale =  $query->row() ;
         //var_dump( $sale_row->id );exit;
         $date = date('Y-m-d H:i:s');

         $data = array( 'date' => $date, 'sale_id' => $sale->id, 'reference_no' => $this->getReference('RETURNSL'),
             'customer_id' => $sale->customer_id, 'customer' => $sale->customer, 'biller_id' => $sale->biller_id,
             'biller' => $sale->biller, 'warehouse_id' => $sale->warehouse_id, 'note' => 'N/A', 'total' => $sale->total,
             'product_discount' => $sale->product_discount, 'order_discount_id' => $sale->order_discount_id,
             'order_discount' => $sale->order_discount, 'total_discount' => $sale->total_discount, 'product_tax' => $sale->product_tax,
             'order_tax_id' => $sale->order_tax_id, 'order_tax' => $sale->order_tax, 'total_tax' => $sale->total_tax,
             'surcharge' => 0, 'grand_total' => $sale->grand_total, 'created_by' => $user_id
         );

        $this->db->insert( 'return_sales', $data );

       // var_dump( $data ); exit;
     }

    // echo $this->db->last_query();
     //exit;
     return true;

 }

 function save_return_items($data, $where){

        //echo "Here..";exit;

        $insert_return = true;

        $return_id = false;

        foreach ( $data as $value ) {

            $sale_id = $value['sale_refid'];

            $sale_exists = $this->checkReturnSaleExists( $sale_id );

            if( $insert_return && !$sale_exists ) {
                $this->copySaleToReturn( $sale_id, $where['user_id'] );
                $return_id = $this->db->insert_id();
                $insert_return =false;
            }

            if( ($value['prod_order_qty'] != $value['prod_order_qty_delivered']) && $return_id ){
                $product_details = $this->checkProduct( $value['prod_system_id'] );

                $ret_qty = ( $value['prod_order_qty'] - $value['prod_order_qty_delivered'] );
                //$value['prod_order_qty'] = $value['prod_order_qty_delivered'];
                $value['return_id'] = $return_id;
                $value['sale_item_id'] = $value['sale_item_id'];
                $value['warehouse_id'] = $value['wh'];
                $value['quantity'] = $ret_qty;
                $value['discount'] = $value['discounted_price'];
                $value['net_unit_price'] = $value['unit_price'];
                $value['tax'] = $value['tax_amount'];
                $value['account_id'] = $where['account_id'];




                $value['product_id'] = $value['prod_system_id'];
                $value['product_code'] = $product_details->code;
                $value['product_name'] = $product_details->name;
                $value['product_type'] = $product_details->type;

                $order_id = $value['order_id'];
                unset($value['id'], $value['delivery_status'], $value['prod_order_qty'], $value['prod_order_qty_delivered'],$value['prod_system_id'],
                    $value['wh'],$value['sale_refid'],$value['discounted_price'],$value['order_id'],$value['unit_price'],$value['timestamp'],$value['_id']
                    ,$value['tax_amount'],$value['local_system_id'],$value['tax_rate']);

               // var_dump( $value ); exit;

                $this->db->insert('return_items', $value);

                $this->db->query("UPDATE sma_sale_items SET delivery_status='completed' WHERE id = ".$value['sale_item_id']);

                $ret[] = array( 'order_id' => $order_id, 'success' => 1, 'msg' => 'Success.','sys_sales_item_id'=>$value['sale_item_id'] );

               // echo $this->db->last_query();exit;
            }
            else
                $ret[] = array( 'order_id' => 0, 'error' => 1, 'msg' => 'Failed.','sys_sales_item_id'=>$value['sale_item_id'] );




        }//End Foreach

     $this->db->query("UPDATE sma_sales SET sale_status='completed' WHERE id = ".$sale_id );
        return $ret;

    }

    function get_all_sale_items($where){

        $this->db->select('*');
        $this->db->where($where);
        $salequery = $this->db->get( $this->db->dbprefix("sale_items") );
        //echo $this->db->last_query();exit;
        //echo $this->db->last_query();
        //echo $this->db->last_query();exit;
        if ($salequery->num_rows() > 0) {
            $rows        = $salequery->result_array();
            $saleslist = array();

            /*  id, sale_id, account_id, product_id, product_code, product_name, product_type, option_id, net_unit_price, unit_price, quantity, warehouse_id,
                item_tax, tax_rate_id, tax, discount, item_discount, subtotal, serial_no, dev_timestamp, sale_refid, device_imei, id, id
            */

            foreach ($rows as $row) {
                $tmp["sale_item_id"]= $row['id'];
                $tmp["sy_sale_id"]= $row['sale_id'];
                $tmp["wh"] =  $row['warehouse_id'];
                $tmp["prod_system_id"] =  $row['product_id'];
                $tmp["prod_order_qty"] =  $row['quantity'];
                $tmp["sale_refid"] =  $row['sale_refid'];
                $tmp["discounted_price"] =  $row['discounted_price'];
                $tmp["subtotal"] =  $row['subtotal'];
               // $tmp["order_id"] =  $row['id'];
                $tmp["unit_price"] =  $row['unit_price'];
                $tmp["net_unit_price"] =  $row['net_unit_price'];
                $tmp["timestamp"] =  $row['dev_timestamp'];
                //$tmp["_id"] =  $row['id'];
                $tmp["tax_amount"] =  ($row['tax_amount'] == null ) ? 0 :$row['tax_amount'];
               // $tmp["local_system_id"] =  $row['id'];
                $tmp["tax_rate"] =  $row['tax'];
                $tmp["item_tax"]= ($row['item_tax'] == null ) ? 0 :$row['item_tax'];
                $tmp["delivery_status"]= $row['delivery_status'];

                $saleslist[] = $tmp;
            }
            return $saleslist;
        } else
            return array();

    }
    public function checkPurchaseExists( $reference_no ,$user_id)
    {
        $this->db->select('id');
        $query = $this->db->get_where($this->db->dbprefix('purchases'), array(
            'reference_no' => $reference_no,
            'created_by' => $user_id
        ));
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0)
            return $query->row()->id;
        else
            return false;
    }

    public function save_purchase_items($data, $where)    {
        

        $created_on = date('Y-m-d h:i:s');
        
        $flag = array();
        
        $ret           = array();

        $supplier_info = $this->getDefaultSupplier(array('account_id' => $where['account_id']));


        $sub_tot         = 0;
        foreach ($data as $value) {
            $sys_purchass_ins_id = false;
            $purchase_ins_id = $this->checkPurchaseExists( $value['batch'], $where['user_id'] );
            $sup_id = ( $supplier_info) ? $supplier_info->id : 0;
            $sup_name = ( $supplier_info) ? $supplier_info->company : 'N/A';
            if(!$purchase_ins_id) {
                $this->db->insert( $this->db->dbprefix('purchases'), array(
                    'reference_no' => $data[0]['batch'],
                    'date' => date('Y-m-d h:i:s'),
                    'supplier_id' => $sup_id,
                    'supplier' => $sup_name,
                    'warehouse_id' => $data[0]['warehouse'],
                    'created_by' => $where['user_id'],
                    'account_id' => $where['account_id'],
                    'device_origion' => 1
                ));
               // echo $this->db->last_query();exit;
                if(!$purchase_ins_id)
                $purchase_ins_id = $this->db->insert_id();
            }

            $prod_details = $this->checkProduct($value['prod_id']);
            $product_qty  = $this->checkQuantity($value['prod_id']);
            if ($prod_details) {
                $sub_tot += ($value['product_qty'] * $value['unit_purch_price']);
                $purcItemsdata = array(
                    'purchase_id' => $purchase_ins_id,
                    'product_code' => $prod_details->code,
                    'product_id' => $value['prod_id'],
                    'product_name' => $prod_details->name,
                    'warehouse_id' => $value['warehouse'],
                    'quantity' => $value['product_qty'],
                    'batch' => $value['batch'],
                    'expiry' => $value['expiry'],
                    'net_unit_cost' => $value['unit_purch_price'],
                    'date' => $created_on,
                    'subtotal' => $sub_tot,
                    'quantity_balance' => $value['product_qty'],
                    'status' => 'received',
                    'account_id' => $where['account_id'],
                    'device_origion' => 1,
                    'dev_timestamp' => $value['timestamp']
                );
                //var_dump($value);
                $purchase_item_id_exists = $this->checkPurchaseItemExists( array( 'purchase_id' =>$purchase_ins_id,'dev_timestamp'=>$value['timestamp']) );
                //echo $this->db->last_query();exit;
                if(!$purchase_item_id_exists) {
                    $inserted = $this->db->insert($this->db->dbprefix('purchase_items'), $purcItemsdata);
                    //$this->db->insert_id();exit;
                    // Update Product Quantity
                    $sys_purchass_ins_id = $this->db->insert_id();

                   // $qtyToUpdate = ((int)$value['product_qty'] + (int)$product_qty);

                    if ( $inserted ) {
                       /* $this->db->where('id', $value['prod_id']);
                        $this->db->update('products', array(
                            'quantity' => $qtyToUpdate
                        ));*/

                       // $this->syncProductQty($value['prod_id']);

                        //$this->addQuantity($value['prod_id'], $value['warehouse'], $value['product_qty'] );
                        $this->updateProductQuantity($value['prod_id'], $value['product_qty'], $value['warehouse'],  $value['unit_purch_price'] );
                       // echo $this->db->last_query();exit;

                        //echo $this->db->last_query();exit;

                    }
                }// If purchase Exists
                else
                    $ret[] = array(
                        'prod_id' => $value['prod_id'],
                        'sys_purchase_id' => $purchase_item_id_exists,
                        'failed' => 1,
                        'msg' => 'Purchase item already exists'
                    );

                // Response to the Caller
                if ($sys_purchass_ins_id > 0)
                    $ret[] = array(
                        'prod_id' => $value['prod_id'],
                        'sys_purchase_id' => $sys_purchass_ins_id,
                        'status' => 1,
                        'msg' => 'Success'
                    );

                
                
            }
            else {
                $ret[] = array(
                    'prod_id' => $value['prod_id'],
                    'sys_purchase_id' => 0,
                    'failed' => 1,
                    'msg' => 'No product found for the supplied ID.'
                );
            }
            
        }
        
        if ($sub_tot > 0) {
            $this->db->where(array(
                'id' => $purchase_ins_id
            ));
            $this->db->update($this->db->dbprefix('purchases'), array(
                'total' => $sub_tot

            ));
            
        }
//        exit;
        return $ret;
        
    }
    
    public function checkQuantity($product_id)
    {
        $this->db->select('quantity');
        $query = $this->db->get_where( $this->db->dbprefix('products'), array(
            'id' => $product_id
        ));
        if ($query->num_rows() > 0)
            return $query->row()->quantity;
        else
            return 0;
    }
    
    public function getDefaultBiller($where)
    {
        $this->db->select('*');
        $query = $this->db->get_where( $this->db->dbprefix('companies'), array_merge($where, array(
            'group_name' => 'biller'
        )));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return array(
                'id' => $row->id,
                'name' => $row->name
            );
        } else {
            
            $this->db->insert( $this->db->dbprefix('companies'), array(
                'group_name' => 'biller',
                'name' => 'Default Biller',
                'company' => 'Default Biller Company',
                'account_id' => $where['account_id']
            ));
            return array(
                'id' => $this->db->insert_id(),
                'name' => 'Default Biller'
            );
        }
    }
    
    public function getDefaultSupplier($where)
    {
        $this->db->select('*');
        $query = $this->db->get_where( $this->db->dbprefix('companies'), array_merge($where, array(
            'group_name' => 'supplier'
        )));
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return false;
    }
    
    
    public function save_categories($data, $where)
    {
        //$this->db->trans_start();        
        //var_dump($data);exit;
        $created_on = date('Y-m-d h:i:s');
        // $emp = $this->get_assigned_emp( array('device_imei' => $where['device_imei']) );   
        //var_dump( $data );     
        // if( $emp ){
        $flag       = array();
        //$this->db->trans_start();
        //var_dump($data);exit;
        $ret        = array();
        foreach ($data as $value) {
            $cat_details = $this->checkCategory($value['cat_code']);
            //var_dump($cat_details);exit;                          
            if ($cat_details == false) {
                //var_dump($prod_details);exit;
                $purcItemsdata = array(
                    'code' => $value['cat_code'],
                    'name' => $value['cat_name'],
                    'account_id' => $where['account_id'],
                    'device_origion' => 1
                );
                
                $inserted  = $this->db->insert( $this->db->dbprefix('categories'), $purcItemsdata);
                $insert_id = $this->db->insert_id();
                $ret[]     = array(
                    '_id' => $value['_id'],
                    'cat_id' => $insert_id,
                    'status' => 1,
                    'msg' => 'Success'
                );
            } else
                $ret[] = array(
                    '_id' => $value['_id'],
                    'cat_id' => $cat_details,
                    'status' => 0,
                    'msg' => 'Already Exists.'
                );
        }
        //var_dump($ret);exit;
        return $ret;
    }

    function checkTracking( $where ){
        $this->db->where( $where );
        $resultset = $this->db->get( $this->db->dbprefix('device_tracking') );
        //var_dump($this->db->last_query());exit;
        if($resultset->num_rows() > 0){
            return $resultset->row()->id;
        }
        else
            return false;
    }
    public function device_tracking($data, $where)    {

        $created_on = date('Y-m-d h:i:s');
        $flag       = array();
        $ret        = array();

           //var_dump( $data );exit;
        foreach ($data as $value) {

            $trackers_exists = $this->checkTracking( array('device_imei'=>$where['device_imei'], 'tracker_id'=>$value['tracker_id']) );
             //$this->checkCategory( $value['cat_code'] );

            $trackers = array( 'lat' => $value['lat'], 'lng' => $value['lng'],'device_date' => $value['date'],
                'tracker_id'=>$value['tracker_id'], 'device_imei'=>$where['device_imei'], 'account_id'=>$where['account_id']);

            if ( $trackers_exists == false ) {
                $inserted  = $this->db->insert('device_tracking', $trackers);

                $insert_id = $this->db->insert_id();
                $ret[]     = array(
                    'tracker_id' => $value['tracker_id'],
                    'server_id' => $insert_id,
                    'status' => 1,
                    'msg' => 'Inserted' );
            } else {
                $this->db->where(array('id'=>$trackers_exists));
                $this->db->update( $this->db->dbprefix('device_tracking'), $trackers);
                $ret[] = array(
                    'tracker_id' => $value['tracker_id'],
                    'server_id' => $trackers_exists,
                    'status' => 1,
                    'msg' => 'Updated');
            }
        }
        //var_dump($ret);exit;
        return $ret;
    }

    function checkWarehouse($wh_id, $where)
    {
        $this->db->select('*');
        $this->db->where(array(
            'warehouses.id' => $wh_id,
            'warehouses.account_id' => $where['account_id'],
            'devices.device_imei' => $where['device_imei']
        ));
        $this->db->from( $this->db->dbprefix('warehouses') );
        $this->db->join('warehouse_map', 'warehouses.id = warehouse_map.warehouse_id');
        $this->db->join('devices', 'devices.account_id = warehouses.account_id');
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0)
            return true;
        else
            return false;
    }
    function checkProduct($product_id)
    {
        $this->db->select('*');
        $this->db->where(array('id' => $product_id ));
        $query = $this->db->get( $this->db->dbprefix('products') );

        if ($query->num_rows() > 0) {
            return $query->row();
        } else
            return false;
        
    }

 /*function checkSale($referenece_no)
    {
        $this->db->select('*');
        $this->db->where( array('referenece_no' => $referenece_no));
        $query = $this->db->get('sales');
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->row();
        } else
            return false;
        
    }
*/
    
    function checkCategory($cat_code)    {
        $this->db->select('*');
        $this->db->where(array(
            'code' => $cat_code
        ));
        $query = $this->db->get( $this->db->dbprefix('categories') );
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else
            return false;
        
    }

    function getWarehouseByDevice($device_imei, $where)    {
        //echo $device_imei;exit;
        if( $where )
            $sql = "SELECT sw.id,sw.name,sw.code,sw.address FROM sma_warehouse_map wm, sma_devices sd, sma_warehouses sw
                                        WHERE
                                        wm.user_id = sd.assigned_to AND
                                        sw.id = wm.warehouse_id AND
                                        sd.device_imei = '$device_imei' " . $where;
        else
            $sql = "SELECT sw.id,sw.name,sw.code,sw.address FROM sma_warehouse_map wm, sma_devices sd, sma_warehouses sw
                                        WHERE
                                        wm.user_id = sd.assigned_to AND
                                        sw.id = wm.warehouse_id AND
                                        sd.device_imei = '$device_imei'";

        $query = $this->db->query($sql);
        //echo $this->db->last_query();exit;
        $data  = array();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $address               = $this->clear_tags($row->address);
                //var_dump($address);exit;
                $datatmp['wh_id']      = $row->id;
                $datatmp['wh_name']    = $row->name;
                $datatmp['wh_address'] = $address; //strip_tags($row->address, '<p><p/>');
                $datatmp['wh_code']    = $row->code;
                $data[]                = $datatmp;
            }
            return $data;
        } else {
            return array();
        }
        
    }

    function getAccount_settings( $account_where )    {
       // var_dump($account_id);exit;
        $this->db->select("id,enable_tax, enable_discount, print_receipt, export_db, import_db, allow_product_editing, max_discount_per,
                            allow_stock_receiving, allow_customer_adding, allow_customer_adding,allow_customer_editing,track_time_interval, allow_product_adding, application_type,
                             allow_product_sharing,comission_rate,allow_post_paid");
        $this->db->where( $account_where );
        $query = $this->db->get("account_settings");
        //echo $this->db->last_query();exit;
        $data  = array();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return array();
        }

    }

    
    function getWarehouseByUser($user_id)
    {
        //echo $device_imei;exit;
        $query = $this->db->query("SELECT * FROM sma_warehouse_map WHERE user_id = {$user_id}");
        //echo $this->db->last_query();exit;
        $data  = array();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row->warehouse_id;
            }
            return $data;
        } else {
            return 0;
        }
        
    }
    
    function getDefaultWarehouseByUser($user_id)
    {
        //echo $device_imei;exit;
        $query = $this->db->query("SELECT * FROM sma_warehouse_map WHERE user_id = {$user_id}");
        //echo $this->db->last_query();exit;
        $data  = array();
        if ($query->num_rows() > 0) {
            return $row = $query->row()->warehouse_id;
        } else {
            return 0;
        }
        
    }
    /*
     * Update - Update Time on Records :: Unused
     * */
    public function sync_last_update_time($params, $lastsync)
    {
        
        $ack      = $lastsync[$params['tag']];
        $lasttime = $lastsync[$params['tag'] . "_synctime"];
        
        $lastwhere = array(
            'device_imei' => $params['device_id'],
            'account_id' => $params['account_id'],
            'last_update_time >' => $lasttime
        );
        
        if ($ack == 0) {
            $this->get_all_users($lastwhere); //all users
        } else if ($ack == 1) {
            $this->get_all_users_updates($lastwhere); // updates and new if any
        }
    }
    /*
     * update the statuses of the devices' entities
     * */
    public function updateLastSyncStatuses($where, $data)
    {
        $this->db->update( $this->db->dbprefix('device_sync_status'), $data, $where);
        if ($this->db->affected_rows()) {
            return true;
        }
    }
    // From Site Model
    public function getWarehouseProducts($product_id) {
        $q = $this->db->get_where($this->db->dbprefix('warehouses_products'), array('product_id' => $product_id));
        if($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncProductQty($product_id) {
        $wh_prs = $this->getWarehouseProducts($product_id);
        //var_dump($wh_prs);exit;
        $qty = 0;
        foreach ($wh_prs as $row) {
            $qty += $row->quantity;
        }
        if($this->db->update($this->db->dbprefix('products'), array('quantity' => $qty), array('id' => $product_id))) {
            return TRUE;
        }
        return FALSE;
    }

    // Adjust Product Quantity

    public function addQuantity($product_id, $warehouse_id, $quantity) {
        //$this->getProductQuantity($product_id, $warehouse_id);
        //echo $this->db->last_query();exit;
        if($this->checkWarehouseProductQuantity($product_id, $warehouse_id)) {

            $warehouse_quantity = $this->getProductQuantity($product_id, $warehouse_id);
            $old_quantity = $warehouse_quantity['quantity'];
            $new_quantity = $old_quantity + $quantity;

            if($this->updateQuantity($product_id, $warehouse_id, $new_quantity)) {
              //  echo $this->db->last_query();exit;
                return TRUE;
            }
        } else {
          //$this->insertQuantity($product_id, $warehouse_id, $quantity);
         // echo $this->db->last_query();exit;
            if($this->insertQuantity($product_id, $warehouse_id, $quantity)) {
                return TRUE;
            }
        }
         // echo $this->db->last_query();exit;
        return FALSE;
    }

    public function getProductQuantity($product_id, $warehouse) {
        $q = $this->db->get_where($this->db->dbprefix('warehouses_products'), array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
       //echo $this->db->last_query();
        if($q->num_rows() > 0) {
         //   var_dump( $q->row_array() );exit;
            return $q->row_array(); //$q->row();
        }

        return array('quantity'=>0);
    }
    public function checkWarehouseProductQuantity($product_id, $warehouse) {
        $q = $this->db->get_where($this->db->dbprefix('warehouses_products'), array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
       //echo $this->db->last_query();
        if($q->num_rows() > 0) {
         //   var_dump( $q->row_array() );exit;
            return $q->row_array(); //$q->row();
        }

        return false;
    }

    public function updateQuantity($product_id, $warehouse_id, $quantity) {
        if($this->db->update($this->db->dbprefix('warehouses_products'), array('quantity' => $quantity), array('product_id' => $product_id, 'warehouse_id' => $warehouse_id))) {
            $this->syncProductQty($product_id);
            return true;
        }
        return false;
    }



    public function insertQuantity($product_id, $warehouse_id, $quantity) {
        $productData = array(
            'product_id' => $product_id,
            'warehouse_id' => $warehouse_id,
            'quantity' => $quantity
        );
        if($this->db->insert('warehouses_products', $productData)) {
            $this->syncProductQty($product_id);
            return true;
        }
        return false;
    }

    public function updateProductQuantity($product_id, $quantity, $warehouse_id, $product_cost) {
        if($this->addQuantity($product_id, $warehouse_id, $quantity)) {

            $this->syncProductQty($product_id);
            return true;
        }
        return false;
    }

    public function getReference($field) {
        $q = $this->db->get_where('order_ref', array('ref_id' => '1'), 1);
        if($q->num_rows() > 0) {
            $ref = $q->row();
            $prefix = $field;
            $ref_no = (! empty($prefix)) ? $prefix.'/' : '';

                $ref_no .=date('Y').'/'.date('m').'/'. $this->getRandomReference();


            return $ref_no;
        }
        return FALSE;
    }

    public function getRandomReference($len = 12) {

        $result = '';
        for($i = 0; $i < $len; $i++) {
            $result .= mt_rand(0, 9);
        }
        if($this->getSaleByReference($result)) {
            $this->getRandomReference();
        }
        return $result;
    }

    public function getSaleByReference($ref) {
        $this->db->like('reference_no', $ref, 'before');
        $q = $this->db->get($this->db->dbprefix('sales'), 1);
        if($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncQuantitySale($product_id, $warehouse_id, $quantity) {
        if($warehouse_quantity = $this->getProductQuantitySale($product_id, $warehouse_id)) {
            $new_quantity = $warehouse_quantity['quantity'] - $quantity;
            if($this->updateQuantitySale($product_id, $warehouse_id, $new_quantity)) {
                $this->syncProductQty($product_id);
                return TRUE;
            }
        } else {
            if($this->insertQuantitySale($product_id, $warehouse_id, -$quantity)) {
                $this->syncProductQtySale($product_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function insertQuantitySale($product_id, $warehouse_id, $quantity) {
        if($this->db->insert($this->db->dbprefix('warehouses_products'), array( 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity ))) {
            return true;
        }
        return false;
    }

    public function updateQuantitySale($product_id, $warehouse_id, $quantity) {
        if($this->db->update($this->db->dbprefix('warehouses_products'), array( 'quantity' => $quantity ), array('product_id' => $product_id, 'warehouse_id' => $warehouse_id))) {
            return true;
        }
        return false;
    }

    public function getProductQuantitySale($product_id, $warehouse) {
        $q = $this->db->get_where( $this->db->dbprefix('warehouses_products'), array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if($q->num_rows() > 0) {
            return $q->row_array(); //$q->row();
        }
        return FALSE;
    }

    public function getProductOptionsSale($product_id, $warehouse_id) {
        $this->db->select('product_variants.id as id, product_variants.name as name, product_variants.price as price, product_variants.quantity as total_quantity, warehouses_products_variants.quantity as quantity')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            ->where('product_variants.product_id', $product_id)
            ->where('warehouses_products_variants.warehouse_id', $warehouse_id)
            ->where('warehouses_products_variants.quantity >', 0)
            ->group_by('product_variants.id');
        $q = $this->db->get('product_variants');
        if($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductVariantsSale($product_id) {

        $q = $this->db->get_where( $this->db->dbprefix('product_variants'), array('product_id' => $product_id));
        if($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllDeliveries( $commons ) {

        $q = $this->db->get_where( $this->db->dbprefix( 'deliveries' ),$commons );
        if($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data['id'] = $this->clear_tags( $row->id );
                $data['sale_id'] = $this->clear_tags( $row->sale_id );
                $data['do_reference_no'] =  $row->do_reference_no ;
                $data['sale_reference_no'] = $row->sale_reference_no ;
                $data['customer'] = $row->customer;
                $data['customer_id'] = $row->customer_id;
                $data['address'] = $this->clear_tags( $row->address );
                $data['note'] = $this->clear_tags( $row->note );
                $data['date_delivered'] = $row->date_delivered;
                $data['date_to_deliver'] = $row->date_to_deliver;
                $data['date_update'] = $row->date_update;
                //$data['date_update'] = $row->date_update;
                $data['lat'] = $row->lat;
                $data['lng'] = $row->lng;
                $data['delivery_method'] = $row->delivery_method;
                $data['vehicle_no'] = $row->vehicle_no;
                $data['status'] = $row->status;

                $tmp[] = $data;
            }
            return $tmp;
        }
        return FALSE;
    }

    // Save the delivery received from the device
    public function upload_delivery($data, $where )    {
        $created_on = date('Y-m-d h:i:s');
        $flag       = array();
        $ret        = array();

        foreach ($data as $value) {
            $delivery_exists = $this->checkDelivery( array('do_reference_no'=>$value['do_reference_no']) );
            if ( $delivery_exists ) {
                $this->db->where(array( 'id'=> $delivery_exists ));
                $this->db->update( $this->db->dbprefix('deliveries'), array( 'status' => 'completed','date_delivered'=>$value['date_delivered']) );
                $this->db->update( $this->db->dbprefix('sales'), array( 'sale_status' => 'completed'),array('id'=>$value['sale_id']) );
                $this->db->update( $this->db->dbprefix('sale_items'), array( 'delivery_status' => 'completed'),array('sale_id'=>$value['sale_id']) );
                $ret[] = array(
                    'do_reference_no' => $value['do_reference_no'],
                    'server_id' => $delivery_exists,
                    'status' => 1,
                    'msg' => 'Updated');
            } else {
                $ret[] = array(
                    'do_reference_no' => 0,
                    'server_id' => 0,
                    'status' => 0,
                    'msg' => 'Nothing to update');
            }
        }
        return $ret;
    }

    // Check if the delivery already exists ??
    function checkDelivery( $where ){
        $this->db->where( $where );
        $resultset = $this->db->get( $this->db->dbprefix('deliveries') );
        //var_dump($this->db->last_query());exit;
        if($resultset->num_rows() > 0){
            return $resultset->row()->id;
        }
        else
            return false;
    }

    // Save the image file
    public function saveImageFrom($data,$general)    {

        $imgname = $data['name'];
        $target = 'images/photos/' . $general['device_imei'] . '/delivery';
        if (!file_exists($target)) {
            mkdir($target, 0755, true);
        }
        $filename = $general['device_imei'] . '_' . $imgname;
        $imsrc = base64_decode($data['content']);
        header('Content-Type: bitmap; charset=utf-8');
        $path = $target . '/' . $filename;

        $fp = fopen($path, 'wb');
        fwrite($fp, $imsrc);
        $size = filesize( $path );

        $f = finfo_open();
        $type = finfo_buffer($f, $imsrc, FILEINFO_MIME);
        $type = explode(";", $type);

        if (fclose($fp)) {
            return array('path' => $path, 'size' => $size, 'name' => $filename, 'type' => $type[0]);
        } else {
            return false;
        }

    }

    //  Update the received signature image path and Resize it to fit on delivery details signature section
    public function updateDeliverySignature($data, $save){

        $this->db->where(array( 'do_reference_no'=> $data['foreginkeyid'] ));
        $this->db->update( $this->db->dbprefix('deliveries'), array( 'sign_image' => $save['path']) );

        // Resize the signature
        $config['image_library'] = 'gd2';
        $config['source_image']	 = $save['path'];
        //$config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        //$config['width']	= 75;
        $config['height']	= 80;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
        return true;

    }
    // Clear HTMl tags from the string
    public function clear_tags($str) {

        $strip_tags = "";
        $clean_html = preg_replace("#<\s*\/?(".$strip_tags.")\s*[^>]*?>#im", '', $str);
        return $clean_html;

       /* return htmlentities(
            strip_tags($str,
                '<span><div><a><br><p><b><i><u><img><blockquote><small><ul><ol><li><hr><big><pre><code><strong><em><table><tr><td><th><tbody><thead><tfoot><h3><h4><h5><h6>'
            ),
            ENT_QUOTES | ENT_XHTML | ENT_HTML5,
            'UTF-8'
        );*/
    }
}