<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                	<a href="<?php echo site_url('employees'); ?>" style="float:right;margin: 10px;" type="button" class="btn btn-primary"><?php  echo lang('back')?> </a>
                                	</div>	
                                  	  	<div class="box-body">
							  
<div class="table-responsive">
<table class="table">
<tbody>
	
	 <?php
		foreach ($headers as $header) {
			foreach ($info as $data) { 
				if($header =='fullname'){ ?>
<tr><th><?php echo lang($header); ?></th><td><?php echo $data['firstname'] . ' ' . $data['middle_name'] . ' ' . $data['lastname']; ?> </td></tr>					
				<?php }else{ ?>
				<tr><th><?php echo lang($header)?></th><td><?php echo $data[$header]; ?></td></tr>
		<?php 	}
		}
		}
	 	?> 
	</div>
                         
                        </div>                        	
                        </div>	
                        </section>
                        		
			