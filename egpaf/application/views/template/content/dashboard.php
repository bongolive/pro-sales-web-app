  	<!-- Main content -->
	<section class="content">

		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3><?php echo $totals['today']; ?> </h3>
						<p>
							<?php echo lang('count_today') ?>
						</p>
					</div>
					<div class="icon">
							<i class="ion ion-stats-bars"></i>
					</div>
					<a href="#" class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i> </a>
				</div>
			</div><!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3><?php echo $totals['week']; ?> </h3>
						<p>
							<?php echo lang('this_week_count') ?>
						</p>
					</div>
					<div class="icon">
						<i class="ion ion-stats-bars"></i>
					</div>
					<a href="#" class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i> </a>
				</div>
			</div><!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3><?php echo $totals['month']; ?> </h3>
						<p>
							<?php echo lang('this_month_count') ?>
						</p>
					</div>
					<div class="icon">
						<i class="ion ion-stats-bars"></i>
					</div>
					<a href="#" class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i> </a>
				</div>
			</div><!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-red">
					<div class="inner">
						<h3><?php echo $totals['total']; ?> </h3>
						<p>
							<?php echo lang('total_count') ?>
						</p>
					</div>
					<div class="icon">
							<i class="ion ion-stats-bars"></i>
					</div>
					<a href="#" class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i> </a>
				</div>
			</div><!-- ./col -->
		</div><!-- /.row -->

		<!-- top row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">

			</div><!-- /.col -->
		</div>
		<!-- /.row -->

		<!-- Main row -->
		<div class="row">
			<!-- Left col -->
			<section class="col-lg-12 connectedSortable">
				<!-- Box (with bar chart) -->
				<div class="box box-danger" id="loading-example">
				  
 					<!-- Custom tabs (Charts with tabs)-->
					<div class="nav-tabs-custom">
						<!-- Tabs within a box -->
						<ul class="nav nav-tabs pull-right">
							<li class="active">
								<a href="#revenue-chart" data-toggle="tab">Area</a>
							</li>
							<!--<li>
								<a href="#sales-chart" data-toggle="tab">Donut</a>
							</li>-->
							<li class="pull-left header">
								<i class="fa fa-inbox"></i> Participants
							</li>
						</ul>
						<div class="tab-content no-padding">
							<!-- Morris chart - Sales -->
							<div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
							<div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
						</div>
					</div><!-- /.nav-tabs-custom -->
</section> 

			</section><!-- /.Left col -->

		</div><!-- /.row (main row) -->

	</section><!-- /.content -->
</aside><!-- /.right-side -->