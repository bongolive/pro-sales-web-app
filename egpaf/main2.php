<?php
require_once ('services2.php');

$services = new Services();

$data = file_get_contents('php://input');
$data = json_decode($data, true);
//$data = Array('response' => Array( Array('response' => '2015-01-14', 'participant_id' => 2, 'survey_no' => 1, 'ack' => 0, 'responseid' => 9, 'question_no' => 21, 'response_date' => '2015-02-17 13:29', 'choice_no' => 2, 'comments' => 'comments', 'prev_qn' => 1, 'response_id' => 1, )), 'tag' => 'response_data', 'imei' => '351663058738881', );

write_log($data);
echo '<pre>';
$tag = trim($data['tag']);

$my_data = array();

//check device status
if ($tag == 'participant_data' || $tag == 'response_data' || $tag == 'acktag') {

	$device = FALSE;

} else {

	$device = $services -> check_device($data);

}

//if true,read all data, else read oly updates
if (!$device) {

	/*
	 * STEP 1: REGISTER THE DEVICE THAT IS ATTEMPTING TO SYNC DATA
	 */
	$services -> syncing_device($data);

	//one time insertion
	//read device sync status
	$devstat = $services -> device_sync_status($data);

	if ($tag) {

		switch ($tag) {
			case 'user_data' :
				//check device status
				if ($devstat['users'] == 0) {
					//get users data
					$get_users = $services -> read_users($data);
					if ($get_users) {
						$my_data['success'] = 1;
						$my_data['user_data_array'] = $get_users;
					} else {
						$my_data['failure'] = 1;

					}
				} else {
					$my_data['failure'] = 1;
				}
				break;
			//------------ user data ends

			case 'response_data' :
				//saving participant responses

				$response = $services -> save_responses($data);
				if ($response) {
					$toreturn = array();
					foreach ($data['response'] as $key => $value) {
						$toreturn[] = array('participant_id' => $value['participant_id'], 'syc_date' => date('Y-m-d h:i:s'), 'response_id' => $value['response_id']);
					}

					$my_data['success'] = 1;
					$my_data['imei'] = $data['imei'];
					$my_data['response_data_array'] = array('imei' => $data['imei'], 'responses' => $toreturn);
				} else {
					$my_data['failure'] = 1;
				}

				break;

			//updating response data

			case 'response_update' :
				//saving participant responses

				$response = $services -> update_responses($data);
				if ($response) {
					$my_data['success'] = 1;
					$my_data['response_data_array'] = array('imei' => $data['imei'], 'response_id' => $data['response_id'], 'syc_date' => date('Y-m-d h:i:s'));
				} else {
					$my_data['failure'] = 1;
				}
				break;
			//-------------- user roles ends

			case 'questions_data' :
				//check device status
				if ($devstat['questions'] == 0) {
					//------------ get questions data
					$data = $services -> read_questions($data);

					if ($data) {

						$my_data['success'] = 1;
						$my_data['questions_data_array'] = $data;
					} else {
						$my_data['failure'] = 1;
					}
				} else {
					$my_data['failure'] = 1;
				}
				break;
			//------------ questions data end

			case 'response_choice_data' :
				if ($devstat['responses'] == 0) {
					//------------ get questions data
					$data = $services -> read_response_choices($data);

					if ($data) {
						$my_data['success'] = 1;
						$my_data['response_choices_array'] = $data;
					} else {
						$my_data['failure'] = 1;
					}
				} else {
					$my_data['failure'] = 1;
				}
				break;

			case 'settings_data' :
				if ($devstat['settings'] == 0) {
					//----------- get system settings

					$data = $services -> read_settings($data);
					if ($data) {

						$my_data['success'] = 1;
						$my_data['settings_data_array'] = $data;
					} else {
						$my_data['failure'] = 1;
					}
				} else {
					$my_data['failure'] = 1;
				}
				break;
			//------------ system settings ends

			case 'participant_data' :
				// ----- save the participants data

				$participant = $services -> save_participant($data);
				if ($participant) {
					$toreturn = array();
					foreach ($data['Participant'] as $key => $value) {
						$toreturn[] = array('participant_id' => $value['participant_id'], 'syc_date' => date('Y-m-d h:i:s'));
					}
					$my_data['success'] = 1;
					$my_data['imei'] = $data['imei'];
					$my_data['participant_data_array'] = array('participants' => $toreturn);
				} else {
					$my_data['failure'] = 1;
				}

				break;

			//update participants
			case 'participant_update' :
				// ----- save the participants data

				$participant = $services -> update_participant($data);
				if ($participant) {

					$my_data['success'] = 1;
					$my_data['participant_data_array'] = array('participant_id' => $data['participant_id']);
				} else {
					$my_data['failure'] = 1;
				}

				break;

			//get the surveys data
			case 'survey_data' :
				if ($devstat['surveys'] == 0) {
					//---------- save participants responces

					$surveys = $services -> read_surveys($data);

					if ($surveys) {
						$my_data['success'] = 1;
						$my_data['survey_data_array'] = $surveys;
					} else {
						$my_data['failed'] = '0';
					}
				} else {
					$my_data['failure'] = 1;
				}
				break;
			case 'acktag' :
				$services -> update_device_sync_status($data);
				$my_data['success'] = 1;
				break;

			default :
				$my_data['failure'] = '0';
				break;
		}

	} else {
		$my_data['failure'] = 'No Tag';
	}
} else {
	//get all updatede details basing on the requested tag
	$updated_data = array();

	switch ($tag) {
		case 'user_data' :
			//get users data

			$get_users = $services -> get_users_updates($device);

			if ($get_users) {
				$services -> unset_device_sync_status($data, 'users');

				$updated_data['success'] = 1;
				$updated_data['update_user_data_array'] = $get_users;
			} else {
				$my_data['failure'] = 1;
			}

			break;

		case 'settings_data' :
			//----------- get system settings

			$settings_data = $services -> settings_updates($data);
			if ($settings_data) {

				$services -> unset_device_sync_status($data, 'settings');
				$updated_data['success'] = 1;
				$updated_data['update_settings_data_array'] = $settings_data;
			} else {
				$my_data['failure'] = 1;
			}

			break;

		case 'questions_data' :
			//------------ get questions data

			$questions_updates = $services -> questions_updates($device);

			if ($questions_updates) {
				$services -> unset_device_sync_status($data, 'questions');
				$updated_data['success'] = 1;
				$updated_data['update_questions_data_array'] = $questions_updates;
			} else {
				$my_data['failure'] = 1;
			}

			break;

		case 'response_choice_data' :
			//------------ get questions data
			$responses_updates = $services -> get_responses_updates($device);

			if ($responses_updates) {
				$services -> unset_device_sync_status($data, 'responses');
				$updated_data['success'] = 1;
				$updated_data['update_response_choices_array'] = $responses_updates;

			} else {
				$my_data['failure'] = 1;
			}
			break;

		case 'survey_data' :

			//---------- save participants responces
			$survey_updates = $services -> get_survey_updates($device);
			if ($survey_updates) {
				$services -> unset_device_sync_status($data, 'surveys');
				$updated_data['success'] = 1;
				$updated_data['update_survey_data_array'] = $survey_updates;

			} else {
				$my_data['failure'] = 1;
			}

			break;
		default :
			$updated_data['failure'] = 'Tag Not Identified';
			break;
	}

	//return the values to update
	$my_data = $updated_data;
}

if ($my_data) {

	sending_data($my_data);
	write_log($my_data);

}

function sending_data($data) {

	header('Content-type : application/json');
	echo json_encode($data);
}

function write_log($data) {
	//	$data = json_encode($data);
	//$myfile = fopen("received_data.txt", "a") or die("Unable to open file!");

	file_put_contents("received_data2.txt", print_r($data, true), FILE_APPEND);

	//fclose($myfile);
}

/*
 * user these to generate dammy data
 */

//$services->demo_answers();
//$services->demo_participants();
