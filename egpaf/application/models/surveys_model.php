<?php
class Surveys_model extends MY_Model {
	public $table = 'surveys';
	public $table_id = 'survey_id';

	function __construct() {

		parent::__construct();

	}

	public function form_validation() {

		$this -> form_validation -> set_rules('survey_title', 'survey name', 'trim|required');

	}

	public function read_statistics($where = false) {
		$this -> db -> select('count(responder_id) as total');
		$this -> db -> from('responders');

		if ($where) {
			$this -> db -> where($where);
		}

		$data = $this -> db -> get() -> result_array();

		if (!$data) {
			$data = FALSE;
		}
		return $data;

	}

	public function statistics($where = false) {
		$this -> db -> select('count(responder_id) as total');
		$this -> db -> from('responders');
		$this -> db -> join('surveys', 'survey_  = survey_no','left');
		if ($where) {
			$this -> db -> where($where);
		}

		$data = $this -> db -> get() -> result_array();

		if (!$data) {
			$data = FALSE;
		}
		return $data;

	}

	public function read_responces($where = false) {

		$this -> db -> select('*');
		$this -> db -> from('responders');
		if ($where) {
			$this -> db -> where($where);
		}
		$this -> db -> join('facilities', 'facility_code  = facility_no','left');
		$this -> db -> join('surveys', 'survey_id  = survey_no','left');

		$this -> db -> limit(10);
		$data = $this -> db -> get() -> result_array();

		if (!$data) {
			$data = FALSE;
		}
		return $data;

	}

	public function read_my_responces($where = false) {

		$this -> db -> select('*');
		$this -> db -> from('responces');
		if ($where) {
			$this -> db -> where($where);
		}

		$data = $this -> db -> get() -> result_array();

		if (!$data) {
			$data = FALSE;
		}
		return $data;

	}


    /*
    * get all surveys to this user sna account
    * */
    public function api_get_all_surveys($data)
    {
        $this->db->where($data);

        $query = $this->db->get('pro_surveys');

        if($query->num_rows() > 0) {
            return $query->result_array();  // its usage should be $settings = $query['settings']
        }
    }
}
