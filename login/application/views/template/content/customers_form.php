				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							
							<h2><?php 
							if(!$info){
							_l('customer_details');
							}else{
								echo _l('customer_details').' : '. strtoupper($info['business_name']);
							}
							 ?></h2>
					   
            </header> 

        </article>

    </div>	
    <div class="row">
    	  
<?php  
if($info) {
		echo form_open('customers/update',array('class' => 'form-horizontal'));
		echo '<input type="hidden" name="customer_id" value="'. $info['customer_id'].'"/>';
		}else { 
 		echo form_open('customers/new_customer',array('class' => 'form-horizontal'));
 }
?>
<article class="span12 data-block">
								<div class="modal-header">
									<h4><?php _l('customer_details'); ?></h4>
								</div>
								<div class="modal-body">
  
 <div class="control-group">
	<span class="control-label"><?php _l('business_name');?></span> <?php echo form_error('business_name'); ?>
<div class="controls"><input type="text" name="business_name" value="<?php if($info){ echo $info['business_name'];}else{echo set_value('business_name');} ?>" size="50" /></div>
</div>
 <div class="control-group">
<span class="control-label"><?php _l('contact_person')?></span><?php echo form_error('contact_person'); ?>
<div class="controls"><input type="text" name="contact_person" value="<?php if($info){ echo $info['contact_person']; }else{  echo set_value('contact_person');} ?>" size="50" /></div>
</div>
<div class="control-group">
<span class="control-label"><?php _l('cp_title');?></span><?php echo form_error('cp_title'); ?>
<div class="controls"> 
	 <input type="text" name="cp_title" value="<?php if($info){ echo $info['cp_title']; }else{  echo set_value('cp_title');} ?>" size="50" />
	 </div>
	 
	</div>

<!--
<div class="control-group">
<span class="control-label"><?php _l('service_category');?></span><?php echo form_error('service_category'); ?>
<div class="controls"> 
	<?php if($info){ $bstype = $info['service_category']; }else{ $bstype =  set_value('service_category'); }
 /*$industry = array(
	'Agriculture & Forestry/Wildlife'=>'Agriculture & Forestry/Wildlife',
	'Food & Hospitality'=>'Food & Hospitality',
	'Health Services'=>'Health Services',
	'Natural Resources/Environmental'=>'Natural Resources/Environmental',
	'Transportation'=>'Transportation',
	'Business & Information'=>'Business & Information');
	echo form_dropdown('service_category',$industry,$bstype);*/
	
		$form = array('name' => 'service_category',  'value' => $bstype);
	 	echo form_input($form);
 ?>
</div></div>

 <div class="control-group">
<span class="control-label"><?php _l('service_type');?></span><?php echo form_error('service_type'); ?>
<div class="controls"> 
	<?php if($info){ $service = $info['service_type']; }else{ $service =  set_value('service_type'); }
 
	/*$services = array(
	'Farming(Animal Production)'=>'Farming(Animal Production)',
'Farming(Crop Production)'=>'Farming(Crop Production)',
'Retail Sales'=>'Retail Sales',
'Equipment Rental'=>'Equipment Rental',
'Restaurant/Bar'=>'Restaurant/Bar',
'Specialty Food(Fruit/Vegetables)'=>'Specialty Food(Fruit/Vegetables)',
'Specialty Food(Meat)'=>'Specialty Food(Meat)',
'Specialty Food(Seafood)'=>'Specialty Food(Seafood)',
'Tobacco Product Manufacturing'=>'Tobacco Product Manufacturing',
'Wholesale Drug Distribution'=>'Wholesale Drug Distribution',
'Truck Transportation(Fuel)'=>'Truck Transportation(Fuel)',
'Truck Transportation(Non Fuel)'=>'Truck Transportation(Non Fuel)',);
	echo form_dropdown('service_type',$services,$service);
		*/
		$form = array('name' => 'service_type',  'value' => $service);
	 	echo form_input($form);
	 
	?>
	</div>
</div> 
</div> 
</article>
<article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l('customer_details'); ?></h4>
								</div>
								<div class="modal-body">
-->
 <div class="control-group">
<span class="control-label"><?php _l('mobile');?></span><?php echo form_error('mobile'); ?>
<div class="controls"><input type="text" name="mobile" value="<?php if($info){ echo $info['mobile']; }else{ echo set_value('mobile'); }?>" size="50"  /></div>
</div> 

 <div class="control-group">
<span class="control-label"> <?php _l('email');?></span> <?php echo form_error('email'); ?>
<div class="controls"><input type="text" name="email" value="<?php if($info){ echo $info['email']; }else{ echo set_value('email'); } ?>" size="50" /> </div>
</div>
 <div class="control-group">
<span class="control-label"> <?php _l('area');?></span><?php echo form_error('area'); ?>
<div class="controls"> 
<input type="text" name="area" value="<?php if($info){ echo $info['area']; }else{ echo set_value('area'); } ?>" size="50" />
</div>
</div> 
 <div class="control-group">
<span class="control-label"> <?php _l('street');?></span><?php echo form_error('street'); ?>
<div class="controls"> 
<input type="text" name="street" value="<?php if($info){ echo $info['street']; }else{ echo set_value('street'); } ?>" size="50" />
</div>
</div>

 <div class="control-group">
<span class="control-label"> <?php _l('city');?></span><?php echo form_error('city'); ?>
<div class="controls"> 
<input type="text" name="city" value="<?php if($info){ echo $info['city']; }else{ echo set_value('city'); } ?>" size="50" />
</div>
</div> 
 
 <div class="control-group">
<span class="control-label"><?php _l('map_location');?></span><?php echo form_error('map_location'); ?>
<div class="controls">
<input type="text" class="input-medium"  name="longtd" placeholder="longitude" value="<?php if($info){ echo $info['longtd']; }else{ echo set_value('longtd');} ?>" size="50"  />
<input type="text" class="input-medium"  name="lat" placeholder="latitude" value="<?php if($info){ echo $info['lat']; }else{ echo set_value('lat');} ?>" size="50"  /></div>
</div> 

</div>
	<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('customers'); ?>" class="btn btn-alt" data-dismiss="modal">Back</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
 </article>
</section> 

