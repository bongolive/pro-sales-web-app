				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							
							<h2><?php 
							echo _l('customer_details') . ' : '  .ucfirst($customer['cp_title']).'. '. ucwords($customer['contact_person']).' - '. ucwords($customer['business_name']);
							 ?></h2>
							 <ul class="data-header-actions">
								<li><a href="<?php echo site_url('customers'); ?>"><?php _l('back_to_customers');?></a></li> 
								</ul>
							 
					   
            </header> 

        </article>

    </div>	
   
   	
    <div class="row">
    	
    	  
      <article class="span8 data-block">
						<header>
							<h2><?php _l('latest_customer_orders'); ?></h2>
							<ul class="data-header-actions">
								<li><a href="<?php echo site_url('orders'); ?>"><?php _l('new_order');?></a></li> 
								<li><a href="#">Export</a></li>
							 
							</ul>
						</header>
						<section>
          		
   <table class="table table-striped table-bordered table-hover table-media">
			  <thead>
 					<tr>
									 
                        <?php foreach($tb_headers as $head){?>

						<th><?php _l($head); ?></th>

                        <?php } ?>
					</tr>
				</thead>

				<tbody>
			   <?php if($tb_data){ ?>
					<tr>
						<?php foreach($tb_headers as $header){?>
							 <td> <?php echo $tb_data[$header]; ?></td>  
 			 			<?php } ?> 
 		 			</tr>
 		 			<?php }else{ ?>
 		 				<tr><td colspan="<?php echo  count($tb_headers) ; ?>">No Orders Made</td> </tr>
						
 		 			<?php } ?>
 			 </table>
 			 </section>
        </article> 
 
  
      <article class="span4 data-block">
						<header>
							<h2><?php _l('customer_details');
							 
 ?></h2>
							<ul class="data-header-actions">
								<li><a href="<?php echo site_url($edit).'/'.$customer['customer_id']; ?>"><?php _l('edit');?></a></li> 
								<li><a href="#">Export</a></li>
								 
							</ul>
						</header>
						<section>
          		
   <table class="table table-striped table-bordered table-hover table-media">
			  <thead>
 
                        <?php foreach($cust_headers as $head){?>

						<tr><th><?php _l($head); ?></th>  <td>
							
							 <?php 
							 if($head =='status'){
							 	if($customer[$head] ==1){
							 	echo "<label class='label label-success' > Active </label>";
								}else{
								echo "<label class='label label-danger' > In-Active </label>";	
								}
							 }else{ 
							 echo ucwords($customer[$head]);
							 } ?>
							 
							 </td></tr>  
 			 			<?php } ?> 
 		 			 
 			 </table>
 			 </section>
        </article>
        </div>
 
    </div>
 
