				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 	
    <div class="row">
     			<!-- Data block -->
					<article class="span12  data-block">
						<header>
							<h2><?php _l('account_settings');  ?></h2>
							<ul class="data-header-actions">
								<li class="demoTabs active"><a href="#one"><?php _l('account_settings') ?></a></li>
								<li class="demoTabs"><a href="#two"><?php _l('account_profile') ?></a></li>
							</ul>
						</header>
						<section class="tab-content">
						
							<!-- Tab #one -->
							<div class="tab-pane active" id="one">
								<h3><?php _l('account_settings'); ?></h3>
								<div class="widget-content">
  
<?php  
 echo form_open('settings/update',array('class' => 'form-horizontal'));  
?>
 <fieldset>  
 <?php
//success msg display
if( $this->session->userdata('saved') ) {
	echo '<div class="alert alert-success" style="text-align:center;">Your Settings Successfuly saved.</div>';
	$this->session->unset_userdata('saved');

} 
  
  ?>
<div class="control-group">
	 	<table class="table table-bordered table-striped">
					        <thead>
					          <tr>
					           
					       <th style="font-weight:bolder;">Settings</th> <th style="font-weight:bolder;" >Status</th>   </tr>
					        </thead>
					        <tbody>
					       <?php  $nn = count($info[0]); ?>
				  						 
				  					 <?php
				  					 
				  				if($info){
				  					
				  					 foreach($info[0] as $key => $value){
				  					 if($key=="account_id" OR $key=="setting_id") {
				  					 	
				  					 }elseif($key=="default_currency"){ ?>
				  							<tr><td > <?php _l($key); ?></td> <td> <?php
				  					 
				  $currencies = json_decode(file_get_contents('http://www.localeplanet.com/api/auto/currencymap.json'),true); 							
			$currency_types = array(''=>'Select');	  					 
  foreach($currencies as $currency){
  	 
		 $currency_types[$currency['symbol']]= $currency['symbol_native']."  -".$currency['code']."";//.' : ' .$currency['currency_symbol'];
} 	 		  			 
		echo form_dropdown('default_currency',$currency_types,$settings['default_currency']);   ?>   </td></tr> 
				  					 	 
				  					 
				  					 <?php 
				  					 
				  					 }elseif($key=="vat"){ ?>
				  					 	
				  					 <tr><td > <?php _l($key); ?></td> <td> <input type="text" name="<?php echo $key;?>" value="<?php echo $value; ?>" /></td></tr>
				  					 
									<?php	
									}elseif($key=="tracking_interval"){ ?>
				  					 	
				  					 <tr><td > <?php _l($key); ?></td> <td>
				  					 	<div class="input-append">
<input id="appendedInput" class="span2"type="number" min="15" name="<?php echo $key;?>" value="<?php echo $value; ?>" />
<span class="add-on">In Minutes</span>
</div>
				  					 	
				  					 	
				  					 
									<?php	
									
									}elseif($key=="commission_rate"){ ?>
									<?php

									}elseif($key=="discount_limit"){ ?>
				  					 	
				  					 <tr><td > <?php _l($key); ?></td> <td>
				  					 	<div class="form-controls">
<div class="input-append">
<input id="appendedInput" class="span2"type="number" name="<?php echo $key;?>" value="<?php echo $value; ?>" />
<span class="add-on">%</span>
</div>
<span class="help-inline">Value as Percentage</span>
</div>
				  					 	
				  					 	</td></tr>
				  					 
									<?php
				  					 }else { ?>	
				  					   <tr><td > <?php _l($key); ?></td> <td>   <input type="checkbox"  name="<?php echo $key; ?>" <?php if($value ==1){ echo "checked";}  ?> /> </td></tr>
				  					 
				  						 <?php } 
									 }
				  						 }else{ echo ' <td> No data available</td>'; }
				  						 ?> 
						    	</tbody>
						     </table> 
	 
</div>
 
<div class="form-actions">
 <input type="reset" class="btn btn-large" value="Reset">
<button type="submit"  class="btn btn-info btn-large"><i class="icon-pencil"></i>Save</button>
 </div>

</fieldset>
</form>  

</div>

							</div>
							<!-- /Tab #one -->
							
							<!-- Tab #two -->
							<div class="tab-pane" id="two">
							<h3><?php _l('account_profile'); ?></h3>
							<article class="span12 data-block">
<?php  
 echo form_open('settings/update_profile',array('class' => 'form-horizontal'));  
?>
 <fieldset>  
 <?php
//success msg display
if( $this->session->userdata('saved') ) {
	echo '<div class="alert alert-success" style="text-align:center;">Information Successfully updated.</div>';
	$this->session->unset_userdata('saved');
} 
 
  ?>
<div class="control-group">
	 	<table class="table table-bordered table-striped">
					        <thead>
					          <tr>
					           
					       <th style="font-weight:bolder;" colspan="3">Account Profile</th>    </tr>
					        </thead>
					        <tbody>
					       <?php 
					       if($profile) { 
					       		foreach($profile as $profile): ?>
				  						 <tr><td style="vertical-align:middle;"><?php _l('company_name');?></span> </td> <td ><input name="company_name" type="text" placeholder="company name" value="<?php echo $profile['company_name'];?>" > </td></tr> 
				  						  
				  						 <tr><td   style="vertical-align:middle;"><?php _l('mobile');?>  </td> <td ><input name="mobile" value="<?php echo $profile['mobile'];?>" type="text" placeholder="mobile"> </td></tr> 
				  						  <tr><td   style="vertical-align:middle;"><?php _l('email');?> </td> <td ><input name="email" value="<?php echo $profile['email'];?>" type="text" placeholder="email"> </td></tr>
				  						  <tr><td  style="vertical-align:middle;"><?php _l('street');?>  </td> <td ><input name="street" value="<?php echo $profile['street'];?>" type="text" placeholder="street"> </td></tr>
				  						  <tr><td  style="vertical-align:middle;"><?php _l('street');?>  </td> <td ><input name="area" value="<?php echo $profile['area'];?>" type="text" placeholder="area"> </td></tr>
				  						  
				  						 <tr><td   style="vertical-align:middle;"><?php _l('city');?> </td> <td ><input name="city" value="<?php echo $profile['city'];?>" type="text" placeholder="city"> </td></tr> 
				  						 <tr><td  style="vertical-align:middle;"><?php _l('tin_no');?> </td> <td ><input name="tin_no" value="<?php echo $profile['tin_no'];?>"  type="text" placeholder="TIN Number"> </td></tr> 
				  						  <tr><td  style="vertical-align:middle;"><?php _l('vat_no');?>  </td> <td ><input name="vat_no" value="<?php echo $profile['vat_no'];?>"  type="text" placeholder="VAT Number"> </td></tr>
                                     <?php endforeach; }?>
						    	</tbody>
						     </table> 
	 
</div>
 
<div class="form-actions" >
 <input type="reset" class="btn btn-large" value="Reset">
<button type="submit"  class="btn btn-info btn-large"><i class="icon-pencil"></i>Update</button>
 </div>

</fieldset>
</form>  

</div> 
 
								 
									
							</div>
							<!-- /Tab #two -->
								
						</section>
					</article>
					<!-- /Data block -->
     		 
 </div>
