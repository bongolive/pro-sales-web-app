<?php 
session_start();
if($_SESSION['LogIn'] != 1){
	header("location: login.php");
}
include('common.php');
include ("connection.php");

if(isset($_GET['userid'])){
	$userid=$_GET['userid'];
}else{
	$userid='';
}

$result = "";
$query_str = "";
if ($_SERVER['QUERY_STRING'] !== ''){

	//$_POST['txt_enddate'];
	$date = date_create($_GET['startdate']);
	$sdate = date_format($date, 'Y-m-d H:i:s');
	$date = date_create($_GET['enddate']);
	$edate = date_format($date, 'Y-m-d H:i:s');
	$sub = "t.subject like '%$_GET[txtsubject]%'";
	if(strlen($_GET['txtsubject']) <= 0){
		$sub = "1=1";
	}else{
		$query_str .= "?txtsubject=".$_GET['txtsubject'];
	}
	
	$task = "t.task like '%$_GET[txttask]%'";
	if(strlen($_GET['txttask']) <= 0){
		$task = "1=1";
	}else{
		$query_str .= (($query_str == "") ? "?" : "&")."txttask=".$_GET['txttask'];
	}
	$phoneid = "1=1";
	if(isset($_GET['cb_devices'])){
		if(count($_GET['cb_devices']) >0){
			$phoneid = "(";
			for($i = 0; $i < count($_GET['cb_devices']); $i++){
				$phoneid .= "t.phoneid = ".$_GET['cb_devices'][$i]." ".(($i < count($_GET['cb_devices'])-1) ? " OR " : "");
				$query_str .= (($query_str == "") ? "?" : "&")."cb_devices[]=".$_GET['cb_devices'][$i];
			}
			$phoneid .= ")";
		}
	}
	
	$stat = "1=1";
	if($_GET['statussearch'] != -1 && $_GET['statussearch'] != ''){
		$stat = "t.status = '$_GET[statussearch]'";
		$query_str .= (($query_str == "") ? "?" : "&")."statussearch=".$_GET['statussearch'];
	}else{
		
	}
	$date = "t.datetime between '$sdate' and '$edate'";
	if(strlen($_GET['startdate']) <= 0 || strlen($_GET['enddate']) <= 0)
	{
		$date = "1=1";
	}else{
		$query_str .= (($query_str == "") ? "?" : "&")."startdate=".$_GET['startdate'];
		$query_str .= (($query_str == "") ? "?" : "&")."enddate=".$_GET['enddate'];
	}
	$sql = "SELECT t.*,p.device_name FROM tasks t inner join phones p on p.phoneid=t.phoneid left join phones_users pu on pu.phoneid=p.phoneid where  pu.userid=".$_GET['userid']." and $sub and $task and $phoneid and $stat and $date";
	//$sql = "select * from tasks where $sub and $phoneid and datetime between '$sdate' and '$edate'";
	$result =mysql_query($sql,$con);
	
	//echo $sql;
	//echo mysql_num_rows($result);
	
	
	if(isset($_GET["export"])){
		if($_GET["export"] == "excel"){
			
			require_once 'PHPExcel_classes/PHPExcel.php';
		
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getProperties()->setCreator("Foot Print Admin")
										 ->setLastModifiedBy("Foot Print Admin")
										 ->setTitle("Exported Tasks from Foot Print Site")
										 ->setSubject("Exported Tasks from Foot Print Site")
										 ->setDescription("Exported Tasks from Foot Print Site for the User '".$_SESSION['username']."'")
										 ->setKeywords("Exported Tasks from Foot Print Site")
										 ->setCategory("Exported Tasks from Foot Print Site");			
		
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1', 'Device Name')
						->setCellValue('B1', 'Subject')
						->setCellValue('C1', 'Task')
						->setCellValue('D1', 'Status')
						->setCellValue('E1', 'Date Time')
						->setCellValue('F1', 'Comments');
			
			$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
			
			
			
			$index = 2;
			while($row = mysql_fetch_array($result))
			{
				$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$index, $row["device_name"])
							->setCellValue('B'.$index, $row["subject"])
							->setCellValue('C'.$index, $row["task"])
							->setCellValue('D'.$index, $row["status"])
							->setCellValue('E'.$index, $row["datetime"])
							->setCellValue('F'.$index, $row["comments"]);
							
				$index++;
			}
			
			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('Tasks');
			
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			
			// Redirect output to a client's web browser (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="tasks_for_user-'.$_SESSION['username'].'_'.date('m-d-Y_h-i-s-a', time()).'.xlsx"');
			header('Cache-Control: max-age=0');
			
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');
			exit;
			
			
		}
	}
	
	
	
	
}else{
	//$sql="SELECT * FROM tasks where phoneid in (select phoneid from phones_users where userid = ".$_SESSION['userid'].")";
	$sql ="SELECT t.*,p.device_name FROM tasks t inner join phones p on p.phoneid=t.phoneid left join phones_users pu on pu.phoneid=p.phoneid  order by datetime DESC";
	$result = mysql_query($sql);
}

?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
<link rel="stylesheet" href="bx_styles.css" type="text/css" />
<title>footprint</title>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" media="all" type="text/css" href="jquery-ui-timepicker-addon.css" />
<link rel="stylesheet" media="all" type="text/css" href="css/pagination.css" />

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/commons.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.8.24/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="js/jquery.pagination.js"></script>
<link href="facebox.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="js/facebox.js" type="text/javascript"></script>


<script type="text/javascript">
	/* $(document).ready(function(){
	
	
    $("#deletetask").click(function(){
	alert("hi");
		var x=window.confirm("Are you sure want to delete this task?")
		if (x){
			var parent = $(this).closest('TR');
			var id = parent.attr('id');
			var uid = parent.attr('uid');
			$.ajax({
				type: 'POST',
				data: 'id=' +id+"&uid="+uid+"&type=task",
				url: 'deleteitem.php',
				success: function(msg){
					$('#'+id).remove();
				}
				});
		}
	});

  });*/
  
  function deleteclicked(ctrl){
	var x=window.confirm("Are you sure want to delete this task?")
		if (x){			
			var parent = $(ctrl).closest('TR');
			var id = parent.attr('id');
			var uid = parent.attr('uid');
			$.ajax({
				type: 'POST',
				data: 'id=' +id+"&uid="+uid+"&type=task",
				url: 'deleteitem.php',
				success: function(msg){					
					$('#'+id).remove();
					initPagination();
				}
				});
		}
  }
  $(document).ready(function(){
		$('#startdate').datetimepicker();
		$('#enddate').datetimepicker();
		
		if(document.location.search.length) {
			
			var sd = decodeURIComponent(getQuerystring("startdate","")).replace("+"," ");
			var ed = decodeURIComponent(getQuerystring("enddate","")).replace("+"," ");
			$("#txtsubject").val(getQuerystring("txtsubject",""));
			$("#txttask").val(getQuerystring("txttask",""));
			$("#startdate").val(sd);
			$("#enddate").val(ed);
			
			var s = $("select[name='device_name']");
			var v = getQuerystring("device_name","");
			s.val(v);
			var s1 = $("select[name='statussearch']");
			var v1 = getQuerystring("statussearch","");
			s1.val(v1);
			var su = $("select[name='userid']");
			var vu = getQuerystring("userid","");
			su.val(vu);
	
		}
  });
  
	function getQuerystring(key, defaultValue) {
		if (defaultValue == null) defaultValue = "";
		key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
		var qs = regex.exec(window.location.href);
		if (qs == null)
			return defaultValue;
		else
			return qs[1];
	}

 

</script>


<script type="text/javascript">
function validate_fields(){
	var s_date = document.getElementById("startdate").value;
	var e_date = document.getElementById("enddate").value;
	
	if(e_date != "" && s_date != ""){
			
		s_date = s_date.split(" ");
		var s_date_arr = s_date[0].split("/");
		var s_time_arr = s_date[1].split(":");
		
		e_date = e_date.split(" ");
		var e_date_arr = e_date[0].split("/");
		var e_time_arr = e_date[1].split(":");
		
		s_date = new Date(s_date_arr[2], s_date_arr[0], s_date_arr[1], s_time_arr[0], s_time_arr[1]);
		e_date = new Date(e_date_arr[2], e_date_arr[0], e_date_arr[1], e_time_arr[0], e_time_arr[1]);
		
		if(Date.parse(s_date.toString()) > Date.parse(e_date.toString())){
			alert("Start Date & Time is Greater then End Date & Time");
			return false;
		}
		
	}
	return true;
	
}
</script>
<script type="text/javascript">
$(document).ready(function()
{
$("#userid").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;

$.ajax
({
type: "POST",
url: "ajax_phone_list.php",
data: dataString,
cache: false,
success: function(html)
{
$("#phoneList").html(html);
}
});

});

});
</script>

</head>

<body>
<?php /*session_start();
include('common.php');
include ("connection.php");*/
?>
	<div class="body_class">
    	<div class="center_header_page">
        	<div class="header-left">
            	<a href="index.php" ><img src="images/logo3.png" alt=""  /></a>
            </div><div class="header-right">
            	<span style="font-family:Arial; font-size:14px; color:#FFFFFF; margin-top:8px; float:left">Welcome <?php echo $_SESSION['username'];?> <br /><a href="logout.php" style="color:#FFFFFF">Logout</a></span>
            </div>
            
        </div>
	</div>
	<div class="center_header_pageinner">
    	<div class="menu_wrapperinner">
        	
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="adm_device.php" >Device</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="adm_track.php" >Track</a>
                </div>
            </div>
			  <div class="menu_firstinner">
            	
                <div class="active menu_contentinner">
                	<a href="adm_task.php" >Tasks</a>
                </div>
            </div>
			  <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="adm_report.php" >Reports</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                		<a href="adm_boundary.php" >Boundaries</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="adm_profile.php" >Profile</a>
                </div>
            </div>
       
            <div class="clear"></div>
			</div>
        </div>
    
    </div>
    <div class="line">
    </div>
    <div class="center_header_page">
      <div class="content" >
        
            <div class="content_first content_sec_heading">
                
        <div class="content_thirdlog"><!--content_third-->
			<div class="content_thirdlog_left1">
         	  
                <div class="content_third_left_second">
                	<h3>Search</h3>
                    <div class="clear"></div>
					<form id="search_task_form" action="adm_task.php" method="get" onsubmit="return validate_fields();" >
                    <div class="content_third_left_contactlogpro">
                    	
				
                        <div class="content_third_left_contactlogpro_input">
                        <input type="text" value="" id="startdate" name="startdate" placeholder="Start date" class="search"/>						                     
						</div>
						
						<div class="content_third_left_contactlogpro_input">
                    	<input type="text" value="" id="enddate" name="enddate" placeholder="End date"  class="search"/>						
                        </div>
						
						
						<div class="content_third_left_contactlogpro_input">
                    	<select id="userid" name="userid" class="search">
							<option value="">Select User</option>
						<?php
						//include ("connection.php");

						$sql="select userid,name from users";
						$result2 = mysql_query($sql);
						while($row = mysql_fetch_array($result2))
						{
                          echo "<option value={$row['userid']}>{$row['name']}</option>";
                        }
						?>
                        </select>
						
                        </div>
						
						
						
                        <?php
						
						if($userid !=''){
						$sql="SELECT * FROM phones where phoneid in (select phoneid from phones_users where userid=".$userid.")";
						}else{
							$sql="SELECT * FROM phones";
						}
						$res = mysql_query($sql);
						
						echo '<div id="phoneList" name="phoneList" style="background-color:#D3D2D2; border: 2px solid #AAAAAA; color:#010101; font-family:BKANT; font-size:14px; margin:0 0 10px 0; '.((mysql_num_rows($res) <= 1) ? ' height:auto;  min-height:25px;' : 'max-height:100px; overflow-x:hidden; overflow-y:scroll;').' ">';
						
						while($row = mysql_fetch_array($res))
						{
							echo '<div style="width:165px; float:left;">';
							echo '<input id="cb'.$row['phoneid'].'" type="checkbox" name="cb_devices[]" value="'.$row['phoneid'].'" /><label for="cb'.$row['phoneid'].'">'.$row['device_name'].'</label><br />';
							echo '</div>';
						}
						
						echo '</div>';
						?>
                        
                        
						
						<div class="content_third_left_contactlogpro_input">
							<input type="text" value="" name="txtsubject" id="txtsubject" placeholder="Subject"  class="search"/>
                        </div>	
						<div class="content_third_left_contactlogpro_input">
							<input type="text" value="" name="txttask" id="txttask" placeholder="Task"  class="search"/>
                        </div>							
                        <div class="content_third_left_contactlogpro_input">
							
							<select name="statussearch" id="statussearch" style="color:#000000; font-size:16px"  class="search">
								<option selected value="-1">Select Status</option>
								<option value="New" >New</option>
								<option value="In-Progress">In-Progress</option>
								<option value="Done">Done</option>
							</select>						
                        </div>
						
                    </div>
					<input type="submit" value="" id="btn_search" name="btn_search" class="btnsearch"/>
                    </form>
					
              </div>
              
              <div style="clear:both; padding:20px 0 0 0; height:40px;">
                  <a href="task.php<?php echo $query_str.(($query_str == "") ? "?" : "&")."export=excel"; ?>"><img src="images/excel_export.png" width="20" height="20"  style="float:left;" /><strong style="float:left; height:20px; line-height:20px; margin:3px 3px 3px 3px; font-size:14px;">Excel Export</strong></a>
              </div>
              
              
         	</div>
            <div class="content_third_center1">
			
            </div>
			
			
			
			
         	<div class="content_third_right1">
<table width="660px" align="right" cellpadding="0" cellspacing="0" border="1" bordercolor="#ffffff" id="hiddenresult" style="display:none;">

<?php


if (mysql_num_rows($result) > 0){
while($row = mysql_fetch_array($result))
  { 
	$task="";
	if(strlen($row['task']) > 30){
		$task = substr($row['task'], 0,30)."...<a rel='facebox' href='showtask.php?XDEBUG_SESSION_START=dk&id={$row['id']}'>Read more</a>";
	}else{
		$task = $row['task'];
	}
echo "<tr id='{$row['id']}' uid='{$_GET['userid']}' style='font-family: Calibri; color:#000000; font-size:16px; text-align:center; background-image:url(images/bgupper.png); background-repeat:repeat ; height:70px;' class='result'>";
echo "<td>{$row['device_name']}</td>";
echo "<td>{$row['subject']}</td>";
echo "<td>{$task}</td>";
echo "<td>{$row['status']}</td>";
echo "<td>{$row['datetime']}</td>";
//echo "<td><a rel='facebox' href='edittask.php?XDEBUG_SESSION_START=dk&id={$row['id']}'><img src='images/EDIT.png' /></a></td>";
echo "<td><a rel='facebox' href='adm_edittask.php?id={$row['id']}'><img src='images/EDIT.png' /></a></td>";
echo "<td><a href='javascript:void(0);' class='deleteitem1' id='deletetask' onclick='deleteclicked(this);'><img src='images/deletebutton.png' /></a></td>";
echo "</tr>";
}
}
?>
</table>

<div class="clear"></div>
<table width="660px" align="right" cellpadding="0" cellspacing="0" border="1" bordercolor="#ffffff" id="searchresult">
<tr style="font-family: Calibri; color:#000000; font-size:16px; text-align:center; background-image:url(images/bgupper.png); background-repeat:repeat ; height:70px;">
	<td><strong>Device Name</strong></td>
	<td><strong>Subject</strong></td>
	<td><strong>Task</strong></td>
	<td><strong>Status</strong></td>
	<td><strong>Date</strong></td>
	<td><strong>Edit</strong></td>
	<td><strong>Delete</strong></td>
</tr>
</table>
<div id="Pagination" class="pagination">
</div>
<div class="clear"></div>					
 <a rel="facebox" href="adm_newtask.php"><div class="content_third_left_buttontask">&nbsp;
                        </div>
				</a>
          </div>
		  <div class="clear"></div>
		 
         	</div> 
			 <div id="dialog"></div>          
            <div class="clear"></div>
			
         </div><!--content_third-->
            </div>
      </div>
    
</div>
    <div class="footer-wrapper">
    	<div class="center_footer_page">
        	<div class="footer_center">
            	<div class="footer_navi" style="display:none;">
            	<a href="" >Home</a>
                <a href="" >Company</a>
                <a href="" >Clients</a>
                <a href="" >Resources</a>
                <a href="" >Support</a>
                <a href="" >Blog</a>
                <a href="" >Contact</a>
                </div>
            </div>
         <div class="clear"></div>
        </div>
    </div>
    <div class="center_header_page">
    	<div class="footer_buttom">
    	<h3>FOOT PRINT</h3><p>Copyright &copy; 2012. All Rights Reserved.</p>
        </div>
         <div class="clear"></div>
    </div>

		
	<script type="text/javascript">
		var page_size = 10;
        var num_entries = $('#hiddenresult tr.result').length;
        var num_entries2 = 0;
		var hrd;
        $(document).ready(function () {
//			alert(num_entries);
			/*hrd = $('#searchresult').html();
			$("#Pagination").pagination(num_entries, {
                items_per_page: page_size,
				num_edge_entries: 10,
                callback: pageselectCallback
            });
            if (num_entries <= page_size) {
                $("#Pagination").hide();
            } else {
                $("#Pagination").show();
            }*/
			initPagination();
        });
	
	function initPagination(){
		hrd = $('#searchresult').html();
		$('#searchresult').empty();
			$("#Pagination").pagination(num_entries, {
                items_per_page: page_size,
				num_edge_entries: 10,
                callback: pageselectCallback
            });
            if (num_entries <= page_size) {
                $("#Pagination").hide();
            } else {
                $("#Pagination").show();
            }
			
		}
			
	function pageselectCallback(page_index, jq) {
		try{
			var items_per_page = page_size;
			var max_elem = Math.min((page_index + 1) * items_per_page, num_entries);
			var newcontent = '';

			for (var i = page_index * items_per_page; i < max_elem; i++) {
				//newcontent += $('#hiddenresult tr.result:eq(' + i + ')').outerHTML();
				newcontent += $('#hiddenresult tr.result:eq(' + i + ')').clone().wrap('<div></div>').parent().html();
			}
			newcontent = hrd + newcontent;
			//            var new_content = jQuery('#hiddenresult div.result:eq(' + page_index + ')').clone();
			$('#searchresult').empty().append(newcontent);
			setTimeout("initfacebox();",100);
			$('#facebox .content').empty()
		}catch(e){
			alert("error");
		}
		return false;
	}
	

	</script>
	
	<script type="text/javascript">
	
	function initfacebox(){
		$('a[rel*=facebox]').facebox();
	}
			$(document).ready(function(){
		//$('a[rel*=facebox]').facebox();
	});  
	
		</script>
<script src="js/analytics.js" type="text/javascript"></script>
</body>
</html>
