<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">



		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l($tb_name); ?></h2>
							 
						<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('accounts/save'); ?>";" style="float: right; padding:8px;"><?php _l('new_account'); ?></a>
					 
					  	<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('accounts'); ?>" ;"="" style="float: right; padding: 8px;"><?php _l('back') ?></a>
					 
            </header> 

        </article>

    </div>	
 
    <div class="row">
  
        <article class="span12 data-block">
 
            <section>
         		Inside Change Type...
   <table class="table table-striped table-bordered table-hover table-media">
							  
								<tbody>

							 
									  <?php if ($tb_data){
									 // 	print_r($tb_data); 
									foreach ($tb_data as $data):   
                                            $id = $data[$table_id];
                                    ?>
								  
                                        <?php foreach ($row_fields as $field): ?> 
										<tr class="even gradeC"><th><?php _l($field) ?></th>	<td><?php echo ucwords($data[$field]); ?></td></tr>	  
											 
                                        <?php endforeach; ?>
										 
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td colspan="2"> No data available </td> </tr>';
										}
									?>
								</tbody>
	</table> 
          </section>

        </article>

    
    </div>

</section>
