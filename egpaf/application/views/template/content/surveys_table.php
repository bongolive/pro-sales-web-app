   <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
 
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('surveys_list') ?></h3>    
                                  <!--  <a href="<?php echo site_url('surveys/create'); ?>" style="float:right;margin: 10px;" type="button" class="btn btn-primary"><?php echo lang('new_survey') ?> </a>-->                                
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="type1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr> 
                                            	  <?php foreach($tb_headers as $header){?>

										<th><?php echo lang($header); ?></th>

                                        <?php } ?>
                                            	 <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
   <?php if ($tb_data){ 
									foreach ($tb_data as $data):   
                                            $id = $data['survey_id'];
                                    ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
										<!--	<td><input id="optionsCheckbox" type="checkbox" value="option1"></td>-->
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php
											if ($field == 'status') {
												if ($data[$field] == 1) {
													echo '<span class="label label-success">Active</span>';
												} else {

													echo '<span class="label label-warning">In Active</span>';
												}
											} elseif ($field == 'assigned_to') {
													$assignee = array('0'=>'Normal Users','1'=>'Survey Administrators');
												echo $assignee[$data[$field]];
											} else {
												echo ucwords($data[$field]);
											}
										  	?></td>
                                        <?php endforeach; ?>
										
                                            <td>
                                               <div class="btn-group">
													<a href="<?php echo site_url($view) . '/' . $id; ?>" type="button" class="btn btn-small btn-info" <?php if($data['status']==0){ ?> onClick="confview()" <?php } ?> > <i class="fa fa-eye"></i></a>
												
												 <?php if(in_array(3, $this->user_permissions)){   ?>
													<a href="<?php echo site_url($edit) . '/' . $id; ?>" type="button" class="btn btn-small btn-warning"> <i class="fa fa-pencil"></i> <span class="icon-pencil"/> </a>
													<a href="<?php echo site_url($delete) . '/' . $id; ?>"   onclick="return confdeleting();"  type="button" class="btn btn-small btn-danger"><i class="fa fa-trash-o"></i> </a>
													<?php 
													if($data['status'] ==0){
													?>
<a href="<?php echo site_url('surveys/survey_status/active/' . $id); ?>" type="button"  class="btn btn-small btn-success" ><i class="fa fa-play"></i></a>
													<?php }else{ ?>
<a href="<?php echo site_url('surveys/survey_status/deactive/' . $id); ?>" type="button" class="btn btn-small btn-primary" ><i class="fa fa-pause"></i></a>
														<?php }}?>
													 
												 </div> 						 
											</td>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                               <?php foreach($tb_headers as $header){?>

										<th><?php echo lang($header); ?></th>

                                        <?php } ?>

                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
          