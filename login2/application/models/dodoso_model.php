<?php
class Dodoso_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
public function read_majibu_excel($where=FALSE)
	{
 		 
		$this->db->select('*');
		$this->db->from('majibu');
		if($where) {
			$this->db->where($where);
		} 
		
		$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
	}	
 public function read($where=FALSE,$toa=FALSE)
	{
			$this->db->select('*');		
			$this->db->select('count(majibu.namba_ya_dodoso) as answers');		
			$this->db->from('dodoso');		
		
		if($where) {
			 $this->db->where($where);
		}
	if($toa) { $this->db->where_in('majibu.swali',$toa);}
		$this->db->group_by('dodoso.namba_ya_dodoso');
		
		  $this->db->join('majibu','dodoso.namba_ya_dodoso=majibu.namba_ya_dodoso','left');
		 $result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
	}
public function read_dodoso($where=FALSE) {
	
	$this->db->select('*');
	 $this->db->from('dodoso');
		if($where) {
			 $this->db->where($where);
		 } 
 	  	  
		$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
}
public function read_dodoso_excel($where=FALSE) {
	
	$this->db->select('*');
	 $this->db->from('dodoso');
		if($where) {
			 $this->db->where($where);
		 } 

		$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
}
public function read_pathfinder($where=FALSE,$title=FALSE) {
	
	$this->db->select('*');
	 $this->db->from('pathfinder_dodoso');
		
		if($where) {
			 $this->db->where($where);
		 }
	if($title) {
			$this->db->limit(1);
		}  
		$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
}

public function read_mume($where=FALSE,$toa=FALSE)
	{
			$this->db->select('*');		
			//$this->db->select('count(majibu.namba_ya_dodoso) as answers');		
			$this->db->from('dodoso');		
		
		if($where) {
			 $this->db->where($where);
		}
	if($toa) { $this->db->where_not_in('majibu.swali',$toa);}
			$this->db->group_by('majibu.namba_ya_dodoso');
			
		 $this->db->join('majibu','dodoso.namba_ya_dodoso=majibu.namba_ya_dodoso','left');
		 $result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
	}


public function idadi_maswali() {
			$this->db->select('namba_ya_dodoso,count(namba_ya_dodoso) as answers');		
			$this->db->from('majibu');	
			if($where) {
			 $this->db->where($where);
		} 
		$this->db->group_by('namba_ya_dodoso');
		 //$this->db->join('majibu','dodoso.namba_ya_dodoso=majibu.namba_ya_dodoso','left');
		 $result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
}


public function get_phones($where)
	{ 
		$this->db->select('*');		 
		$this->db->from('phones_users'); 
	  	$this->db->join('phones','phones.phoneid=phones_users.phoneid');
		$this->db->where($where);
		$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
	}
	
	
	
public function read_majibu($where=FALSE)
	{
		$this->db->select('*');		 
		$this->db->from('majibu');
	 	if($where) {
  		$this->db->where($where);
  	} 
		$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
	}


	public function read_maswali_excel($where=FALSE,$swali=FALSE)
	{ 
	 	$this->db->select('*')
					->from('maswali');
		if($where) {$this->db->where($where);}
		if($swali=='mume') {	
				$aa=array('A1','B','P','TK','PN','MT','MC','HA','FP','IN','SL','L');$this->db->where_not_in('kundi',$aa);
			}elseif($swali=='mke') {
				 $aa=array('A1','DM'); $this->db->where_not_in('kundi',$aa);
				 }  
		$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
	}	
public function read_maswali($where=FALSE)
	{
		$this->db->select('*')
					->from('maswali');
		if($where) {
			 $this->db->where($where);
		 } 
		$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
	}	
//categories

public function  maswali_kundi($where=FALSE)
	{
		$this->db->select('*')
					->from('maswali_kundi');
		if($where) {
			 $this->db->where($where);
		 }
	 
	 $result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
	}

public function mikoa($where=FALSE) {
	$this->db->select('mkoa');
	 $this->db->from('vijiji');
		if($where) {
			 $this->db->where($where);
		 } 
	$this->db->group_by('mkoa');	 
	$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
}
public function wilaya($where=FALSE) {
	$this->db->select('wilaya,mkoa');
	 $this->db->from('vijiji');
		if($where) {
			 $this->db->where($where);
		 } 
			$this->db->group_by('wilaya'); 
	$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
}

public function kata($where=FALSE) {
		$this->db->select('wilaya,kata');
	 	$this->db->from('vijiji');
		if($where) {
			 $this->db->where($where);
		 } 
	$this->db->group_by('kata'); 	 
	$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
}

public function vijiji($where=FALSE) {
	$this->db->select('*');
	 $this->db->from('vijiji');
		if($where) {
			 $this->db->where($where);
		 } 
	$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
}

 
public function dodoso_idadi($where=FALSE) {
	$this->db->select('count(namba_ya_dodoso)')
					->from('dodoso');
		if($where) {
			 $this->db->where($where);
		 }
 		$this->db->group_by('kijiji');
		$result=$this->db->get()->result_array();
		if($result) {
			return $result;
		}else { return FALSE; }
}

public function update($where,$data) { 
  $this->db->where('dodoso_namba', $where);	
  $result=$this->db->update('pathfinder_dodoso', $data); 
	
	return $result;
}
 
 public function update_dodo($where,$data) { 
	$this->db->where('namba_ya_dodoso', $where);
	$result=$this->db->update('dodoso', $data); 
	
	return $result;
}
public function update_majibu($where,$data) { 
	$this->db->where('dodoso_namba', $where);
	$result= $this->db->update('majibu', $data); 
	
	return $result;
}

public function save($data) {
	$result=$this->db->insert('pathfinder_dodoso', $data);
	return $result; 
}


}