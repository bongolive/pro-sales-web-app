<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Check_boundaries_and_send_alert extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('boundry_model');
		$this->load->library('email');
	}

	public function index()
	{
		date_default_timezone_set("Africa/Dar_es_Salaam");
		$curr_date = date("Y-m-d H:i:s");
		
		$res = $this->boundry_model->get_outOfBoundary($curr_date);
		
		foreach($res as $row){
	
			if($row["devicelat"] != NULL && $row["devicelng"] != NULL ){
		
				$dist_km = get_dist_km($row["devicelat"], $row["devicelng"], $row["centerlat"], $row["centerlng"]);
				//$dist_km = round($dist_km, 2);
			
				$message = "";
		
				$radius = floatval( $row["radius"]) / 1000;
				echo $radius;
				if( $dist_km <= $radius && $row["laststatus"] == "0"){
			
					echo $message .= "This is an email to notify you that your device named as '".$row["device_name"]."' entered in Boundary '".$row["alertname"]."'. ";
					
					send_email($row["email"], $message);
					
					$cond = array('alocationid'=> $row["alocationid"]);
					$data = array('laststatus' => 1);
					$this->boundry_model->update_boundry_alocation($data,$cond);
					
					//set_new_status($row["alocationid"], "1");
			
				}else if($dist_km > $radius && $row["laststatus"] == "1"){
			
					$message .= "This is an email to notify you that your device named as '".$row["device_name"]."' leaved the Boundary '".$row["alertname"]."'. ";
			
					send_email($row["email"], $message);
					//set_new_status($row["alocationid"], "0");
					$cond = array('alocationid'=> $row["alocationid"]);
					$data = array('laststatus' => 0);
					$this->boundry_model->update_boundry_alocation($data,$cond);
				}
		
			}
	
		}

		
		
		
		
	}
	
}