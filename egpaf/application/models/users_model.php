<?php
class Users_model extends MY_Model {
	public $table = 'users';
	public $table_id = 'user_id';

	function __construct() {
		parent::__construct();
	}

	public function form_validation() {

		$this -> form_validation -> set_rules('username', 'username', 'trim|required|'); 
		$this -> form_validation -> set_rules('role', 'role', 'trim|required|');
		if ($this -> input -> post('password')) {
			$this -> form_validation -> set_rules('username', 'Username', 'trim|required|');
			$this -> form_validation -> set_rules('password', 'password', 'trim|required|min_length[6]|matches[conf_pass]');
			$this -> form_validation -> set_rules('conf_pass', 'password', 'trim|required');
		}
	}
  
}
