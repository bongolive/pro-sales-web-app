<?php
class accounts_model extends MY_Model {
	public $table = 'accounts';
	public $table_id = 'account_id';

	function __construct() {
		parent::__construct();
	}

	public function form_validation() {

		$this -> form_validation -> set_rules('organisation', 'organisation name', 'trim|required ');
		$this -> form_validation -> set_rules('contact_person', 'contact person', 'trim|required');
		$this -> form_validation -> set_rules('account_type', 'account type', 'trim|required');
		$this -> form_validation -> set_rules('mobile', 'mobile', 'required|numeric|callback_checkmobile[' . $this -> input -> post('account_id') . ']');
		$this -> form_validation -> set_rules('email', 'email', 'valid_email|callback_checkmail[' . $this -> input -> post('account_id') . ']');
		$this -> form_validation -> set_rules('user_limit', 'user limit', 'trim');

		$this -> form_validation -> set_rules('username', 'Username', 'trim|required');

		if ($this -> input -> post('password')) {
			$this -> form_validation -> set_rules('password', 'password', 'trim|required|min_length[6]|matches[conf_password]');

		}
	}

	public function read($where = FALSE) {
		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		$this -> db -> join('users', 'accounts.admin = users.user_id');
		$data = $this -> db -> get() -> result_array();
		return $data;
	}

	public function save_user($data) {
		$this -> table = 'users';
		$result = $this -> save($data);
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function saving($data) {
		$this -> table = 'accounts';
		$this -> table_id = 'account_id';
		if ($this -> save($data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update($id, $data) {
	 
		$this -> table = 'accounts';
		$this -> table_id = 'account_id';
		 
		$this -> db -> where($this -> table_id, $id);
		if ($this -> db -> update($this -> table, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

}
