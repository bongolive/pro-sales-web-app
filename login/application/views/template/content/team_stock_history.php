				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('sales_team_stock_history'); ?></h2>
							
							<ul class="data-header-actions">
<li>
<a class="btn btn-alt" href="<?php echo site_url('stock/sales_team'); ?>" style="  padding:8px;"><?php _l('back'); ?></a>
</li>
</ul> 
							
							
            </header> 
        </article>

    </div>	

    	 	 
    	  <?php include_once('filtering.php') ?>
                           <div class="row"> 
                        <article class="span12 data-block">
								<div class="modal-header">
									<h4><?php _l('products_batches');   
										?>
	 
<a class="btn btn-alt btn-info" href="<?php echo site_url('stock/team_stock/'.$tb_data[0]['device_imei']); ?>" style="float:right;  padding:8px;"><?php _l('view_all_products'); ?></a>
  
									</h4>
									
								
									
									
								</div>
								<div class="modal-body"> 
								
								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>

									<tr>
<th>No.</th>
                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>
 				  <?php if ($tb_data){
 				  	$i = 1; 
									foreach ($tb_data as $data):  
									  
                                            $id = $table_id;
                                     ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
											<td><?php echo $i; ?></td>
                                        <?php foreach ($row_fields as $field): ?>
                                        
										  <td><?php echo $data[$field]; ?></td>
                                        <?php endforeach; ?>
										
                                            <td> 
                                            	<a href="<?php echo site_url('stock/viewbatch/' . $data['batch_no']); ?>" class="btn btn-small btn-info"><span class="awe-eye-open"/> <?php _l('view'); ?></a>
                                                	<a href="<?php echo site_url('stock/updateteamstock/' . $data['batch_no']); ?>" class="btn btn-small"><span class="awe-pencil"/> <?php _l('edit'); ?></a>
                                               <!-- 	<a href="<?php echo site_url('stock/delete/' . $data['batch_no']); ?>" class="btn btn-small btn-danger" ><span class="awe-trash"/> <?php _l('delete'); ?></a>--> 
											</td>
									</tr>
									<?php
									$i++;
									endforeach;

									}else {
									echo '<tr> <td> No data available <td> </tr>';
									}
									?>
								</tbody>
	</table> 
           
		</article>		
                        
                        </div>                        	
                        </div>	
                        </section> 