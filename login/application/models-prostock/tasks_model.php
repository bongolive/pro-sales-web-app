<?php
class Tasks_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_taskById($taskid = FALSE)
	{
		$query = $this->db->get_where('tasks', array('id' => $taskid));
		return $query->row_array();
	}
	
	public function getTasksForPhones($phoneid)
	{
		$query = $this->db->get_where('tasks', array('phoneid' => $phoneid,'isdelete' => 0));
		return $query->result_array();
	}
	
	public function get_tasksByUserId($userid = FALSE)
	{
		if ($userid === FALSE)
		{
			$query = $this->db->query("select tasks.*,phones.device_name,users.name from tasks,phones,users,phones_users where tasks.phoneid = phones.phoneid and phones.phoneid = phones_users.phoneid and phones_users.userid=users.userid");
			return $query->result_array();
		}
	
		$query = $this->db->query("select tasks.*,phones.device_name,users.name from tasks,phones,users,phones_users where tasks.phoneid = phones.phoneid and phones.phoneid = phones_users.phoneid and phones_users.userid=users.userid and users.userid= $userid");
		return $query->result_array();
	}
	
	public function add_task($data)
	{
		return $this->db->insert('tasks', $data);
	}
	
	public function update_task($data,$taskid)
	{
		$this->db->where('id',$taskid);
		return $this->db->update('tasks', $data);
	}
	
	public function delete($taskid)
	{
		return $this->db->delete('tasks', array('id' => $taskid));
	}
	
	
	public function export($userid,$sub,$task,$phoneid,$stat,$date)
	{
		$query = $this->db->query("SELECT t.*,p.device_name FROM tasks t inner join phones p on p.phoneid=t.phoneid left join phones_users pu on pu.phoneid=p.phoneid where  pu.userid=".$userid." and $sub and $task and $phoneid and $stat and $date");
		return $query->result_array();
	}

}
