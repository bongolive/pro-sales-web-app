<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l($tb_name); ?></h2>
							&nbsp; &nbsp;	<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('orders/export'); ?>" style="float: right; padding:8px; margin-left: 20px;"> <img src="<?php echo base_url('template/img/icons/excel.png'); ?>" alt="export"/> <?php _l('export') ?></a>
					  
						<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('orders/new_order'); ?>";" style="float: right; padding:8px;"><?php _l('new_order'); ?></a>
					  
            </header> 

        </article>

    </div>	
    
    
<!-- include filtering -->
   	<?php
	include_once ('filtering.php');
   	?>
   	
   	
    <div class="row">
  
        <article class="span12 data-block">
 
            <section>
         		
   <table class="table table-striped table-bordered table-hover table-media">
								<thead>

								<tr>
										<th class="span1"><input id="optionsCheckbox" type="checkbox" value="option1"></th>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>

							 
									  <?php if ($tb_data){
									 // 	print_r($tb_data); 
									foreach ($tb_data as $data):   
                                            $id = $data[$table_id];
                                    ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
											<td><input id="optionsCheckbox" type="checkbox" value="option1"></td>
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php
										
										if ($field == 'staff') {
											//check staff and way of submission
											echo ucwords($data['firstname'] . ' ' . $data['lastname']);
											if($data['device_imei']==0){
												echo '<span class="label label-success">Web</span>';
											}else{
												echo '<span class="label label-info">Mobile</span>';
											}
										} elseif ($field == 'order_status') {
											if ($data[$field] == 1) {
												echo '<span class="label label-inverse">'; _l('pending'); echo '</span>';
											 } elseif ($data[$field] == 2) {
										 
												echo '<span class="label label-success">'; _l('closed'); echo '</span>';
											 
											} elseif ($data[$field] == 3) {
												echo '<span class="label label-important">'; _l('canceled'); echo '</span>';
											} 
										} elseif( $field == 'payment_status') {
                                            $j = $data[$field];
                                            if($j == 0) echo _l('receipt_notprinted');
                                            if($j == 1) echo _l('receipt_printed') ;
                                        } else {
											echo ucwords($data[$field]);
										}
										   ?></td>
                                        <?php endforeach; ?>
										
                                            <td>
                                            	<?php  	if ($data['order_status'] == 1 && $data['device_imei'] ==0) { ?>
                                            	<a href="<?php echo site_url('orders/process') . '/' . $id; ?>" class="btn btn-small btn-success"> <span class="icon-check"/><?php _l('processed') ?></a>
                                            	
                                            	<?php }else{  ?>
                                            		<a href="" class="btn btn-small" style="pointer-events: none;  cursor: default;"><span class="icon-check"/> <?php _l('processed') ?></a>
                                            		<?php } ?>
                                               <div class="btn-group">
													<a href="<?php echo site_url($view) . '/' . $id; ?>" class="btn btn-small"> <span class="icon-eye-open"/> </a>
													<?php 	if ($data['order_status'] == 1 &&  $data['device_imei']==0 ) { $style = ''; $icon='<span class="icon-pencil"/>'; }else{$style ='style="pointer-events: none;  cursor: default;"'; $icon ='<span class="elusive icon-pencil-alt"></span>';} ?>
													<a href="<?php echo site_url($edit) . '/' . $id; ?>" class="btn btn-small" <?php echo $style; ?> ><?php echo $icon;?>  </a>
													<a href="<?php echo site_url($delete) . '/' . $id; ?>" class="btn btn-small btn-danger"> <span class="icon-trash"/> </a>
                                                   <a href="<?php echo site_url($locate) . '/' . $id; ?>" class="btn btn-small btn-link"> <span class="icon-map-marker"/> </a>
												 </div> 						 
											</td>
									</tr>
									<?php endforeach;
									echo $pagenate;
										}else {
										echo '<tr> <td colspan="'.count($tb_headers).'"> No data available </td> </tr>';
										}
									?>
								</tbody>
	</table> 
          </section>

        </article>

    
    </div>

</section>
