<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Boundaries extends User_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('boundry_model');
		$this->load->model('devices_model');
		$this->session_userid = $this->session->userdata('userid');
	}

	
	public function index()
	{
		$condUserPerm = array('userid'=>$this->session->userdata('userid'));
		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] ="";
		$header['device_tab'] ="";
		$header['tasks_tab'] ="";
		$header['reports_tab'] ="";
		$header['deliveries_tab'] ="";
		$header['customers_tab'] ="";
		$header['boundaries_tab'] ="";
		$header['support_tab'] ="allow";
		$header['pathfinder_tab'] ="";
		$header['stocks_tab'] ="";
		$header['registrations_tab'] ="";
		
		foreach($user_permissions as $user_permission){
			if($user_permission['permission_name']=='track tab'){
				$header['track_tab'] ="allow";
			}else if($user_permission['permission_name']=='device tab'){
				$header['device_tab'] ="allow";
			}if($user_permission['permission_name']=='tasks tab'){
				$header['tasks_tab'] ="allow";
			}if($user_permission['permission_name']=='reports tab'){
				$header['reports_tab'] ="allow";
			}if($user_permission['permission_name']=='deliveries tab'){
				$header['deliveries_tab'] ="allow";
			}if($user_permission['permission_name']=='customers tab'){
				$header['customers_tab'] ="allow";
			}if($user_permission['permission_name']=='boundaries tab'){
				$header['boundaries_tab'] ="allow";
			}if($user_permission['permission_name']=='pathfinder survey tab'){
				$header['pathfinder_tab'] ="allow";
			}if($user_permission['permission_name']=='stocks tab'){
				$header['stocks_tab'] ="allow";
			}if($user_permission['permission_name']=='registrations tab'){
				$header['registrations_tab'] ="allow";
			}						
		}
		
		if($header['boundaries_tab'] !="allow"){
			redirect('profile/support');
		}
		
		$fields['devices'] = $this->devices_model->get_devicelistbyuserid($this->session_userid);
		$fields['tb_data'] = $this->boundry_model->get_boundryByUserId($this->session_userid);
       
	    $ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js','custom/custom-datatable.js');
        $fields['tb_name'] = 'boundaries_tb_name';
        $fields['row_fields'] = array('alertname','devices','valid_from','valid_to');
        $fields['tb_headers'] = array('alert_name','devices','valid_from','valid_to','action');
        $header['bd_active'] = 'class="active"';
        $fields['add_btn'] = 'add_boundary';
		
		$this->load->view('template/header',$header);
        $this->load->view('template/content/table',$fields);
        $this->load->view('template/footer',$ft_data);

	}
	
	
	public function add()
	{		
			$data = array(
				//'userid' => $session_userid,
                'userid' => $this->session_userid,
				'alertname' => $this->input->post('alert_name'),
				'centerlat' => $this->input->post('center_lat'),
				'centerlng' => $this->input->post('center_lng'),
				'radius' => $this->input->post('circle_radius'),
				'valid_from' => date("Y-m-d H:i:s",strtotime($this->input->post('valid_from'))),
				'valid_to' => date("Y-m-d H:i:s",strtotime($this->input->post('valid_to')))
			);
			
			$boundryid = $this->boundry_model->add_boundry($data);
			
			$devices = $this->input->post('cb_devices');
			
			for($i = 0; $i < count($devices); $i++ ){
				$data2 = array(
					'boundryid' => $boundryid,
					'imei' => $devices[$i],
					'laststatus' => '0'
				);
				
				$result = $this->boundry_model->add_boundry_alocation($data2);
				unset($data2);		
			}
			//redirect('/boundries/', 'refresh');
			
			if($result === TRUE){
				echo 1;
			}else{
				echo 0;
			}
			
	}
	
	
	public function edit()
	{
	    // This function will load the view to add or edit the device boundary
        $data['do'] = $this->uri->segment(4);
        if ($this->uri->segment(5)){
            $data['boundryid'] = $this->uri->segment(5);
            $data['boundaries'] = $this->boundry_model->get_boundryById($this->uri->segment(5));
            
        }
        $data['devices'] = $this->devices_model->get_devicelistbyuserid($this->session_userid);
		$boundryAllocation = $this->boundry_model->get_boundry_alocationByBoundryId($this->uri->segment(5));
		$boundryalocation_data = array();
		
		foreach($boundryAllocation as $r){
			array_push($boundryalocation_data, $r["imei"]);
		}
		
		$data['boundryalocation_data'] = $boundryalocation_data;
		
 		$this->load->view('template/content/edit_boundary',$data);
        //var_dump($data);
	}
	
	
	public function update()
	{		
			$data = array(
				//'userid' => $session_userid,
                'userid' => $this->session_userid,
				'alertname' => $this->input->post('alert_name'),
				'centerlat' => $this->input->post('center_lat'),
				'centerlng' => $this->input->post('center_lng'),
				'radius' => $this->input->post('circle_radius'),
				'valid_from' => date("Y-m-d H:i:s",strtotime($this->input->post('valid_from'))),
				'valid_to' => date("Y-m-d H:i:s",strtotime($this->input->post('valid_to')))
			);
			
			$devices = $this->input->post('cb_devices');
			$boundryid = $this->input->post('boundryid');
			$result = $this->boundry_model->update_boundry($data,$boundryid);
			
			
			//selecting all devices of this boundry
			$saved_ime = $this->boundry_model->get_boundry_alocationByBoundryId($boundryid);
			
			$saved_imeis = array();
				
				foreach($saved_ime as $r){
					array_push($saved_imeis, $r["imei"]);
				}
			
			$to_be_inserted = array_diff($devices, $saved_imeis);
			$to_be_inserted = array_values($to_be_inserted);
				
			$to_be_deleted = array_diff($saved_imeis, $devices);
			$to_be_deleted = array_values($to_be_deleted);
			
			
			for($i = 0; $i < count($to_be_deleted); $i++ ){
				$data2 = array(
					'boundryid' => $boundryid,
					'imei' => $to_be_deleted[$i]
				);	
				
				$result = $this->boundry_model->delete_boundry_alocation($data2);
				unset($data2);	
							
			}
			
			
			for($i = 0; $i < count($to_be_inserted); $i++ ){
				$data2 = array(
					'boundryid' => $boundryid,
					'imei' => $to_be_inserted[$i],
					'laststatus' => '0'
				);
				
				$result = $this->boundry_model->add_boundry_alocation($data2);
				unset($data2);		
			}
			
			if($result === TRUE){
				echo 1;
			}else{
				echo 0;
			}
	}
	
	public function delete($boundryid)
	{
		$result = 	$this->boundry_model->delete($boundryid);
		if($result === TRUE)
		{
			redirect('boundaries');
            //echo 'yes';
		}
		else
		{
			redirect('boundaries');
            //echo 'no';
		}
	}
    
    public function showboundary(){
        
        //  Get the parameters on the url
        $data['lat'] = $this->uri->segment(4);
        $data['long'] = $this->uri->segment(5);
        $data['rad'] = $this->uri->segment(6);
          if ($data['lat'] != '' && $data['long'] != '' && $data['rad'] != ''){
               $this->load->view('template/content/showboundary',$data);
          }
 
        
    }

}

/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */
