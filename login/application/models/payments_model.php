<?php
class Payments_model extends MY_Model {
	public $table = 'sales_orders';
	public $table_id = 'order_id';

	function __construct() {

		parent::__construct();

	}

	public function form_validation() {
		$this -> form_validation -> set_rules('paid_amount', 'required|numeric');
	}

	public function read_orders($where) {
		$this -> table = 'sales_orders';
		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$this -> db -> join('customers', 'customers.customer_id = sales_orders.customer_id', 'left');

		$data = $this -> db -> get() -> result_array();

		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function pay_modes($where) {
		$this -> table = "payment_mode";

		$mode = $this -> read($where);
		if ($mode) {
			foreach ($mode as $mode) {
				$pay_mode[$mode['mode_id']] = $mode['mode_name'];
			}
			return $pay_mode;
		} else {
			return FALSE;
		}

	}

	public function saveTransaction($data) {
		$this -> table = 'transactions';

		if ($this -> save($data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function readTransactions($where) {
		$this -> table = 'transactions';

		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$this -> db -> join('payment_mode', 'mode_id = trans_pay_mode', 'left');
		$trasn = $this -> db -> get() -> result_array();
		if ($trasn) {
			return $trasn;
		} else {
			return FALSE;
		}

	}

	public function update_order($id, $data) {
		$this -> table = 'sales_orders';
		$this -> table_id = 'order_id';
		if ($this -> update($id, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function updateTransactions($id, $data) {
		$this -> table = 'transactions';
		$this -> table_id = 'trans_id';
		if ($this -> update($id, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

}
