<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
 
                            <div class="box">
                                <div class="box-header">
                                 
                                    <h4 class="page-header box-title">
<?php echo lang('surveyors_report') ?>
<small> View Report per Surveyor, select a surveyor from the list </small>
</h4>                                 
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="type2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr><th>#</th>
                                            	  <?php foreach($tb_headers as $header){?>

											<th><?php echo lang($header); ?></th>

                                        <?php } ?>
                                            	 <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
  				  <?php if ($tb_data){
  				 
									foreach ($tb_data as $data):  
									  
                                            $id = '';//$data[$table_id];
                                     ?>
									<tr>
										<td><input type="checkbox" name="item_no[]" value="<?php echo $data['device_id']; ?>"/></td>
                                        <?php foreach ($row_fields as $field): ?>
                                        	
										  <td><?php
										if ($field == 'total_response') {
											if($summary){
											foreach ($summary as $responses) {
												if ($responses['device_imei'] == $data['device_imei']) {
													echo $responses['total'];
												}
											}}else{echo 0;}
										} else {
											echo $data[$field];
										}
									?></td>
                                        <?php endforeach; ?>
										
                                            <td> 
                                                	<a href="<?php echo site_url('reports/surveyor_report') . '/' . $data['data_collector_no']; ?>" type="button"   class="btn btn-small btn-success" title="View" ><i class="fa fa-eye"></i> <?php echo lang('view') ?></a>
                                                	
                                                	 
											</td>
									
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
                                        </tbody>
                                        <tfoot>
                                            <tr><th>#</th>
                                               <?php foreach($tb_headers as $header){?>

											<th><?php echo lang($header); ?></th>

                                        <?php } ?>

                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div> 
                </section><!-- /.content --> 

