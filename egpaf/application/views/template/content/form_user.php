				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							
							<h2><?php
							if (!$info) {
								_l('new_user');
							} else {
								echo _l('update_user') . ' : ' . strtoupper($info['firstname'].' '.$info['lastname']);
							}
							 ?></h2>
					   
            </header> 

        </article>

    </div>	
    <div class="row">
    	 
    	 <article class="span12 data-block">
								<div class="modal-header">
									<h4><?php _l('user_details'); ?></h4>
								</div>
								<div class="modal-body">
									<?php
									if ($info) {
										$action = 'users/update_user';
									} else {
										$action = 'users/new_user';
									};
								?>
									
<?php
echo form_open($action, array('class' => 'form-horizontal'));
	if ($info) {?>
		<input type="hidden" name="user_id" value="<?php echo $info['user_id']; ?>"   /> 
<?php } ?>
 <fieldset>     
  <div class="control-group">
<span class="control-label"><?php _l('firstname'); ?></span><?php echo form_error('firstname'); ?>
<div class="controls"><input type="text" name="firstname" value="<?php
if ($info) { echo $info['firstname'];
} else { echo set_value('firstname');
}
 ?>" size="50"  /></div>
</div>
 <div class="control-group">
<span class="control-label"><?php _l('lastname'); ?></span><?php echo form_error('lastname'); ?>
<div class="controls"><input type="text" name="lastname" value="<?php
if ($info) { echo $info['lastname'];
} else { echo set_value('lastname');
}
 ?>" size="50"  /></div>
</div> 
  
<div class="control-group">
<span class="control-label"><?php _l('facility'); ?></span><?php echo form_error('facility'); ?>
<div class="controls">
			<?php
				 
					if ($info) {$value = $info['facility']; } else {$value = set_value('facility'); }
					echo form_dropdown('facility', $facilities, $value);
			?>
 </div>
</div> 
  
<div class="control-group">
<span class="control-label"><?php _l('role'); ?></span><?php echo form_error('role'); ?>
<div class="controls">
<?php
$role = array('' => 'Select', '1' => 'Admin', '0' => 'User');
if ($info) {$value = $info['role'];
} else {$value = set_value('role');
}
echo form_dropdown('role', $role, $value);
?>
 </div>
</div> 
 
 <div class="control-group">
<span class="control-label"><?php _l('username'); ?></span><?php echo form_error('username'); ?>
<div class="controls"><input type="text" name="username" value="<?php
if ($info) { echo $info['username'];
} else { echo set_value('username');
}
 ?>" size="50"  /></div>
</div>
   
<div class="control-group">
<span class="control-label"><?php _l('password'); ?></span><?php echo form_error('password'); ?>
<div class="controls"><input type="password" name="password"  size="50"  /></div>
</div> 
 

 <div class="control-group"> 
<span class="control-label"> <?php _l('conf_pass'); ?></span> <?php echo form_error('conf_pass'); ?>
<div class="controls"><input type="password" name="conf_pass"   size="50" /> </div>
 </div>
 
<div class="form-actions"> 

<a href="<?php echo site_url('users'); ?>" class="btn btn-large">Cancel</a>
 <button type="submit" class="btn btn-info btn-large"><i class="icon-pencil"></i>Save</button></div>

</fieldset>
</form>  

</div>
</div>
