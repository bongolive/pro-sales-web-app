<?php
class User_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_userlist($userid = FALSE)
	{
		if ($userid === FALSE)
		{
			$query = $this->db->get('users');
			return $query->result_array();
		}
	
		$query = $this->db->get_where('users', array('userid' => $userid));
		return $query->row_array();
	}
	
	public function getUserByUsernameAndPassword($data)
	{
		$query = $this->db->get_where('users', $data);
		return $query->row_array();
	}
	
	public function get_user($cond)
	{
		$query = $this->db->get_where('users', $cond);
		return $query->row_array();
	}
	
	
	public function add_user($data)
	{
		return $this->db->insert('users', $data);
	}
	
	public function update($data,$cond)
	{
		$this->db->where($cond);
		return $this->db->update('users', $data);
	}
	
	public function update_user($data,$userid)
	{
		$this->db->where('userid',$userid);
		return $this->db->update('users', $data);
	}
	
	public function delete($userid)
	{
		$this->db->delete('users', array('userid' => $userid));
		$this->db->delete('phones_users', array('userid' => $userid)); 
	}
    
    public function logout ()
	{
		$this->session->sess_destroy();
	}
    public function loggedin ()
	{
		return (bool) $this->session->userdata('logged_in');
	}
	
	public function getUserPermissions($cond)
	{
		$query = $this->db->get_where('user_permissions', $cond);
		return $query->result_array();
	}
	
	public function deleteUserPermissions($cond)
	{
		return $this->db->delete('user_permissions', $cond);
	}
	
	public function addUserPermission($data)
	{
		return $this->db->insert('user_permissions', $data);	
	}
	
}
