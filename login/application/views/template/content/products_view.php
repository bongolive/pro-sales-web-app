				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
 
  
<section class="container" role="main">
	 
    <div class="row">
    	  			<!-- Data block -->
					<article class="span12 data-block">
						<header>
							<h2><?php _l('product');
							echo ' : ' . strtoupper($tb_data['product_name']);
 ?></h2>
							<ul class="data-header-actions">
								<li><a href="<?php echo site_url('products'); ?>">Back to Products</a></li> 
								<li><a href="#">Export</a></li>
								<li><a href="#">Print</a></li>
							</ul>
						</header>
						<section>
  
							<!-- Grid row -->
							<div class="row-fluid">
								<div class="span6">
									<div class="well">
										<h4 class="no-margin"><?php _l('stock_info') ?></h4>
										<table class="table table-striped table-hover">
											<thead>
											<tr> 
												<th><?php _l('store_inv') ?></th><th><?php _l('purchase_price') ?><br /> <span style="color: rgb(41, 128, 185);">( <?php echo $this->settings['default_currency'] ?> )</span></th><th><?php _l('sale_price') ?> <br /> <span style="color: rgb(41, 128, 185);">( <?php echo $this->settings['default_currency'] ?> )</span></th><th><?php _l('margin') ?></th><th>Status</th>
											</tr>
											</thead>
										 	
										 	<tbody>
										 	<tr> 
										 		<td><?php echo $tb_data['items_on_hand'] ?> </td><td><?php echo money_format('%i', $stock['purchase_price']+0);?></td><td><?php echo money_format("%i",$tb_data['unit_sale_price'] + 0)?></td><td> <?php echo money_format('%i', $tb_data['unit_sale_price'] - $stock['purchase_price']  );?></td><td><?php if($tb_data['re_order_value'] <= $tb_data['re_order_value']){
										 			echo '<span class="label label-success">Available</span>';
													
										 		}else{ echo '<span class="label label-danger">Out of Stock</span>';} ?> </td>
										 	</tr>
                                            </tbody>
                                            </table>
                                            <!--starting warehousing-->
                                        <h4 class="no-margin"><?php _l('product_warehouse') ?></h4>
                                        <table class="table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <td></td><th><?php _l('wh_title') ?>
                                                </th> <th><?php _l('stock_qty') ?></th>
                                                <th><?php _l('created_on') ?></th>
                                                <th><?php _l('last_update') ?></th>
                                                <th><?php _l('expiry_date') ?></th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <?php $c = 0; foreach ($warehouse as $key => $value):
                                                $c = $c +1 ;?>
                                            <tr>
                                                <td><?php echo $c ?></td>
                                                <td><?php echo $value['warehouse'] ?> </td>
                                                <td><?php echo $value['stock_qty'] ?></td>
                                                <td><?php echo $value['created_on'] ?></td>
                                                <td><?php echo $value['last_update'] ?></td>
                                                <td><?php echo $value['expiry_date'] ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                            <!--finishing warehousing-->


									</div>
								</div>
								
								<div class="span6">
									<div class="well">
										<h4 class="no-margin"><?php _l('stock_details') ?> <a href="<?php echo site_url($edit).'/'.$tb_data['product_id']; ?>" class="btn btn-alt btn-primary" style="float: right">Edit Details</a></h4> 
										<table class="table table-striped table-hover">
											 
											<tr> 
												<td><?php _l('sku_product_no') ?></td><td><?php echo $tb_data['sku_product_no'] ?></td>
											</tr>
											<tr> 
												<td><?php _l('product_name') ?></td><td><?php echo $tb_data['product_name'] ?></td>
											</tr>
											<tr> 
												<td><?php _l('category') ?></td><td><?php echo $tb_data['category_name'] ?></td>
											</tr> 
										 
										  <tr>
												<td><?php _l('package_type') ?></td><td><?php echo $tb_data['package_name'] ?></td>
											</tr>
											 <tr> 
												<td><?php _l('package_units') ?></td><td><?php echo $tb_data['package_units'] ?></td>
											</tr>
											 <tr> 
												<td><?php _l('unit_size') ?></td><td><?php echo $tb_data['unit_size'].' ('.$tb_data['unit_measure'].')' ?></td>
											</tr>
											  
											<tr> 
												<td><?php _l('sale_price') ?></td><td><?php echo $tb_data['unit_sale_price']  ?><span style="color: rgb(41, 128, 185);">( <?php echo $this->settings['default_currency'] ?> )</span></td>
											</tr> 
											<tr> 
												<td><?php _l('tax_rate') ?></td><td><?php echo $tb_data['tax_rate'] ?></td>
											</tr>

                                            <tr>
                                                <td><?php _l('assigned_stock') ?></td><td><?php echo $tb_data['assigned_stock'] ?></td>
                                            </tr>

                                        </table>
									</div>
								</div>
								
 				 
							</div>
							<!-- /Grid row -->

	<div class="row-fluid">
								<div class="span6">
 
<h3><?php _l('latest_sales_orders');?></h3>

							<table class="table table-striped table-bordered table-condensed table-hover no-margin">
								<thead>
									<tr>
										<th>#</th><th><?php _l('order_date') ?></th>
										<th><?php _l('customer_name') ?></th>
										<th><?php _l('quantity') ?></th>
										<th><?php _l('sale_price') ?> <br /><span style="color: rgb(41, 128, 185);">( <?php echo $this->settings['default_currency'] ?> )</span></th>
										
										<th><?php _l('total') ?> <br /><span style="color: rgb(41, 128, 185);">( <?php echo $this->settings['default_currency'] ?> )</span></th>
									</tr>
								</thead>
								<tbody>
									<?php
									$total = 0;
									if($orders){
										foreach ($orders as $order) {
									  
									?>
									 
									<tr>
										<td>1</td>
										<td><?php echo $order['order_date'] ?></td>
										<td><?php echo $order['contact_person'] ?></td>
										<td><?php echo $order['order_qty'] ?></td>
										<td><?php echo $order['sale_price'] ?></td>
										<td><?php 
										$sale = $order['sale_price']*$order['order_qty'];
										echo money_format('%i',$sale) ?></td>
										 
									</tr>
									<?php 
										$total = $total + $sale;
										} }?>
									 
									  
									<tr>
										<td colspan="5"><strong><?php _l('total_sales') ?>:</strong></td>
										<td><strong><?php echo money_format('%i',$total) ?> </td></strong></td>
									</tr>
									<tr>
										<td colspan="5"><strong><?php _l('total_tax') ?>:</strong></td>
										<td><strong><?php echo money_format('%i',$total*0.18) ?></strong> </td>
									</tr>
									<tr>
										<td colspan="5"><strong><?php _l('grand_total') ?>:</strong></td>
										<td><strong><?php echo money_format('%i',$total+$total*0.18) ?></strong></td>
									</tr>

								</tbody>
							</table>
						</div>

<div class="span6">
<h3><?php _l('latest_puchases_orders');?> 
								<a  class="btn btn-primary" style="float: right"  href="<?php echo site_url('stock/addstock'); ?>"><?php _l('add_stock'); ?></a></h3>
							<table class="table table-striped table-bordered table-condensed table-hover no-margin">
								<thead>
									<tr>
										<th>#</th>
										<th><?php _l('puchase_date') ?></th>
										<th><?php _l('supplier_name') ?></th>
										<th><?php _l('quantity') ?></th>
										<th><?php _l('purchase_price') ?> <br /><span style="color: rgb(41, 128, 185);">( <?php echo $this->settings['default_currency'] ?> )</span></th> 
										<th><?php _l('total') ?> <br /><span style="color: rgb(41, 128, 185);">( <?php echo $this->settings['default_currency'] ?> )</span></th>
									</tr>
								</thead>
								<tbody>
									<?php
									$total_buy = 0; $c = 1;
									if($stocks_hist){
										foreach($stocks_hist as $stock){
										 
											$buy = $stock['purchase_price'] * $stock['stock_qty'];
									 
$total_buy = $total_buy + $buy;
									?>
									
									<tr>
										<td><?php echo $c ?></td>
										<td><?php echo $stock['stock_date'] ?></td>
										<td><?php echo $stock['supplier_name'] ?></td>
											<td><?php echo $stock['stock_qty'] ?></td>
											<td><?php echo money_format('%i', $stock['purchase_price'] )?></td>
											<td><?php 
											
											echo money_format('%i', $buy); ?></td>
									</tr>
									<?php $c++; }} ?>
								  
									<tr>
										<td colspan="5"><strong><?php _l('total_sales') ?>:</strong></td>
										<td><strong><?php echo money_format('%i', $total_buy) ?></strong></td>
									</tr>
									<tr>
										<td colspan="5"><strong><?php _l('total_tax') ?>:</strong></td>
										<td><strong><?php echo money_format('%i', $total_buy * 0.18) ?></strong> </td>
									</tr>
									<tr>
										<td colspan="5"><strong><?php _l('grand_total') ?>:</strong></td>
										<td><strong><?php echo money_format('%i', $total_buy + $total_buy * 0.18) ?></strong></td>
									</tr>

								</tbody>
							</table>
						</div>
</div>

						</section>
					</article>
					<!-- /Data block -->
					
				</div>
				<!-- /Grid row -->

			</section>
			<!-- /Main page container -->
 
</div>
