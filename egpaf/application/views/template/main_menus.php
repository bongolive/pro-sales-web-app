                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                        	
                          <i class="ion ion-person"></i>
                        </div>
                        <div class="pull-left info">
                          <p> 	<?php echo $this->session->userdata['username'].' - '.$this->session->userdata['role_title']; ?>
									 </p>
                            <a href="<?php echo site_url('login/logout'); ?>"><i class="fa fa-circle text-success"></i>Log Out</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="<?php echo site_url('dashboard') ?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <?php if(in_array(2, $this->user_permissions)){  ?>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span><?php echo lang('surveys'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                        <?php if(in_array(3, $this->user_permissions)){  ?> <li>
                            <a href="<?php echo site_url('surveys/create') ?>">
                                <i class="fa fa-pencil-square-o"></i> <span><?php  echo lang('surveys_create') ?></span> 
                            </a>
                        </li> <?php } ?>
                        <li>
                            <a href="<?php echo site_url('surveys') ?>">
                                <i class="fa fa-th"></i> <span><?php  echo lang('surveys_list') ?></span> 
                            </a>
                        </li>
                        </ul></li>
                         <?php } ?>
                          <?php if(in_array(6, $this->user_permissions)){  ?>
                        <li class="treeview">
                            <a href="#">
                                   <i class="fa  fa-map-marker "></i></i> <span><?php echo lang('locations'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                        <li>
                            <a href="<?php echo site_url('locations/create') ?>">
                                <i class="fa fa-pencil-square-o"></i> <span><?php  echo lang('create_location') ?></span> 
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('locations') ?>">
                                <i class="fa fa-th"></i> <span><?php  echo lang('location_list') ?></span> 
                            </a>
                        </li>
                        </ul></li> <?php } ?>
                         <?php if(in_array(4, $this->user_permissions)){  ?>
                        <li>
                            <a href="<?php echo site_url('devices') ?>">
                                <i class="fa fa-mobile"></i>
                                  <span><?php  echo lang('devices') ?></span> 
                            </a> 
                        
                        </li>  <?php } ?>
                        <!--<?php if(in_array(3, $this->user_permissions)){  ?>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-users"></i> <span><?php echo lang('questionaires'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                            	<li><a href="<?php echo site_url('questionaires/create'); ?>"><i class="fa  fa-users"></i><?php echo lang('new_question'); ?></a></li>
                                <li><a href="<?php echo site_url('questionaires'); ?>"><i class="fa fa-user"></i></span><?php echo lang('questionaires_list'); ?></a></li> 
                            </ul>
                        </li><?php } ?> -->
                        
                         <!--<li>
                            <a href="<?php echo site_url('reports') ?>">
                                <i class="fa  fa-clipboard"></i>
                                  <span><?php  echo lang('reports') ?></span>  
                            </a> 
                        </li>-->
                          <?php if(in_array(7, $this->user_permissions)){  ?> 
                            <li class="treeview">
                            <a href="#">
                                <i class="fa fa-book"></i> <span><?php echo lang('reports'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                            	<li><a href="<?php echo site_url('reports/survey'); ?>"><i class="fa fa-file-text"></i><?php echo lang('survey_report'); ?></a></li>
                            	<li><a href="<?php echo site_url('reports/locations'); ?>"><i class="fa  fa-map-marker"></i><?php echo lang('locations_report'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/surveyor'); ?>"><i class="fa fa-mobile"></i></span><?php echo lang('surveyor_report'); ?></a></li> 
                            </ul>
                        </li>
                         <?php } ?>
                          <?php if(in_array(1, $this->user_permissions)){  ?> 
                          
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-users"></i> <span><?php echo lang('people'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                            	<li><a href="<?php echo site_url('employees'); ?>"><i class="fa  fa-users"></i><?php echo lang('employees'); ?></a></li>
                                <li><a href="<?php echo site_url('users'); ?>"><i class="fa fa-user"></i></span><?php echo lang('users'); ?></a></li> 
                            </ul>
                        </li> <?php } ?>
                         <?php if(in_array(5, $this->user_permissions)){  ?>
                          <li>
                            <a href="<?php echo site_url('settings') ?>">
                                <i class="fa fa-gears (alias)"></i>
                                  <span><?php  echo lang('settings') ?></span> 
                            </a> 
                        </li>
                        <?php } ?>
                    </ul>
                </section>