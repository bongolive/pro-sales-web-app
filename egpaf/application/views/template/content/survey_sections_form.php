<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php
									if ($info) {
										
										echo lang('update_survey') . ' : ' . strtoupper($info['survey_title']);
										$action = 'surveys/update';
									} else {
										echo lang('new_section');
										$action = 'surveys/create';
									}
							 ?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                 
                                	  <form role="form" method="post" action="<?php echo site_url($action); ?>">
                                	  	<div class="box-body">
                                	                                	<?php if($info){ ?>
                                		<input name="section_id" id="section_id" class="form-control" type="hidden"  value="<?php  echo $info['section_id']; ?>" />
                                		<?php } ?> 
                                       
                                        <div class="form-group">
                                        	<label class="control-label" for="section_title" ><?php echo lang('survey_title'); ?></label>
                                        	<?php echo form_error('survey_title'); ?>
                                        	<input name="survey_title" id="section_title" class="form-control" type="text"  value="<?php
											if ($info) { echo $info['survey_title'];
											} else { $value = set_value();
											}
 ?>" required />
                                        
                                    	</div> 
                                    	  
                                    	         
								</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('surveys'); ?>" type="button" class="btn btn-default" data-dismiss="modal">Back</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div></form>
								</div>
								</div>
								</div>
								</section>