<!-- Grid row -->
<div class="row">

	<!-- Data block -->
	<article class="span12 data-block">
		<section class="filter-bar">

			<form class="form-inline" method="post" action="<?php echo site_url($controller) ?>">
				<fieldset>
					<!--sales team-->
					<?php if($controller =='customers/sales_team' || $controller =='stock/sales_team' || $controller =='employees' || $controller =='orders'): ?>

					<!--<div class="control-group">
					<label class="control-label">Device Imei</label>
					<div class="controls">
					<div class="input-prepend">
					<span class="add-on"><i class="icon-phone"></i></span><input class="input-medium" type="date" name="device" id="" />
					</div>
					</div>
					</div> -->

					<div class="control-group">
						<label class="control-label"><?php _l('employee') ?></label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-person"></i></span>
								
								<?php
								if(!$sales_team){
								 $sales_team[] = "No Sales Team Available";
								}
								echo form_dropdown('employee', $sales_team, '', 'class="input-medium"'); ?>
							</div>
						</div>
					</div>

					<?php endif; ?>

					<?php if($controller =='orders'): ?>

					<div class="control-group">
						<label class="control-label"><?php _l('dates');?></label>
						<div class="controls">
							<div class="input-prepend">
								<!--<span class="add-on"><i class="icon-calendar"></i></span>-->
								<input class="input-medium" type="text" placeholder="date range" name="order_date" id="demoDaterangePicker" />
							</div>
						</div>
					</div>
<div class="control-group">
						<label class="control-label"><?php _l('payment_status') ?></label>
						<div class="controls">
							 
								<!--<input id="optionsCheckbox" type="checkbox" class="input-small" type="checkbox" name="payment_status" id="" />--->
								<?php
								$status = array(''=>'Select','outstanding'=>'Not Issued','complete'=>'Issued');
								 echo form_dropdown('payment_status',$status,'','class="input-medium"') ?>    
							 </div> 
					</div>
					<!--
					<div class="control-group">
					<label class="control-label">Pay Status</label>
					<div class="controls">
					<div class="input-prepend">
					<span class="add-on"><i class="icon-money"></i></span>
					<input class="input-medium" type="checkbox" name="payment_status" id="" />
					</div>
					</div>
					</div> -->
					<?php  endif; ?>
					<!--custoemrs -->
					<?php if($controller =='customers' || $controller =='orders'): ?>

					<!--<div class="control-group">
					<label class="control-label">Contact Person</label>
					<div class="controls">
					<div class="input-prepend">
					<span class="add-on"><i class="icon-person"></i></span><input class="input-medium" type="text" name="contact_person" id="" />
					</div>
					</div>
					</div>-->

					<div class="control-group">
						<label class="control-label"><?php _l('business_name') ?></label>
						<div class="controls">
							<div class="input-prepend">
								<!--<span class="add-on"><i class="icon-person"></i></span>-->
								<?php
								if(!$business){							
								 $business[] = "No Customers Available"; 
								} 
								 echo form_dropdown('business_name', $business, '', 'class="input-medium"');
								?>
							</div>
						</div>
					</div>
					<?php endif; ?>

					<!--employees & devices

					<?php if($controller ==''): ?>

					<div class="control-group">
					<label class="control-label">Employee name</label>
					<div class="controls">
					<div class="input-prepend">
					<span class="add-on"><i class="icon-person"></i></span><input class="input-medium" type="text" name="employee_name" id="" />
					</div>
					</div>
					</div>

					<?php endif; ?>

					<!--stock and sales-->

					<?php if($controller =='stock'): ?>
					<div class="control-group">
						<label class="control-label"><?php _l('date') ?></label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-calendar"></i></span>
								<input class="input-medium" type="date" name="stock_date" id="" />
							</div>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label"><?php _l('product_name') ?></label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-person"></i></span>
								<?php
									if(!$products){							
								 $products[] = "No Products Available"; 
								} 
								 echo form_dropdown('product_name', $products); ?>
							</div>
						</div>
					</div>
					<?php endif; ?>

					<!--products-->
					<?php if($controller =='products'): ?>
					<div class="control-group">
						<label class="control-label"><?php _l('product_category') ?></label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-bucket"></i></span>
								<?php
								if(!$categories){							
								 $categories[] = "No Products Categories Available"; 
								} 
								 echo form_dropdown('category', $categories); ?>
							</div>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label"><?php _l('product_name') ?></label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-cart"></i></span>
								<input class="input-medium" type="text" name="product_name" id="" />
							</div>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label"><?php _l('supplier_name') ?></label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-cart"></i></span>
								<input class="input-medium" type="text" name="supplier" id="" />
							</div>
						</div>
					</div>
					<?php endif; ?>

					<!--products |payments -->
					<?php if($controller =='payments'): ?>
					<div class="control-group">
						<label class="control-label"><?php _l('order_date') ?></label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-person"></i></span>
								<input class="input-medium" type="text" name="order_date" id="demoDaterangePicker" placeholder="date range"   />
							</div>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label"><?php _l('order_number') ?></label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-person"></i></span>
								<input class="input-medium" type="text" name="order_no" id="" />
							</div>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label"><?php _l('business_name')?></label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-person"></i></span>
								<input class="input-medium" type="text" name="business_name" id="" />
							</div>
						</div>
					</div>

					<?php endif; ?>
					<div class="control-group">
						<div class="controls">
							<input type="submit" class="btn btn-alt" name="filter"  value="Filter"/>
							<a href="<?php echo site_url($controller); ?>" class="btn btn-alt" title="Apply filter"> View All </span></a>
						</div>
					</div>
				</fieldset>
			</form>

		</section>
	</article>
	<!-- /Data block -->

</div>
<!-- /Grid row -->