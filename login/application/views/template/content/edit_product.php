                    <div class="modal-header">

									<h4><?php _l('update_product'); ?></h4>

								</div>

                                <form class="form-horizontal" action="<?php echo site_url("stocks/updateProduct"); ?>" method="post">
									 <?php foreach ($products as $product): ?>
                                    <fieldset>
                                        <div class="control-group">
                                        	<label class="control-label" for="product_name" ><?php _l('product_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="product_name" id="product_name" class="input-xlarge" type="text" value="<?php echo $product['product_name']; ?>" required />
                                        	<input name="userid" type="hidden" id="userid" value="<?php echo $product['user_id'];?>" />
                                        	</div>
                                    	</div>
										<div class="control-group">
                                        	<label class="control-label" for="description" ><?php _l('description'); ?></label>
                                        	<div class="controls">
                                        	<textarea name="description" id="description" class="input-xlarge" rows="5" required><?php echo $product['description']; ?></textarea>
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="items_on_hand"><?php _l('items_on_hand'); ?></label>
                                        	<div class="controls">
                                        	<input name="items_on_hand" id="items_on_hand" class="input-xlarge" type="number" value="<?php echo $product['items_on_hand']; ?>" required />
                                        	</div>
                                    	</div>            
								</div>
								<div class="modal-footer">
									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>
									<div class="form-actions">
            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;margin-left: 172px;padding: 6px 10px;">Submit</button>
                                    </div>
								</div>
                            </fieldset>
							<?php endforeach; ?>
                        </form>
