<?php
class Boundry_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_boundryById($boundryId)
	{
		$query = $this->db->get_where('boundries', array('boundryid' => $boundryId));
		return $query->result_array();
	}
	
	public function get_boundryByUserId($userid = FALSE)
	{
		if ($userid === FALSE)
		{
			$query = $this->db->query("SELECT b.*, (SELECT COUNT(ba.alocationid) FROM  boundry_alocation ba WHERE ba.boundryid =  b.boundryid) AS devices,u.name as client_name FROM boundries b,users u  WHERE u.userid=b.userid  ORDER BY b.alertname ASC");
			return $query->result_array();
		}
	
		$query = $this->db->query("SELECT b.*, (SELECT COUNT(ba.alocationid) FROM  boundry_alocation ba WHERE ba.boundryid =  b.boundryid) AS devices,u.name as client_name FROM boundries b,users u WHERE u.userid=b.userid and b.userid = ".$userid." ORDER BY b.alertname ASC");
		return $query->result_array();
	}
	
	public function add_boundry($data)
	{
		$this->db->insert('boundries', $data);
		return $this->db->insert_id();
	}
	
	public function update_boundry($data,$boundryid)
	{
		$this->db->where('boundryid',$boundryid);
		return $this->db->update('boundries', $data);
	}
	
	function get_boundry_alocationByBoundryId($boundry_id)
	{
		$this->db->select('imei');
		$query = $this->db->get_where('boundry_alocation', array('boundryid ' => $boundry_id));
		return $query->result_array();
	}
	
	
	public function add_boundry_alocation($data)
	{
		return $this->db->insert('boundry_alocation', $data);
	}
	
	public function delete_boundry_alocation($data)
	{
		$this->db->where($data);
		return $this->db->delete('boundry_alocation');
	}
	
	public function update_boundry_alocation($data,$cond)
	{
		$this->db->where($cond);
		return $this->db->update('boundry_alocation',$data);
	}
	
	public function delete($boundryid)
	{
		$this->db->delete('boundries', array('boundryid' => $boundryid));
		return $this->db->delete('boundry_alocation', array('boundryid' => $boundryid));
	}
	
	
	public function get_outOfBoundary($curr_date)
	{
		$query = $this->db->query("SELECT u.name, u.email, p.device_name, p.mobile_no, p.holder_name, ba.alocationid, ba.imei, b.alertname, b.centerlat, b.centerlng, b.radius, b.valid_from, b.valid_to, ba.laststatus, ifNull(( SELECT l.latitude FROM location l WHERE l.imei = ba.imei ORDER BY l.servertime DESC LIMIT  1 ),NULL) AS devicelat , ifNull(( SELECT l.longitude FROM location l WHERE l.imei = ba.imei ORDER BY l.servertime DESC LIMIT  1 ),NULL) AS devicelng FROM boundries b INNER JOIN boundry_alocation ba ON b.boundryid = ba.boundryid INNER JOIN users u ON b.userid = u.userid INNER JOIN phones p ON ba.imei = p.phone_imei WHERE b.valid_from <= '".$curr_date."' AND b.valid_to >= '".$curr_date."' ORDER BY b.alertname, u.name ASC");
		return $query->result_array();
	}

	

}
