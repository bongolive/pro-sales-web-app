<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Orders extends User_Controller {
	public $user_id;
	public $account_id;

	public function __construct() {
		parent::__construct();

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('order_date', 'contact_person', 'business_name', 'sales_total', 'payment_status', 'order_status', 'staff');

		$this -> fields['ft_data'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js', 'plugins/sparkline/jquery.sparkline.min.js');

		if (!$this -> session -> userdata('logged_in') == TRUE) {
			$this -> session -> set_userdata('login_error', 'Please, Loggin first');
			redirect('login');
		}

		$this -> load -> model('orders_model');
		$this -> load -> model('stock_model');
		$this -> load -> model('employees_model');
		$this -> load -> model('customers_model');
		$this -> load -> model('products_model');
        $this -> load -> model('transaction_model') ;

		$this -> fields['table_id'] = $this -> orders_model -> table_id;

		$this -> orders_model -> form_validation();
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');

		$this -> user_id = $this -> session -> userdata('userid');
		$this -> account_id = $this -> account_id;

		$this -> fields['controller'] = 'orders';
		$this -> fields['edit'] = 'orders/update_order';
		$this -> fields['view'] = 'orders/view';
		$this -> fields['delete'] = 'orders/delete';

	}



    public function index() {
		$this -> session -> unset_userdata('id');

		$where = array('sales_orders.account_id' => $this -> account_id);

		$customers = $this -> customers_model -> read(array('account_id' => $this -> account_id));
		if ($customers) {
			$this -> fields['tb_data'] = $customers;
		} else { $this -> fields['tb_data'] = FALSE;
		}
		$bs_ness[''] = "Select Business Name";
		if ($customers) {
			foreach ($customers as $business) {
				$bs_ness[$business['customer_id']] = $business['business_name'];
			}
		} else {
			$bs_ness[] = "No Customers Available";
		}
		$this -> fields['business'] = $bs_ness;

		$employee = $this -> employees_model -> read(array('account_id' => $this -> account_id));
		$emp[''] = 'Select Staff';
		if ($employee) {
			foreach ($employee as $employee) {
				$emp[$employee['id']] = $employee['firstname'] . ' ' . $employee['lastname'];
			}
		} else {
			$emp[] = "No Sales Team Available";
		}

		$this -> fields['sales_team'] = $emp;

		if ($this -> input -> post('filter')) {
			extract($_POST);
			IF ($employee) {
				$where['employee_id'] = $employee;
			}
			if ($payment_status) {
				$where['paid_amount !='] = 0;
			}
			if ($order_date) {

				$dd = explode('-', $order_date);

				$where['order_date >='] = $dd[0];
				$where['order_date <='] = $dd[1];
			}
			if ($business_name) {
				$where['sales_orders.customer_id'] = $business_name;
			}
		}

		$this -> session -> set_userdata($where);
		$order = $this -> orders_model -> read_order($where, $this -> limit, $this -> startpoint);

		if ($order) {
			$o_count = $this -> orders_model -> mycounter();

			//this will display the links to pages
			$this -> fields['pagenate'] = $this -> pagination($o_count, $this -> limit, $this -> page);
			$this -> fields['tb_data'] = $order;
		} else {
			$this -> fields['tb_data'] = FALSE;
		}
		$this -> fields['tb_name'] = 'order_tb_name';

		$header['stc_active'] = 'class="active"';
		$this -> fields['add_btn'] = 'add_new_order';

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/orders_table', $this -> fields);
		$this -> load -> view('template/footer');
	}

	public function export()
    {

        $row_fields = array('order_date', 'contact_person', 'business_name', 'sales_total', 'payment_status', 'order_status');
        $where = array('sales_orders.account_id' => $this->account_id);

        if ($this->input->post('filter')) {

            IF ($this->session->userdata('employee')) {
                $where['employee_id'] = $this->session->userdata('employee');
            }
            IF ($this->session->userdata('payment_status')) {
                $where['paid_amount !='] = $this->session->userdata('payment_status');
            }
            IF ($this->session->userdata('order_date >=')) {

                $where['order_date >='] = $this->session->userdata('order_date >=');
                $where['order_date <='] = $this->session->userdata('order_date <=');
            }
            IF ($this->session->userdata('business_name')) {
                $where['sales_orders.customer_id'] = $this->session->userdata('business_name');
            }
        }
        $tb_data = $this->orders_model->read_order($where, $this->limit, $this->startpoint);

        if ($tb_data) {
            $rows = $details = array();
            foreach ($tb_data as $data) :

                foreach ($row_fields as $field) :
                    if ($field == 'balance') {
                        $details[$field] = $data['sales_total'] - $data['paid_amount'];
                    } else {
                        $details[$field] = $data[$field];
                    }
                endforeach;
                $rows[] = $details;
            endforeach;
            /*
             echo "<pre>";
             print_r($rows); */
            $this->excel_export($rows, "Paymets");
        } else {
            redirect('orders');
        }

    }

	public function new_order() {

		if ($this -> form_validation -> run() == FALSE) {

			$header = FALSE;
			$where = array('account_id' => $this -> account_id);

			//get products
			$products = $this -> products_model -> read($where);
			if ($products) {
				$product[] = 'Select Product';
				foreach ($products as $products) {
					$product[$products['product_id']] = $products['product_name'];
				}
			} else {
				$product = FALSE;
			}

			//get products

//            $unitprice = $this->products_model ->fetch_unit_price($products['product_id']);

			$where_emp = array('employees.account_id' => $this -> account_id, 'emp_role' => 'sales', 'emp_status' => 1);

			$employees = $this -> stock_model -> read_sales_team($where_emp);

			if ($employees) {
				$employee[] = 'Select Sales Person';
				foreach ($employees as $employees) {
					$employee[$employees['id']] = $employees['firstname'] . ' ' . $employees['lastname'];
				}
			} else {

				$employee[] = 'No Sales Team';
			}

			$this -> fields['order'] = $this -> fields['order_details'] = FALSE;//concatinating the array['order']
			// to the array order_details

			//get customers
			$customers = $this -> customers_model -> read($where);
			if ($customers) {
				foreach ($customers as $customer) {
					$cust[$customer['customer_id']] = $customer['business_name'];
				}

			} else {
				$cust = FALSE;
			}

			$this -> fields['customers'] = $cust;

			$this -> fields['products'] = $product;
			$this -> fields['employees'] = $employee;

			$this -> load -> view('template/header', $header);
			$this -> load -> view('template/content/orders_form', $this -> fields);
			$this -> load -> view('template/footer');

		} else {
			extract($_POST);
			$date = date('Y-m-d h:i:s');
			echo "<pre>";

			//get the employee details
			$where_team = array('employees.id' => $assigned_to);
			$sales_team = $this -> stock_model -> read_sales_team($where_team);
			foreach ($sales_team as $team) {
			}

			//check if mult products inserted
			$deviceorderid = microtime(true) . '/' . $this -> user_id;
			$order = array('employeeid' => $assigned_to, 'device_order_id' => $deviceorderid, 'order_date' => $order_date, 'customer_id' => $customer_id, 'created_on' => $date, 'delivery_date' => $delivery_date, 'account_id' => $this -> account_id, 'phone_imei' => $team['device_imei'], 'created_by' => $this -> user_id, 'order_status' => 1);

			//print_r($order);

			$this -> db -> trans_start();
			$this -> orders_model -> save($order);
			$order_id = $this -> db -> insert_id();

			$sales_total = 0;
			for ($p = 0; $p < count($product_id); $p++) {

				//get products prices
				$where_stock = array('product_id' => $product_id[$p]);
				$stocks = $this -> products_model -> read($where_stock);
				foreach ($stocks as $stock) {
				}
				$sales_total = $sales_total + ($stock['unit_sale_price'] * $order_qty[$p]);

				$order_details = array('order_id' => $deviceorderid, 'phone_imei' => $team['device_imei'], 'product_id' => $product_id[$p], 'order_qty' => $order_qty[$p], 'sale_price' => $stock['unit_sale_price'], 'sale_tax' => $stock['tax_rate'], 'account_id' => $this -> account_id);
				//	print_r($order_details);
				$this -> orders_model -> save_order_details($order_details);
			}
			$this -> db -> trans_complete();

			//update order sales total
			$orderV = array('sales_total' => $sales_total);
			$this -> orders_model -> updateorder($order_id, $orderV);

			redirect('orders');

		}

	}


	public function update_order() {

		if ($this -> form_validation -> run() == FALSE) {

			if ($this -> session -> userdata('id')) {
				$id = $this -> session -> userdata('id');
			} else {
				$id = $this -> uri -> segment(4);
				$this -> session -> set_userdata('id', $id);
			}

			$header = FALSE;
			$where = array('account_id' => $this -> account_id);

			//get products
			$products = $this -> products_model -> read($where);

			if ($products) {
				$product[] = 'Select Product';
				foreach ($products as $products) {
					$product[$products['product_id']] = $products['product_name'];
				}
			} else {
				$product = FALSE;
			}

			//get products

			$where_emp = array('employees.account_id' => $this -> account_id, 'emp_role' => 'sales', 'emp_status' => 1);

			$employees = $this -> stock_model -> read_sales_team($where_emp);

			if ($employees) {
				$employee[] = 'Select Sales Person';
				foreach ($employees as $employees) {
					$employee[$employees['id']] = $employees['firstname'] . ' ' . $employees['lastname'];
				}
			} else {

				$employee[] = 'No Sales Team';
			}
			//get order details

			$where_o = array('order_id' => $id);
			$order = $this -> orders_model -> read($where_o);

			foreach ($order as $order) {
				$this -> fields['order'] = $order;
			}
			$order_details = array('order_id' => $order['device_order_id']);
			$order_details = $this -> orders_model -> read_order_details($order_details);

			if ($order_details) {

				$this -> fields['order_details'] = $order_details;

			} else {
				$this -> fields['order_details'] = FALSE;
			}
			//get customers
			$customers = $this -> customers_model -> read($where);
			if ($customers) {
				foreach ($customers as $customer) {
					$cust[$customer['customer_id']] = $customer['business_name'];
				}

			} else {
				$cust = FALSE;
			}
			$this -> fields['customers'] = $cust;

			$this -> fields['products'] = $product;
			$this -> fields['employees'] = $employee;

			$this -> load -> view('template/header', $header);
			$this -> load -> view('template/content/orders_form', $this -> fields);
			$this -> load -> view('template/footer');

		} else {
			extract($_POST);
			$date = date('Y-m-d h:i:s');
			 echo "<pre>";
			//print_r($product_id);
			//get the employee details
			$where_team = array('employees.id' => $assigned_to);
			$sales_team = $this -> stock_model -> read_sales_team($where_team);
			foreach ($sales_team as $team) {
			}

			//check if mult products inserted
			$order = array('employee_id' => $assigned_to, 'order_date' => $order_date, 'customer_id' => $customer_id, 'created_on' => $date, 'delivery_date' => $delivery_date, 'account_id' => $this -> account_id, 'phone_imei' => $team['device_imei'], 'created_by' => $this -> user_id);

			// print_r($order);

		 	if ($this -> orders_model -> update($order_no, $order)) {
				//get order details
				$order = $this -> orders_model -> read(array('order_id' => $order_no));
 
				//remove all order details

			 	$this -> orders_model -> remove_details($order[0]['device_order_id']);
				$sales_total = 0;
				$p_count = count($product_id);
				print_r($product_id);
				for ($p = 0; $p < count($product_id); $p++) {

					//get products prices
					$where_stock = array('product_id' => $product_id[$p]);
					$stocks = $this -> products_model -> read($where_stock);
					if ($stocks) {
						foreach ($stocks as $stock) {
						}
					} else {
						$stock = 0;
					}
					$sales_total = $sales_total + ($stock['unit_sale_price'] * $order_qty[$p]);
			 
					$order_details = array('order_id' => $order[0]['device_order_id'], 'phone_imei' => $team['device_imei'], 'product_id' => $product_id[$p], 'order_qty' => $order_qty[$p], 'sale_price' => $stock['unit_sale_price'], 'sale_tax' => $stock['tax_rate'], 'account_id' => $this -> account_id);
					// print_r($order_details);
					$this -> orders_model -> save_order_details($order_details);
				}
		 

				//update order sales total
				$orderV = array('sales_total' => $sales_total);
			//	print_r($orderV);
			 	$this -> orders_model -> updateorder($order_no, $orderV);

		 	}
		 	redirect('orders/view/' . $order_no);

		}

	}

    public function print_receipt()
    {

    }

	public function view() {
		$this -> fields['order_hd'] = array('business_name', 'order_date', 'payment_status', 'payment_mode', 'order_status', 'delivery_date','trans_uniq_id');
		
		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('product_name', 'order_qty', 'sale_price', 'sale_tax', 'total_sales', 'total_tax');

		$id = $this -> uri -> segment(4);

        $transactiondetails = $this->transaction_model->getTransactionId($id);
        $this->fields['transactionvalue'] = $transactiondetails;

		$where = array('order_id' => $id);

		$order = $this -> orders_model -> read_order($where);
 
		if ($order) {

			foreach ($order as $order) {
			}

			$this -> fields['order_info'] = $order;

			//get the history orders details
			$where_old = array('customer_id' => $order['customer_id']);
			$old_orders = $this -> orders_model -> read($where_old);
			
			if ($old_orders) {
				$this -> fields['old_orders'] = $old_orders;
			} else {
				$this -> fields['old_orders'] = FALSE;
			}

			//get  this order details
			$g_order = array('order_id' => $order['device_order_id']);
			$order_details = $this -> orders_model -> read_order_details($g_order, $this -> limit, $this -> startpoint);
			if ($order_details) {

				$o_count = count($order_details[0]);
				//$this->orders_model->mycounter();

				//this will display the links to pages
				$this -> fields['pagenate'] = $this -> pagination($o_count, $this -> limit, $this -> page);

				$this -> fields['tb_data'] = $order_details;
			} else {

				$this -> fields['tb_data'] = FALSE;
			}

		} else {

			$this -> fields['order_info'] = $this -> fields['tb_data'] = FALSE;
		}
		$this -> fields['tb_name'] = 'order_tb_name';

		$header['stc_active'] = 'class="active"';
		$this -> fields['add_btn'] = 'add_new_order';

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/orders_view', $this -> fields);
		$this -> load -> view('template/footer');
	}

	public function process($id) {
		$status = array('order_status' => 2);
		$this -> orders_model -> update($id, $status);
		redirect('orders');

	}

	public function delete($id) {
		$this -> orders_model -> delete($id);
		redirect('orders');
	}

    public function locate($id) {


        $data = $this -> orders_model -> locate($id);
        $this->fields['lat'] = $data->lat;
        $this->fields['lng'] = $data->lng ;
        $this->fields['order'] = $id;

        $this -> load -> view('template/header');
        $this -> load -> view('template/content/orders_location', $this -> fields);
        $this -> load -> view('template/footer');
    }

	public function checkproduct($product) {
	 
		if ($product == 0) {
			$this -> form_validation -> set_message('checkproduct', 'select at least one product');

			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function check_deliverydate($date) {
		$date = strtotime($date);
		$today = strtotime(date('Y-m-d'));

		if ($date <= $today) {
			$this -> form_validation -> set_message('check_deliverydate', 'Incorect Delivery Date');

			return FALSE;
		}
	}

}
