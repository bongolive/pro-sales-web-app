<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Settings extends My_Controller {

	public $settings_dsc = array('id', 'account_no', 'active_survey', 'skip_value', 'default_language', 'sms_report');
	public $user_id;
	public $account_id;
	public function __construct() {

		parent::__construct();

		/*	if (!$this -> session -> userdata('is_login') == TRUE) {
		 $this -> session -> set_userdata('login_error', 'Please, Loggin first');
		 redirect('login');
		 }*/
		$this -> load -> model('settings_model');
		$this -> load -> model('permissions_model');

		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');

		$this->data['row_fields'] = $this -> settings_dsc;
		$this->data['tb_headers'] = $this -> settings_dsc;
		$this->data['tb_name'] = 'all_settings';
		$this->data['controller'] = 'settings';
		$this->data['view'] = 'settings/view/';
		$this->data['edit'] = 'settings/update/';
		$this->data['delete'] = 'settings/delete/';

		$this->data['tb_name'] = 'all_settings';
		$this->data['controller'] = 'settings';
		$this -> user_id = 1;
		$this -> account_id = 1;
	}

	public function index() {

		//get currencies

		// $currencies = json_decode(file_get_contents('http://www.localeplanet.com/api/auto/currencymap.json'),true);
		//foreach ($currencies as $currencies) {	}
		// echo "<pre>";	print_r($currencies);
		//fetch available settings
		$where = array('account_no' => $this -> account_id);

		$info = $this -> settings_model -> read($where);
		
		if ($info) {
			$this->data['info'] = $info;
		} else {
			$this->data['info'] = FALSE;
		}
		$surveys = $this -> settings_model -> get_surveys($where);
		if ($surveys) {
			$surv[''] = 'Select Active survey';
			foreach ($surveys as $survys) {
				$surv[$survys['survey_id']] = $survys['survey_title'];
			}
			$this->data['surveys'] = $surv;
		} else {
			$this->data['surveys'] = array('' => 'No Surveys Available');
		}
		$wheres = array('account_id'=>$this->account_id);
		$this->data['profile'] = $this -> settings_model -> read_profile($wheres);

		//settings
		$settings = $this -> settings_model -> read_settings($where);
		if ($settings) {
			foreach ($settings as $settings) {
				$this->data['settings'] = $settings;
			}
		} else {$this->data['settings'] = FALSE;
		}
		$this->data['currency'] = FALSE;
		//$this -> settings_model -> read_currency();
		$this -> load -> view('template/header',$this->data);
		$this -> load -> view('template/content/settings_form');
		$this -> load -> view('template/footer');

	}

	public function update() {

		extract($_POST);

		if ($sms_report) { $sms_report = 1;
		} else { $sms_report = 0;
		}
  
		$settings = array('active_survey' => $active_survey,'cap_size'=>$cap_size, 'server_url'=>$server_url, 'skip_value'=>$skip_value, 'default_language'=>$default_language,    'sms_report' => $sms_report,);
 
		 
		 $this -> settings_model -> update($this -> session -> userdata('account_id'), $settings);
		 $this -> session -> set_userdata('saved', TRUE);

		 redirect('settings');

	}

	public function update_profile() {

		extract($_POST);

		$settings = array('company_name' => $company_name, 'mobile' => $mobile, 'email' => $email, 'area' => $area, 'vat_no' => $vat_no, 'tin_no' => $tin_no, 'city' => $city, 'street' => $street);

		//updating
		$this -> settings_model -> update_profile($this -> account_id, $settings);
		$this -> session -> set_userdata('saved', TRUE);

		redirect('settings');

	}

}
