<!-- Main content -->
<device class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<div class="row">
						<div class="col-md-12">
							<h3 class="page-header box-title"> <?php echo lang('devices_list') ?> </h3>
						 
							<!--<a class="btn btn-primary" type="button" href="<?php echo site_url('reports/survey_report/'.$survey['device_id']) ?>" style="float: right;margin: 10px;"> <?php echo lang('report'); ?></a>-->
							<!--<a class="btn btn-primary" type="button" href="<?php echo site_url('devices/create_device/'.$survey['device_id']) ?>" style="float: right;margin: 10px;"> <?php echo lang('new_device'); ?></a>
							<a class="btn btn-info"  type="button" href="<?php echo site_url('devices') ?>" ;"="" style="float: right; margin: 10px;"> <?php echo lang('back_to_devices'); ?></a>-->
							</div></div>
							</div>

							</div> </div> </div>

							<div class="row">
							<div class="col-md-8">
							<div class="box box-primary">
							<div class="box-header">

							<h4 class="page-header box-title"> <?php echo lang('devices_list') ?>   </h4> </div>

							<div class="box-body " >
   <div class="box-body table-responsive">
                                    <table id="type2" class="table table-bordered table-striped">
                                        <thead>							<tr>
							<?php foreach($tb_headers as $header){?>

							<th><?php echo lang($header); ?></th>

							<?php } ?>
							<th></th>
							</tr>
							</thead>
							<tbody>

							<?php if ($tb_data){
							foreach ($tb_data as $data):
							$id = $data['device_id'];
							?>
							<tr class="even gradeC" id="<?php echo $id; ?>">

							<?php foreach ($row_fields as $field): ?>
							<td><?php
							if ($field == 'status') {
								if ($data['device_status'] == 1) {
									echo 'Active';
								} elseif ($data['device_status'] == 0) { echo "Disabled";
								}

							} elseif ($field == 'assigned_users') {
								if ( $data[$field]) {	
								$users = explode('|', $data[$field]);
								
									echo '<ol>';
									foreach ($users as $user) {
										echo '<li>' . ucwords($user) . '</li>';
									}
									echo "</ol>";
								}else{
									echo 'No users Assigned';
								}
							} else {
								echo ucwords($data[$field]);
							}
							?></td>
							<?php endforeach; ?>

							<td>
							<div class="btn-group">
					  
                            	<a href="<?php echo site_url($edit) . '/' . $data['device_id']; ?>" class="btn btn-small btn-info " title="Edit"><i class="fa fa-pencil"></i></a>
                            	
                            	<?php if($data['device_status']==1){?>
                            	<a href="<?php echo site_url($delete) . '/remove/' . $data['device_id']; ?>" class="btn btn-small btn-danger" onclick="return confdeactivate();"  title="Disable" ><i class="fa fa-stop"></i></a>
                            	<?php }else{ ?>
                            		
								<a href="<?php echo site_url($delete) . '/add/' . $data['device_id']; ?>" class="btn btn-small btn-success" title="Activate" ><i class="fa fa-play"></i></a>	
                            	<?php } ?>
                            	<a href="<?php echo site_url('devices/reset/' . $data['device_id']); ?>" class="btn btn-small btn-primary" title="Reset" ><i class="fa fa-repeat"></i></a>
							</div>
							
							
							</td>
							</tr>									<?php endforeach;
								}
  ?>
							</tbody>
							<tfoot>
							<tr>
							<?php foreach($tb_headers as $header){ ?>

							<th><?php echo lang($header); ?></th>

							<?php } ?>

							</tr>
							</tfoot>
							</table>
							</div>

							</div>  </div> </div>

							<div class="row">
							<div class="col-md-4">
							<div class="box box-info">
							<div class="box-header">
							<h3 class="box-title"><?php echo lang('new_devices') ?></h3>

							</div>
							<?php  if($info){  ?>
							<form role="form" method="post" action="<?php echo site_url('devices/update_device/'); ?>">

							<?php }else{ ?>
							<form role="form" method="post" action="<?php echo site_url('devices/create_device/'); ?>">

							<?php  	} ?>
							<?php

							if ($this -> session -> userdata('empty_data')) {
								echo '<span class="alert alert-danger"> device Title is Required </span>';
								$this -> session -> unset_userdata('empty_data');
							} elseif ($this -> session -> userdata('device_saved')) {
								echo '<span class="alert alert-success"> device Title Added Succesfuly </span>';
								$this -> session -> unset_userdata('device_saved');
							}
							?>

							<div class="box-body">
							<?php if($info){ ?>
							<input name="device_id" id="device_id" class="form-control" type="hidden"  value="<?php  echo $info['device_id']; ?>" />
							<?php } ?>
						 
						<div class="form-group">
							<label class="control-label" for="device_imei" ><?php echo lang('device_imei'); ?></label> <i>(Required)</i>
 <?php
if ($this -> session -> userdata('error')) {
	echo '<span class="alert alert-danger">' . $this -> session -> userdata('error') . '</span>';
	$this -> session -> unset_userdata('error');
}
 ?>
							<input name="device_imei" id="device_imei" class="form-control" placeholder="Device Imei" type="text"  value="<?php
							if ($info) { echo $info['device_imei'];
							} else { echo set_value();
							}
							?>" required />
						</div>
							<div class="form-group">
							<label class="control-label" for="assigned_to" ><?php echo lang('assigned_to'); ?></label> <i>(Optional)</i>
 
								
                                        		<?php

												if ($employees == FALSE) {

													$employees = array('' => 'No Packages Type Defined');
												}

												if ($info) {$value = $info['assigned_to'];
												} else {
													$value = set_value();
												}
												echo form_dropdown('assigned_to', $employees, $value, 'class="form-control"');
                                        		?>
                                        		<?php echo form_error('assigned_to'); ?> 
						</div>
							<div class="form-group">
							<label class="control-label" for="device_mobile" ><?php echo lang('device_mobile'); ?></label><i>(Optional)</i>
 
							<input name="device_mobile" id="device_mobile" class="form-control" placeholder="Device Mobile" type="text"  value="<?php
							if ($info) { echo $info['device_mobile'];
							} else { echo set_value();
							}
							?>"  />
						</div>
						
						 
						<div class="form-group">
							<label class="control-label" for="device_descr" ><?php echo lang('device_descr'); ?></label> <i>(Optional)</i>
<textarea id="descriptions" class="form-control" style="" name="device_descr"><?php
if ($info) { echo $info['device_descr'];
} else { echo set_value();
}
							?></textarea>
							 
						</div>
					</div>
					<div class="modal-footer">

						<div class="form-actions">
							<a href="<?php echo site_url('devices') ?>" type='button' class="btn btn-info btn-medium"  ><?php echo lang('reset'); ?></a>
						 
							<button class="btn btn-alt btn-medium btn-success" type="submit">
								Save
							</button>
						</div>
					</div></form>
				</div>
			</div>

		</div>
</device>
