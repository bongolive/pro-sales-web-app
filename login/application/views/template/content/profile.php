<section class="container" role="main">

<div class="row">

    <article class="span12 data-block">

            <header>

                <h2>Profile</h2>

            </header>

            <section> 
            <?php if((isset($errorflag)) && ($errorflag == 0)){ ?>
                 <?php if (isset($msg)){ ?>
                        <div class="alert alert-success fade in">
    								<button class="close" data-dismiss="alert">&times;</button >
    								<strong>Success!</strong><?php echo $msg; ?>
                        </div>
                    <?php } ?>
            <?php }else if(isset($msg)){ ?>
                        <div class="alert alert-error fade in">
    								<button class="close" data-dismiss="alert">&times;</button >
    								<strong>Error!</strong><?php echo $msg; ?>
                        </div>
            <? } ?>
            <div class="row-fluid">
               
                <div class="span6">

                <h4><?php _l('change_profile_details'); ?></h4>
                
                    <form class="form-horizontal" action="<?php echo site_url("profile/update"); ?>" method="post">
                        <fieldset>

                            <div class="control-group">

                            	<label class="control-label" for="input"><?php _l('name'); ?></label>

                            	<div class="controls">
								<input name="userid" type="hidden" value="<?php echo $users['userid'];?>" />
                            	<input name="name" id="name" class="input-xlarge" type="text" value="<?php echo $users['name']?>" placeholder="<?php echo $users['name'];?>" required>

                            	</div>

                        	</div>

                        	<div class="control-group">

                            	<label class="control-label" for="username"><?php _l('username'); ?></label>

                            	<div class="controls">

                            	<input name="username" id="username" class="input-xlarge" value="<?php echo $users['username'];?>" placeholder="<?php echo $users['username'];?>" type="text" required >

                            	</div>

                        	</div>

                            <div class="control-group">

                            	<label class="control-label" for="email" ><?php _l('email'); ?></label>

                            	<div class="controls">

                            	<input type="email" name="email" id="email" value="<?php echo $users['email'];?>" class="input-xlarge" placeholder="<?php echo $users['email'];?>" required >

                            	</div>

                        	</div>

                            <div class="control-group">

                            	<label class="control-label" for="mobile"><?php _l('mobile'); ?></label>

                            	<div class="controls">

                            	<input name="mobile" id="mobile" class="input-xlarge" value="<?php echo $users['mobile_no'];?>" type="number" placeholder="<?php echo $users['mobile_no'];?>" required />

                            	</div>

                        	</div>

                            <div class="control-group">

                            	<label class="control-label" for="company_name"><?php _l('company_name'); ?></label>

                            	<div class="controls">

                            	<input name="company_name" id="company_name" class="input-xlarge" value="<?php echo $users['company_name'];?>" placeholder="<?php echo $users['company_name'];?>" type="text" />

                            	</div>

                        	</div>

                            <div class="form-actions">

				                <button class="btn btn-alt btn-large btn-primary" type="submit">Save changes</button>

				            </div>                  

                        </fieldset>
                    </form>

                </div>

                <div class="span6">

				    <h4><?php _l('change_password'); ?></h4>

									

				<form class="form-horizontal" action="<?php echo site_url("profile/change_password"); ?>" method="post">

        				<fieldset>

            				<div class="control-group">

                				<label class="control-label" for="password"><?php _l('new_password'); ?></label>

                				<div class="controls">

                				    <input name="password" id="password" class="input-xlarge" type="password" required/>

                				</div>

            				</div>

            				<div class="control-group">

                				<label class="control-label" for="confirm_password"><?php _l('confirm_new_password'); ?></label>

                				<div class="controls">

                				    <input  type="password" name="confirm_password" id="confirm_password" class="input-xlarge" />

                				</div>

            				</div>

            				<div class="form-actions">

                                <button class="btn btn-alt btn-large btn-primary" type="submit">Change password</button>

            				</div>

        				</fieldset>

				</form>

				</div>

            </div>

            </section>

    </article>

</div>

</section>