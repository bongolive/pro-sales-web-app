<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tracking extends MY_Controller {

    function __construct() {
        parent::__construct();

        ///$this->output->enable_profiler( TRUE );
        
        if(!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if($this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->load('sales', $this->Settings->language);
        $this->load->library('form_validation'); 
        $this->load->model('tracking_model');
        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif';
        $this->allowed_file_size = '1024';
        $this->data['logo'] = true;
    }

    function index(){
        show_404();
    }

    function sales_tracking(){
        $bc = array(array('link' => base_url(), 'page' => 'Tracking'), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => 'Track'));
        $meta = array('page_title' => 'Tracking', 'bc' => $bc);
        $this->data['devices'] = $this->tracking_model->getAllDevicesOfAccount( $this->session->userdata('account_id') );
        //var_dump($this->data['devices']);exit;
        $this->page_construct('tracking/track', $meta, $this->data);
    }

    function device_tracker(){
        $bc = array(array('link' => base_url(), 'page' => 'Tracking'), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => 'Track'));
        $meta = array('page_title' => 'Tracking', 'bc' => $bc);
        $this->data['devices'] = $this->tracking_model->getAllDevicesOfAccount( $this->session->userdata('account_id') );
        //var_dump($this->data['devices']);exit;
        $this->page_construct('tracking/device_tracker', $meta, $this->data);
    }

    public function showtrack()
    {
        $tracks_count = 0;
        $data = "";
        $start = "NO";
        $user_dtl = "";
        $start = urldecode($this->uri->segment(3));
        $sdate = urldecode($this->uri->segment(4));
        $edate = urldecode($this->uri->segment(5));
        $thc = array('-', 'A');
        $replace = array('/',':');
        $sdate = str_replace($thc,$replace,$sdate);
        $edate = str_replace($thc,$replace,$edate);
        
       //var_dump($this->uri->segment(4));
        if(($start != 'start') && $sdate && $edate){
            // echo "Here";exit;
            //if($this->input->post('start') == "start"){
              //  $start = "YES";
            //}else{
                $date = date_create($sdate);
                $sdate = date_format($date, 'Y-d-m');
                $date = date_create($edate);
                $edate = date_format($date, 'Y-d-m');
                $imei = $start;
               // echo $sdate;
                //$content['devices'] = $this->tracking_model->get_deviceLocationByImei( $this->session->userdata('account_id'), $imei, $sdate, $edate );
                
                //}
          //  }
        }
            
        $content['imei'] = $start;
        $content['sdate'] = $sdate;
        $content['edate'] = $edate;
       // $content['imei'] = $start;
        $content['user_dtl'] = $user_dtl;
        $content['data'] = $data;
        $content['user_dtl'] = $user_dtl;
        //Load shoetrack view here
        $this->load->view($this->theme .'tracking/showtrack',$content);
    }


    public function showdevices(){

        $this->load->view($this->theme .'tracking/show_devices');

    }
    public function showdevicestracker(){

        $this->load->view($this->theme .'tracking/show_devicestracker');

    }

    public function lastlocation($imei = null, $sdate = null, $edate = null)
    {
        $devices = $this->tracking_model->getLastKnownLocation($this->session->userdata('account_id'), $imei, $sdate, $edate);
        if(count($devices)>0){
            $xml = "<locations>";        
            foreach($devices as $row){
                $xml .= "<location>";
                $xml .= "<device_id>".$row["device_id"]."</device_id>";
                $xml .= "<device_imei>".$row["device_imei"]."</device_imei>";
                $xml .= "<device_name>".$row["device_name"]."</device_name>";
                $xml .= "<phone>".$row["phone"]."</phone>";
                $xml .= "<holder_name>".$row["holder_name"]."</holder_name>";
                $xml .= "<latitude>".$row["latitude"]."</latitude>";
                $xml .= "<longitude>".$row["longitude"]."</longitude>";
                $xml .= "<grand_total>".$row["grand_total"]."</grand_total>";
                $xml .= "<total_tax>".$row["total_tax"]."</total_tax>";
                $xml .= "<paid>".$row["paid"]."</paid>";
                $xml .= "<localdate>".$row["localdate"]."</localdate>";
                $xml .= "</location>";
            }
        
            $xml .= "</locations>";
            echo $xml;
        }else{
            echo "ERROR. !";
        }
    }

    public function gettrackerlocation( $imei = null, $sdate = null, $edate = null )    {

        $devices = $this->tracking_model->getTrackerLocation( $this->session->userdata('account_id'), $imei, $sdate, $edate);
        //echo $this->db->last_query();exit;
        //echo $this->db->last_query();exit;

        if( count( $devices ) > 0 ){
            $xml = "<locations>";
            foreach($devices as $row){
                $xml .= "<location>";
                $xml .= "<device_imei>".$row["device_imei"]."</device_imei>";
                $xml .= "<lat>".$row["lat"]."</lat>";
                $xml .= "<lng>".$row["lng"]."</lng>";
                $xml .= "<device_date>".$row["device_date"]."</device_date>";
                $xml .= "<device_name>".$row["device_name"]."</device_name>";
                $xml .= "<device_mobile>".$row["phone"]."</device_mobile>";
                $xml .= "</location>";
            }
            $xml .= "</locations>";
            echo $xml;
        }else{
            echo "ERROR. !";
        }
    }
    public function devicetracker_location($imei = null, $sdate = null, $edate = null)
    {
        $devices = $this->tracking_model->getLastKnownLocation($this->session->userdata('account_id'), $imei, $sdate, $edate);
        if(count($devices)>0){
            $xml = "<locations>";
            foreach($devices as $row){
                $xml .= "<location>";
                $xml .= "<device_id>".$row["device_id"]."</device_id>";
                $xml .= "<device_imei>".$row["device_imei"]."</device_imei>";
                $xml .= "<device_name>".$row["device_name"]."</device_name>";
                $xml .= "<phone>".$row["phone"]."</phone>";
                $xml .= "<holder_name>".$row["holder_name"]."</holder_name>";
                $xml .= "<latitude>".$row["latitude"]."</latitude>";
                $xml .= "<longitude>".$row["longitude"]."</longitude>";
                $xml .= "<grand_total>".$row["grand_total"]."</grand_total>";
                $xml .= "<total_tax>".$row["total_tax"]."</total_tax>";
                $xml .= "<paid>".$row["paid"]."</paid>";
                $xml .= "<localdate>".$row["localdate"]."</localdate>";
                $xml .= "</location>";
            }

            $xml .= "</locations>";
            echo $xml;
        }else{
            echo "ERROR. !";
        }
    }

    function customer_tracking(){
        $bc = array(array('link' => base_url(), 'page' => 'Tracking'), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => 'Track'));
        $meta = array('page_title' => 'Tracking', 'bc' => $bc);
        $this->data['devices'] = $this->tracking_model->getAllDevicesOfAccount( $this->session->userdata('account_id') );
       // echo $this->db->last_query();exit;
        //var_dump($this->data['devices']);exit;
        $this->page_construct('tracking/track_customer', $meta, $this->data);
    }

    public function showtrack_customer()    {
        $tracks_count = 0;
        $data = "";
        $start = "NO";
        $user_dtl = "";
        $start = urldecode($this->uri->segment(3));
        $sdate = urldecode($this->uri->segment(4));
        $edate = urldecode($this->uri->segment(5));
        $thc = array('-', 'A');
        $replace = array('/',':');
        $sdate = str_replace($thc,$replace,$sdate);
        $edate = str_replace($thc,$replace,$edate);

        //var_dump($this->uri->segment(4));
        if(($start != 'start') && $sdate && $edate){
            // echo "Here";exit;
            //if($this->input->post('start') == "start"){
            //  $start = "YES";
            //}else{
            $date = date_create($sdate);
            $sdate = date_format($date, 'Y-d-m');
            $date = date_create($edate);
            $edate = date_format($date, 'Y-d-m');
            $imei = $start;
            // echo $sdate;
            //$content['devices'] = $this->tracking_model->get_deviceLocationByImei( $this->session->userdata('account_id'), $imei, $sdate, $edate );

            //}
            //  }
        }

        $content['imei'] = $start;
        $content['sdate'] = $sdate;
        $content['edate'] = $edate;
        // $content['imei'] = $start;
        $content['user_dtl'] = $user_dtl;
        $content['data'] = $data;
        $content['user_dtl'] = $user_dtl;
        //Load shoetrack view here
        $this->load->view($this->theme .'tracking/showtrack_customer',$content);
    }

    public function showdevices_customer(){
        $this->load->view($this->theme .'tracking/show_devices_customer');
    }

    public function lastlocation_customer( $imei = null )
    {
        $devices = $this->tracking_model->getCustomerLocation( $this->session->userdata('account_id'), $imei);
        //echo $this->db->last_query();exit;
        if(count($devices)>0){
            $xml = "<locations>";
            foreach($devices as $row){
                $xml .= "<location>";
               // $xml .= "<device_id>".$row["device_id"]."</device_id>";
                //$xml .= "<device_imei>".$row["device_imei"]."</device_imei>";
                //$xml .= "<device_name>".$row["device_name"]."</device_name>";
                $xml .= "<phone>".$row["phone"]."</phone>";
                $xml .= "<holder_name>".$row["holder_name"]."</holder_name>";
                $xml .= "<latitude>".$row["latitude"]."</latitude>";
                $xml .= "<longitude>".$row["longitude"]."</longitude>";
                $xml .= "<grand_total>".$row["grand_total"]."</grand_total>";
                //$xml .= "<total_tax>".$row["total_tax"]."</total_tax>";
                //$xml .= "<paid>".$row["paid"]."</paid>";
                $xml .= "<localdate>".$row["localdate"]."</localdate>";
                $xml .= "<address>".$row["address"]."</address>";
                $xml .= "</location>";
            }

            $xml .= "</locations>";
            echo $xml;
        }else{
            echo "ERROR. !";
        }
    }

    public function view_raw_data(){
        $imei=  null;
         $filter_string_imei = ($imei) ? " AND C.device_imei = ".$imei : "";
         $account_id = $this->session->userdata('account_id');
        $query = $this->db->query("SELECT coalesce(SUM(S.total), 0) AS grand_total, C.`name` as holder_name, C.phone , C.lattd  AS latitude, C.
                              lontd  AS longitude,
                          C.last_update_time AS localdate,C.address
                            FROM sma_companies C
                            LEFT OUTER JOIN sma_sales S
                            ON  S.customer_id = C.id WHERE C.group_name = 'customer' AND C.account_id = {$account_id} ".$filter_string_imei."
                            GROUP BY C.id;");
       //echo $this->db->last_query();exit;
        echo "<pre>";
        var_dump( $query->result_array() ); exit;

    }

    public function view_raw_data_tracker(){

        //$del = "DELETE FROM sma_device_tracking WHERE device_imei = '356318068050513' AND account_id = '85'";
       // $this->db->query($del);
        //$query = $this->db->query("DELETE FROM sma_companies WHERE group_name='customer' and account_id=85;");
       //echo $this->db->last_query();exit;
       // echo "<pre>";
       // var_dump( $query->result_array() ); exit;

    }
}