<?php
class Permission_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	
	public function getPermissions($cond)
	{
		$query = $this->db->get_where('promobile_permissions', $cond);
		return $query->result_array();
	}
	
	public function add($data)
	{
		return $this->db->insert('promobile_permissions', $data);
	}
	
	public function update($data,$cond)
	{
		$this->db->where($cond);
		return $this->db->update('promobile_permissions', $data);
	}
	
	public function delete($cond)
	{
		return $this->db->delete('promobile_permissions', $cond);
	}

}