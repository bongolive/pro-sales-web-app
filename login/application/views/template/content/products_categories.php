				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('products_categories'); ?></h2>
					   
            </header> 

        </article>

    </div>	
    <div class="row">
 
    	 <article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l('all_products_categories'); ?></h4>
								</div>
								<div class="modal-body"> 
								
								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>

									<tr>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>
 				  <?php if ($tb_data){ 
									foreach ($tb_data as $data):  
									  
                                            $id = $table_id;
                                     ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php echo $data[$field]; ?></td>
                                        <?php endforeach; ?>
										
                                            <td> 
                                                	<a href="<?php echo site_url($edit) . '/' . $data[$id]; ?>" class="btn btn-small btn-info"> Edit</a>
                                                	<a href="<?php echo site_url($delete) . '/' . $data[$id]; ?>" class="btn btn-small btn-danger" > Delete</a> 
											</td>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
								</tbody>
	</table> 
           
		</article>							
 
    	 
    	 <article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l('add_new_category'); ?></h4>
								</div>
								<div class="modal-body">
									<?php if($info){ ?>
										<form class="form-horizontal" method="post" action="<?php echo site_url('products/update_category'); ?>">
								
								<?php	}else{ ?>
                                <form class="form-horizontal" method="post" action="<?php echo site_url('products/add_category'); ?>">
                                	<?php
									}

									if($info){
 ?>
                                			<input name="category_id" value="<?php echo $info['cat_id']; ?>" type="hidden" />
                                <?php 	} ?>
                                	
                                    <fieldset>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="category_name" ><?php _l('category_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="category_name" id="category_name" value="<?php
												if ($info) { echo $info['category_name'];
												} else {echo set_value('category_name');
												}
 ?>" class="input-large" type="text" required />
                                        	</div>
                                    	</div>
										
                                    	 
                                    	 <div class="control-group">
                                        	<label class="control-label" for="category_descr" ><?php _l('category_descr'); ?></label>
                                        	<div class="controls">
                                        		<textarea name="category_descr" id="description" class="input-large"><?php
												if ($info) { echo $info['category_descr'];
												} else {echo set_value('category_descr');
												}
 ?></textarea>
                                        	 </div>
                                    	</div>
                                    		     
								</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('products/categories'); ?>" class="btn btn-alt" data-dismiss="modal">Clear</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>
                        </div>                        	
                        </div>	
                        </section>
                        		
					 