<?php
class Products_model extends MY_Model {
	public $table = 'products';
	public $table_id = 'product_id';

	function __construct() {

		parent::__construct();

	}

	public function form_validation() {

		$this -> form_validation -> set_rules('product_name', 'Product name', 'trim|required');
		$this -> form_validation -> set_rules('product_category', 'Product Category', 'trim|required');
		$this -> form_validation -> set_rules('package_type', 'package_type', 'trim|required');
		$this -> form_validation -> set_rules('package_units', 'package_units', 'required');
		$this -> form_validation -> set_rules('sku_product_no', 'sku_product_no', 'required');
		$this -> form_validation -> set_rules('tax_rate', 'tax_rate', 'trim|required');
		$this -> form_validation -> set_rules('unit_sale_price', 'unit_sale_price', 'trim|required');

	}

	public function read_products($where) {
		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$this -> db -> join('products_categories', 'cat_id = category', 'left');
		$this -> db -> join('packages', 'package_type = package_id', 'left');

		$data = $this -> db -> get() -> result_array();
		return $data;
	}


    function fetch_productsbyaccount($accountid) {
        $this->db->where('account_id',$accountid);
        $result = $this->db->get($this->table);
        if($result->num_rows() > 0) {
            return $result->result_array();
        }
    }
	/*
	 * categoriess
	 */
	public function get_product_categories($where = False) {

		$this -> table = 'products_categories';
		$this -> table_id = 'cat_id';

		$result = $this -> read($where);
		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	//get latest stock

	public function getStock($where) {
		$this -> table = "stock";
		$result = $this -> read($where);
		if ($result) {
			return $result;
		} else {
			return FALSE;
		}

	}

	public function read_packages($where = false) {
		$this -> table = 'packages';
		$this -> table_id = 'package_id';

		$result = $this -> read($where);
		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public function add_categories($data) {
		$this -> table = 'products_categories';
		$this -> table_id = 'cat_id';

		if ($this -> save($data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function update_categories($id, $data) {
		$this -> table = 'products_categories';
		$this -> table_id = 'cat_id';
		if ($this -> update($id, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}


    public function check_category($where){
        $this->table = 'products_categories';

        $this->db->where('item_id',$where);
        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }



    public function check_package($where){
        $this -> table = 'packages';

        $this->db->where('item_id',$where);
        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }



    public function check_container($where){
        $this->table = 'products_containers';

        $this->db->where('item_id',$where);
        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

	/*
	 * packages
	 */

	public function add_packages($data) {
		$this -> table = 'packages';
		$this -> table_id = 'package_id';

		if ($this -> save($data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function get_packages($where = False) {
		$this -> table = 'packages';
		$this -> table_id = 'package_id';

		$packages = $this -> read($where);

		if ($packages) {
			return $packages;
		} else {
			return FALSE;
		}
	}

	public function update_packages($id, $data) {
		$this -> table = 'packages';
		$this -> table_id = 'package_id';
		if ($this -> update($id, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function add_containers($data) {
		$this -> table = 'products_containers';
		$this -> table_id = 'container_id';
		if ($this -> save($data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update_containers($id, $data) {
		$this -> table = 'products_containers';
		$this -> table_id = 'container_id';
		if ($this -> update($id, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function get_containers($where = False) {
		$this -> table = 'products_containers';
		$this -> table_id = 'container_id';
		$packages = $this -> read($where);

		if ($packages) {
			return $packages;
		} else {
			return FALSE;
		}
	}

    public function get_unit_price($where){
        $this->db->where('product_id',$where);
        $result = $this->db->get('pro_products');
        if($result->num_rows()  >0 ){
            $j = $result->row();
            return $j->unit_sale_price ;
        }
    }

    function fetch_products($accountid) {
        $return = array();
        $this->db->from($this->table);
        $this->db->order_by('product_name');
        $this->db->where('account_id',$accountid);
        $result = $this->db->get();
        $return[0] = 'Select Product';
        if($result->num_rows() > 0) {
            foreach($result->result_array() as $row) {
                $return[$row['product_id']] = $row['product_name'];
            }
        }
        return $return;
    }
    function fetch_unit_price($id){
        $return = array();
        $this->db->from($this->table);
        $this->db->where('product_id', $id);
        $this->db->order_by('product_name');
        $result = $this->db->get();
        if($result->num_rows() > 0) {
            $return = $result->result();
        }
        return $return;
    }

    function add_warehouse($data){
        $this -> table = 'warehouses';
        $this -> table_id = 'wh_id';
        if ($this -> save($data)) {
            return TRUE;
        } else {

            return FALSE;
        }
    }
    public function get_warehouses($where){
        $this -> table = 'warehouses';
        $this -> table_id = 'wh_id';
        $result = $this -> read($where);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }
    public function get_wherehousebyhouseid($where){
        $this->db->where('wh_id',$where);
        $result = $this->db->get('warehouses');
        if($result->num_rows()  >0 ){
            $j = $result->row();
            return $j->wh_title ;
        }
    }

    public function get_warehousebystockid($where){

        $this->db->where('stock_id',$where);
        $result = $this->db->get('pro_stock');
        if($result->num_rows()  >0 ){
            $j = $result->row();
            return $j->warehouse ;
        }
    }

    public function update_warehouse($id, $data) {
        $this -> table = 'warehouses';
        $this -> table_id = 'wh_id';
        if ($this -> update($id, $data)) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function get_data(){

        $type = $this->input->post('type');

        if($type != 1){
            return array();
        }

        return array(
            array(
                'name' => 'Abigail',
                'email' => 'ut.sem.Nulla@duinecurna.org',
                'registered_date' => '01/17/2014'
            ),
            array(
                'name' => 'Ralph',
                'email' => 'ultrices.posuere@Sed.org',
                'registered_date' => '10/08/2013'
            ),
        );
    }
}
