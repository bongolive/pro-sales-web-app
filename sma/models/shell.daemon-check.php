<?php
include_once "bootstrap.php";
include_once(LIB_DIR . "shell.inc.php");

$debug = false;
//print_r(__FILE__);exit;
if ($debug == true) { echo "Checking Starts...\n"; }
// handle starts
$sql = "SELECT * FROM " . get_table_name("processes") . " WHERE `run_as` LIKE '" . $_SERVER["USER"] ."' AND status=1";
$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
while($row = mysql_fetch_assoc($res)) {
	if ($debug == true) { echo "Name: " . $row["name"] . "..."; }
	$pid = getPID($row["check"]);
	if($pid == "") {
		if ($debug == true) { echo " Starting..."; }
		$ret = startProcess($row["start"]);
		if ($debug == true) { echo " Started..."; }
		$sql_update = "UPDATE " . get_table_name("processes") . " SET modified = NOW() WHERE id LIKE '" . $row["id"] . "'";
		$res_update = mysql_query($sql_update) or mysql_error_show($sql_update,__FILE__,__LINE__);
	} else {
		if ($debug == true) { echo "Running with PID: " . $pid . "..."; }
	}
	if ($debug == true) { echo "\n"; }
}


if ($debug == true) { echo "Checking Stops...\n"; }
// handle stops
$sql = "SELECT * FROM " . get_table_name("processes") . " WHERE `run_as` LIKE '" . $_SERVER["USER"] ."' AND status=0";
$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
while($row = mysql_fetch_assoc($res)) {
	if ($debug == true) { echo "Name: " . $row["name"] . "..."; }
	$pid = getPID($row["check"]);
	if($pid == "") {
		if ($debug == true) { echo "Not Running"; }
	} else {
		if ($debug == true) { echo " Stopping..."; }
		$ret = stopProcess($pid);
		if ($debug == true) { echo " Stopped..."; }
		$sql_update = "UPDATE " . get_table_name("processes") . " SET modified = NOW() WHERE id LIKE '" . $row["id"] . "'";
		$res_update = mysql_query($sql_update) or mysql_error_show($sql_update,__FILE__,__LINE__);
	}
	if ($debug == true) { echo "\n"; }
}

echo $sql;
print_r($_SERVER);

include_once(LIB_DIR . "close.php");
?>
