<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permissions extends CI_Controller { 
    
	public $perms_dsc  = array('fullname', 'permissions');
      
	public function __construct()
	{
		
		parent::__construct();
 
		
		$this->data['css'] = array('bootstrap.css','bootstrap-responsive.css',
                             'bootstrap-overrides.css',
                             'ui-lightness/jquery-ui-1.8.21.custom.css',
                             'DT_bootstrap.css',
                             'responsive-tables.css',
                             'slate.css','datepicker.css',
                             'slate-responsive.css', 
                             );
        $this->data['js'] = array('jquery-1.7.2.min.js',
                            'jquery-ui-1.8.21.custom.min.js',
                             'jquery.ui.touch-punch.min.js',
                             'bootstrap.js',
                             'plugins/datatables/jquery.dataTables.js',
                             'plugins/datatables/DT_bootstrap.js',
                             'plugins/responsive-tables/responsive-tables.js',
                             'Slate.js',
                             'demos/demo.tables.js'
                             );
                             
                             if(!$this->session->userdata('is_login') == TRUE) {
	                             	$this->session->set_userdata('login_error','Please, Loggin first');
											redirect('login');
                             	}
                             
 					$this->load->model('employees_model');
 					$this->load->model('permissions_model');
 					$this->load->model('users_model');
 				  	$this->permissions_model->form_validation();
 					$this->form_validation->set_error_delimiters('<div class="alert alert-block span4">', '</div>');
 					
					
				   $this->data['row_fields']= $this->perms_dsc;
					$this->data['tb_headers'] = $this->perms_dsc;
					$this->data['links']="permissions/save";
					$this->data['tb_name'] = 'all_permissions';
					$this->data['actions']='permissions/update/';
					$this->data['actions_del']='permissions/delete/';
	 			  	$this->data['view']="permissions/view/";	
					 
					$this->data['controller']='permissions';	
					$this->data['id'] = $this->permissions_model->table_id;
			 
	 /******************** check user permissions ***************************/
		 
	 	$modules=explode(',',$this->session->userdata('permissions'));
		$this->data['is_allowed']=$this->permissions_model->read_modules($modules);		
		
		/******************** check user permissions ***************************/
	} 
	
	public function index()
	{
		 
 	//the search from
	 $this->data['controller']='permissions';
	/*	 
	if(isset($_POST['search'])) {	
			extract($_POST);	
		 
			if(!empty($name)) {
        		$where=array('firstname'=>$name,'created_by'=>$this->session->userdata('account')); 
        	}elseif(!empty($role)) {
     			$where=array('role'=>$role,'created_by'=>$this->session->userdata('account'));
     		}else {
 		$where=array('created_by'=>$this->session->userdata('account'));
 	} 	
 	}else {
 		$where=array('permissions.created_by'=>$this->session->userdata('account'));
 	} 	
  */
	 
	 
	 	$where=array('permissions.created_by'=>$this->session->userdata('account'));
	   
	  
	     $this->session->unset_userdata('id');
	     $this->session->set_userdata('for_permissions',TRUE);
	     
	   $this->data['tb_data']= $this->permissions_model->read($where);
	   
	     $this->load->view('template/header', $this->data);
	     $this->load->view('template/content/table');
	     $this->load->view('template/footer');   
	   $this->session->unset_userdata('for_permissions'); 
 }
 
public function save()
	{ 
	     	if($this->form_validation->run()==FALSE) {
	     		$where_by=array('users.created_by'=>$this->session->userdata('account'));
	     		$data['users']=$this->users_model->read($where=FALSE,$where_by);
	     		$data['read_permissions']=$this->permissions_model->read_modules();
	     				$data['info']=FALSE;
	     				
		        		$this->load->view('template/header', $this->data);
		      	 	$this->load->view('template/content/form_permissions',$data);
		        		$this->load->view('template/footer');   
		        	}else {
			        	extract($_POST); 
			         $permits = implode(',',$permits);
							$data=array(
								 'user_id'=>$user_id, 'permissions'=>$permits, 'created_by'=>$this->session->userdata('account'));
		 				 
			  		$this->permissions_model->save($data);
				  	redirect('permissions');							
 	 
	 } 		        
 }
 public function update()
	{ 
 
	if($this->session->userdata('id')){
		$id=$this->session->userdata('id');
		}else {
		$id=$this->uri->segment(4); 
	}
$this->load->view('template/header', $this->data);

	 
	     	if($this->form_validation->run()==FALSE) {
	     	 	$where_by=array('users.created_by'=>$this->session->userdata('account'));  
	     		//$data['employees']=$this->employees_model->read($where=FALSE,$where_by);
	     		$data['users']=$this->users_model->read($where=FALSE,$where_by);
	     		$data['read_permissions']=$this->permissions_model->read_modules();
	     			
	     		$where=array('perm_id'=>$id);
	     		$info=$this->permissions_model->read($where);
	     		
	     		  foreach($info as $info){$data['info'] = $info;}
		        		$this->load->view('template/header', $this->data);
		      	 	$this->load->view('template/content/form_permissions',$data);
		        		$this->load->view('template/footer');   
		        	}else {
			        	extract($_POST); 
			         $permits = implode(',',$permits);
							$data=array('user_id'=>$user_id, 'permissions'=>$permits, 'created_by'=>$this->session->userdata('account'));
	  		 
 $this->permissions_model->update($perm_id,$data);
  redirect('permissions');							
 	 
	 } 
 }
 
 public function check_user($user_id) {
 		$where=array('permissions.user_id'=>$user_id);
 	  	$user = $this->permissions_model->read($where);
 	   if($user) {
	$this->form_validation->set_message('check_user', 'This user permissions settings already exist');
			      return false;       
		      } else{     return true;  }
	}	
  
 public function delete() {
	$id=$this->uri->segment(4);
	if($this->permissions_model->delete($id)) {
		redirect('permissions');
	} 	
} 
 }