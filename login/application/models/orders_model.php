<?php
class Orders_model extends MY_Model {
	public $table = 'sales_orders';
	public $table_id = 'order_id';

	function __construct() {

		parent::__construct();

	}

	public function form_validation() {

		$this -> form_validation -> set_rules('order_date', 'Order date', 'trim|required');
		$this -> form_validation -> set_rules('delivery_date', 'Delivery date', 'trim|required|callback_check_deliverydate');
		$this -> form_validation -> set_rules('customer_id', 'Customer name', 'trim|required');
		$this -> form_validation -> set_rules('product_id[]', 'Product details', 'trim|required|callback_checkproduct');
		$this -> form_validation -> set_rules('assigned_to', 'Sales Team name', 'trim|required');

	}

	public function save($data) {

		if ($this -> db -> insert($this -> table, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function save_order_details($data) {
		$this -> table = 'sales_order_details';

		if ($this -> save($data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function read_order_details($where = false, $limit = false, $startpoint = FALSE) {
		$this -> table = 'sales_order_details';

		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		if ($limit) {
			$this -> db -> limit($limit, $startpoint);
		}
		$this -> db -> join('products', 'products.product_id = ' . $this -> table . '.product_id', 'left');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function latest_team_summary($where = false, $like = false) {

		$this -> table = 'sales_orders';

		$this -> db -> select('device_order_id,order_date');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		if ($like) {
			$this -> db -> like('order_date', $like);
		}
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}

	}

	public function latest_sales_summary($where) {

		$this -> table = 'sales_order_details';

		$this -> db -> select(' sum(order_qty * sale_price) as today_sales');
		$this -> db -> from($this -> table);
		 
		if($where){
			$this->db->where_in('order_id',$where);
		} 
		 
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}

	}

	public function getOrderDetails($where) {
		$this -> table = 'sales_order_details';
		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$this -> db -> join('sales_orders', 'sales_order_details.order_id = sales_orders.order_id', 'left');
		//	$this -> db -> join('products', 'products.product_id = ' . $this -> table . '.product_id', 'left');
		$this -> db -> join('customers', 'customers.customer_id = sales_orders.customer_id', 'left');

		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function read_order($where = false, $limit = false, $startpoint = FALSE) {
		$this -> table = 'sales_orders';
		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		if ($limit) {
			$this -> db -> limit($limit, $startpoint);
		}
		$this -> db -> join('customers', 'customers.customer_id = sales_orders.customer_id', 'left');
		$this -> db -> join('employees', 'employees.id = sales_orders.employee_id', 'left');

		$data = $this -> db -> get() -> result_array();

		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function remove_details($where) {
		$this -> table = 'sales_order_details';
		$this -> table_id = 'order_id';
		$this -> delete($where);

	}

	public function highsale($where) {
		$this -> table = 'sales_order_details';

		$this -> db -> select("sum(order_qty) as orders,product_name");
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$this -> db -> join('products', 'products.product_id = ' . $this -> table . '.product_id', 'left');
		$this -> db -> group_by('sales_order_details.product_id');
		$this -> db -> order_by('orders', 'desc');
		$this -> db -> limit(5);
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function lowsale($where) {
		$this -> table = 'sales_order_details';

		$this -> db -> select("sum(order_qty) as orders,product_name");
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$this -> db -> join('products', 'products.product_id = ' . $this -> table . '.product_id', 'left');
		$this -> db -> group_by('sales_order_details.product_id');
		$this -> db -> order_by('orders', 'asc');
		$this -> db -> limit(5);
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	function updateorder($id, $data) {
		$table = 'sales_orders';
		$table_id = 'order_id';

		//do update data into database
		$this -> db -> where($table_id, $id);
		if ($this -> db -> update($table, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

    public function locate($where){
        $this->db->where('order_id',$where);
        $result = $this->db->get($this->table);
        if($result->num_rows() > 0){
            $row = $result->row();
            return $row;
        }
    }

    public function getOrderLocation($where){
        $query = $this->db->query("SELECT od.order_id,  ifnull((SELECT CONCAT(e.firstname, ' ',e.lastname) FROM pro_employees e WHERE e.id = od.employee_id ORDER BY od.order_date LIMIT 1),NULL ) AS holder_name,
  ifnull((SELECT ct.business_name FROM pro_customers ct WHERE ct.customer_id = od.customer_id ORDER BY od.order_date LIMIT 1),NULL ) AS business,
  od.phone_imei,od.sales_total,od.total_tax,od.paid_amount, lat AS latitude, lng AS longitude, od.order_date
  FROM pro_sales_orders od WHERE od.order_id ='$where' ");
        if($query){
            return $query->result_array();
        }
    }



    public function get_assigned_emp($where){
        $this->db->where($where);
        $qty = $this->db->get('pro_devices');
        if($qty -> num_rows() == 1){
            $q = $qty->row();
            return $q->assigned_to ;
        }
    }



    public function _api_save_orders($data,$where){
        $this->db->trans_start();
        $created_on = date('Y-m-d h:i:s');

        $emp = $this->get_assigned_emp(array('device_imei' => $where['device_imei']));
        if($emp){

            $flag = array();
            foreach($data as $value){
                $this->db->trans_start();
                $orderno = $where['device_imei'] . '.' . $value['order_id']. '.' .date('Y-m-d');


                $orderdata = array('device_order_id' => $orderno,'phone_imei' => $where['device_imei'],
                    'employee_id' => $emp,'order_date' => $value['order_date'],
                    'customer_id' => $value['customer_id'],'sales_total' => $value['total_sales'],
                    'total_tax' => $value['total_tax'],'created_on' => $created_on,
                    'account_id' => $where['account_id'],'payment_status' => $value['payment_status'],
                    'payment_mode' => $value['payment_mode'],'receipt_no' => $value['receipt_no'],
                    'paid_amount' => $value['payment_amount'], 'order_status' => 2,
                    'lat' => $value['lat'],'lng' => $value['lng']);
                $insert = $this->db->insert('pro_sales_orders', $orderdata);

                if($insert){

                    $id = $this -> db -> insert_id();
                    $transtoken = $this->generate_token(2);
                    $transdata = array('account_id' => $where['account_id'],
                        'created_by' => $value['customer_id'],'old_payments' => 0,
                        'order_id' => $id,'trans_date' =>  date('Y-m-d h:i:s'),
                        'trans_amount' => $value['payment_amount'],'trans_status' => 1,
                        'trans_pay_mode' => $value['payment_mode'],'transaction_token'  => $transtoken
                    );
                    $paymnts = $this->saveTransaction($transdata);
                    if($paymnts)
                    {
                        $flag['success'] = 1;
                    } else {
                        $flag['success'] = 0;
                    }

                }
                $this->db->trans_complete();
             }
            return $flag;
          }
        }

    public function saveTransaction($data){
        $insert = $this->db->insert('pro_transactions',$data);
        if($insert){
            return true;
        }
        return false;
    }

    public function generate_token ($len = 15)
    {

        $date = date('Y.m.d');

        $chars = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        );
        shuffle($chars);
        $num_chars = count($chars) - 1;
        $token = '';


        for ($i = 0; $i < $len; $i++)
        {
            $token .= $chars[mt_rand(0, $num_chars)];
        }
        $n = $this->getLastTokenId();
        $token = $token.$date."-".$n;
        return $token;
    }

    public function getLastTokenId(){

        $query = $this->db->get('pro_transactions');
        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
    }

    public function _api_save_order_details($data,$where){
        $created_on = date('Y-m-d h:i:s');
        $emp = $this->get_assigned_emp(array('device_imei' => $where['device_imei']));
        if($emp){
            $flag = array();
            foreach($data as $value){
                $this->db->trans_start();
                $orderno = $where['device_imei'] . '.' . $value['order_id']. '.' .date('Y-m-d');
                $vals = array('order_id' => $orderno, 'phone_imei' => $where['device_imei'],
                    'product_id' => $value['prod_system_id'], 'order_qty' => $value['prod_order_qty'],
                    'sale_price' => $value['sale_price'], 'sale_tax' => $value['tax_rate']
                    );
                $save = $this->db->insert('pro_sales_order_details',$vals);
                if($save){
                    $where = array('prodid' => $value['prod_system_id'],
                        'qty' => $value['prod_order_qty'],'device_imei' => $where['device_imei']);
                    $updatestock = $this->_api_update_remaining_stock($where);
                    if($updatestock){
                        $flag['success'] = 1;
                    } else{
                        $flag['failed'] = 1;
                    }
                } else {
                    $flag['failed'] = 1;
                }
                $this->db->trans_complete();
            }
            return $flag;
        }
    }

    public function _api_update_remaining_stock($where){
        $prod = $where['prodid'];
        $prodstock = $this->_api_get_assigned_product_qty($prod);
        $arraywhere = array('device_imei' => $where['device_imei'],'product_id'=>$prod);
        $currentstokc = $this->_api_get_current_stock($arraywhere);

        if($prodstock && $currentstokc){
            $newrqty = $currentstokc - $where['qty'];
            $newrqty = array('remained_qty' => $newrqty);

            $prodqty = $prodstock - $where['qty'];
            $prodqty = array('assigned_stock' => $prodqty);

            $updateproduct = $this->db->update('pro_products',$prodqty,array('product_id' => $prod));
            $updatestock = $this->db->update('pro_assigned_stock',$newrqty,$arraywhere);

            if($updateproduct && $updatestock){
                return true ;
            } else {
                return false;
            }
        }
    }
    public function _api_get_assigned_product_qty($where){
        $this->db->where('product_id',$where);
        $qty = $this->db->get('pro_products');
        if($qty -> num_rows() == 1){
            $q = $qty->row();
            return $q->assigned_stock ;
        }
    }


    public function _api_get_current_stock($where){
        $this->db->where($where);
        $qty = $this->db->get('pro_assigned_stock');
        if($qty -> num_rows() == 1){
            $q = $qty->row();
            return $q->remained_qty ;
        }
    }

}
