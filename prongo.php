<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
<link rel="stylesheet" href="bx_styles.css" type="text/css" />
<title>footprint</title>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>

<script src="js/jquery.bxSlider.js" type="text/javascript"></script>
<script src="js/jquery.bxSlider.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    
	 $('.slider').bxSlider({
    auto: true,
    pager: true
  });
  });
</script>
</head>

<body>
	<div class="body_class">
    	<div class="center_header_page">
        	<div class="header-left">
            	<img src="images/logo3.png" alt=""  />
            </div>
            <div class="header-right">
			<?php session_start();
			if(!isset($_SESSION['username'])){
			echo "<a href='login.php' ><img src='images/login_button.png' alt='' /></a>";
			}else{
			echo "<a href='device.php' ><img src='images/login_button.png' alt='' /></a>";
			}
			?>
            	
            </div>
        </div>
	</div>
<div class="center_header_page">
   	<div class="menu_wrapper">
        	<div class="menu_first">
            	<div class="menu_img home">
                	<img src="images/home.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >Home</a>
                </div>
            </div>
            <div class="menu_first">
            	<div class="menu_img">
                	<img src="images/company.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >Company</a>
                </div>
            </div>
            <div class="menu_first">
            	<div class="menu_img">
                	<img src="images/client.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >ProNGO</a>
                </div>
            </div>
            <div class="menu_first">
            	<div class="menu_img">
                	<img src="images/resource.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >ProSales</a>
                </div>
            </div>
            <div class="menu_first">
            	<div class="menu_img">
                	<img src="images/support.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >ProTracker</a>
                </div>
            </div>
            <div class="menu_first">
            	<div class="menu_img">
                	<img src="images/blog.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >ProManager</a>
                </div>
            </div>
            <div class="menu_first">
            	<div class="menu_img home">
                	<img src="images/contact.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >Contact</a>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    
    </div>
    <div class="line">
    </div>
    <div class="center_header_page">
<!--    	 <div class="slider_buttom">
            	
            </div>
        <div class="slider_wrapper">
    	<div class="slider">
            <div class="slider_1">
            <img src="images/slider4.jpg" alt=""  />
                           <div class="slider_text">
               <h3>Take Your Organization Mobile - </h3>
               <p> Collect orders & reports, grow sales</p>
               </div>
            </div>

            <div class="slider_1">
            <img src="images/slider1.jpg" alt=""  />
                           <div class="slider_text">
               <h3>Get Peace of Mind - </h3>
               <p> Monitor where your employees are at anytime.</p>
               </div>
            </div>
            
            <div class="slider_1">
            <img src="images/slider2.jpg" alt=""  />
                           <div class="slider_text">
               <h3>Improve Productivity - </h3>
               <p> Send tasks directly to the mobile. Keep employees accountable.</p>
               </div>
            </div>
            
            <div class="slider_1">
            	<img src="images/slider3.jpg" alt=""  />
               <div class="slider_text">
               <h3>Ensure Safety - </h3>
               <p> Know the location of your family members or employees in case of emergency.</p>
               </div>
                         
            </div>
        </div>
        </div> -->
        	
        <div class="content" >
        	<div class="content_first">
            	<div class="content_first_heading">
                <img src="images/sub_line.png" alt=""  />
                <h2>   Pro NGO   </h2>
                <img src="images/sub_line.png" alt=""  />
                </div>
                <div class="clear"></div>
                <p>Pro NGO is part of the Pro Mobile family of products. Pro NGO is tailored to meet the location reporting and data collection requirements of NGOs. It is a powerful mobile solution that helps NGOs to quickly gather various forms of data from the field and monitor the location of of field staff. ProNGO allows you to manage all of this from the the comfort of your office or from your phone while you are on the move. <p>
	
<!--	   		<div class="content_learn content_learn_more ">
            	<h3>Learn more</h3>
                <img src="images/arrow.png" alt=""  />
            </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="content_first content_sec_heading">
                <img src="images/sub_line.png" alt=""  />
                <h2>   Features   </h2>
                <img src="images/sub_line.png" alt=""  />
               <div class="content_second">
               	<div class="content_sec_left">
                	<div class="content_sec_left_first content_sec_left_first_detail">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_1.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head">
                        	<h3>Live Tracking on Website</h3>
                        </div>
                        <div class="clear"></div>
                        <p>View the current location of a device in realtime.</p>
						<div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
               		 	</div>
                      </div>
                <div class="clear"></div>
                <div class="content_sec_left_first content_sec_left_first_detail">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_2.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_sec_left_margin">
                        	<h3>Historical Records</h3>
                        </div>
                        <div class="clear"></div>
                        <p>We store historical location records for upto three months for your reference.</p>
						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                		</div>
                       </div>
                <div class="clear"></div>
                <div class="content_sec_left_first content_sec_left_first_detail">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_3.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_sec_left_margin">
                        	<h3>Markers</h3>
                        </div>
                        <div class="clear"></div>
                        <p>We display clear markers on a map for you to track the route taken.</p>
						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                		</div>
                       </div>
                 <div class="clear"></div>
                <div class="content_sec_left_first content_sec_left_first_detail">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_7.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_sec_left_margin">
                        	<h3>Receive Orders & Reports</h3>                            
                        </div>
                        <div class="clear"></div>
                        <p>Provide this solution to your agents/distributors to receive orders or to staff to receive reports</p>
						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                        </div>
                      
                       </div>
                         <div class="clear"></div>
                    </div> 
             
                <div class="content_sec_right">
                	
                    <div class="content_sec_left_first">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_4.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_sec_right_margin">
                        	<h3>Manage Multiple Devices</h3>
                        </div>
                        <div class="clear"></div>
                        <p>You can monitor multiple devices from one account with ease.</p>
						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
               		 	</div>
                      </div> 
                      <div class="clear"></div>
                      <div class="content_sec_left_first">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_5.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_sec_right_margin">
                        	<h3>Boundaries</h3>
                        </div>
                        <div class="clear"></div>
                        <p>You can set boundaries that limit how far an individual can travel & get alerts if they step outside it.</p>
						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
               		 	</div>
                      </div>
                	 <div class="clear"></div>
                      <div class="content_sec_left_first">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_6.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_sec_right_margin">
                        	<h3>Alerts</h3>
                        </div>
                        <div class="clear"></div>
                        <p>Get email notifications when a report is received, task is completed, destination is reached or a boundary is crossed.</p>
						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
               		 	</div>
                      </div> 
                </div> 
               
            
            
           </div>-->
           <!--content_second-->
	     <div class="clear"></div>
         
         <div class="content_third"><!--content_third-->
         	<div class="content_third_left">
     <!--       	<div class="content_thir_left_first">
                	<h3>Testimonials</h3>
                    <div class="clear"></div>
                	<div class="content_third_left_first_block" >
                    <div class="clear"></div>
                    	<p>"We love this solution. It saves us time in monitoring our sales staff and ensuring they are productive."</p>
					<h4>Owner - TJD Computers</h4>
                    </div>
                </div>-->
                <div class="clear"></div>
				<a name="contact"></a>
                <div class="content_third_left_second">
                	<h3>Quick Contact</h3>
                    <div class="clear"></div>
					<form id="contact_form" action="javascript:void(0);" method="post">
                    <div class="content_third_left_contact">
                    	<div class="content_third_left_contact_input">
                    	<p>NAME:&nbsp;</p><input type="text" value="" name="txt_name" />
                        </div>
                        <div class="content_third_left_contact_input">
                        <p>EMAIL:</p><input type="text" value="" name="txt_email"  />
                        </div>
                        <div class="content_third_left_contact_txtarea">
                        <p>MESSAGE:</p><textarea  name="txt_message" rows="4" cols="20" ></textarea>
                        </div>
                        <div class="content_third_left_button">
                        	<a href="javascript:void(0);" id="submit_a">SEND</a>
                        </div>
                    </div>
					</form>
                     <div class="clear"></div>
                     <div class="content_third_left_buttom">
                     	<img src="images/phone.png" alt=""  /><p>Call Us Now! +255 688 121 252</p>
                     </div>
                </div>
            
            </div>
            <div class="content_third_center">
            </div>
            
            <div class="content_third_right">
            	<div class="content_third_right_heading">
                	<h2>Benefits</h2>
                </div>
                <div class="clear"></div>
                 <div class="content_third_right_first">
                    	<div class="content_third_right_mrg">
                        	<img src="images/check_simbols.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_third_right_mrg_head">
                        	<h3>Improve Accountability</h3>
                        </div>
                        <div class="clear"></div>
                        <p>Your field or data collection staff spend a large part of their day out of the office. Are they really working on your tasks. This solution helps you ensure they are working full time and reduce fraud in the process.</p>
<!--						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                        </div> -->
                      	 <div class="clear"></div>
                       </div>
                       <div class="clear"></div>
                 <div class="content_third_right_first">
                    	<div class="content_third_right_mrg">
                        	<img src="images/check_simbols.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_third_right_mrg_head">
                        	<h3>Reduce Manual Data Entry</h3>
                        </div>
                        <div class="clear"></div>
                        <p>Manual data entry will no longer exist as all your data will automatically be stored in an organized manner on our platform. This will also reduce the risk of invalid or incorrect data.</p>
						<!-- <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                        </div>-->
                      	 <div class="clear"></div>
                       </div>
                       <div class="clear"></div>
                 <div class="content_third_right_first">
                    	<div class="content_third_right_mrg">
                        	<img src="images/check_simbols.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_third_right_mrg_head">
                        	<h3>Easier Reporting</h3>
                        </div>
                        <div class="clear"></div>
                        <p>Easily export your tasks and reports to excel and quickly develop reports for management use.</p>
	<!--					 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                        </div>-->
                      	 <div class="clear"></div>
                       </div>
                       <div class="clear"></div>
                 <div class="content_third_right_first">
                    	<div class="content_third_right_mrg">
                        	<img src="images/check_simbols.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_third_right_mrg_head">
                        	<h3>Integrate with Your Systems</h3>
                        </div>
                        <div class="clear"></div>
                        <p>Our API will assist you to retrieve data from ProNGO for your internal processing and workflows.</p>
<!--						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                        </div> -->
                      	 <div class="clear"></div>
                       </div>
                       <div class="clear"></div>
                 <div class="content_third_right_first">
                    	<div class="content_third_right_mrg">
                        	<img src="images/check_simbols.png" alt=""  />
                        </div>
         <!--               <div class="content_sec_left_first_head content_third_right_mrg_head">
                        	<h3>Family</h3>
                        </div>
                        <div class="clear"></div>
                        <p>Track the location of your chilren or adult family members in realtime.</p>
						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                        </div>
                      	 <div class="clear"></div> -->
                       </div>
            </div>
            
         	 <div class="clear"></div>
         </div><!--content_third-->
            </div>
        </div>
    
    </div>
    <div class="footer-wrapper">
    	<div class="center_footer_page">
        	<div class="footer_center">
            	<div class="footer_navi">
 <!--           	<a href="" >Home</a>
                <a href="" >Company</a>
                <a href="" >Clients</a>
                <a href="" >Resources</a>
                <a href="" >Support</a>
                <a href="" >Blog</a> 
                <a href="" >Contact</a>-->
                <a href="" ><p>Copyright &copy; Bongo Live Enterprise Ltd. All Rights Reserved.</p></a>
                </div>
            </div>
         <div class="clear"></div>
        </div>
    </div>
    <div class="center_header_page">
    	<div class="footer_buttom">
<!--    	<h3>FOOT PRINT</h3><p>Copyrihgt &copy; 2012. All Rights Reserved.</p>-->
        </div>
         <div class="clear"></div>
    </div>
	
	<script type="text/javascript">
	$(document).ready(function(){
	 $("#submit_a").click(function(){
		
		submitForm();
	      
	    });
	 
	});
	function submitForm(){
	  
        var str = $("#contact_form").serialize();
        $.ajax({
		type : 'POST',
		url : 'ses_test/contactmail.php',
		data : str,
		success : function(data){
               alert("Your request has been submitted.");
			}  
		}); 
        return(false);
   
	}
	</script>
	<script src="js/analytics.js" type="text/javascript"></script>
</body>
</html>
