<?php
class LanguageLoader {
	function initialize() {
		$ci = &get_instance();
		$ci -> load -> helper('language');
		$ci -> load -> library('session');
		//the system languages
		$languages = array('english', 'kiswahili');
		//$ci -> lang -> load('main', 'kiswahili');

		$site_lang = $ci -> session -> userdata('site_lang');
		if (in_array($site_lang, $languages)) {

			if ($site_lang) {
				$ci -> lang -> load('main', $ci -> session -> userdata('site_lang'));
			} else {
				$ci -> lang -> load('main', 'english');
			}
	 
		} else {
				$ci -> lang -> load('main', 'english');
		}
	}

}
?>