<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
 
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('questions_list_rerank') ?></h3>      
                                   <!--   <a href="<?php echo site_url($create) ?>" style="float: right;margin:10px; " class="btn btn-primary" type="button" ><?php echo lang('new_question'); ?></a>-->
                                    <a href="<?php echo site_url($back) ?>" style="float: right;margin:10px;color:#fff; " class="btn btn-success" type="button" ><?php echo lang('save_n_back'); ?></a>                       
                                </div><!-- /.box-header -->
                                
                                 <div class="box-body">
                               		<div style="padding: 10px; border:1px solid lightgray;overflow: auto;background-color: #f3f4f5;font-weight: bold">
                               			<?php foreach ($tb_headers as $header){   ?>
                               			<span 
                               			<?php 
                               			if($header=='question_text'){ echo 'class="col-xs-7"';}elseif($header=='question_type'){ echo 'class="col-xs-2"';}else { echo 'class="col-xs-1"';} 
                               			?>><?php echo lang($header);?> </span>
                               		 
                               			<?php } ?>
                               		</div>
                                   	 	 
                                    <div id="qn_list"> 
                                         <ul id="sortable">
                                         <?php if ($tb_data){ 
									foreach ($tb_data as $data):   
                                            $id = $data[$table_id];
                                    ?>
                                      <li id="<?php echo $data['question_id']; ?>">
                                      	<span style="float: left;height: 100%;padding-top: 18px;background-color: #555555;"><img src="<?php echo base_url('template/images/drag.png'); ?>" /></span>
             	 						<span class="col-xs-7" style="height: 100%">  <?php echo $data['question_text'];?> </span> 
										<span class="col-xs-2"> <?php echo $data['question_type'];?> </span> 
										<span class="col-xs-1"> <?php echo $data['survey_section'];?> </span> 
										<span class="col-xs-1"> <?php echo $data['question_rank'];?> </span>
										<span class="col-xs-0"> <?php echo $data['language'];?> </span>
									</li>		
                                       <?php endforeach; } ?>
                                    </ul>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content --> 
                
</aside><!-- /.right-side -->
</div><!-- ./wrapper -->
 