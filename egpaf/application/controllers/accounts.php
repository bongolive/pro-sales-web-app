<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Accounts extends My_Controller {

	public $user_id;
	public $account_id;
	public $data;

	public function __construct() {

		parent::__construct();

		if (!$this -> session -> userdata('is_login') == TRUE) {
			$this -> session -> set_userdata('login_error', 'Please, Loggin first');
			redirect('login');
		}
		$this -> data['tb_headers'] = $this -> data['row_fields'] = $this -> data['tb_headers'] = array('organisation', 'contact_person', 'account_type', 'user_limit', 'mobile', 'email', 'status', 'admin', 'created_on');

		$this -> load -> model('accounts_model');
		$this -> load -> model('users_model');

		$this -> accounts_model -> form_validation();
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');

		$this -> data['view'] = 'accounts/view/';
		$this -> data['edit'] = 'accounts/update/';
		$this -> data['delete'] = 'accounts/delete/';

		$this -> data['table_id'] = $this -> accounts_model -> table_id;

		$this -> data['tb_name'] = 'accounts_list';
		$this -> data['controller'] = 'accounts';
		$this -> user_id = $this -> session -> userdata('user_id');
		$this -> account_id = $this -> session -> userdata('account_id');

		//filtering the property items according to companyies

		/******************** check user permissions ***************************/

		$modules = explode(',', $this -> session -> userdata('permissions'));
		//$this->data['is_allowed'] = $this -> permissions_model -> read_modules($modules);

		/******************** check user permissions ***************************/

	}

	public function index() {

		$this -> session -> unset_userdata('id');

		$this -> data['controller'] = 'accounts';

		if ($this -> input -> post('filter')) {
			extract($_POST);
			IF ($account) {
				$where['account_id'] = $account;
			}
		}

		$accounts = $this -> accounts_model -> read();
		if ($accounts) {
			$this -> data['tb_data'] = $accounts;
		} else {
			$this -> data['tb_data'] = FALSE;
		}

		//checking users permissions
		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/accounts_table');
		$this -> load -> view('template/table_helper');
		$this -> load -> view('template/footer');

	}

	//viewing
	public function view() {

		$id = $this -> uri -> segment(3);

		$where = array('accounts.account_id' => $id);

		//geting the data
		$acc = $this -> accounts_model -> read($where);

		if ($acc) {

			//foreach ($emp as $emp) {
			$this -> data['info'] = $acc;
			//	}
		} else {
			$this -> data['info'] = FALSE;
		}

		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/accounts_view', $this -> data);
		$this -> load -> view('template/footer');

	}

	public function create() {

		if ($this -> form_validation -> run() == FALSE) {
			$this -> data['info'] = FALSE;

			//checking users permissions
			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/accounts_form', $this -> data);
			$this -> load -> view('template/footer');
		} else {

			extract($_POST);

			//save user
			$user_data = array('username' => $username, 'password' => sha1($password), 'role' => $role, 'permissions' => '9', 'account_no' => $this -> account_id, 'created_on' => date('Y-m-d h:i:s'), 'status' => 1);

			$this -> db -> trans_start();

			$this -> accounts_model -> save_user($user_data);
			$admin = $this -> db -> insert_id();

			$data = array('admin' => $admin, 'organisation' => $organisation, 'contact_person' => $contact_person, 'account_type' => $account_type, 'user_limit' => $user_limit, 'mobile' => $mobile, 'email' => $email, 'status' => 1, 'created_on' => date('Y-m-d h:i:s'));

			$this -> accounts_model -> saving($data);

			$this -> db -> trans_complete();

			redirect('accounts');
		}
	}

	public function update() {
		if ($this -> form_validation -> run() == FALSE) {
			if ($this -> session -> userdata('id')) {
				$id = $this -> session -> userdata('id');
			} else {
				$id = $this -> uri -> segment(3);
				$this -> session -> set_userdata('id', $id);
			}

			$where = array('accounts.account_id' => $id);
			//geting the data
			$info = $this -> accounts_model -> read($where);
			if ($info) {
				foreach ($info as $info) {
					$this -> data['info'] = $info;
				}

				$this -> load -> view('template/header', $this -> data);
				$this -> load -> view('template/content/accounts_form', $this -> data);
				$this -> load -> view('template/footer');
			} else {
				redirect('accounts');
			}
		} else {

			extract($_POST);

			$permissions = 9;
			$data = array('organisation' => $organisation, 'contact_person' => $contact_person, 'account_type' => $account_type, 'user_limit' => $user_limit, 'mobile' => $mobile, 'email' => $email, 'status' => 1);

			$this -> accounts_model -> update($account_id, $data);

			$user_data = array('username' => $username, 'role' => $role, 'permissions' => $permissions);
			if ($password) {
				$user_data['password'] = sha1($password);
			}

			$this -> users_model -> update($user_id, $user_data);
			redirect('accounts');
		}

	}

	public function checkmail($email, $accounts_id) {

		$where = array('accounts.email' => $email);
		$accounts = $this -> accounts_model -> read($where);
		if ($accounts) {

			if ($accounts[0]['account_id'] == $accounts_id) {
				return TRUE;
			} else {
				$this -> form_validation -> set_message('checkmail', 'Sorry! the email is already used by someone else');
				return FALSE;
			}
		} else {
			return TRUE;
		}
	}

	public function checkmobile($mobile, $accounts_id) {

		$where = array('accounts.mobile' => $mobile);
		$accounts = $this -> accounts_model -> read($where);
		if ($accounts) {
			if ($accounts[0]['account_id'] == $accounts_id) {
				return TRUE;
			} else {
				$this -> form_validation -> set_message('checkmobile', 'Sorry! the mobile number is already used by someone else');
				return FALSE;
			}
		} else {
			return TRUE;
		}

	}

	public function delete() {
		$id = $this -> uri -> segment(5);
		$act = $this -> uri -> segment(4);
		if ($act == 'add') {
			$data = array('emp_status' => 1);
		} elseif ($act == 'remove') {
			$data = array('emp_status' => 0);
		}
		if ($this -> accounts_model -> update($id, $data)) {
			redirect('accounts');
		}
	}

}
