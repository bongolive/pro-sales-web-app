				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							
							<h2><?php
								if (!$info) {
									_l('devices');
								} else {
									echo _l('update_device') . ' : ' . strtoupper($info['device_imei']);
								}
							 ?></h2>
					   
            </header> 

        </article>

    </div>	
    <div class="row">
    	 <article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l($tb_name); ?></h4>
								</div>
								<div class="modal-body"> 
								
								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>

									<tr>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>
 				  <?php if ($tb_data){ 
									foreach ($tb_data as $data):  
									  
                                            $id = $table_id;
                                     ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php
											if ($field == 'device_status') {
												if ($data[$field] == 1) {
													echo '<span class="label label-success">Active</span>';
												} else {

													echo '<span class="label label-warning">In Active</span>';
												}
											} else {
												echo ucwords($data[$field]);

											}
										?></td>
                                        <?php endforeach; ?>
										
                                            <td> 
                                                	<a href="<?php echo site_url($edit) . '/' . $data[$id]; ?>" class="btn btn-small " title="Edit"><span class="awe-pencil"/></a>
                                                    <a href="<?php echo site_url($reset) . '/' . $data['device_imei']; ?>" class="btn btn-small " title="Reset Sync Status"><span class="awe-reorder"/></a>

                                                    <?php if($data['device_status']==1){?>
                                                	<a href="<?php echo site_url($delete) . '/remove/' . $data[$id]; ?>" class="btn btn-small btn-danger" title="Disable" ><span class="awe-remove"/></a>
                                                	<?php }else{ ?>
                                                		
													<a href="<?php echo site_url($delete) . '/add/' . $data[$id]; ?>" class="btn btn-small btn-success" title="Activate" ><span class="awe-plus"/></a>	
                                                	<?php } ?>
											</td>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
								</tbody>
	</table> 
           
		</article>							
 
    	 <article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l('device_details'); ?></h4>
								</div>
								<div class="modal-body">
									<?php
									$action = 'devices/update_device';
										 ?>
											
					 			 	 
                                <form class="form-horizontal" method="post" action="<?php echo site_url($action); ?>">
                                	<?php if($info){ ?>
                                		<input name="device_id" id="device_id" class="input-large" type="hidden"  value="<?php  echo $info['device_id']; ?>" />
                                		<?php } ?> 
                                    <fieldset>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="device_imei" ><?php _l('device_imei'); ?></label>
                                        	<div class="controls"><?php echo form_error('device_imei'); ?>
                                        	<input name="device_imei" id="device_imei" class="input-large" type="text"  value="<?php
												if ($info) { echo $info['device_imei'];
												} else { $value = set_value();
												}
 ?>" required />
                                        	</div>
                                    	</div>
									 
                                    	
                                    	 <div class="control-group">
                                        	<label class="control-label" for="device_descr" ><?php _l('device_descr'); ?></label>
                                        	<div class="controls"><?php echo form_error('device_descr'); ?>
                                        	<textarea name="device_descr" id="device_descr"   type="text"><?php
												if ($info) { echo $info['device_descr'];
												} else { $value = set_value();
												}
 ?></textarea>
                                        	</div>
                                    	</div>
                                        <div class="control-group">
                                        	<label class="control-label" for="assigned_to"><?php _l('assigned_to'); ?></label>
                                        	<div class="controls">
                                        		
                                        		<?php
												if ($employees == FALSE) {

													$employees = array('' => 'No Employee Assigned');
												}

												if ($info) {$value = $info['assigned_to'];
												} else { $value = set_value();
												}
												echo form_dropdown('assigned_to', $employees, $value);
                                        		?>
                                        		<?php echo form_error('assigned_to'); ?> 
                                        	</div>
                                    	</div>   
                                    	 
                                    		<div class="control-group">
                                        	<label class="control-label" for="device_mobile" ><?php _l('device_mobile'); ?></label>
                                        	<div class="controls">
                                        		<?php echo form_error('device_mobile'); ?>
                                        	<input name="device_mobile" id="description" class="input-large" type="number" value="<?php
													if ($info) { echo $info['device_mobile'];
													} else { $value = set_value();
													}
 ?>">
                                        	</div>
                                    	</div>
                                    		<!-- <div class="control-group">
                                        	<label class="control-label" for="assigned_on" ><?php _l('assigned_on'); ?></label>
                                        	<div class="controls"><?php echo form_error('assigned_on'); ?>
                                        	<input name="assigned_on" id="description"  class="input-large" type="date" value="<?php
												if ($info) { echo $info['assigned_on'];
												} else { $value = set_value();
												}
											?>" required>
                                        	</div>
                                    	</div> -->
                                    	   
								</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('devices'); ?>" class="btn btn-alt" data-dismiss="modal">Clear</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>
                        </div>                        	
                        </div>	
                        </section>
                        		
					 