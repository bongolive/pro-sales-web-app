<?php

class Services {
	private $conn;
	public $data;
	public $imei;

	function __construct() {
		$host = 'localhost';
		$username = 'admin';
		$password = 'Random123';
		$database = 'pro_stock';

		$this -> conn = mysqli_connect($host, $username, $password, $database) or die(mysqli_connect_error());

	}
 public function assign_customers($data) {

		//uploading customer details to mobile device

		$results = $this -> conn -> query("select * from pro_customers where device_imei = '" . $data['imei'] . "' and is_synced = 0 ") or die(mysqli_error($this -> conn));


		if ($results) {
		  $customers =array();
			while ($row = mysqli_fetch_assoc($results)) {
                $customers[]  = array('system_customer_id' => $row['customer_id'], 'customer_name' => $row['contact_person'], 'business_name' => $row['business_name'], 'mobile' => $row['mobile'], 'email' => $row['email'], 'street' => $row['street'], 'city' => $row['city'], 'lat' => $row['lat'], 'lng' => $row['longtd'], 'area' => $row['area']);

			}
            return $customers;
		}
		mysqli_close($this->conn);

	}

    public function assign_products($data) {
		$results = $this -> conn -> query("SELECT * FROM pro_sales_team LEFT JOIN pro_products on pro_products.product_id = pro_sales_team.product_id  WHERE pro_sales_team.device_imei='" . $data['imei'] . "' and is_synced = 0") or die(mysqli_error($this -> conn));

		if ($results) {
            $productlist = array();
			while ($row = mysqli_fetch_assoc($results)) {
                $productlist[] = array('sys_stock_id' => $row['stock_id'], 'prod_system_id' => $row['product_id'],
                    'stock_qty' => $row['stock_qty'], 'product_name' => $row['product_name'], 'packaging_type' => $row['package_type'],
                    'packaging_units' => $row['package_units'], 'unit_sale_price' => $row['unit_sale_price'],
                    'tax_rate' => $row['tax_rate'],'sku' => $row['sku_product_no'],'size' => $row['unit_size'],'measure' => $row['unit_measure']);

			}
            return $productlist;
		}

		mysqli_close($this->conn);

	}


	public function upload_customers($data) {

		//receiving customer details from mobile device
		$results = $this -> conn -> query("SELECT * FROM pro_devices WHERE device_imei ='" . $data['imei'] . "'") or die(mysqli_error($this -> conn));

        if ($results) {

            $flag = array();
            foreach($data['incommingcustomers'] as $key => $value):

                $row = mysqli_fetch_assoc($results);

                $customer = "INSERT INTO pro_customers (contact_person,business_name,device_imei, mobile, email, street, city, area, account_id, created_on, created_by, is_synced, lat, longtd) VALUES('" . $value['customer_name'] . "', '" . $value['business_name'] . "','" . $data['imei'] . "', '" . $value['mobile'] . "','" . $value['email'] . "','" . $value['street'] . "', '" . $value['city'] . "','" . $value['street'] . "','" . $row['account_id'] . "', '" . date('Y-m-d h:i:s') . "','" . $row['account_id'] . "',1,'" . $value['lat'] . "','" . $value['lng'] . "')";

                $query = $this -> conn -> query($customer) or die(mysqli_error($this -> conn));
                $id = $this -> conn -> insert_id;
                //get the inserted id
                if ($query) {
                    $flag[] = array('system_customer_id' => $id,'customer_id' => $value['customer_id']);
                }
                endforeach;
            $arrval['systemid'] = $flag;
            return $arrval;

		} else {
			return false;
		}

	}

	public function upload_orders_details($data) {//receive orders_details from devices

		//get accounts details
		$results = $this -> conn -> query("select account_id,assigned_to,device_imei from pro_devices where device_imei ='" . $data['imei'] . "'") or die(mysqli_error($this -> conn));


		if ($results) {
            $flag = array("success"=> 0);

            $row = mysqli_fetch_assoc($results);

            foreach($data['orderitems'] as $key => $value):


			//combine order id with a device imei
			$orderno = $row['device_imei'] . '.' . $value['order_id'];

			//get accounts details finished
			$qry = "INSERT INTO pro_sales_order_details  values(null,'" . $orderno . "','" . $data['imei'] . "','" . $value['prod_system_id'] . "', '" . $value['prod_order_qty'] . "', '" . $value['sale_price'] . "','" . $value['tax_rate'] . "','" . $row['account_id'] . "')";
 
			$save_order = $this -> conn -> query($qry) or die(mysqli_error($this -> conn));

			if ($save_order) {
                $where = array('prodid' => $value['prod_system_id'],'qty' => $value['prod_order_qty']);
                if($this -> update_assigned_stock($where)){
                    $flag = array('success' => 1);
                } else{
                    $flag = array('failed' => 1);
                }
			} else {

				$flag = array('failed' => 1);
			}
                endforeach;
            return $flag;
		} else {
            return false;
        }

	}

    public function update_assigned_stock($where){
        $prod = $where['prodid'];
        $query = $this->conn->query("SELECT assigned_stock FROM pro_products WHERE product_id = '$prod' ");
        if($query->num_rows > 0){
            $qt = $query->fetch_assoc();
            $qty = $qt['assigned_stock'];
            $newqty = $qty - $where['qty'];

            $q = $this->conn->query("UPDATE pro_products SET assigned_stock = '$newqty' WHERE product_id = '$prod'");

        if($q){
            return true;
        }
        }
        return false;
    }

    public function is_orderitem_stored($data){
        $result = $this->conn->query("SELECT * FROM pro_sales_order_details WHERE order_id = '$data'");
        if(mysqli_num_rows($result) > 0):
            return true;
            endif;
    }

	public function upload_orders($data) { //receive order details from devices


		//get accounts details
		$results = $this -> conn -> query("select account_id,assigned_to,device_imei from pro_devices where device_imei = '" . $data['imei'] . "'") or die(mysqli_error($this -> conn));

		if ($results) {

            $row = mysqli_fetch_assoc($results);

			//get accounts details finished
            foreach($data['orders'] as $key => $value):
			$created_on = date('Y-m-d h:i:s');

			//combine order id with a device imei
                $orderno = $row['device_imei'] . '.' . $value['order_id'];

            $transdata = array('accountid' => $row['account_id'],
                'created_by' => $value['customer_id'],
                'order_id' => $orderno,
                'trans_amount' => $value['payment_amount'],
                'trans_pay_mode' => $value['payment_mode']
            );


			$save_order = $this -> conn -> query("INSERT INTO pro_sales_orders (device_order_id,phone_imei, employee_id, order_date, customer_id, sales_total, total_tax, created_on, account_id, payment_status, payment_mode, receipt_no, paid_amount,order_status,lat,lng) VALUES ('" . $orderno . "','" . $data['imei'] . "','" . $row['assigned_to'] . "','" . $value['order_date'] . "','" . $value['customer_id'] . "','" . $value['total_sales'] . "','" . $value['total_tax'] . "', '" . $created_on . "','" . $row['account_id'] . "','" . $value['payment_status'] . "','" . $value['payment_mode'] . "','" . $value['receipt_no'] . "','" . $value['payment_amount'] . "',2,'" . $value['lat'] . "','" . $value['lng'] . "')") or die(mysqli_error($this -> conn));

			if ($save_order) {
                $this->saveTransaction($transdata);

				$flag = array('success' => 1);
			} else {

				$flag = array('failed' => 1);
			}
            endforeach;

		} else {
			$flag = array('failed' => 1);
		}


		return $flag;
	}

	/*
	 * receiveing acknoledgements form mobile
	 */

	public function update_customer($data) {

		$data = $this -> conn -> query('update  pro_customers set is_synced = 1 where customer_id="' . $data['system_customer_id'] . '"') or die(mysqli_error($this -> conn));
		if ($data) {
			return TRUE;
		} else {
			return 'update  pro_customers set is_synced = 1 where customer_id="' . $data['system_customer_id'] . '"';
		}

	}

	public function update_products($data) {
		$sql = 'update  pro_sales_team set is_synced = 1 where stock_id="' . $data['sys_stock_id'] . '"';
		$data = $this -> conn -> query($sql) or die(mysqli_error($this -> conn));
		if ($data) {
			return 'ni kweli:' . $sql;
		} else {
			return 'hapan:' . $sql;
		}
	}

	/*
	 * end of receiving acknowledgements
	 */

	//read settings
	public function read_settings($data) {//receive order details from devices
		//get accounts details

        if($this->getSyncStatus($data)){

            $results = $this->conn->query("select account_id from pro_devices where device_imei = '" . $data['imei'] . "'") or die(mysqli_error($this->conn));

            if ($results) {

                $acc = mysqli_fetch_assoc($results);

                //get accounts details
                $results = $this->conn->query("select * from pro_settings where account_id = '" . $acc['account_id'] . "'") or die(mysqli_error($this->conn));

                if ($results) {
                    $sets = array();
                    $row = mysqli_fetch_assoc($results);
                    $sets[] = array('include_tax' => $row['include_tax'], 'vat' => 18, 'enable_discounts' => $row['enable_discounts'],
                        'print_receipt' => $row['print_receipts'], 'alter_sales_price' => $row['use_fixed_prices'],
                        'discount_limit' => $row['discount_limit'], 'default_currency' => 'TZS', 'use_fixed_prices' => 0, 'tracktime' => $row['tracking_interval']);

                    return $sets;
                } else {
                    return FALSE;
                }
            } else {
                return False;
            }
        }

	}

    public function getSyncStatus($data){
        $productsrow = $this -> conn -> query("SELECT COUNT(*) FROM pro_sales_team
                   LEFT JOIN pro_products on pro_products.product_id = pro_sales_team.product_id
                   WHERE pro_sales_team.device_imei='" . $data['imei'] . "'")
        or die(mysqli_error($this -> conn));

        $syncedproducts = $this -> conn -> query("SELECT COUNT(*) FROM pro_sales_team
                   LEFT JOIN pro_products on pro_products.product_id = pro_sales_team.product_id
                   WHERE pro_sales_team.device_imei='" . $data['imei'] . "' and is_synced = 1")
        or die(mysqli_error($this -> conn));

        if($productsrow == $syncedproducts){
            return true;
        }
        return false;
    }



	//read pro_payment_mode
	public function read_payment_mode($data) {//receive order details from devices
		//get accounts details

    if($this->getSyncStatus($data)) {
        $results = $this->conn->query("select account_id from pro_devices where device_imei = '" . $data['imei'] . "'") or die(mysqli_error($this->conn));

        if ($results) {
            $acc = mysqli_fetch_assoc($results);
            //get accounts details
            $results = $this->conn->query("select * from pro_payment_mode where account_id = '" . $acc['account_id'] . "'") or die(mysqli_error($this->conn));

            if ($results) {
                $modes = array();
                while ($row = mysqli_fetch_assoc($results)) {
                    $modes[] = array('system_mode_id' => $row['mode_id'], 'mode_title' => $row['mode_name'], 'mode_symbol' => $row['mode_symbol']);
                }
                return $modes;
            } else {
                return FALSE;
            }
        } else {
            return False;
        }
    }

	}



	function cleanQuery($string) {
		if (get_magic_quotes_gpc())// prevents duplicate backslashes
		{
			$string = stripslashes($string);
		}
		if (phpversion() >= '4.3.0') {
			$string = mysqli_real_escape_string($string);
		} else {
			$string = mysqli_escape_string($string);
		}
		return $string;
	}

	/*
	 * unseting is sync
	 */

	public function un_sync() {

		//uploading customer details to mobile device

		$results = $this -> conn -> query("update pro_customers set is_synced  = 0") or die(mysqli_error($this -> conn));

		$results = $this -> conn -> query("update pro_sales_team set is_synced  = 0") or die(mysqli_error($this -> conn));
		if ($results) {
			file_put_contents("received_data.txt", date('Y-m-d'));
			return true;
		}
		//clear log contents

	}


    public function saveTransaction($data){
        $transdata = array_values($data);

        $transdate = date('Y-m-d h:i:s');
        $transtoken = $this->generate_token(2);
        $orderid = $this->getOrderId($transdata[2]);

        $report = $this->conn->query("INSERT INTO pro_transactions (trans_date, order_id, trans_amount, old_payments, trans_pay_mode,
         created_by, account_id, trans_status, transaction_token) VALUES ('".$transdate."','".$orderid."','".$transdata[3]."',
         '0','".$transdata[4]."','".$transdata[1]."','".$transdata[0]."','1','".$transtoken."')");
        if($report){
            return true;
        } else {
            echo mysqli_error($this->conn);
        }
    }
//account_id,created_by,order_id,trans_amount,trans_pay_mode
    public function generate_token ($len = 15)
    {

        $date = date('Y.m.d');

        $chars = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        );
        shuffle($chars);
        $num_chars = count($chars) - 1;
        $token = '';


        for ($i = 0; $i < $len; $i++)
        {
            $token .= $chars[mt_rand(0, $num_chars)];
        }
        $n = $this->getLastTokenId();
        $token = $token.$date."-".$n;
        return $token;
    }

    public function getLastTokenId(){
        $query = $this->conn->query("SELECT * FROM pro_transactions");
        if($query->num_rows > 0)
        {
            return $query->num_rows;
        }
    }


        public function getOrderId($data){
            $query = $this->conn->query("SELECT order_id FROM pro_sales_orders WHERE device_order_id = '$data' ");
            if($query->num_rows > 0){
                $order = $query->fetch_assoc();
                $orderid = $order['order_id'];
                return $orderid;
            }
    }

    public function receive_location_updates($data)
    {
        $results = $this->conn->query("select account_id from pro_devices where device_imei = '" . $data['imei'] . "'") or die(mysqli_error($this->conn));

        if ($results) {
            $flag = array("success"=> 0);

            foreach($data['datapoints'] as $key => $value):

                $arrayvalues = array(
                    'latitude' => $value['lat'], 'longitude' => $value['lng'], 'date' => $value['date'],
                    'servertime' => date('Y-m-d h:i:s'), 'imei' => $data['imei']
                );
                $values = '\''. implode('\',\'', $arrayvalues).'\'';
                $fields = '`'. implode('`,`', array_keys($arrayvalues)).'`';

                $query = $this->conn->query("INSERT INTO pro_location ($fields) VALUES ($values)") or die(mysqli_error($this -> conn));

                //get accounts details finished

                if ($query) {

                    $flag = array('success' => 1);

                } else {

                    $flag = array('failed' => 1);
                }

            endforeach;

            return $flag;
        } else {
            return false;
        }

    }
}

