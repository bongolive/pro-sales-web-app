</aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<!-- jQuery 2.0.2 -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<?php
if($this->session->userdata('sortable')){
$js = array('jquery-ui-1.10.3.min.js', 'bootstrap.min.js', 'plugins/morris/morris.min.js', 'plugins/sparkline/jquery.sparkline.min.js', 'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js', 'plugins/jvectormap/jquery-jvectormap-world-mill-en.js', 'plugins/fullcalendar/fullcalendar.min.js', 'plugins/jqueryKnob/jquery.knob.js', 'datepicker/datepicker.js', 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js', 'plugins/iCheck/icheck.min.js', 'AdminLTE/app.js', 'AdminLTE/dashboard.js', 'plugins/datatables/jquery.dataTables.js', 'plugins/datatables/dataTables.bootstrap.js',  'jquery-1.10.2.js', 'jquery-ui-1.10.4.custom.min.js', 'script.js', );
$this->session->unset_userdata('sortable');	
}else{
	$js = array('jquery-ui-1.10.3.min.js', 'bootstrap.min.js', 'plugins/morris/morris.min.js', 'plugins/sparkline/jquery.sparkline.min.js', 'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js', 'plugins/jvectormap/jquery-jvectormap-world-mill-en.js', 'plugins/fullcalendar/fullcalendar.min.js', 'plugins/jqueryKnob/jquery.knob.js', 'datepicker/datepicker.js', 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js', 'plugins/iCheck/icheck.min.js', 'AdminLTE/app.js', 'AdminLTE/dashboard.js', 'plugins/datatables/jquery.dataTables.js', 'plugins/datatables/dataTables.bootstrap.js',  );
}
get_js($js);
?>
<script type="text/javascript">
  

	function confdeleting() {
		var answer = confirm("Are you sure you want to delete !!");
		if (answer) {
			document.messages.submit();
		}

		return false;
	}
	function confdeactivate() {
		var answer = confirm("Confirm Deactivating The Device !!");
		if (answer) {
			document.messages.submit();
		}

		return false;
	}function confdeactivate_user() {
		var answer = confirm("Confirm Deactivating The User !!");
		if (answer) {
			document.messages.submit();
		}

		return false;
	}
</script>

</body>
</html>