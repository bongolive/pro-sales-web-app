<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Safi  - Project</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- bootstrap 3.0.2 -->

		<?php
		$css = array('bootstrap.min.css', 'font-awesome.min.css', 'ionicons.min.css', 'morris/morris.css', 'jvectormap/jquery-jvectormap-1.2.2.css', 'fullcalendar/fullcalendar.css', 'datepicker/datepicker.css', 'bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css', 'AdminLTE.css', 'datatables/dataTables.bootstrap.css');

		get_css($css);
	 
		?>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
		.req{color:darkred}
			.no-print {
				display: none;
			}
			/* Sortable ******************/
			#sortable {
				list-style: none;
				text-align: left;
			}
			#sortable li {
				margin: 0 0 10px 0;
				height:70px;
				border:1px solid lightblue;
				cursor: move; 
			}
			#sortable li span {
			  
				cursor: move;
			}
			 
			#sortable li div {
				padding: 5px;
			}
			#sortable li h2 {
				font-size: 16px;
				line-height: 20px;
			}
#qn_list ul{
	padding:0;
}
		</style>
	</head>
	<body class="skin-blue">

		<div class="modal fade" id="suryestastus" tabindex="-1" role="dialog" aria-labelledby="suryestastus" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"   aria-hidden="true">
							&times;
						</button>
						<h4 class="modal-title" id="myModalLabel">Changing Suryey Status</h4>
					</div>
					<div class="modal-body">
						<h3>Are you Sure You Want to Change Survey Status</h3>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">
							Close
						</button>
						<button type="button" class="btn btn-primary">
							Change
						</button>
					</div>
				</div>
			</div>
		</div>

		<!-- header logo: style can be found in header.less -->
		<header class="header">
			<a href="index.html" class="logo"> <!-- Add the class icon to your logo image or logo icon to add the margining --> Safi - Project</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
				<div class="navbar-right">
					<ul class="nav navbar-nav">

						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-user"></i> <span><?php echo $this -> session -> userdata['username']; ?>
								<i class="caret"></i></span> </a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header bg-light-blue">
									<!--<img src="img/avatar3.png" class="img-circle" alt="User Image" />-->
									<p>
										<?php echo $this -> session -> userdata['username'] . ' - ' . $this -> session -> userdata['role_title']; ?>

									</p>
								</li>
								<!-- Menu Body -->

								<!-- Menu Footer-->
								<li class="user-footer">
									<!--<div class="pull-left">
									<a href="#" class="btn btn-default btn-flat">Profile</a>
									</div> -->
									<div class="pull-right">
										<a href="<?php echo site_url('login/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<div class="wrapper row-offcanvas row-offcanvas-left">
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="left-side sidebar-offcanvas">
				<?php
				if ($this -> session -> userdata('role_title') == 'super') {
					@require_once ('super_menus.php');
				} else {
					@require_once ('main_menus.php');
				}
				?>
				<!-- /.sidebar -->
			</aside>
			<!-- Right side column. Contains the navbar and content of the page -->
			<aside class="right-side">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1> <?php echo lang($controller); ?>
					<small>Management Panel</small></h1>
					<ol class="breadcrumb">
						<li>
							<a href="#"><i class="fa fa-dashboard"></i> Home</a>
						</li>
						<li>
							<a href="#">Tables</a>
						</li>
						<li class="active">
							Data tables
						</li>
					</ol>
				</section>
