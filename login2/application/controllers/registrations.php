<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Registrations extends User_Controller {
	
	public $session_userid = '';
	public function __construct()
	{
		parent::__construct();
		$this->load->model('registrations_model');
		$this->session_userid = $this->session->userdata('userid');
	}

	public function index()
	{
		$condUserPerm = array('userid'=>$this->session->userdata('userid'));
		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] ="";
		$header['device_tab'] ="";
		$header['tasks_tab'] ="";
		$header['reports_tab'] ="";
		$header['deliveries_tab'] ="";
		$header['customers_tab'] ="";
		$header['boundaries_tab'] ="";
		$header['support_tab'] ="allow";
		$header['pathfinder_tab'] ="";
		$header['stocks_tab'] ="";
		$header['registrations_tab'] ="";
		
		foreach($user_permissions as $user_permission){
			if($user_permission['permission_name']=='track tab'){
				$header['track_tab'] ="allow";
			}else if($user_permission['permission_name']=='device tab'){
				$header['device_tab'] ="allow";
			}if($user_permission['permission_name']=='tasks tab'){
				$header['tasks_tab'] ="allow";
			}if($user_permission['permission_name']=='reports tab'){
				$header['reports_tab'] ="allow";
			}if($user_permission['permission_name']=='deliveries tab'){
				$header['deliveries_tab'] ="allow";
			}if($user_permission['permission_name']=='customers tab'){
				$header['customers_tab'] ="allow";
			}if($user_permission['permission_name']=='boundaries tab'){
				$header['boundaries_tab'] ="allow";
			}if($user_permission['permission_name']=='pathfinder survey tab'){
				$header['pathfinder_tab'] ="allow";
			}if($user_permission['permission_name']=='stocks tab'){
				$header['stocks_tab'] ="allow";
			}if($user_permission['permission_name']=='registrations tab'){
				$header['registrations_tab'] ="allow";
			}						
		}
		
		if($header['registrations_tab'] !="allow"){
			redirect('profile/support');
		}
		
		$cond = array('user_id'=>$this->session_userid);
		$fields['tb_data'] = $this->registrations_model->getRegistrations($cond);
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js','custom/custom-datatable.js');
        $fields['tb_name'] = 'registrations_tb_name';
        $fields['row_fields'] = array('fullname','gender','dob','phone','email','address');
        $fields['tb_headers'] = array('fullname','gender','dob','phone','email','address','action');
        $header['cus_active'] = 'class="active"';
        $fields['add_btn'] = 'new_registration';

        $this->load->view('template/header',$header);
        $this->load->view('template/content/table',$fields);
        $this->load->view('template/footer',$ft_data);

	}

}



/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */
