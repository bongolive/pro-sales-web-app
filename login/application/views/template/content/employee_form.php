				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							
							<h2><?php
								if (!$info) {
									_l('employee_details');
								} else {
									echo _l('employee_details') . ' : ' . strtoupper($info['firstname'] . ' ' . $info['lastname']);
								}
							 ?></h2>
					   
            </header> 

        </article>

    </div>	
    <div class="row">
    	  
<?php
			if ($info) {
				echo form_open('employees/update', array('class' => 'form-horizontal'));
				echo '<input type="hidden" name="employee_id" value="' . $info['id'] . '"/>
				<input type="hidden" name="user_id" value="' . $info['user_id'] . '"/>';
			} else {
				echo form_open('employees/new_employee', array('class' => 'form-horizontal'));
			}
		?>
<article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l('employee_details'); ?></h4>
								</div>
								<div class="modal-body">
  
 <div class="control-group">
	<span class="control-label"><?php _l('firstname'); ?></span> <?php echo form_error('firstname'); ?>
<div class="controls"><input type="text" name="firstname" value="<?php
		if ($info) { echo $info['firstname'];
		} else {echo set_value('firstname');
		}
 ?>" size="50" /></div>
</div>
 <div class="control-group">
<span class="control-label"><?php _l('lastname')?></span><?php echo form_error('lastname'); ?>
<div class="controls"><input type="text" name="lastname" value="<?php
	if ($info) { echo $info['lastname'];
	} else {  echo set_value('lastname');
	}
 ?>" size="50" /></div>
</div>

 <div class="control-group">
<span class="control-label"> <?php _l('gender'); ?></span><?php echo form_error('gender'); ?>
<div class="controls">
<?php
$options = array('male' => 'Male', 'female' => 'Female', );
if ($info) {$sex = $info['gender'];
} else {$sex = "";
}
echo form_dropdown('gender', $options, $sex);
?> 
</div>
</div>

 <div class="control-group">
<span class="control-label"><?php _l('role'); ?></span><?php echo form_error('role'); ?>
<div class="controls">
<?php
	$role = array('' => 'Select', 'sales' => 'Sales', 'stockmanager' => 'Stock Manager', 'marketing' => 'Marketing', 'supervisor' => 'Supervisor', 'manager' => 'Manager', 'accountant' => 'Accountant');
	if ($info) {$role1 = $info['emp_role'];
	} else {$role1 = set_value('role');
	}
	echo form_dropdown('role', $role, $role1);
?>
 </div>
</div> 
 <div class="control-group">
<span class="control-label"><?php _l('doe'); ?></span><?php echo form_error('doe'); ?>
<div class="controls"><input type="date"   name="doe" value="<?php
	if ($info) { echo $info['doe'];
	} else { echo set_value('doe');
	}
 ?>" size="50"  /></div>
</div> 
 <div class="control-group">
<span class="control-label"><?php _l('mobile'); ?></span><?php echo form_error('mobile'); ?>
<div class="controls"><input type="text" name="mobile" value="<?php
	if ($info) { echo $info['mobile'];
	} else { echo set_value('mobile');
	}
?>" size="50"  /></div>
</div> 

 <div class="control-group">
<span class="control-label"> <?php _l('email'); ?></span> <?php echo form_error('email'); ?>
<div class="controls"><input type="text" name="email" value="<?php
	if ($info) { echo $info['email'];
	} else { echo set_value('email');
	}
 ?>" size="50" /> </div>
</div> <div class="control-group">
<span class="control-label"> <?php _l('commission'); ?></span> <?php echo form_error('commission'); ?>
<div class="controls"><input type="number" min="0" step="any" name="commission" value="<?php
	if ($info) { echo $info['commission'];
	} else { echo set_value('commission');
	}
 ?>" size="50" /> </div>
</div>
 </article>
 <article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l('user_details'); ?></h4>
								</div>
							 
 <div class="control-group">
	<span class="control-label"><?php _l('username'); ?></span> <?php echo form_error('username'); ?>
<div class="controls"><input type="text" name="username" value="<?php
		if ($info) { echo $info['username'];
		} else {echo set_value('username');
		}
 ?>" size="50" /></div>
</div>
 <div class="control-group">
<span class="control-label"><?php _l('password')?></span><?php echo form_error('password'); ?>
<div class="controls"><input type="password" name="password" value="" size="50" /></div>
</div>
 
 
 <div class="control-group">
<span class="control-label"><?php _l('conf_password'); ?></span><?php echo form_error('conf_password'); ?>
<div class="controls"><input type="password"  name="conf_password" value="" size="50"  /></div>
</div> 
<!-- <div class="control-group">
<span class="control-label"><?php /*_l('permissions'); */?></span><?php /*echo form_error('permissions'); */?>
<div class="controls">
	</div>
	</div>
 <div class="control-group">	
	<div class="row">
		
	<?php
/*	if($modules){
		foreach ($modules as $module) {
			if($info){
				
				$persm = explode(',',  $info['permissions']);
			}
			
			*/?>
 <div class="span2" style="margin-left: 70px;margin-bottom: 1px;">
<input id="optionsCheckbox" type="checkbox" name="permission[]" accept=""
<?php
/*if($info){
foreach ($persm as $perms) {
	if ($perms == $module['id']) { echo "checked";
	}
 }
}
*/?>

 value="<?php /*echo $module['id']; */?>"/>
<?php /*echo $module['module_name'] */?>
  </div>
<?php	/*	}

	}else{
	echo "No Permission_modules";
	}
	*/?>
	
	<input type="hidden" name="permissions" value="<?php
/*		if ($info) { echo $info['permissions'];
		} else { echo set_value('permissions');
		}
	*/?>" size="50"  /></div>
</div> 
  -->
<div class="form-actions"> 

<a href="<?php echo site_url('employees'); ?>" class="btn btn-large">Cancel</a>
 <button type="submit" class="btn btn-info btn-large"><i class="icon-pencil"></i>Save</button></div>

</fieldset>
 
</div>
</div>
</article></section> 

