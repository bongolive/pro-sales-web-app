				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('current_stock'); ?></h2>
							
							<ul class="data-header-actions">
<li>
<a class="btn btn-alt" href="<?php echo site_url('stock/sales_team'); ?>" style="  padding:8px;"><?php _l('back'); ?></a>
</li>
</ul> 
							
							
            </header> 
        </article>

    </div>	

                           <div class="row"> 
                        <article class="span12 data-block">
								<div class="modal-header">
									<h4><?php _l('current_assigned_stock');
										?>
									</h4>
									
								
									
									
								</div>
								<div class="modal-body"> 
								
								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>

									<tr>
<th>No.</th>
                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>
 				  <?php if ($current_stock){
 				  	$i = 1; 
									foreach ($current_stock as $data):
                                     ?>
									<tr >
										<td><?php echo $i; ?></td>
                                        <td><?php echo $data['product_name']; ?></td>
                                        <td><?php echo $data['sales_person']; ?></td>
                                        <td><?php echo $data['assigned_qty']; ?></td>
                                        <td><?php echo $data['remained_qty']; ?></td>
                                        <td><?php echo $data['stock_date']; ?></td>
                                        <td><?php echo $data['updated_date']; ?></td>
									</tr>
									<?php
									$i++;
									endforeach;

									}else {
									echo '<tr> <td> No data available <td> </tr>';
									}
									?>
								</tbody>
	</table> 
           
		</article>		
                        
                        </div>                        	
                        </div>	
                        </section> 