<?php

/**
 * This file carries the login values for language in English
 */

$lang['welcome_user'] = 'Welcome User';
$lang['invalid_login'] = 'Invalid Username or Password';
$lang['payment_expire'] = 'Payment Expire';
$lang['enter_your_name'] = 'Enter your Name';
$lang['password'] = 'Password';
$lang['login'] = 'Login';
$lang['forget_password'] = 'Forgot Your Password';
$lang['submit_email'] = 'Submit Email';
$lang['enter_email'] = 'Enter email';
$lang['no_email_client'] = 'No Client with that Email';
$lang['email_empty'] = 'Email field is empty empty';
$lang['please__check_email'] = 'Please check your emails';