<?php
class Customers_model extends MY_Model {
	public $table = 'customers';
	public $table_id = 'customer_id';

	function __construct() {
		parent::__construct();
	}

	public function form_validation() {

		$this -> form_validation -> set_rules('business_name', 'Business name', 'trim|required');
		$this -> form_validation -> set_rules('contact_person', 'Contact person', 'trim|required');

		$this -> form_validation -> set_rules('mobile', 'mobile', 'required|numeric||callback_checkmobile[' . $this -> input -> post('customer_id') . ']');
		$this -> form_validation -> set_rules('email', 'email', 'valid_email|callback_checkmail[' . $this -> input -> post('customer_id') . ']');
	}

	public function read_sales_team($where) {

		$this -> db -> select('*,CONCAT(firstname," ",lastname) as employee_name', FALSE);
		$this -> db -> from('employees');
		$this -> db -> where($where);
		$this -> db -> join('devices', 'devices.assigned_to = employees.id');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}

	}
    public function reset_device($where){
        $this-> db -> where('device_imei',$where);
        $data = array('is_synced'=>'0');

        $cust = $this->db->update('pro_customers',$data);
        if($cust){
            return true;
        }
    }

    public function _api_get_assigned_customers($data){
        $this->db->where($data);
        $q = $this->db->get('pro_customers') ;
        if($q->num_rows() > 0){
            $customers =array();
            foreach($q->result_array() as $row){
                $customers[]  = array('system_customer_id' => $row['customer_id'], 'customer_name' => $row['contact_person'], 'business_name' => $row['business_name'], 'mobile' => $row['mobile'], 'email' => $row['email'], 'street' => $row['street'], 'city' => $row['city'],
                    'lat' => $row['lat'], 'lng' => $row['longtd'], 'area' => $row['area']);
            }
            return $customers;
        } 
    }

    public function _api_receive_device_customer($commons,$data){
        $this -> db -> trans_start();
        $this->db->where($commons);

        $flag = array();

        foreach($data as $value){

            $vals = array('contact_person'=> $value['customer_name'],
            'business_name' => $value['business_name'], 'device_imei' => $commons['device_imei'],
             'mobile' => $value['mobile'],'email' => $value['email'],
             'street' => $value['street'],'area' => $value['city'],
             'account_id' => $commons['account_id'],'created_on' => date('Y-m-d h:i:s'),
             'created_by' => 1,'is_synced' => 1
            );

            $cust = $this->db->insert('pro_customers',$vals);
            if($cust){
                $id = $this -> db -> insert_id();
                $flag[] = array('system_customer_id' => $id,'customer_id' => $value['customer_id']);
            }

            $this -> db -> trans_complete();
        }
        $arrval['systemid'] = $flag;
        return $arrval;
    }
    public function _api_update_sales_team_sync($where){
        $this->db->where($where);
        $update = $this->db->update('pro_customers',array('is_synced' => 1));
        if($update){
            return true;
        }
    }

}
