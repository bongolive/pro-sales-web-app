<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Surveys extends My_Controller {
	public $user_id, $account_id, $data;

	public function __construct() {
		parent::__construct();

		if ($this -> session -> userdata('is_login') == FALSE) {
			redirect('login', 'refresh');
		}

		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('survey_title', 'start_date', 'end_date', 'assigned_to', 'status');

		$this -> data['ft_data'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js', 'js/libs/modernizr.js', 'js/libs/selectivizr.js');

		$this -> load -> model('surveys_model');
		$this -> load -> model('survey_sections_model');
		$this -> load -> model('locations_model');
		$this -> load -> model('questionaires_model');
		$this -> data['table_id'] = $this -> surveys_model -> table_id;

		$this -> surveys_model -> form_validation();
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');

		//print_r($this->session->all_userdata());
		$this -> user_id = $this -> session -> userdata('user_id');
		$this -> account_id = $this -> session -> userdata('account_id');

		$this -> data['controller'] = 'surveys';
		$this -> data['edit'] = 'surveys/update';
		$this -> data['view'] = 'surveys/view';
		$this -> data['delete'] = 'surveys/delete';

	}

	public function index() {

		$this -> data['controller'] = 'surveys';
		//surveys
		$surveys = $this -> surveys_model -> read(array('account_no' => $this -> account_id));
		if ($surveys) {
			$surv = array('' => 'Select Survey');
			foreach ($surveys as $surveys) {
				$surv[$surveys['survey_id']] = $surveys['survey_title'];
			}
		} else {
			$surv[''] = 'No Survyes Avalable';
		}

		$location = $this -> locations_model -> read(array('account_no' => $this -> account_id));
		if ($location) {
			$facilities = array('' => 'Select Facility');
			foreach ($location as $location) {
				$facilities[$location['location_id']] = $location['location_name'];
			}
		} else {
			$facilities[''] = 'No Facilities Avalable';
		}

		$this -> data['surveys'] = $surv;
		$this -> data['facilities'] = $facilities;

		$where = array('surveys.account_no' => 1);
		//filtering data

		if ($this -> input -> post('filter')) {
			extract($_POST);

			if ($survey_title) {
				$where['survey_id'] = $survey_title;
			}
			if ($survey_title == FALSE) {
				$where = FALSE;
			}

		}
		$surveys = $this -> surveys_model -> read($where);

		if ($surveys) {
			$this -> data['tb_data'] = $surveys;
		} else {
			$this -> data['tb_data'] = FALSE;
		}

		$this -> data['tb_name'] = 'surveys_tb_name';

		$this -> data['stc_active'] = 'class="active"';
		$this -> data['add_btn'] = 'add_new_stock';

		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/surveys_table');
		$this -> load -> view('template/table_helper');
		$this -> load -> view('template/footer');

	}

	public function view() {

		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('section_title', 'qn_numbering');
		//$this -> data['row_fields'] = $this -> data['tb_headers'] = array('question_text', 'question_type', 'question_rank');
		$where = array('survey_id' => $this -> uri -> segment(3));

		$surveys = $this -> surveys_model -> read($where);

		if ($surveys) {

			foreach ($surveys as $surveys) {

			}

			$this -> data['survey'] = $surveys;
			$where = array('survey_no' => $surveys['survey_id']);

			//read survey sections

			$sections = $this -> survey_sections_model -> read($where);
			if ($sections) {

				$this -> data['tb_data'] = $sections;

			} else {
				$this -> data['tb_data'] = FALSE;
			}

			$this -> data['info'] = FALSE;

			/*//get questions in a survey
			 if ($surveys['no_questions'] > 0) {

			 $this -> data['pagenate'] = $this -> pagination($surveys['no_questions'], $this -> limit, $this -> page);
			 } else {
			 $this -> data['pagenate'] = FALSE;
			 }
			 $data = $this -> questionaires_model -> read_questions($where, $this -> limit, $this -> startpoint);

			 $this -> data['tb_data'] = $data;*/

			$this -> data['tb_name'] = 'surveys_tb_name';

			$this -> data['stc_active'] = FALSE;
			$this -> data['add_btn'] = FALSE;

			//questionaires Options

			$this -> data['edit'] = 'questionaires/update_question';
			$this -> data['view'] = 'questionaires/view';
			$this -> data['delete'] = 'questionaires/delete';

			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/surveys_view', $this -> data);
			$this -> load -> view('template/table_helper');
			$this -> load -> view('template/footer');

		} else {
			redirect('surveys');

		}

	}

	public function create() {
		if (!in_array(3, $this -> user_permissions)) {
			redirect('login/logout');

		}
		if ($this -> form_validation -> run() == FALSE) {

			$this -> data['info'] = FALSE;

			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/surveys_form', $this -> data);
			$this -> load -> view('template/footer');

		} else {
			extract($_POST);

			$surveys = array('survey_title' => $survey_title, 'assigned_to' => $assigned_to, 'cap_size' => $cap_size, 'key_word' => $key_word, 'no_questions' => $no_questions, 'descriptions' => $descriptions, 'start_date' => $start_date, 'end_date' => $end_date, 'account_no' => $this -> account_id);

			$this -> surveys_model -> save($surveys);

			redirect('surveys');
		}

	}

	public function update($survey) {
		if (!in_array(3, $this -> user_permissions)) {
			redirect('login/logout');

		}
		if ($this -> form_validation -> run() == FALSE) {

			$where = array('survey_id' => $this -> uri -> segment(3));

			$surveys = $this -> surveys_model -> read($where);

			if ($surveys) {
				foreach ($surveys as $surveys) {
				}
				$this -> data['info'] = $surveys;
			} else {
				$this -> data['info'] = FALSE;
			}

			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/surveys_form', $this -> data);
			$this -> load -> view('template/footer');

		} else {
			extract($_POST);

			$surveys = array('survey_title' => $survey_title, 'assigned_to' => $assigned_to, 'cap_size' => $cap_size, 'key_word' => $key_word, 'no_questions' => $no_questions, 'descriptions' => $descriptions, 'start_date' => $start_date, 'end_date' => $end_date, 'account_no' => $this -> account_id);

			$this -> surveys_model -> update($survey_id, $surveys);

			redirect('surveys');
		}

	}

	/*
	 * surveys categories ends
	 *
	 */

	public function survey_status() {
		$state = $this -> uri -> segment(3);
		$id = $this -> uri -> segment(4);
		if ($state == 'active') {
			$state = 1;
		} elseif ($state = 'deactive') {
			$state = 0;
		}
		$status = array('status' => $state);
		//first. deactivate if any active survey

		$activated = $this -> surveys_model -> read(array('status' => 1));
		if ($activated) {
			$dstatus = array('status' => 0);
			foreach ($activated as $key => $value) {
				$this -> surveys_model -> update($value['survey_id'], $dstatus);
			}
		}
		//then activate this
		if ($this -> surveys_model -> update($id, $status)) {
			redirect('surveys');
		}

	}

	public function view_responce() {

		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('responder_mobile', 'survey_title', 'responce_date', 'location_name', 'responce_status');
		$where = array('responder_id' => $this -> uri -> segment(4));

		$data = $this -> surveys_model -> read_responces($where);
		foreach ($data as $data) {
			$this -> data['info'] = $data;
		}
		$where_s = array('survey_no' => $data['survey_no']);
		$this -> data['questions'] = $this -> questionaires_model -> read($where_s);
		$where_me = array('survey_no' => $data['survey_no'], 'responder' => $data['responder_mobile']);
		$replies = $this -> surveys_model -> read_my_responces($where_me);
		$this -> data['replies'] = $replies;

		$this -> data['stc_active'] = 'class="active"';
		$this -> data['add_btn'] = 'add_new_stock';

		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/view_responce', $this -> data);
		$this -> load -> view('template/footer');

	}

	public function delete($survey) {
		$this -> surveys_model -> delete($survey);
		redirect('surveys');

	}

	/*
	 * survey sections
	 *
	 */
	public function create_section($survey) {
		if (!in_array(3, $this -> user_permissions)) {
			redirect('login/logout');

		}
		if (trim($this -> input -> post('section_title')) == FALSE) {
			$this -> session -> set_userdata('empty_data', TRUE);

		} else {
			extract($_POST);

			$sections = array('section_title' => ucwords($section_title), 'survey_no' => $survey, 'account_no' => $this -> account_id, 'qn_numbering' => $qn_numbering);

			$this -> survey_sections_model -> save($sections);

			$this -> session -> set_userdata('section_saved', TRUE);
		}
		redirect('surveys/view/' . $survey);

	}

	public function delete_sections($survey, $section) {
		$this -> survey_sections_model -> delete($section);
		redirect('surveys/view/' . $survey);

	}

	public function update_section($survey, $section) {
		if (!in_array(3, $this -> user_permissions)) {
			redirect('login/logout');

		}
		if (trim($this -> input -> post('section_title')) == FALSE) {

			$this -> data['row_fields'] = $this -> data['tb_headers'] = array('section_title', 'qn_numbering');

			$where = array('survey_id' => $survey);

			$surveys = $this -> surveys_model -> read($where);

			if ($surveys) {

				foreach ($surveys as $surveys) {

				}

				$this -> data['survey'] = $surveys;
				$where = array('survey_no' => $surveys['survey_id']);

				//read survey sections

				$sections = $this -> survey_sections_model -> read($where);
				if ($sections) {

					$this -> data['tb_data'] = $sections;

				} else {
					$this -> data['tb_data'] = FALSE;
				}
				//section to update
				if ($section) {
					$section = $this -> survey_sections_model -> read(array('section_id' => $section));
					if ($section) {
						$this -> data['info'] = $section[0];
					} else {
						$this -> data['info'] = FALSE;
					}
				} else {
					$this -> data['info'] = FALSE;
				}

				$this -> load -> view('template/header', $this -> data);
				$this -> load -> view('template/content/surveys_view', $this -> data);
				$this -> load -> view('template/table_helper');
				$this -> load -> view('template/footer');
			} else {
				redirect('surveys/view/' . $survey);

			}
		} else {

			extract($_POST);

			$sections = array('section_title' => ucwords($section_title), 'qn_numbering' => $qn_numbering);

			$this -> survey_sections_model -> update($section_id, $sections);

			$this -> session -> set_userdata('section_saved', TRUE);
			redirect('surveys/view/' . $survey);
		}

	}

	public function view_section($survey, $section) {

		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('question_text', 'question_type', 'question_rank');
		$where = array('survey_id' => $survey);

		$surveys = $this -> surveys_model -> read($where);

		if ($surveys) {

			foreach ($surveys as $surveys) {
			}

			$this -> data['survey'] = $surveys;
			$where = array('questionaires.survey_no' => $surveys['survey_id'], 'survey_section' => $section, 'language' => 'en');

			//get questions in a survey
			if ($surveys['no_questions'] > 0) {

				$this -> data['pagenate'] = $this -> pagination($surveys['no_questions'], $this -> limit, $this -> page);
			} else {
				$this -> data['pagenate'] = FALSE;
			}
			$questions = $this -> questionaires_model -> read_questions($where);

			if ($questions) {
				$this -> data['tb_data'] = $questions;
			} else {
				$this -> data['tb_data'] = FALSE;
			}

			//questionaires Options

			$this -> data['edit'] = 'questionaires/update_question';
			$this -> data['view'] = 'questionaires/view';
			$this -> data['delete'] = 'questionaires/delete';
			$this -> data['create'] = 'questionaires/create/' . $survey . '/' . $section;
			$this -> data['back'] = 'surveys/view/' . $survey;
			$this -> data['reordering'] = 'questionaires/questions/' . $survey;

			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/questionaires_table', $this -> data);
			$this -> load -> view('template/table_helper');
			$this -> load -> view('template/footer');

		} else {
			redirect('surveys');

		}

	}

	public function save_qn_sorting() {
		file_put_contents("kwincho.txt", print_r($_POST, true), FILE_APPEND);

	}

}
