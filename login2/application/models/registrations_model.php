<?php
class Registrations_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	
	public function getRegistrations($cond)
	{
		$query = $this->db->get_where('registrations', $cond);
		return $query->result_array();
	}
	
	public function add($data)
	{
		return $this->db->insert('registrations', $data);
	}
	
	public function update($data,$cond)
	{
		$this->db->where($cond);
		return $this->db->update('registrations', $data);
	}
	
	public function delete($cond)
	{
		return $this->db->delete('registrations', $cond);
	}

}
