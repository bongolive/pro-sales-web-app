<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
 
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><?php $pagetitle ?></h3>                                    
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                               <?php
	echo form_open('settings/update', array('class' => 'form-horizontal'));
?>
 <fieldset>  
 <?php
//success msg display
if ($this -> session -> userdata('saved')) {
	echo '<div class="alert alert-success" style="text-align:center;">Your Settings Successfuly saved.</div>';
	$this -> session -> unset_userdata('saved');

}
  ?>
                                    <table id="example1" class="table table-bordered table-striped">
                                       <!--<thead>
                                            <tr><th>#</th>
                                            	  <?php foreach($tb_headers as $header){?>

											<th><?php echo lang($header); ?></th>

                                        <?php } ?>
                                            	 
                                            </tr>
                                        </thead>-->
                                        <tbody>
 <tbody>
					       <?php  $nn = count($info[0]); ?>
				  						 
				  					 <?php
				  					 
				  				if($info){	 
				  					 foreach($info[0] as $key => $value){
				  					 if($key=="account_no" OR $key=="settings_id" or  $key=="last_update_time") {
				  					 	
				  					 }elseif($key=="active_survey"){ ?>
				  			 
				  					 <tr><td > <?php echo lang($key); ?></td> <td> 
				  					 	<?php
				  					 	$stype =' class="form-control"';
				  					 	 echo form_dropdown($key,$surveys,$value,$stype) ?> 
				  					 	 </td></tr>
				  					 
									<?php 
									}elseif($key=="default_language"){ ?>
				  			 <!-- default language -->
				  					 <tr><td > <?php echo lang($key); ?></td> <td> 
				  					 	<?php
				  					 	$stype =' class="form-control"';
										$language = array('en'=>'English','sw'=>'Kiswahili');
				  					 	 echo form_dropdown($key,$language,$value,$stype) ?> 
				  					 	 </td></tr>
				  					 
									<?php 
									  
									}elseif($key=="cap_size"){ ?>
										
									<tr><td > <?php echo lang($key); ?></td> <td>
										<div class="input-append"> 
											<input type="number"  name="<?php echo $key; ?>"  class="form-control" value="<?php echo $value; ?>"  placeholder='Time' />
											 
										 </td></tr>	
				  					 <?php 
									}elseif($key=="reminders_per_survey"){ ?>
										
									<tr><td > <?php echo lang($key); ?></td> <td>
										<div class="input-append"> 
											<input type="number"  name="<?php echo $key; ?>"  class="form-control" value="<?php echo $value; ?>"  placeholder='Time' />
											 
										 </td></tr>	
				  					 <?php 
									}elseif($key=="server_url"){ ?>
										
									<tr><td > <?php echo lang($key); ?></td> <td>
										<div class="input-append"> 
											<input type="text"  name="<?php echo $key; ?>"  class="form-control" value="<?php echo $value; ?>"  placeholder='server url' />
											 
										 </td></tr>	
				  					 <?php 
									 
										}elseif($key=="skip_value"){ ?>
										
									<tr><td > <?php echo lang($key); ?></td> <td>
										<div class="input-append"> 
											<input type="text"  name="<?php echo $key; ?>"  class="form-control" value="<?php echo $value; ?>"  placeholder='skip default value' />
											   
										 </td></tr>	
				  					 <?php }elseif($key=="sender_number"){ ?>
				  				<tr><td > <?php echo $key; ?></td> <td>	<input type="number"  name="<?php echo $key; ?>"  class="form-control" value="<?php echo $value; ?>"  placeholder='Number' /></td></tr>
				  					 	<?php }else { ?>	
				  					   <tr><td > <?php echo lang($key); ?></td> <td>   <input type="checkbox"  name="<?php echo $key; ?>" <?php
										if ($value == 1) { echo "checked";
										}
  ?> /> </td></tr> 
				  					 
				  						 <?php
										}
										}
										}else{ echo ' <td> No data available</td>'; }
				  						 ?> 
						    	</tbody>
                                        </tbody>
                                
                                    </table>
                                      <div class="form-actions">
 <input type="reset" class="btn btn-large" value="Reset">
<button type="submit"  class="btn btn-info btn-large"><i class="icon-pencil"></i>Save</button>
 </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div> 
                </section><!-- /.content --> 
