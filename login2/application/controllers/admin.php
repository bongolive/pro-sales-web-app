<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends User_Controller {
	public $session_userid = '';
	public function __construct()
	{
		parent::__construct();
		$this->session_userid = $this->session->userdata('userid');
		if($this->session->userdata('user_type') != "Administrator"){
			redirect("/track/");
		}
	}
	//=========================================================================================================================
	//Track controllers
	public function index()
	{
		$this->load->model('devices_model');
		$this->load->model('user_model');
		$fields['users'] = $this->user_model->get_userlist();
		$fields['devices'] = $this->devices_model->get_devicelistbyuserid();
        $header['tr_active'] = 'class="active"';
        $fields['tb_name'] = 'track_tb_name';
        $this->load->view('admin/header',$header);
        $this->load->view('admin/track',$fields);
        $this->load->view('admin/footer');
	}
	
	public function lastlocation()
	{
		$this->load->model('devices_model');
		$devices = $this->devices_model->get_lastLocation();
		if(count($devices)>0){
			$xml = "<locations>";
			foreach($devices as $row){
				$xml .= "<location>";
				$xml .= "<phoneid>".$row["phoneid"]."</phoneid>";
				$xml .= "<imei>".$row["imei"]."</imei>";
				$xml .= "<devicename>".$row["device_name"]."</devicename>";
				$xml .= "<mobileno>".$row["mobile_no"]."</mobileno>";
				$xml .= "<holdername>".$row["holder_name"]."</holdername>";
				$xml .= "<latitude>".$row["latitude"]."</latitude>";
				$xml .= "<longitude>".$row["longitude"]."</longitude>";
				$xml .= "<localdate>".$row["localdate"]."</localdate>";
				$xml .= "</location>";
			}
			$xml .= "</locations>";
			echo $xml;
		}else{
			echo "ERROR. !";
		}
	}
	
	public function showtrack()
	{
		$this->load->model('devices_model');
		$tracks_count = 0;
		$data = "";
		$start = "NO";
		$user_dtl = "";
		$start = urldecode($this->uri->segment(4));
        $sdate = urldecode($this->uri->segment(5));
        $edate = urldecode($this->uri->segment(6));
        $thc = array('-', 'A');
        $replace = array('/',':');
        $sdate = str_replace($thc,$replace,$sdate);
        $edate = str_replace($thc,$replace,$edate);
        //echo $start.$sdate.$edate;
		if(($start != 'start') && $sdate && $edate){
			if($this->input->post('start') == "start"){
				$start = "YES";
			}else{
				$date = date_create($sdate);
				$sdate = date_format($date, 'Y-m-d H:i:s');
				$date = date_create($edate);
				$edate = date_format($date, 'Y-m-d H:i:s');
				$imei = $start;
				$res = $this->devices_model->get_deviceLocationByImei($imei,$sdate,$edate);
				if (count($res)==0)
				{
                    $start = "NO";
				}else{
					$tracks_count = count($res);
					if($tracks_count > 0){
						$i = 0;
						foreach($res as $r) {
							$data .= "[ '".$r['date']."' , ".$r['latitude'].", ".$r['longitude']." ] ".(($tracks_count-1 == $i ) ? "" : ", " );
							$i += 1;
						}
						$r  = $this->devices_model->get_devicelistbyImei($imei);
						if(count($r) >0){
							$user_dtl .= "'".$r["phone_imei"]."', '".$r["device_name"]."', '".$r["mobile_no"]."', '".$r["holder_name"]."' ";
						}
					}else{
						$tracks_count = 0;
					}
				}
			}
		}
        $content['start'] = $start;
		$content['user_dtl'] = $user_dtl;
		$content['data'] = $data;
		$content['user_dtl'] = $user_dtl;
		//Load shoetrack view here
		$this->load->view('admin/showtrack',$content);
	}
    
    public function showdevices(){   
        $this->load->view('admin/show_devices');
    }
	//=========================================================================================================================
	//End of track controller
	
	
	
	
	//========================================================================================================================
	//Devices controller start
	public function devices()
	{
		$this->load->model('devices_model');
		$this->load->model('user_model');
		$fields['users'] = $this->user_model->get_userlist();
		$fields['tb_data'] = $this->devices_model->get_devicelistbyuserid();
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js','custom/custom-datatable.js');
        $fields['tb_name'] = 'device_tb_name';
        // Put the fields from the database in this array
        $fields['row_fields'] = array('client_name','phone_imei','device_name','checking_interval','mobile_no','holder_name');
        $fields['tb_headers'] = array('client_name','device_imei','device_name','track_intv','mobile_nmber','holder_name','action');
        $header['dv_active'] = 'class="active"';
		$fields['add_btn'] = 'add_new_device';
        $this->load->view('admin/header',$header);
        $this->load->view('admin/table',$fields);
        $this->load->view('admin/footer',$ft_data);
	}
	
	public function getUserDevices()
	{
		$userid = $this->input->post('id');
		$this->load->model('devices_model');
		$devices = $this->devices_model->get_devicelistbyuserid($userid);
		echo "<option value=''>Please select Device</option>";
		if(count($devices)>0)
		{
			foreach($devices as $device)
			{
				echo "<option value='".$device['phone_imei']."'>".$device['device_name']."</option>";
			}
		}
	}
	
	public function getUserDevicesId()
	{
		$userid = $this->input->post('id');
		$this->load->model('devices_model');
		$devices = $this->devices_model->get_devicelistbyuserid($userid);
		echo "<option value=''>Please select Device</option>";
		if(count($devices)>0)
		{
			foreach($devices as $device)
			{
				echo "<option value='".$device['phoneid']."'>".$device['device_name']."</option>";
			}
		}
	}
	
	public function getUserDevicesCheckbox()
	{
		$userid = $this->input->post('id');
		$this->load->model('devices_model');
		$devices = $this->devices_model->get_devicelistbyuserid($userid);
		if(count($devices)>0)
		{
			foreach($devices as $device)
			{
				echo '<label class="checkbox custom-checkbox">'; 
				echo "<input id='"."cb".$device['phoneid']."' type='checkbox' name='cb_devices[]' value='".$device['phoneid']."' checked='checked' />".$device['device_name'];
                echo "</label>";
			}
		}
	}
	
	
	public function getUserDevicesCheckboxImei()
	{
		$userid = $this->input->post('id');
		$this->load->model('devices_model');
		$devices = $this->devices_model->get_devicelistbyuserid($userid);
		if(count($devices)>0)
		{
			foreach($devices as $device)
			{
				echo '<label class="checkbox custom-checkbox">'; 
				echo "<input id='"."cb".$device['phoneid']."' type='checkbox' name='cb_devices[]' value='".$device['phone_imei']."' checked='checked' />".$device['device_name'];
                echo "</label>";
			}
		}
	}
	
	

	public function addDevice()
	{		
			$this->load->model('devices_model');
			$userid = $this->input->post('user');
			$imei = $this->input->post('device_imei');
			$device_name = $this->input->post('device_name');
			$mobile = formatMobileNumber($this->input->post('mobile'));
			$checking_interval = $this->input->post('track_intv');
			$holder_name = $this->input->post('holder_name');
			$email_report = $this->input->post('email_reports');
			$devicedata = array(
				'phone_imei' => $imei,
				'device_name' => $device_name,
				'checking_interval' => $checking_interval,
				'mobile_no' => $mobile,
				'holder_name' => $holder_name,
				'email_report' => $email_report
			);
			//Checking if user has this device on phones
			$user_divice  = $this->devices_model->get_deviceFromPhonesUsers($userid ,$imei);
			$phonesdata		= $this->devices_model->get_devicelistbyImei($imei);
			//Now we check if device exist on
			if (count($user_divice) == 0 && count($phonesdata) == 0)
			{
				//No Divice in all tables so we insert it
				$this->devices_model->add_device($devicedata);
				$phonesdata		= $this->devices_model->get_devicelistbyImei($imei);
				//generating array for 
				$deviceusersdata = array(
					'userid' => $userid ,
					'phoneid' => $phonesdata['phoneid'],
					'imei' => $imei
				);
				//No Divice in all tables so we insert it
				$this->devices_model->add_device_users($deviceusersdata);
			}else{
				if(count($user_divice)==0){
					$phonesdata		= $this->devices_model->get_devicelistbyImei($imei);
					//generating array for 
					$deviceusersdata = array(
						'userid' => $userid ,
						'phoneid' => $phonesdata['phoneid'],
						'imei' => $imei
					);
					$this->devices_model->add_device_users($deviceusersdata);
				}
				//updating record since it existing
				$this->devices_model->update_diviceByImei($devicedata,$imei);
			}
			redirect('admin/devices');
	}

	public function editDevice($id)
	{
		$this->load->model('devices_model');
		$data['devices'] = $this->devices_model->get_devicelistbyid($id);
		$this->load->view('admin/edit_device',$data);
	}
	
	public function updateDevice()
	{
			$this->load->model('devices_model');
			//$userid = $this->input->post('user');
			$imei = $this->input->post('device_imei');
			$device_name = $this->input->post('device_name');
			$mobile = formatMobileNumber($this->input->post('mobile'));
			$checking_interval = $this->input->post('track_intv');
			$holder_name = $this->input->post('holder_name');
			$email_report = $this->input->post('email_reports');
			$phoneid = $this->input->post('deviceid');
			$devicedata = array(
				'phone_imei' => $imei,
				'device_name' => $device_name,
				'checking_interval' => $checking_interval,
				'mobile_no' => $mobile,
				'holder_name' => $holder_name,
				'email_report' => $email_report
			);
			$row = $this->devices_model->get_devicelistbyImei($imei);
			$int = $row['checking_interval'];
			$id = $row['phoneid'];
			$regid = $row['regid'];
			//updating record since it existing
			$this->devices_model->update($devicedata,$phoneid);
			if($int != $checking_interval){
					$rownew = $this->devices_model->get_devicelistbyImei($imei);
					$apiKey = "AIzaSyAfOtZpNks_9J9jZfGU0V3OyDZoeseyfBY";
					$registrationIDs[] = $rownew['regid']; 
					// Message to be sent
					$message = $rownew['checking_interval'];
					// Set POST variables
					$url = 'https://android.googleapis.com/gcm/send';
					$fields = array(
						'registration_ids'  => $registrationIDs,
						'data'              => array( "message" => $message),
						);
					//print_r(json_encode($fields));
					$headers = array( 
							'Authorization: key=' . $apiKey,
							'Content-Type: application/json'
						);
					$ch = curl_init();
					curl_setopt( $ch, CURLOPT_URL, $url );
					curl_setopt( $ch, CURLOPT_POST, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
					curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0); 
					curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
					$result = curl_exec($ch);
					if(curl_errno($ch))
					{ 
						echo 'Curl error: ' . curl_error($ch); 
					} 
					curl_close($ch);
				}
			redirect('admin/devices');
	}
	
	public function deleteDevice()
	{
		$deviceid = $this->uri->segment(4);
		$userid =  $this->uri->segment(5);
		$this->load->model('devices_model');
		$result = 	$this->devices_model->delete($deviceid,$userid);
		redirect('admin/devices');
	}
	//========================================================================================================================
	//Device controller end
	
	
	
	
	//========================================================================================================================
	//Start of Task controller
	public function tasks()
	{
		$this->load->model('tasks_model');
		$this->load->model('devices_model');
		$this->load->model('user_model');
		$fields['users'] = $this->user_model->get_userlist();
		$fields['devices'] = $this->devices_model->get_devicelistbyuserid();
		$fields['tb_data'] = $this->tasks_model->get_tasksByUserId();
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js','plugins/bootstrapModal/bootstrap-modalmanager.js','plugins/bootstrapModal/bootstrap-modal.js','custom/custom-datatable.js');
        $fields['tb_name'] = 'tasks_tb_name';
        $fields['row_fields'] = array('device_name','subject','task','status','comments','datetime');
        $fields['tb_headers'] = array('device_name','subject','task','status','comments','date','action');
        $header['ts_active'] = 'class="active"';
        $fields['add_btn'] = 'add_task';
		$fields['download_btn'] = 'download_task';
		$ft_data['add_btn'] = 'add_task';
        $ft_data['download_btn'] = 'download_task';
        $this->load->view('admin/header',$header);
        $this->load->view('admin/table',$fields);
        $this->load->view('admin/footer',$ft_data);
	}
    
    public function editTask($taskid)
	{
		$this->load->model('tasks_model');
		$this->load->model('devices_model');
		$fields['devices'] = $this->devices_model->get_devicelistbyuserid();
		$fields['tasks'] = $this->tasks_model->get_taskById($taskid);
        $this->load->view('admin/edit_task', $fields);
    }
	
	public function addTask()
	{
		$this->load->model('tasks_model');
		$this->load->model('devices_model');
		$data = array(
			'subject' => $this->input->post('subject'),
			'task' => $this->input->post('task'),
			'datetime' => date("Y-m-d H:i:s"),
			'phoneid' => $this->input->post('device_name'),
			'status' => $this->input->post('status'),
			'comments' => $this->input->post('comments')
		);
		$tsResult = $this->tasks_model->add_task($data);
		if($tsResult)
		{
			$device = $this->devices_model->get_devicelistbyid($this->input->post('device_name'));
			if($device['phone_imei'] !="")
			{
				$registrationIDs[] = $device['regid'];
				$apiKey = "AIzaSyCg5BsKkoHHHUxkTnP_fNqVMuOjeXVg0SI";
				$message = "You got a new Task";
				$fields = array(
					'registration_ids'  => $registrationIDs,
					'data'              => array( "message" => $message),
				);
					//print_r(json_encode($fields));
				$headers = array( 
					'Authorization: key=' . $apiKey,
					'Content-Type: application/json'
				);
				$response = sentTask($headers, $fields);
			}	
		}
		redirect('admin/tasks');
	}
	
	
	public function updateTask()
	{
		$this->load->model('tasks_model');
		$this->load->model('devices_model');
		$data = array(
			'subject' => $this->input->post('subject'),
			'task' => $this->input->post('task'),
			'datetime' => date("Y-m-d H:i:s"),
			'phoneid' => $this->input->post('device_name'),
			'status' => $this->input->post('status'),
			'comments' => $this->input->post('comments')
		);
		$taskid = $this->input->post('taskid');
		$tsResult = $this->tasks_model->update_task($data,$taskid);
		
		if($tsResult)
		{
			$device = $this->devices_model->get_devicelistbyid($this->input->post('device_name'));
			if($device['phone_imei'] !="")
			{
				$registrationIDs[] = $device['regid'];
				$apiKey = "AIzaSyCg5BsKkoHHHUxkTnP_fNqVMuOjeXVg0SI";
				$message = "You got an update of task";
				
				$fields = array(
					'registration_ids'  => $registrationIDs,
					'data'              => array( "message" => $message),
				);
					//print_r(json_encode($fields));
				$headers = array( 
					'Authorization: key=' . $apiKey,
					'Content-Type: application/json'
				);
				$response = sentTask($headers, $fields);
			}	
		}
		redirect('admin/tasks');
	}
	
	
	public function deleteTask($taskid)
	{
		$this->load->model('tasks_model');
		$result = 	$this->tasks_model->delete($taskid);
		if($result === TRUE)
		{
			redirect('admin/tasks');
		}
		else
		{
			redirect('admin/tasks');
		}
	}
	
	
	public function exportTask()
	{
		$this->load->model('tasks_model');
		$this->load->model('devices_model');
		$result = "";
		$query_str = "";
		$taskposted = $this->input->post('txttask');
		$devicesposted = $this->input->post('cb_devices');
		$subjectposted = $this->input->post('txtsubject');
		$userid = $this->input->post('users');

		if (count($devicesposted) > 0){

			//$_POST['txt_enddate'];
			$date = date_create($this->input->post('startdate'));
			$sdate = date_format($date, 'Y-m-d H:i:s');
			$date = date_create($this->input->post('enddate'));
			$edate = date_format($date, 'Y-m-d H:i:s');
			$sub = "t.subject like '%".$subjectposted."%'";
			if(strlen($subjectposted) <= 0){
				$sub = "1=1";
			}else{
				$query_str .= "?txtsubject=".$subjectposted;
			}
	
			$task = "t.task like '%$taskposted%'";
			if(strlen($taskposted) <= 0){
				$task = "1=1";
			}else{
				$query_str .= (($query_str == "") ? "?" : "&")."txttask=".$taskposted;
			}
			$phoneid = "1=1";
			if($devicesposted != ""){
				if(count($devicesposted) >0){
					$phoneid = "(";
					for($i = 0; $i < count($devicesposted); $i++){
						$phoneid .= "t.phoneid = ".$devicesposted[$i]." ".(($i < count($devicesposted)-1) ? " OR " : "");
						$query_str .= (($query_str == "") ? "?" : "&")."cb_devices[]=".$devicesposted[$i];
					}
					$phoneid .= ")";
				}
			}
			
			$stat = "1=1";
			if($this->input->post('statussearch') != -1 && $this->input->post('statussearch') != ''){
				$stat = "t.status = '".$this->input->post('statussearch')."'";
				$query_str .= (($query_str == "") ? "?" : "&")."statussearch=".$this->input->post('statussearch');
			}else{
		
			}
			$date = "t.datetime between '$sdate' and '$edate'";
			if(strlen($this->input->post('startdate')) <= 0 || strlen($this->input->post('enddate')) <= 0)
			{
				$date = "1=1";
			}else{
				$query_str .= (($query_str == "") ? "?" : "&")."startdate=".$this->input->post('startdate');
				$query_str .= (($query_str == "") ? "?" : "&")."enddate=".$this->input->post('enddate');
			}
			
			$result = $this->tasks_model->export($userid,$sub,$task,$phoneid,$stat,$date);
			if(count($result) > 0){
				if($this->input->post("btn_export") == "1"){
			
					$this->load->library('excel');
			
					$this->excel->getProperties()->setCreator("Foot Print Admin")
										 ->setLastModifiedBy("Foot Print Admin")
										 ->setTitle("Exported Tasks from Foot Print Site")
										 ->setSubject("Exported Tasks from Foot Print Site")
										 ->setDescription("Exported Tasks from Foot Print Site by User '".$this->session->userdata('username')."'")
										 ->setKeywords("Exported Tasks from Foot Print Site")
										 ->setCategory("Exported Tasks from Foot Print Site");			
		
					$this->excel->setActiveSheetIndex(0)
						->setCellValue('A1', 'Device Name')
						->setCellValue('B1', 'Subject')
						->setCellValue('C1', 'Task')
						->setCellValue('D1', 'Status')
						->setCellValue('E1', 'Date Time')
						->setCellValue('F1', 'Comments');
			
					$this->excel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
	
					$index = 2;
					foreach($result as $row)
					{
						$this->excel->setActiveSheetIndex(0)
							->setCellValue('A'.$index, $row["device_name"])
							->setCellValue('B'.$index, $row["subject"])
							->setCellValue('C'.$index, $row["task"])
							->setCellValue('D'.$index, $row["status"])
							->setCellValue('E'.$index, $row["datetime"])
							->setCellValue('F'.$index, $row["comments"]);
							
						$index++;
					}
			
					// Rename worksheet
					$this->excel->getActiveSheet()->setTitle('Tasks');
			
					// Set active sheet index to the first sheet, so Excel opens this as the first sheet
					$this->excel->setActiveSheetIndex(0);
			
					// Redirect output to a client's web browser (Excel2007)
					header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					header('Content-Disposition: attachment;filename="tasks_by_user-'.$this->session->userdata('username').'_'.date('m-d-Y_h-i-s-a', time()).'.xlsx"');
					header('Cache-Control: max-age=0');
			

					$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
					$objWriter->save('php://output');
					exit;	}
			}
		}
        //exit;
		redirect('admin/tasks');
	}
	
	
	//========================================================================================================================
	//end of task controller
	
	
	
	//=========================================================================================================================
	//Start of report controller
	public function reports()
	{
		$this->load->model('reports_model');
        $this->load->model('devices_model');
		$this->load->model('user_model');
		$fields['devices'] = $this->devices_model->get_devicelistbyuserid();
		$fields['users'] = $this->user_model->get_userlist();
		$fields['tb_data'] = $this->reports_model->get_reportByUserId();
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js','custom/custom-datatable.js');
        $fields['tb_name'] = 'reports_tb_name';
        $fields['row_fields'] = array('device_name','subject','report','datetime','comments');
        $fields['tb_headers'] = array('device_name','subject','report','date','comments','action');
        $header['rp_active'] = 'class="active"';
        $fields['download_btn'] = 'download_report';
		$ft_data['add_btn'] = 'add_task';
        $ft_data['download_btn'] = 'download_task';
        $this->load->view('admin/header',$header);
        $this->load->view('admin/table',$fields);
        $this->load->view('admin/footer',$ft_data);
	}
    
    public function editReport($reportid){
        $this->load->model('reports_model');
		$this->load->model('devices_model');
		$fields['reports'] = $this->reports_model->get_reportById($reportid);
       $this->load->view('admin/edit_report',$fields);
    }
	
	public function updateReport()
	{
		$this->load->model('reports_model');
		$data = array(
			'comments' => $this->input->post('comments')
		);
		$reportid = $this->input->post('reportid');
		$this->reports_model->update_report($data,$reportid);
		redirect('admin/reports');
	}
	
	public function deleteReport($reportid)
	{
		$this->load->model('reports_model');
		$result = 	$this->reports_model->delete($reportid);
		redirect('admin/reports');
	}
	
	
	public function exportReport()
	{
		$this->load->model('reports_model');
        $this->load->model('devices_model');		
		$error = "";
		if($this->input->post("btn_export") !=""){
			if(count($this->input->post("cb_devices")) <= 0){
				$error .= "No Device Selected.";
			}else if($this->input->post("startdate") == ""){
				$error .= "<br />Start Date is Empty.";
			}else if($this->input->post("enddate") == ""){
				$error .= "<br />End Date is Empty.";
			}else{
				$date = date_create($this->input->post('startdate'));
				$s_date = date_format($date, 'Y-m-d H:i:s');
				$date = date_create($this->input->post('enddate'));
				$e_date = date_format($date, 'Y-m-d H:i:s');
		
				$imei = $this->input->post("cb_devices");
		
				$imei_query = "";
				for($i = 0; $i < count($imei); $i++){
					$imei_query .= "phoneid	 = '".$imei[$i]."' ".(($i < count($imei)-1) ? " OR " : "");
				}
			
				$this->load->library('excel');
	
				// Set document properties
				$this->excel->getProperties()->setCreator("Foot Print Admin")
									 ->setLastModifiedBy("Foot Print Admin")
									 ->setTitle("Exported Reports from Foot Print Site")
									 ->setSubject("Exported Reports from Foot Print Site")
									 ->setDescription("Exported Reports from Foot Print Site by the User '".$this->session->userdata('username')."'")
									 ->setKeywords("Exported Reports from Foot Print Site")
									 ->setCategory("Exported Reports from Foot Print Site");			
		
				// Add some data
				$this->excel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Device Name')
					->setCellValue('B1', 'Subject')
					->setCellValue('C1', 'Report')
					->setCellValue('D1', 'Date')
					->setCellValue('E1', 'Comments')
					->setCellValue('F1', 'Latitude')
					->setCellValue('G1', 'Longitude');
				$this->excel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
		
				$result = $this->reports_model->export($s_date,$e_date,$imei_query);
				
				$index = 2;
				foreach($result  as $row)
				{
					$this->excel->setActiveSheetIndex(0)
						->setCellValue('A'.$index, $row["device_name"])
						->setCellValue('B'.$index, $row["subject"])
						->setCellValue('C'.$index, $row["report"])
						->setCellValue('D'.$index, $row["datetime"])
						->setCellValue('E'.$index, $row["comments"])
						->setCellValue('F'.$index, $row["latitude"])
						->setCellValue('G'.$index, $row["longitude"]);
					$index++;
				}
		
				// Rename worksheet
				$this->excel->getActiveSheet()->setTitle('Reports');

				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$this->excel->setActiveSheetIndex(0);
		
				// Redirect output to a client's web browser (Excel2007)
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="report_by_user-'.$this->session->userdata('username').'.xlsx"');
				header('Cache-Control: max-age=0');
		
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
				$objWriter->save('php://output');
				exit;
			}
		}
		redirect('admin/reports');
		//exit;
	}
	
	public function showmap()
	{
		$data['name'] = urldecode($this->uri->segment(4));
		$data['lat'] = $this->uri->segment(5);
		$data['lon'] = $this->uri->segment(6);
		$this->load->view('template/content/showmap',$data);
	}

	//=========================================================================================================================
	//End of report controller
	
	
	
	//==========================================================================================================================
	//Start of Boundaries Controller
	public function boundaries()
	{
		$this->load->model('boundry_model');
		$this->load->model('devices_model');
		$this->load->model('user_model');
		$fields['users'] = $this->user_model->get_userlist();
		$fields['devices'] = $this->devices_model->get_devicelistbyuserid();
		$fields['tb_data'] = $this->boundry_model->get_boundryByUserId();
	    $ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js','custom/custom-datatable.js');
        $fields['tb_name'] = 'boundaries_tb_name';
        $fields['row_fields'] = array('client_name','alertname','devices','valid_from','valid_to');
        $fields['tb_headers'] = array('client_name','alert_name','devices','valid_from','valid_to','action');
        $header['bd_active'] = 'class="active"';
        $fields['add_btn'] = 'add_boundary';
		$ft_data['add_btn'] = 'add_boundary';
        $this->load->view('admin/header',$header);
        $this->load->view('admin/table',$fields);
        $this->load->view('admin/footer',$ft_data);
	}
	
	
	public function addBoundary()
	{		
		$this->load->model('boundry_model');
		$this->load->model('devices_model');
			$data = array(
                'userid' => $this->input->post('users'),
				'alertname' => $this->input->post('alert_name'),
				'centerlat' => $this->input->post('center_lat'),
				'centerlng' => $this->input->post('center_lng'),
				'radius' => $this->input->post('circle_radius'),
				'valid_from' => date("Y-m-d H:i:s",strtotime($this->input->post('valid_from'))),
				'valid_to' => date("Y-m-d H:i:s",strtotime($this->input->post('valid_to')))
			);
			$boundryid = $this->boundry_model->add_boundry($data);
			$devices = $this->input->post('cb_devices');
			for($i = 0; $i < count($devices); $i++ ){
				$data2 = array(
					'boundryid' => $boundryid,
					'imei' => $devices[$i],
					'laststatus' => '0'
				);
				$result = $this->boundry_model->add_boundry_alocation($data2);
				unset($data2);		
			}
			if($result === TRUE){
				echo 1;
			}else{
				echo 0;
			}
	}
	
	
	public function editBoundary()
	{
	    $this->load->model('boundry_model');
		$this->load->model('devices_model');
		$this->load->model('user_model');
		$data['users'] = $this->user_model->get_userlist();
        $data['do'] = $this->uri->segment(4);
        if ($this->uri->segment(5)){
            $data['boundryid'] = $this->uri->segment(5);
            $data['boundaries'] = $this->boundry_model->get_boundryById($this->uri->segment(5));
        }
        $data['devices'] = $this->devices_model->get_devicelistbyuserid();
		$boundryAllocation = $this->boundry_model->get_boundry_alocationByBoundryId($this->uri->segment(5));
		$boundryalocation_data = array();
		foreach($boundryAllocation as $r){
			array_push($boundryalocation_data, $r["imei"]);
		}
		$data['boundryalocation_data'] = $boundryalocation_data;
 		$this->load->view('admin/edit_boundary',$data);
	}
	
	
	public function updateBoundary()
	{		
		$this->load->model('boundry_model');
		$this->load->model('devices_model');
			$data = array(
				'alertname' => $this->input->post('alert_name'),
				'centerlat' => $this->input->post('center_lat'),
				'centerlng' => $this->input->post('center_lng'),
				'radius' => $this->input->post('circle_radius'),
				'valid_from' => date("Y-m-d H:i:s",strtotime($this->input->post('valid_from'))),
				'valid_to' => date("Y-m-d H:i:s",strtotime($this->input->post('valid_to')))
			);
			$devices = $this->input->post('cb_devices');
			$boundryid = $this->input->post('boundryid');
			$result = $this->boundry_model->update_boundry($data,$boundryid);
			//selecting all devices of this boundry
			$saved_ime = $this->boundry_model->get_boundry_alocationByBoundryId($boundryid);
			$saved_imeis = array();
				foreach($saved_ime as $r){
					array_push($saved_imeis, $r["imei"]);
				}
			$to_be_inserted = array_diff($devices, $saved_imeis);
			$to_be_inserted = array_values($to_be_inserted);
			$to_be_deleted = array_diff($saved_imeis, $devices);
			$to_be_deleted = array_values($to_be_deleted);			
			for($i = 0; $i < count($to_be_deleted); $i++ ){
				$data2 = array(
					'boundryid' => $boundryid,
					'imei' => $to_be_deleted[$i]
				);	
				$result = $this->boundry_model->delete_boundry_alocation($data2);
				unset($data2);	
			}			
			for($i = 0; $i < count($to_be_inserted); $i++ ){
				$data2 = array(
					'boundryid' => $boundryid,
					'imei' => $to_be_inserted[$i],
					'laststatus' => '0'
				);
				$result = $this->boundry_model->add_boundry_alocation($data2);
				unset($data2);		
			}
			if($result === TRUE){
				echo 1;
			}else{
				echo 0;
			}
	}
	
	public function deleteBoundary($boundryid)
	{
		$this->load->model('boundry_model');
		$this->load->model('devices_model');
		$result = 	$this->boundry_model->delete($boundryid);
		if($result === TRUE)
		{
			redirect('admin/boundaries');
		}
		else
		{
			redirect('admin/boundaries');
		}
	}
    
    public function showboundary(){
        $this->load->model('boundry_model');
		$this->load->model('devices_model');
        $data['lat'] = $this->uri->segment(4);
        $data['long'] = $this->uri->segment(5);
        $data['rad'] = $this->uri->segment(6);
          if ($data['lat'] != '' && $data['long'] != '' && $data['rad'] != ''){
               $this->load->view('admin/showboundary',$data);
          }
    }
	//===========================================================================================================================
	//end of boundries controllers
	
	
	
	//===========================================================================================================================
	//Start of user controllers
	public function users()
	{
        $this->lang->load('profile');
		$this->load->model('user_model');
		$fields['tb_data'] = $this->user_model->get_userlist();
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js','custom/custom-datatable.js');
        $fields['tb_name'] = 'user_tb_name';
        // Put the fields from the database in this array
        $fields['row_fields'] = array('company_name','name','username','mobile_no','email','user_type','valid_till');
        $fields['tb_headers'] = array('company_name','name','username','mobile_no','email','user_type','valid_till','action');
        $header['sp_active'] = 'class="active"';
		$fields['add_btn'] = 'add_new_user';
        $this->load->view('admin/header',$header);
        $this->load->view('admin/table',$fields);
        $this->load->view('admin/footer',$ft_data);
	}
	
	public function addUser()
	{
			$data = array(				
				'company_name' => $this->input->post('company_name'),
				'name' => $this->input->post('name'),
				'mobile_no' => $this->input->post('mobile'),
				'email' => $this->input->post('email'),
				'user_type' => $this->input->post('user_type'),
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password')),
				'valid_till' =>  date("Y-m-d H:i:s",strtotime($this->input->post('valid_till')))
			);
			$this->user_model->add_user($data);
			redirect("admin/users");
	}
	
	public function editUser($userid)
	{
		$this->load->model('user_model');
        $this->lang->load('profile');
		$data['users'] = $this->user_model->get_userlist($userid);
		$this->load->view('admin/edit_user',$data);
	}
	
	public function updateUser()
	{
		if($this->input->post('password') !=""){
			$password = md5($this->input->post('password'));
		}else{
			$password = $this->input->post('old_password');
		}
		
		$data = array(				
				'company_name' => $this->input->post('company_name'),
				'name' => $this->input->post('name'),
				'mobile_no' => $this->input->post('mobile'),
				'email' => $this->input->post('email'),
				'user_type' => $this->input->post('user_type'),
				'username' => $this->input->post('username'),
				'password' => $password,
				'valid_till' =>  date("Y-m-d H:i:s",strtotime($this->input->post('valid_till')))
			);
			$this->user_model->update_user($data,$this->input->post('userid'));
			redirect("admin/users");
	}

	public function deleteUser($userid)
	{
		$result = 	$this->user_model->delete($userid);
		redirect("admin/users");
	}
    
    public function logout(){
        $this->user_model->logout();
    	redirect('login');
    }
	
	public function userPermissions($userid)
	{
		$cond =array('is_deleted'=>'0');
		$cond2 =array('userid'=>$userid);
		$this->load->model('permission_model');
		$this->load->model('user_model');
		$fields['userid'] = $userid;
		$fields['user_permissions'] = $this->user_model->getUserPermissions($cond2);
		$fields['permissions'] = $this->permission_model->getPermissions($cond);
		$this->load->view('admin/user_permission',$fields);	
	}
	
	public function updateUserPermissions()
	{
		$this->load->model('user_model');
		$userid = $this->input->post('userid');
		$permissions = $this->input->post('cb_permission');
		$cond = array('userid'=>$userid);
		$this->user_model->deleteUserPermissions($cond);
		if(count($permissions)>0){
			foreach($permissions as $permission){
				$data = array('userid'=>$userid,'permission_name'=>$permission);
				$this->user_model->addUserPermission($data);
				unset($data);
			}
		}
		redirect('admin/users');
	}
	
	//end of user controllers
	//===========================================================================================================================
	
	
	public function permissions()
	{
		$cond =array('is_deleted'=>'0');
		$this->load->model('permission_model');
		$fields['tb_data'] = $this->permission_model->getPermissions($cond);
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js','custom/custom-datatable.js');
        $fields['tb_name'] = 'permission_tb_name';
        // Put the fields from the database in this array
        $fields['row_fields'] = array('permission_name','created','updated','is_deleted');
        $fields['tb_headers'] = array('permission_name','created','updated','is_deleted','action');
        $header['pm_active'] = 'class="active"';
		$fields['add_btn'] = 'add_new_permission';
        $this->load->view('admin/header',$header);
        $this->load->view('admin/table',$fields);
        $this->load->view('admin/footer',$ft_data);
	}
	
	public function addPermission()
	{
		$this->load->model('permission_model');
		$data = array('permission_name'=>$this->input->post('permission_name'),'created'=>date("Y-m-d H:i:s"),'updated'=>date("Y-m-d H:i:s"));
		$this->permission_model->add($data);
		redirect('admin/permissions');
	}
	
	
	public function editPermission($id)
	{
		$this->load->model('permission_model');
		$cond = array('id'=>$id);
		$data['permissions'] = $this->permission_model->getPermissions($cond);
		$this->load->view('admin/edit_permission',$data);
	}
	
	public function  updatePermission()
	{
		$this->load->model('permission_model');
		$cond = array('id'=>$this->input->post('permissionid'));
		$data = array('permission_name'=>$this->input->post('permission_name'),'updated'=>date("Y-m-d H:i:s"));
		$this->permission_model->update($data,$cond);
		redirect('admin/permissions');
	}
	
	public function deletePermission($id)
	{
		$this->load->model('permission_model');
		$cond = array('id'=>$id);
		$data = array('is_deleted'=>'1','updated'=>date("Y-m-d H:i:s"));
		$this->permission_model->update($data,$cond);
		redirect('admin/permissions');
	}
		
}