<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_Controller extends MY_Controller {

	public $session_userid = '';
	public $user_id;
	public $account_id;
	public $settings;
	public $page, $limit, $startpoint;

	function __construct() {

		parent::__construct();
 
		$this -> load -> model('user_model');

		// Login check

		$exception_uris = array('login', 'profile/logout');

		if (!$this -> session -> userdata('logged_in') == TRUE) {
			$this -> session -> set_userdata('login_error', 'Please, Loggin first');
			redirect('login');
		}

		$this -> user_id = $this -> session -> userdata('user_id');
		$this -> account_id = $this -> session -> userdata('account_id');

		$this -> load -> model('settings_model');
 
		if ($this -> session -> userdata['role'] != 'superuser') { 
			$settings = $this -> settings_model -> read(array('settings.account_id' => $this -> account_id));
			foreach ($settings as $settings) {
				$this -> fields['settings'] = $this -> settings = $settings;
			}
		
		 
	}

		$this -> page = (int)(!isset($_GET["page"]) ? 1 : $_GET["page"]);
		$this -> limit = 10;
		$this -> startpoint = ($this -> page * $this -> limit) - $this -> limit;

	}

	function pagination($total, $per_page = 10, $page = 1, $url = '?') {

		$adjacents = "2";

		$page = ($page == 0 ? 1 : $page);
		$start = ($page - 1) * $per_page;

		$prev = $page - 1;
		$next = $page + 1;

		$lastpage = ceil($total / $per_page);
		$lpm1 = $lastpage - 1;

		$pagination = "";
		if ($lastpage > 1) {
			$pagination .= "<ul class='pagination dataTables_paginate paging_bootstrap pagination'>";
			$pagination .= "<li class='details'>Page $page of $lastpage</li>";
			if ($lastpage < 7 + ($adjacents * 2)) {
				for ($counter = 1; $counter <= $lastpage; $counter++) {
					if ($counter == $page)
						$pagination .= "<li><a class='current'>$counter</a></li>";
					else
						$pagination .= "<li><a href='{$url}page=$counter'>$counter</a></li>";
				}
			} elseif ($lastpage > 5 + ($adjacents * 2)) {
				if ($page < 1 + ($adjacents * 2)) {
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
						if ($counter == $page)
							$pagination .= "<li><a class='current'>$counter</a></li>";
						else
							$pagination .= "<li><a href='{$url}page=$counter'>$counter</a></li>";
					}
					$pagination .= "<li class='dot'>...</li>";
					$pagination .= "<li><a href='{$url}page=$lpm1'>$lpm1</a></li>";
					$pagination .= "<li><a href='{$url}page=$lastpage'>$lastpage</a></li>";
				} elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
					$pagination .= "<li><a href='{$url}page=1'>1</a></li>";
					$pagination .= "<li><a href='{$url}page=2'>2</a></li>";
					$pagination .= "<li class='dot'>...</li>";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
						if ($counter == $page)
							$pagination .= "<li><a class='current'>$counter</a></li>";
						else
							$pagination .= "<li><a href='{$url}page=$counter'>$counter</a></li>";
					}
					$pagination .= "<li class='dot'>..</li>";
					$pagination .= "<li><a href='{$url}page=$lpm1'>$lpm1</a></li>";
					$pagination .= "<li><a href='{$url}page=$lastpage'>$lastpage</a></li>";
				} else {
					$pagination .= "<li><a href='{$url}page=1'>1</a></li>";
					$pagination .= "<li><a href='{$url}page=2'>2</a></li>";
					$pagination .= "<li class='dot'>..</li>";
					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
						if ($counter == $page)
							$pagination .= "<li><a class='current'>$counter</a></li>";
						else
							$pagination .= "<li><a href='{$url}page=$counter'>$counter</a></li>";
					}
				}
			}

			if ($page < $counter - 1) {
				$pagination .= "<li><a href='{$url}page=$next'>Next</a></li>";
				$pagination .= "<li><a href='{$url}page=$lastpage'>Last</a></li>";
			} else {
				$pagination .= "<li><a class='current'>Next</a></li>";
				$pagination .= "<li><a class='current'>Last</a></li>";
			}
			$pagination .= "</ul>\n";
		}

		return $pagination;
	}

/*
 * 		excel printing
 */

	public function excel_export($data,$controller) {

		// Original PHP code by Chirp Internet: www.chirp.com.au
		// Please acknowledge use of this code by including this header.

		$this -> cleanData($str);

		// filename for download
		$filename = $controller.'_as_on_'. date('Ymd') . ".xls";

		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-Type: application/vnd.ms-excel");

		$flag = false;
		if ($data) {
			foreach ($data as $row) {
				if (!$flag) {
					// display field/column names as first row
					echo implode("\t", array_keys($row)) . "\r\n";
					$flag = true;
				}
				array_walk($row, array($this, 'cleanData'));
				echo implode("\t", array_values($row)) . "\r\n";
			}
		}
		exit ;
	}

	public function cleanData(&$str) {
		$str = preg_replace("/\t/", "\\t", $str);
		$str = preg_replace("/\r?\n/", "\\n", $str);
		if (strstr($str, '"'))
			$str = '"' . str_replace('"', '""', $str) . '"';
	}

/*
 * excel print ends
 */
}

