<?php

/**
 * This file carries the Profile values for language in English
 */

$lang['change_profile_details'] = 'Change Profile Details';
$lang['name'] = 'Name';
$lang['username'] = 'Username';
$lang['email'] = 'E-mail';
$lang['mobile'] = 'Mobile';
$lang['company_name'] = 'Company Name';
$lang['change_password'] = 'Change Password';
$lang['new_password'] = 'New Password';
$lang['confirm_new_password'] = 'Confirm Password';
$lang['support'] = 'Get Support';
$lang['support_details'] = 'Please fill in the form below';
$lang['comment_question'] = 'Question/Comment';
