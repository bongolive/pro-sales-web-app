<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Sendforgotpassword extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->helper('form');
		$this->load->library('email');
	}

	public function index()
	{
	   if($this->input->post('mail') !=""){
	   		$cond = array('email' => $this->input->post('mail'));
			$user = $this->user_model->get_user($cond);
			
			if(count($user)>0){
				$pass= gen_md5_password(8);
				$to =  $user['email'];
				$body = "Your new password for username ".$user['username']." is ".$pass; 
				


				$this->email->from('info@pro.co.tz', 'ForgotPassword');
				$this->email->to($to);
				//$this->email->cc('another@another-example.com');
				//$this->email->bcc('them@their-example.com');

				$this->email->subject('Forgot Password for Footprint');
				$this->email->message($body);

				$this->email->send();

				//echo $this->email->print_debugger();
				$data = array('password'=> md5($pass));
				$user = $this->user_model->update($data,$cond);
				$result = array("status" => "1");
				echo json_encode($result);
			
			
	   		}else{
				$result = array("status" => "-1");
				echo json_encode($result);
			}
	   }
	}

}