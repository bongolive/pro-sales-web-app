<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class employees extends User_Controller {

	public $user_id;
	public $account_id;

	public function __construct() {
		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('firstname', 'lastname', 'gender', 'emp_role', 'doe', 'mobile', 'email', 'emp_status', 'username');
		parent::__construct();
		$this -> fields['ft_data'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js');

		 if (!$this -> session -> userdata('logged_in') == TRUE) {
		 $this -> session -> set_userdat+a('login_error', 'Please, Loggin first');
		 redirect('login');
		 } 

		$this -> load -> model('employees_model');
		$this -> load -> model('permissions_model');
		$this -> employees_model -> form_validation();
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');
		$this -> fields['view'] = 'employees/view/';
		$this -> fields['edit'] = 'employees/update/';
		$this -> fields['delete'] = 'employees/delete/';

		$this -> fields['table_id'] = $this -> employees_model -> table_id;

		$this -> fields['tb_name'] = 'all_employees';
		$this -> fields['controller'] = 'employees';
	$this -> user_id = $this->session->userdata('user_id');
		$this -> account_id = $this->account_id;

		//filtering the property items according to companyies

		/******************** check user permissions ***************************/

		$modules = explode(',', $this -> session -> userdata('permissions'));
		//$this -> fields['is_allowed'] = $this -> permissions_model -> read_modules($modules);

		/******************** check user permissions ***************************/
	}

	public function index() {
		$this -> session -> unset_userdata('id');
		$this -> fields['controller'] = 'employees';

		$where = array('account_id' => $this -> account_id);

		if ($this -> input -> post('filter')) {
			extract($_POST);
			IF ($employee) {
				$where['id'] = $employee;
			}

		}

		$employee = $this -> employees_model -> read($where);
if($employee){
		foreach ($employee as $s_team) {
			$sale_team[$s_team['id']] = $s_team['firstname'] . ' ' . $s_team['lastname'];
		}
}else{
	$sale_team[''] ="No Team Available";
}
		$this -> fields['sales_team'] = $sale_team;
		$this -> fields['tb_data'] = $employee;
		if ($this -> fields == FALSE) {$this -> data['tb_data'] = array();
		} else { $this -> data['tb_data'] = $this -> fields;
		}

		//checking users permissions
		$this -> load -> view('template/header', $this -> fields);
		$this -> load -> view('template/content/employees_table');
		$this -> load -> view('template/footer');
	}

	//viewing
	public function view() {

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('id', 'firstname', 'lastname', 'gender', 'emp_role', 'doe', 'mobile', 'email', /*'created_by',*/ 'created_on', 'username', 'permissions','commission');

		$id = $this -> uri -> segment(4);

		$where = array('employees.id' => $id);
		//geting the data
		$emp = $this -> employees_model -> read_employee($where);
		if ($emp) {
			foreach ($emp as $emp) {
				$this -> fields['tb_data'] = $emp;
			}
		} else {
			$this -> fields['tb_data'] = FALSE;
		}

		$this -> load -> view('template/header', $this -> fields);
		$this -> load -> view('template/content/employee_view', $this -> fields);
		$this -> load -> view('template/footer');

	}

	public function new_employee() {

		if ($this -> form_validation -> run() == FALSE) {
			$this -> fields['info'] = FALSE;
			$where = array('module_status' => 0);
			$this -> fields['modules'] = $this -> employees_model -> get_modules();
			//checking users permissions
			$this -> load -> view('template/header', $this -> fields);
			$this -> load -> view('template/content/employee_form', $this -> fields);
			$this -> load -> view('template/footer');
		} else {

			extract($_POST);
			//echo "<pre>";
			$data = array('firstname' => $firstname, 'lastname' => $lastname, 'emp_role' => $role, 'gender' => $gender, 'doe' => $doe, 'mobile' => $mobile, 'email' => $email, 'created_by' => $this -> user_id, 'account_id' => $this -> account_id, 'created_on' => date('Y-m-d h:i:s'),'commission' => $commission);
			//print_r($data);
			$this -> db -> trans_start();
			if ($this -> employees_model -> save($data)) {
				$emp_id = $this -> db -> insert_id();
				if ($emp_id) {
//					$permissions = implode(',', $permission);

					if ($username) {
						$status = 1;
					} else {
						$status = 0;
						$username = $firstname;
					}

					$user_data = array('username' => $username, 'password' => sha1($password), 'role' => $role, /*'permissions' => $permissions,*/ 'employee_id' => $emp_id, 'created_by' => $this -> user_id, 'account_id' => $this -> account_id, 'created_on' => date('Y-m-d h:i:s'), 'status' => $status);
					$this -> employees_model -> save_user($user_data);
					$this -> db -> trans_complete();

					//print_r($user_data);
				}

				redirect('employees');
			}
		}
	}

	public function update() {
		if ($this -> form_validation -> run() == FALSE) {
			if ($this -> session -> userdata('id')) {
				$my_id = $this -> session -> userdata('id');
			} else {
				$my_id = $this -> uri -> segment(4);
				$this -> session -> set_userdata('id', $my_id);
			}

			$where = array('employees.id' => $my_id);
			//geting the data
			$info = $this -> employees_model -> read_employee($where);
			if ($info) {
				foreach ($info as $info) {
					$this -> fields['info'] = $info;
				}
				$this -> fields['modules'] = $this -> employees_model -> get_modules();

				$this -> load -> view('template/header', $this -> fields);
				$this -> load -> view('template/content/employee_form', $this -> fields);
				$this -> load -> view('template/footer');
			} else {
				redirect('employees');
			}
		} else {

			extract($_POST);
			$this -> session -> unset_userdata('id');

			$permissions = implode(',', $permission);

			$data = array('firstname' => $firstname, 'lastname' => $lastname, 'gender' => $gender, 'emp_role' => $role, 'mobile' => $mobile, 'email' => $email,'commission' => $commission);

			if ($this -> employees_model -> update($employee_id, $data)) {
				$per = array('permissions' => $permissions,'username'=>$username,'password'=>sha1($password));
				$this -> employees_model -> update_users($user_id, $per);
				redirect('employees');
			} else {
				redirect('employees/update');
			}
		}
	}

	public function checkmail($email, $customer_id) {

		$where = array('employees.email' => $email);
		$customer = $this -> employees_model -> read($where);
		if ($customer) {

			if ($customer[0]['id'] == $customer_id) {
				return TRUE;
			} else {
				$this -> form_validation -> set_message('checkmail', 'Sorry! the email is already used by someone else');
				return FALSE;
			}
		} else {
			return TRUE;
		}
	}

	public function checkmobile($mobile, $customer_id) {

		$where = array('employees.mobile' => $mobile);
		$customer = $this -> employees_model -> read($where);
		if ($customer) {
			if ($customer[0]['id'] == $customer_id) {
				return TRUE;
			} else {
				$this -> form_validation -> set_message('checkmobile', 'Sorry! the mobile number is already used by someone else');
				return FALSE;
			}
		} else {
			return TRUE;
		}

	}

	public function delete() {
		$id = $this -> uri -> segment(5);
		$act = $this -> uri -> segment(4);
		if ($act == 'add') {
			$data = array('emp_status' => 1);
		} elseif ($act == 'remove') {
			$data = array('emp_status' => 0);
		}
		if ($this -> employees_model -> update($id, $data)) {
			redirect('employees');
		}
	}

}
