<?php

/**
 * This file carries the Profile values for language in English
 */

$lang['change_profile_details'] = 'Badilisha Taarifa zako';
$lang['name'] = 'Jina';
$lang['username'] = 'Jina la matumizi';
$lang['email'] = 'Barua Pepe';
$lang['mobile'] = 'Namba ya simu';
$lang['company_name'] = 'Jina la Kampuni';
$lang['change_password'] = 'Badili neno la siri';
$lang['new_password'] = 'Neno la siri jipya';
$lang['confirm_new_password'] = 'Rudi neno la siri';
$lang['support'] = 'Pata Msaada';
$lang['support_details'] = 'Tafadhali jaza hii fomu kupata msaada';
$lang['comment_question'] = 'Swali au Maoni';