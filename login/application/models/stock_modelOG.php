<?php
class Stock_model extends User_model{
public $table = 'stock';
	public $table_id = 'stock_id';

	function __construct() {

		parent::__construct();

	}

	public function form_validation() {

		/*$this -> form_validation -> set_rules('product_id', 'Product name', 'trim|required');
		 $this -> form_validation -> set_rules('stock_date', 'Stock date', 'trim|required');
		 $this -> form_validation -> set_rules('stock_qty', 'Stock qty', 'trim|required');
		 $this -> form_validation -> set_rules('price', 'Surchase price', 'required');
		 */
		return TRUE;
	}

	public function get_suppliers($where = false) {
		$this -> table = "suppliers";

		$suppliers = $this -> read($where);

		if ($suppliers) {
			return TRUE;
		} else {
			return FALSE;
		}

	}
public function get_product_stock($where = False) {

		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		//$this -> db -> join('products', 'products.product_id = stock.product_id');
		$this -> db -> join('suppliers', 'suppliers.supplier_id = stock.supplier', 'left');
		$this -> db -> order_by('stock_date', 'desc');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}
public function read_stock($where = False) {

		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$this -> db -> join('products', 'products.product_id = stock.product_id', 'left');
		$this -> db -> join('packages', 'products.package_type = packages.package_id', 'left');

		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}
public function get_stock($where) {

		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		$this -> db -> where_in('stock_id', $where);
		$this -> db -> join('products', 'products.product_id = stock.product_id');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}

	}
public function get_stock_forwarehouse($where) {

        $data =   $this->db->query("SELECT * FROM pro_stock s
        LEFT JOIN pro_products p ON  s.product_id = p.product_id WHERE s.warehouse =$where");
        return $data->result_array();

	}
public function read_sales_team($where) {

		$this -> db -> select('*,CONCAT(firstname," ",lastname) as employee_name', FALSE);
		$this -> db -> from('employees');
		$this -> db -> where($where);
		$this -> db -> join('devices', 'devices.assigned_to = employees.id');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}

	}
public function team_stock_summary($where) {

		$this -> table = 'sales_team';

		$this -> db -> select('sum(stock_qty * sale_price) as total_stock');
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}

	}
public function team_sales_summary($where) {

		$this -> table = 'sales_order_details';

		$this -> db -> select('sum(order_qty * sale_price) as total_sales');
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}

	}
public function latest_team_summary($where) {

		$this -> table = 'sales_team';

		$this -> db -> select(' sum(stock_sales * sale_price) as today_sales');
		$this -> db -> from($this -> table);
		$this -> db -> like();
		($where);
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}

	}

	public function stock_sold($where = false) {
		$this -> table = 'sales_order_details';

		$this -> db -> select('sales_order_details.product_id,sum(order_qty) as stock_sold');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		$this -> db -> group_by('sales_order_details.product_id');
	//	$this -> db -> join('products', 'products.product_id = sales_order_details.product_id');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}

	}
public function stock_assigned($where = false) {
		$this -> table = 'sales_team';
		$this -> table_id = 'stock_id';

		$this -> db -> select('sales_team.product_id,product_name,sum(stock_qty) as total_stock');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		$this -> db -> group_by('sales_team.product_id');
		$this -> db -> join('products', 'products.product_id = sales_team.product_id');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}

	}

	public function get_sales_team_stock($where = false) {
		$this -> table = 'sales_team';
		$this -> table_id = 'stock_id';
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$this -> db -> join('products', 'products.product_id = sales_team.product_id');
		$this -> db -> join('packages', 'products.package_type = packages.package_id', 'left');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}

	}
public function sales_team_stock($where = false) {
		$this -> table = 'sales_team';
		$this -> table_id = 'stock_id';
		$this -> db -> from($this -> table);
		$this -> db -> where($where);

		$this -> db -> join('products', 'products.product_id = sales_team.product_id');
		$this -> db -> join('packages', 'products.package_type = packages.package_id', 'left');
		$this -> db -> group_by('batch_no');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}

	}

	public function save_team_stock($data) {

		$this -> table = 'sales_team';

		if ($this -> save($data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}
public function get_current_stock_assigned($where){
        $query = $this->db->query("SELECT s.assigned_qty, s.stock_date, s.updated_date,remained_qty,
  ifNull((SELECT CONCAT(l.firstname,' ', l.lastname ) FROM pro_employees l WHERE l.id = s.employee_id ORDER BY s.assigned_id DESC LIMIT  1 ),NULL) AS sales_person ,
  ifNull(( SELECT pp.product_name FROM pro_products pp WHERE pp.product_id = s.product_id ),NULL ) AS product_name
  FROM pro_assigned_stock s  WHERE s.employee_id = '$where' ORDER BY s.assigned_id DESC");
        return $query->result_array();
    }

    public function get_recent_batch($where){
        $this->db->where('employee_id',$where);
        $this->db->order_by('batch_no','desc');
        $this->db->limit(1);
        $query = $this->db->get('pro_sales_team');
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $batch = $row['batch_no'];
            $return = array('employee_id' => $where, 'batch_no' => $batch);
            return $return;
        }
    }

    public function reset_device($where){
        $this-> db -> where('device_imei',$where);
        $data = array('is_synced'=>'0');

        $updat = $this->db->update('pro_sales_team',$data);
        if($updat){
            return true;
        }
    }

    public function get_warehouse_perproduct($prod){
        $query = $this->db->query("SELECT
 DISTINCT ifNull((SELECT w.wh_title FROM pro_warehouses w WHERE w.wh_id = s.warehouse ORDER BY s.stock_id DESC LIMIT  1 ),NULL) AS warehouse
 , s.stock_qty,s.expiry_date,s.created_on,s.last_update
FROM pro_products p INNER JOIN pro_stock  s ON p.product_id = s.product_id AND p.product_id = $prod ");
        return $query->result_array();
    }

public function assign_current_product($data){
        $this -> table = 'assigned_stock';
        while($data):

        if($this->is_product_assigned($data)){
            $j =  $this->udpdate_current_product($data);
            if($j){
                return true;
            } else {
                return false;
            }
        } else {
            $data = $this->db->insert($this->table,$data);
             if($data)
                 return true;
        }
        endwhile;
    }

    public function udpdate_current_product($where){
        $this -> table = 'assigned_stock';
	$tmpVar = $this->getCurrentStockQty($where);
        $newqty = $tmpVar['assigned_qty'] + $where['assigned_qty'];
	$getCurStock = $this->getCurrentStockQty($where);
        $newremaind = $getCurStock['remained_qty']+ $where['assigned_qty'];

        $data = array('sale_price' => $where['sale_price'],
            'updated_date' => $where['updated_date'],'assigned_qty' => $newqty
            ,'remained_qty' => $newremaind);
        $this->db->where(array('device_imei' => $where['device_imei'],'product_id'=>$where['product_id']));
        if($this->db->update($this->table, $data)){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getCurrentStockQty($where){
        $this->table = 'assigned_stock';
        $this->db->where(array('device_imei' => $where['device_imei'],'product_id'=>$where['product_id']));
        $qty = $this->db->get($this->table);
        if($qty -> num_rows() == 1){
            return $qty->row_array();
        }
    }

    public function add_new_assignedstock($where){
        $this->table = 'assigned_stock';
//        print_r($where);
        $data = $this->db->insert($this->table,$where);
        if($data)
            return true;
    }


    public function is_product_assigned($where){
        $this->table = 'assigned_stock';
//        print_r($where);
        $this->db->where(array('device_imei' => $where['device_imei'],'product_id'=>$where['product_id']));
        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }


    public function _api_get_current_stock($where){
        $this->db->where(array('device_imei' => $where['device_imei'],'product_id'=>$where['product_id']));
        $qty = $this->db->get('pro_assigned_stock');
        if($qty -> num_rows() == 1){
            $q = $qty->row();
            return $q->remained_qty ;
        }
    }

    public function _api_get_all_stock($where){

        $this->db->select('*');
        $this->db->from('pro_sales_team');
        $this->db->where($where);
        $this->db->join('pro_products','pro_products.product_id = pro_sales_team.product_id','left');
        $qty = $this->db->get();

        if($qty-> num_rows() > 0){
            $row = $qty->result_array() ;
            $productlist = array();
            foreach($row as $key => $row){
                $rqty = $this->_api_get_current_stock(array('product_id'=>$row['product_id'],'device_imei' => $where['device_imei']));
                if($rqty) {
                    $productlist[] = array('sys_stock_id' => $row['stock_id'], 'prod_system_id' => $row['product_id'],
                        'stock_qty' => $rqty,
                        'product_name' => $row['product_name'], 'packaging_type' => $row['package_type'],
                        'packaging_units' => $row['package_units'], 'unit_sale_price' => $row['unit_sale_price'],
                        'tax_rate' => $row['tax_rate'], 'sku' => $row['sku_product_no'], 'size' => $row['unit_size'], 'measure' => $row['unit_measure']);
                }
            }
            return $productlist;

        }
    }
}
