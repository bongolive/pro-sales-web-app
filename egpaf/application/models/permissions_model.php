<?php 
class Permissions_model extends MY_Model {
    public $table='permissions';
    public $table_id='perm_id';
	
	function __construct() {
		parent::__construct();
	}
   
public function form_validation() {
 if(empty($_POST['perm_id'])) {
  		$this->form_validation->set_rules('user_id','User name','trim|required|callback_check_user');
  	}else {
  		$this->form_validation->set_rules('user_id','User name','trim|required');
  	}
  		 
  }   
   
  public function read($where=FALSE) {
   $this->db->select("*,CONCAT(firstname, ' ', lastname) AS fullname",FALSE);  
  	$this->db->from($this->table);
		if($where) {  	
		  	$this->db->where($where);
		  	}
  
   $this->db->join('users','users.user_id = permissions.user_id','Left');
   $this->db->join('employees','employees.id=users.employee_id','Left');
   
	$query=$this->db->get()->result_array();
	if($query) {
		return $query;
		}else {
			return FALSE;
		} 
} 

  public function read_modules($where_in=FALSE) {
   $this->db->select("*");  
  	$this->db->from('modules');
		 if($where_in) {
		 		$this->db->where_in('module_id',$where_in);
		 } 
	$query=$this->db->get()->result_array();
	if($query) {
		return $query;
		}else {
			return FALSE;
		} 
}  
  
}