<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php
									if (!$info) {
										echo lang('new_question');
									} else {
										echo lang('update_question') . ' : ';
									}
							 ?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php if($info){ ?>
                                	<form role="form" method="post" action="<?php echo site_url('questionaires/update_question'); ?>">
                                	<?php }else{ ?>
                                	  <form role="form" method="post" action="<?php echo site_url('questionaires/create'); ?>">
                                	  	<?php } ?>
                                	  	<div class="box-body">
                                	                                	<?php if($info){ ?>
                                	                                		<input name="survey_no" id="survey_no" class="form-control" type="hidden"  value="<?php  echo $info['survey_no']; ?>" />
                                		<input name="question_id" id="question_id" class="form-control" type="hidden"  value="<?php  echo $info['question_id']; ?>" />
                                		<?php }else{ ?>
                                			<input name="survey_no" id="survey_no" class="form-control" type="hidden"  value="<?php  echo $survey; ?>" />
                                			<input name="section_no" id="section_no" class="form-control" type="hidden"  value="<?php  echo $section; ?>" />
                                	 <?php 	} ?> 
                                     <div class="form-group">
                                        	<label class="control-label" for="question" ><?php echo lang('question'); ?></label>
                                        	<?php echo form_error('question'); ?>
                                        	<textarea name="question" id="question" class="form-control" style="" ><?php
											if ($info) { echo $info['question_text'];
											} else { $value = set_value('question');
											}
											?></textarea>  
                                        	
                                    	</div>
                                             <div class="form-group">
                                        	<label class="control-label" for="survey_section" ><?php echo lang('survey_section'); ?></label>
                                        	<?php echo form_error('survey_section'); ?>
                                        	 <?php
                                        	 
                                         
												if ($info) { $value =  $info['survey_section'];
												} else { $value = $section;
												}
												$style= 'class="form-control"';
												echo form_dropdown('survey_section',$sections,$value,$style);
												?>
												</div> 
                                    	 
                                    		<div class="form-group">
                                        	<label class="control-label" for="rank" ><?php echo lang('rank'); ?></label>
                                        	
                                        		<?php echo form_error('rank'); ?>
                                        	<?php
											if ($info) { $value = $info['question_rank'];
											} else { $value = 'Auto Generated';
											}
												 ?>
													 <input type="number" name='rank' class="form-control" disabled="" value="<?php echo $value; ?>" />
                                        	
                                    	</div>
                                        <div class="form-group">
                                        	<label class="control-label" for="question_type" ><?php echo lang('question_type'); ?></label>
                                       <?php
									$types = array("" => "Select Question Type", "Open_ended" => "Open-ended (essay or short-answer)", "Closed_ended" => "Close-ended (Single Select)", "Closed_ended_mult" => "Close ended (Mult-Select)", "Partial_open_ended" => "Partial open ended (single-select with ‘other’ option)", "Partial_open_ended_mult" => "Partial open ended (multiple-choice with ‘other’ option)");

									echo form_error('question_type');
									if ($info) {
										$value = $info['question_type'];
									} else {
										$value = set_value();
									}

									$style = 'class="form-control"';
									echo form_dropdown('question_type', $types, $value, $style);
												 ?>
													 <!--<input type="number" name='question_type' class="form-control" value="<?php echo $value;?>" />-->
                                        	
                                    	</div>
                                        
                                        <div class="form-group">
                                        	<label class="control-label" for="answer_choices" ><?php echo lang('answer_choices'); ?></label>
                                        	<?php echo form_error('answer_choices'); ?>
 <TABLE   class="table" style="width: 100%;" >
 	<tr><td colspan="9"><p style="color: darkred;text-align: right"> Required fields are marked with an asterisk (<abbr class="req" title="required">*</abbr>).</p></td></tr>
		 <tr><th><INPUT type="checkbox" name=""/></th><th width='32%'><?php echo lang('response_text'); ?> <abbr class="req" title="required">*</abbr></th><th class="heads"><?php echo lang('next_question'); ?></th><th class="heads"><?php echo lang('match_with'); ?></th>
		 	
		 	<th class="heads"><?php echo lang('min_value'); ?></th><th class="heads"><?php echo lang('max_value'); ?></th><th class="heads"><?php echo lang('data_type'); ?> <abbr class="req" title="required">*</abbr></th><th class="heads"><?php echo lang('choice_no'); ?> <abbr class="req" title="required">*</abbr></th><th class="heads"><?php echo lang('required'); ?></th>
		 	 
		 	
		 	</tr>                                       	 
      </table>                                  	     
<TABLE id="dataTable" class="table" style="width: 100%;" >
 
   <?php 
    
   
     	 
 
 if($answers){
	foreach ($answers as $key => $value):
		 
		?>
		
		<TR>
			<TD><INPUT type="checkbox" name="chk"/></TD>    
          <TD>
            <INPUT type="hidden" name="response_id[]" class="form_control" value="<?php echo $value['choice_id'] ?>" />
            <INPUT type="text" name="response_text[]" class="form_control" style="width: 30%" value="<?php echo $value['response_text'] ?>" placeholder="Answer"/> 
            <INPUT type="text" name="next_question[]"class="form_control choices"  value="<?php if($value['next_question']){echo $value['next_question'];}else{ echo 0;} ?>"  /> 
           <INPUT type="text" name="match_with[]" class="form_control choices" value="<?php if($value['match_with']){echo $value['match_with'];}else{ echo 0;} ?>"   /> 
            <INPUT type="text" name="min_value[]" class="form_control choices" value="<?php if($value['min_value']){ echo $value['min_value'];}else{ echo 0;} ?>" /> 
             <INPUT type="text" name="max_value[]" class="form_control choices" value="<?php if($value['max_value']){ echo $value['max_value'];}else{ echo 0;} ?>" /> 
            <!-- <INPUT type="text" name="data_type[]"class="form_control choices" value="<?php echo $value['data_type'] ?>" />--> 
            <?php 
            $datatypes = array('1'=>'Numeric','2'=>'Alphanumeric','3'=>'Date','4'=>'Time');
			$style = 'class="form-control"';
            echo form_dropdown('data_type[]',$datatypes,$value['data_type']);
            ?>
            <INPUT type="text" name="choice_no[]"class="form_control choices" value="<?php echo $value['choice_no'] ?>" /> 
            <INPUT type="checkbox" name="required[]" class="form_control choices" value="<?php echo $value['required'] ?>"/> <?php echo lang('required'); ?> 
        </TR>
		
<?php 	endforeach; 
			}else{
		?>
    	 
     
        <TR>
            <TD><INPUT type="checkbox" name="chk"/></TD>
             
            <TD>
 
 
            	<INPUT type="text" name="response_text[]"class="form_control" style="width: 30%" placeholder="Response text"/>
     
   		<INPUT type="text" name="next_question[]" class="form_control choices" value="0" placeholder="Next question" />
    
           <INPUT type="text" name="match_with[]" class="form_control choices" value="0"  placeholder="Match with" />
           <INPUT type="text" name="min_value[]" class="form_control choices" value="0" placeholder="Min value" />
           <INPUT type="text" name="max_value[]" class="form_control choices" value="0" placeholder="Max value" />
           <!--<INPUT type="text" name="data_type[]" class="form_control choices" placeholder="data_type" /> -->
           <?php 
            $datatypes = array('1'=>'Numeric','2'=>'Alphanumeric','3'=>'Date','4'=>'Time');
			$style = 'class="form-control"';
            echo form_dropdown('data_type[]',$datatypes,$value);
            ?>
           <INPUT type="number"   name="choice_no[]" class="form_control choices" placeholder="Choice no">
           <INPUT type="checkbox" name="required[]" class="form_control choices" value="1"  /><?php echo lang('required'); ?> 	
           
            </TD> 
             
        </TR>
    <?php } ?>
      </TABLE> 
    
       <span class="">
 <input type="button" class="btn btn-small btn-info" value="Add Answer" id="anc_add" onclick="addRow('dataTable')" />
 <input type="button" class="btn btn-small btn-danger" value="Delete"  id='anc_rem' onclick="deleteRow('dataTable')" />
 </span>   
  
</div>

								 
                                
                                    		
                     
								<div class="modal-footer">
									
									<div class="form-actions">
										<?php
										if ($info) {
											$survey = $info['survey_no'];
											$section = $info['survey_section'];
										}
 ?>
										<a href="<?php echo site_url('surveys/view_section/' . $survey . '/' . $section); ?>"  class="btn btn-default btn-medium" data-dismiss="modal">Back</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
 

