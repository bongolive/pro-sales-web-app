<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
<link rel="stylesheet" href="bx_styles.css" type="text/css" />
<title>footprint</title>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>

<script src="js/jquery.bxSlider.js" type="text/javascript"></script>
<script src="js/jquery.bxSlider.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    
	 $('.slider').bxSlider({
    auto: true,
    pager: true
  });
  });
</script>
</head>

<body>
	<div class="body_class">
    	<div class="center_header_page">
        	<div class="header-left">
            	<img src="images/logo3.png" alt=""  />
            </div>
            <div class="header-right">
			<?php session_start();
			if(!isset($_SESSION['username'])){
			echo "<a href='login.php' ><img src='images/login_button.png' alt='' /></a>";
			}else{
			echo "<a href='device.php' ><img src='images/login_button.png' alt='' /></a>";
			}
			?>
            	
            </div>
        </div>
	</div>
<div class="center_header_page">
<!--    	<div class="menu_wrapper">
        	<div class="menu_first">
            	<div class="menu_img home">
                	<img src="images/home.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >Home</a>
                </div>
            </div>
            <div class="menu_first">
            	<div class="menu_img">
                	<img src="images/company.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >Company</a>
                </div>
            </div>
            <div class="menu_first">
            	<div class="menu_img">
                	<img src="images/client.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >Clients</a>
                </div>
            </div>
            <div class="menu_first">
            	<div class="menu_img">
                	<img src="images/resource.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >Resources</a>
                </div>
            </div>
            <div class="menu_first">
            	<div class="menu_img">
                	<img src="images/support.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >Support</a>
                </div>
            </div>
            <div class="menu_first">
            	<div class="menu_img">
                	<img src="images/blog.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >Blog</a>
                </div>
            </div>
            <div class="menu_first">
            	<div class="menu_img home">
                	<img src="images/contact.png" alt="" />
                </div>
                <div class="menu_content">
                	<a href="" >Contact</a>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    
    </div>
    <div class="line">
    </div>-->
    <div class="center_header_page">
    	 <div class="slider_buttom">
            	
            </div>
        <div class="slider_wrapper">
    	<div class="slider">
            <div class="slider_1">
            <img src="images/slider1.jpg" alt=""  />
                           <div class="slider_text">
               <h3>Get Peace of Mind - </h3>
               <p> Monitor where your employees are at anytime.</p>
               </div>
            </div>
            
            <div class="slider_1">
            <img src="images/slider2.jpg" alt=""  />
                           <div class="slider_text">
               <h3>Improve Productivity - </h3>
               <p> Keep your employees accountable about their work.</p>
               </div>
            </div>
            
            <div class="slider_1">
            	<img src="images/slider3.jpg" alt=""  />
               <div class="slider_text">
               <h3>Ensure Safety - </h3>
               <p> Know the location of your family members or employees in case of emergency.</p>
               </div>
                         
            </div>
        </div>
        </div>
        	
        <div class="content" >
        	<div class="content_first">
            	<div class="content_first_heading">
                <img src="images/sub_line.png" alt=""  />
                <h2>   WELCOME   </h2>
                <img src="images/sub_line.png" alt=""  />
                </div>
                <div class="clear"></div>
                <p>proTracker is a new and powerful monitoring solution that helps you manage what your employees are doing and track their location in real time. It is a solution that tracks the location of a mobile phone anywhere in the world whilst allowing you to easily manage your workforce from the comfort of your home or office. <p>
               
                <p>It is also an ideal solution for parents who are worried about their children's safety and would like to track the location of their children at any time, any where in the world.<p>
                
                <p>Read more below about how it can help your company or family.</p>
	
	   		<div class="content_learn content_learn_more ">
            	<h3>Learn more</h3>
                <img src="images/arrow.png" alt=""  />
            </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="content_first content_sec_heading">
                <img src="images/sub_line.png" alt=""  />
                <h2>   Features   </h2>
                <img src="images/sub_line.png" alt=""  />
               <div class="content_second">
               	<div class="content_sec_left">
                	<div class="content_sec_left_first content_sec_left_first_detail">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_1.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head">
                        	<h3>Live Tracking on Website</h3>
                        </div>
                        <div class="clear"></div>
                        <p>View the current location of your employee or child in realtime.</p>
						<div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
               		 	</div>
                      </div>
                <div class="clear"></div>
                <div class="content_sec_left_first content_sec_left_first_detail">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_2.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_sec_left_margin">
                        	<h3>Historical Records</h3>
                        </div>
                        <div class="clear"></div>
                        <p>We store historical location records for upto three months for your reference.</p>
						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                		</div>
                       </div>
                <div class="clear"></div>
                <div class="content_sec_left_first content_sec_left_first_detail">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_3.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_sec_left_margin">
                        	<h3>Markers</h3>
                        </div>
                        <div class="clear"></div>
                        <p>We display clear markers on a map for you to track the route taken.</p>
						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                		</div>
                       </div>
                 <div class="clear"></div>
                <div class="content_sec_left_first content_sec_left_first_detail">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_7.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_sec_left_margin">
                        	<h3>Hidden</h3>                            
                        </div>
                        <div class="clear"></div>
                        <p>Our service is invisible on the phone. The individual will not know that the phone is being monitored.</p>
						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                        </div>
                      
                       </div>
                         <div class="clear"></div>
                    </div> 
             
                <div class="content_sec_right">
                	
                    <div class="content_sec_left_first">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_4.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_sec_right_margin">
                        	<h3>Manage Multiple Devices</h3>
                        </div>
                        <div class="clear"></div>
                        <p>You can monitor multiple individuals from one account with ease.</p>
						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
               		 	</div>
                      </div> 
                      <div class="clear"></div>
                      <div class="content_sec_left_first">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_5.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_sec_right_margin">
                        	<h3>Boundaries</h3>
                        </div>
                        <div class="clear"></div>
                        <p>You can set boundaries that limit how far an individual can travel & get alerts if they step outside it.</p>
						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
               		 	</div>
                      </div>
                	 <div class="clear"></div>
                      <div class="content_sec_left_first">
                    	<div class="content_sec_left_first_img">
                        	<img src="images/f_img_6.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_sec_right_margin">
                        	<h3>Alerts</h3>
                        </div>
                        <div class="clear"></div>
                        <p>Get email notifications when a destination is reached or a boundary is crossed.</p>
						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
               		 	</div>
                      </div> 
                </div>
               
            
            
           </div><!--content_second-->
	     <div class="clear"></div>
         
         <div class="content_third"><!--content_third-->
         	<div class="content_third_left">
            	<div class="content_thir_left_first">
                	<h3>Testimonials</h3>
                    <div class="clear"></div>
                	<div class="content_third_left_first_block" >
                    <div class="clear"></div>
                    	<p>"We love this solution. It saves us time in monitoring our sales staff and ensuring they are productive."</p>
					<h4>Owner - TJD Computers</h4>
                    </div>
                </div>
                <div class="clear"></div>
				<a name="contact"></a>
                <div class="content_third_left_second">
                	<h3>Quick Contact</h3>
                    <div class="clear"></div>
					<form id="contact_form" action="javascript:void(0);" method="post">
                    <div class="content_third_left_contact">
                    	<div class="content_third_left_contact_input">
                    	<p>NAME:&nbsp;</p><input type="text" value="" name="txt_name" />
                        </div>
                        <div class="content_third_left_contact_input">
                        <p>EMAIL:</p><input type="text" value="" name="txt_email"  />
                        </div>
                        <div class="content_third_left_contact_txtarea">
                        <p>MESSAGE:</p><textarea  name="txt_message" rows="4" cols="20" ></textarea>
                        </div>
                        <div class="content_third_left_button">
                        	<a href="javascript:void(0);" id="submit_a">SEND</a>
                        </div>
                    </div>
					</form>
                     <div class="clear"></div>
                     <div class="content_third_left_buttom">
                     	<img src="images/phone.png" alt=""  /><p>Call Us Now! +255 688 121 252</p>
                     </div>
                </div>
            
            </div>
            <div class="content_third_center">
            </div>
            
            <div class="content_third_right">
            	<div class="content_third_right_heading">
                	<h2>Who Can Use It</h2>
                </div>
                <div class="clear"></div>
                 <div class="content_third_right_first">
                    	<div class="content_third_right_mrg">
                        	<img src="images/check_simbols.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_third_right_mrg_head">
                        	<h3>Manufacturing Business</h3>
                        </div>
                        <div class="clear"></div>
                        <p>Your procurement and sales staff spend a large part of their day out of the office. Are they really working on your tasks. This solution helps you ensure they are working full time and reduce fraud in the process.</p>
<!--						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                        </div> -->
                      	 <div class="clear"></div>
                       </div>
                       <div class="clear"></div>
                 <div class="content_third_right_first">
                    	<div class="content_third_right_mrg">
                        	<img src="images/check_simbols.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_third_right_mrg_head">
                        	<h3>Pharma &amp; Healthcare</h3>
                        </div>
                        <div class="clear"></div>
                        <p>Are your sales staff and medical representatives actually visiting doctors? This solution helps you make sure that they really are, saving you time and increasing your sales.</p>
						<!-- <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                        </div>-->
                      	 <div class="clear"></div>
                       </div>
                       <div class="clear"></div>
                 <div class="content_third_right_first">
                    	<div class="content_third_right_mrg">
                        	<img src="images/check_simbols.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_third_right_mrg_head">
                        	<h3>Service Business</h3>
                        </div>
                        <div class="clear"></div>
                        <p>Your employees are the vital assets of your business. Whether they are visiting customers, purchasing or out in the field, ensure they are accountable and productive with their time by monitoring their location.</p>
	<!--					 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                        </div>-->
                      	 <div class="clear"></div>
                       </div>
                       <div class="clear"></div>
                 <div class="content_third_right_first">
                    	<div class="content_third_right_mrg">
                        	<img src="images/check_simbols.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_third_right_mrg_head">
                        	<h3>Construction Companies</h3>
                        </div>
                        <div class="clear"></div>
                        <p>Manage your supervisors, drivers, technicians and engineers to ensure they are at the sites they are supposed to be.</p>
<!--						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                        </div> -->
                      	 <div class="clear"></div>
                       </div>
                       <div class="clear"></div>
                 <div class="content_third_right_first">
                    	<div class="content_third_right_mrg">
                        	<img src="images/check_simbols.png" alt=""  />
                        </div>
                        <div class="content_sec_left_first_head content_third_right_mrg_head">
                        	<h3>Family</h3>
                        </div>
                        <div class="clear"></div>
                        <p>Track the location of your chilren or adult family members in realtime.</p>
<!--						 <div class="content_learn">
                            <h3>MORE</h3>
                            <img src="images/arrow.png" alt=""  />
                        </div>-->
                      	 <div class="clear"></div>
                       </div>
            </div>
            
         	 <div class="clear"></div>
         </div><!--content_third-->
            </div>
        </div>
    
    </div>
    <div class="footer-wrapper">
    	<div class="center_footer_page">
        	<div class="footer_center">
            	<div class="footer_navi">
 <!--           	<a href="" >Home</a>
                <a href="" >Company</a>
                <a href="" >Clients</a>
                <a href="" >Resources</a>
                <a href="" >Support</a>
                <a href="" >Blog</a> 
                <a href="" >Contact</a>-->
                <a href="" ><p>Copyright &copy; Bongo Live Enterprise Ltd. All Rights Reserved.</p></a>
                </div>
            </div>
         <div class="clear"></div>
        </div>
    </div>
    <div class="center_header_page">
    	<div class="footer_buttom">
<!--    	<h3>FOOT PRINT</h3><p>Copyrihgt &copy; 2012. All Rights Reserved.</p>-->
        </div>
         <div class="clear"></div>
    </div>
	
	<script type="text/javascript">
	$(document).ready(function(){
	 $("#submit_a").click(function(){
		
		submitForm();
	      
	    });
	 
	});
	function submitForm(){
	  
        var str = $("#contact_form").serialize();
        $.ajax({
		type : 'POST',
		url : 'ses_test/contactmail.php',
		data : str,
		success : function(data){
               alert("Your request has been submitted.");
			}  
		}); 
        return(false);
   
	}
	</script>
	<script src="js/analytics.js" type="text/javascript"></script>
</body>
</html>
