<?php
//exit;
//var_dump( $imei );
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>footprint</title>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
        var map;
        var openedInfoWindow = null;
        var allMarkers = [];
        
        //var userId = ;
        var locations = [];
        
        function showLastLocations(){
        	locations.length = 0;
        	var xmlDocument ;
        	// show some processing icon
        	document.getElementById("div_map_processing").style.display = 'block';
        	
        	jQuery.ajax({
        	url: "<?php echo site_url('tracking/lastlocation_customer/'.$imei.'/'.$sdate.'/'.$edate); ?>",//sending ajax request to our php file
        	//cache: false,
        	//processData: false,
        	data: xmlDocument,//storing the data in xmlDocument cariable
        	success: function(xmlDocument) {
        		// retreving the data by applying loop on tag "segment"
        		jQuery(xmlDocument).find("location").each(function()
        		{
        			var loc = [];
        			loc.push( jQuery(this).find("phone").text());
        			loc.push( jQuery(this).find("holder_name").text());
        			loc.push( jQuery(this).find("latitude").text());
        			loc.push( jQuery(this).find("longitude").text());
					loc.push( jQuery(this).find("grand_total").text());
					loc.push( jQuery(this).find("localdate").text());
					loc.push( jQuery(this).find("address").text());

						locations.push(loc);
        		});
        		
        		
        		//alert("main:"+locations.length);
        		//alert("detail:"+locations[0].length);
        		removeAllMarkers();

				for(var i = 0; i < locations.length; i++){


						//console.log( locations[i][3] );

						var content = "<strong>Customer Name : </strong>"+locations[i][1]+"<br>";
							content += "<strong>Mobile Number : </strong>"+locations[i][0]+"<br>";
							content += "<strong>Address : </strong>"+locations[i][6]+"<br>";
							content += "<strong>Total Sales : </strong>"+locations[i][4]+"<br>";
							content += "<strong>Date : </strong>"+locations[i][5]+"<br>";

						var latlngpoint = new google.maps.LatLng( locations[i][2], locations[i][3]);
        				
        				//var latlngpoint = new google.maps.LatLng( locations[i][5], locations[i][6]);
        				//console.log(content);
        				setMarker(map, latlngpoint, content);

        		}
        		setBoundsToAllMarkers();
        		
        		document.getElementById("div_map_processing").style.display = 'none';
        	}
          });
        }
        
        function initializeMap() {
        //document.getElementById("ajaxLoader").style.display = 'block';
          
        var centerPoint = new google.maps.LatLng(33.773654,-84.37324);
        
        var myOptions = {
        	zoom: 2,
        	center: centerPoint,
        	mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        
        map = new google.maps.Map(document.getElementById("div_loc_map"), myOptions);
        
        //
        showLastLocations();
        //
        }
        
        function setMarker(map, latlng, content){
        	
        	var infowindow = new google.maps.InfoWindow({
        		content: content
        	});
        	
        	var marker = new google.maps.Marker({
        		position: latlng,
        		map: map
        	});
        	
        	allMarkers.push(marker);
        	
        	/*google.maps.event.addListener(marker, 'click', function() {
        		if (openedInfoWindow != null) {
        			openedInfoWindow.close();
        		}
        		infowindow.open(map,marker);
        		
        		openedInfoWindow = infowindow;
        		google.maps.event.addListener(infowindow, 'closeclick', function() {
        			openedInfoWindow = null;
        		});
        	});*/
        	
        	google.maps.event.addListener(marker, 'mouseover', function() {
        		if (openedInfoWindow != null) {
        			openedInfoWindow.close();
        		}
        		infowindow.open(map,marker);
        		
        		openedInfoWindow = infowindow;
        		google.maps.event.addListener(infowindow, 'closeclick', function() {
        			openedInfoWindow = null;
        		});
        	});
        	
        	
        }
        
        function removeAllMarkers(){
        	/*if(allMarkers){
        		for(i in allMarkers){
        				allMarkers[i].setMap(null);
        		}
        		allMarkers.length = 0;
        	}*/
        	//alert("length : "+allMarkers.length);
        	for(var i = 0; i < allMarkers.length; i ++){
        		//alert(allMarkers[i]);
        		allMarkers[i].setMap(null);
        	}
        	allMarkers.length = 0;
        }
        
        function setBoundsToAllMarkers(){
        	var bounds = new google.maps.LatLngBounds();
        	
        	for (var index in allMarkers) {
        
        		var latlng = allMarkers[index].getPosition();
        
        		bounds.extend(latlng);
        	}
        	map.fitBounds(bounds);	
        }
</script>

</head>
<body onload="initializeMap();">

            <div id="div_map_wraper" style="margin:0 auto; width:100%; height:500px; position:relative;" >
            
                <div id="div_loc_map" class="content_first" style="width:100%; height:500px; border-radius:10px; -moz-border-radius:10px;">
                    
                </div>
                
                <div id="div_map_processing" style="width:100%; height:500px; position:absolute; top:0; display:none; background-color:rgb(0, 0, 0); opacity:0.5;filter:alpha(opacity=50);  border-radius:10px; -moz-border-radius:10px;">
                	<img src="<?php echo base_url(); ?>images/loading.gif" width="32" height="32" style="position: relative;top: 44%;left: 48%;" />
                </div>
                
            </div>
            
</body>
</html>