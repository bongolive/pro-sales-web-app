				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('products_stock_history'); ?></h2>
						 
            			</header> 
       				 </article> 
    </div>

    <div class="row">
     						
  
    	 <article class="span12 data-block">

								<div class="modal-header">
									<h4><?php _l('add_stock'); ?></h4>
								</div>
								<div class="modal-body">
						<?php  if($info){ ?>
							<form class="form-horizontal" method="post" action="<?php echo site_url('stock/update_stock'); ?>">
							<?php }else{ ?>			
                                <form class="form-horizontal" method="post" action="<?php echo site_url('stock/addstock'); ?>">
                             <?php } ?>   	
                                	<input type="hidden" name="addstock" value="true"/>

                                    <fieldset>
                                        <table class="datatable table table-striped table-bordered table-hover">

                                            <tr><td>
                                        <div class="control-group">
                                            <label class="control-label" for="warehouse" ><?php _l('product_warehouse'); ?></label>
                                            <div class="controls">
                                                <?php echo form_error('warehouse');

                                                if(!$warehouse){
                                                    $warehouse = array(""=>"No Warehouse Specified");
                                                }
                                                if ($info) { $value =  $info[0]['warehouse'];
                                                } else { $value = set_value();
                                                }
                                                echo form_dropdown('warehouse',$warehouse,$value);

                                                ?>

                                            </div>
                                        </div>
</td>
                                                <td>
                                                    <div class="control-group">
                                                        <label class="control-label" for="expiry_date" ><?php _l('expiry_date'); ?></label>
                                                        <div class="controls">
                                                            <?php echo form_error('expiry_date'); ?>
                                                            <div class="input-append no-margin">
                                                                <input  name="expiry_date" class="datepicker input-small"
                                                                        value="<?php if($info){echo $info[0]['expiry_date'];}else{echo set_value();}?>"
                                                                        placeholder="<?php echo date('Y-m-d')?>" type="text" required><span class="add-on">
                                                                    <i class="icon-calendar"></i></span>

                                                            </div>
                                                    </div>
                                                        </div>
                                                </td> </tr><tr>
                                                <td>
                                                    <div class="control-group">
                                                        <label class="control-label" for="purchase_order_no" ><?php _l('purchase_order_no'); ?></label>
                                                        <div class="controls"><?php echo form_error('purchase_order_no'); ?>
                                                            <input name="purchase_order_no" id="purchase_order_no" class="input-large"  placeholder="0" type="text" value="<?php
                                                            if ($info) {  echo $info[0]['purchase_order_no'];
                                                            } else { echo set_value();
                                                            }
                                                            ?>" required>
                                                        </div>
                                                    </div>
                                                </td><td>
                                                    <div class="control-group">
                                                        <label class="control-label" for="delivery_order_no" ><?php _l('delivery_order_no'); ?></label>
                                                        <div class="controls"><?php echo form_error('delivery_order_no'); ?>
                                                            <input name="delivery_order_no" id="delivery_order_no" class="input-large"  placeholder="0" type="text" value="<?php
                                                            if ($info) {  echo $info[0]['delivery_order_no'];
                                                            } else { echo set_value();
                                                            }
                                                            ?>" required>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>


<div class="control-group">
                                        	<label class="control-label" for="order_no" ><?php _l('stock_details'); ?></label>
                                        	<div class="controls">
                                        		<table class="datatable table table-striped table-bordered table-hover"><tr><th></th><th><?php  _l('product_name')?></th><!-- <th><?php  _l('stock_date')?></th>--><th><?php  _l('entry_mode')?></th><th><?php  _l('stock_qty')?></th> <th><?php  _l('unit_purchase_price');?></th></tr></table>

<TABLE id="dataTable" class="datatable table table-striped table-bordered table-hover" >
   <?php 
   
   //get products
   					if($products ==FALSE){  $products = array(''=>'No Products'); } 
            		if($info==FALSE){$prod = set_value('product_id');} 
   
     	if($info) {
// print_r($info);
	foreach ($info as $info):
		 ;
		
		 	$prod = $info['product_id'];  
		?>
		
		<TR>
            <TD><INPUT type="hidden" name="stock_id" value="<?php echo $info['stock_id'];?>"/><INPUT type="hidden" name="last_stock" value="<?php echo $info['last_stock'];?>"/></TD>
            <TD><?php echo form_dropdown('product_id[]', $products, $prod,'required'); ?>
            </TD>
           <!-- <TD><input  type="date"  name="stock_date[]"class="input-medium" value="<?php echo $info['stock_date']; ?>" placeholder="Stock date" required="" /></TD>-->
            <TD>
            	<?php 
            	$value =  $info['entry_mode']; 
            	$emodes = array('units'=>'Units','packages'=>'package');
				$attr = 'class="input-small"';
				echo form_dropdown('entry_mode',$emodes,$value,$attr); 
            	?> </TD>
            <TD><INPUT type="number" name="stock_qty[]"class="input-medium"  value="<?php
            //check entry mode if was package, convert units to package
            if($info['entry_mode']=='packages'){
            	$stockqty = $info['stock_qty'] / $info['package_units'];
            }else{ $stockqty = $info['stock_qty']; }
             echo $stockqty; ?>" placeholder="Quantity" required="" /></TD>
            <TD>
            	 
<div class="input-append">
	<INPUT type="number"  name="price[]" id="appendedInput" class="span2"  placeholder="Price"   value="<?php echo $info['purchase_price']; ?>"required="" />
 
<span class="add-on" style="color: rgb(41, 128, 185);"> <?php echo $this->settings['default_currency'] ;?></span>
</div>
 
            	
            	</TD>
            
        </TR>
		
<?php 	endforeach;
			}else{
		?>
    	 
     
        <TR>
            <TD><INPUT type="checkbox" name="chk"/></TD>
            <TD><?php

			if ($products == FALSE) {  $products = array('' => 'No Products');
			}
			if ($info) { 	$prod = $info['product_id'];
			} else {$prod = set_value('product_id');
			}
			echo form_dropdown('product_id[]', $products, $prod);
            		?>
            </TD>
         <!--   <TD><input  type="date"  name="stock_date[]"class="input-medium" placeholder="Stock date" required="" /></TD>-->
            <TD><?php 
            	$value =  $info['entry_mode']; 
            	$emodes = array('units'=>'Units','packages'=>'packages');
				$attr = 'class="input-small"';
				echo form_dropdown('entry_mode',$emodes,$value,$attr);
				
            	?> </TD>
            <TD><INPUT type="number" name="stock_qty[]"class="input-medium" placeholder="Quantity" required="" /></TD>
            <TD>
            	<div class="input-append">
	<INPUT type="number" name="price[]"class=" input-medium" placeholder="Price" required="" />
 
<span class="add-on" style="color: rgb(41, 128, 185);"> <?php echo $this->settings['default_currency'] ;?></span>
</div>
            	</TD>

            <script type="javascript">
                var controller = 'orders';
                var base_url = '<?php echo site_url(); ?>';

                $("#s_t_product").change(function(){
                    var res = $("#s_t_product option:selected").val();
                    alert(res);
                    var controller = 'orders';
                    var base_url = '<?php echo site_url(); ?>';
                    $.ajax({
                        type : 'POST',
                        data : 'product_id='+ res,
                        url : base_url+ '/' + controller+ '/getUnitPrice',
                        success : function(data){
                            $('#result_area').val(data);
                        }
                    });
                });
            </script>
            
        </TR>
    <?php } ?>
      </TABLE> 
     <?php
     if($info ==FALSE){
     ?> 
       <span class="" style="float: right;padding-right: 5%">
 <input type="button" class="btn btn-medium btn-info" value="Add Row" onclick="addRow('dataTable')" />
 <input type="button" class="btn btn-medium btn-danger" value="Delete Row" onclick="deleteRow('dataTable')" />
 </span>   
    <?php } ?>
    </div>
</div>
						</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('stock'); ?>" class="btn btn-alt" data-dismiss="modal">Back</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>
                        </div>                        	
                        </div>	
                        </section>
					 