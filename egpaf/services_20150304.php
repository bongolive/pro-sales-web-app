<?php

class Services {
	private $conn;
	public $data;
	public $imei;

	function __construct() {
		$host = 'localhost';
		$username = 'survey';
		$password = '123456';
		$database = 'apps_surveys';

		$this -> conn = mysqli_connect($host, $username, $password, $database) or die(mysqli_connect_error());

	}

	/*
	 * REGISTER THE DEVICE THAT IS ATTEMPTING TO SYNC
	 */
	public function syncing_device($data) {

		if ($data['imei']) {
			$qry = "INSERT IGNORE INTO  pro_device_sync_status  SET  device_imei  = '" . $this -> conn -> real_escape_string($data['imei']) . "'";

			$this -> conn -> query($qry) or die(mysqli_errno($this -> conn));

			/*
			 * check if device has completed sycing
			 */
			$device = $this -> conn -> query("SELECT * FROM pro_device_sync_status WHERE  device_imei='" . $this -> conn -> real_escape_string($data['imei']) . "'") or die(mysqli_error($this -> conn));
			if ($device) {
				$row = mysqli_fetch_assoc($device);

				//check fields values
				if ($row['settings'] == 1 && $row['users'] == 1 && $row['surveys'] == 1 && $row['questions'] == 1 && $row['responses'] == 1) {

					$sql = "UPDATE pro_devices SET  last_sync='" . date('Y-m-d H:i:s') . "' where device_imei ='" . $this -> conn -> real_escape_string($row['device_imei']) . "'";
					$this -> conn -> query($sql) or die(mysqli_error($this -> conn));
				}
			}
			return TRUE;
		} else {
			return FALSE;
		}
	}

	//check device sync status
	public function device_sync_status($data) {
		$device = $this -> conn -> query("SELECT * FROM pro_device_sync_status WHERE  device_imei='" . $this -> conn -> real_escape_string($data['imei']) . "'") or die(mysqli_error($this -> conn));
		if ($device) {
			$row = mysqli_fetch_assoc($device);
			return $row;
		} else {
			return FALSE;
		}
	}

	public function check_device($data) {

		$device = $this -> conn -> query("SELECT * FROM pro_devices WHERE pro_devices.device_imei='" . $this -> conn -> real_escape_string($data['imei']) . "'") or die(mysqli_error($this -> conn));
		if ($device) {

			$row = mysqli_fetch_assoc($device);

			//check device status
			if ($row['last_sync'] == '0000-00-00 00:00:00') {
				$state = FALSE;
			} else {
				$state = $row;
			}

		}

		return $state;

	}

	public function read_questions($data) {
		//get account
		$account = $this -> conn -> query("SELECT account_no FROM pro_devices WHERE pro_devices.device_imei='" . $this -> conn -> real_escape_string($data['imei']) . "'") or die(mysqli_error($this -> conn));

		if ($account) {
			$account = mysqli_fetch_assoc($account);
			$results = $this -> conn -> query("select *,survey_section as category from pro_questionaires where account_no = '" . $account['account_no'] . "' order by question_id") or die(mysqli_error($this -> conn));
			$questions = array();
			if ($results) {
				while ($row = mysqli_fetch_assoc($results)) {
					$questions[] = $row;
				}
			} else {
				$questions['failure'] = FALSE;
			}
		} else {
			return FALSE;
		}

		mysqli_close($con);
		return $questions;

	}

	/*
	 * read questions updates
	 */

	public function questions_updates($data) {

		//get account
		$account = $this -> conn -> query("SELECT account_no FROM pro_devices WHERE pro_devices.device_imei='" . $data['device_imei'] . "'") or die(mysqli_error($this -> conn));

		if ($account) {
			$account = mysqli_fetch_assoc($account);
			$results = $this -> conn -> query("select *,survey_section as category from pro_questionaires where account_no = '" . $account['account_no'] . "' && last_update_time >'" . $data['last_sync'] . "'") or die(mysqli_error($this -> conn));
			$questions = array();
			if ($results) {
				while ($row = mysqli_fetch_assoc($results)) {
					$questions[] = $row;
				}
			} else {
				$questions['failure'] = FALSE;
			}
		} else {
			return FALSE;
		}

		mysqli_close($con);
		return $questions;

	}

	public function read_response_choices($data) {
		//get account
		$account = $this -> conn -> query("SELECT account_no FROM pro_devices WHERE pro_devices.device_imei='" . $this -> conn -> real_escape_string($data['imei']) . "'") or die(mysqli_error($this -> conn));

		if ($account) {
			$account = mysqli_fetch_assoc($account);
			$results = $this -> conn -> query("select * from pro_response_choices where account_no = '" . $account['account_no'] . "'") or die(mysqli_error($this -> conn));
			$questions = array();
			if ($results) {
				while ($row = mysqli_fetch_assoc($results)) {
					$questions[] = $row;
				}
			} else {
				$questions['failure'] = FALSE;
			}
		} else {
			return FALSE;
		}

		mysqli_close($con);
		return $questions;

	}

	/*
	 * reading responses updates
	 */
	public function get_responses_updates($data) {
		//get account
		$account = $this -> conn -> query("SELECT account_no FROM pro_devices WHERE pro_devices.device_imei='" . $data['device_imei'] . "'") or die(mysqli_error($this -> conn));

		if ($account) {

			$account = mysqli_fetch_assoc($account);
			$results = $this -> conn -> query("select * from pro_response_choices where account_no = '" . $account['account_no'] . "' && last_update_time >'" . $data['last_sync'] . "'") or die(mysqli_error($this -> conn));
			$questions = array();
			if ($results) {

				while ($row = mysqli_fetch_assoc($results)) {
					$questions[] = $row;
				}
			} else {

				$questions['failure'] = FALSE;
			}
		} else {

			return FALSE;
		}

		mysqli_close($con);
		return $questions;

	}

	// assigning users to devices
	public function read_users($data) {
		$users = $this -> conn -> query("SELECT account_no,device_id FROM pro_devices WHERE pro_devices.device_imei='" . $this -> conn -> real_escape_string($data['imei']) . "'") or die(mysqli_error($this -> conn));

		if ($users) {
			$users = mysqli_fetch_assoc($users);

			$results = $this -> conn -> query("select * from pro_users where device_imei = '" . $users['device_id'] . "' and account_no = '" . $users['account_no'] . "'") or die(mysqli_error($this -> conn));
			$users = array();
			if ($results) {
				while ($row = mysqli_fetch_assoc($results)) {
					$users[] = $row;
				}
			} else {
				$users['failure'] = FALSE;
			}
		} else {
			$users = FALSE;
		}

		mysqli_close($con);
		return $users;
	}

	/*
	 * read uses updates
	 */

	// assigning users to devices
	public function get_users_updates($data) {

		$users = $this -> conn -> query("SELECT account_no,device_id FROM pro_devices WHERE pro_devices.device_imei='" . $data['device_imei'] . "'") or die(mysqli_error($this -> conn));

		if ($users) {
			$users = mysqli_fetch_assoc($users);

			$results = $this -> conn -> query("select * from pro_users where device_imei = '" . $users['device_id'] . "' and account_no = '" . $users['account_no'] . "' && last_update_time >'" . $data['last_sync'] . "'") or die(mysqli_error($this -> conn));
			$users = array();
			if ($results) {
				while ($row = mysqli_fetch_assoc($results)) {
					$users[] = $row;
				}
			} else {
				$users['failure'] = FALSE;
			}
		} else {
			$users = FALSE;
		}

		mysqli_close($con);
		return $users;
	}

	public function save_participant($data) {

		//check existance of data
		$results = $this -> conn -> query("select pro_users.account_no,assigned_to,pro_devices.device_imei,location from pro_devices  left join pro_users on pro_users.employee_id = pro_devices.assigned_to  where pro_devices.device_imei ='" . $this -> conn -> real_escape_string($data['imei']) . "'") or die(mysqli_error($this -> conn));

		if ($results) {
			//device information
			$row = mysqli_fetch_assoc($results);
			foreach ($data['Participant'] as $key => $value) {

				if ($this -> check_participant($value, $this -> conn -> real_escape_string($data['imei']))) {

					$participant = "INSERT INTO pro_participants(  survey_no, interview_date, location_code, participant_no,anc_number,pmtct_status, interviewer_no, device_imei, start_time, end_time)
	VALUES ( '" . $this -> conn -> real_escape_string($value['survey_no']) . "',
	'" . $this -> conn -> real_escape_string($value['interview_date']) . "',
	'" . $this -> conn -> real_escape_string($row['location']) . "','" . $this -> conn -> real_escape_string($value['participant_no']) . "',
	'" . $this -> conn -> real_escape_string($value['anc']) . "','" . $this -> conn -> real_escape_string($value['status']) . "' ,
	'" . $this -> conn -> real_escape_string($value['interviewer_no']) . "','" . $this -> conn -> real_escape_string($this -> conn -> real_escape_string($data['imei'])) . "',
	'" . $this -> conn -> real_escape_string($value['start_time']) . "','" . $this -> conn -> real_escape_string($value['end_time']) . "')";

					$query = $this -> conn -> query($participant) or die(mysqli_error($this -> conn));

				}

			}
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/*
	 * updating participant
	 */

	public function update_participant($data) {

		//check existance of data

		$qry = "select * from pro_participants where participant_no ='" . $data['participant_no'] . "' and device_imei='" . $this -> conn -> real_escape_string($data['imei']) . "'";

		$result = $this -> conn -> query($qry) or die(mysqli_error($this -> conn));
		if ($result) {

			$row = mysqli_fetch_assoc($results);

			$participant = "UPDATE pro_participants SET  anc_number = '" . $this -> conn -> real_escape_string($value['anc']) . "', pmtct_status = '" . $this -> conn -> real_escape_string($value['status']) . "' ,  location_code='" . $row['location'] . "', participant_no = '" . $this -> conn -> real_escape_string($data['participant_no']) . "', interviewer_no='" . $this -> conn -> real_escape_string($data['interviewer_no']) . "' WHERE participant_id ='" . $row['participant_id'] . "'";

			$query = $this -> conn -> query($participant) or die(mysqli_error($this -> conn));

			if ($query) {
				return TRUE;
			} else {
				return FALSE;
			}

		} else {
			return FALSE;
		}
	}

	public function check_participant($data, $imei) {

		$qry = "select * from pro_participants where participant_no ='" . $data['participant_no'] . "' and device_imei='" . $imei . "'";

		$result = $this -> conn -> query($qry) or die(mysqli_error($this -> conn));
		if (mysqli_num_rows($result)) {

			return FALSE;
		} else {

			return TRUE;
		}

	}

	public function check_responses($data, $imei) {

		$qry = "select * from pro_responses where response_qn = '" . $data['question_no'] . "' and participant_no ='" . $data['participant_id'] . "' and pro_responses.device_imei='" . $imei . "'";

		$result = $this -> conn -> query($qry) or die(mysqli_error($this -> conn));

		if (mysqli_num_rows($result)) {

			return FALSE;
		} else {

			return TRUE;
		}

	}

	//reading question options
	public function quetion_options($data) {
		//get question type

		//echo "select question_id,question_type  from pro_questionaires where question_rank = '" . $data['question_no'] . "' and  survey_no = '" . $data['survey_no'] . "' and account_no = '" . $data['account_no'] . "'";
		$qn = "select question_id,question_type  from pro_questionaires where question_rank = '" . $data['question_no'] . "' and  survey_no = '" . $data['survey_no'] . "' and account_no = '" . $data['account_no'] . "'";

		$question = $this -> conn -> query($qn) or die(mysqli_error($this -> conn));

		if (mysqli_num_rows($question)) {
			$qn = mysqli_fetch_assoc($question);
			
			if ($qn['question_type'] == 'Closed_ended_mult' || $qn['question_type'] == 'Partial_open_ended_mult' || $qn['question_type'] == 'Partial_open_ended' || $qn['question_type'] == 'Open_ended') {

				$options = "select * from pro_response_choices where question_no = '" . $qn['question_id'] . "' and account_no = '" . $data['account_no'] . "'";

				$options = $this -> conn -> query($options) or die(mysqli_error($this -> conn));

				$opts = array();
				if (mysqli_num_rows($options)) {

					while ($row = mysqli_fetch_assoc($options)) {
						$opts[] = $row;
					}
				}

				if ($qn['question_type'] == 'Partial_open_ended') {
					$opts['partial'] = TRUE;
				} elseif ($qn['question_type'] == 'Open_ended') {$opts['open_ended'] = TRUE;
				}

				return $opts;
			} else {
				return FALSE;
			}
		} else {

			return FALSE;
		}

	}

	public function my_final_answers($data) {

		$opts = $this -> quetion_options(array('question_no' => $data['question_no'], 'survey_no' => $data['survey_no'], 'account_no' => $data['account_no']));
		 
		 
		if ($opts) {
			$mychoices = explode('||', rtrim($data['choice_no'], '||'));
			$mychoice2 = explode('||', rtrim($data['response'], '||'));
			 
			if (array_key_exists('partial', $opts)) {

				unset($opts['partial']);
			 
				//check which data is returned
				foreach ($opts as $choice) {

					if ($choice['data_type'] == 3) {
						if ($mychoices[0] == $choice['choice_no']) {

							$answr .= $mychoice2[0] . '|';
						} else {
							$answr .= '0000-00-00|';
						}

					} elseif ($choice['data_type'] == 2) {
						if ($mychoices[0] == $choice['choice_no']) {
							$answr .= $mychoice2[0] . '|';
						} else {
							$answr .= '0|';
						}
					} else {
						if ($mychoices[0] == $choice['choice_no']) {
							$answr .= $mychoices[0] . '|';
						} else {
							$answr .= '0|';
						}
					}
				}

			} elseif (array_key_exists('open_ended', $opts)) {
				unset($opts['open_ended']);
				$ch = 0;
				foreach ($opts as $choice) {

					if ($choice['data_type'] == 3) {
						if (array_key_exists($ch, $mychoice2)) {
							if (strpos('-', $mychoice2[$ch])) {

								$answr .= $mychoice2[$ch] . '|';
							} else {
								$answr .= '0000-00-00|';
							}
						} else {
							$answr .= '0000-00-00|';
						}

					} elseif ($choice['data_type'] == 2) {
						if (array_key_exists($ch, $mychoice2)) {
							$answr .= $mychoice2[$ch] . '|';
						} else {
							$answr .= '0|';
						}
					} else {
					if (array_key_exists($ch, $mychoice2)) {
							$answr .= $mychoice2[$ch] . '|';
						} else {
							$answr .= '0|';
						}
					}
					$ch++;
				}

			} else {

				$answr = '';
				$d = 0;
				foreach ($opts as $choice) {
					if ($choice['data_type'] == 3) {
						$answr .= $mychoice2[$d] . '|';
					} elseif ($choice['data_type'] == 2) {
						end($mychoice2);
						$va = key($mychoice2);
						$answr .= $mychoice2[$va] . '|';

					} else {
						if (in_array($choice['choice_no'], $mychoices)) {
							$answr .= $choice['choice_no'] . '|';

						} else {

							$answr .= '0|';

						}
					}
					$d++;
				}
			}
			//$answr = rtrim($answr, '|');
		} else {

			if ($data['choice_no'] == 0) {
				$answr = $data['response'];
			} else {
				$answr = $data['choice_no'];
			}
			//$answr = rtrim($answr, '|');
		}
		$answr = rtrim($answr, '|');

		 
		return $answr;
	}

	public function save_responses($data) {//receive orders_details from devices

		//check existance of data
		$table = 'pro_responses';

		$results = $this -> conn -> query("select pro_users.account_no,assigned_to,pro_devices.device_imei,location from pro_devices  left join pro_users on pro_users.employee_id = pro_devices.assigned_to  where pro_devices.device_imei ='" . $this -> conn -> real_escape_string($data['imei']) . "'") or die(mysqli_error($this -> conn));
		if ($results) {
			$row = mysqli_fetch_assoc($results);

			foreach ($data['response'] as $key => $value) {

				if ($value['ack'] == 0) {

					if ($this -> check_responses($value, $this -> conn -> real_escape_string($data['imei']))) {

						//read questions options

						$value['account_no'] = $row['account_no'];
						$myanswer = $this -> my_final_answers($value);

						//get accounts details finished
						$save = "INSERT INTO pro_responses(participant_no,device_imei, survey_no, response_qn, response,response_no, location_code, response_date)
	VALUES ('" . $this -> conn -> real_escape_string($value['participant_id']) . "','" . $this -> conn -> real_escape_string($this -> conn -> real_escape_string($data['imei'])) . "',
	'" . $this -> conn -> real_escape_string($value['survey_no']) . "','" . $this -> conn -> real_escape_string($value['question_no']) . "',
	'" . $this -> conn -> real_escape_string($value['response']) . "','" . $this -> conn -> real_escape_string($myanswer) . "',
	'" . $this -> conn -> real_escape_string($row['location']) . "','" . $this -> conn -> real_escape_string($value['response_date']) . "')";
					 
					  $response = $this -> conn -> query($save) or die(mysqli_error($this -> conn));

					}
				} elseif ($value['ack'] == 2) {

					if ($this -> check_responses($value, $this -> conn -> real_escape_string($data['imei']))) {

						//read questions options

						$value['account_no'] = $row['account_no'];
						$myanswer = $this -> my_final_answers($value);

						//get accounts details finished
						$save = "INSERT INTO pro_responses(participant_no,device_imei, survey_no, response_qn, response,response_no, location_code, response_date)
	VALUES ('" . $this -> conn -> real_escape_string($value['participant_id']) . "','" . $this -> conn -> real_escape_string($this -> conn -> real_escape_string($data['imei'])) . "',
	'" . $this -> conn -> real_escape_string($value['survey_no']) . "','" . $this -> conn -> real_escape_string($value['question_no']) . "',
	'" . $this -> conn -> real_escape_string($value['response']) . "','" . $this -> conn -> real_escape_string($myanswer) . "',
	'" . $this -> conn -> real_escape_string($row['location']) . "','" . $this -> conn -> real_escape_string($value['response_date']) . "')";

						$response = $this -> conn -> query($save) or die(mysqli_error($this -> conn));

					} else {

						$qry = "select * from pro_responses where response_qn = '" . $value['question_no'] . "' and participant_no ='" . $value['participant_id'] . "' and pro_responses.device_imei='" . $this -> conn -> real_escape_string($data['imei']) . "'";

						$result = $this -> conn -> query($qry) or die(mysqli_error($this -> conn));

						if ($result) {

							$row = mysqli_fetch_assoc($result);

							//get accounts details
							//read questions options
							$value['account_no'] = $row['account_no'];
							$myanswer = $this -> my_final_answers($value);

							$qry = "UPDATE pro_responses SET response= '" . $this -> conn -> real_escape_string($value['response']) . "', response_no ='" . $this -> conn -> real_escape_string($myanswer) . "'  where response_id = '" . $row['response_id'] . "'";

							$this -> conn -> query($qry) or die(mysqli_error($this -> conn));

						} else {

							return FALSE;
						}
					}
				}	elseif ($value['ack'] == 3) {
				 
					foreach ($data['response'] as $deletevalue) {
						$delete = "DELETE FROM pro_responses WHERE participant_no ='" . $deletevalue['participant_id'] . "' and survey_no ='" . $deletevalue['survey_no'] . "' and response_qn ='" . $deletevalue['question_no'] . "' and device_imei ='" . $data['imei'] . "'";
					 
						$this -> conn -> query($delete) or die(mysqli_error($this -> conn));
					 
						/*if (mysqli_affected_rows($this -> conn) > 0) {
							return TRUE;
						} else {
							return FALSE;
						} */
					}

				}
			}
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update_responses($data) {//receive orders_details from devices

		//check existance of data

		$qry = "select * from pro_responses where response_qn = '" . $data['question_no'] . "' and participant_no ='" . $data['participant_id'] . "' and pro_responses.device_imei='" . $this -> conn -> real_escape_string($data['imei']) . "'";

		$result = $this -> conn -> query($qry) or die(mysqli_error($this -> conn));

		if ($result) {
			$row = mysqli_fetch_assoc($result);
			//get accounts details

			$response = $this -> conn -> query("UPDATE pro_responses SET response= '" . $this -> conn -> real_escape_string($value['response']) . "', response_no ='" . $this -> conn -> real_escape_string($value['choice_no']) . "'  where response_id = '" . $row['response_id'] . "'");

			if ($response) {

				return TRUE;
			} else {
				return FALSE;
			}

		} else {
			return FALSE;
		}

	}

	//read surveys
	public function read_surveys($data) {//receive order details from devices
		//get surveys details

		$results = $this -> conn -> query("select account_no from pro_devices where device_imei = '" . $this -> conn -> real_escape_string($data['imei']) . "'") or die(mysqli_error($this -> conn));

		if ($results) {

			$acc = mysqli_fetch_assoc($results);
			//get accounts details

			$results = $this -> conn -> query("select * from pro_surveys where account_no = '" . $acc['account_no'] . "'") or die(mysqli_error($this -> conn));

			if ($results) {

				$survyes = array();
				while ($row = mysqli_fetch_assoc($results)) {
					$survyes[] = $row;
				}

				return $survyes;
			} else {
				return FALSE;
			}
		} else {
			return False;
		}

	}

	/*
	 * get surveys updates
	 */

	//read settings
	public function get_survey_updates($data) {//receive order details from devices
		//get surveys details
		$results = $this -> conn -> query("select account_no from pro_devices where device_imei = '" . $data['device_imei'] . "'") or die(mysqli_error($this -> conn));

		if ($results) {
			$acc = mysqli_fetch_assoc($results);
			//get accounts details
			$results = $this -> conn -> query("select * from pro_surveys where account_no = '" . $acc['account_no'] . "' && last_update_time >'" . $data['last_sync'] . "'") or die(mysqli_error($this -> conn));

			if ($results) {
				$survyes = array();
				while ($row = mysqli_fetch_assoc($results)) {
					$survyes[] = $row;
				}
				return $survyes;
			} else {
				return FALSE;
			}
		} else {
			return False;
		}

	}

	//read settings
	public function read_settings($data) {//receive order details from devices
		//get accounts details

		$results = $this -> conn -> query("select account_no from pro_devices where device_imei = '" . $this -> conn -> real_escape_string($data['imei']) . "'") or die(mysqli_error($this -> conn));

		if ($results) {

			$acc = mysqli_fetch_assoc($results);

			//get accounts details

			$results = $this -> conn -> query("select * from pro_settings where account_no = '" . $acc['account_no'] . "'") or die(mysqli_error($this -> conn));

			if ($results) {
				$row = mysqli_fetch_assoc($results);
				return $row;
			} else {
				return FALSE;
			}
		} else {
			return False;
		}

	}

	//read settings
	public function settings_updates($data) {//receive order details from devices
		//get accounts details

		$results = $this -> conn -> query("select account_no from pro_devices where device_imei = '" . $this -> conn -> real_escape_string($data['imei']) . "'") or die(mysqli_error($this -> conn));

		if ($results) {

			$acc = mysqli_fetch_assoc($results);

			//get accounts details

			$results = $this -> conn -> query("select * from pro_settings where account_no = '" . $acc['account_no'] . "'   && last_update_time >'" . $data['last_sync'] . "'") or die(mysqli_error($this -> conn));

			if ($results) {
				$row = mysqli_fetch_assoc($results);
				return $row;
			} else {
				return FALSE;
			}
		} else {
			return False;
		}

	}

	public function unset_device_sync_status($data, $item) {//receive orders_details from devices

		//check existance of data

		$qry = "UPDATE pro_device_sync_status SET " . $item . " = 0  WHERE device_imei='" . $this -> conn -> real_escape_string($data['imei']) . "'";

		$result = $this -> conn -> query($qry) or die(mysqli_error($this -> conn));

		if ($result) {

			return TRUE;

		} else {
			return FALSE;
		}

	}

	public function update_device_sync_status($data) {//receive orders_details from devices

		//check existance of data

		$qry = "UPDATE pro_device_sync_status SET " . $data['item'] . " = 1 WHERE device_imei='" . $this -> conn -> real_escape_string($data['imei']) . "'";
		 
		$result = $this -> conn -> query($qry) or die(mysqli_error($this -> conn));

		if ($result) {

			return TRUE;

		} else {
			return FALSE;
		}

	}

	/*function cleanQuery($string) {
	 if (get_magic_quotes_gpc())// prevents duplicate backslashes
	 {
	 $string = stripslashes($string);
	 }
	 if (phpversion() >= '4.3.0') {
	 $string = mysqli_$this->conn->real_escape_string($string);
	 } else {
	 $string = mysqli_escape_string($string);
	 }
	 return $string;
	 }
	 *

	 /*
	 * unseting is sync
	 */

	public function un_sync() {

		//uploading customer details to mobile device

		file_put_contents("received_data.txt", date('Y-m-d h:i:s'));

	}

}
