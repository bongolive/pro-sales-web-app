<?php
class Location_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_locationById($locationid = FALSE)
	{
		if ($locationid === FALSE)
		{
			$query = $this->db->get('location');
			return $query->result_array();
		}
	
		$query = $this->db->get_where('location', array('id' => $locationid));
		return $query->row_array();
	}
	
	public function get_locationByIMEI($imei = FALSE)
	{
		if ($imei === FALSE)
		{
			$query = $this->db->get('location');
			return $query->result_array();
		}
	
		$query = $this->db->get_where('location', array('imei' => $imei));
		return $query->row_array();
	}
	
	public function add_location($data)
	{
		return $this->db->insert('location', $data);
	}
	
	public function update_location($data,$locationid)
	{
		$this->db->where('id',$locationid);
		return $this->db->update('location', $data);
	}
	
	
	public function get_deviceLocation($phone_imei,$interval_start,$interval_end)
	{
		$query = $this->db->query("SELECT id, imei, date, latitude, longitude FROM location WHERE imei = '".$phone_imei."' AND date BETWEEN '".$interval_start."' AND '".$interval_end."'");
		return $query->row_array();
	}

    public function _api_save_tracking($data,$where){
        $flag = array();
        foreach($data as $value){
            $this->db->trans_start();
            $arrayvalues = array(
                'latitude' => $value['lat'], 'longitude' => $value['lng'], 'date' => $value['date'],
                'servertime' => date('Y-m-d h:i:s'), 'imei' => $where['device_imei']
            );
            $insert = $this->db->insert('pro_location',$arrayvalues);
            if($insert){
                $flag['success'] = 1;
            }
            $this->db->trans_complete();
        }
        return $flag;
    }

}
