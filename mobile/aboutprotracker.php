<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
<link rel="stylesheet" href="bx_styles.css" type="text/css" />
<link rel="stylesheet" href="media-quries-resposive.css" type="text/css"  />
<title>footprint</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

<link href="facebox.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="js/facebox.js" type="text/javascript"></script> 
<script type="text/javascript">
  $(document).ready(function(){
     $('a[rel*=facebox]').facebox();
	 
	
  });
</script>

</head>

<body>
	<div class="wrapper-center">
    <div class="mobile_wrapper">
	<div class="body_class">
    	<div class="center_header_page">
        	<div class="header-left">
            	<a href="index.php" ><img src="images/logo3.png" alt=""  /></a>
            </div>
            
        </div>
	</div>
    <div class="line">
    </div>
    <div class="center_header_page">
      <div class="content" >
      <div class="content_first about_tracker_page"> <p> proTracker is a new and powerful monitoring solution that helps you manage what your employees are doing and track their location in real time. It is a solution that tracks the location of a mobile phone anywhere in the world whilst allowing you to easily manage your workforce from the comfort of your home or office.</p> 

<p>It is also an ideal solution for parents who are worried about their children's safety and would like to track the location of their children at any time, any where in the world.</p>

<p>Read more below about how it can help your company or family.</p>
</div><br />
<div style="color:#d3954c; font-size:16px; padding-left: 1%; text-decoration:underline">
<h3>Features</h3></div>
<ul style="float:left; color:#8F8E8E; font-size:13px; padding-left: 4%;padding-right: 6%;
    width: 93%;"><li>View the current location of your employee or child in realtime.</li>
<li>You can monitor multiple individuals from one account with ease.</li>
<li>We store historical location records for upto three months for your reference.</li>
<li>You can set boundaries that limit how far an individual can travel & get alerts if they step outside it.</li>
<li>We display clear markers on a map for you to track the route taken.</li>
<li>Get email notifications when a destination is reached or a boundary is crossed.</li>
<li>Our service is invisible on the phone. The individual will not know that the phone is being monitored.</li>

</ul>

            
      </div>
   
</div><div class="clear"></div> <br /><br />
    <div class="footer-wrapper">
    	<div class="center_footer_page">
        	<div class="footer_center">
            	<div class="footer_navi">
            	<a href="index.php" >Home</a>
                <a href="aboutprotracker.php" >About Pro Tracker</a>
                <a href="support.php" >Contact us</a>
                <a href="pro.co.tz" >Full Site</a>
               
                </div>
            </div>
         <div class="clear"></div>
        </div>
    </div>
    <div class="center_header_page">
    	<div class="footer_buttom">
    	<h3>FOOT PRINT</h3><p>Copyrihgt &copy; 2012. All Rights Reserved.</p>
        </div>
         <div class="clear"></div>
    </div>
   </div>
  </div>
	
<script type="text/javascript">


function postForm(){
    
        var str = $("#login_form").serialize();
        $.ajax({
		type : 'POST',
		url : 'checklogin.php?XDEBUG_SESSION_START=dk',
		data : str,
		dataType: 'json',
		success : function(data){	

                if(data.status == "1") {
                    document.location = "device.php";
                } else {
					alert("Please enter correct username and password");
					$("#username").val("");
					$("#txt_password").val("");
                }

			}
  
		}); 
        return(false);
    
}
</script>
<script src="js/analytics.js" type="text/javascript"></script>
</body>
</html>
