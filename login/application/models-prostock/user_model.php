<?php
class User_model extends My_Model {
	public $table = 'users';
	public $table_id = 'user_id';
	public function __construct() {
		$this -> load -> database();
	}

	public function get_userlist($userid = FALSE) {
		if ($userid === FALSE) {
			$query = $this -> db -> get('users');
			return $query -> result_array();
		}

		$query = $this -> db -> get_where('users', array('user_id' => $userid));
		return $query -> row_array();
	}

	public function getUserByUsernameAndPassword($where) {
		$this -> db -> select('*');
		$this -> db -> from('users');
		$this -> db -> where($where);
		$this -> db -> join('accounts', 'accounts.account_id = users.account_id', 'left');
		$query = $this -> db -> get() -> result_array();
		return $query;
	}

	public function get_user($cond) {
		$query = $this -> db -> get_where('users', $cond);
		return $query -> row_array();
	}

	public function add_user($data) {
		return $this -> db -> insert('users', $data);
	}
/*
	public function update($data, $cond) {
		$this -> db -> where($cond);
		return $this -> db -> update('users', $data);
	}*/

	public function update_user($data, $userid) {
		$this -> db -> where('userid', $userid);
		return $this -> db -> update('users', $data);
	}

	public function delete($userid) {
		$this -> db -> delete('users', array('userid' => $userid));
		$this -> db -> delete('phones_users', array('userid' => $userid));
	}

	public function logout() {
		$this -> session -> sess_destroy();
	}

	public function loggedin() {
		return (bool)$this -> session -> userdata('logged_in');
	}

	public function getUserPermissions($cond) {
		$query = $this -> db -> get_where('permissions', $cond);
        $row = $query->row();
		return $row->permissions;
	}

	public function deleteUserPermissions($cond) {
		return $this -> db -> delete('user_permissions', $cond);
	}

	public function addUserPermission($data) {
		return $this -> db -> insert('user_permissions', $data);
	}

    public function check_user($where){
        $this->table = 'users';

        $this->db->where($where);
        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

}
