<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Dashboard extends My_Controller {

	public $data = array();

	public function __construct() {

		parent::__construct();

		if (!$this -> session -> userdata('is_login') == TRUE) {
			$this -> session -> set_userdata('login_error', 'Please, Loggin first');
			 redirect('login');
		}
		$this -> load -> model('dashboard_model');
		$this -> load -> model('surveys_model');
		$this -> load -> model('locations_model');
		$this -> load -> model('reports_model');
		$this -> load -> model('questionaires_model');

		$this -> where = array('account_id' => $this -> session -> userdata('account_id'));
		$this -> data['controller'] = 'Dashboard';
	}

	public function index() {
 


		$intervals = array('today', 'week', 'month', 'total');
		$total = array();
		foreach ($intervals as $interval) {

			if ($interval == 'today') {
				$where = array('participants.interview_date' => date('Y-m-d'));
			} elseif ($interval == 'week') {
				$date = date('Y-m-d', strtotime(date('Y-m-d')) - (24 * 3600 * 7));
				$where = array('participants.interview_date >=' => $date,'participants.interview_date <=' => date('Y-m-d'));
			} elseif ($interval == 'month') {
				$date = date('Y-m-d', strtotime(date('Y-m-d')) - (24 * 3600 * 30));
				$where = array('participants.interview_date >=' => $date,'participants.interview_date <=' => date('Y-m-d'));
			} elseif ($interval == 'total') {
				$where = FALSE;
			}

			$totals = $this -> dashboard_model -> count_participants($where);
			if ($totals) {
				$total[$interval] = $totals[0]['total'];
			}
		}
		if ($total) {
			$this -> data['totals'] = $total;
		} else {
			$this -> data['totals'] = FALSE;
		}

		//for the area chart, the daily participants responses
		$pastdate = date('Y-m-d', strtotime('-7 days'));
		$where = array('interview_date >=' => date('Y-m-d'), 'interview_date >=' => $pastdate);

		$participants = $this -> dashboard_model -> participants_trend($where);
		if ($participants) {
 
			$this -> data['graph_data'] = $participants;
		} else {
			$this -> data['graph_data'] = FALSE;
		}

		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/dashboard');
		$this -> load -> view('template/footer');
		$this -> load -> view('template/makegraph');
	}

	public function location() {

		$fac = array('location_code' => $this -> session -> userdata['location']);
		$facilities = $this -> locations_model -> read($fac);
		if ($facilities) {
			foreach ($facilities as $key => $value) {
				$this -> data['location'] = $value;
			}

			$where = array('survey_no' => $this -> settings['active_survey']);
			$this -> load -> model('questionaires_model');
			$questions = $this -> questionaires_model -> read_questions($where, FALSE, FALSE);
			if ($questions) {
				$this -> data['tb_data'] = $questions;
				$responders = $this -> reports_model -> responders_counter(array('location_no' => $this -> session -> userdata['location'], 'responce_date >=' => date('Y-m-d')));
				$this -> data['responders'] = $responders['total'];
				//get reponces
				$this -> load -> model('reports_model');

				$where = array('location_code' => $this -> session -> userdata['location']);

				$responces = $this -> reports_model -> read_responces($where);
				if ($responces) {

					$respos = $this -> reports_model -> count_responces($where);

					$this -> data['respos'] = $respos;

					$this -> data['responces'] = $responces;

					$this -> data['fac_score'] = $this -> location_score($this -> session -> userdata['location'], $this -> settings['active_survey']);
					$this -> data['qn_scores'] = $this -> responces();

				} else {
					$this -> data['responces'] = FALSE;
				}
			} else {
				$this -> data['tb_data'] = FALSE;
			}

			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/location_dashboard');
			$this -> load -> view('template/footer');

		} else {
			redirect('login', 'refresh');
		}

	}

	public function location_score($facilty, $survey) {

		$where = array('survey_no' => $survey);
		$questions = $this -> questionaires_model -> read_questions($where, FALSE, FALSE);
		$where = array('location_code' => $facilty);
		$responces = $this -> reports_model -> read_responces($where);

		//calculate the location score
		$syes = $sno = $sdwi = $av_score = 0;

		if ($responces) {
			foreach ($questions as $qnscores) {
				//echo "<pre>";
				if ($qnscores['rank'] == 9) {

				} else {
					$qscore = explode(',', $qnscores['scores']);
					//total responce
					$p = 0;
					foreach ($responces as $fscorez) {

						//match questions

						if ($qnscores['rank'] == $fscorez['responce_qn']) {
							$p++;
							// $qn[$quns['question_id']] = e
							$jibu = explode(',', $fscorez['responce']);

							if ($jibu[0] != '99') {
								$syes = $syes + $qscore[0];

							} elseif ($jibu[1] != 99) { $sno = 0;

							} elseif ($jibu[2] != '99') {
								$sdwi = $sdwi + 0.5;

							} else {

							}
						}
					}

				}
				$total_score = $syes + $sdwi;

				$qn_av_score = $total_score / $p;

				$av_score = $av_score + $qn_av_score;
				$syes = $sno = $sdwi = 0;
			}
			return $av_score;
		} else {
			return 0;

		}

	}

	public function averall_location_score($survey) {

		$where = array('survey_no' => $survey);
		$questions = $this -> questionaires_model -> read_questions($where, FALSE, FALSE);
		$facilities = $this -> locations_model -> read();
		if ($facilities) {
			$all_fac_scores = array();
			foreach ($facilities as $key => $value) {
				$where = array('location_code' => $value['location_code']);
				$responces = $this -> reports_model -> read_responces($where);

				//calculate the location score
				$syes = $sno = $sdwi = $av_score = 0;

				if ($responces) {
					foreach ($questions as $qnscores) {
						//echo "<pre>";
						if ($qnscores['rank'] == 9) {

						} else {
							$qscore = explode(',', $qnscores['scores']);
							//total responce
							$p = 0;
							foreach ($responces as $fscorez) {

								//match questions

								if ($qnscores['rank'] == $fscorez['responce_qn']) {
									$p++;
									// $qn[$quns['question_id']] = e
									$jibu = explode(',', $fscorez['responce']);

									if ($jibu[0] != '99') {
										$syes = $syes + $qscore[0];

									} elseif ($jibu[1] != 99) { $sno = 0;

									}
									if (count($jibu) > 2) {
										if ($jibu[2] != '99') {
											$sdwi = $sdwi + 0.5;
										}
									} else {

									}
								}
							}

						}
						$total_score = $syes + $sdwi;
						if ($total_score > 0) {
							$qn_av_score = $total_score / $p;
						} else {
							$qn_av_score = 0;
						}
						$av_score = $av_score + $qn_av_score;
						$syes = $sno = $sdwi = 0;
					}
					$fscore = $av_score;
				} else {
					$fscore = 0;

				}
				$all_fac_scores[$value['location_code']] = $fscore;
			}
			return $all_fac_scores;
		} else {
			return FALSE;

		}
	}

	public function location_score_per_qn($survey) {

		//get the location
		//$fac = array('location_code' => $this -> session -> userdata['location']);
		$facilities = $this -> locations_model -> read();

		//get questions
		$where = array('survey_no' => $survey);
		$questions = $this -> questionaires_model -> read_questions($where, FALSE, FALSE);
		$fac_score = array();
		if ($facilities) {

			foreach ($facilities as $key => $value) {

				$where = array('location_code' => $value['location_code']);
				$responces = $this -> reports_model -> read_responces($where);

				//calculate the location score
				$syes = $sno = $sdwi = $av_score = 0;

				if ($questions) {

					foreach ($questions as $qnscores) {
						//echo "<pre>";
						if ($qnscores['rank'] == 9) {

						} else {
							if ($responces) {
								$qscore = explode(',', $qnscores['scores']);
								//total responce
								$p = 0;
								foreach ($responces as $fscorez) {

									//match questions

									if ($qnscores['rank'] == $fscorez['responce_qn'] && $qnscores['rank'] != 9) {
										$p++;
										// $qn[$quns['question_id']] = e
										$jibu = explode(',', $fscorez['responce']);

										if ($jibu[0] != '99') {
											$syes = $syes + $qscore[0];

										} elseif ($jibu[1] != 99) { $sno = 0;

										} elseif (count($jibu) > 2) {
											if ($jibu[2] != '99') {
												$sdwi = $sdwi + 0.5;
											} else {

											}
										} else {

										}
									}
								}
								$total_score = $syes + $sdwi;
								if ($total_score > 0) {
									$qn_av_score = $total_score / $p;
								} else {
									$qn_av_score = 0;
								}
								$av_score = $av_score + $qn_av_score;
								//set the location quetions score values
								$qscores[$qnscores['rank']] = $qn_av_score;
								$syes = $sno = $sdwi = 0;
							} else {
								//set the location quetions score values
								$qscores[$qnscores['rank']] = 0;
							}

						}
					}
				} else {
					$qscores = 0;
				}
				$fac_score[$value['location_code']] = $qscores;
			}
			return $fac_score;
		} else {
			return FALSE;
		}

	}

	public function responces() {

		$where = array('survey_no' => $this -> settings['active_survey']);
		$questions = $this -> questionaires_model -> read_questions($where, FALSE, FALSE);
		if ($questions) {
			$where = array('location_code' => $this -> session -> userdata['location']);

			$responces = $this -> reports_model -> read_responces($where);
			$results = array();
			foreach ($questions as $key => $value) {
				$choises = explode(',', $value['answer_choices']);

				if ($responces) {
					$yes = $no = $dwi = 0;
					foreach ($responces as $respo) {
						//match questions

						if ($value['rank'] == $respo['responce_qn'] && $value['qn_type'] != 'open_ended') {
							// $qn[$quns['question_id']] = e
							$jibu = explode(',', $respo['responce']);
							if ($jibu[0] != 99) { $yes = $yes + 1;
							} elseif ($jibu[1] != 99) { $no = $no + 1;
							} elseif ($jibu[2] != 99) { $dwi = $dwi + 1;
							}
						}
					}
				}
				//data by questions
				if ($choises) {
					$cc = count($choises);
					if ($cc > 1) {
						if ($cc <= 2) {
							$results[$value['rank']] = array($choises[0] => $yes, $choises[1] => $no);
						} else {
							$results[$value['rank']] = array('qn' => $value['rank'], $choises[0] => $yes, $choises[1] => $no, $choises[2] => $dwi);
						}
					}
				}
			}
			return $results;

		}

	}

	public function overall_responces() {

		$where = array('survey_no' => $this -> settings['active_survey']);
		$questions = $this -> questionaires_model -> read_questions($where, FALSE, FALSE);
		if ($questions) {
			$where = FALSE;

			//array('location_code' => $this -> session -> userdata['location']);

			$responces = $this -> reports_model -> read_responces($where);
			$results = array();
			foreach ($questions as $key => $value) {
				if ($value['rank'] != 9) {
					$choises = explode(',', $value['answer_choices']);

					if ($responces) {
						$yes = $no = $dwi = 0;
						foreach ($responces as $respo) {
							//match questions

							if ($value['rank'] == $respo['responce_qn'] && $value['qn_type'] != 'open_ended') {
								// $qn[$quns['question_id']] = e
								$jibu = explode(',', $respo['responce']);
								if ($jibu[0] != 99) { $yes = $yes + 1;
								} elseif ($jibu[1] != 99) { $no = $no + 1;
								}
								if (count($jibu) > 2) {
									if ($jibu[2] != 99) { $dwi = $dwi + 1;
									}
								}
							}
						}
					}
					//data by questions
					if ($choises) {
						$cc = count($choises);
						if ($cc > 1) {
							if ($cc <= 2) {
								$results[$value['rank']] = array($choises[0] => $yes, $choises[1] => $no);
							} else {
								$results[$value['rank']] = array('qn' => $value['rank'], $choises[0] => $yes, $choises[1] => $no, $choises[2] => $dwi);
							}
						}
					}
				}
			}
			//echo "<pre>";
			//print_r($results);
			return $results;
		}
	}

}
