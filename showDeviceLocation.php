<?php 
session_start();
include('common.php');

$all_values_set = true;

$imei = "";
$device_name = 	"";
$from = "";
$to = "";
$lat = "";
$lng = "";


if(!isset($_REQUEST["imei"]) || !isset($_REQUEST["device"]) || !isset($_REQUEST["from"]) || !isset($_REQUEST["to"]) || !isset($_REQUEST["lat"]) || !isset($_REQUEST["lng"])){
	$all_values_set = false;
}else{
	
	$imei = $_REQUEST["imei"];
	$device_name = 	$_REQUEST["device"];
	$from = $_REQUEST["from"];
	$to = $_REQUEST["to"];
	$lat = $_REQUEST["lat"];
	$lng = $_REQUEST["lng"];	
	
}


?>

  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
  <link rel="stylesheet" href="bx_styles.css" type="text/css" />
  <title>footprint</title>
<!--  <script src="http://code.jquery.com/jquery-latest.js" type="text/javascript	"></script>
  
  <script src="js/jquery.bxSlider.js" type="text/javascript"></script>
  <script src="js/jquery.bxSlider.min.js" type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function(){
        
	   $('.slider').bxSlider({
		  auto: true,
		  pager: true
		});
    });
  </script>-->
  
  
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
var map;
var openedInfoWindow = null;

var imei = "<?php echo $imei; ?>";
var device_name = "<?php echo $device_name; ?>";
var from = "<?php echo $from; ?>";
var to = "<?php echo $to; ?>";
var lat = <?php echo $lat; ?>;
var lng = <?php echo $lng; ?>;

function initializeMap() {
  
var centerPoint = new google.maps.LatLng(lat, lng);

var myOptions = {
	zoom: 14,
	center: centerPoint,
	mapTypeId: google.maps.MapTypeId.ROADMAP
}

map = new google.maps.Map(document.getElementById("div_loc_map"), myOptions);

var content = "<strong>IMEI : </strong>"+ imei +"<br>";
content += "<strong>Device Name : </strong>"+ device_name +"<br>";
content += "<strong>From : </strong>"+ from +"<br>";
content += "<strong>To : </strong>"+ to +"<br>";

var latlngpoint = new google.maps.LatLng( lat, lng);

setMarker(map, latlngpoint, content);
}

function setMarker(map, latlng, content){
	
	var infowindow = new google.maps.InfoWindow({
		content: content
	});
	
	var marker = new google.maps.Marker({
		position: latlng,
		map: map
	});
	
	
	google.maps.event.addListener(marker, 'mouseover', function() {
		if (openedInfoWindow != null) {
			openedInfoWindow.close();
		}
		infowindow.open(map,marker);
		
		openedInfoWindow = infowindow;
		google.maps.event.addListener(infowindow, 'closeclick', function() {
			openedInfoWindow = null;
		});
	});
	
	
}






</script>
  
  
  </head>
  
  <body <?php if($all_values_set){echo 'onload="initializeMap();"';} ?>>
  <?php //session_start();
  include ("connection.php");
  //include('common.php');
  global $user;
		  $sql = "select * from users where username = '$_SESSION[username]'";
		  if (!mysql_query($sql,$con))
		  {
			  die('Error: ' . mysql_error());
		  }
		  $rows = mysql_query($sql, $con);
		  if(mysql_num_rows($rows) > 0) {	
		  while($row = mysql_fetch_array($rows))
			  {		
			  $user = $row;
			  }
		  }
  
  ?>
	  <div class="body_class">
	  <div class="center_header_page">
		  <div class="header-left">
		 <a href="index.php" ><img src="images/logo3.png" alt=""  /></a>
	      </div><div class="header-right">
		  <span style="font-family:Arial; font-size:14px; color:#FFFFFF; margin-top:8px; float:left">Welcome <?php echo $_SESSION['username'];?><br /><a href="logout.php" style="color:#FFFFFF">Logout</a></span>
	      </div>
	      
	  </div>
	  </div>
	  <div class="center_header_pageinner">
	  <div class="menu_wrapperinner">
		
	      <div class="menu_firstinner">
		  
		  <div class=" menu_contentinner">
			  <a href="device.php" >Device</a>
		  </div>
	      </div>
	      <div class="menu_firstinner">
		  
		  <div class="menu_contentinner">
			  <a href="track.php" >Track</a>
		  </div>
	      </div>
		    <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="task.php" >Tasks</a>
                </div>
            </div>
			  <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="report.php" >Reports</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                		<a href="boundary.php" >Boundaries</a>
                </div>
            </div>
	      <div class="menu_firstinner">
		  
		  <div class="menu_contentinner">
			  <a href="" >Profile</a>
		  </div>
	      </div>
	      <div class="menu_firstinner">
		  
		  <div class="menu_contentinner">
			 <a href="support.php" >Support</a>
		  </div>
	      </div>
	      <div class="clear"></div>
	  </div>
      
      </div>
      <div class="line">
      </div>
      <div class="center_header_page">
	<div class="content" >
	  
	      <div class="content_first content_sec_heading">
		  
		  
  
	   
	  <div class="content_thirdlog"><!--content_third-->
	    <div class="clear">
        	
        	
            
            
            
            <div id="div_loc_map" class="content_first" style="width:800px; height:400px; border:3px solid; border-radius:10px; -moz-border-radius:10px; margin:0 auto;">
                    
            </div>
            
            
            
            
            
            
            
            
        </div>
			  
	   </div><!--content_third-->
	      </div>
	</div>
      
  </div>
      <div class="footer-wrapper">
	  <div class="center_footer_page">
		  <div class="footer_center">
		  <div class="footer_navi" style="display:none;">
		  <a href="" >Home</a>
		  <a href="" >Company</a>
		  <a href="" >Clients</a>
		  <a href="" >Resources</a>
		  <a href="" >Support</a>
		  <a href="" >Blog</a>
		  <a href="" >Contact</a>
		  </div>
	      </div>
	   <div class="clear"></div>
	  </div>
      </div>
      <div class="center_header_page">
	  <div class="footer_buttom">
	  <h3>FOOT PRINT</h3><p>Copyrihgt &copy; 2012. All Rights Reserved.</p>
	  </div>
	   <div class="clear"></div>
      </div>
	  
	  <script src="js/analytics.js" type="text/javascript"></script>
  </body>
  </html>
