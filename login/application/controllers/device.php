<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Device extends User_Controller {
	
    
    
	public function __construct()
	{
		parent::__construct();
		$this->load->model('devices_model');
		$this->session_userid = $this->session->userdata('userid');
        $this -> user_id = $this -> session -> userdata('userid');
        $this -> account_id = $this -> account_id;

        //$this->session_userid = '1';
		
	}
	
	
	public function index()
	{
$condUserPerm = array('userid'=>$this->session->userdata('userid'));
		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] ="";
		$header['device_tab'] ="";
		$header['tasks_tab'] ="";
		$header['reports_tab'] ="";
		$header['deliveries_tab'] ="";
		$header['customers_tab'] ="";
		$header['boundaries_tab'] ="";
		$header['support_tab'] ="allow";
		$header['pathfinder_tab'] ="";
		$header['stocks_tab'] ="";
		$header['registrations_tab'] ="";
		
		foreach($user_permissions as $user_permission){
			if($user_permission['permission_name']=='track tab'){
				$header['track_tab'] ="allow";
			}else if($user_permission['permission_name']=='device tab'){
				$header['device_tab'] ="allow";
			}if($user_permission['permission_name']=='tasks tab'){
				$header['tasks_tab'] ="allow";
			}if($user_permission['permission_name']=='reports tab'){
				$header['reports_tab'] ="allow";
			}if($user_permission['permission_name']=='deliveries tab'){
				$header['deliveries_tab'] ="allow";
			}if($user_permission['permission_name']=='customers tab'){
				$header['customers_tab'] ="allow";
			}if($user_permission['permission_name']=='boundaries tab'){
				$header['boundaries_tab'] ="allow";
			}if($user_permission['permission_name']=='pathfinder survey tab'){
				$header['pathfinder_tab'] ="allow";
			}if($user_permission['permission_name']=='stocks tab'){
				$header['stocks_tab'] ="allow";
			}if($user_permission['permission_name']=='registrations tab'){
				$header['registrations_tab'] ="allow";
			}						
		}
		
		if($header['device_tab'] !="allow"){
			redirect('profile/support');
		}
		
		$fields['tb_data'] = $this->devices_model->get_devicelistbyuserid($this->session_userid);
		
		
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js','custom/custom-datatable.js');
        $fields['tb_name'] = 'device_tb_name';
        // Put the fields from the database in this array
        $fields['row_fields'] = array('phone_imei','device_name','checking_interval','mobile_no','holder_name');
        $fields['tb_headers'] = array('device_imei','device_name','track_intv','mobile_nmber','holder_name','action');
        $header['dv_active'] = 'class="active"';
		$fields['add_btn'] = 'add_new_device';
        $fields['reset_btn'] = 'reset_device_sync_status';
        $this->load->view('template/header',$header);
        $this->load->view('template/content/table',$fields);
        $this->load->view('template/footer',$ft_data);
	}
	
	
	
	public function add()
	{		
			$imei = $this->input->post('device_imei');
			$device_name = $this->input->post('device_name');
			$mobile = formatMobileNumber($this->input->post('mobile'));
			$checking_interval = $this->input->post('track_intv');
			$holder_name = $this->input->post('holder_name');
			$email_report = $this->input->post('email_reports');
			
			$devicedata = array(
				'phone_imei' => $imei,
				'device_name' => $device_name,
				'checking_interval' => $checking_interval,
				'mobile_no' => $mobile,
				'holder_name' => $holder_name,
				'email_report' => $email_report
			);
			
			//Checking if user has this device on phones
			$user_divice  = $this->devices_model->get_deviceFromPhonesUsers($this->session_userid,$imei);
			$phonesdata		= $this->devices_model->get_devicelistbyImei($imei);
			
			//Now we check if device exist on
			if (count($user_divice) == 0 && count($phonesdata) == 0)
			{
				//No Divice in all tables so we insert it
				$this->devices_model->add_device($devicedata);
				
				$phonesdata		= $this->devices_model->get_devicelistbyImei($imei);
				
				//generating array for 
				$deviceusersdata = array(
					'userid' => $this->session_userid,
					'phoneid' => $phonesdata['phoneid'],
					'imei' => $imei
				);
				
				//No Divice in all tables so we insert it
				$this->devices_model->add_device_users($deviceusersdata);
				
				
			}else{
				
				if(count($user_divice)==0){
				
					$phonesdata		= $this->devices_model->get_devicelistbyImei($imei);
				
					//generating array for 
					$deviceusersdata = array(
						'userid' => $this->session_userid,
						'phoneid' => $phonesdata['phoneid'],
						'imei' => $imei
					);
					$this->devices_model->add_device_users($deviceusersdata);
				}
				
				
				
				//updating record since it existing
				$this->devices_model->update_diviceByImei($devicedata,$imei);
			}
			
			redirect('device');
	}
	
	
	
	
	public function edit($id)
	{
		$data['devices'] = $this->devices_model->get_devicelistbyid($id);
		$this->load->view('template/content/edit_device',$data);
	}
	
	
	
	public function update()
	{
			$imei = $this->input->post('device_imei');
			$device_name = $this->input->post('device_name');
			$mobile = formatMobileNumber($this->input->post('mobile'));
			$checking_interval = $this->input->post('track_intv');
			$holder_name = $this->input->post('holder_name');
			$email_report = $this->input->post('email_reports');
			$phoneid = $this->input->post('deviceid');
			
			
			$devicedata = array(
				'phone_imei' => $imei,
				'device_name' => $device_name,
				'checking_interval' => $checking_interval,
				'mobile_no' => $mobile,
				'holder_name' => $holder_name,
				'email_report' => $email_report
			);
			
			$row = $this->devices_model->get_devicelistbyImei($imei);
		
			$int = $row['checking_interval'];
			$id = $row['phoneid'];
			$regid = $row['regid'];
			
			
			//updating record since it existing
			$this->devices_model->update($devicedata,$phoneid);
			
			
			if($int != $checking_interval){
					
					$rownew = $this->devices_model->get_devicelistbyImei($imei);
					
					$apiKey = "AIzaSyAfOtZpNks_9J9jZfGU0V3OyDZoeseyfBY";

					$registrationIDs[] = $rownew['regid']; 

					// Message to be sent
					$message = $rownew['checking_interval'];

					// Set POST variables
					$url = 'https://android.googleapis.com/gcm/send';

					$fields = array(
						'registration_ids'  => $registrationIDs,
						'data'              => array( "message" => $message),
						);
					//print_r(json_encode($fields));
					$headers = array( 
							'Authorization: key=' . $apiKey,
							'Content-Type: application/json'
						);

					// Open connection
					$ch = curl_init();

					// Set the url, number of POST vars, POST data
					curl_setopt( $ch, CURLOPT_URL, $url );

					curl_setopt( $ch, CURLOPT_POST, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

					curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );

					curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0); 
					curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);

					// Execute post
					$result = curl_exec($ch);
					if(curl_errno($ch))
					{ 
						echo 'Curl error: ' . curl_error($ch); 
					} 

					// Close connection
					curl_close($ch);
				}
			
			
			
			
			
			
			
			
			redirect('device');
	}
	
	public function delete($deviceid)
	{
		$result = 	$this->devices_model->delete($deviceid,$this->session_userid);
		if($result === TRUE)
		{
			redirect('device');
            //echo 'yes';
		}
		else
		{
			redirect('device');
            //echo 'no';
		}
	}


    public function reset($imei){
        $result = $this-> devices_model ->reset_device($imei);
        if($result){
            redirect('device');
        } else {
            redirect('device');
        }

    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
