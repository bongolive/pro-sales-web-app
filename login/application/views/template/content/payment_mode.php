				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('payment_mode'); ?></h2>
					   
            </header> 

        </article>

    </div>	
    <div class="row">
 
    	 <article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l('all_payment_mode'); ?></h4>
								</div>
								<div class="modal-body"> 
								
								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>

									<tr>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>
 				  <?php if ($tb_data){ 
									foreach ($tb_data as $data):  
									  
                                            $id = 'mode_id';
                                     ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php
											if ($field =="status") {
												 
												if ($data[$field] == 1) {
													echo '<span class="label label-success">Active</span>';
												} else {

													echo '<span class="label label-warning">In Active</span>';
												}
											} else {
												echo $data[$field];
											}
 ?></td>
                                        <?php endforeach; ?>
										
                                            <td> 
                                                	<a href="<?php echo site_url($edit) . '/' . $data[$id]; ?>" class="btn btn-small btn-info"> Edit</a>
                                                	
                                                	<?php if($data['status']==1){?>
                                                	<a href="<?php echo site_url($delete) . '/remove/' . $data[$id]; ?>" class="btn btn-small btn-danger" title="Disable" ><span class="awe-remove"/></a>
                                                	<?php }else{ ?>
                                                		
													<a href="<?php echo site_url($delete) . '/add/' . $data[$id]; ?>" class="btn btn-small btn-success" title="Activate" ><span class="awe-plus"/></a>	
                                                	<?php } ?>
                                                	
                                                	<!--a href="<?php echo site_url($delete) . '/' . $data[$id]; ?>" class="btn btn-small btn-danger" > Delete</a>--> 
											</td>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
								</tbody>
	</table> 
           
		</article>							
 
    	 
    	 <article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l('add_new_mode'); ?></h4>
								</div>
								<div class="modal-body">
									<?php if($info){ ?>
										<form class="form-horizontal" method="post" action="<?php echo site_url('settings/update_mode'); ?>">
								
								<?php	}else{ ?>
                                <form class="form-horizontal" method="post" action="<?php echo site_url('settings/add_mode'); ?>">
                                	<?php
									}

									if($info){
 ?>
                                			<input name="mode_id" value="<?php echo $info['mode_id']; ?>" type="hidden" />
                                <?php 	} ?>
                                	
                                    <fieldset>
                                        
                                        <div class="control-group">
                                        	<label class="control-label" for="mode_name" ><?php _l('mode_name'); ?></label>
                                        	<div class="controls">
                                        	<input name="mode_name" id="mode_name" value="<?php
											if ($info) { echo $info['mode_name'];
											} else {echo set_value('mode_name');
											}
 ?>" class="input-large" type="text" required />
                                        	</div>
                                    	</div>
								
                                    	 
                                     <div class="control-group">
                                        	<label class="control-label" for="mode_symbol" ><?php _l('mode_symbol'); ?></label>
                                        	<div class="controls">
                                        	<input name="mode_symbol" id="mode_symbol" value="<?php
											if ($info) { echo $info['mode_symbol'];
											} else {echo set_value('mode_symbol');
											}
 ?>" class="input-large" type="text"   />
                                        	</div>
                                    	</div>
                                    	 <div class="control-group">
                                        	<label class="control-label" for="mode_payment_no" ><?php _l('mode_payment_no'); ?></label>
                                        	<div class="controls">
                                        	<input name="mode_payment_no" id="mode_payment_no" value="<?php
											if ($info) { echo $info['mode_payment_no'];
											} else {echo set_value('mode_payment_no');
											}
 ?>" class="input-large" type="text"   />
                                        	</div>
                                    	</div>
                                     
                                    		     
								</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('settings/payment_mode'); ?>" class="btn btn-alt" data-dismiss="modal">Clear</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
                            </fieldset>
                        </form>
                        </article>
                        </div>                        	
                        </div>	
                        </section>
                        		
					 