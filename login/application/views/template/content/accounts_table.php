<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l($tb_name); ?></h2>
							  
							<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('accounts/save'); ?>";" style="float: right; padding:8px;"><?php _l('new_account'); ?></a>
					 
            </header> 

        </article>

    </div>	
    
     <!-- include the filtering paget-->
    
    <?php
	if ($controller != "customers/view_team") {
		include_once ('filtering.php');

	}
?>
   	
    <div class="row">
  
        <article class="span12 data-block">
 
            <section>
         		
   <table class="table table-striped table-bordered table-hover table-media">
								<thead>

								<tr>
										<th class="span1"><input id="optionsCheckbox" type="checkbox" value="option1"></th>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>

							 
									  <?php if ($tb_data){
									 // 	print_r($tb_data); 
									  	//echo $this->db->last_query();
									foreach ($tb_data as $data):

											 
											//echo $table_id;
                                            $id = $data[$table_id];
                                        // var_dump($data); 
										  //$id = $data['user_id'];
                                    ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
											<td><input id="optionsCheckbox" type="checkbox" value="option1"></td>

                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php

											if ($field == 'status') {
												if ($data['status'] == 1) {
													echo '<span class="label label-success">Active</span>';
												} else {

													echo '<span class="label label-warning">In Active</span>';
												}
											} else {
												echo ucwords($data[$field]);

											}
										   ?></td>
                                        <?php endforeach; ?>
										
                                            <td>
                                               <div class="btn-group">
													<a href="<?php echo site_url($view) . '/' . $id; ?>" title="View" class="btn btn-small"> <span class="awe-eye-open"/> </a>		
													<?php
													if($controller =="customers/view_team"){ ?>
													<a href="<?php echo site_url('customers/re_assign') . '/'.$team['id'] .'/'. $id; ?>" class="btn btn-small btn-danger" title="Re-Assign" ><span class="awe-remove"/></a>	
													<?php 
													}else{
													?>
													
													<a href="<?php echo site_url($edit) . '/' . $id; ?>" class="btn btn-small " title="Edit"><span class="awe-pencil"/></a>
                                                	
                                                	<?php if($data['status']==1){?>
                                                	<a href="<?php echo site_url($delete) . '/remove/' . $id; ?>" class="btn btn-small btn-danger" title="Disable" ><span class="awe-remove"/></a>
                                                	<?php }else{ ?>
                                                		
													<a href="<?php echo site_url($delete) . '/add/' . $id; ?>" class="btn btn-small btn-success" title="Activate" ><span class="awe-plus"/></a>	
                                                	<?php
														}

														}
 ?>
												 </div> 						 
											</td>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td colspan="2"> No data available </td> </tr>';
										}
									?>
								</tbody>
	</table> 
          </section>

        </article>

    
    </div>

</section>
