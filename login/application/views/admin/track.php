<section class="container" role="main">

    <div class="row">

        <article class="span12 data-block">

            <header>

                <h2><span class="icon-folder-open" style="padding-right:10px;"></span><?php _l($tb_name); ?></h2>

                <form method="post" action="javascript:void(0);" style="position: absolute;top: 15px;right: 30px;">
					
                    <select style="height: 28px;width: 150px;" id="user" name="user">
                        <option value="">Please select User</option>
                        <?php foreach ($users as $user): ?>
                           <option value="<?php echo $user['userid'];?>"><?php echo $user['name'];?></option>
						<?php endforeach; ?>
                    </select>
                    
                    <select style="height: 28px;width: 150px;" id="devicename">
                        <option value="">Please select Device</option>
                        <?php foreach ($devices as $device): ?>
                           <option value="<?php echo $device['phone_imei'];?>"><?php echo $device['device_name'];?></option>
						<?php endforeach; ?>
                    </select>

                    <input class="show_date_piker" type="text" id="start_date" style="height: 15px;width: 140px;" placeholder="<?php _l('start_date'); ?>" required/>

                    <input class="show_date_piker" type="text" id="end_date" style="height: 15px;width: 140px;" placeholder="<?php _l('end_date'); ?>" required/>

                    <input class="btn btn-small" type="submit" id="submit_search" value="<?php _l('search'); ?>" onclick="javascript:void(0);"/>

                </form>

            </header>

            <section style="padding: 0px;">

                <iframe id="map_iframe" width="100%" height="500px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php echo site_url('admin/showdevices');?>" ></iframe>

            </section>

        </article>

    </div>

</section>

<script>

$(document).ready(function(){
	 $("#submit_search").click(function(){
		//submitForm();
        var device_name = $('#devicename').val();

		var start_date = $('#start_date').val();
        var start_date = start_date.replace('/','-');
        var start_date = start_date.replace('/','-');
        var start_date = start_date.replace(':','A');

		var end_date = $('#end_date').val();
        var end_date = end_date.replace('/','-');
        var end_date = end_date.replace('/','-');
        var end_date = end_date.replace(':','A');
        
			var map_url = "<?php echo site_url('admin/showtrack'); ?>" + "/" + device_name + "/" + start_date + "/" + end_date;

			//alert(map_url);

			$('#map_iframe').attr('src', map_url);

			$('#map_a').attr('href', map_url);
           
        return(false);

	    });

	});

	function submitForm(){
		var device_name = $('#devicename').val();
		var start_date = escape($('#start_date').val());
		var end_date = escape($('#end_date').val());
			//var map_url = "<?php echo site_url(); ?>" + "admin/showTrack/" + device_name + "/" + start_date + "/" + end_date;
            var map_url = "<?php echo site_url(); ?>" + "admin/showTrack/start";
			$('#map_iframe').attr('src', map_url);
			$('#map_a').attr('href', map_url);
            $('#map_a').refresh();
        return false;
	}
</script>
<script type="text/javascript">
	$(document).ready(function()
	{
		$("#user").change(function()
	{
		var id=$(this).val();
		var dataString = 'id='+ id;

	$.ajax
	({
		type: "POST",
		url: "<?php echo site_url("admin/getUserDevices"); ?>",
		data: dataString,
		cache: false,
		success: function(html)
	{
		$("#devicename").html(html);
	}
	});

	});

});
</script>