<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dodoso extends User_Controller {
 
public $dodoso_dsc =array( 'namba_ya_dodoso', 'namba_ya_kaya', 'namba_ya_mwanamke', 'eneo', 'tarehe', 'msimamizi', 'mhoji'); 
    
    public function __construct()
	{
		parent::__construct();
		$this->load->model('dodoso_model');
		
       $this->lang->load('profile');
		$this->session_userid = $this->session->userdata('userid');
		//Make all tab not allowed
		$header['track_tab'] ="";
		$header['device_tab'] ="";
		$header['tasks_tab'] ="";
		$header['reports_tab'] ="";
		$header['deliveries_tab'] ="";
		$header['customers_tab'] ="";
		$header['boundaries_tab'] ="";
		$header['support_tab'] ="allow";
		$header['pathfinder_tab'] ="";
		$header['stocks_tab'] ="";
		$header['registrations_tab'] ="";		
		
		$condUserPerm = array('userid'=>$this->session->userdata('userid'));
		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
		foreach($user_permissions as $user_permission){
			if($user_permission['permission_name']=='track tab'){
				$header['track_tab'] ="allow";
			}else if($user_permission['permission_name']=='device tab'){
				$header['device_tab'] ="allow";
			}if($user_permission['permission_name']=='tasks tab'){
				$header['tasks_tab'] ="allow";
			}if($user_permission['permission_name']=='reports tab'){
				$header['reports_tab'] ="allow";
			}if($user_permission['permission_name']=='deliveries tab'){
				$header['deliveries_tab'] ="allow";
			}if($user_permission['permission_name']=='customers tab'){
				$header['customers_tab'] ="allow";
			}if($user_permission['permission_name']=='boundaries tab'){
				$header['boundaries_tab'] ="allow";
			}if($user_permission['permission_name']=='pathfinder survey tab'){
				$header['pathfinder_tab'] ="allow";
			}if($user_permission['permission_name']=='stocks tab'){
				$header['stocks_tab'] ="allow";
			}if($user_permission['permission_name']=='registrations tab'){
				$header['registrations_tab'] ="allow";
			}						
		}
		
	}
 
	  
 public function index()
	{
$this->session->unset_userdata('kata');		
$this->session->unset_userdata('phone_id');		
$this->session->unset_userdata('phoneid');		
$this->session->unset_userdata('start_date');		
$this->session->unset_userdata('end_date');		
$this->session->unset_userdata('startdate');		
$this->session->unset_userdata('enddate');		
$this->session->unset_userdata('enddate');		
 	 
 if($this->input->post('chuja')) {

	//view by filtered data
	extract($_POST);
	
	if($phones !=0 && $start_date ==""){ 
		$where=array('dodoso.phone_id'=>$phones);  
		$sess=array('phone_id'=>$phones);
	}elseif($start_date !="" && $phones !=0){ 
 
 	$where=array('dodoso.tarehe >='=>date('Y-m-d',strtotime($start_date)),'dodoso.tarehe <='=>date('Y-m-d',strtotime($end_date)),'dodoso.phone_id'=>$phones);
	  	$sess =array('start_date'=>date('Y-m-d',strtotime($start_date)),'end_date'=>date('Y-m-d',strtotime($end_date)),'phoneid'=>$phones);
	}else if($start_date !="" && $phones ==0){ 
	  $where=array('dodoso.tarehe >='=>date('Y-m-d',strtotime($start_date)),'dodoso.tarehe <='=>date('Y-m-d',strtotime($end_date))); 
	  $sess =array('startdate'=>date('Y-m-d',strtotime($start_date)),'enddate'=>date('Y-m-d',strtotime($end_date)));	
	}else{ $where=FALSE;$sess=FALSE; }	
}else{ $sess=FALSE;			$where=FALSE;			$where_wilaya=FALSE; }
 	
//storing filter conditions for export
if($where) { $this->session->set_userdata($sess); }

	$dodoso_dsc=array('kijiji', 'namba_ya_dodoso','namba_ya_mwanamke' ,'tarehe','msimamizi','mhoji'); 
	$data['tb_name']='dodoso_regions';
	$data['tb_headers'] = $dodoso_dsc;
	$data['row_fields'] = $dodoso_dsc;
 
	$data['maswali'] =$this->dodoso_model->read_maswali_excel($where=false,$swali='mke'); 
		 foreach($data['maswali'] as $swali){
					$toa[] = $swali['swali_no']; 
		 }
  
	$data['tb_data'] =$this->dodoso_model->read_dodoso($where); 
	
	$phones = array('userid'=>$this->session->userdata('userid'));
	$data['phones'] = $this->dodoso_model->get_phones($phones);
	$data['mikoa'] = $this->dodoso_model->mikoa();
	$data['wilaya'] = $this->dodoso_model->wilaya();
	$data['kata'] = $this->dodoso_model->kata();
	
 	$data['js'] = array('plugins/dataTables/jquery.datatables.min.js','custom/custom-datatable.js');
	 
			$this->load->view('template/header',$this->header);
			$this->load->view('template/content/dodoso_table',$data);
			$this->load->view('template/footer');
			 
	}
 
public function view()
	{
 $dodoso=$this->uri->segment(4);
  $data['loan_no'] = str_replace("-","/",$this->input->post("loan_no"));
	$data['tb_name']='dodoso_majibu';
	
	$where=array('namba_ya_dodoso'=>$dodoso);
	
	$data['dodoso'] = $this->dodoso_model->read_dodoso($where);
	if($data['dodoso']) {
			foreach($data['dodoso'] as $mke){ //get mume dodoso 
			
				$data['no_dodoso']=$mke['namba_ya_dodoso'];
			 //selecting out maswali ya kike
			$data['maswali'] =$this->dodoso_model->read_maswali_excel($where=false,$swali='mke'); 
			$toa[]='A1';
				 foreach($data['maswali'] as $swali){
							$toa[] = $swali['swali_no']; 
				 }
			
			$where_mke=array('majibu.namba_ya_dodoso'=>$mke['namba_ya_dodoso']);
			 
			//mume majibu
				$dodoso_mume = $this->dodoso_model->read_mume($where_mke,$toa);
			 
				if($dodoso_mume){		
				
						$data['dodoso_mume'] =$dodoso_mume;
				
						foreach( $dodoso_mume as $dodoso_mume){  
							$where_mume=array('majibu.namba_ya_dodoso'=>$dodoso_mume['namba_ya_dodoso']);
							 
							$data['majibu_mume'] = $this->dodoso_model->read_majibu($where_mume);
					}
			}else { $data['dodoso_mume']=FALSE;$data['majibu_mume'] =FALSE; }
		 
		}	
			}else {
				 
				$data['dodoso'] =FALSE;$data['dodoso_mume']=FALSE;$data['majibu_mume'] =FALSE; $data['no_dodoso']=0;
			}
	$where=array('namba_ya_dodoso'=>$dodoso);
	  $data['majibu'] = $this->dodoso_model->read_majibu($where);
	$data['maswali'] = $this->dodoso_model->read_maswali($where=FALSE);
	$data['kundi'] = $this->dodoso_model->maswali_kundi($where=FALSE);

	$this->load->view('template/header',$this->header);
	$this->load->view('template/content/dodoso_view',$data);
	$this->load->view('template/footer');
	}

public function get_vijiji($term=FALSE) {
 		  
	$where=array('kata'=>$term); 
	
	$kijiji = $this->dodoso_model->vijiji($where);
 if($kijiji) {
	foreach($kijiji as $kijiji){ 
	  echo "<option value='".$kijiji['code']."'>".$kijiji['jina_la_kijiji']."</option>"; 
	}
}else {  
	 echo "<option value=''>Hakuna vijiji</option>"; 
}
	 
}
 
public function report()
	{
		  
//updating pathfinder table
 
 //$this->update_pathfinder();		 
		 
		$error = "";
		
	 $this->load->library('excel');
	//fileter conditions for export
	 
				// Set document properties
				$this->excel->getProperties()->setCreator("Foot Print Admin")
									 ->setLastModifiedBy("Foot Print Admin")
									 ->setTitle("Exported dodoso from Foot Print Site")
									 ->setSubject("Exported dodoso from Foot Print Site")
									 ->setDescription("Exported dodoso from Foot Print Site for the User '".$this->session->userdata('username')."'")
									 ->setKeywords("Exported dodoso from Foot Print Site")
									 ->setCategory("Exported dodoso from Foot Print Site");			
		
				// Add some data
					 $maswali =	$this->dodoso_model->read_pathfinder($where=FALSE,$swali=FALSE);
					 $title =	$this->dodoso_model->read_pathfinder($where=FALSE,$title=TRUE);
				
					 
					  $i=1;$column='A';
					 
					  if($title) {
					  
					  	foreach($title as $thedata){ 
						 	foreach(array_keys($thedata) as $key){
						 
		 				     $this->excel->getActiveSheet()->setCellValue($column.$i,$key);
				  			    	
							    	$column++;
							    	if($key == 'dmsl13_nyingine') {
							    		break;
							    	} 
							    }
					    } 
					   }else { echo 'No data available'; }
					  
			/*
				if($this->session->userdata('start_date') && $this->session->userdata('end_date')) {
				 $where=array('dodoso.tarehe >='=>$this->session->userdata('start_date'),'dodoso.tarehe <='=>$this->session->userdata('end_date'), 'dodoso.phone_id'=>$this->session->userdata('phoneid'));
				  $result = $this->dodoso_model->read_dodoso($where);//getting dodoso information
				}elseif($this->session->userdata('kata')) {
					$where=array('dodoso.kata'=>$this->session->userdata('kata'));
				}elseif($this->session->userdata('phone_id')) {
				$where=array('dodoso.phone_id'=>$this->session->userdata('phone_id'));
				}elseif($this->session->userdata('startdate') && $this->session->userdata('enddate')) {
				$where=array('dodoso.tarehe >='=>$this->session->userdata('startdate'),'dodoso.tarehe <='=>$this->session->userdata('enddate'));  
			 	}else { $where=FALSE;} 
			
		$result = $this->dodoso_model->read_dodoso($where);//getting dodoso information	  
	 */
	
			 if($maswali) {	
		 	
		 	 $index = 2;
				foreach($maswali as $value)  
					{ 	

					 	$column='A';	
			 			foreach($value as $taarifa){ 
			 		 		 	 	$this->excel->setActiveSheetIndex(0); 
	 					 			$this->excel->getActiveSheet()->setCellValue($column.$index, $taarifa);
							      $column++;
						}$index++;
			    }
			}else { $this->excel->getActiveSheet()->setCellValue('A2','no data available'); }

 
			 	// Rename worksheet
		 	  $this->excel->getActiveSheet()->setTitle('dodoso'); 
		
				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			  $this->excel->setActiveSheetIndex(0);
		 
	 
				// Redirect output to a client's web browser (Excel2007)
			   header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="PathFinder_survey.xlsx"');
				header('Cache-Control: max-age=0');
		
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
				$objWriter->save('php://output');
				exit;
		 
				redirect('dodoso'); 
   
	}

//pathfinder table updating function
/*
public function update_pathfinder() {
	
 $wherez=array('status'=>0);
$resultdodoso=$this->dodoso_model->read_dodoso($wherez);
 
if($resultdodoso){
	foreach($resultdodoso as $rowdodoso){
		
		//checking if this record exists on pathfinder table
		 $where_nani=array('dodoso_namba'=>$rowdodoso['namba_ya_dodoso']);
			 
		 $resultfinder=$this->dodoso_model->read_pathfinder($where_nani);
		 
 			
			
 if($resultfinder){
				//record xist so we pdate it
  $sqlupdate =array("mkoa"=>strtoupper($rowdodoso['mkoa']),"wilaya"=>strtoupper($rowdodoso['wilaya']),"kata"=>strtoupper($rowdodoso['kata']),"kijiji"=>strtoupper($rowdodoso['kijiji']),"namba_ya_kaya"=>strtoupper($rowdodoso['namba_ya_kaya']),"namba_ya_mwanamke"=>strtoupper($rowdodoso['namba_ya_mwanamke']),"eneo"=>strtoupper($rowdodoso['eneo']),"tarehe"=>strtoupper($rowdodoso['tarehe']),"muda"=>strtoupper($rowdodoso['muda']),"muhoji"=>strtoupper($rowdodoso['mhoji']),"msimamizi"=>strtoupper($rowdodoso['msimamizi']));
				
	 	$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$sqlupdate);
			}else{
				
				//record not exist so we insert new
				$jibu = array("dodoso_namba"=>strtoupper($rowdodoso['namba_ya_dodoso']),"mkoa"=>strtoupper($rowdodoso['mkoa']),"wilaya"=>strtoupper($rowdodoso['wilaya']),"kata"=>strtoupper($rowdodoso['kata']),"kijiji"=>strtoupper($rowdodoso['kijiji']),"namba_ya_kaya"=>strtoupper($rowdodoso['namba_ya_kaya']),"namba_ya_mwanamke"=>strtoupper($rowdodoso['namba_ya_mwanamke']),"eneo"=>strtoupper($rowdodoso['eneo']),"tarehe"=>strtoupper($rowdodoso['tarehe']),
				"muda"=>strtoupper($rowdodoso['muda']),"muhoji"=>strtoupper($rowdodoso['mhoji']),"msimamizi"=>strtoupper($rowdodoso['msimamizi']));
				
				$this->dodoso_model->save($jibu);
	 	} 
	  
 
	//now we select majibu ya dodoso ili ku update table ya pathfinder dodoso
		$where_majb=array('namba_ya_dodoso' =>$rowdodoso['namba_ya_dodoso']);
			 $resultmajibu = $this->dodoso_model->read_majibu_excel($where_majb);
 
			 
	
		if($resultmajibu){
				foreach($resultmajibu as $rowmajibu){
					switch($rowmajibu['swali']){
					 	case 'A1':
							$jibu = explode("|",$rowmajibu['jibu']);
							$idadiwanakaya = count($jibu);
							$counter= 1;
							while($counter <= $idadiwanakaya){
								if($counter==1){
									$mjibu = explode(",",$jibu[0]);
									
  	$info=array( "a1_umri1"=>$mjibu[0],"a1_jinsia1"=>$mjibu[1],"a1_uhusiano1"=>$mjibu[2],"jumla_wanakaya"=>$idadiwanakaya);
		 $this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
									  
								}else if($counter==2){
									$mjibu = explode(",",$jibu[1]);
									
							$info=array( "b2"=>$rowmajibu['jibu']);
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);	
								 
								}else if($counter==3){
									$mjibu = explode(",",$jibu[2]);
									
$info=array( "a1_umri3"=>$mjibu[0],"a1_jinsia3"=>$mjibu[1],"a1_uhusiano3"=>$mjibu[2]);
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);										
									  
								}else if($counter==4){
									$mjibu = explode(",",$jibu[3]);
						 		$info=array( "a1_umri4"=>$mjibu[0],"a1_jinsia4"=>$mjibu[1],"a1_uhusiano4"=>$mjibu[2]);
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
								}else if($counter==5){
									$mjibu = explode(",",$jibu[4]);
									
					$info=array( "a1_umri5"=>$mjibu[0],"a1_jinsia5"=>$mjibu[1],"a1_uhusiano5"=>$mjibu[2]);
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);						
								 
								}else if($counter==6){
									$mjibu = explode(",",$jibu[5]); 
							$info=array( "a1_umri6"=>$mjibu[0],"a1_jinsia6"=>$mjibu[1],"a1_uhusiano6"=>$mjibu[2]);
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);	 
									 
								}else if($counter==7){
									$mjibu = explode(",",$jibu[6]);
				$info=array( "a1_umri7"=>$mjibu[0],"a1_jinsia7"=>$mjibu[1],"a1_uhusiano7"=>$mjibu[2]);
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);	 
									  
								}else if($counter==8){
									$mjibu = explode(",",$jibu[7]);
								$info=array( "a1_umri8"=>$mjibu[0],"a1_jinsia8"=>$mjibu[1],"a1_uhusiano8"=>$mjibu[2]);
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);	 
									 
								}else if($counter==9){
									$mjibu = explode(",",$jibu[8]);
									
									$info=array( "a1_umri9"=>$mjibu[0],"a1_jinsia9"=>$mjibu[1],"a1_uhusiano9"=>$mjibu[2]);
									$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);	 
								}else if($counter==10){
									$mjibu = explode(",",$jibu[9]);
									
									$info=array( "a1_umri10"=>$mjibu[0],"a1_jinsia10"=>$mjibu[1],"a1_uhusiano10"=>$mjibu[2]);
									$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);	
									 
								}else if($counter==11){
									$mjibu = explode(",",$jibu[10]);
									
								$info=array( "a1_umri11"=>$mjibu[0],"a1_jinsia11"=>$mjibu[1],"a1_uhusiano11"=>$mjibu[2]);
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);	
									 
								}else if($counter==12){
									$mjibu = explode(",",$jibu[11]);
									
									$info=array( "a1_umri12"=>$mjibu[0],"a1_jinsia12"=>$mjibu[1],"a1_uhusiano12"=>$mjibu[2]);
$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);	 
								}else if($counter==13){
									$mjibu = explode(",",$jibu[12]);
									
									$info=array( "a1_umri13"=>$mjibu[0],"a1_jinsia13"=>$mjibu[1],"a1_uhusiano13"=>$mjibu[2]);
$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);	
									 
								}else if($counter==14){
									$mjibu = explode(",",$jibu[13]); 
$info=array( "a1_umri14"=>$mjibu[0],"a1_jinsia14"=>$mjibu[1],"a1_uhusiano14"=>$mjibu[2]);
$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);	
									 
								}else if($counter==15){
									$mjibu = explode(",",$jibu[14]);
									
$info=array( "a1_umri15"=>$mjibu[0],"a1_jinsia15"=>$mjibu[1],"a1_uhusiano15"=>$mjibu[2]);
$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);	
									 
								}
								$counter = $counter + 1;
							}
							//$jibu = mysql_query("update pathfinder_dodoso set ");
						break; 
						 
						
						case 'B1':
							$jibu = explode(",",$rowmajibu['jibu']);
							$info=array("b1_mwezi"=>$jibu[0],"b1_mwaka"=>$jibu[1]);
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'B2': 
							$info=array( "b2"=>$rowmajibu['jibu']);
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						break;
						
						case 'B3':
			 				$info=array( "b3"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'B4': 
						$info=array( "b4"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'B5': 
						$info=array( "b5"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'B6': 
						$info=array( "b6"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);  
						break;
						
						case 'B7': 
						$info=array("b7"=>$rowmajibu['jibu']    );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'B8': 
							$info=array(  "b8"=>$rowmajibu['jibu']  );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'B9': 
						$info=array( "b9"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);  
						break;
						
						case 'B10': 
							$info=array( "b10"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'B11':
$info=array( "b11"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'B12':
						$info=array( "b12"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'B13':
						$info=array( "b10"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);  
						break;
						
						case 'B14':
						$info=array( "b14"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'B15':
						$info=array( "b15"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'B16':
						$info=array( "b16"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'B17':
						$info=array( "b17"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'B18':
						$info=array( "b18"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'B19':
						$info=array( "b19"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'B20':
						$info=array( "b20"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'B21':
							$info=array( "b21"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'B22':
							$info=array( "b22"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'B23':
						$jibu = explode(",",$rowmajibu['jibu']);
						
						$info=array( "b23_umeme"=>$jibu[0],"b23_taa"=>$jibu[1],"b23_redio"=>$jibu[2],"b23_televisheni"=>$jibu[3],"b23_simu_mkononi"=>$jibu[4],"b23_simu_mezani"=>$jibu[5],"b23_pasi"=>$jibu[6],"b23_friji"=>$jibu[7]  );
						
					 $this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);  
						break;
						
						case 'B24':
						$jibu = explode(",",$rowmajibu['jibu']);
$info=array(  "b24_saa"=>$jibu[0],"b24_baiskeli"=>$jibu[1],"b24_pikipiki"=>$jibu[2],"b24_gari"=>$jibu[3],"b24_akaunti"=>$jibu[4]   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);  
						break;
						
						case 'B25':
						$info=array( "b25"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							  
						break;
						
						case 'B26':
						$info=array( "b26"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						case 'P1':
							$info=array( "p1"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'P2':
							$info=array( "p2"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'P3':
							$jibu = explode(",",$rowmajibu['jibu']);
							$info=array( "p3_daktari"=>$jibu[0],"p3_muuguzi"=>$jibu[1],"p3_afya_wengine"=>$jibu[2],"p3_mkunga_jadi"=>$jibu[3],"p3_afya_jamii"=>$jibu[4],"p3_wengine"=>$jibu[5]   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'P4':
	$jibu = explode(",",$rowmajibu['jibu']);
	$info=array( "p4_hospitali_serikali"=>$jibu[0],"p4_hospitali_dini"=>$jibu[1],"p4_kituo_serikali"=>$jibu[2],"p4_kituo_dini"=>$jibu[3],"p4_zahanati_serikali"=>$jibu[4],"p4_zahanati_dini"=>$jibu[5],"p4_nyingine"=>$jibu[6]  ); 
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'P5':
							$info=array( "p5"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'P6':
							$info=array( "p6"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 	
						break;
						
						case 'P7':
						$jibu = explode(",",$rowmajibu['jibu']);
							$info=array( "p7_presha"=>$jibu[0],"p7_mkojo"=>$jibu[1],"p7_damu"=>$jibu[2] );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'P8':
						$info=array( "p8"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'P9':
						$info=array( "p9"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'P10':
						$info=array( "p10"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
					 
						break;
						
						case 'P11':
							$info=array( "p11"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'P12':
						$info=array( "p12"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'P13':
						$info=array( "p13"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'P14':
						$info=array( "p14"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'P15':
							$jibu = explode(",",$rowmajibu['jibu']);
							$info=array( "p15_damu_ukeni"=>$jibu[0],"p15_homa"=>$jibu[1],"p15_kuvimba_uso"=>$jibu[2],"p15_kuchoka"=>$jibu[3],"p15_kichwa_kuuma"=>$jibu[4],"p15_kifafa_mimba"=>$jibu[5],"p15_kucheza_mtoto"=>$jibu[6],"p15_nyingine"=>$jibu[7]  );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'P16':
						$info=array( "p16"=>$rowmajibu['jibu']   );
						 $this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'P17':
						$info=array( "p17"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'P18':
						$info=array( "p19"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'P19':
						$info=array( "p19"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'P20':
						$info=array( "p20"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'P21':
							$info=array( "p21"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'P22':
						$info=array( "p22"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'P23':
							$info=array( "p23"=>$rowmajibu['jibu']   );
						 
						break;
						
						case 'P24':
						$info=array( "p24"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);	 
						break;
						
						case 'P25':
							$jibu = explode(",",$rowmajibu['jibu']);
						$info=array( "p25_kituo"=>$jibu[0],"p25_usafiri"=>$jibu[1],"p25_fedha_dharula"=>$jibu[2],"p25_mtoaji_damu"=>$jibu[3],"p25_mtaalamu"=>$jibu[4],"p25_vifaa_safi"=>$jibu[5],"p25_eleza"=>$jibu[6]  );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
								 
						break;
						
						case 'P26':
							$info=array( "p26"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'P27':
							$info=array( "p27"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'P28':
							$info=array( "p28"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'P29':
							$info=array( "p29"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'P30':
						
							$info=array( "p30"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'P31':
							$jibu = explode(",",$rowmajibu['jibu']); 
							$info=array( "p31_ujauzito"=>$jibu[0],"p31_kujifungua"=>$jibu[1],"p31_kunyonyesha"=>$jibu[2]   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'P32':
						$jibu = explode(",",$rowmajibu['jibu']);
							$info=array( "p32_kutonyonyesha"=>$jibu[0],"p32_kunyonyesha"=>$jibu[1],"p32_arv"=>$jibu[2],"p32_ushauri"=>$jibu[3],"p32_nyingine"=>$jibu[4]  );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'P33':
							$jibu = explode(",",$rowmajibu['jibu']);
			 $info=array( "p33_daktari"=>$jibu[0],"p33_muuguzi"=>$jibu[1],"p33_afya_kijiji"=>$jibu[2],"p33_mkunga"=>$jibu[3],"p33_ndugu"=>$jibu[4],"p33_wengine"=>$jibu[5],"p33_hakuna"=>$jibu[6] );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'P34':
							$info=array( "p34"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'P35':
							$info=array( "p35"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'P36':
							$info=array( "p36"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'P37':
							$info=array( "p37"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'P38':
							$info=array( "p38_kizio"=>$jibu[0],"p38_idadi"=>$jibu[1]  );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
							$jibu = explode(",",$rowmajibu['jibu']); 
						break;
						
						case 'P39':
							$info=array( "p39"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'P40':
							$jibu = explode(",",$rowmajibu['jibu']);
					$info=array( "p40_daktari"=>$jibu[0],"p40_dawa"=>$jibu[1],"p40_usafiri"=>$jibu[2],"p40_jumla"=>$jibu[3]   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'PN1':
							$info=array( "pn1"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'PN2':
							$jibu = explode(",",$rowmajibu['jibu']); 
							 $info=array( "pn2_kizio"=>$jibu[0],"pn2_idadi"=>$jibu[1] );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'PN3':
								$info=array( "pn3"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'PN4':
								$info=array( "pn4"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'PN5':
							$jibu = explode(",",$rowmajibu['jibu']);
						 $info=array( "pn5_a"=>$jibu[0],"pn5_b"=>$jibu[1],"pn5_c"=>$jibu[2],"pn5_d"=>$jibu[3],"pn5_e"=>$jibu[4]  );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);   
						break;
						
						case 'PN6':
								$info=array( "pn6"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);  
						break;
						
						case 'PN7':
								$info=array( "pn7"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						 
						break;
						
						case 'PN8':
								$info=array( "pn8"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'PN9':
								$info=array( "pn9"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'PN10':
								$info=array( "pn10"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'PN11':
								$info=array( "pn11"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'PN12':
								$info=array( "pn12"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);  
						break;
						
						case 'PN13':
								$info=array( "pn13"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'PN14':
								$info=array( "pn14"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'PN15':
								$info=array( "pn15"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
					 
						break;
						
						case 'PN16':
							$jibu = explode(",",$rowmajibu['jibu']);
								$info=array( "pn16_kizio"=>$jibu[0],"pn16_idadi"=>$jibu[1]   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						 
						break;
						
						case 'PN17':
								$info=array( "pn17"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'PN18':
								$info=array( "pn18"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						 
						break;
						
						case 'PN19':
								$info=array( "pn19"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'PN20':
								$info=array( "pn20"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'PN21': 
								$info=array( "pn21"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'C1': 
								$info=array( "c1"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);  
						break;
						
						case 'C2':
								$info=array( "c2"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							  
						break;
						
						case 'C3':
							$jibu = explode(",",$rowmajibu['jibu']);
								$info=array( "c3_kizio"=>$jibu[0],"c3_idadi"=>$jibu[1] );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							  
						break;
						
						case 'C4':
								$info=array( "c4"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							  
						break;
						
						case 'C5':
								$info=array( "c5"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'C6':
								$info=array( "c6"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'C7':
								$info=array( "c7"=>$rowmajibu['jibu']);
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						 
						break;
						
						case 'C8':
								$info=array( "c8"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'C9':
								$info=array( "c9"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'C10':
								$info=array( "c10"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							  
						break;
						
						case 'C11':
								$info=array( "c11"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							  
						break;
						
						case 'C12':
								$info=array( "c12"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							 
						break;
						
						case 'C13':
							$jibu = explode(",",$rowmajibu['jibu']);
								$info=array( "c13_vvu"=>$jibu[0],"c13_huduma_wagonjwa"=>$jibu[1],"c13_chanjo"=>$jibu[2],"c13_kujifungua_hospitali"=>$jibu[3],"c13_kujiandaa_kujifungua"=>$jibu[4],"c13_lishe"=>$jibu[5],"c13_matibabu"=>$jibu[6],"c13_huduma_ujauzito"=>$jibu[7],"c13_usafi"=>$jibu[8],"c13_choo"=>$jibu[9],"c13_maji_safi"=>$jibu[10],"c13_uzazi_mpango"=>$jibu[11],"c13_afya_familia"=>$jibu[12],"c13_nyingine"=>$jibu[13],"c13_sikumbuki"=>$jibu[14] );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);  
						break;
						
						case 'C14':
								$info=array( "c14"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							  
						break;
						
						case 'C15':
							$jibu = explode(",",$rowmajibu['jibu']);
								$info=array( "c15_huduma"=>$jibu[0],"c15_dawa"=>$jibu[1],"c15_rufaa"=>$jibu[2],"c15_wafanyakazi_afya"=>$jibu[3],"c15_nyingine"=>$jibu[4],"c15_hakuna"=>$jibu[5],"c15_sijui"=>$jibu[6]  );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);  
						break;
						
						case 'C16':
							$jibu = explode(",",$rowmajibu['jibu']);
							 $info=array( "c16_matibabu"=>$jibu[0],"c16_mbali"=>$jibu[1],"c16_ghali"=>$jibu[2],"c16_ghalama_daktari"=>$jibu[3],"c16_dawa"=>$jibu[4],"c16_rufaa"=>$jibu[5],"c16_upungufu_wafanyakazi"=>$jibu[6],"c16_mtazamo"=>$jibu[7],"c16_kusubiri"=>$jibu[8],"c16_nyingine"=>$jibu[9],"c16_matatizo"=>$jibu[10]   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'C17':
								$info=array( "c17"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
							  
						break;
						
						case 'C18':
					 	$jibu = explode(",",$rowmajibu['jibu']);	
					 	$info=array( "c18_a"=>$jibu[0],"c18_b"=>$jibu[1],"c18_c"=>$jibu[2],"c18_d"=>$jibu[3],"c18_e"=>'');
						 $this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);  
						break;
						
						case 'FP1':
						
						$info=array( "fp1"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							  
						break;
						
						case 'FP2':						
						$info=array( "fp2"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'FP3':						
						$info=array( "fp3"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'FP4':
							$jibu = explode(",",$rowmajibu['jibu']);						
						$info=array( "fp4_kizazi_mwanamke"=>$jibu[0],"fp4_kizazi_mwanaume"=>$jibu[1],"fp4_kitanzi"=>$jibu[2],"fp4_sindano"=>$jibu[3],"fp4_kipandizi"=>$jibu[4],"fp4_vidonge"=>$jibu[5],"fp4_kondomu_kiume"=>$jibu[6],"fp4_kondomu_kike"=>$jibu[7],"fp4_kiwambo"=>$jibu[8],"fp4_povu"=>$jibu[9],"fp4_kunyonyesha"=>$jibu[10],"fp4_kalenda"=>$jibu[11],"fp4_shahawa_nje"=>$jibu[12],"fp4_kujibari"=>$jibu[13],"fp4_vidonge_uzazi"=>$jibu[14],"fp4_nyingine"=>$jibu[15] );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'FP5':
						$info=array( "fp5"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
					 
						break;
						
						case 'FP6':						
						$info=array( "fp6"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'FP7':						
						$info=array( "fp7"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
					 
						break;
						
						case 'FP8':						
						$info=array( "fp8"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
					 
						break;
						
						case 'IN1':						
						$info=array( "in1"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
					 
						break;
						
						case 'IN2':						
						$info=array( "in2"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'IN3':						
						$info=array( "in3"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'IN4':						
						$info=array( "in4"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'IN5':						
							$info=array( "in5"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
					 
						break;
						
						case 'SL1':
						
						$info=array( "sl1"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'SL2':
						$info=array( "sl2"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'SL3':
						$info=array( "sl3"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'SL4':
						$info=array( "sl4"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'SL5':
						$info=array( "sl5"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'SL6':
							$jibu = explode(",",$rowmajibu['jibu']);$info=array( "sl6_mfukoni"=>$jibu[0],"sl6_akiba"=>$jibu[1],"sl6_nilikopa"=>$jibu[2],"sl6_nyingine"=>$jibu[3] );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'SL7':
							$info=array( "sl7"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'SL8':
							$info=array( "sl8"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);  
						break;
						
						case 'SL9':
						$info=array( "sl9"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'SL10':
						$info=array( "sl10"=>$rowmajibu['jibu']   );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'SL11':
						$jibu = explode(",",$rowmajibu['jibu']);	$info=array( "sl11_kinamama"=>$jibu[0],"sl11_jamii"=>$jibu[1],"sl11_vijana"=>$jibu[2],"sl11_wenye_nyumba"=>$jibu[3],"sl11_sokoni"=>$jibu[4],"sl11_dini"=>$jibu[5],"sl11_kisiasa"=>$jibu[6],"sl11_kingine"=>$jibu[7]);
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'SL12':
								$info=array( "sl12"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'SL13':
				$jibu = explode(",",$rowmajibu['jibu']);		$info=array( "sl13_tv"=>$jibu[0],"sl13_redio"=>$jibu[1],"sl13_magazeti"=>$jibu[2],"sl13_mbao"=>$jibu[3],"sl13_kingine"=>$jibu[4]);
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'SL14':
					$jibu = explode(",",$rowmajibu['jibu']);	
					$info=array( "sl14_familia"=>$jibu[0],"sl14_marafiki"=>$jibu[1],"sl14_tv"=>$jibu[2],"sl14_redio"=>$jibu[3],"sl14_wafanyakazi_afya"=>$jibu[4],"sl14_wafanyakazi_jamii"=>$jibu[5],"sl14_dini"=>$jibu[6],"sl14_kimila"=>$jibu[7],"sl14_mfamasia"=>$jibu[8],"sl14_jamii"=>$jibu[9],"sl14_wengine"=>$jibu[10] );
							$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
					 
						break;
						
						case 'DMB1': 
								$info=array( "dmb1"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'DMB2':
						$info=array( "dmb2"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						   
						break;
						
						case 'DMB3':
						$info=array( "dmb3"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'DMB4':
						$info=array( "dmb4"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'DMB5':
						$info=array( "dmb5"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						  
						break;
						
						case 'DMB6':
						$info=array( "dmb6"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'DMB7':
						$info=array( "dmb7"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'DMB8':
						$info=array( "dmb8"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						   
						break;
						
						case 'DMB9':
						$info=array( "dmb9"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
				 
						break;
						
						case 'DMB10':
						$info=array( "dmb10"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						  ;
						break;
						
						case 'DMB11':
						$info=array( "dmb11"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'DMB12':
						$info=array( "dmb12"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
					 
						break;
						
						case 'DMB13':
						$info=array( "dmb13"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						   
						break;
						
						case 'DMB14':
							$info=array( "dmb14"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
							 
						break;
						
						case 'DMB15':
						$info=array( "dmb15"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						   
						break;
						
						case 'DMSL1':
						$info=array( "dmb1"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						   
						break;
						
						case 'DMSL2':
						$info=array( "dmsl2"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						   
						break;
						
						case 'DMSL3':
								$info=array( "dmsl3"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						
						case 'DMSL4':
						$info=array( "dmsl4"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						   
						break;
						
						case 'DMSL5':
								$info=array( "dmsl5"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						   
						break;
						
						case 'DMSL6':
								$info=array( "dmsl6"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
			 
						break;
						
						case 'DMSL7':
						$info=array( "dmsl7"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						  
						break;
						
						case 'DMSL8':
						$info=array( "dmsl8"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						   
						break;
						
						case 'DMSL9':
						$info=array( "dmsl9"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'DMSL10':
							$jibu = explode(",",$rowmajibu['jibu']);
						$info=array( "dmsl10_wanaume"=>$jibu[0],"dmsl10_jamii"=>$jibu[1],"dmsl10_vijana"=>$jibu[2],"dmsl10_wenye_nyumba"=>$jibu[3],"dmsl10_wafanyabiashara"=>$jibu[4],"dmsl10_dini"=>$jibu[5],"dmsl10_vicoba"=>$jibu[6],"dmsl10_siasa"=>$jibu[7],"dmsl10_nyingine"=>$jibu[8] );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'DMSL11':
						$info=array( "dmsl11"=>$rowmajibu['jibu']   );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						   
						break;
						
						case 'DMSL12':
							$jibu = explode(",",$rowmajibu['jibu']);
						$info=array( "dmsl12_tv"=>$jibu[0],"dmsl12_redio"=>$jibu[1],"dmsl12_magazeti"=>$jibu[2],"dmsl12_mbao"=>$jibu[3],"dmsl12_nyingine"=>$jibu[4] );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info);
						 
						break;
						
						case 'DMSL13':
							$jibu = explode(",",$rowmajibu['jibu']);
				 $info=array( "dmsl13_familia"=>$jibu[0],"dmsl13_marafiki"=>$jibu[1],"dmsl13_tv"=>$jibu[2],"dmsl13_redio"=>$jibu[3],"dmsl13_wafanyakazi_afya"=>$jibu[4],"dmsl13_wahudumu_afya"=>$jibu[5],"dmsl13_viongozi_dini"=>$jibu[6],"dmsl13_waganga_jadi"=>$jibu[7],"dmsl13_wauzaji_madawa"=>$jibu[8],"dmsl13_jamii"=>$jibu[9],"dmsl13_nyingine"=>$jibu[10] );
								$this->dodoso_model->update($rowdodoso['namba_ya_dodoso'],$info); 
						break;
						 
						//changing status of table majibu
					  		$info=array( "status" => 1);
						 	$this->dodoso_model->update_majibu($rowmajibu['jibu_id'],$info); 
		  
					}
				}
			}
		//update status of dodoso as proccessed
	 	$info=array( "status" => 1); 
		$this->dodoso_model->update_dodo($rowdodoso['namba_ya_dodoso'],$info); 
 
	}
} 

}  */
}

/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */
