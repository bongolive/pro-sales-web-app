<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Stock extends User_Controller {
	public $user_id;
	public $account_id;

	public function __construct() {
		parent::__construct();

        //$this->output->enable_profiler(true);
		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('stock_id', 'product_name', 'stock_date', 'stock_qty', 'entry_mode', /*'sold_qty', 'balance',*/
		'purchase_price', 'value','warehouse','expiry_date');

		$this -> fields['ft_data'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js', 'plugins/sparkline/jquery.sparkline.min.js');

		$this -> load -> model('stock_model');
		$this -> load -> model('orders_model');
		$this -> load -> model('products_model');
        $this -> load -> model('transaction_model') ;
		$this -> fields['table_id'] = $this -> stock_model -> table_id;

		$this -> stock_model -> form_validation();
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');

		$this -> user_id = $this -> session -> userdata('user_id');
		$this -> account_id = $this -> account_id;
		$this -> fields['controller'] = 'stock';
		$this -> fields['edit'] = 'stock/update_stock';
		$this -> fields['view'] = 'stock/view';
		$this -> fields['delete'] = 'stock/delete';
        $this -> fields['current_stock'] = 'stock/current_stock_info';

	}

	public function index() {

		$where = array('stock.account_id' => $this -> account_id);

		//filtering data

		if ($this -> input -> post('filter')) {
			extract($_POST);
			IF ($stock_date) {
				$where['stock_date'] = $stock_date;
			}
			if ($product_name) {
				$where['stock.product_id'] = $product_name;
			}
		}

		$stock = $this -> stock_model -> read_stock($where);

		if ($stock) {

			$this -> fields['tb_data'] = $stock;
		} else {
			$this -> fields['tb_data'] = FALSE;
		}

		//get products
		$products = $this -> products_model -> read(array('account_id' => $this -> account_id));

		if ($products) {
			$product[] = 'Select Product';
			foreach ($products as $products) {
				$product[$products['product_id']] = $products['product_name'];
			}
		} else {
			$product = FALSE;
		}

		$this -> fields['products'] = $product;
		$this -> fields['tb_name'] = 'stock_tb_name';

		$header['stc_active'] = 'class="active"';
		$this -> fields['add_btn'] = 'add_new_stock';

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/stock_table', $this -> fields);
		$this -> load -> view('template/footer');
	}

    /**/

	public function addstock() {

		if ($this -> input -> post('addstock')) {

			extract($_POST);

			if ($product_id[0]) {
				$date = date('Y-m-d h:i:s');

				//check if mult products inserted

				$st = count($product_id);
				for ($p = 0; $p < $st; $p++) {
					$prod = $this -> products_model -> read(array('product_id' => $product_id[$p]));

					//get last stock
					if ($entry_mode == 'packages') {
						$total_units = $prod[0]['package_units'] * $stock_qty[$p];
					} else {
						$total_units = $stock_qty[$p];
					}

					$stock = array('product_id' => $product_id[$p], 'last_stock' => $prod[0]['items_on_hand'],
                        'stock_date' => date('Y-m-d'), 'entry_mode' => $entry_mode,
                        'stock_qty' => $total_units, 'purchase_price' => $price[$p],
                        'supplier' => 0, 'order_no' => strtotime($date), 'created_by' => $this -> user_id,
                        'created_on' => $date, 'account_id' => $this -> account_id, 'warehouse' => $warehouse,
                        'expiry_date' => $expiry_date,'purchase_order_no' => $purchase_order_no, 'delivery_order_no' => $delivery_order_no);

					if ($this -> stock_model -> save($stock)) {

						//update the stock value
						$new_stock = $prod[0]['items_on_hand'] + $total_units;

						$new_stock = array('items_on_hand' => $new_stock);

						$this -> products_model -> update($product_id[$p], $new_stock);

					}
				}
			}
			redirect('stock');

		} else {


			$header = FALSE;
			$where = array('account_id' => $this -> account_id);

			//get products
			$products = $this -> products_model -> read($where);

			if ($products) {
				$product[] = 'Select Product';
				foreach ($products as $products) {
					$product[$products['product_id']] = $products['product_name'];
				}
			} else {
				$product = FALSE;
			}

            $warehouse = $this-> products_model -> get_warehouses($where);

            if($warehouse){
                $ware[] = "Choose Warehouse";
                foreach($warehouse as $warehouse){
                    $ware[$warehouse['wh_id']] = $warehouse['wh_title'];
                }
            } else {
                $ware = false;
            }

			//get suppliers
			$suppliers = $this -> stock_model -> get_suppliers($where);

			if ($suppliers) {
				foreach ($suppliers as $suppliers) {
					$supplier[$suppliers['supplier_id']] = $suppliers['supplier_name'];
				}
			} else {
				$supplier = FALSE;
			}
			$this -> fields['info'] = FALSE;

			$this -> fields['suppliers'] = $supplier;
			$this -> fields['products'] = $product;
            $this -> fields['warehouse'] = $ware;

			$this -> load -> view('template/header', $header);
			$this -> load -> view('template/content/stock_form', $this -> fields);
			$this -> load -> view('template/footer');

		}

	}

	public function update_stock() {

		if ($this -> input -> post('addstock')) {

			extract($_POST);
			$date = date('Y-m-d h:i:s');
			$st = count($product_id);

			for ($p = 0; $p < $st; $p++) {

                $prod = $this -> products_model -> read(array('product_id' => $product_id[$p]));

                if ($entry_mode == 'packages') {
                    $total_units = $prod[0]['package_units'] * $stock_qty[$p];

                } else {
                    $total_units = $stock_qty[$p];
                }

				$stock = array('product_id' => $product_id[$p], 'stock_date' => $stock_date[$p],
                    'stock_qty' => $total_units, 'entry_mode' => $entry_mode, 'purchase_price' => $price[$p],
                    'supplier' => 0, 'last_update' => $date,'warehouse'=> $warehouse,'expiry_date' => $expiry_date
                ,'purchase_order_no' => $purchase_order_no, 'delivery_order_no' => $delivery_order_no);
				if ($this -> stock_model -> update($stock_id, $stock)) {

					//update the stock value
					$new_stock = $last_stock + $total_units;

					$new_stock = array('items_on_hand' => $new_stock);

					$this -> products_model -> update($product_id[$p], $new_stock);

				}
				echo $stock_id;
			}

			redirect('stock');

		} else {

			$where = array('stock_id' => $this -> uri -> segment(4));

			$stock = $this -> stock_model -> get_stock($where);

			if ($stock) {

				$this -> fields['info'] = $stock;
			} else {
				$this -> fields['info'] = FALSE;
			}
			//get products
			$where_prod = array('account_id' => $this -> account_id);
			$products = $this -> products_model -> read($where_prod);

			if ($products) {
				$product[] = 'Select Product';
				foreach ($products as $products) {
					$product[$products['product_id']] = $products['product_name'];
				}
			} else {
				$product = FALSE;
			}

//            $this -> fields['row_fields'] =  array('stock_id', 'expiry_date', 'warehouse', 'purchase_order_no', 'delivery_order_no');

//            $this -> fields['table_id'] = $this -> stock_model -> table_id;


            $warehouse = $this-> products_model -> get_warehouses($where_prod);

            if($warehouse){
                $ware[] = "Choose Warehouse";
                foreach($warehouse as $warehouse){
                    $ware[$warehouse['wh_id']] = $warehouse['wh_title'];
                }
            } else {
                $ware = false;
            }

			$this -> fields['products'] = $product;
            $this -> fields['warehouse'] = $ware;
			$this -> fields['package_types'] = FALSE;

			$this -> load -> view('template/header');
			$this -> load -> view('template/content/stock_form', $this -> fields);
			$this -> load -> view('template/footer');

		}

	}

	public function sales_team() {

		$this -> fields['controller'] = 'stock/sales_team';

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('id', 'employee_name', 'device_imei', 'mobile', 'stock_value','commission');

		$this -> fields['table_id'] = 'id';
		$this -> fields['view'] = 'stock/view_team';
		$where = array('employees.account_id' => $this -> account_id, 'emp_role' => 'sales');

		//filtering data

		if ($this -> input -> post('filter')) {
			extract($_POST);
			IF ($date) {
				$where['stock_date'] = $product_name;
			}
			if ($product_name) {
				$where['product_id'] = $product_name;
			}
		}

		$team = $this -> stock_model -> read_sales_team($where);

		if ($team) {
			foreach ($team as $s_team) {
				$sale_team[$s_team['id']] = $s_team['firstname'] . ' ' . $s_team['lastname'];
			}

			$this -> fields['sales_team'] = $sale_team;

			$this -> fields['tb_data'] = $team;

		} else {
            $this -> fields['sales_team'] = 	$this -> fields['tb_data'] = FALSE;
		}
		$this -> fields['tb_name'] = 'team_tb_name';

		$header['stc_active'] = 'class="active"';
		$this -> fields['add_btn'] = 'add_new_stock';

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/sales_team_table', $this -> fields);
		$this -> load -> view('template/footer');
	}

	public function view_team($id) {

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('order_date', 'business_name', 'sales_total', 'total_tax', 'payment_mode', 'receipt_no', 'paid_amount', 'delivery_date','commission');

		$this -> fields['table_id'] = $this -> orders_model -> table_id;
		$this -> fields['view'] = 'stock/view_team';
		$where = array('employees.id' => $id, 'emp_role' => 'sales');

		$team = $this -> stock_model -> read_sales_team($where);

		if ($team) {
			foreach ($team as $team) {
			}
			$this -> fields['team'] = $team;

			//get today sales
			$this -> fields['t_sales'] = $this -> get_latest_sales($team['device_imei'], date('Y-m-d'));
			//teams stock balance
			$this -> fields['summary'] = $this -> team_stock_balance($team['device_imei']);

			$where_team = array('employee_id' => $team['id'], 'sales_orders.account_id' => $this -> account_id);


			$team_orders = $this -> orders_model -> read_order($where_team);
			//	print_r($team_orders);
			if ($team_orders) {
				$this -> fields['tb_data'] = $team_orders;
			} else {
				$this -> fields['tb_data'] = FALSE;
			}

		}
		$this -> fields['tb_name'] = 'team_tb_name';

		$header['stc_active'] = 'class="active"';
		$this -> fields['add_btn'] = 'add_new_stock';

		$this -> load -> view('template/header', $header);
		//$this -> load -> view('template/content/sales_team_view', $this -> fields);
		$this -> load -> view('template/content/sales_team_view', $this -> fields);
		$this -> load -> view('template/footer');

	}

	public function team_stock_balance($team) {

		// 1. --> get total sold items value
		$where_team = array('device_imei' => $team, 'account_id' => $this -> account_id);
		$stock_summary = $this -> stock_model -> team_stock_summary($where_team);

		// 2. --> get total team stock
		$where_team = array('phone_imei' => $team, 'account_id' => $this -> account_id);
		$sales_summary = $this -> stock_model -> team_sales_summary($where_team);

		$summary = array('total_stock' => $stock_summary[0]['total_stock'], 'total_sales' => $sales_summary[0]['total_sales']);

		return $summary;
	}

	public function get_latest_sales($imei, $date) {
		//latest teams summary
		//  1. --> select all orders issued totay

		$latest_orders = $this -> orders_model -> latest_team_summary(array('phone_imei' => $imei), $date);

		if ($latest_orders) {

			$lateorders = array();
			foreach ($latest_orders as $latest) {
				array_push($lateorders, $latest['device_order_id']);
			}

			// 2. ---> select sold items basing on the returned orders
			$latesales = $this -> orders_model -> latest_sales_summary($lateorders);
			//pass data

			$t_sales = $latesales[0]['today_sales'];

			// end of reading latest sales details
		} else {
			$t_sales = 0;
		}
		return $t_sales;
	}

	public function salesteamstock($id) {
		if (!$id) {
			redirect('stock/sales_team');
		}

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('batch_no', 'stock_date', 'status');

		$this -> fields['table_id'] = $this -> products_model -> table_id;
		$this -> fields['view'] = 'stock/view_team';
		$where = array('employees.id' => $id, 'emp_role' => 'sales');

		$team = $this -> stock_model -> read_sales_team($where);

		if ($team) {
			foreach ($team as $team) {
			}
			$this -> fields['team'] = $team;

			$where_team = array('employee_id' => $team['id']);
			$this -> fields['tb_data'] = $this -> stock_model -> sales_team_stock($where_team);

		}

		$this -> fields['products'] = FALSE;
		$this -> fields['tb_name'] = 'team_tb_name';

		$header['stc_active'] = 'class="active"';
		$this -> fields['add_btn'] = 'add_new_stock';

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/team_stock_history', $this -> fields);
		$this -> load -> view('template/footer');

	}

	public function viewbatch($id) {

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('batch_no', 'stock_date', 'product_name', 'stock_qty', 'entry_mode', 'sale_price', 'status');

		$this -> fields['table_id'] = $this -> products_model -> table_id;
		$this -> fields['view'] = 'stock/view_team';

		$where_team = array('batch_no' => $id);
		$stock = $this -> stock_model -> get_sales_team_stock($where_team);

		if ($stock) {
			$this -> fields['tb_data'] = $stock;
		} else {
			$this -> fields['tb_data'] = FALSE;
		}
		//get products
		$products = $this -> products_model -> read(array('products.account_id' => $this -> account_id));

		if ($products) {
			$product[] = 'Select Product';
			foreach ($products as $products) {
				$product[$products['product_id']] = $products['product_name'];
			}
		} else {
			$product = FALSE;
		}
		$this -> fields['products'] = $product;
		$this -> fields['tb_name'] = 'team_tb_name';

		$header['stc_active'] = 'class="active"';
		$this -> fields['add_btn'] = 'add_new_stock';

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/batch_view', $this -> fields);
		$this -> load -> view('template/footer');

	}

	public function team_stock($team) {
		if (!$team) {
			redirect('stock/sales_team');
		}
 
 
		$this -> fields['table_id'] = $this -> products_model -> table_id;
		$this -> fields['view'] = 'stock/view_team';

		$this -> fields['tb_data'] = FALSE;

		//get stock assigned
		$products = $this -> stock_model -> stock_assigned(array('device_imei' => $team, 'sales_team.account_id' => $this -> account_id));

		if (!$products) {

			$products = FALSE;
		}
		$this -> fields['stock_assigned'] = $products;

		//get sold stock
		$sold = $this -> stock_model -> stock_sold(array('phone_imei' => $team, 'sales_order_details.account_id' => $this -> account_id));

		if (!$sold) {

			$sold = FALSE;
		}
		$this -> fields['stock_sold'] = $sold;
		$this -> fields['products'] = FALSE;
		$this -> fields['tb_name'] = 'team_tb_name';

		$header['stc_active'] = 'class="active"';
		$this -> fields['add_btn'] = 'add_new_stock';
		
		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('product_name',  'total_stock', 'stock_sold', 'stock_balance');

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/stock_balance_view', $this -> fields);
		$this -> load -> view('template/footer');

	}

	public function add_stock_to_team() {

		if ($this -> input -> post('team_id') == FALSE) {

			$id = $this -> uri -> segment(4);
			$this -> fields['team_id'] = $id;
			$this -> fields['info'] = FALSE;

			$where = array('employees.id' => $id);
			$team = $this -> stock_model -> read_sales_team($where);
			foreach ($team as $team) {
				$this -> fields['team'] = $team;
			}

			//get products
			$where_prod = array('account_id' => $this -> account_id);
			$products = $this -> products_model -> read($where_prod);

			if ($products) {
				$product[] = 'Select Product';
				foreach ($products as $products) {
					$product[$products['product_id']] = $products['product_name'];
				}
			} else {
				$product = FALSE;
			}
			$this -> fields['products'] = $product;

			$this -> load -> view('template/header');
			$this -> load -> view('template/content/add_team_stock', $this -> fields);
			$this -> load -> view('template/footer');

		} else {

			//adding data to team
			extract($_POST);
			$date = date('Y-m-d h:i:s');

			///batch no

			$batch = $team_id . '.' . date('ymd.his');
			for ($p = 0; $p < count($product_id); $p++) {
				//get product price sales
				$products = $this -> products_model -> read(array('product_id' => $product_id[$p]));

				if ($entry_mode == 'packages') {
					$total_stock = $products[0]['package_units'] * $stock_qty[$p];
				} else {
					$total_stock = $stock_qty[$p];
				}
				$items_on_hand = $products[0]['items_on_hand'] - $total_stock;
                $assigned_stock = $products[0]['assigned_stock']+$total_stock;

				$stock = array('employee_id' => $team_id, 'batch_no' => $batch, 'device_imei' => $device_imei,
                    'entry_mode' => $entry_mode, 'stock_date' => $date, 'stock_qty' => $total_stock,
                    'sale_price' => $products[0]['unit_sale_price'], 'is_synced' => 0,
                    'product_id' => $product_id[$p], 'created_by' => $this -> user_id,
                    'created_date' => $date, 'account_id' => $this -> account_id);

                $currentstock = array('employee_id' => $team_id,'device_imei' => $device_imei,'stock_date' => $date,
                    'assigned_qty' => $total_stock,'sale_price' => $products[0]['unit_sale_price'],
                    'product_id' => $product_id[$p],'account_id' => $this -> account_id,'stock_date' => $date,
                     'updated_date' => $date,'remained_qty'=>$total_stock);

				$stock_balance = array('items_on_hand' => $items_on_hand,'assigned_stock' => $assigned_stock);

				if ($this -> stock_model -> save_team_stock($stock)) {
					$this -> products_model -> update($products[0]['product_id'], $stock_balance);

                    if($this->stock_model->is_product_assigned($currentstock)){
                        $this -> stock_model -> udpdate_current_product($currentstock);
                    } else {
                        $this -> stock_model -> add_new_assignedstock($currentstock);
                    }
				}
			}
			redirect('stock/view_team/' . $team_id);
		}
	}

	public function updateteamstock() {

		if ($this -> input -> post('team_id') == FALSE) {

			$id = $this -> uri -> segment(4);

			$where = array('sales_team.batch_no' => $id);
			$teamstock = $this -> stock_model -> get_sales_team_stock($where);

			if ($teamstock) {
				foreach ($teamstock as $team) {
					$this -> fields['team'] = $team;
				}
				$this -> fields['teamstock'] = $teamstock;
				//get products
				$where_prod = array('account_id' => $this -> account_id);
				$products = $this -> products_model -> read($where_prod);

				if ($products) {
					$product[] = 'Select Product';
					foreach ($products as $products) {
						$product[$products['product_id']] = $products['product_name'];
					}
				} else {
					$product = FALSE;
				}
				$this -> fields['products'] = $product;

				$this -> load -> view('template/header');
				$this -> load -> view('template/content/update_team_stock', $this -> fields);
				$this -> load -> view('template/footer');
			} else {
				redirect('stock/salesteamstock');
			}

		} else {

			//adding data to team
			extract($_POST);
			$date = date('Y-m-d h:i:s');
			for ($p = 0; $p < count($product_id); $p++) {

				//get product price sales
				$products = $this -> products_model -> read(array('product_id' => $product_id[$p]));
				//convert packages to units
				if ($entry_mode == 'packages') {
					$total_stock = $products[0]['package_units'] * $stock_qty[$p];
				} else {
					$total_stock = $stock_qty[$p];
				}

				if ($stock_id[$p] == 'added') {
					$items_on_hand = $products[0]['items_on_hand'] - $total_stock;
					$stock_balance = array('items_on_hand' => $items_on_hand);
				} else {
					//get stock details
					$where = array('sales_team.stock_id' => $stock_id[$p]);
					$teamstock = $this -> stock_model -> sales_team_stock($where);
					//return the pre-assigned them deduct the new value
					$items_on_hand = $products[0]['items_on_hand'] + $teamstock[0]['stock_qty'];

					$items_on_hand = $items_on_hand - $total_stock;
                    $assigned_stock = $products[0]['assigned_stock'] + $total_stock;

				}
				$stock_balance = array('items_on_hand' => $items_on_hand,'assigned_stock' => $assigned_stock);
				if ($stock_id[$p] == 'added') {

					//add addition stock to the batch
					$stock = array('employee_id' => $team_id, 'batch_no' => $batch_no,
                        'device_imei' => $device_imei, 'entry_mode' => $entry_mode,
                        'stock_date' => $date, 'stock_qty' => $total_stock,
                        'sale_price' => $products[0]['unit_sale_price'], 'is_synced' => 0,
                        'product_id' => $product_id[$p], 'created_by' => $this -> user_id,
                        'created_date' => $date, 'account_id' => $this -> account_id);

					$this -> stock_model -> save_team_stock($stock);
					//updating ware house stock

				} else {
					//update the current stock
					$stock = array('entry_mode' => $entry_mode, 'stock_qty' => $total_stock,
                        'sale_price' => $products[0]['unit_sale_price'],
                        'is_synced' => 0, 'product_id' => $product_id[$p]);
/*
                    $currstck = array('stock_qty'=>$total_stock, 'sale_price' => $products[0]['unit_sale_price'],
                        'product_id' => $product_id[$p], 'updated_date' => $date);*/

					$this -> stock_model -> update($stock_id[$p], $stock);

				}
				//updating ware house stock
                $currentstock = array('employee_id' => $team_id,'device_imei' => $device_imei,'stock_date' => $date,
                    'assigned_qty' => $total_stock,'sale_price' => $products[0]['unit_sale_price'],
                    'product_id' => $product_id[$p],'account_id' => $this -> account_id,'stock_date' => $date,
                    'updated_date' => $date);
                $this -> stock_model ->assign_current_product($currentstock);
				$this -> products_model -> update($products[0]['product_id'], $stock_balance);
			}
			redirect('stock/view_team/' . $team_id);
		}
	}

	public function get_product_price($id) {

		$where_prod = array('product_id' => $id);
		$products = $this -> products_model -> read($where_prod);

		if ($products) {

			foreach ($products as $products) {
			}
			echo '<label class="negative">' . $products['unit_sale_price'] . '</label>';

		} else {
			$product = FALSE;
		}

	}

	public function delete() {
		$id = $this -> uri -> segment(4);

		$this -> stock_model -> delete($id);
		redirect('stock');

	}

	public function puchases() {

		$this -> fields['tb_name'] = 'stock_tb_name';

		$header['stc_active'] = 'class="active"';
		$this -> fields['add_btn'] = 'add_new_stock';
		$this -> load -> view('template/header');
		$this -> load -> view('template/content/table', $this -> fields);
		$this -> load -> view('template/footer');

	}

    public function current_stock_info($team){
        $this -> fields['tb_headers'] = array('product_name', 'stock_qty', 'sales_person', 'stock_date','updated_date');

        if (!$team) {
            redirect('stock/sales_team');
        }


//        $recentbatch = $this -> stock_model -> get_recent_batch($team);
        //get current stock assigned
//        if($recentbatch)
        $products = $this -> stock_model -> get_current_stock_assigned($team);

        if (!$products) {

            $products = FALSE;
        }


        $this -> fields['current_stock'] = $products;
        $this -> fields['products'] = FALSE;
        $this -> fields['tb_name'] = 'team_tb_name';

        $header['stc_active'] = 'class="active"';
        $this -> fields['add_btn'] = 'add_new_stock';

        $this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('product_name',  'sales_person', 'stock_qty','remained_qty', 'stock_date','updated_date');

        $this -> load -> view('template/header', $header);
        $this -> load -> view('template/content/current_stock_information', $this -> fields);
        $this -> load -> view('template/footer');
    }
}
