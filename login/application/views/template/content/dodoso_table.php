<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealAddBoundary" class="modal fade hide" style="width: 800px;margin-left:-385px;">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2>Dodoso</h2>
					  
  <a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php //echo site_url('dodoso/report'); ?>" style="float: right; padding:8px;"><img src="<?php echo base_url('template/img/icons/excel1.png');?>" alt="fd" > <?php _l('pakua'); ?></img></a>

            </header> 

        </article>

    </div>	
    <div class="row">
 
		<!-- Data block -->
					<article class="span2">

						<article class="red data-block accordion-block">
							<div id="accordionName" class="accordion">
							
	<?php 

foreach($mikoa as $mikoa){ 
	?>						
							
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle" href="#<?php echo $mikoa['mkoa'];?>" data-parent="#accordionName" data-toggle="collapse"><?php echo $mikoa['mkoa'];?></a>
									</div>
									<div id="<?php echo $mikoa['mkoa'];?>" class="accordion-body collapse">
										<div class="accordion-inner"> 
					<ul>
				<?php 
			
				 foreach($wilaya as $wilayas){
				  	 if($mikoa['mkoa'] == $wilayas['mkoa']) {				
				?>	
<li><a href="<?php echo site_url('dodoso/index');?>/<?=$wilayas['wilaya'];?>" style="color:#000;" > <?php  echo $wilayas['wilaya']; ?> </a>
		<ul>
		<?php	 
  foreach($kata as $katas){ 	 
				   if($wilayas['wilaya'] == $katas['wilaya']) {   	
						 echo '<li><a href="'.site_url('dodoso/index').'/'.$katas['kata'].'" >'.$katas['kata'].'</a></li>';		
						 }
 				 
				}  
					?>
		</ul>
</li>					
 				<?php  } }  ?>
					</ul>	 									
										 </div>
									</div>
								</div>
								<?php } ?>
						
							</div>
						</article>
						
					</article>
					<!--Data block -->
        <article class="span10 data-block">
  <!--
				<header>

				    <h2><span class="icon-folder-open" style="padding-right:10px;"></span><?php _l($tb_name); ?></h2>
                     
				</header>
 -->
                <section>
 <div class="row" style="height:30px;">
 
 <form method="post" action="<?php echo site_url('dodoso');?>" style="position: absolute;top: 15px;right: 30px;" class="form-inline no-margin">
 				 
               <?php
               
                     //phone list
                       	$mobs[]='select device name';
                      	if($phones) {
		                       foreach ($phones as $mob){
													$mobs[$mob['phoneid']] = $mob['device_name'];                        
		                        }  
                         }else { $mobs[]='No Devices registered'; }
                     
                        echo form_dropdown('phones',$mobs);	
                 ?>
 
                    <input class="show_date_piker input-small"   type="text" name="start_date"  style="height: 15px;width: 140px;" placeholder="<?php _l('start_date'); ?>"  />

                    <input class="show_date_piker input-small" name="end_date" type="text"  style="height: 15px;width: 140px;" placeholder="<?php _l('end_date'); ?>"  />

                    <input class="btn btn-small" type="submit" id="submit_search" name='chuja'value="<?php _l('search'); ?>" onclick="javascript:void(0);"/>

                </form> </div>
   <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  <thead>
 			<tr>
 				   <?php foreach($tb_headers as $header){?>
	 				 	<th><?php _l($header); ?></th>
 							  <?php }?><th>Actions</th>
						 	</tr>
								</thead>
 <tbody>
 	  <?php if (isset($tb_data) && $tb_data != 0){
									
									foreach ($tb_data as $data):  
									 if (isset($data['id'])){
                                            $id = $data['id'];
                                    }elseif(isset($data['phoneid'])){
                                            $id = $data['phoneid'];
                                    }else{
                                        $id = '';
                                    }?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
                                        <?php foreach ($row_fields as $field): ?>
                                        <!-- Output maswali -->
 <td><?php echo '<a href="'.site_url('dodoso/view').'/'.$data['namba_ya_dodoso'].'">'.$data[$field].'</a>';?></td>
                                        <?php endforeach; ?>
                                         <?php 
                                        //the percentage calculate
                                        /* if($data['answers'] !=0) {
                                        $asilimia= ($data['answers'] / count($maswali) *100);}else {echo $data['answers']; }
                                        echo number_format((float)$asilimia,2, '.', '').'%';
                                      */
  	?>  <td><a data-toggle="modal" href="#revealModal" style="text-decoration: none;padding-left: 40px;"><span class="icon-eye-open" id="<?php echo site_url('reports/showmap/'.$data['imei'].'/'.$data['latitude'].'/'.$data['longitude']);?>" ></span></a></td>
									</tr>
									<?php endforeach;
									 
										}else {
										echo '<tr> <td colspan="6" style="text-align:center;"><div class="alert alert-block fade in no-margin">Hakuna taarifa</div> <td> </tr>';
											}									
									?>
								</tbody>
							</table> 
          </section>

        </article>

   

    </div>

</section>
