<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Users extends My_Controller {
	private $account_id;
	public $users_dsc = array('location', 'username', 'data_collector_no', 'role', /* 'permissions', 'status',*/ );

	public function __construct() {

		parent::__construct();

		if (!$this -> session -> userdata('is_login') == TRUE) {
			$this -> session -> set_userdata('login_error', 'Please, Loggin first');
			redirect('login');
		}

		if (!in_array(1, $this -> user_permissions)) {

			redirect('login/logout');
		}

		$this -> load -> model('users_model');

		$this -> users_model -> form_validation();
		$this -> load -> model('locations_model');
		$this -> load -> model('permissions_model');

		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');

		$this -> data['row_fields'] = $this -> users_dsc;
		$this -> data['tb_headers'] = $this -> users_dsc;

		$this -> data['tb_name'] = 'all_users';
		$this -> data['edit'] = 'users/update_user/';
		$this -> data['delete'] = 'users/delete/';

		$this -> data['view'] = "users/view/";

		$this -> data['controller'] = 'users';
		$this -> data['table_id'] = $this -> users_model -> table_id;
		$this -> account_id = $this -> session -> userdata('account_id');

	}

	public function index() {

		//the search from
		$this -> data['controller'] = 'users';
		$where = array('account_no' => $this -> session -> userdata('account_id'));

		$this -> session -> unset_userdata('id');
		if ($this -> input -> post('filter')) {
			extract($_POST);

			if ($username) {
				$where['username'] = $username;
			}

			if ($role) {
				$where['role'] = $role;
			}
		}

		$users = $this -> users_model -> read($where);
		if ($users) {
			$this -> data['tb_data'] = $users;
		} else {
			$this -> data['tb_data'] = FALSE;
		}

		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/users_table');
		$this -> load -> view('template/table_helper');
		$this -> load -> view('template/footer');

	}

	//viewing
	public function view() {

		$id = $this -> uri -> segment(4);

		$where = array('users.user_id' => $id);
		//geting the data
		$this -> data['tb_data'] = $this -> users_model -> read($where);

		$data['rudi'] = 'users';
		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/prop_view', $data);
		$this -> load -> view('template/footer');

	}

	public function new_user() {
		if ($this -> form_validation -> run() == FALSE) {

			$where_f = array('account_no' => $this -> account_id);
			//locations
			$locations = $this -> locations_model -> read($where_f);
			$location = array('' => 'Assign Facility', '00' => 'Manage All Facilities');

			if ($locations) {

				foreach ($locations as $locations) {
					$location[$locations['location_code']] = $locations['location_name'];
				}
			} else {
				$location[''] = 'No Facility available';
			}
			$data['locations'] = $location;
			$data['info'] = FALSE;
			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/form_user', $data);
			$this -> load -> view('template/footer');
		} else {
			extract($_POST);

			//set permissions
			$permissions = 0;
			$user = array('firstname' => $firstname, 'lastname' => $lastname, 'location' => $location, 'username' => $username, 'password' => sha1($password), 'role' => $role, 'permissions' => $permissions, 'account_no' => $this -> session -> userdata('account_id'));

			$this -> users_model -> save($user);

			redirect('users');

		}
	}

	public function update_user() {
		if ($this -> form_validation -> run() == FALSE) {
			if ($this -> session -> userdata('id')) {
				$id = $this -> session -> userdata('id');
			} else {
				$id = $this -> uri -> segment(4);
				$this -> session -> set_userdata('id', $id);
			}
			$where = array('users.user_id' => $id);
			//geting the data

			$info = $this -> users_model -> read($where);

			if ($info) {
				foreach ($info as $info) {
					$data['info'] = $info;
				}
			} else {
				//redirect('users');
			}

			//locations
			$where_f = array('account_no' => $this -> account_id);
			$locations = $this -> locations_model -> read($where_f);
			$location = array('' => 'Assign Facility', '00' => 'Manage All Facilities');

			if ($locations) {
				foreach ($locations as $locations) {
					$location[$locations['location_code']] = $locations['location_name'];
				}
			} else {
				$location[''] = 'No Facility available';
			}
			$data['locations'] = $location;

			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/form_user', $data);
			$this -> load -> view('template/footer');

		} else {
			$this -> session -> unset_userdata('id');
			extract($_POST);

			$permissions = 0;
			$user = array('firstname' => $firstname, 'lastname' => $lastname, 'location' => $location, 'role' => $role, 'permissions' => $permissions, );
			if ($username) {
				$user['username'] = $username;
			}
			if ($password) {
				$user['password'] = sha1($password);
			}
			print_r($user);
			$this -> users_model -> update($user_id, $user);

			redirect('users');

		}
	}

	public function delete() {
		$id = $this -> uri -> segment(4);
		$data = array('status' => 0);
		if ($this -> users_model -> update($id, $data)) {
			redirect('users');
		}
	}

}
