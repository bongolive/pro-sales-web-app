                    <div class="modal-header">

									<h4><?php _l('Update_Permission'); ?></h4>

								</div>
								
                                
                                 <?php foreach ($permissions as $permission): ?>
                                <form class="form-horizontal" action="<?php echo site_url("admin/updatePermission"); ?>" method="post">

                                    <fieldset>
                                        <div class="modal-body">
                                        <div class="control-group">

                                        	<label class="control-label" for="permission_name" ><?php _l('permission_name'); ?></label>

                                        	<div class="controls">

                                        	<input name="permission_name" id="permission_name" class="input-xlarge" type="text" value="<?php echo $permission['permission_name'];?>">
                                        	<input name="permissionid" type="hidden" id="permissionid" value="<?php echo $permission['id'];?>" />
                                        	</div>

                                    	</div>
								</div>

								<div class="modal-footer">

									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>

									<div class="form-actions">

            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;padding: 6px 10px;">Submit</button>

                                    </div>

								</div>

                            </fieldset>

                        </form>
                        <?php endforeach; ?>