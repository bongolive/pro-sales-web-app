<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript">
		

		function loadMap () {

			var markers = [];
			markers.length = 0;
			var xmlDocument ;

			jQuery.ajax({
				url: "<?php echo site_url('tracking/gettrackerlocation'); ?>",//sending ajax request to our php file
				data: xmlDocument,//storing the data in xmlDocument cariable
				success: function(xmlDocument) {
					jQuery(xmlDocument).find("location").each(function()
					{
						var loc = { "device_imei" : jQuery(this).find("device_imei").text(), 
									"lat" : jQuery(this).find("lat").text(),
									"lng" : jQuery(this).find("lng").text(), 
									"device_date" :  jQuery(this).find("device_date").text(),
									"device_name" : jQuery(this).find("device_name").text(),
									"device_mobile" : jQuery(this).find("device_mobile").text() };
						//console.log(loc);
						markers.push(loc);

			var mapOptions = {
				center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
				zoom: 10,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
			var infoWindow = new google.maps.InfoWindow();
			var lat_lng = new Array();
			var latlngbounds = new google.maps.LatLngBounds();
			for (i = 0; i < markers.length; i++) {
				var data = markers[i]
				var myLatlng = new google.maps.LatLng(data.lat, data.lng);
				lat_lng.push(myLatlng);
				var marker = new google.maps.Marker({
					position: myLatlng,
					map: map,
					title: data.device_name
				});
				latlngbounds.extend(marker.position);
				(function (marker, data) {
					google.maps.event.addListener(marker, "click", function (e) {
						
						var content = "<strong>Customer Name : </strong>"+data.device_name+"<br>";
							content += "<strong>IMEI : </strong>"+data.device_imei+"<br>";
							content += "<strong>Mobile Number : </strong>"+data.device_mobile+"<br>";
							content += "<strong>Lat : </strong>"+data.lat+"<br>";
							content += "<strong>Lng : </strong>"+data.lng+"<br>";
							content += "<strong>Date : </strong>"+data.device_date+"<br>";

						infoWindow.setContent( content );
						infoWindow.open(map, marker);
					});
				})(marker, data);
			}
			map.setCenter(latlngbounds.getCenter());
			map.fitBounds(latlngbounds);

			//***********ROUTING****************//

			//Intialize the Path Array
			var path = new google.maps.MVCArray();

			//Intialize the Direction Service
			var service = new google.maps.DirectionsService();

			//Set the Path Stroke Color
			var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });

			//Loop and Draw Path Route between the Points on MAP
			for (var i = 0; i < lat_lng.length; i++) {
				if ((i + 1) < lat_lng.length) {
					var src = lat_lng[i];
					var des = lat_lng[i + 1];
					path.push(src);
					poly.setPath(path);
					service.route({
						origin: src,
						destination: des,
						travelMode: google.maps.DirectionsTravelMode.DRIVING
					}, function (result, status) {
						if (status == google.maps.DirectionsStatus.OK) {
							for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
								path.push(result.routes[0].overview_path[i]);
							}
						}
					});
				}
			}

					});

					//console.log(locations);

				}
			});

		}
	</script>
</head>
<body onload="loadMap();">

<div id="dvMap" style="width: 100%; height: 500px">
</div>
</body>
</html>
