<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php
									if ($info) {
									 
										
										echo lang('update_survey') . ' : ' . strtoupper($info['survey_title']);
										$action = 'surveys/update';
									} else {
										echo lang('new_surveys');
										$action = 'surveys/create';
									}
							 ?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                
                                	  <form role="form" method="post" action="<?php echo site_url($action); ?>">
                                	  	<div class="box-body">
                                	                                	<?php if($info){ ?>
                                		<input name="survey_id" id="survey_id" class="form-control" type="hidden"  value="<?php  echo $info['survey_id']; ?>" />
                                		<?php } ?> 
                                       
                                        <div class="form-group">
                                        	<label class="control-label" for="survey_name" ><?php echo lang('survey_title'); ?></label>
                                        	<?php echo form_error('survey_title'); ?>
                                        	<input name="survey_title" id="survey_name" class="form-control" type="text"  value="<?php
											if ($info) { echo $info['survey_title'];
											} else { $value = set_value();
											}
 ?>" required />
                                        
                                    	</div> 
                                    	 <div class="form-group">
                                        	<label class="control-label" for="descriptions" ><?php echo lang('descriptions'); ?></label>
                                        	<?php echo form_error('descriptions'); ?>
                                        	<textarea name="descriptions" id="descriptions" class="form-control" style="" ><?php
											if ($info) { echo $info['descriptions'];
											} else { $value = set_value('descriptions');
											}
											?></textarea>  
                                     	</div>
                                    	  <div class="form-group">
                                        	<label class="control-label" for="key_word" ><?php echo lang('survey_key_word'); ?></label>
                                        	<?php echo form_error('key_word'); ?>
                                        	<input name="key_word" id="key_word" class="form-control" type="text"  value="<?php
											if ($info) { echo $info['key_word'];
											} else { $value = set_value();
											}
 ?>" required />
                                        	
                                    	</div>
                                    	  <div class="form-group">
                                        	<label class="control-label" for="no_questions" ><?php echo lang('no_questions'); ?></label>
                                        	<?php echo form_error('questions'); ?>
                                        	<input name="no_questions" id="no_questions" class="form-control" type="number"  value="<?php
											if ($info) { echo $info['no_questions'];
											} else { $value = set_value();
											}
 ?>" />

                                    	</div> 
                                     
                                    	  
                                    		<div class="form-group">
                                        	<label class="control-label" for="start_date" ><?php echo lang('start_date'); ?></label>
                                        	
                                        		<?php echo form_error('start_date'); ?>
                                        	<input name="start_date" id="description" class="form-control" type="date" value="<?php
											if ($info) { echo $info['start_date'];
											} else { $value = set_value();
											}
 ?>"required>
                                    	</div>
                                    		<div class="form-group">
                                        	<label class="control-label" for="end_date" ><?php echo lang('end_date'); ?></label>
                                        	<?php echo form_error('end_date'); ?>
                                        	<input name="end_date" id="description" class="form-control" type="date" value="<?php
											if ($info) { echo $info['end_date'];
											} else { $value = set_value();
											}
											?>" required>
                                        	
                                    	</div>
                                    	<!--<div class="form-group">
                                        	<label class="control-label" for="cap_size" ><?php echo lang('cap_size'); ?></label>
                                        	<?php echo form_error('cap_size'); ?>
                                        	<input name="cap_size" id="description" class="form-control" type="number" value="<?php
											if ($info) { echo $info['cap_size'];
											} else { $value = set_value();
											}
 ?>"  required>
                                        	
                                    	</div> -->
                                      	<div class="form-group">
                                        	<label class="control-label" for="assigned_to" ><?php echo lang('assigned_to'); ?></label>
                                        	<?php echo form_error('assigned_to');
											$assignee = array('0'=>'Normal Users','1'=>'Survey Administrators');
											if($info){
												$assg = $info['assigned_to'];
											}else{
												$assg = set_value('assigned_to');
											}
											
											echo form_dropdown('assigned_to',$assignee,$assg,'class="form-control"');
											 ?>
                                        	  
                                    	</div>
                                    	         
								</div>
								<div class="modal-footer">
									
									<div class="form-actions">
										<a href="<?php echo site_url('surveys'); ?>" type="button" class="btn btn-default" data-dismiss="modal">Back</a>
            				                <button class="btn btn-alt btn-medium btn-primary" type="submit">Save</button>
                                    </div>
								</div>
								</div></div></div></section>
