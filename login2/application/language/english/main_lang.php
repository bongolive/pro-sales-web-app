<?php



/**

 * This file carries the main values for language in English

 */



$lang['device_pg_title'] = 'Device';

$lang['track_pg_title'] = 'Track'; 

$lang['task_pg_title'] = 'Tasks';

$lang['reports_pg_title'] = 'Reports';

$lang['customers_pg_title'] = 'Customers';

$lang['profile_pg_title'] = 'Profile';

$lang['boundary_pg_title'] = 'Boundaries';

$lang['support_pg_title'] = 'Support';

$lang['login_pg_title'] = 'Login';

$lang['welcome_pg_title'] = 'Welcome';

 
$lang['kijiji'] = 'Kijiji';
$lang['namba_ya_dodoso'] = 'Dodoso No.';
$lang['namba_ya_mwanamke'] = 'Mwanamke No.';
$lang['tarehe'] = 'Tarehe';
$lang['msimamizi'] = 'Msimamizi';



// Table fields



$lang['device_tb_name'] = 'Devices';

$lang['boundaries_tb_name'] = 'Boundaries';

$lang['reports_tb_name'] = 'Reports';

$lang['customers_tb_name'] = 'Customers';

$lang['tasks_tb_name'] = 'Tasks';

$lang['track_tb_name'] = 'Track';

$lang['user_tb_name'] = 'Users';

$lang['permission_tb_name'] = 'Permissions';



// Table Field names



$lang['device_name'] = 'Device Name';

$lang['subject'] = 'Subject';

$lang['task'] = 'Task';

$lang['status'] = 'Status';

$lang['date'] = 'Date';

$lang['action'] = 'Actions';

$lang['device_imei'] = 'Device IMEI';

$lang['track_intv'] = 'Tracking Interval';

$lang['mobile_nmber'] = 'Mobile Number';

$lang['holder_name'] = 'Name of Holder';

$lang['alert_name'] = 'Alert Name';

$lang['devices'] = 'Device(s)';

$lang['valid_from'] = 'Valid From';

$lang['valid_to'] = 'Valid To';

$lang['boundary'] = 'Boundary';

$lang['report'] = 'Report';

$lang['comments'] = 'Comments';

$lang['location'] = 'Location';

$lang['customer_name'] = 'Customer Name';

$lang['mobile_number'] = 'Mobile Number';

$lang['email'] = 'Email';

$lang['address'] = 'Address';

$lang['valid_till'] = 'Valid Till';

$lang['user_type'] = 'User Type';

$lang['permission_name'] = 'Permission Name';

$lang['is_deleted'] = 'Is Deleted';


// Tabs

$lang['device'] = 'Device';

$lang['support'] = 'Support';

$lang['track'] = 'Track';

$lang['reports'] = 'Reports';

$lang['customers'] = 'Customers';

$lang['boundaries'] = 'Boundaries';

$lang['tasks'] = 'Tasks';

$lang['users'] = 'Users';

$lang['permissions'] = 'Permissions';





// add Device

$lang['email_reports'] = 'Email Reports';

$lang['enable_email_reports'] = 'Enable Email Reports';

$lang['add_new_device'] = 'Add New Device';



// Add Task



$lang['add_task'] = 'Add Task';

$lang['new_customer'] = 'Add Customer';

$lang['add_new_permission'] = 'New Permission';

$lang['add_new_user'] = 'Add User';

$lang['new'] = 'New';

$lang['in_progress'] = 'In Progress';

$lang['done'] = 'Done';

$lang['update_permission'] = 'Update Permission';

$lang['update_client_permission'] = 'Update Client Permissions';

$lang['client_permissions'] = 'Client Permissions';



// add Boundary

$lang['add_boundary'] = 'Add Boundary';

$lang['edit_boundary'] = 'Edit Boundary';

$lang['step_one'] = 'Step 1 (Defining Boundary)';



// Edit Device

$lang['edit_device'] = 'Edit this Device';

$lang['update_customer'] = 'Update Customer Record';

$lang['update_user'] = 'Update User';


// Table reveal modal

$lang['confirm_deletion'] = 'Confirm Deletion';

$lang['are_you_sure'] = 'Are you sure';



//Tracking 



$lang['start_date'] = 'Start Date';

$lang['end_date'] = 'End Date';

$lang['search'] = 'Search';



//Downloading forms



$lang['txt_task'] = 'Tasks';

$lang['all_task'] = 'All Tasks';

$lang['all_dates'] = 'All Dates';

$lang['all_report'] = 'All Reports';

$lang['all_subject'] = 'All Subjects';

$lang['download_task'] = 'Download Tasks';

$lang['download_report'] = 'Download Report';


//customers form






$lang['stocks_tb_name'] = 'Device Stocks';
$lang['product_name'] = 'Product Name';
$lang['items_received'] = 'Received';
$lang['items_issued'] = 'Issued';
$lang['items_returned'] = 'Returned';
$lang['returned_items'] = 'Returned';
$lang['stock_date'] = 'Stock Date';
$lang['add_new_stock'] = 'Assign Stock To Devices';
$lang['products_tb_name'] = 'Products';
$lang['description'] = 'Description';
$lang['items_on_hand'] = 'Items on Hand';
$lang['add_new_product'] = 'Add New Product';
$lang['sales_tb_name'] = 'Product Sales';
$lang['client_name'] = 'Name';
$lang['sales_datetime'] = 'Sales Date';
$lang['quantity'] = 'Quantity';
$lang['price'] = 'Price';
$lang['clients_tb_name'] = 'Customers';
$lang['phone'] = 'Phone';
$lang['shop_name'] = 'Shop Name';
$lang['add_new_client'] = 'Add New Customer';
$lang['returned_stocks_tb_name'] = 'Returned Stock';


$lang['update_product'] = 'Update Product';
$lang['update_device_stock'] = 'Updating Device Stock';
$lang['return_device_stock'] = 'Returned Device Stock';
$lang['update_client'] = 'Updating Customer Information';

$lang['products'] = 'Products';
$lang['returns'] = 'Return';
$lang['sales'] = 'Sales';
$lang['stock'] = 'Stock';

$lang['add_product_stock'] = 'Add Product Stock';
$lang['items_to_add'] = 'Items To Add';





