<?php

/**
 * This file carries the login values for language in swahili
 */

$lang['welcome_user'] = 'Karibu Mtumiaji';
$lang['invalid_login'] = 'Umekosea Jina au Neno la siri';
$lang['payment_expire'] = 'Malipo yamekwisha';
$lang['enter_your_name'] = 'Ingiza Jina lako';
$lang['password'] = 'Neno La Siri';
$lang['login'] = 'Ingia';
$lang['forget_password'] = 'Umesahau neno lako la Siri';
$lang['submit_email'] = 'Tuma Barua Pepe';
$lang['enter_email'] = 'Ingiza Barua Pepe';
$lang['no_email_client'] = 'Hamna Mtumiaji mwenye hiyo barua pepe';
$lang['email_empty'] = 'Hujaingiza barua pepe';
$lang['please__check_email'] = 'Tafadhali anagalia barua pepe zako';