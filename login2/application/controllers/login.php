<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();;
		$this->load->model('user_model');
        $this->lang->load('login');
	}
	
	public function index()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$data['msg'] = "";
		$data['errorflag'] =0;
		
		
		if(!empty($username)  && !empty($password))
		{
			$data = array(
				'username' => $username,
				'password' => md5($password)
			);
			
			$arrUser = $this->user_model->getUserByUsernameAndPassword($data);
			if(count($arrUser)>=1)
			{
				
				$today = date("Y-m-d H:i:s");
				$expire = $arrUser['valid_till'];

				$today_time = strtotime($today);
				$expire_time = strtotime($expire);
				
				//Assign Userdata to session
				
				//redirecting user
				if($arrUser['user_type']=='Administrator'){
					 $newdata = array(
                  		 'userid'  => $arrUser['userid'],
                   		'username'     => $arrUser['name'],
						'user_type'     => $arrUser['user_type'],
                   		'logged_in' => TRUE
               		);
					$this->session->set_userdata($newdata);
					 
					 redirect('en/admin/');
				}else if($arrUser['user_type']=='Client'){
					if ($expire_time >= $today_time) {
						 $newdata = array(
                  		 	'userid'  => $arrUser['userid'],
                   			'username'     => $arrUser['name'],
							'user_type'     => $arrUser['user_type'],
                   			'logged_in' => TRUE
               			);
						$this->session->set_userdata($newdata);
					 
					 	redirect('/track/', 'refresh');
					}else{
						$data['msg'] = 'payment_expire';
						$data['errorflag'] =1;
						$this->load->view('template/login',$data);
					}
				}else{
					$data['msg'] = 'invalid_login';
					$data['errorflag'] =1;
					$this->load->view('template/login',$data);
				}
			}
			else
			{
				$data['msg'] = 'invalid_login';
				$data['errorflag'] =1;
				$this->load->view('template/login',$data);
			}
			
		} else
		{
			$this->load->view('template/login',$data);
		}
	}
	
	
	
	
	public function sendforgotpassword()
	{
	   
       $data['msg'] = '';
	   $data['errorflag'] =0;
	   if($this->input->post('mail') !=""){
	   		$cond = array('email' => $this->input->post('mail'));
			$user = $this->user_model->get_user($cond);
			
			if(count($user)>0){
				$pass= gen_md5_password(8);
				$to =  $user['email'];
				$body = "Your new password for username ".$user['username']." is ".$pass; 
				

				$this->load->library('email');
				$this->email->from('info@pro.co.tz', 'ForgotPassword');
				$this->email->to($to);
				//$this->email->cc('another@another-example.com');
				//$this->email->bcc('them@their-example.com');

				$this->email->subject('Forgot Password for Footprint');
				$this->email->message($body);

				$this->email->send();

				//echo $this->email->print_debugger();
				$data = array('password'=> md5($pass));
				$user = $this->user_model->update($data,$cond);
                $data['errorflag'] = 0;
				$data['msg'] = 'please__check_email';
	   		}else{
				$data['msg'] = 'no_email_client';
				$data['errorflag'] =1;
			}
	   }else{
	       $username = $this->input->post('username');
		   $password = $this->input->post('password');
	       if(($username) && ($password)){
	           $this->index();
               return;
	       }else{
	           $data['msg'] = 'email_empty';
			   $data['errorflag'] =1;
	       }
	   		
	   }
	   $this->load->view('template/login',$data);
	}
	
	
	
	
	
}