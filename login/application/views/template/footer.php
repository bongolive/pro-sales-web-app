<!-- Sticky footer push -->

<div id="push"></div>

</div>

<!-- /Full height wrapper -->

<!-- Main page footer -->

<footer id="footer">

	<div class="container">
		<!-- Footer info -->
		<p>
			Copyright © 2013. All Rights Reserved.
		</p>

		<!-- Footer back to top -->

		<a href="#top" class="btn btn-alt"><span class="icon-arrow-up"></span></a>

	</div>

</footer>
<div id="loadingGif" style="display: none;">
	<article class="span3 blue data-block" style="margin-top: 26px;margin-left: 25%;">
		<header>
			<h2>Loading&hellip;</h2>
			<ul class="data-header-actions">
				<li>
					<span class="loading blue" title="Loading, please wait&hellip;">Loading&hellip;</span>
				</li>
			</ul>
		</header>
	</article>
</div>

<!-- /Main page footer -->

<!-- Bootstrap scripts -->

<?php get_js('bootstrap/bootstrap.min.js'); ?>

<!-- Load the custom JS -->

<?php
if (isset($js)) {
	get_js($js);
}
?>
<script type="text/javascript" src="js/plugins/dateRangePicker/date.js"></script>
<script type="text/javascript" src="js/plugins/dateRangePicker/daterangepicker.js"></script>

<script>
	function editContent(id) {
		var dataString = 'post_type=ajax';
		$("#revealModal").html(loading);
		$.ajax({
			type : "POST",
			url : id,
			data : dataString,
			cache : false,
			success : function(kk) {
				$("#revealModal").html(kk);
			}
		});
	}


	$(document).ready(function() {
 
		$('.datepicker').datepicker({
			"autoclose" : true
		});

		$('#dp1').daterangepicker({
			format : 'yy-mm-dd'
		});

		$('.show_date_piker').datetimepicker();

		// Tooltips

		$('[title]').tooltip({

			placement : 'top',

			container : 'body'

		});

		// Tabs

		$('.demoTabs a, .demoTabs2 a').click(function(e) {

			e.preventDefault();

			$(this).tab('show');

			$('.fullcalendar').fullCalendar('render');
			// Refresh jQuery FullCalendar for hidden tabs

		});
 
	});

</script>

<script>
	var clickedRow;
	var loading = $('#loadingGif').html();

	$(document).ready(function() {
  
		$(".icon-pencil-boundary").click(function() {
			$('#editBoundary').attr('src', this.id);

			//$('#editBoundary').attr('href', this.id);

		});

		$("#confirmDeleteionButton").click(function() {
			var rowID = clickedRow;
			var form = $(this).closest('form')[0];
			var hold = $('#' + form.id).html();
			alert(rowID);
			$("#revealDelete").html(loading);
			$.ajax({
				type : "POST",
				url : form.action,
				data : 'delete=ajax',
				cache : false,
				success : function(html) {
					if (html == 1) {
						//$('#revealDelete').close();
						$('#' + rowID).remove();
						$("#revealDelete").html(hold);
					} else {
						$("#revealDelete").html(hold);
					}
				}
			});
			return false;
		});

		$(".icon-trash").click(function() {
			var dataString = 'post_type=ajax';
			$("#form-delete-confirm").attr('action', this.id);
			var row = $(this).closest('tr')[0];
			clickedRow = row.id;
		});

		$(".icon-eye-open").click(function() {
			var dataString = 'post_type=ajax';
			$("#revealModal").html(loading);
			//$("#revealModal").css('width','770px');
			$.ajax({
				type : "POST",
				url : this.id,
				data : dataString,
				cache : false,
				success : function(html) {
					$("#revealModal").html(html);
				}
			});
		});
		

	});

	/*
	 *   new scripts for automatic adding rows and deleteing
	 */
	function addRow(tableID) {

		var table = document.getElementById(tableID);

		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);

		var colCount = table.rows[0].cells.length;

		for (var i = 0; i < colCount; i++) {

			var newcell = row.insertCell(i);

			newcell.innerHTML = table.rows[0].cells[i].innerHTML;
			//alert(newcell.childNodes);
			switch(newcell.childNodes[0].type) {
			case "text":
				newcell.childNodes[0].value = "";
				break;
			case "checkbox":
				newcell.childNodes[0].checked = false;
				break;
			case "select-one":
				newcell.childNodes[0].selectedIndex = 0;
				break;
			}
		}
	}

	function deleteRow(tableID) {
		try {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;

			for (var i = 0; i < rowCount; i++) {
				var row = table.rows[i];
				var chkbox = row.cells[0].childNodes[0];
				if (null != chkbox && true == chkbox.checked) {
					if (rowCount <= 1) {
						alert("Cannot delete all the rows.");
						break;
					}
					table.deleteRow(i);
					rowCount--;
					i--;
				}
			}
		} catch(e) {
			alert(e);
		}
	}

	/*
	 *   new scripts for automatic adding rows and deleteing
	 */

	/*
	 *   new scripts for automatic adding rows and deleteing
	 */

</script>
<script>
	$(document).ready(function() {
	 
		$('#demoDaterangePicker').daterangepicker({
			format : 'yy/MM/dd'
		});

		// Draw graph via function
		function responsiveVisualize() {
			$('.visualize-chart').each(function() {
				var chartType = '';
				// Set chart type
				var chartWidth = $(this).parent().width() * 0.9;
				// Set chart width to 90% of its parent

				if (chartWidth < 350) {
					var chartHeight = chartWidth;
				} else {
					var chartHeight = chartWidth * 0.4;
				}

				if ($(this).attr('data-chart')) {// If exists chart-chart attribute
					chartType = $(this).attr('data-chart');
					// Get chart type from data-chart attribute
				} else {
					chartType = 'area';
					// If data-chart attribute is not set, use 'area' type as default. Options: 'bar', 'area', 'pie', 'line'
				}

				if (chartType == 'line' || chartType == 'pie') {
					$(this).hide().visualize({
						type : chartType,
						width : chartWidth,
						height : chartHeight,
						colors : [ '#27ae60','#c0392b',  /*'#8e44ad',  '#16a085', '#d35400', '#2c3e50', '#7f8c8d'*/],
						lineDots : 'double',
						interaction : true,
						multiHover : 5,
						tooltip : true,
						tooltiphtml : function(data) {
							var html = '';
							for (var i = 0; i < data.point.length; i++) {
								html += '<p class="tooltip chart_tooltip"><div class="tooltip-inner"><strong>' + data.point[i].value + '</strong> ' + data.point[i].yLabels[0] + '</div></p>';
							}
							return html;
						}
					});
				} else {
					$(this).hide().visualize({
						type : chartType,
						width : chartWidth,
						height : chartHeight,
						colors : ['#c0392b', '#2980b9', '#27ae60', '#8e44ad', '#16a085', '#d35400', '#2c3e50', '#7f8c8d'],
					});
				}
			});
		}


		$(window).on("resize load", function() {
			$('.visualize').remove();
			responsiveVisualize();
		});
	 
	}); 
</script>
<script>
$(document).ready(function() {
  
});
</script>

</body>

</html>

