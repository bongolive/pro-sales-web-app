<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Tasks extends User_Controller {
	
	
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('tasks_model');
		$this->load->model('devices_model');
		$this->session_userid = $this->session->userdata('userid');
	}


	public function index()

	{
		$condUserPerm = array('userid'=>$this->session->userdata('userid'));
		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] ="";
		$header['device_tab'] ="";
		$header['tasks_tab'] ="";
		$header['reports_tab'] ="";
		$header['deliveries_tab'] ="";
		$header['customers_tab'] ="";
		$header['boundaries_tab'] ="";
		$header['support_tab'] ="allow";
		$header['pathfinder_tab'] ="";
		$header['stocks_tab'] ="";
		$header['registrations_tab'] ="";
		
		foreach($user_permissions as $user_permission){
			if($user_permission['permission_name']=='track tab'){
				$header['track_tab'] ="allow";
			}else if($user_permission['permission_name']=='device tab'){
				$header['device_tab'] ="allow";
			}if($user_permission['permission_name']=='tasks tab'){
				$header['tasks_tab'] ="allow";
			}if($user_permission['permission_name']=='reports tab'){
				$header['reports_tab'] ="allow";
			}if($user_permission['permission_name']=='deliveries tab'){
				$header['deliveries_tab'] ="allow";
			}if($user_permission['permission_name']=='customers tab'){
				$header['customers_tab'] ="allow";
			}if($user_permission['permission_name']=='boundaries tab'){
				$header['boundaries_tab'] ="allow";
			}if($user_permission['permission_name']=='pathfinder survey tab'){
				$header['pathfinder_tab'] ="allow";
			}if($user_permission['permission_name']=='stocks tab'){
				$header['stocks_tab'] ="allow";
			}if($user_permission['permission_name']=='registrations tab'){
				$header['registrations_tab'] ="allow";
			}						
		}
		
		if($header['tasks_tab'] !="allow"){
			redirect('profile/support');
		}
		
		$fields['devices'] = $this->devices_model->get_devicelistbyuserid($this->session_userid);
		
		$fields['tb_data'] = $this->tasks_model->get_tasksByUserId($this->session_userid);
		
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js','plugins/bootstrapModal/bootstrap-modalmanager.js','plugins/bootstrapModal/bootstrap-modal.js','custom/custom-datatable.js');
        $fields['tb_name'] = 'tasks_tb_name';
        $fields['row_fields'] = array('device_name','subject','task','status','comments','datetime');
        $fields['tb_headers'] = array('device_name','subject','task','status','comments','date','action');
        $header['ts_active'] = 'class="active"';
        $fields['add_btn'] = 'add_task';
        $fields['download_btn'] = 'download_task';
        $this->load->view('template/header',$header);
        $this->load->view('template/content/table',$fields);
        $this->load->view('template/footer',$ft_data);

	}
    
    public function edit($taskid){
        $fields['devices'] = $this->devices_model->get_devicelistbyuserid($this->session_userid);
		$fields['tasks'] = $this->tasks_model->get_taskById($taskid);
        $this->load->view('template/content/edit_task', $fields);
    }
	
	public function add()
	{
		$data = array(
			'subject' => $this->input->post('subject'),
			'task' => $this->input->post('task'),
			'datetime' => date("Y-m-d H:i:s"),
			'phoneid' => $this->input->post('device_name'),
			'status' => $this->input->post('status'),
			'comments' => $this->input->post('comments')
		);
		
		$tsResult = $this->tasks_model->add_task($data);
		
		if($tsResult)
		{
			$device = $this->devices_model->get_devicelistbyid($this->input->post('device_name'));
			if($device['phone_imei'] !="")
			{
				$registrationIDs[] = $device['regid'];
				$apiKey = "AIzaSyCg5BsKkoHHHUxkTnP_fNqVMuOjeXVg0SI";
				$message = "You got a new Task";
				
				$fields = array(
					'registration_ids'  => $registrationIDs,
					'data'              => array( "message" => $message),
				);
						
					//print_r(json_encode($fields));
				$headers = array( 
					'Authorization: key=' . $apiKey,
					'Content-Type: application/json'
				);
				
				$response = sentTask($headers, $fields);
				
			}	
		}
		
		//redirect goes here
		redirect('tasks');
		
	}
	
	
	public function update()
	{
		$data = array(
			'subject' => $this->input->post('subject'),
			'task' => $this->input->post('task'),
			'datetime' => date("Y-m-d H:i:s"),
			'phoneid' => $this->input->post('device_name'),
			'status' => $this->input->post('status'),
			'comments' => $this->input->post('comments')
		);
		
		$taskid = $this->input->post('taskid');
		
		$tsResult = $this->tasks_model->update_task($data,$taskid);
		
		if($tsResult)
		{
			$device = $this->devices_model->get_devicelistbyid($this->input->post('device_name'));
			if($device['phone_imei'] !="")
			{
				$registrationIDs[] = $device['regid'];
				$apiKey = "AIzaSyCg5BsKkoHHHUxkTnP_fNqVMuOjeXVg0SI";
				$message = "You got an update of task";
				
				$fields = array(
					'registration_ids'  => $registrationIDs,
					'data'              => array( "message" => $message),
				);
						
					//print_r(json_encode($fields));
				$headers = array( 
					'Authorization: key=' . $apiKey,
					'Content-Type: application/json'
				);
				
				$response = sentTask($headers, $fields);
				
			}	
		}
		
		//redirect goes here
		redirect('tasks');
		
	}
	
	
	public function delete($taskid)
	{
		$result = 	$this->tasks_model->delete($taskid);
		if($result === TRUE)
		{
			redirect('tasks');
            //echo 'yes';
		}
		else
		{
			redirect('tasks');
            //echo 'no';
		}
	}
	
	
	public function export()
	{
		$result = "";
		$query_str = "";
		$taskposted = $this->input->post('txttask');
		$devicesposted = $this->input->post('cb_devices');
		$subjectposted = $this->input->post('txtsubject');
		$userid = $this->session->userdata('userid');
		
		
		if (count($devicesposted) > 0){

			//$_POST['txt_enddate'];
			$date = date_create($this->input->post('startdate'));
			$sdate = date_format($date, 'Y-m-d H:i:s');
			$date = date_create($this->input->post('enddate'));
			$edate = date_format($date, 'Y-m-d H:i:s');
			$sub = "t.subject like '%".$subjectposted."%'";
			if(strlen($subjectposted) <= 0){
				$sub = "1=1";
			}else{
				$query_str .= "?txtsubject=".$subjectposted;
			}
	
			$task = "t.task like '%$taskposted%'";
			if(strlen($taskposted) <= 0){
				$task = "1=1";
			}else{
				$query_str .= (($query_str == "") ? "?" : "&")."txttask=".$taskposted;
			}
			$phoneid = "1=1";
			if($devicesposted != ""){
				if(count($devicesposted) >0){
					$phoneid = "(";
					for($i = 0; $i < count($devicesposted); $i++){
						$phoneid .= "t.phoneid = ".$devicesposted[$i]." ".(($i < count($devicesposted)-1) ? " OR " : "");
						$query_str .= (($query_str == "") ? "?" : "&")."cb_devices[]=".$devicesposted[$i];
					}
					$phoneid .= ")";
				}
			}
			
			$stat = "1=1";
			if($this->input->post('statussearch') != -1 && $this->input->post('statussearch') != ''){
				$stat = "t.status = '".$this->input->post('statussearch')."'";
				$query_str .= (($query_str == "") ? "?" : "&")."statussearch=".$this->input->post('statussearch');
			}else{
		
			}
			$date = "t.datetime between '$sdate' and '$edate'";
			if(strlen($this->input->post('startdate')) <= 0 || strlen($this->input->post('enddate')) <= 0)
			{
				$date = "1=1";
			}else{
				$query_str .= (($query_str == "") ? "?" : "&")."startdate=".$this->input->post('startdate');
				$query_str .= (($query_str == "") ? "?" : "&")."enddate=".$this->input->post('enddate');
			}
			
			$result = $this->tasks_model->export($userid,$sub,$task,$phoneid,$stat,$date);
			if(count($result) > 0){
				if($this->input->post("btn_export") == "1"){
			
					$this->load->library('excel');
			
					$this->excel->getProperties()->setCreator("Foot Print Admin")
										 ->setLastModifiedBy("Foot Print Admin")
										 ->setTitle("Exported Tasks from Foot Print Site")
										 ->setSubject("Exported Tasks from Foot Print Site")
										 ->setDescription("Exported Tasks from Foot Print Site for the User '".$this->session->userdata('username')."'")
										 ->setKeywords("Exported Tasks from Foot Print Site")
										 ->setCategory("Exported Tasks from Foot Print Site");			
		
					$this->excel->setActiveSheetIndex(0)
						->setCellValue('A1', 'Device Name')
						->setCellValue('B1', 'Subject')
						->setCellValue('C1', 'Task')
						->setCellValue('D1', 'Status')
						->setCellValue('E1', 'Date Time')
						->setCellValue('F1', 'Comments');
			
					$this->excel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
			
			
			
					$index = 2;
					foreach($result as $row)
					{
						$this->excel->setActiveSheetIndex(0)
							->setCellValue('A'.$index, $row["device_name"])
							->setCellValue('B'.$index, $row["subject"])
							->setCellValue('C'.$index, $row["task"])
							->setCellValue('D'.$index, $row["status"])
							->setCellValue('E'.$index, $row["datetime"])
							->setCellValue('F'.$index, $row["comments"]);
							
						$index++;
					}
			
					// Rename worksheet
					$this->excel->getActiveSheet()->setTitle('Tasks');
			
					// Set active sheet index to the first sheet, so Excel opens this as the first sheet
					$this->excel->setActiveSheetIndex(0);
			
					// Redirect output to a client's web browser (Excel2007)
					header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					header('Content-Disposition: attachment;filename="tasks_for_user-'.$this->session->userdata('username').'_'.date('m-d-Y_h-i-s-a', time()).'.xlsx"');
					header('Cache-Control: max-age=0');
			

					$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
					$objWriter->save('php://output');
					exit;	}
			}
		}
        //exit;
		redirect('tasks');
	}
	
	
	

}



/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */
