<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webservice extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('tasks_model');
		$this->load->model('reports_model');
		$this->load->model('devices_model');
		$this->load->model('location_model');
		$this->load->helper('url');
		$this->load->helper('form');
	}
	
	public function deleteitemcall()
	{
		$string = file_get_contents('php://input');
		$jsonobj=json_decode($string);
		
		$type = $jsonobj -> type;
		if($type == "task"){
			$data = array(
				'isdelete' => 1
			);
			$taskid = $jsonobj -> id;
			$result = $this->tasks_model->update_task($data,$taskid);
		}
		else{
			$data = array(
				'isdelete' => 1
			);
			$reportid = $jsonobj -> id;
			$result = $this->reports_model->update_report($data,$reportid);
		}
		if (!$result)
		{
			die('Error: ' .$result);
			echo "fail";
		}else{
			echo "success";
		}
	}
	
	
	public function editdeliverycall()
	{
		$string = file_get_contents('php://input');
		$jsonobj= json_decode($string);
		$data = array(
			'subject' => $jsonobj-> subject,
			'receivedby' => $jsonobj-> receivedby,
			'report' => $jsonobj-> report,
			'latitude' => $jsonobj-> latitude,
			'longitude' => $jsonobj-> longitude
		);
		$reportid = $jsonobj-> reportid;
		$result = $this->reports_model->update_report($data,$reportid);
		if (!$result)
		{
			die('Error: ' . $result);
			echo "fail";
		}
		else {
			echo "success";
		}
	}
	
	
	public function editreportcall()
	{
		$string = file_get_contents('php://input');
		$jsonobj= json_decode($string);
		$data = array(
			'subject' => $jsonobj-> subject,
			'report' => $jsonobj-> report,
			'latitude' => $jsonobj-> latitude,
			'longitude' => $jsonobj-> longitude
		);
		$reportid = $jsonobj-> reportid;
		$result = $this->reports_model->update_report($data,$reportid);
		if (!$result)
		{
			die('Error: ' . $result);
			echo "fail";
		}
		else {
			echo "success";
		}
	}
	
	
	
	public function edittaskcall()
	{
		$string = file_get_contents('php://input');
		$jsonobj= json_decode($string);
		$data = array(
				'status' => $jsonobj-> status,
				'comments' => $jsonobj-> comments
		);
		$taskid = $jsonobj -> $jsonobj-> taskid;
		$result = $this->tasks_model->update_task($data,$taskid);
		if (!$result)
		{
			die('Error: ' . $result);
			echo "fail";
		}
		else {
			echo "success";
		}
	}
	
	
	public function getreports()
	{
		$string = file_get_contents('php://input');
		$jsonobj=json_decode($string);
		$row = $this->devices_model->get_devicelistbyImei($jsonobj-> imei);
		if(!$row){
			die('Error: ' .$row);
			echo "fail";
		}else{
			$report = $this->reports_model->getReportsForPhones($row['phoneid']);
			if(!$report){
				die('Error: ' .$reports);
				echo "fail";
			}else{
				$reports = array();
				foreach($report as $row){
					$reports[] = array('report'=>$row);
				}
				
				header('Content-type : application/json');
				echo json_encode(array('ReportList'=>$reports));
			}
		}
		
	}
	
	
	
	public function gettasks()
	{
		$string = file_get_contents('php://input');
		$jsonobj=json_decode($string);
		$row = $this->devices_model->get_devicelistbyImei($jsonobj-> imei);
		if(!$row){
			die('Error: ' .$row);
			echo "fail";
		}else{
			$task = $this->tasks_model->getTasksForPhones($row['phoneid']);
			if(!$task){
				die('Error: ' .$task);
				echo "fail";
			}else{
				
				$tasks = array();
				foreach($task as $row){
					$tasks[] = array('task'=>$row);
				}
				
				header('Content-type : application/json');
				echo json_encode(array('TaskList'=>$tasks));
			}
		}
	}
	
	
	public function newdelivery()
	{
		$string = file_get_contents('php://input');
		$jsonobj=json_decode($string);
		$row = $this->devices_model->get_devicelistbyImei($jsonobj-> imei);
		if(!$row){
			die('Error: ' .$row);
			echo "fail";
		}else{
			$data = array(
				'subject' => $jsonobj-> subject,
				'receivedby' => $jsonobj-> receivedby,
				'report' => $jsonobj-> report,
				'datetime' => date("Y-m-d H:i:s"),
				'phoneid' => $row['phoneid'],
				'latitude' => $jsonobj-> latitude,
				'longitude' => $jsonobj-> longitude
			);
			$result = $this->reports_model->add_report($data);
			if (!$result)
			{
				die('Error: ' . $result);
				echo "fail";
			}
			else {
				echo $result;
			}
		}
	}
	
	public function newreport()
	{
		$string = file_get_contents('php://input');
		$jsonobj=json_decode($string);
		$row = $this->devices_model->get_devicelistbyImei($jsonobj-> imei);
		if(!$row){
			die('Error: ' .$row);
			echo "fail";
		}else{
			$data = array(
				'subject' => $jsonobj-> subject,
				'report' => $jsonobj-> report,
				'datetime' => date("Y-m-d H:i:s"),
				'phoneid' => $row['phoneid'],
				'latitude' => $jsonobj-> latitude,
				'longitude' => $jsonobj-> longitude
			);
			$result = $this->reports_model->add_report($data);
			if (!$result)
			{
				die('Error: ' . $result);
				echo "fail";
			}
			else {
				echo $result;
			}
		}
	}
	
	public function savelocation()
	{
		$string = file_get_contents('php://input');
		$jsonobj=json_decode($string);
		
		$data = array(
			'latitude' => $jsonobj-> latitude,
			'longitude' => $jsonobj-> longitude,
			'date' => $jsonobj->date,
			'servertime' => gmdate("Y-m-d H:i:s"),
			'imei' => $jsonobj-> imei
		);
		
		$result = $this->location_model->add_location($data);
		if (!$result)
		{
			die('Error: ' . $result);
			echo "fail";
		}
		else {
			echo "success";
		}
	}
	
	
	public function regidforpush()
	{
		$string = file_get_contents('php://input');
		$jsonobj=json_decode($string);
		$dd = $jsonobj-> registrationId;
		
		$cond = array('phone_imei' => $jsonobj-> imei, 'regid' => $jsonobj-> registrationId);
		$phones = $this->devices_model->get_device($cond);
		
		if(count($phones)>0){
			echo $phones['phoneid'];
		}else{
			unset($cond);
			$cond = array('phone_imei' => $jsonobj-> imei);
			$data = array('regid' => $jsonobj-> registrationId);
			
			$result = $this->devices_model->update($devicedata,$phoneid);
			if(!$result){
				die('Error: ' .$result);
			}else{
				unset($phones);
				$phones = $this->devices_model->get_device($cond);
				echo $phones['phoneid'];
			}
			
		}
	}
	
	
}