<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class employees extends My_Controller {

	public $user_id;
	public $account_id;
	public $data;

	public function __construct() {

		parent::__construct();

		if (!$this -> session -> userdata('is_login') == TRUE) {
			$this -> session -> set_userdata('login_error', 'Please, Loggin first');
			redirect('login');
		}
		$this -> data['tb_headers'] = $this -> data['row_fields'] = $this -> data['tb_headers'] = array('firstname',  'device_imei', 'gender', /*'department',*/
		'location', 'date_of_emp', 'mobile', 'email', 'status');

		$this -> load -> model('employees_model');
		$this -> load -> model('permissions_model');
		$this -> load -> model('locations_model');
		$this -> load -> model('devices_model');
		$this -> load -> model('users_model');
		$this -> employees_model -> form_validation();
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');

		$this -> data['view'] = 'employees/view/';
		$this -> data['edit'] = 'employees/update/';
		$this -> data['delete'] = 'employees/delete/';

		$this -> data['table_id'] = $this -> employees_model -> table_id;

		$this -> data['tb_name'] = 'employees_list';
		$this -> data['controller'] = 'employees';
		$this -> user_id = $this -> session -> userdata('user_id');
		$this -> account_id = $this -> session -> userdata('account_id');

		//filtering the property items according to companyies

		/******************** check user permissions ***************************/

		if (!in_array(1, $this -> user_permissions)) {

			redirect('login/logout');
		}
		/******************** check user permissions ***************************/

	}

	public function index() {

		$this -> session -> unset_userdata('id');
		$this -> data['controller'] = 'employees';

		$where = array('employees.account_no' => $this -> account_id);

		if ($this -> input -> post('filter')) {
			extract($_POST);
			IF ($employee) {
				$where['id'] = $employee;
			}
		}

		$employee = $this -> employees_model -> read_employee($where);
		if ($employee) {
			$this -> data['tb_data'] = $employee;
		} else {
			$this -> data['tb_data'] = FALSE;
		}

		//checking users permissions
		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/employees_table');
		$this -> load -> view('template/table_helper');
		$this -> load -> view('template/footer');

	}

	//viewing
	public function view() {

		$this -> data['headers'] = $this -> data['tb_headers'] = array('staff_no', 'fullname', 'gender', /* 'department',*/
		'location', 'date_of_emp', 'dob', 'mobile', 'email', 'username', 'device_imei');

		$id = $this -> uri -> segment(3);

		$where = array('employees.employee_id' => $id);

		//geting the data
		$emp = $this -> employees_model -> read_employee($where);

		if ($emp) {

			//foreach ($emp as $emp) {
			$this -> data['info'] = $emp;
			//	}
		} else {
			$this -> data['info'] = FALSE;
		}

		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/employee_view', $this -> data);
		$this -> load -> view('template/footer');

	}

	public function create() {

		if ($this -> form_validation -> run() == FALSE) {
			$this -> data['info'] = FALSE;

			//GET locations
			$where = array('account_no' => $this -> account_id);
			$locations = $this -> locations_model -> read($where);
			if ($locations) {

				$location[] = "Assign Location";
				foreach ($locations as $key => $value) {
					$location[$value['location_code']] = $value['location_name'];
				}

			} else { $location[] = "No locations";
			}

			$this -> data['locations'] = $location;

			//get list of devices
			$my_devices = array('' => 'Assign To Device');
			$devices = $this -> devices_model -> read($where);

			if ($devices) {
				foreach ($devices as $device) {
					$my_devices[$device['device_id']] = $device['device_imei'] . '- ' . $device['device_descr'];
				}
				$this -> data['devices'] = $my_devices;
			} else {
				$this -> data['devices'] = FALSE;
			}
			//read permissions modules]
			$modules = $this -> employees_model -> read_modules();
			print_r($modules);
			//checking users permissions
			$this -> data['modules'] = $modules;

			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/employee_form', $this -> data);
			$this -> load -> view('template/footer');
		} else {

			extract($_POST);
			echo "<pre>";

			$data = array('firstname' => $firstname, 'lastname' => $lastname, 'middle_name' => $middle_name, 'staff_no' => $staff_no, /*'department' => $department,*/
			'location' => $location, 'gender' => $gender, 'dob' => $dob, 'date_of_emp' => $date_of_emp, 'mobile' => $mobile, 'email' => $email, 'account_no' => $this -> account_id, 'created_on' => date('Y-m-d h:i:s'));

			$this -> db -> trans_start();

			if ($this -> employees_model -> save($data)) {
				$emp_id = $this -> db -> insert_id();
				if ($emp_id) {
					$permissions = 9;

					if ($username) {
						$user_data = array('username' => $username, 'device_imei' => $device_imei, 'data_collector_no' => $staff_no, 'location' => $location, 'password' => sha1($password), 'allow_web_access' => $allow_web_access, 'role' => $role, 'permissions' => $permissions, 'employee_id' => $emp_id, 'account_no' => $this -> account_id, 'created_on' => date('Y-m-d h:i:s'), 'status' => 1);

						$this -> employees_model -> save_user($user_data);
					}

				}
			}
			$this -> db -> trans_complete();

			redirect('employees');
		}
	}

	public function update() {
		if ($this -> form_validation -> run() == FALSE) {
			if ($this -> session -> userdata('id')) {
				$id = $this -> session -> userdata('id');
			} else {
				$id = $this -> uri -> segment(3);
				$this -> session -> set_userdata('id', $id);
			}

			$where = array('employees.employee_id' => $id);
			//geting the data
			$info = $this -> employees_model -> read_employee($where);
			if ($info) {
				foreach ($info as $info) {
					$this -> data['info'] = $info;
				}
				//$this -> data['modules'] = $this -> employees_model -> get_modules();
				//GET locations
				$where = array('account_no' => $this -> account_id);
				$locations = $this -> locations_model -> read($where);
				if ($locations) {

					$location[] = "Assign Location";
					foreach ($locations as $key => $value) {
						$location[$value['location_code']] = $value['location_name'];
					}

				} else { $location[] = "No locations";
				}

				$this -> data['locations'] = $location;

				//get list of devices
				$my_devices = array('' => 'Assign To Device');
				$devices = $this -> devices_model -> read($where);

				if ($devices) {
					foreach ($devices as $device) {
						$my_devices[$device['device_id']] = $device['device_imei'] . '- ' . $device['device_descr'];
					}
					$this -> data['devices'] = $my_devices;
				} else {
					$this -> data['devices'] = FALSE;
				}

				//read permissions modules]
				$modules = $this -> employees_model -> read_modules();
				$this -> data['modules'] = $modules;

				$this -> load -> view('template/header', $this -> data);
				$this -> load -> view('template/content/employee_form', $this -> data);
				$this -> load -> view('template/footer');
			} else {
				redirect('employees');
			}
		} else {

			extract($_POST);
			$this -> session -> unset_userdata('id');

			$permissions = implode(',', $permissions);

			//$data = array('firstname' => $firstname, 'lastname' => $lastname, 'gender' => $gender, 'emp_role' => $role, 'mobile' => $mobile, 'email' => $email);

			$data = array('firstname' => $firstname, 'lastname' => $lastname, 'middle_name' => $middle_name, 'staff_no' => $staff_no, /* 'department' => $department, */
			'location' => $location, 'gender' => $gender, 'dob' => $dob, 'date_of_emp' => $date_of_emp, 'mobile' => $mobile, 'email' => $email, 'created_on' => date('Y-m-d h:i:s'));

			if ($this -> employees_model -> update($employee_id, $data)) {

				//check if user exists
				$user = $this -> users_model -> read(array('employee_id' => $employee_id));

				if ($user) {
					if ($password) {
						$password = sha1($password);
						$per = array('permissions' => $permissions, 'data_collector_no' => $staff_no, 'password' => $password, 'username' => $username, 'allow_web_access' => $allow_web_access, 'role' => $role, 'location' => $location, 'device_imei' => $device_imei);
					} else {
						$per = array('permissions' => $permissions, 'data_collector_no' => $staff_no, 'username' => $username, 'allow_web_access' => $allow_web_access, 'role' => $role, 'location' => $location, 'device_imei' => $device_imei);

					}

					$this -> employees_model -> update_users($user_id, $per);

				} else {

					$user_data = array('username' => $username, 'device_imei' => $device_imei, 'data_collector_no' => $staff_no, 'location' => $location, 'password' => sha1($password), 'allow_web_access' => $allow_web_access, 'role' => $role, 'permissions' => $permissions, 'employee_id' => $employee_id, 'account_no' => $this -> account_id, 'created_on' => date('Y-m-d h:i:s'), 'status' => 1);

					$this -> employees_model -> save_user($user_data);

				}
				redirect('employees');
			} else {
				redirect('employees/update');
			}
		}
	}

	public function checkmail($email, $employee_id) {

		$where = array('employees.email' => $email);
		$employee = $this -> employees_model -> read($where);
		if ($employee) {

			if ($employee[0]['employee_id'] == $employee_id) {
				return TRUE;
			} else {
				$this -> form_validation -> set_message('checkmail', 'Sorry! the email is already used by someone else');
				return FALSE;
			}
		} else {
			return TRUE;
		}
	}

	public function checkmobile($mobile, $employee_id) {

		$where = array('employees.mobile' => $mobile);
		$employee = $this -> employees_model -> read($where);
		if ($employee) {
			if ($employee[0]['employee_id'] == $employee_id) {
				return TRUE;
			} else {
				$this -> form_validation -> set_message('checkmobile', 'Sorry! the mobile number is already used by someone else');
				return FALSE;
			}
		} else {
			return TRUE;
		}

	}

	public function delete($act, $id) {

		if ($act == 'add') {
			$data = array('emp_status' => 1);
		} elseif ($act == 'remove') {
			$data = array('emp_status' => 0);
		}
		if ($this -> employees_model -> update($id, $data)) {
			redirect('employees');
		}
	}

}
