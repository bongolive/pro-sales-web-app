<?php
$data = file_get_contents('php://input');
$data = json_decode($data,true);
$imei = $data['imei'];
$tag = $data['tag'];
$nextsync = $data['nextsync'];
$date = date('Y-m-d h:i:s');

$towrite = json_encode(array("imei" => $imei,"tag" => $tag,"time" => $date, "next sync time" => $nextsync));
file_put_contents('sync.txt', $towrite."\n",FILE_APPEND);

echo $towrite;
