<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Products extends User_Controller {
	public $user_id;
	public $account_id;

	public function __construct() {
		parent::__construct();

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('sku_product_no', 'product_name',
            'category_name', 'package_name', 'items_on_hand', 're_order_value', 'unit_sale_price', 'tax_rate',
            'updated_on','assigned_stock');

		$this -> fields['ft_data'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js');

		$this -> load -> model('products_model');
		$this -> load -> model('orders_model');
		$this -> load -> model('stock_model');
		$this -> fields['table_id'] = $this -> products_model -> table_id;

		$this -> products_model -> form_validation();
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');
        $this -> user_id = $this->session->userdata('user_id');
		$this -> account_id = $this->account_id;

		$this -> fields['controller'] = 'products';
		$this -> fields['edit'] = 'products/update_product';
		$this -> fields['view'] = 'products/view';
		$this -> fields['delete'] = 'products/delete';

	}

	public function index() {

		$where = array('products.account_id' => $this->account_id);
		//filtering data

		if ($this -> input -> post('filter')) {
			extract($_POST);
			IF ($category) {
				$where['category'] = $category;
			}
			if ($product_name) {
				$where['product_name'] = $product_name;
			}
			if ($supplier) {
				$where['supplier_id'] = $supplier;
			}
		}
		$products = $this -> products_model -> read_products($where);

		if ($products) {

			$this -> fields['tb_data'] = $products;
		} else {
			$this -> fields['tb_data'] = FALSE;
		}

		$categories = $this -> products_model -> get_product_categories(array('account_id' => $this -> account_id));

		if ($categories) {
			$cat[''] = 'Select Category';
			foreach ($categories as $categories) {
				$cat[$categories['cat_id']] = $categories['category_name'];
			}
		} else {
			$cat = FALSE;
		}



		$this -> fields['categories'] = $cat;
		$this -> fields['tb_name'] = 'products_tb_name';

		$header['stc_active'] = 'class="active"';
		$this -> fields['add_btn'] = 'add_new_stock';

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/products_table', $this -> fields);
		$this -> load -> view('template/footer');

	}

	public function view() {

		$where = array('product_id' => $this -> uri -> segment(4));

		$products = $this -> products_model -> read_products($where);

		if ($products) {
			foreach ($products as $products) {
			}
			$this -> fields['tb_data'] = $products;

			$stock = $this -> products_model -> getStock($where);
			if ($stock) {
				foreach ($stock as $stock) { $this -> fields['stock'] = $stock;
				}
			} else {
				$this -> fields['stock'] = 0;
			}
			/*
			 * get orders
			 */
			$where = array('sales_order_details.product_id' => $this -> uri -> segment(4));
			$orders = $this -> orders_model -> getOrderDetails($where);
			if ($orders) {
				$this -> fields['orders'] = $orders;
			} else {
				$this -> fields['orders'] = FALSE;
			}

            /*
             * get product warehouses
             */
            $prod = $this->uri -> segment(4);
            $wareshouse = $this->stock_model->get_warehouse_perproduct($prod);
            if($wareshouse){
                $this->fields['warehouse'] = $wareshouse;
            } else {
                $this->fields['warehouse'] = false;
            }

			/*
			 * get stock purchases
			 */

			$where = array('stock.product_id' => $this -> uri -> segment(4));
			$stock_hist = $this -> stock_model -> get_product_stock($where);
			if ($stock_hist) {

				$this -> fields['stocks_hist'] = $stock_hist;
			} else {

				$this -> fields['stocks_hist'] = FALSE;
			}

		} else {
			$this -> fields['tb_data'] = FALSE;
		}
		$this -> fields['tb_name'] = 'products_tb_name';

		$header['stc_active'] = 'class="active"';
		$this -> fields['add_btn'] = 'add_new_stock';

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/products_view', $this -> fields);
		$this -> load -> view('template/footer');

	}


	public function new_products() {

		if ($this -> form_validation -> run() == FALSE) {
			$header = FALSE;
			$where = array('account_id' => $this->account_id);

			$categories = $this -> products_model -> get_product_categories($where);

			if ($categories) {
				foreach ($categories as $categories) {
					$cat[$categories['cat_id']] = $categories['category_name'];
				}
			} else {
				$cat = FALSE;
			}
			$packages = $this -> products_model -> read_packages($where);
			if ($packages) {
				//print_r($packages);
				foreach ($packages as $packages) {
					$package[$packages['package_id']] = $packages['package_name'];
				}
			} else {
				$package = FALSE;
			}

			$containers = $this -> products_model -> get_containers($where);

			if ($containers) {
				$conter[] = "Select Container";
				foreach ($containers as $containers) {
					$conter[$containers['container_id']] = $containers['container_name'];
				}
			} else {
				$conter = FALSE;
			}



			$this -> fields['info'] = FALSE;
			$this -> fields['containers'] = $conter;
			$this -> fields['categories'] = $cat;
			$this -> fields['packages'] = $package;

			$this -> load -> view('template/header', $header);
			$this -> load -> view('template/content/products_form', $this -> fields); 
			$this -> load -> view('template/footer');

		} else {
			extract($_POST);

			$products = array('product_name' => $product_name, 'items_on_hand'=>$starting_quantity, 'container_type' => $container_type,
                're_order_value' => $re_order_value, 'unit_size' => $unit_size, 'unit_measure' => $unit_measure,
                'user_id' => $this -> account_id, 'description' => $descriptions,
                'category' => $product_category, 'package_type' => $package_type,
                'package_units' => $package_units, 'unit_sale_price' => $unit_sale_price,
                'sku_product_no' => $sku_product_no, 'tax_rate' => $tax_rate,
                'created_on' => date('Y-m-d h:i:s'), 'created_by' => $this -> user_id,
                'account_id' => $this -> account_id);

			//print_r($products);

			$this -> products_model -> save($products);

			redirect('products');
		}

	}

	public function update_product() {

		if ($this -> form_validation -> run() == FALSE) {

			$where = array('product_id' => $this -> uri -> segment(4));
			$where_r = array('account_id' => $this -> account_id);
			$products = $this -> products_model -> read($where);

			if ($products) {
				foreach ($products as $products) {
				}
				$this -> fields['info'] = $products;
			} else {
				$this -> fields['info'] = FALSE;
			}

			$categories = $this -> products_model -> get_product_categories($where_r);

			if ($categories) {
				foreach ($categories as $categories) {
					$cat[$categories['cat_id']] = $categories['category_name'];
				}

			} else {
				$cat = FALSE;
			}
			$packages = $this -> products_model -> read_packages($where_r);
			if ($packages) {
				foreach ($packages as $packages) {
					$package[$packages['package_id']] = $packages['package_name'];
				}
			} else {
				$package = FALSE;
			}
			
				$containers = $this -> products_model -> get_containers($where_r);

			if ($containers) {
				$conter[] = "Select Container";
				foreach ($containers as $containers) {
				$conter[$containers['container_id']] = $containers['container_name'];
				}
			} else {
				$conter = FALSE;
			}


			
			$this -> fields['containers'] = $conter;
			$this -> fields['categories'] = $cat;
			$this -> fields['packages'] = $package;

			$this -> load -> view('template/header');
			$this -> load -> view('template/content/products_form', $this -> fields);
			$this -> load -> view('template/footer');

		} else {
			extract($_POST);

			$products = array('product_name' => $product_name, 'items_on_hand'=>$starting_quantity,  're_order_value' => $re_order_value,
                'container_type' => $container_type, 'unit_size' => $unit_size, 'unit_measure' => $unit_measure,
                'description' => $descriptions, 'category' => $product_category, 'package_type' => $package_type,
                'package_units' => $package_units, 'unit_sale_price' => $unit_sale_price, 'sku_product_no' => $sku_product_no,
                'tax_rate' => $tax_rate, 'updated_on' => date('Y-m-d h:i:s'), 'updated_by' => $this -> user_id);

			$this -> products_model -> update($product_id, $products);

			redirect('products');
		}

	}

	/*
	 * products categories start
	 */
	public function categories() {

		$this -> fields['table_id'] = 'cat_id';
		$this -> fields['edit'] = 'products/categories/update';
		$this -> fields['delete'] = 'products/categories/delete';

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('category_name', 'category_status', 'created_on');

		$where = array('account_id' => $this -> account_id);

		$this -> fields['tb_data'] = $this -> products_model -> get_product_categories($where);

		$this -> fields['tb_name'] = 'products_categories';

		$header['stc_active'] = 'class="active"';

		if ($this -> uri -> segment(4) == 'update') {
			//updating category
			if ($this -> uri -> segment(5)) {
				$where = array('cat_id' => $this -> uri -> segment(5));
				$category = $this -> products_model -> get_product_categories($where);
				if ($category) {
					foreach ($category as $category) {
						$this -> fields['info'] = $category;
					}

				}
			}

		} elseif ($this -> uri -> segment(4) == 'delete') {
			$id = $this -> uri -> segment(5);
			$data = array('category_status' => '0');
			$this -> products_model -> update_categories($id, $data);
			$this -> fields['info'] = FALSE;
		} else {

			$this -> fields['add_btn'] = 'add_new_stock';
			$this -> fields['info'] = FALSE;
		}

		$this -> load -> view('template/header');
		$this -> load -> view('template/content/products_categories', $this -> fields);
		$this -> load -> view('template/footer');

	}

	public function add_category() {

		if ($this -> input -> post('category_name')) {
			extract($_POST);
            $itemid = ucfirst($category_name).'.'.$this->account_id ;
            $checkitemid = $this->products_model->check_category($itemid);
            if(!$checkitemid){
                $data = array('category_name' => ucfirst($category_name),
                    'category_descr' => $category_descr, 'account_id' => $this -> account_id,
                    'created_by' => $this -> user_id, 'item_id' => $itemid, 'created_on' => date('Y-m-d h:i:s'));
                $this -> products_model -> add_categories($data);
                if ($from_products) {
                    $this -> session -> set_userdata('csettings', TRUE);
                    redirect('products/new_products');
                } else {
                    $this -> categories();
                }
            } else {
                $this -> categories();
            }

		}

	}

    public function add_warehouse() {

		if ($this -> input -> post('wh_title') && $this-> input -> post('wh_supervisor')) {
			extract($_POST);
			$data = array('wh_title' => ucfirst($wh_title), 'wh_supervisor' => $wh_supervisor, 'account_id' => $this -> account_id, 'created_on' => date('Y-m-d h:i:s'));
			$this -> products_model -> add_warehouse($data);
			if($from_warehouse){
                $this -> session -> set_userdata('warehz', TRUE);
                redirect('products/warehouses');
            } else {
				$this -> warehouses();
			}
		}

	}

    /*
	 * products categories start
	 */
    public function warehouses() {

        $this -> fields['table_id'] = 'wh_id';
        $this -> fields['edit'] = 'products/warehouses/update';
        $this -> fields['delete'] = 'products/warehouses/delete';
        $this -> fields['view'] = 'products/warehouse';

        $this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('wh_title', 'wh_supervisor', 'created_on');

        $where = array('account_id' => $this -> account_id);

        $this -> fields['tb_data'] = $this -> products_model -> get_warehouses($where);

        $this -> fields['tb_name'] = 'warehouses';

        $header['stc_active'] = 'class="active"';

        if ($this -> uri -> segment(4) == 'update') {
            //updating category
            if ($this -> uri -> segment(5)) {
                $where = array('wh_id' => $this -> uri -> segment(5));
                $warehauz = $this -> products_model -> get_warehouses($where);
                if ($warehauz) {
                    foreach ($warehauz as $warehauz) {
                        $this -> fields['info'] = $warehauz;
                    }

                }
            }

        } elseif ($this -> uri -> segment(4) == 'delete') {
            $id = $this -> uri -> segment(5);
            $data = array('wh_status' => '0');
            $this -> products_model -> update_warehouse($id, $data);
            $this -> fields['info'] = FALSE;
        }
        else {

            $this -> fields['add_btn'] = 'add_new_stock';
            $this -> fields['info'] = FALSE;
        }

        $this -> load -> view('template/header');
        $this -> load -> view('template/content/products_warehouses', $this -> fields);
        $this -> load -> view('template/footer');

    }

    public function warehouse($id){
        $this -> fields['table_id'] = 'wh_id';
        $this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('wh_title', 'wh_supervisor', 'created_on');
        $this -> fields['row_fields2'] = $this -> fields['tb_headers2'] = array('product_name', 'stock_date','stock_qty','expiry_date', 'created_on');

        $where = array('wh_id' => $id);


        $this -> fields['tb_data'] = $this -> products_model -> get_warehouses($where);

        $this -> fields['tb_data2'] = $this -> stock_model -> get_stock_forwarehouse($id);

        $this -> load -> view('template/header');
        $this -> load -> view('template/content/warehouse_view', $this -> fields);
        $this -> load -> view('template/footer');
    }

    public function update_warehouse() {

        if ($this -> input -> post('wh_id')) {
            extract($_POST);
            $data = array('wh_title' => ucfirst($wh_title), 'wh_supervisor' => $wh_supervisor);
            $this -> products_model -> update_warehouse($wh_id, $data);

        }
        redirect('products/warehouses');

    }

	public function update_category() {

		if ($this -> input -> post('category_id')) {
			extract($_POST);
			$data = array('category_name' => ucfirst($category_name), 'category_descr' => $category_descr, 'modify_date' => date('Y-m-d h:i:s'));
			$this -> products_model -> update_categories($category_id, $data);

		}
		redirect('products/categories');

	}

	/*
	 * products categories ends
	 *
	 */

	/*
	 * products packaging starts
	 */

	public function packages() {

		$this -> fields['table_id'] = 'package_id';
		$this -> fields['edit'] = 'products/packages/update';
		$this -> fields['delete'] = 'products/packages/delete';

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('package_name', 'status');

		$where = array('account_id' => $this->account_id);

		$this -> fields['tb_data'] = $this -> products_model -> get_packages($where);

		$this -> fields['tb_name'] = 'products_packages';

		$header['stc_active'] = 'class="active"';

		if ($this -> uri -> segment(4) == 'update') {

			//updating category
			if ($this -> uri -> segment(5)) {
				$where = array('package_id' => $this -> uri -> segment(5));
				$packages = $this -> products_model -> get_packages($where);
				if ($packages) {
					foreach ($packages as $packages) {
						$this -> fields['info'] = $packages;
					}

				}
			}

		} elseif ($this -> uri -> segment(4) == 'delete') {
			$id = $this -> uri -> segment(5);
			$data = array('status' => '0');
			$this -> products_model -> update_packages($id, $data);
			$this -> fields['info'] = FALSE;
		} else {

			$this -> fields['add_btn'] = 'add_package';
			$this -> fields['info'] = FALSE;
		}

		$this -> load -> view('template/header');
		$this -> load -> view('template/content/products_packages', $this -> fields);
		$this -> load -> view('template/footer');

	}

	public function add_packages() {

		if ($this -> input -> post('package_name')) {
			extract($_POST);
            $itemid = ucfirst($package_name).'.'.$this->account_id ;
            $checkitemid = $this->products_model->check_package($itemid);
            if(!$checkitemid) {
                $data = array('package_name' => ucfirst($package_name),
                    'package_descr' => $package_descr, 'item_id' => $itemid,
                    'account_id' => $this->account_id, 'created_by' => $this->user_id,
                    'created_on' => date('Y-m-d h:i:s'));
                $this->products_model->add_packages($data);
                if ($from_products) {
                    $this->session->set_userdata('psettings', TRUE);
                    redirect('products/new_products');
                } else {
                    redirect('products/packages');
                }
            } else {
                redirect('products/packages');
            }
		}

	}

	public function update_packages() {

		if ($this -> input -> post('package_id')) {
			extract($_POST);

			$data = array('package_name' => ucfirst($package_name), 'package_descr' => $package_descr);

			$this -> products_model -> update_packages($package_id, $data);
		}
		redirect('products/packages');

	}

	/*
	 * products packaging ends
	 */

	/*
	 * products containers starts
	 */

	public function containers() {

		$this -> fields['table_id'] = 'container_id';
		$this -> fields['edit'] = 'products/containers/update';
		$this -> fields['delete'] = 'products/containers/delete';

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('container_name', 'status');

		$where = array('account_id' => $this->account_id);

		$this -> fields['tb_data'] = $this -> products_model -> get_containers($where);

		$this -> fields['tb_name'] = 'products_containers';

		$header['stc_active'] = 'class="active"';

		if ($this -> uri -> segment(4) == 'update') {

			//updating category
			if ($this -> uri -> segment(5)) {
				$where = array('container_id' => $this -> uri -> segment(5));
				$containers = $this -> products_model -> get_containers($where);
				if ($containers) {
					foreach ($containers as $containers) {
						$this -> fields['info'] = $containers;
					}

				}
			}

		} elseif ($this -> uri -> segment(4) == 'delete') {
			$id = $this -> uri -> segment(5);
			$data = array('status' => '0');
			$this -> products_model -> update_containers($id, $data);
			$this -> fields['info'] = FALSE;
		} else {

			$this -> fields['add_btn'] = 'add_container';
			$this -> fields['info'] = FALSE;
		}

		$this -> load -> view('template/header');
		$this -> load -> view('template/content/products_containers', $this -> fields);
		$this -> load -> view('template/footer');

	}

	public function add_container() {

		if ($this -> input -> post('container_name')) {
			extract($_POST);
            $itemid = ucfirst($container_name).'.'.$this->account_id ;
            $checkitemid = $this->products_model->check_container($itemid);
            if(!$checkitemid) {
                $data = array('container_name' => ucfirst($container_name),
                    'container_descr' => $container_descr,  'item_id' => $itemid,
                    'account_id' => $this->account_id, 'created_by' => $this->user_id,
                    'created_on' => date('Y-m-d h:i:s'));
                $this->products_model->add_containers($data);
                if ($from_products) {
                    $this->session->set_userdata('contsettings', TRUE);
                    redirect('products/new_products');
                } else {
                    redirect('products/containers');
                }
            } else{
                redirect('products/containers');
            }
		}

	}

	public function update_container() {

		if ($this -> input -> post('container_id')) {
			extract($_POST);

			$data = array('container_name' => ucfirst($container_name), 'container_descr' => $container_descr);

			$this -> products_model -> update_containers($container_id, $data);
		}
		redirect('products/containers');

	}

	/*
	 * products containers ends
	 */

	public function stock_history() {

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('employee_name', 'phone_imei', 'value_stock_in_hand', 'last_sale', 'last_restock');

		$where = array('account_id' => $this->account_id);

		$this -> fields['tb_data'] = $this -> products_model -> get_product_categories($where);

		$this -> fields['tb_name'] = 'products_categories';

		$header['stc_active'] = 'class="active"';
		$this -> fields['add_btn'] = 'add_new_stock';

		$this -> load -> view('template/header', $header);
		$this -> load -> view('template/content/team_stock_history', $this -> fields);
		$this -> load -> view('template/footer');

	}

	public function deactivate() {
		$id = $this -> uri -> segment(4);
		$status = array('status' => 0);
		if ($this -> products_model -> update($id, $status)) {
			$this -> index();
		}
	}

}
