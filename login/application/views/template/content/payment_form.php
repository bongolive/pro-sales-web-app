<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l($tb_name); ?></h2>
							&nbsp; &nbsp;	
							<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('payments/invoice'); ?>" style="float: right; padding:8px; margin-left: 20px;">Issue Invoice</a>
					  
						<a class="btn btn-alt btn-primary" data-toggle="modal" href="<?php echo site_url('payments'); ?>";" style="float: right; padding:8px;"><?php _l('back'); ?></a>
					  
            </header> 

        </article>

    </div>	
    
    
     <!-- searching
    <div class="row">
		        	<section>
		           		 <article class="span6 turquoise data-block">
								<header> <h2>Search form</h2></header>
									<section> 
												<form class="form-search no-margin">
													<div class="control-group">
														<div class="controls">
															<input class="search-query" type="text"/>
															<button class="btn btn-alt" type="submit">Search</button>
														</div>
													</div>
												</form> 
									 </section>
		   	 
   	</div>-->
   	
   	
   	
    <div class="row">
  
        <article class="span12 data-block"> 
 
         			<div class="row-fluid">
								<div class="span6">
								 <section>
										<h4 class="no-margin"><?php _l('customer_details') ?></h4>
										<br />
										<table class="table table-striped table-hover">
												

                                        <?php      foreach($order_hd as $header){ ?>

										<tr><th><?php _l($header); ?></th><td><?php
                                                if($header == 'payment_status'){
                                                    $j = $info[$header];
                                                    if($j == 0) echo _l('receipt_notprinted');
                                                    if($j == 1) echo _l('receipt_printed') ;
                                                } else  if($header == 'order_status'){
                                                    	if ($info[$header] == 1) {
                                                        echo '<span class="label label-inverse">'; _l('new_order'); echo '</span>';
                                                    } elseif ($info[$header] == 2) {
                                                        echo '<span class="label label-info">'; _l('closed'); echo '</span>';
                                                    } elseif ($info[$header] == 3) {
                                                        echo '<span class="label label-danger">'; _l('cancelled'); echo '</span>';
                                                    }
                                                } else {
                                                    echo $info[$header];
                                                } ?> </td></tr>

											<?php }  ?>
										 	</tbody>
										 </table>
								   </section>
								</div>
								
								<div class="span6">
								 <section style="">	 
								 	 
												<h4 class="no-margin"><?php _l('payments_summary') ?></h4>
												<br />
											<table class="table table-striped table-hover">	 
											
											<?php $currency = $settings['default_currency']; ?>
  			<tr><td><?php _l('total_sales');?> </td><td><strong><?php echo $currency;?></strong> <strong><?php echo money_format('%i', $info['sales_total']);?></strong></td></tr>
			 <tr><td><?php _l('paid_amount');?> </td><td><strong><?php echo $currency ;?></strong><strong><?php echo  money_format('%i', $info['paid_amount']);?></strong></td></tr>
			 <tr><td><?php _l('outstanding');?> </td><td><strong><?php echo $currency ;?></strong> <strong><?php echo money_format('%i', $info['sales_total'] - $info['paid_amount'] );?></strong></td></tr>
 </table>
                                     <div>
                                         <?php if(!is_null($error_message)){
                                             echo $error_message;
                                         }
                                         ?>
                                     </div>
												 
									</section> 
								</div>
								 
							</div>
	 
 
         			<div class="row-fluid">						
			
					<div class="span6">				 
            <section>
 <fieldset>
<legend><?php _l('transactions_history'); ?></legend>  

<table class="table table-striped table-hover">	 
  	<tr><th><?php _l('trans_date') ?></th><th><?php _l('trans_amount'); echo "(".$settings['default_currency'].")"?></th><th><?php _l('payments_mode_title')?></th></tr>
  <?php
 
  if($transactions){
  	foreach ($transactions as $transactions) {  
  		
  		?>
	<tr><td><?php echo $transactions['trans_date']; ?></td><td><?php echo $transactions['trans_amount']; ?></td><td><?php echo $transactions['mode_name']; ?></td><td><a href="<?php echo site_url($edit) . '/' . $transactions['trans_id']; ?>" class="btn btn-small " title="Edit"><span class="awe-pencil"/></a></td></tr>	  
	 <?php }
  } else{ ?>
  	<tr><td colspan="3">No Transactions made</td></tr>
	
 <?php } ?>
 </table>
			</div>
							
			<div class="span6">				 
            <section>
 <fieldset>
<legend><?php _l('payment_details'); ?></legend>         		
   
<?php  
if($trans) {
		echo form_open('payments/update',array('class' => 'form-horizontal'));
		echo '<input type="hidden" name="trans_id" value="'. $trans['trans_id'].'"/>';
		}else { 
 		echo form_open('payments/make_payments',array('class' => 'form-horizontal'));
		echo '<input type="hidden" name="order_id" value="'. $info['order_id'].'"/>';
 }
?>
  
   <div class="control-group">
	<span class="control-label"> <?php _l('paid_amount'); ?></span>  
	<div class="controls">
		<div class="input-append">
 
<input type="number" id="appendedInput" class="input-small" required="" min="1"  name="paid_amount"  value="<?php if($trans){echo $trans['trans_amount'];}else{ echo set_value('paid_amount');} ?>" />
<span class="add-on"><?php echo $settings['default_currency'] ?></span>
</div>
		 
			 	
</div> 
</div>
  
   <div class="control-group">
	<span class="control-label"> <?php _l('payments_mode_title'); ?></span>
	<div class="controls"> 
			 <?php if($trans){ $dmode = $trans['trans_pay_mode'];}else{$dmode = FALSE;} echo form_dropdown('payment_mode',$pay_modes,$dmode,'class="input-medium"'); ?>
			
</div> 
</div>
<br />
  
<div class="form-actions"> 

<a href="<?php echo site_url('payments');?>" class="btn btn-large">Cancel</a>
 <button type="submit" class="btn btn-info btn-large"><i class="icon-pencil"></i>Save</button></div>

</fieldset>
 
</div>
</div>
</article></section> 




