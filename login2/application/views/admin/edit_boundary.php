<!DOCTYPE html>

<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->

<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->

<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>FootPrint</title>
    <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.8.23/themes/smoothness/jquery-ui.css" />
	<?php get_css('sangoma-blue.css');?>
    <style>
        
        /* css for timepicker */
		.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
		.ui-timepicker-div dl { text-align: left; }
		.ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
		.ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
		.ui-timepicker-div td { font-size: 90%; }
		.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
		
		.ui-timepicker-rtl{ direction: rtl; }
		.ui-timepicker-rtl dl { text-align: right; }
		.ui-timepicker-rtl dl dd { margin: 0 65px 10px 10px; }
        
        </style>
		
    <?php get_css('plugins/prettyCheckable.css');?>  
   
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.0/jquery-ui.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/libs/jquery.js"><\/script>')</script>
        
        <?php get_js('jquery-ui-timepicker-addon_2.js');?>
        <?php get_js('libs/modernizr.js');?>



        <?php get_js('libs/selectivizr.js');?>

</head>

<body style="background-color:white;">



                            <div class="tabbable tabs-left">		

                            <div class="tab-content">   		

                                <div class="tab-pane active" id="tab1">

                                    

                                        <fieldset>

                                        <form class="form-horizontal" method="post" action="<?php echo ($do == 'add') ? site_url('admin/addBoundary'): site_url('admin/updateBoundary');?>" id="add_boundary" >
										<?php 
										if($do == 'add'){ ?>	
                                        <div class="control-group">
                                        	<label class="control-label" for="user">Users</label>
                                       		<div class="controls">
                                        	<select id="users" name="users" class="input-xlarge">
                        					<option value="">Please select User</option>
                        					<?php foreach ($users as $user): ?>
                          					 <option value="<?php echo $user['userid'];?>"><?php echo $user['name'];?></option>
											<?php endforeach; ?>
                    						</select>
                                        	</div>
                                    	</div>
                                        <?php } ?>    
                                            
                                            <div class="control-group">

                                            	<label class="control-label"><?php _l('device_name'); ?></label>
												
                                            	<div class="controls" id="divice_checkboxes" style="padding-left: 26px;height: 100px;overflow: auto;width: 235px;">

                                                

                                                    <?php foreach($devices as $device){ ?>

                                                        <label class="checkbox custom-checkbox">
														<?php	
																$chech = "";
																if( checkoldimei($device['phone_imei'],$boundryalocation_data) == true ){
																	$chech = 'checked="checked"';
																}
														?>
                                                            
											                 <input id="<?php echo 'cb'.$device['phoneid']; ?>" type="checkbox" name="cb_devices[]" value="<?php echo $device['phone_imei']; ?>" <?php echo $chech; ?> />

											                 <?php echo $device['device_name']; ?>

                                                        </label>

                                                   <?php } ?>  

                                                

                                            	</div>

                                        	</div>

                                        	<div class="control-group">

                                            	<label class="control-label" for="alert_name"><?php _l('alert_name'); ?></label>

                                            	<div class="controls">

                                            	<input name="alert_name" id="alert_name" class="input-xlarge" type="text"  value="<?php echo (isset($boundaries[0]['alertname'])) ? $boundaries[0]['alertname'] : ''; ?>" required>

                                            	</div>

                                        	</div>

                                            <div class="control-group">

                                            	<label class="control-label" for="valid_from" ><?php _l('valid_from'); ?></label>

                                            	<div class="controls">

                                            	   <input name="valid_from" id="valid_from" class="datepicker input-xlarge" type="hidden" value="<?php echo (isset($boundaries[0]['valid_from'])) ? $boundaries[0]['valid_from'] : ''; ?>" required >

                                            	</div>

                                        	</div>

                                            <div class="control-group">

                                            	<label class="control-label" for="valid_to" ><?php _l('valid_to'); ?></label>

                                            	<div class="controls">

                                            	   <input name="valid_to" id="valid_to" class="datepicker input-xlarge" value="<?php echo (isset($boundaries[0]['valid_to'])) ? $boundaries[0]['valid_to'] : ''; ?>" type="hidden" required >

                                            	</div>

                                        	</div>

                                            <div class="control-group">

                                            	<div class="controls">

                                            	   <a id="showMap" class="btn btn-alt btn-medium btn-primary" href="#tab2" >Next</a>

                                            	</div>

                                        	</div> 

                                            <input type="hidden" name="center_lat" id="centerlat" value="<?php echo (isset($boundaries[0]['centerlat'])) ? $boundaries[0]['centerlat'] : ''; ?>" />

                                            <input type="hidden" name="center_lng" id="centerlng" value="<?php echo (isset($boundaries[0]['centerlng'])) ? $boundaries[0]['centerlng'] : ''; ?>" />

                                            <input type="hidden" name="circle_radius" id="circleradius" value="<?php echo (isset($boundaries[0]['radius'])) ? $boundaries[0]['radius'] : ''; ?>" />

                                            <input type="hidden" name="do" id="do" value="<?php echo $do; ?>" />

                                            <?php if($do == 'edit'){?>

                                            <input type="hidden" name="boundryid" id="boundryid" value="<?php echo (isset($boundaries[0]['boundryid'])) ? $boundaries[0]['boundryid'] : ''; ?>" />

                                            <?php }?>                                                                                               

    								</div>

                                    <div class="tab-pane" id="tab2">

                                    <div id="mapCanvas" style="width:100%; height:315px;"></div>

                                            <div style="width:auto; height:auto; margin:5px auto; width: 100%;">

                                                <div id="div_center_point" style="float:left;">&nbsp;</div>

                                                <div id="div_radius_km" style="float:left;">&nbsp;</div>

                                            </div>

                                        <div class="form-actions" style="float: right;margin-top: -16px;">

                                                <div class="controls">

                                            	   <a id="backDetails" class="btn btn-alt btn-small btn-primary" href="javascript:void(0);" >Back</a>

                                                   <a href="javascript:void(0);" class="btn btn-alt btn-small btn-primary" onClick="save_boundry();">Save</a>

                                                </div>

                				                

                                        </div>

                                    </div>

    								</form>

                                </fieldset>

                            </div>

                         </div>

                         <div id="success" style="display: none;"><div class="alert alert-success fade in">

								<button class="close" data-dismiss="alert">&times;</button >

								<strong>Boundary Successfully added Refreshing ...</strong>

                        </div></div>

<?php get_js('bootstrap/bootstrap.min.js');?>

<?php get_js('plugins/prettyCheckable/prettyCheckable.js');?> 

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<script type="text/javascript">



var map;

var action_do = "<?php echo $do; ?>";

    $(document).ready(function() {


                $('#valid_to').datetimepicker();
                $('#valid_from').datetimepicker();
				$('.custom-checkbox input').prettyCheckable();
                
                
                
                if (action_do == 'add'){
                            parent.top.$('#show_valid_to').show();
                            parent.top.$('#show_valid_from').show();
                }else if(action_do == 'edit'){
                            parent.top.$('#show_edit_valid_to').show();
                            parent.top.$('#show_edit_valid_from').show();
                            parent.top.$('#show_edit_valid_to').val($('#valid_to').val());
                            parent.top.$('#show_edit_valid_from').val($('#valid_from').val());
                }

                   $("#showMap").click(function() {
                          var valid_from = '';
                          var valid_to  = '';
                          if (action_do == 'add'){
                                 valid_from = parent.top.$('#show_valid_from');
                                 valid_to = parent.top.$('#show_valid_to');
                          }else if(action_do == 'edit'){
                                valid_from = parent.top.$('#show_edit_valid_from');
                                valid_to = parent.top.$('#show_edit_valid_to');
                          }
                          $('#valid_to').val(valid_to.val());
                          $('#valid_from').val(valid_from.val());

                          var checkBoxes = $("input[type=checkbox]:checked");

                          var alert_name = $("#alert_name");
                          
                          
                		 //var valid_from = $("#valid_from");
                         

                		 // var valid_to = $("#valid_to");
                   
                		 if(checkBoxes.length <= 0){

                			alert("No device selected");

                			return false;

                		 }else if(alert_name.val().length <= 0){

                			alert("Alert name should not blank");

                			return false;

                		 } else if(valid_from.val().length <= 0){

                			alert("Valid From date should not blank");

                			return false;

                		 }else if(valid_to.val().length <= 0){

                			alert("Valid To date should not blank");

                			return false;

                		 }else if(   Date.parse( valid_from.val().replace("-", "/").replace("-", "/") ) > Date.parse( valid_to.val().replace("-", "/").replace("-", "/") )   ){

                			alert("'From date' shouldn't be greated then 'To date'.");

                			return false;

                		 }

                		 else{

                			//document.getElementById('device_form').submit();
                                    $('#tab2').addClass('active');

                                   $('#tab1').removeClass('active');
                                   if (action_do == 'add'){
                                                parent.top.$('#show_valid_to').hide();
                                                parent.top.$('#show_valid_from').hide();
                                    }else if(action_do == 'edit'){
                                                parent.top.$('#show_edit_valid_to').hide();
                                                parent.top.$('#show_edit_valid_from').hide();
                                               
                                    }
                                   initializeMap();
                		  }
                });
                
                $("#backDetails").click(function() {
                                    $('#tab1').addClass('active');

                                   $('#tab2').removeClass('active');
                                   if (action_do == 'add'){
                                                parent.top.$('#show_valid_to').show();
                                                parent.top.$('#show_valid_from').show();
                                    }else if(action_do == 'edit'){
                                                parent.top.$('#show_edit_valid_to').show();
                                                parent.top.$('#show_edit_valid_from').show();
                                   }
                });

                



    });

function initializeMap() {



	var startCenterPoint = new google.maps.LatLng(-6.822924, 39.269656);

	var startRadius = 2000;

	

	if(action_do == "edit"){

		startCenterPoint = new google.maps.LatLng(document.getElementById("centerlat").value, document.getElementById("centerlng").value);

		startRadius = parseFloat( document.getElementById("circleradius").value);

	}



	var myOptions = {

		zoom: 13,

		center: startCenterPoint,

		mapTypeId: google.maps.MapTypeId.ROADMAP

	}



	map = new google.maps.Map(document.getElementById("mapCanvas"), myOptions);

	

	var circleOptions = {

	  center: startCenterPoint,

	  radius: startRadius,

	  map: map,

	  editable: true

	};

	var circle = new google.maps.Circle(circleOptions);

	

	set_circle_values(circle);

	

	google.maps.event.addListener(circle, 'center_changed', function() {

		set_circle_values(circle);

	});

	

	google.maps.event.addListener(circle, 'radius_changed', function() {

		set_circle_values(circle);

	});

	

	

}



function set_circle_values(circle){

	var center = circle.getCenter();

	document.getElementById("centerlat").value = center.lat();

	document.getElementById("centerlng").value = center.lng();

	document.getElementById("circleradius").value = circle.getRadius(); // radius in meters

	

	document.getElementById("div_center_point").innerHTML = "<strong>Center Point : </strong>"+center.lat().toFixed(10)+", "+center.lng().toFixed(10);

	document.getElementById("div_radius_km").innerHTML = "&nbsp;&nbsp;<strong> Radius : </strong>"+( (circle.getRadius() / 1000).toFixed(2) ) + "  KM"; 

	

}

function save_boundry(){
    	$.ajax( {
            	type: "POST",

				url: $("#add_boundary").attr( 'action' ),

				data: $("#add_boundary").serialize(),

				success: function( response ) {

					if(response == "1"){

					    var success_data = $('#success').html();

                        $('.tab-content').html(success_data);

						setTimeout("parent.location.reload();", 2000);

					}else{

						$('.tab-content').html(response);

					}

					

				}

			} );  

	  }

</script>

<script type="text/javascript">
			$(document).ready(function()
			{
				$("#users").change(function()
				{
					var id=$(this).val();
					var dataString = 'id='+ id;
					$.ajax
					({
						type: "POST",
						url: "<?php echo site_url("admin/getUserDevicesCheckboxImei"); ?>",
						data: dataString,
						cache: false,
						success: function(html)
						{
							$("#divice_checkboxes").html(html);
						}
					});
				});
			});
		</script>

</body>

</html>

