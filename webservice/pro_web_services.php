<?php
require_once ('services.php');

$services = new Services();
//save received file
//echo $services->generate_token(8);
$data = file_get_contents('php://input');
$data = json_decode($data, true);
$tag = trim($data['tag']);// = 'assign_products';
write_log($data);

$my_data = array();
 

if ($tag) {
	 
	 switch ($tag) {
		case 'upload_orders' :
			$save = $services -> upload_orders($data);
		if($save){
            $arrayback = array();
            foreach($data["orders"] as $key => $value):
                $arrayback[] = array("order_id" => $value['order_id']);
            endforeach;
			$my_data['success'] = 1;
			$my_data['orderList']= array('ordersList' => $arrayback);
		}else{
			$my_data['failure']=1;
			}
			break;

		case 'upload_orders_items' :
			$save = $services -> upload_orders_details($data);
		if($save){
		    $returnval = array();
            foreach($data["orderitems"] as $key => $value):
                $returnval[] = array("order_id" => $value['order_id']);
            endforeach;
			$my_data['success'] = 1;
			$my_data['orderList']= array('orderitemsList' => $returnval);
		}else{                           
			$my_data['failure']=1;
			}

			break;
			case 'assign_products' :

			$products = $services -> assign_products($data);

			if ($products) {
				$my_data['success'] = 1;
				$my_data['productsList'] =  $products;
			}else{
				$my_data['failure']=1;
			}
			break;

		 case 'assign_customers' :
			$customers = $services -> assign_customers($data);
			if ($customers) {
				$my_data['success'] = 1;
				$my_data['customerList'] = $customers;

			}else{
				$my_data['failure']=1;
			}
			break;
		 case 'upload_customers' :
            if($data):
            $save = $services -> upload_customers($data);
            if($save){
                $returnval = array();
                foreach($save["systemid"] as $key => $value):
                    $returnval[] = array("system_customer_id" => $value['system_customer_id'],
                                         "customer_id" => $value['customer_id']);
                endforeach;
                $my_data['success'] = 1;
                $my_data['customer']= array('customerSentList' => $returnval);
            }else{
                $my_data['failure']=1;
            }
			 endif;
			break;
  
		case 'acktag' :
			if ($data['ack'] == 1) {

				switch ($data['key']) {
					case 'prod_system_id' :
						$my_data['acknoledge'] = $data;
						$my_data['time'] = date('Y-m-d his');
						 $data = $services -> update_products($data);
					 
						if($data){
							$my_data['update'] = 'stock updated';
						}else{
							$my_data['failure'] = '1';
						}
						break;
					case 'system_customer_id' :
						$my_data['acknoledge'] = $data;
						$data = $services -> update_customer($data);
						if($data){
							$my_data['update'] = 'customer updated';
						}else{
							$my_data['failure'] = '1';
						}
						break;
                    case 'system_mode_id':
//                         $services->stopPaymentModeSync($data);
                        break;
					default :
						break;
				}
			}
			//$services -> assign_customers($services -> imei);
			break;

//get settings
case 'assign_settings':
 $my_data['success'] = 1;
			$sets = $services->read_settings($data);
			$paymodes = $services->read_payment_mode($data);
			$settins = array();
			/*if($sets==FALSE){ 
				$sets='no settins loaded';
				}
			if($paymodes==false){
				$paymodes ='no payment modes';
				}*/
			$settins['settings']=$sets; 
			$settins['pay_modes']=$paymodes;
			 $my_data['settingList'] = array($settins);
			// $my_data['dudu']=json_encode($settins);
			break;
        case 'device_tracking':
            $save = $services -> receive_location_updates($data);
            if($save){
                $returnval = array();
                foreach($data["datapoints"] as $key => $value):
                    $returnval[] = array("tracker_id" => $value['tracker_id']);
                endforeach;
                $my_data['success'] = 1;
                $my_data['datapointList']= array('datapointLists' => $returnval);
            }else{
                $my_data['failure']=1;
            }
            break;
		default :
			$my_data['failure'] =1;
			break;
	} 
	  
} else {
	$my_data['failure'] = 1;
}



if ($my_data) {
	sending_data($my_data);
	write_log($my_data);

}

function sending_data($data) {

	header('Content-type : application/json');
	echo json_encode($data);
}

function write_log($data) {
	//	$data = json_encode($data);
	//$myfile = fopen("received_data.txt", "a") or die("Unable to open file!");

	file_put_contents("received_data.txt", print_r($data, true),FILE_APPEND);
//    file_put_contents($data['imei'],print_r($data,true),FILE_APPEND);

	//fclose($myfile);
} 


