<?php
$wm = array('0' => lang('no'), '1' => lang('yes'));
$ps = array('0' => lang("disable"), '1' => lang("enable"));
?>
<script>
    $(document).ready(function () {
        <?php if(isset($message)) { echo 'localStorage.clear();'; } ?>
        var timezones = <?php echo json_encode(DateTimeZone::listIdentifiers(DateTimeZone::ALL)); ?>;
        $('#timezone').autocomplete({
            source: timezones
        });
        if ($('#protocol').val() == 'smtp') {
            $('#smtp_config').slideDown();
        } else if ($('#protocol').val() == 'sendmail') {
            $('#sendmail_config').slideDown();
        }
        $('#protocol').change(function () {
            if ($(this).val() == 'smtp') {
                $('#sendmail_config').slideUp();
                $('#smtp_config').slideDown();
            } else if ($(this).val() == 'sendmail') {
                $('#smtp_config').slideUp();
                $('#sendmail_config').slideDown();
            } else {
                $('#smtp_config').slideUp();
                $('#sendmail_config').slideUp();
            }
        });
        $('#overselling').change(function(){
            if($(this).val() == 1) {
                if($('#accounting_method').select2("val") != 2) {
                    bootbox.alert('<?=lang('overselling_will_only_work_with_AVCO_accounting_method_only')?>');
                    $('#accounting_method').select2("val", '2');
                }
            }
        });
            $('#accounting_method').change(function () {
                var oam = <?=$account_settings->accounting_method?>, nam = $(this).val();
                if(oam != nam) {
                    bootbox.alert('<?=lang('accounting_method_change_alert')?>');
                }
            });
$('#accounting_method').change(function(){
    if($(this).val() != 2) {
        if($('#overselling').select2("val") == 1) {
            bootbox.alert('<?=lang('overselling_will_only_work_with_AVCO_accounting_method_only')?>');
            $('#overselling').select2("val", 0);
        }
    }
});
$('#item_addition').change(function(){
    if($(this).val() == 1) {
        bootbox.alert('<?=lang('product_variants_feature_x')?>');
    }
});
});
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i><?= lang('account_settings'); ?></h2>
        <div class="box-icon">
           <!-- <ul class="btn-tasks">
                <li class="dropdown"><a href="<?=site_url('system_settings/paypal')?>" class="toggle_up"><i class="icon fa fa-paypal"></i><span class="padding-right-10"><?=lang('paypal');?></span></a></li>
                <li class="dropdown"><a href="<?=site_url('system_settings/skrill')?>" class="toggle_down"><i class="icon fa fa-bank"></i><span class="padding-right-10"><?=lang('skrill');?></span></a></li>
            </ul>
            -->
        </div>
    </div>
    <div class="box-content"> 
        <div class="row">            
            <div class="col-lg-12">

                <p class="introtext"><?= lang('update_info'); ?></p>

                <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo form_open_multipart("system_settings/account_settings", $attrib);

                $yes_no = array(1=>'Yes', 0=>'No')
                ?>
                <div class="row">            
                    <div class="col-lg-12">
                    <!--<div class="alert alert-warning" role="alert"><p><strong>Corn Job:</strong> <code>0 1 * * * wget -qO- <?php //echo site_url('corn/run'); ?> &gt;/dev/null 2&gt;&amp;1</code> to run at 1:00 AM daily.</p></div>-->
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border"><?= lang('account_config') ?></legend>
                            <div class="col-md-4">
                                <div class="form-group">
                                <?= lang("enable_tax", "enable_tax"); ?>
                                <?php echo form_dropdown('enable_tax',$yes_no, $account_settings->enable_tax, 'class="form-control tip" id="enable_tax"  required="required"'); ?>
                            </div></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("enable_discount", "enable_discount"); ?>
                                    <?php echo form_dropdown('enable_discount',$yes_no, $account_settings->enable_discount, 'class="form-control tip" id="enable_discount-"  required="required"'); ?>
                                </div></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("print_receipt", "print_receipt"); ?>
                                    <?php echo form_dropdown('print_receipt',$yes_no, $account_settings->print_receipt, 'class="form-control tip" id="print_receipt"  required="required"'); ?>
                                </div></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("export_db", "export_db"); ?>
                                    <?php echo form_dropdown('export_db',$yes_no, $account_settings->export_db, 'class="form-control tip" id="export_db"  required="required"'); ?>
                                </div></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("import_db", "import_db"); ?>
                                    <?php echo form_dropdown('import_db',$yes_no, $account_settings->import_db, 'class="form-control tip" id="import_db"  required="required"'); ?>
                                </div></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("allow_product_editing", "allow_product_editing"); ?>
                                    <?php echo form_dropdown('allow_product_editing',$yes_no, $account_settings->allow_product_editing, 'class="form-control tip" id="allow_product_editing"  required="required"'); ?>
                                </div></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("max_discount_per", "max_discount_per"); ?>
                                    <?php echo form_input('max_discount_per', $account_settings->max_discount_per, 'class="form-control tip" id="max_discount_per"  required="required"'); ?>
                                </div></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("allow_stock_receiving", "allow_stock_receiving"); ?>
                                    <?php echo form_dropdown('allow_stock_receiving', $yes_no,$account_settings->allow_stock_receiving, 'class="form-control tip" id="allow_stock_receiving"  required="required"'); ?>
                                </div></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("allow_customer_adding", "allow_customer_adding"); ?>
                                    <?php echo form_dropdown('allow_customer_adding',$yes_no, $account_settings->allow_customer_adding, 'class="form-control tip" id="allow_customer_adding"  required="required"'); ?>
                                </div></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("allow_customer_editing", "allow_customer_editing"); ?>
                                    <?php echo form_dropdown('allow_customer_editing',$yes_no, $account_settings->allow_customer_editing, 'class="form-control tip" id="allow_customer_editing"  required="required"'); ?>
                                </div></div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("track_time_interval", "track_time_interval"); ?>
                                    <?php echo form_input('track_time_interval', $account_settings->track_time_interval, 'class="form-control tip" id="track_time_interval"  required="required"'); ?>
                                </div></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("allow_product_adding", "site_allow_product_addingname"); ?>
                                    <?php echo form_dropdown('allow_product_adding',$yes_no, $account_settings->allow_product_adding, 'class="form-control tip" id="allow_product_adding"  required="required"'); ?>
                                </div></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("allow_product_sharing", "allow_product_sharing"); ?>
                                    <?php echo form_dropdown('allow_product_sharing',$yes_no, $account_settings->allow_product_sharing, 'class="form-control tip" id="allow_product_sharing"  required="required"'); ?>
                                </div></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("allow_post_paid", "allow_post_paid"); ?>
                                    <?php echo form_dropdown('allow_post_paid',$yes_no, $account_settings->allow_post_paid, 'class="form-control tip" id="allow_post_paid"  required="required"'); ?>
                                </div></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("comission_rate", "comission_rate"); ?>
                                    <?php echo form_input('comission_rate', $account_settings->comission_rate, 'class="form-control tip" id="comission_rate"  required="required"'); ?>
                                </div></div>


                            <div class="col-md-4"><div class="form-group">
                                <?= lang("application_type", "application_type"); ?>
                                <?php
                                $app_type = array(
                                  'prosale' => 'Prosale',
                                  'proshop' => 'Proshop',
                                  );
                                echo form_dropdown('application_type', $app_type, $account_settings->application_type, 'class="form-control tip" id="application_type" required="required" style="width:100%;"');
                                ?>
                            </div></div>
                                <div style="clear: both; height: 10px;"></div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="controls"> 
                                            <?php echo form_submit('update_settings', lang("update_settings"), 'class="btn btn-primary"'); ?> 
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?> 
                            </div>

                        </div>
                      <!--  <div class="alert alert-warning" role="alert"><p><strong>Corn Job:</strong> <code>0 1 * * * wget -qO- <?php //echo site_url('corn/run'); ?> &gt;/dev/null 2&gt;&amp;1</code> to run at 1:00 AM daily.</p></div>-->
                    </div>
                </div>  
