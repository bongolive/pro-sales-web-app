<?php 
session_start();
if($_SESSION['LogIn'] != 1){
	header("location: login.php");
}
include('common.php');
include ("connection.php");
if($_POST['userid']){
	$userId = $_POST['userid'];
	$alertName = $_POST['alertName'];
	$startDate = $_POST['startdate'];
	$endDate = $_POST['enddate'];
}else{
	$useIid = '';
	$alertName = '';
	$startDate = '';
	$endDate = '';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />

<title>footprint</title>

<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>

<!--<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script src="http://code.jquery.com/jquery-migrate-1.1.1.min.js"></script>

<script src="http://code.jquery.com/jquery-1.8.0.min.js" type="text/javascript"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>-->
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" media="all" type="text/css" href="jquery-ui-timepicker-addon.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.8.24/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<link href="facebox.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="js/facebox.js" type="text/javascript"></script>




<script type="text/javascript">
  $(document).ready(function(){
  $('#startdate').datetimepicker();
	$('#enddate').datetimepicker();
  
  
  $('a[rel*=facebox]').facebox();
 
    /*$(".deleteitem").click(function(){
		var x=window.confirm("Are you sure want to delete this device?")
		if (x){
			var parent = $(this).closest('TR');
			var id = parent.attr('id');
			var uid = parent.attr('uid');
			$.ajax({
				type: 'POST',
				data: 'id=' +id+"&uid="+uid+"&type=device",
				url: 'deleteitem.php',
				success: function(msg){
					$('#'+id).remove();
				}
			});
		}
	});*/

	
  });
 
 
   function deleteclicked(ctrl){
	var x=window.confirm("Are you sure want to delete this Boundary?")
		if (x){			
			var parent = $(ctrl).closest('TR');
			var id = parent.attr('id');
			var uid = parent.attr('uid');
			$.ajax({
				type: 'POST',
				data: 'id=' +id+"&uid="+uid+"&type=boundary",
				url: 'deleteitem.php',
				success: function(msg){					
					$('#'+id).remove();
					
				}
				});
		}
  }
 
 
 function validate_fields(){
	var s_date = document.getElementById("startdate").value;
	var e_date = document.getElementById("enddate").value;
	
	if(e_date != "" && s_date != ""){
			
		s_date = s_date.split(" ");
		var s_date_arr = s_date[0].split("/");
		var s_time_arr = s_date[1].split(":");
		
		e_date = e_date.split(" ");
		var e_date_arr = e_date[0].split("/");
		var e_time_arr = e_date[1].split(":");
		
		s_date = new Date(s_date_arr[2], s_date_arr[0], s_date_arr[1], s_time_arr[0], s_time_arr[1]);
		e_date = new Date(e_date_arr[2], e_date_arr[0], e_date_arr[1], e_time_arr[0], e_time_arr[1]);
		
		if(Date.parse(s_date.toString()) > Date.parse(e_date.toString())){
			alert("Start Date & Time is Greater then End Date & Time");
			return false;
		}
		
	}
	return true;
	
}
 
</script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>



</head>

<body >
<?php 
//session_start();
//include('common.php');
?>
	<div class="body_class">
    	<div class="center_header_page">
        	<div class="header-left">
            	<a href="index.php" ><img src="images/logo3.png" alt=""  /></a>
            </div><div class="header-right">
            	<span style="font-family:Arial; font-size:14px; color:#FFFFFF; margin-top:8px; float:left">Welcome <?php echo $_SESSION['username'];?> <br /><a href="logout.php" style="color:#FFFFFF">Logout</a></span>
            </div>
            
        </div>
	</div>
	<div class="center_header_pageinner">
    	<div class="menu_wrapperinner">
        	
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="adm_device.php" >Device</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="adm_track.php" >Track</a>
                </div>
            </div>
			  <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="adm_task.php" >Tasks</a>
                </div>
            </div>
			  <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="adm_report.php" >Reports</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="active menu_contentinner">
                		<a href="adm_boundary.php" >Boundaries</a>
                </div>
            </div>
            <div class="menu_firstinner">
            	
                <div class="menu_contentinner">
                	<a href="adm_profile.php" >Profile</a>
                </div>
            </div>
          
            <div class="clear"></div>
			</div>
        </div>
    
    </div>
    <div class="line">
    </div>
    <div class="center_header_page">
      <div class="content" >
        <div class="content_first content_sec_heading">
          <div class="content_thirdlog">
            <!--content_third-->
            <div class="content_thirdlog_left1">
              <div class="content_third_left_second">
                <h3>Search</h3>
                <div class="clear"></div>
                <form id="search_boundary_form" action="" method="post" onsubmit="return validate_fields();" >
                  <div class="content_third_left_contactlogpro">
                    <div class="content_third_left_contactlogpro_input">
                      <select id="userid" name="userid" class="search">
                        <option value="">Select User</option>
                        <?php
						//include ("connection.php");

						$sql="select userid,name from users";
						$result = mysql_query($sql);
						while($row = mysql_fetch_array($result))
						{
                          echo "<option value={$row['userid']}>{$row['name']}</option>";
                        }
						?>
                      </select>
                    </div>
                    <div class="content_third_left_contactlogpro_input">
                      <input type="text" value="<?php echo $alertName; ?>" name="alertName" id="alertName" placeholder="Alert Name"  class="search"/>
                    </div>
                    <div class="content_third_left_contactlogpro_input">
                      <input type="text" value="<?php echo $startDate; ?>" id="startdate" name="startdate" placeholder="Start date" class="search"/>
                    </div>
                    <div class="content_third_left_contactlogpro_input">
                      <input type="text" value="<?php echo $endDate; ?>" id="enddate" name="enddate" placeholder="End date"  class="search"/>
                    </div>
                    
                  </div>
                  <input type="submit" value="" id="btn_search" name="btn_search" class="btnsearch"/>
                </form>
              </div>
              <div style="clear:both; padding:20px 0 0 0; height:40px;"></div>
            </div>
            <div class="content_third_center1"> </div>
            <div class="content_first content_sec_heading">
              <div class="content_thirdlog">
                <!--content_third-->
                <div class="content_third_right1">
                  <table width="660px" align="right" cellpadding="0" cellspacing="0" border="1" bordercolor="#ffffff">
                    <tr style="font-family: Calibri; color:#000000; font-size:16px; text-align:center; background-image:url(images/bgupper.png); background-repeat:repeat ; height:70px;">
                      <td><strong>Alert Name</strong></td>
                      <td><strong>Device(s)</strong></td>
                      <td><strong>Valid From</strong></td>
                      <td><strong>Valid To</strong></td>
                      <td><strong>Boundary</strong></td>
                      <td><strong>Edit</strong></td>
                      <td><strong>Delete</strong></td>
                    </tr>
                    <?php
//include ("connection.php");
$searchCond =' ';
if(isset($_POST['userid']) && $_POST['userid'] !=''){
	$searchCond .=" and b.userid=".$_POST['userid']." ";
}

if(isset($_POST['alertName']) && $_POST['alertName'] !=''){
	$searchCond .=" and b.alertname='".$_POST['alertName']."' ";
}

if(isset($_POST['startdate']) && isset($_POST['enddate']) && $_POST['startdate'] !='' && $_POST['enddate'] !='' ){
	$searchCond .=" and b.valid_from >='".$_POST['startdate']."' and b.valid_to <= '".$_POST['enddate']."' ";
}else if(isset($_POST['startdate']) && $_POST['startdate'] !=''){
	$searchCond .=" and b.valid_from ='".$_POST['startdate']."' ";
}else if(isset($_POST['enddate']) && $_POST['enddate'] !=''){
	$searchCond .=" and b.valid_to ='".$_POST['enddate']."' ";
}



 $sql="SELECT b.*, (SELECT COUNT(ba.alocationid) FROM  boundry_alocation ba WHERE ba.boundryid =  b.boundryid) AS devices FROM boundries b WHERE b.alertname != '' ".$searchCond." ORDER BY b.alertname ASC";

$result = mysql_query($sql);
while($row = mysql_fetch_array($result))
{ 
echo "<tr id='{$row['boundryid']}' uid='{$_SESSION['userid']}' style='font-family: Calibri; color:#000000; font-size:16px; text-align:center; background-image:url(images/bgupper.png); background-repeat:repeat ; height:70px;'>";
echo "<td>{$row['alertname']}</td>";
echo "<td>{$row['devices']}</td>";
echo "<td>{$row['valid_from']}</td>";
echo "<td>{$row['valid_to']}</td>";
echo "<td align='center' width='90'><a rel='facebox' href='adm_showboundry.php?lat=".$row["centerlat"]."&lng=".$row["centerlng"]."&radius=".$row["radius"]."'><img src='images/boundry.png' /></a></td>";
echo "<td align='center' width='90'><a rel='facebox' href='adm_boundrypopmaker.php?do=edit&editid=".$row["boundryid"]."'><img src='images/EDIT.png' /></a></td>";
echo "<td><a href='javascript:void(0);' class='deleteitem1' id='deletetask' onclick='deleteclicked(this);'><img src='images/deletebutton.png' /></a></td>";
echo "</tr>";
}
?>
                  </table>
                  
                  
                  <div style="width:126px; height:40px; margin:20px 0 0 20px; padding:10px 0 0 0; clear:both;">
                  <!--<a rel="facebox" href="boundrypop.php"><img src="images/btn-add_boundry.png" width="126" height="40" /></a>-->
                <a rel="facebox" href="adm_boundrypopmaker.php?do=add"><img src="images/btn-add_boundry.png" alt="Add Boundary" width="126" height="40" /></a> </div>
                </div >
                
              </div>
              <div id="dialog2"></div>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <div id="dialog"></div>
          <div class="clear"></div>
        </div>
        <!--content_third-->
      </div>
    </div>
    
</div>
    <div class="footer-wrapper">
    	<div class="center_footer_page">
        	<div class="footer_center">
            	<div class="footer_navi" style="display:none;">
            	<a href="" >Home</a>
                <a href="" >Company</a>
                <a href="" >Clients</a>
                <a href="" >Resources</a>
                <a href="" >Support</a>
                <a href="" >Blog</a>
                <a href="" >Contact</a>
                </div>
            </div>
         <div class="clear"></div>
        </div>
    </div>
    <div class="center_header_page">
    	<div class="footer_buttom">
    	<h3>FOOT PRINT</h3><p>Copyright &copy; 2012. All Rights Reserved.</p>
        </div>
         <div class="clear"></div>
    </div>

<!--<script src="js/analytics.js" type="text/javascript"></script>-->
</body>
</html>
