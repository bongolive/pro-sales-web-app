<?php
      /* file_put_contents("imei.txt',file_get_contents("php:input"));
exit;*/
if (isset($_POST['tag']) && $_POST['tag'] != '') { 
	$tag = $_POST['tag'];
 
	require_once './include/DB_Functions.php';
	$db = new DB_Functions();
        
	$response = array("tag" => $tag, "success" => 0, "error" => 0);
 
	if ($tag == 'login') { 
		$cardno = $_POST['username'];
		$password = $_POST['password'];
 
        $user = $db->getUserRegNoAndPasswordApp($cardno, $password);
        $users = $db->getRolesApp($cardno) ;
        if ($user != false) { 
            $response["success"] = 1;
            $response["user"]["uid"] = $user["unique_id"];
            $response["user"]["username"] = $user["username"]; 
            $response["user"]["created_at"] = $user["created_at"]; 
            $response["user"]["member"] = $user["member"] ;
            $response["user"]["role"] = $users["role_id"] ;  
            echo json_encode($response);
		} else { 
			$response["error"] = 1;
			$response["error_msg"] = "namba ya kadi au password imekosewa!";
			echo json_encode($response);
		} 
	} else if ($tag == 'sajiliuser')
        {
            $_cardno = $_POST['username'] ;
            $_pass = $_POST['password'] ;
            if($db->userYupoApp($_cardno))
            {
                $response["error"] = 2;
                $response["error_msg"] = "namba ya kadi imeshasajiliwa!" ;
                echo json_encode($response) ;
            } else {
                $_user = $db->sajiliUserApp($_cardno, $_pass) ;
                $userrole = $db->getRolesApp($cardno) ;
                if($_user)
                {
                $response["success"] = 1;
                $response["user"]["uid"] = $_user["unique_id"];
                $response["user"]["username"] = $_user["username"];  
                $response["user"]["member"] = $_user["member"];
                $response["user"]["created_at"] = $_user["created_at"]; 
                $response["user"]["role"] = $userrole["role_id"] ;
                echo json_encode($response);
                } else {
                    $response["error"] = 2 ;
                    $response["error_msg"] = "kuna tatizo katika usajili au ni :".$db->err ;
                    echo json_encode($response) ;
                }
             }
         } elseif ($tag == 'boresha') 
             {
                $fname = $_POST['firstname'];
                $lname = $_POST['lastname'] ;
                $gender = $_POST['gender'];
                $bdate = $_POST['date_of_birth'] ;
                $bwilaya = $_POST['district_of_birth'] ;
                $bmkoa = $_POST['region_of_birth'] ;
                $email = $_POST['email'];
                $phone = $_POST['phone_number'];   
                $residence = $_POST['location'] ;
                $occupation = $_POST['occupation'] ;
                $cardno = $_POST['card_number'];
                $regdate = $_POST['tarehe_ya_kujiunga'];
                $regarea = $_POST['tawi'];
                $membertype = $_POST['membership_type'];
	        $ndugu = $_POST['next_of_kin'];   
		$editor = $db->editMwanachama($fname, $shina,$cardno,$email,$wilaya,$phone);
			if ($editor) { 
				$response["success"] = 1; 
			        $response["mwanachama"]["firstname"] = $editor["firstname"]; 
			        $response["mwanachama"]["lastname"] = $editor["lastname"]; 
				$response["mwanachama"]["gender"] = $editor["gender"];
			        $response["mwanachama"]["date_of_birth"] = $editor["date_of_birth"]; 
			        $response["mwanachama"]["district_of_birth"] = $editor["district_of_birth"]; 
			        $response["mwanachama"]["region_of_birth"] = $editor["region_of_birth"]; 
			        $response["mwanachama"]["email"] = $editor["email"];
				$response["mwanachama"]["phone_number"] = $editor["phone_number"];
			        $response["mwanachama"]["location"] = $editor["location"];
			        $response["mwanachama"]["occupation"] = $editor["occupation"];
				$response["mwanachama"]["card_number"] = $editor["card_number"];
			        $response["mwanachama"]["tarehe_ya_kujiunga"] = $editor["tarehe_ya_kujiunga"];
				$response["mwanachama"]["tawi"] = $editor["tawi"]; 
				$response["mwanachama"]["membership_type"] = $editor["membership_type"];
                                $response["mwanachama"]["next_of_kin"] = $editor["next_of_kin"] ;
				echo json_encode($response);
			} else { 
				$response["error"] = 1;
				$response["error_msg"] = "Kuna tatizo kurekebisha taarifa zako";
				echo json_encode($response);
			}
		
      } else if ($tag == 'sajili') { 
	        $fname = $_POST['firstname'];
                $lname = $_POST['lastname'] ;
                $gender = $_POST['gender'];
                $bdate = $_POST['date_of_birth'] ;
                $bwilaya = $_POST['district_of_birth'] ;
                $bmkoa = $_POST['region_of_birth'] ;
                $email = $_POST['email'];
                $phone = $_POST['phone_number'];   
                $residence = $_POST['location'] ;
                $occupation = $_POST['occupation'] ;
                $cardno = $_POST['card_number'];
                $regdate = $_POST['tarehe_ya_kujiunga'];
                $regarea = $_POST['shina'];
                $tawi = $_POST['tawi'] ;
                $membertype = $_POST['membership_type'];
	        $ndugu = $_POST['next_of_kin'];
		if ($db->mwanachamaYupo($cardno)) { 
			$response["error"] = 2;
			$response["error_msg"] = "Mwanachama Ameshasajiliwa" ;
			echo json_encode($response);
		} else { 
			$user = $db->sajiliMwanachamaApp($fname,$lname, $gender,$bdate,$bwilaya,
                                $bmkoa,$email, $phone, $residence,$occupation,$cardno,$regdate,$regarea,$membertype,$ndugu,$tawi);
			if ($user) { 
				$response["success"] = 1; 
			        $response["mwanachama"]["firstname"] = $user["firstname"]; 
			        $response["mwanachama"]["lastname"] = $user["lastname"]; 
				$response["mwanachama"]["gender"] = $user["gender"];
			        $response["mwanachama"]["date_of_birth"] = $user["date_of_birth"]; 
			        $response["mwanachama"]["district_of_birth"] = $user["district_of_birth"]; 
			        $response["mwanachama"]["region_of_birth"] = $user["region_of_birth"]; 
			        $response["mwanachama"]["email"] = $user["email"];
				$response["mwanachama"]["phone_number"] = $user["phone_number"];
			        $response["mwanachama"]["location"] = $user["location"];
			        $response["mwanachama"]["occupation"] = $user["occupation"];
				$response["mwanachama"]["card_number"] = $user["card_number"];
			        $response["mwanachama"]["tarehe_ya_kujiunga"] = $user["tarehe_ya_kujiunga"];
				$response["mwanachama"]["shina"] = $user["shina"]; 
                                $response["mwanachama"]["tawi"] = $user["tawi"] ;
				$response["mwanachama"]["membership_type"] = $user["membership_type"];
                                $response["mwanachama"]["next_of_kin"] = $user["next_of_kin"] ;
				echo json_encode($response);
			} else { 
				$response["error"] = 1;
				$response["error_msg"] = "Kuna tatizo katika usajili ".$db->err;
				echo json_encode($response);
			}
		}
 /*======>> member leaders*/  
                        } else if($tag == 'mwnchm_vnz')
            {
              $tawi = $_POST['tawi'] ;
              $_leader = $db->getMemberAppArray($tawi) ;
              if($_leader)
              {
                  while($row = mysql_fetch_array($_leader))
                  {  
                    $memarray = array() ;
                    $memarray["firstname"] = $_leader["firstname"]; 
                    $memarray["lastname"] = $_leader["lastname"]; 
                    $memarray["gender"] = $_leader["gender"];
                    $memarray["date_of_birth"] = $_leader["date_of_birth"]; 
                    $memarray["district_of_birth"] = $_leader["district_of_birth"]; 
                    $memarray["region_of_birth"] = $_leader["region_of_birth"]; 
                    $memarray["email"] = $_leader["email"];
                    $memarray["phone_number"] = $_leader["phone_number"];
                    $memarray["location"] = $_leader["location"];
                    $memarray["occupation"] = $_leader["occupation"];
                    $memarray["card_number"] = $_leader["card_number"];
                    $memarray["tarehe_ya_kujiunga"] = $_leader["tarehe_ya_kujiunga"];
                    $memarray["shina"] = $_leader["shina"]; 
                    $memarray["tawi"] = $_leader["tawi"] ;
                    $memarray["membership_type"] = $_leader["membership_type"];
                    $memarray["next_of_kin"] = $_leader["next_of_kin"] ;
                    array_push($response["tawiallmember"], $memarray) ;
                  }
                    $response["success"] = 1; 
                    echo json_encode($response) ;
              }
        } else if($tag == 'userinfo')
        {
            $card = $_POST['card_number'] ;
            $details = $db->getMemberDetailsApp($card) ;
            if($details)  
            {
                                $response["success"] = 1 ;
                                $response["mem_info"]["firstname"] = $details["firstname"]; 
			        $response["mem_info"]["lastname"] = $details["lastname"]; 
				$response["mem_info"]["gender"] = $details["gender"];
			        $response["mem_info"]["date_of_birth"] = $details["date_of_birth"]; 
			        $response["mem_info"]["district_of_birth"] = $details["district_of_birth"]; 
			        $response["mem_info"]["region_of_birth"] = $details["region_of_birth"]; 
			        $response["mem_info"]["email"] = $details["email"];
				$response["mem_info"]["phone_number"] = $details["phone_number"];
			        $response["mem_info"]["location"] = $details["location"];
			        $response["mem_info"]["occupation"] = $details["occupation"];
				$response["mem_info"]["card_number"] = $details["card_number"];
			        $response["mem_info"]["tarehe_ya_kujiunga"] = $details["tarehe_ya_kujiunga"];
				$response["mem_info"]["tawi"] = $details["tawi"]; 
                                $response["mem_info"]["shina"] = $details["shina"] ;
				$response["mem_info"]["membership_type"] = $details["membership_type"];
                                $response["mem_info"]["next_of_kin"] = $details["next_of_kin"] ;
                echo json_encode($response);
            } else {
                $response["error"] = 2;
                $response["error_mgs"] = "Habari za mwnchm_arr hazipo" ;
            }
        } else
        {
		echo "tag haitambuliki";
	}
} else {
	echo "kuna tatizo na data";
}
?>
