<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Customers extends User_Controller {
	
	public $session_userid = '';
	public function __construct()
	{
		parent::__construct();
		$this->load->model('customer_model');
		$this->session_userid = $this->session->userdata('userid');
	}

	public function index()
	{
		$condUserPerm = array('userid'=>$this->session->userdata('userid'));
		$user_permissions = $this->user_model->getUserPermissions($condUserPerm);
		//Make all tab not allowed
		$header['track_tab'] ="";
		$header['device_tab'] ="";
		$header['tasks_tab'] ="";
		$header['reports_tab'] ="";
		$header['deliveries_tab'] ="";
		$header['customers_tab'] ="";
		$header['boundaries_tab'] ="";
		$header['support_tab'] ="allow";
		$header['pathfinder_tab'] ="";
		$header['stocks_tab'] ="";
		$header['registrations_tab'] ="";
		
		foreach($user_permissions as $user_permission){
			if($user_permission['permission_name']=='track tab'){
				$header['track_tab'] ="allow";
			}else if($user_permission['permission_name']=='device tab'){
				$header['device_tab'] ="allow";
			}if($user_permission['permission_name']=='tasks tab'){
				$header['tasks_tab'] ="allow";
			}if($user_permission['permission_name']=='reports tab'){
				$header['reports_tab'] ="allow";
			}if($user_permission['permission_name']=='deliveries tab'){
				$header['deliveries_tab'] ="allow";
			}if($user_permission['permission_name']=='customers tab'){
				$header['customers_tab'] ="allow";
			}if($user_permission['permission_name']=='boundaries tab'){
				$header['boundaries_tab'] ="allow";
			}if($user_permission['permission_name']=='pathfinder survey tab'){
				$header['pathfinder_tab'] ="allow";
			}if($user_permission['permission_name']=='stocks tab'){
				$header['stocks_tab'] ="allow";
			}if($user_permission['permission_name']=='registrations tab'){
				$header['registrations_tab'] ="allow";
			}						
		}
		
		if($header['customers_tab'] !="allow"){
			redirect('profile/support');
		}
		
		$cond = array('userid'=>$this->session_userid);
		$fields['tb_data'] = $this->customer_model->getCustomer($cond);
		$ft_data['js'] = array('plugins/dataTables/jquery.datatables.min.js','custom/custom-datatable.js');
        $fields['tb_name'] = 'customers_tb_name';
        $fields['row_fields'] = array('customername','mobilenumber','email','address');
        $fields['tb_headers'] = array('customer_name','mobile_number','email','address','action');
        $header['cus_active'] = 'class="active"';
        $fields['add_btn'] = 'new_customer';

        $this->load->view('template/header',$header);
        $this->load->view('template/content/table',$fields);
        $this->load->view('template/footer',$ft_data);

	}
    
    public function edit($customerid){
        
        // Refer to the edit function in the device controller
		$cond = array('id'=>$customerid);
		$fields['customers'] = $this->customer_model->getCustomer($cond);
       $this->load->view('template/content/edit_customer',$fields);
    }
	
	
	public function add()
	{
		$data = array(
			'userid' => $this->session_userid,
			'customername' => $this->input->post('customername'),
			'mobilenumber' => $this->input->post('mobilenumber'),
			'email' => $this->input->post('email'),
			'address' => $this->input->post('address')
		);
		$this->customer_model->add($data);
		redirect('customers');
	}
	
	public function update()
	{
		$data = array(
			'customername' => $this->input->post('customername'),
			'mobilenumber' => $this->input->post('mobilenumber'),
			'email' => $this->input->post('email'),
			'address' => $this->input->post('address'),
		);
		$cond = array('id'=>$this->input->post('customerid'));
		$this->customer_model->update($data,$cond);
		redirect('customers');
	}
		
	public function delete($customerid)
	{
		$cond = array('id'=>$customerid);
		$result = 	$this->customer_model->delete($cond);
		redirect('customers');
	}

}



/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */
