				<!-- Main page container -->
<div id="revealModal" class="modal fade hide">Loading</div>
<div id="revealDelete" class="modal fade hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;top: 0%;left: 96%;z-index: 2000;background-color: white;opacity: 1;border-radius: 20px;padding: 0px 5px 3px 5px;">&times;</button>
                <h4><?php _l('confirm_deletion'); ?></h4>
               
            </div>
</div><!-- Main page container -->
  
<section class="container" role="main">
		 <!-- Grid row -->
				<div class="row">

					<!-- Smart wizard -->
					<article class="span12  data-block">

						<header class="info">
							<h2><?php _l('product_warehouse'); ?></h2>
					   
            </header> 

        </article>

    </div>	
    <div class="row">
 
    	 <article class="span6 data-block">
								<div class="modal-header">
									<h4><?php _l('product_warehouse'); ?></h4><h4>
                                        <a href="<?php echo site_url('products/warehouses'); ?>" class="btn btn-alt" data-dismiss="modal">Back</a>
                                    </h4>
								</div>
								<div class="modal-body"> 
								
								 <table class="datatable table table-striped table-bordered table-hover" id="example-2">
  		<thead>

									<tr>

                                        <?php foreach($tb_headers as $header){?>

										<th><?php _l($header); ?></th>

                                        <?php } ?>
									</tr>
								</thead>

								<tbody>
 				  <?php if ($tb_data){ 
									foreach ($tb_data as $data):  
									  
                                            $id = $table_id;
                                     ?>
									<tr class="even gradeC" id="<?php echo $id; ?>">
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php echo $data[$field]; ?></td>
                                        <?php endforeach; ?>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
								</tbody>
	</table>
                                    </div></article>
        <div class="row">
            <div class="modal-header">
                <h4><?php _l('warehouse_stock'); ?></h4><h4>
                     </h4>
            </div>
            <article class="span6 data-block">
                                    <table class="datatable table table-striped table-bordered table-hover" id="example-2">
                                        <thead>

                                        <tr>

                                            <?php foreach($tb_headers2 as $header){?>

                                                <th><?php _l($header); ?></th>

                                            <?php } ?>
                                        </tr>
                                        </thead>

								<tbody>
 				  <?php if ($tb_data2){
									foreach ($tb_data2 as $data2):
                                     ?>
									<tr class="even gradeC" >
                                        <?php foreach ($row_fields2 as $field): ?>
										  <td><?php echo $data2[$field];
                                        ?></td>
                                        <?php endforeach; ?>
									</tr>
									<?php endforeach;
										}else {
										echo '<tr> <td> No data available <td> </tr>';
										}
									?>
								</tbody>
	</table>
           
		</article>
                        </div>                        	
                        </div>	
                        </section>


                        		
					 