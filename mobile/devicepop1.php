<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="stylesheet_fp2.css" type="text/css" />
<link rel="stylesheet" href="bx_styles.css" type="text/css" />
<title>footprint</title>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>

<script src="js/jquery.bxSlider.js" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function(){
    
	 $('.slider').bxSlider({
    auto: true,
    pager: true
  });
  });
</script>
</head>

<body>
<?php
session_start();
include('common.php');
?>
	  <div class="body_class">
	  <div class="center_header_page">
		  <div class="header-left">
		 <a href="index.php" ><img src="images/logo3.png" alt=""  /></a>
	      </div><div class="header-right">
		  <span style="font-family:Arial; font-size:14px; color:#FFFFFF; margin-top:8px; float:left">Welcome <?php echo $_SESSION['username'];?><br /><a href="logout.php" style="color:#FFFFFF">Logout</a></span>
	      </div>
	      
	  </div>
	  </div>
	  <div class="line">
      </div>
    <div class="center_header_page">
      <div class="content" >
        
            <div class="content_first content_sec_heading">
                
                

         
        <div class="content_thirdlog"><!--content_third-->
         	<div class="content_thirdlogpopup_left">
         	  
                <div class="content_third_left_second">
                	<h3>Add Device</h3>
                    <div class="clear"></div>
					<form id="device_form" action="addeditdevice.php" method="post" >
                    <div class="content_third_left_contactlogpro">
					<span style="font-size:10px">*IMEI number can be get by dialing *#06#</span>
                    	<div class="content_third_left_contactlogpro_input">
                    	<p>Device IMEI:</p><input type="text" value="" name="txt_imei" id="txt_imei"/>
						
                        </div>
                        <div class="content_third_left_contactlogpro_input">
                        <p>Device Name:<span style="font-size:10px">(User Defined)</span></p><input type="text" value="" name="txt_name" id="txt_name" />
                        </div>
					  <div class="content_third_left_contactlogpro_select">
					  <p>Tracking Interval:</p> 
                    	<select name="checking_interval" id="checking_interval" style="color:#000000; font-size:16px">
                          <option selected value="-1">Please Select</option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="5">5</option>
                          <option value="10">10</option>
                          <option value="15">15</option>
						  <option value="20">20</option>
						  <option value="30">30</option>
						  <option value="60">60</option>
						  <option value="0">Stop</option>
                        </select>
                    	
					  </div>
						<div class="content_third_left_contactlogpro_input">
                    	<p>Mobile Number:</p><input type="text" value="" name="txt_mobile" id="txt_mobile" />
                        </div>
						<div class="content_third_left_contactlogpro_input">
                    	<p>Name of Holder:</p><input type="text" value="" name="txt_holder_name" />
						<input type="hidden" value="<?php echo $_SESSION['userid']?>" name="txt_userid" />
						<input type="hidden" value="add" name="addedit" />
                        </div>
						                        
                        <div class="content_third_left_buttondevice">
                        	<a href="javascript:void(0);" onclick="checkSave();">SUBMIT</a>
                        </div>
                    </div>
                    </form>
              </div>
         	</div>            
         	
			
         </div><!--content_third-->
            </div>
      </div>
    
</div>
<div class="footer-wrapper">
	 <div class="center_footer_page">
        	<div class="footer_center">
            	<div class="footer_navi">
				<a href="" >&nbsp;</a>
				<a href="" >&nbsp;</a>
            	<a  href="device.php" >Device</a>
				<a href="track.php" >Track</a>
                <a href="support.php" >Support</a>
                <a href="logout.php">Logout</a>
               
                </div>
            </div>
	   <div class="clear"></div>
	  </div>
      </div>
      <div class="center_header_page">
	  <div class="footer_buttom">
	  <h3>FOOT PRINT</h3><p>Copyrihgt &copy; 2012. All Rights Reserved.</p>
	  </div>
	   <div class="clear"></div>
      </div>
<script type="text/javascript">
  function checkSave(){
		
		  var imei = $("#txt_imei");
		  var name = $("#txt_name");
		  var interval = $("#checking_interval");
		
		 if(imei.val().length <= 0){
			alert("IMEI number should not blank");
			return false;
		 } else if(name.val().length <= 0){
			alert("Device Name should not blank");
			return false;
		 }else if(interval.val() < 0){
			alert("Interval should be selected");
			return false;
		 }
		 else{
			document.getElementById('device_form').submit();
		  }
		  
	  }
</script>
</body>
</html>
