                    <div class="modal-header">

									<h4><?php _l('add_task'); ?></h4>

                                    

								</div>

								

                                <form class="form-horizontal" action="<?php echo site_url("admin/updateTask"); ?>" method="post">

                                    <fieldset>
                                        <div class="modal-body">
                                        <div class="control-group">

                                        	<label class="control-label" for="device_name" ><?php _l('device_name'); ?></label>

                                        	<div class="controls">

                                        	<select name="device_name" id="device_name" required >
												<option <?php if($tasks['phoneid']==''){ echo 'selected';} ?>></option>
                                                <?php foreach ($devices as $device): ?>
                                                <option value="<?php echo $device['phoneid'];?>"  <?php if($device['phoneid']== $tasks['phoneid']){ echo 'selected';} ?>><?php echo $device['device_name'];?></option>
												<?php endforeach; ?>

											</select>

                                        	<input name="taskid" type="hidden" id="taskid" value="<?php echo $tasks['id'];?>" />
                                        	</div>

                                    	</div>

                                    	<div class="control-group">

                                        	<label class="control-label" for="subject"><?php _l('subject'); ?></label>

                                        	<div class="controls">

                                        	<input name="subject" id="subject" value="<?php echo $tasks['subject'];?>" class="input-xlarge" type="text" required >

                                        	</div>

                                    	</div>

                                        <div class="control-group">

                                        	<label class="control-label" for="task" ><?php _l('task'); ?></label>

                                        	<div class="controls">

                                        	   <textarea name="task" id="task" class="input-xlarge" rows="5"><?php echo $tasks['task'];?></textarea>

                                        	</div>

                                    	</div>

                                        <div class="control-group">

                                        	<label class="control-label" for="status"><?php _l('status'); ?></label>

                                        	<div class="controls">

                                        	<select name="status" id="status" required >
												<option <?php if($tasks['status']==''){ echo 'selected';} ?>></option>
                                                <option value="New" <?php if($tasks['status']=='New'){ echo 'selected';} ?>><?php _l('new'); ?></option>
												<option value="In-Progress" <?php if($tasks['status']=='In-Progress'){ echo 'selected';} ?>><?php _l('in_progress'); ?></option>
												<option value="Done" <?php if($tasks['status']=='Done'){ echo 'selected';} ?>><?php _l('done'); ?></option>
											</select>

                                        	</div>

                                    	</div>
                                        <div class="control-group">

                                        	<label class="control-label" for="comments"><?php _l('comments'); ?></label>

                                        	<div class="controls">

                                            <label class="checkbox custom-checkbox">

                                        	   <input type="text" name="comments" id="comments" value="<?php echo $tasks['comments'];?>">

                                            </label>

                                            </div>

                                    	</div>

                                                    

								</div>

								<div class="modal-footer">

									<a href="#" class="btn btn-alt" data-dismiss="modal">Close</a>

									<div class="form-actions">

            				                <button class="btn btn-alt btn-large btn-primary" type="submit" style="margin-top: -92px;padding: 6px 10px;">Submit</button>

                                    </div>

								</div>

                            </fieldset>

                        </form>