<?php
Class Login_model extends MY_Model {
	public $table = 'users';
	public $table_id = 'user_id';
	function __construct() {
		parent::__construct();
	}

	function form_validation() {
		$this -> form_validation -> set_rules('username', 'Username', 'required');
		$this -> form_validation -> set_rules('password', 'Password', 'required');
	}

	function login($username, $password) {
		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		$this -> db -> where(array('username' => $username, 'password' => sha1($password)));
		$this -> db -> join('user_roles', 'users.role = role_id', 'left');
		$this -> db -> limit(1);

		$query = $this -> db -> get();

		if ($query -> num_rows() == 1) {
			return $query -> result_array();
		} else {
			return false;
		}
	}

}
