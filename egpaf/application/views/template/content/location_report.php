   <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
 
                            <div class="box">
                                <div class="box-header">
                                        
                                  <h4 class="page-header box-title">
<?php echo lang('location_reports') ?>
<small> View Report per Locations, select a Location from the list </small>
</h4>                                                                 
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="type2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr> 
                                            	  <?php foreach($tb_headers as $header){?>

										<th><?php echo lang($header); ?></th>

                                        <?php } ?>
                                            	 <th><?php echo lang('action'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
   <?php if ($tb_data){ 
									foreach ($tb_data as $data):   
                                            $id = $data['location_id'];
                                    ?>
									<tr>
										 
                                        <?php foreach ($row_fields as $field): ?>
										  <td><?php   
										   									  
										  if ($field == 'total_response') {
										  		if($summary){
											foreach ($summary as $responses) {
												if ($responses['location_code'] == $data['location_code']) {
													echo $responses['total'];
												}
											}}else{echo 0;}
										} else {
											echo $data[$field];
										}
											 
										  	?></td>
                                        <?php endforeach; ?>
										 <td> 
                                                	<a href="<?php echo site_url('reports/location_report').'/'.$data['location_code']; ?>" type="button"   class="btn btn-small btn-success" title="View" ><i class="fa fa-eye"></i> <?php echo lang('view') ?></a>
                                                	 
											</td>
									</tr>
									<?php endforeach;
										} 
									?>
                                        </tbody>
                                         
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
          