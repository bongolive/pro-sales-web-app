<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                	<a href="<?php echo site_url('accounts'); ?>" style="float:right;margin: 10px;" type="button" class="btn btn-primary"><?php  echo lang('back')?> </a>
                                	</div>	
                                  	  	<div class="box-body">
							  
<div class="table-responsive">
<table class="table">
<tbody>
	
	 <?php
		foreach ($tb_headers as $header) {
			foreach ($info as $data) { ?>
				 
				<tr><th><?php echo lang($header)?></th><td><?php echo $data[$header]; ?></td></tr>
		<?php 	 
		}
		}
	 	?> 
	</div>
                         
                        </div>                        	
                        </div>	
                        </section>
                        		
			