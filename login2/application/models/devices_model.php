<?php
class Devices_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_devicelistbyid($deviceid = FALSE)
	{
		if ($deviceid === FALSE)
		{
			$query = $this->db->get('phones');
			return $query->result_array();
		}
	
		$query = $this->db->get_where('phones', array('phoneid' => $deviceid));
		return $query->row_array();
	}
	
	
	public function get_devicelistbyImei($imei)
	{
		$query = $this->db->get_where('phones', array('phone_imei' => $imei));
		return $query->row_array();
	}
	
	
	public function get_device($cond)
	{
		$query = $this->db->get_where('phones',$cond);
		return $query->row_array();
	}
	
	
	public function get_devicelistbyuserid($userid = FALSE)
	{
		if ($userid === FALSE)
		{
			$query = $this->db->query("select phones.*,phones_users.userid,users.name as client_name from phones,phones_users,users where phones.phoneid=phones_users.phoneid and phones_users.userid=users.userid");
			return $query->result_array();
		}
	
		$query = $this->db->query("select phones.*,phones_users.userid,users.name as client_name from phones,phones_users,users where phones.phoneid=phones_users.phoneid and phones_users.userid=users.userid and phones_users.userid=".$userid);
		return $query->result_array();
	}
	
	
	public function add_device($data)
	{
		return $this->db->insert('phones', $data);
	}
	
	public function update_divice($data,$cond)
	{
		$this->db->where($cond);
		return $this->db->update('phones', $data);
	}
	
	public function update($data,$phoneid)
	{
		$this->db->where('phoneid',$phoneid);
		return $this->db->update('phones', $data);
	}
	
	public function update_diviceByImei($data,$imei)
	{
		$this->db->where('phone_imei',$imei);
		return $this->db->update('phones', $data);
	}
	
	public function add_device_users($data)
	{
		return $this->db->insert('phones_users', $data);
	}
	
	public function deleteDeviceUsersByPhoneId($phoneid)
	{
		return $this->db->delete('phones_users', array('phoneid' => $phoneid)); 
	}
	
	public function deleteDeviceByPhoneId($phoneid)
	{
		return $this->db->delete('phones', array('phoneid' => $phoneid)); 
	}
	
	
	public function get_deviceFromPhonesUsers($userid,$imei)
	{
	
		$query = $this->db->query("select * from phones_users where userid = $userid and imei = '$imei'");
		return $query->result_array();
	}
	
	public function get_deviceLocationByImei($imei,$sdate,$edate)
	{
		$query = $this->db->query("select * from location where imei = '$imei' and length(imei) > 0 and date between '$sdate' and '$edate'");
		return $query->result_array();
	}
	
	public function delete($deviceid, $userid)
	{
		$data =array(
				'phoneid' => $deviceid,
				'userid' => $userid
		);
		return $this->db->delete('phones_users', $data);
	}
	
	public function get_lastLocation($userid)
	{
		$query = $this->db->query("SELECT pu.phoneid, pu.imei, p.device_name, p.mobile_no, p.holder_name , 
	ifNull((SELECT l.latitude FROM location l WHERE l.imei = pu.imei ORDER BY l.servertime DESC LIMIT  1 ),NULL) AS latitude , ifNull(( SELECT l.longitude FROM location l WHERE l.imei = pu.imei ORDER BY l.servertime DESC LIMIT  1 ),NULL) AS longitude, ifNull(( SELECT l.date FROM location l WHERE l.imei = pu.imei ORDER BY l.date DESC LIMIT  1),NULL) AS localdate FROM phones_users pu INNER JOIN phones p ON pu.imei = p.phone_imei WHERE pu.userid =$userid");
		return $query->result_array();
	}
	
	

}
