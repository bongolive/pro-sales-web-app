<?php
class Employees_model extends MY_Model {
	public $table = 'employees';
	public $table_id = 'id';

	function __construct() {
		parent::__construct();
	}

	public function form_validation() {

		$this -> form_validation -> set_rules('firstname', 'Firstname', 'trim|required');
		$this -> form_validation -> set_rules('lastname', 'lastname', 'trim|required');
		$this -> form_validation -> set_rules('doe', 'Date of Employment', 'required');
		$this -> form_validation -> set_rules('role', 'Role', 'trim|required');
		$this -> form_validation -> set_rules('mobile', 'mobile', 'required|numeric|callback_checkmobile[' . $this -> input -> post('employee_id') . ']');
		$this -> form_validation -> set_rules('email', 'email', 'valid_email|callback_checkmail[' . $this -> input -> post('employee_id') . ']');
	}

	public function get_modules($where = FALSE) {

		$this -> table = 'modules';

		$modules = $this -> read($where);
		if ($modules) {
			return $modules;
		} else {
			return FALSE;
		}

	}

	public function save_user($data) {
		$this -> table = 'users';
		if ($this -> save($data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update_users($id,$data) {
		$this -> table = 'users';
		$this->table_id = 'user_id';
		if ($this -> update($id,$data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function read_employee($where) {
		$this -> table = 'employees';
		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		$this -> db -> where($where);
		$this -> db -> join('users', 'employees.id = users.employee_id', 'left');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

}
